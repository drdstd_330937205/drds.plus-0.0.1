package drds.plus.repository.berkeley.spi;

import drds.plus.executor.repository.RepositoryConfig;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Properties;

public class BDBConfig extends RepositoryConfig {

    public static final String BDB_REPO_CONFIG_FILE_PATH = "BDB_REPO_CONFIG_FILE_PATH";
    /**
     * 清理县城count
     */
    public int cleaner_thread_count;
    /**
     * 清理者
     */
    public int cleaner_batch_file_count;
    public String cleaner_min_utilization = "50";
    public boolean auto_clean;
    public String cleanerLazyMigration = "FALSE";

    // degree:{READ_UNCOMMITTED|READ_COMMITTED|REPEATABLE_READ|SERIALIZABLE}
    public String default_txn_isolation;
    // 最大并发请求数
    public int max_concurrent_request = 100000;
    /**
     * 在diamond中的appName,用来拉配置,appName如果存在，则schema_file和machine_topology都无效。互斥的。
     */
    protected String app;
    /**
     * 秒级，事务超时时间
     */
    protected int txnTimeout;
    /**
     * 秒级，结果集超时时间
     */
    protected int resultTimeout;
    /**
     * 是否允许在非本机执行query
     */
    protected boolean allowExecuteOversea = true;
    /**
     * app文件
     */
    protected String appRuleFile;
    /**
     * 机器拓扑
     */
    protected String machineTopology;
    /**
     * 表文件
     */
    protected String schemaFile;
    // 事务
    protected boolean transactional = true;
    /**
     * 如果本机提供服务，那么应该会对外提供port
     */
    protected int port = 100020;
    protected Integer monitorServerPort = null;
    /**
     * 内部使用，性能测试的时候，屏蔽真正渎取和写入。
     */
    protected boolean isPerfTest = false;
    protected boolean sendJingWei = false;
    // 项目根目录
    private String root_dir;
    // 精卫的topic
    // 序列化/编码
    private String codec_name;
    // 数据目录
    private String repo_dir = ".";
    // 提交时同步刷到文件系统
    private boolean commit_sync = true;
    // 用作缓存的内存的百分比
    private int cache_percent = 60;
    // 执行引擎线程数
    private int execThreadCount = 50;
    private int maxThreadCount = execThreadCount;
    private int keepAliveTime = 5000;

    // ha datanode columnName
    private String group_name;
    // ha datanode query columnName
    private String node_name;
    // ha leader选举优先级
    private int priority = 1;
    // group中的所有节点
    private String group_nodes;
    // rpc 连接权限
    private String userName;
    private String password;
    // 持久化策略
    private String[] durability;
    private int transactionDelayTime;
    private int resultDelayTime;
    // 走mysql
    private boolean useMysql;
    private String mysqlDB;
    private String metaTopic;
    // 精卫映射表
    private String jingweiMap;

    public BDBConfig() {
    }

    public BDBConfig(String file) {
        InputStream in = null;
        load(in);
    }

    public BDBConfig(InputStream in) {
        load(in);
    }

    public String getJingweiMap() {
        return jingweiMap;
    }

    public void setJingweiMap(String jingweiMap) {
        this.jingweiMap = jingweiMap;
    }

    public String[] getDurability() {
        return durability;
    }

    public String getGroupName() {
        return group_name;
    }

    public String getNodeName() {
        return node_name;
    }

    public int getPriority() {
        return priority;
    }

    public String getGroupNodes() {
        return group_nodes;
    }

    public String[] getGroupNodesArray() {
        return group_nodes.split(",");
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getBlockingQueueCapacity() {
        return max_concurrent_request;
    }

    public void setBlockingQueueCapacity(int blockingQueueCapacity) {
        this.max_concurrent_request = blockingQueueCapacity;
    }

    public int getKeepAliveTime() {
        return keepAliveTime;
    }

    public void setKeepAliveTime(int keepAliveTime) {
        this.keepAliveTime = keepAliveTime;
    }

    public int getMaximumPoolSize() {
        return maxThreadCount;
    }

    public void setMaximumPoolSize(int maximumPoolSize) {
        this.maxThreadCount = maximumPoolSize;
    }

    public int getCorePoolSize() {
        return execThreadCount;
    }

    public void setCorePoolSize(int corePoolSize) {
        this.execThreadCount = corePoolSize;
    }

    public int getCachePercent() {
        return cache_percent;
    }

    private void load(InputStream inputStream) {
        Properties properties = new Properties();
        try {
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.repo_dir = properties.getProperty("repo_dir");
        this.codec_name = properties.getProperty("codec_name", "avro");
        this.transactional = Boolean.parseBoolean(properties.getProperty("transactional", "true"));
        this.commit_sync = Boolean.parseBoolean(properties.getProperty("commit_sync", "true"));
        this.txnTimeout = Integer.parseInt(properties.getProperty("txn_timeout", "1000"));
        this.resultTimeout = Integer.parseInt(properties.getProperty("result_timeout", "60"));
        this.max_concurrent_request = Integer.parseInt(properties.getProperty("max_concurrent_request", "100000"));
        this.port = Integer.parseInt(properties.getProperty("port", "6033"));
        this.root_dir = properties.getProperty("root_dir");
        this.cache_percent = Integer.parseInt(properties.getProperty("cache_percent", "60"));
        this.machineTopology = properties.getProperty("machine_topology");
        this.schemaFile = properties.getProperty("schema_file");
        this.app = properties.getProperty("applicationConfigurationFile");
        this.appRuleFile = properties.getProperty("app_rule_file");
        String tempMonitorServerPort = properties.getProperty("monitor_server_port");
        if (tempMonitorServerPort != null) {
            this.monitorServerPort = Integer.valueOf(tempMonitorServerPort);
        }
        String allow_get_group_name = properties.getProperty("allow_get_group_name");
        if (allow_get_group_name == null) {
            this.allowExecuteOversea = Boolean.parseBoolean(properties.getProperty("allow_get_group_name", "true"));
        } else {
            this.allowExecuteOversea = Boolean.parseBoolean(allow_get_group_name);
        }
        this.transactionDelayTime = Integer.valueOf(properties.getProperty("transaction_delay_time", "100000"));
        this.resultDelayTime = Integer.valueOf(properties.getProperty("result_delay_time", "100000"));
        this.userName = properties.getProperty("user_name");
        this.password = properties.getProperty("password");

        this.maxThreadCount = Integer.parseInt(properties.getProperty("max_thread_count", "50"));
        this.execThreadCount = maxThreadCount;
        if (durability != null) {
            System.out.println("durability:" + Arrays.asList(durability));
        } else {
            System.out.println("durability:null");
        }
        this.default_txn_isolation = properties.getProperty("default_txn_degree", "READ_COMMITTED");

        this.sendJingWei = Boolean.parseBoolean(properties.getProperty("sendJingWei", "false"));
        this.metaTopic = properties.getProperty("metaTopic");
        this.jingweiMap = properties.getProperty("jingweiMap");

        this.useMysql = Boolean.parseBoolean(properties.getProperty("use_mysql"));

        this.isPerfTest = Boolean.parseBoolean(properties.getProperty("isPerfTest", "false"));

        this.mysqlDB = properties.getProperty("mysql_DB");

        this.cleaner_thread_count = Integer.parseInt(properties.getProperty("cleaner_thread_count", "1"));
        this.cleaner_batch_file_count = Integer.parseInt(properties.getProperty("cleaner_batch_file_count", "1"));
        this.auto_clean = Boolean.parseBoolean(properties.getProperty("auto_clean", "true"));
        this.cleanerLazyMigration = properties.getProperty("cleanerLazyMigration", "false");
        this.cleaner_min_utilization = properties.getProperty("cleaner_min_utilization", "50");

    }

    public String getRootDir() {
        return root_dir;
    }

    public String getSchemaDir() {
        return root_dir + "/conf/schemas";
    }

    public void setSchemaDir(String dir) {
        this.root_dir = dir;
    }

    public boolean isCommitSync() {
        return commit_sync;
    }

    public void setCommitSync(boolean commit_sync) {
        this.commit_sync = commit_sync;
    }

    public String getCodecName() {
        return codec_name;
    }

    public void setCodecName(String codecName) {
        this.codec_name = codecName;
    }

    public String getRepoDir() {
        return repo_dir;
    }

    public void setRepoDir(String repo_dir) {
        this.repo_dir = repo_dir;
    }

    public String getSchema_file() {
        return schemaFile;
    }

    public void setSchema_file(String schema_file) {
        this.schemaFile = schema_file;
    }

    public String getMachine_topology() {
        return machineTopology;
    }

    public void setMachine_topology(String machine_topology) {
        this.machineTopology = machine_topology;
    }

    public String getApp_rule_file() {
        return appRuleFile;
    }

    public void setApp_rule_file(String app_rule_file) {
        this.appRuleFile = app_rule_file;
    }

    public boolean isAllow_get_group_name() {
        return allowExecuteOversea;
    }

    public void setAllow_get_group_name(boolean allow_get_group_name) {
        this.allowExecuteOversea = allow_get_group_name;
    }

    public boolean isUseMysql() {
        return useMysql;
    }

    public void setUseMysql(boolean useMysql) {
        this.useMysql = useMysql;
    }

    public String getMysqlDB() {
        return mysqlDB;
    }

    public void setMysqlDB(String mysqlDB) {
        this.mysqlDB = mysqlDB;
    }

    public String getMetaTopic() {
        return metaTopic;
    }

    public String getCleanerLazyMigration() {
        return cleanerLazyMigration;
    }

    public void setCleanerLazyMigration(String cleanerLazyMigration) {
        this.cleanerLazyMigration = cleanerLazyMigration;
    }

    public String getCleaner_min_utilization() {
        return cleaner_min_utilization;
    }

    public void setCleaner_min_utilization(String cleaner_min_utilization) {
        this.cleaner_min_utilization = cleaner_min_utilization;
    }

    public int getCleanerThreadCount() {
        return cleaner_thread_count;
    }

    public int getCleanerBatchFileCount() {
        return cleaner_batch_file_count;
    }

    public boolean getAutoClean() {
        return auto_clean;
    }

    public String getDefaultTnxIsolation() {
        return default_txn_isolation;
    }

    public void setDefaultTxnIsolation(String isolation) {
        this.default_txn_isolation = isolation;
    }

    public int getTxnTimeout() {
        return txnTimeout;
    }

    public void setTxnTimeout(int txnTimeout) {
        this.txnTimeout = txnTimeout;
    }

    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }

    public int getResultTimeout() {
        return resultTimeout;
    }

    public void setResultTimeout(int resultTimeout) {
        this.resultTimeout = resultTimeout;
    }

    public Integer getMonitorServerPort() {
        return monitorServerPort;
    }

    public void setMonitorServerPort(Integer monitorServerPort) {
        this.monitorServerPort = monitorServerPort;
    }

    public boolean isAllowExecuteOversea() {
        return allowExecuteOversea;
    }

    public void setAllowExecuteOversea(boolean allowExecuteOversea) {
        this.allowExecuteOversea = allowExecuteOversea;
    }

    public String getAppRuleFile() {
        return appRuleFile;
    }

    public void setAppRuleFile(String appRuleFile) {
        this.appRuleFile = appRuleFile;
    }

    public String getMachineTopology() {
        return machineTopology;
    }

    public void setMachineTopology(String machineTopology) {
        this.machineTopology = machineTopology;
    }

    public String getSchemaFile() {
        return schemaFile;
    }

    public void setSchemaFile(String schemaFile) {
        this.schemaFile = schemaFile;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public boolean isTransactional() {
        return transactional;
    }

    public void setTransactional(boolean transactional) {
        this.transactional = transactional;
    }

    public boolean isSendJingWei() {
        return sendJingWei;
    }

    public void setSendJingWei(boolean sendJingWei) {
        this.sendJingWei = sendJingWei;
    }

    public int getMaxConcurrentRequest() {
        return max_concurrent_request;
    }

    public void getMaxConcurrentRequest(int max_concurrent_request) {
        this.max_concurrent_request = max_concurrent_request;
    }

    public boolean isPerfTest() {
        return isPerfTest;
    }

    public void setPerfTest(boolean isPerfTest) {
        this.isPerfTest = isPerfTest;
    }

}
