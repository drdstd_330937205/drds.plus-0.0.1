package drds.plus.repository.berkeley.spi;

import drds.plus.executor.ExecuteContext;
import drds.plus.executor.cursor.cursor.ISortingCursor;
import drds.plus.executor.cursor.cursor_metadata.CursorMetaData;
import drds.plus.executor.repository.Repository;
import drds.plus.executor.table.ITemporaryTable;
import drds.plus.sql_process.abstract_syntax_tree.configuration.TableMetaData;

public class TemporaryTable extends Table implements ITemporaryTable {

    /**
     * 临时表返回给前段的meta
     */
    CursorMetaData cursorMetaData = null;

    public TemporaryTable(TableMetaData tableMetaData, Repository repository) {
        super(tableMetaData, repository);
    }

    public CursorMetaData getCursorMetaData() {
        return this.cursorMetaData;
    }

    public void setCursorMetaData(CursorMetaData cursorMetaData) {
        this.cursorMetaData = cursorMetaData;
    }

    public ISortingCursor getCursor(ExecuteContext executeContext) throws RuntimeException {
        return super.getCursor(executeContext, this.getTableMetaData().getTableName(), this.getTableMetaData().getPrimaryKeyIndexMetaData());
    }

}
