package drds.plus.repository.berkeley.spi;

import drds.plus.executor.record_codec.RecordCodec;
import lombok.Getter;
import lombok.Setter;

class KeyValueRecordCodec {
    @Setter
    @Getter
    RecordCodec keyRecordCodec;
    @Setter
    @Getter
    RecordCodec valueRecordCodec;
}
