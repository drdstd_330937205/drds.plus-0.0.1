package drds.plus.repository.berkeley.spi;

import drds.plus.executor.repository.IRepositoryFactory;
import drds.plus.executor.repository.Repository;
import drds.plus.util.GeneralUtil;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

@Slf4j

public class RepositoryFactory implements IRepositoryFactory {
    static {
        log.info("存储初始化berkeley数据库介质..." + RepositoryFactory.class.getName());
    }

    public Repository buildRepository(Map repoProperties, Map connectionProperties) {
        String repoConfigFile = GeneralUtil.getExtraCmdString(repoProperties, BDBConfig.BDB_REPO_CONFIG_FILE_PATH);
        BDBConfig bdbConfig = null;
        if (repoConfigFile == null) {
            bdbConfig = new BDBConfig();
            log.warn("bdb repository transactionConfig file is not assigned, use default transactionConfig");
        } else {
            try {
                bdbConfig = new BDBConfig(repoConfigFile);
            } catch (Exception e) {
                throw new RuntimeException("bdb repository init error", e);
            }
        }
        RepositoryImpl repository = new RepositoryImpl(connectionProperties);
        repository.setBdbConfig(bdbConfig);
        repository.init();
        return repository;
    }
}
