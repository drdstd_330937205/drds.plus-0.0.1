package drds.plus.sequence;

import drds.plus.common.lifecycle.Lifecycle;

public interface SequenceDao extends Lifecycle {

    /**
     * 取得下一个可用的序列区间
     *
     * @param name 序列名称
     * @return 返回下一个可用的序列区间
     */
    Range nextRange(String name) throws SequenceException;

    int getStep();
}
