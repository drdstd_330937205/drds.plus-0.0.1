package drds.plus.sequence;

/**
 * 序列
 */
public interface Sequence {

    /**
     * 取得序列下一个值
     *
     * @return 返回序列下一个值
     */
    long nextValue() throws SequenceException;

    /**
     * 返回size大小后的值，比如针对batch拿到size大小的值，自己内存中顺序分配
     */
    long nextValue(int size) throws SequenceException;
}
