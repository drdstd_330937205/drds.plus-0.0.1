package drds.plus.sequence.impl;

import drds.plus.sequence.Sequence;
import drds.plus.sequence.SequenceException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

public class DefaultSequcenceTest {


    private Sequence sequence;

    @Before
    public void setUp() throws Exception {

    }

    @Ignore
    @Test
    public void test_nextValue() throws SequenceException {
        Set<Long> set = new HashSet<Long>();
        for (int i = 0; i < 1000; i++) {
            Long id = sequence.nextValue();
            System.out.println(id);
            boolean b = set.contains(id);
            Assert.assertFalse(b);
            set.add(id);
        }
    }
}
