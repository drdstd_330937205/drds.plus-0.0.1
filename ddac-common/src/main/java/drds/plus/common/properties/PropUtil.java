package drds.plus.common.properties;

import java.util.StringTokenizer;
import java.util.concurrent.TimeUnit;

/**
 * Convenience methods for handling JE properties.
 */
public class PropUtil {


    /**
     * Parses a String duration property (time + optional unit) and returns the
     * value in millis.
     *
     * @throws IllegalArgumentException if the duration string is illegal. Thrown
     *                                  via the Enviornment ctor and
     *                                  setMutableConfig, and likewise for a
     *                                  ReplicatedEnvironment.
     */
    public static int parseDuration(final String property) {
        StringTokenizer tokens = new StringTokenizer(property.toUpperCase(java.util.Locale.ENGLISH), " \t");
        if (!tokens.hasMoreTokens()) {
            throw new IllegalArgumentException("Duration argument is empty");
        }
        final long time;
        try {
            time = Long.parseLong(tokens.nextToken());
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Duration argument does not start with a long integer: " + property);
        }
        /* Convert time from specified unit to millis. */
        long millis;
        if (tokens.hasMoreTokens()) {
            final String unitName = tokens.nextToken();
            if (tokens.hasMoreTokens()) {
                throw new IllegalArgumentException("Duration argument has extra characters execute_plan_optimizer unit: " + property);
            }
            try {
                final TimeUnit unit = TimeUnit.valueOf(unitName);
                millis = TimeUnit.MILLISECONDS.convert(time, unit);
            } catch (IllegalArgumentException e) {
                try {
                    final IEEETimeUnit unit = IEEETimeUnit.valueOf(unitName);
                    millis = unit.toMillis(time);
                } catch (IllegalArgumentException e2) {
                    throw new IllegalArgumentException("Duration argument has unknown unit id: " + property);
                }
            }
        } else {
            /* Default unit is micros. */
            millis = TimeUnit.MILLISECONDS.convert(time, TimeUnit.MICROSECONDS);
        }
        /* If input val is positive, return at least one. */
        if (time > 0 && millis == 0) {
            return 1;
        }
        if (millis > Integer.MAX_VALUE) {
            throw new IllegalArgumentException("Duration argument may not be greater than " + "Integer.MAX_VALUE milliseconds: " + property);
        }
        return (int) millis;
    }

    /**
     * Formats a String duration property (time + optional unit). value in millis.
     */
    public static String formatDuration(long time, TimeUnit unit) {
        return String.valueOf(time) + ' ' + unit.name();
    }

    /**
     * Support for conversion of IEEE time units. Although names are defined in
     * uppercase, we uppercase the input string before calling IEEETimeUnit.valueOf,
     * in order to support input names in both upper and lower case.
     */
    private enum IEEETimeUnit {

        /* Nanoseconds */
        NS() {
            long toMillis(long val) {
                return millisUnit.convert(val, TimeUnit.NANOSECONDS);
            }
        },

        /* Microseconds */
        US() {
            long toMillis(long val) {
                return millisUnit.convert(val, TimeUnit.MICROSECONDS);
            }
        },

        /* Milliseconds */
        MS() {
            long toMillis(long val) {
                return millisUnit.convert(val, TimeUnit.MILLISECONDS);
            }
        },

        /* Seconds */
        S() {
            long toMillis(long val) {
                return millisUnit.convert(val, TimeUnit.SECONDS);
            }
        },

        /* Minutes */
        MIN() {
            long toMillis(long val) {
                return val * 60000;
            }
        },

        /* Hours */
        H() {
            long toMillis(long val) {
                return val * 3600000;
            }
        };

        private static final TimeUnit millisUnit = TimeUnit.MILLISECONDS;

        abstract long toMillis(long val);
    }
}
