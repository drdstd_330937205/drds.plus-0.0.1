package drds.plus.common.properties;

/**
 * A configuration parameter with an string value.
 */
public class StringConfigParam extends ConfigParam {

    public StringConfigParam(String configName, String defaultValue, boolean mutable) {
        /* defaultValue must not be null. */
        super(configName, defaultValue, mutable);

    }

    public void validateValue(String value) throws IllegalArgumentException {
        // do nothing
    }
}
