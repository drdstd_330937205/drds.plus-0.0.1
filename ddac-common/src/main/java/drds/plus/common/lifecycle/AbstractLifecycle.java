package drds.plus.common.lifecycle;


import ch.qos.logback.WithLog;


public abstract class AbstractLifecycle implements Lifecycle, WithLog {

    protected final Object lock = new Object();
    protected volatile boolean isInited = false;

    public void init() {
        synchronized (lock) {
            if (isInited()) {
                return;
            }

            try {
                doInit();
                isInited = true;
            } catch (Exception e) {
                log().error("", e);
                // 出现异常调用destory方法，释放
                try {
                    doDestroy();
                } catch (Exception e1) {
                    // ignore
                }
                throw new RuntimeException(e);
            }
        }
    }

    public void destroy() {
        synchronized (lock) {
            if (!isInited()) {
                return;
            }

            doDestroy();
            isInited = false;
        }
    }

    public boolean isInited() {
        return isInited;
    }

    protected void doInit() {
    }

    protected void doDestroy() {
    }

}
