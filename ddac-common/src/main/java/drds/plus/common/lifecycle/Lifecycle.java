package drds.plus.common.lifecycle;


public interface Lifecycle {

    /**
     * 正常启动
     */
    void init();

    /**
     * 正常停止
     */
    void destroy();

    /**
     * 是否存储运行运行状态
     */
    boolean isInited();

}
