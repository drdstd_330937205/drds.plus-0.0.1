package drds.plus.common;


public class Constants {

    /**
     * 初次获取diamond配置超时时间
     */
    public static final long DIAMOND_GET_DATA_TIMEOUT = 10 * 1000;

    /**
     * default cache expire time, 30000ms
     */
    public static final long DEFAULT_TABLE_META_EXPIRE_TIME = 300 * 1000;

    public static final int DEFAULT_OPTIMIZER_EXPIRE_TIME = 300 * 1000;

    public static final int DEFAULT_OPTIMIZER_CACHE_SIZE = 1000;


    public static final int DEFAULT_CONCURRENT_THREAD_SIZE = 8;

    public static final int MAX_CONCURRENT_THREAD_SIZE = 256;

}
