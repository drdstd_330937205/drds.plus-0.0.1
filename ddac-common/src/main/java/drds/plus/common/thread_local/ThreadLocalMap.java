package drds.plus.common.thread_local;

import java.util.Map;

/**
 * 为了兼容老的tddl3,特别保留了client这个名字
 */
public class ThreadLocalMap {

    protected final static ThreadLocal<Map<Object, Object>> threadContext = new MapThreadLocal();

    public static void put(Object key, Object value) {
        getContextMap().put(key, value);
    }

    public static Object remove(Object key) {
        return getContextMap().remove(key);
    }

    public static Object get(Object key) {
        return getContextMap().get(key);
    }

    public static boolean containsKey(Object key) {
        return getContextMap().containsKey(key);
    }

    /**
     * 取得thread context Map的实例。
     *
     * @return thread context Map的实例
     */
    public static Map<Object, Object> getContextMap() {
        return threadContext.get();
    }

    /**
     * 设置thread context Map的实例。
     */
    public static void setContextMap(Map<Object, Object> context) {
        threadContext.set(context);
    }

    /**
     * 清理线程所有被hold住的对象。以便重用！
     */

    public static void reset() {
        getContextMap().clear();
    }

}
