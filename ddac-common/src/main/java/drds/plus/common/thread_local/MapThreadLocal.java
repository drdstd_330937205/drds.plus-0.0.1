package drds.plus.common.thread_local;

import java.util.HashMap;
import java.util.Map;

class MapThreadLocal extends ThreadLocal<Map<Object, Object>> {

    protected Map<Object, Object> initialValue() {
        return new HashMap<Object, Object>() {

            private static final long serialVersionUID = 3637958959138295593L;

            public Object put(Object key, Object value) {
                return super.put(key, value);
            }
        };
    }
}
