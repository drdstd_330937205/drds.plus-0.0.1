package drds.plus.common.jdbc;

import java.math.BigDecimal;
import java.sql.*;

public enum SetParameterMethod {
    setNull1, //
    setNullWithTypeName, //
    setInt, //
    setLong, //
    setFloat, //
    setDouble, //
    setBigDecimal, //
    //
    setString, //
    setClob, //
    //
    setDate, setTime, //
    setTimestamp, //
    //
    setObject;//

    /**
     * args[0]: index args[1..n] 参数
     */
    public void setParameter(PreparedStatement preparedStatement, Object... args) throws SQLException {
        switch (this) {
            case setNull1:
                preparedStatement.setNull((Integer) args[0], Types.NULL);
                break;
            case setNullWithTypeName:
                preparedStatement.setNull((Integer) args[0], Types.NULL, (String) args[2]);
                break;
            //
            case setInt:
                preparedStatement.setInt((Integer) args[0], (Integer) args[1]);
                break;
            case setLong:
                preparedStatement.setLong((Integer) args[0], (Long) args[1]);
                break;
            case setFloat:
                preparedStatement.setFloat((Integer) args[0], (Float) args[1]);
                break;
            case setDouble:
                preparedStatement.setDouble((Integer) args[0], (Double) args[1]);
                break;
            case setBigDecimal:
                preparedStatement.setBigDecimal((Integer) args[0], (BigDecimal) args[1]);
                break;
            //
            case setString:
                preparedStatement.setString((Integer) args[0], (String) args[1]);
                break;
            case setClob:
                preparedStatement.setClob((Integer) args[0], (Clob) args[1]);
                break;
            //
            case setDate:
                preparedStatement.setDate((Integer) args[0], (Date) args[1]);
                break;
            case setTime:
                preparedStatement.setTime((Integer) args[0], (Time) args[1]);
                break;
            case setTimestamp:
                preparedStatement.setTimestamp((Integer) args[0], (Timestamp) args[1]);
                break;
            //
            case setObject:
                preparedStatement.setObject((Integer) args[0], args[1]);
                break;
            default:
                throw new IllegalArgumentException("Unhandled SetParameterMethod:" + this.name());
        }
    }
}
