package drds.plus.common.jdbc.transaction_policy;

/**
 * 非事务环境下自动提交,事务环境严格单机事务
 */
public class Strict implements ITransactionPolicy {

    public TransactionType getTransactionPolicyType(boolean isAutoCommit) {
        if (isAutoCommit) {
            return TransactionType.non_transaction_and_close_connection_when_commit;
        } else {
            return TransactionType.strict;
        }
    }

}
