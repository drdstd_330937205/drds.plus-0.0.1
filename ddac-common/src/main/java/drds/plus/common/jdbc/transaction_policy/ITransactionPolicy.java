package drds.plus.common.jdbc.transaction_policy;

public interface ITransactionPolicy {

    Strict strict = new Strict();//drds推荐策略
    StrictWriteWithNonTransactionCrossDatabaseRead strict_write_with_non_transaction_cross_database_read = new StrictWriteWithNonTransactionCrossDatabaseRead();
    CobarStyle cobar_style = new CobarStyle();

    TransactionType getTransactionPolicyType(boolean isAutoCommit);

}
