package drds.plus.common.jdbc.transaction_policy;

public enum TransactionType {
    /**
     * 提交时关闭连接
     */
    non_transaction_and_close_connection_when_commit,
    /**
     * 支持跨库事务,针对每建立的连接都开启事务 connection.setAutoCommit(false);
     */
    cobar_style,

    /**
     * 建立连接则开启事务,只会保留一个连接.禁止任跨库操作的事物
     */
    strict,
    /**
     * 允许跨库的读
     */
    strict_write_with_non_transaction_cross_database_read,//strict_write_with_non_transaction_cross_database_read
}
