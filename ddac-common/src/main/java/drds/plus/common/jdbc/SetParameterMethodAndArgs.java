package drds.plus.common.jdbc;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

public class SetParameterMethodAndArgs {

    private SetParameterMethod setParameterMethod;
    /**
     * args[0]: parameterIndex args[1]: 参数值 args[2]: length
     */
    private Object[] args;

    public SetParameterMethodAndArgs() {
    }

    public SetParameterMethodAndArgs(SetParameterMethod setParameterMethod, Object[] args) {
        this.setParameterMethod = setParameterMethod;
        this.args = args;
    }

    public static void setParameters(PreparedStatement preparedStatement, Map<Integer, SetParameterMethodAndArgs> indexToSetParameterMethodAndArgsMap) throws SQLException {
        if (null != indexToSetParameterMethodAndArgsMap) {
            for (SetParameterMethodAndArgs setParameterMethodAndArgs : indexToSetParameterMethodAndArgsMap.values()) {
                setParameterMethodAndArgs.getSetParameterMethod().setParameter(preparedStatement, setParameterMethodAndArgs.getArgs());
            }
        }
    }

    public SetParameterMethod getSetParameterMethod() {
        return setParameterMethod;
    }

    public void setSetParameterMethod(SetParameterMethod setParameterMethod) {
        this.setParameterMethod = setParameterMethod;
    }

    public Object[] getArgs() {
        return args;
    }

    public void setArgs(Object[] args) {
        this.args = args;
    }

    public Object getValue() {
        return args[1];
    }

    public void setValue(Object value) {
        this.args[1] = value;
    }

}
