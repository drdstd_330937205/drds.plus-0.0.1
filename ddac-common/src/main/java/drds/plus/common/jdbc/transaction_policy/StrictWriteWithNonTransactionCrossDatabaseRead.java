package drds.plus.common.jdbc.transaction_policy;

/**
 * 非事务环境下自动提交,事务环境允许垮库读
 */
public class StrictWriteWithNonTransactionCrossDatabaseRead implements ITransactionPolicy {

    public TransactionType getTransactionPolicyType(boolean isAutoCommit) {
        if (isAutoCommit) {
            return TransactionType.non_transaction_and_close_connection_when_commit;
        } else {
            return TransactionType.strict_write_with_non_transaction_cross_database_read;
        }

    }

}
