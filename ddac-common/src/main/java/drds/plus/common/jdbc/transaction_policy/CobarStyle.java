package drds.plus.common.jdbc.transaction_policy;

/**
 * 非事务环境下自动提交,事务环境各库不区分
 */
public class CobarStyle implements ITransactionPolicy {

    public TransactionType getTransactionPolicyType(boolean isAutoCommit) {
        if (isAutoCommit) {

            return TransactionType.non_transaction_and_close_connection_when_commit;
        }

        return TransactionType.cobar_style;

    }
}
