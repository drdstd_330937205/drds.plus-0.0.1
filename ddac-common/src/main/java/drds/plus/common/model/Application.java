package drds.plus.common.model;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 整个系统的核心架构
 */
public class Application {
    @Setter
    @Getter
    private String id;
    @Setter
    @Getter
    private List<DataNode> dataNodeList = new ArrayList<DataNode>();
    @Setter
    @Getter
    private Map<String, String> properties = new HashMap();

    public DataNode getDataNode(String dataNodeId) {
        for (DataNode dataNode : dataNodeList) {
            if (dataNode.getId().equals(dataNodeId)) {
                return dataNode;
            }
        }
        return null;
    }


}
