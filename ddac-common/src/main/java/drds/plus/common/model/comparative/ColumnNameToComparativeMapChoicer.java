package drds.plus.common.model.comparative;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 参数筛选器
 */
public interface ColumnNameToComparativeMapChoicer {

    /**
     * 根据PartinationSet 获取列名和他对应值的map.
     *
     * @param colNameSet   指定的column set
     * @param argumentList PrepareStatement设置的参数
     * @return
     */
    Map<String, Comparative> getColumnNameToComparativeMap(Set<String> columnNameNameSet, List<Object> argumentList);

    /**
     * @param colName   指定的column id
     * @param arguments PrepareStatement设置的参数
     */
    Comparative getColumnComparative(String colName, List<Object> arguments);
}
