package drds.plus.common.model.hint;

import java.util.Map;

public interface RouteCondition {

    String getVirtualTableNamesString();

    void setVirtualTableNamesString(String virtualTableNamesString);

    RouteType getRouteType();

    void setRouteType(RouteType routeType);

    Map<String, Object> getExtraCmds();

}
