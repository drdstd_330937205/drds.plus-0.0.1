package drds.plus.common.model.comparative;

/**
 * <pre>
 * OR节点, 在实际的SQL中，实际上是类似
 * [Comparative]              [comparative]
 *          \                  /
 *            \               /
 *             [Or]
 *
 * 类似这样的节点出现
 * </pre>
 */
public class Or extends List {

    public Or(int function, Comparable<?> value) {
        super(function, value);
    }

    public Or() {
    }

    public Or(Comparative item) {
        super(item);
    }

    public Or(int capacity) {
        super(capacity);
    }

    protected String getRelation() {
        return " or ";
    }
}
