package drds.plus.common.model.hint;

import java.util.*;

public class DirectlyRouteCondition extends ExtraCmdRouteCondition implements RouteCondition {

    protected String dataNodeId; // 目标库的id
    protected Set<String> realTableNamesStringSet = new HashSet<String>(2); // 目标表的id

    public DirectlyRouteCondition() {

    }

    public DirectlyRouteCondition(String dataNodeId) {
        this.dataNodeId = dataNodeId;
    }

    public Set<String> getRealTableNamesStringSet() {
        return realTableNamesStringSet;
    }

    public void setRealTableNamesStringSet(Set<String> realTableNamesStringSet) {
        this.realTableNamesStringSet = realTableNamesStringSet;
    }

    public void addRealTableNamesString(String realTableNamesString) {
        realTableNamesStringSet.add(realTableNamesString);
    }

    public String getDataNodeId() {
        return dataNodeId;
    }

    public void setDataNodeId(String dataNodeId) {
        this.dataNodeId = dataNodeId;
    }

    /**
     * @return 两级的MAP , 顺序为：db index key , original table , targetTable
     */
    public Map<String, List<Map<String, String>>> getShardTableMap() {
        List<Map<String/* original table */, String/* targetTable */>> tableList = new ArrayList<Map<String, String>>(1);
        for (String targetTable : realTableNamesStringSet) {
            Map<String/* original table */, String/* target table */> table = new HashMap<String, String>(realTableNamesStringSet.size());
            table.put(virtualTableNamesString, targetTable);
            if (!table.isEmpty()) {
                tableList.add(table);
            }
        }

        Map<String/* key */, List<Map<String/* original table */, String/* targetTable */>>> shardTableMap = new HashMap<String, List<Map<String, String>>>(2);
        shardTableMap.put(dataNodeId, tableList);
        return shardTableMap;
    }
}
