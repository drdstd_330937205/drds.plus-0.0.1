package drds.plus.common.model.hint;

import java.util.HashMap;
import java.util.Map;

/**
 * 扩展参数
 */
public class ExtraCmdRouteCondition implements RouteCondition {

    protected String virtualTableNamesString;
    protected Map<String, Object> extraCmds = new HashMap<String, Object>();
    protected RouteType routeType = RouteType.flush_on_execute;

    public String getVirtualTableNamesString() {
        return virtualTableNamesString;
    }

    public void setVirtualTableNamesString(String virtualTableNamesString) {
        this.virtualTableNamesString = virtualTableNamesString;
    }

    public RouteType getRouteType() {
        return routeType;
    }

    public void setRouteType(RouteType routeType) {
        this.routeType = routeType;
    }

    public Map<String, Object> getExtraCmds() {
        return extraCmds;
    }

    public void setExtraCmds(Map<String, Object> extraCmds) {
        this.extraCmds = extraCmds;
    }

    public void putExtraCmd(String key, Object value) {
        this.extraCmds.put(key, value);
    }

}
