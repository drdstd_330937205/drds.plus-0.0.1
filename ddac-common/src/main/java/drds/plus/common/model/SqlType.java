package drds.plus.common.model;

public enum SqlType {


    SELECT(0),

    INSERT(1),

    UPDATE(2),

    DELETE(3),

    SELECT_FOR_UPDATE(4),

    REPLACE(5),


    SHOW(6),


    DUMP(7),

    DEBUG(8),

    EXPLAIN(9),

    PROCEDURE(10),

    DESC(11),
    /**
     * 获取上一个insert id
     */
    SELECT_LAST_INSERT_ID(12),

    SELECT_WITHOUT_TABLE(13),

    GET_SEQUENCE(14),


    SET(25);

    private int i;

    SqlType(int i) {
        this.i = i;
    }

    public static SqlType valueOf(int i) {
        for (SqlType t : values()) {
            if (t.value() == i) {
                return t;
            }
        }
        throw new IllegalArgumentException("Invalid SqlType:" + i);
    }

    public int value() {
        return this.i;
    }
}
