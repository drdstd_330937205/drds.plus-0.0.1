package drds.plus.common.model;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.List;

public interface RowValues {

    Object getObject(int index);

    void setObject(int index, Object value);

    Integer getInteger(int index);

    void setInteger(int index, Integer value);

    Long getLong(int index);

    void setLong(int index, Long value);

    List<Object> getValueList();


    String getString(int index);

    void setString(int index, String str);

    Boolean getBoolean(int index);

    void setBoolean(int index, Boolean bool);

    Short getShort(int index);

    void setShort(int index, Short shortval);

    Float getFloat(int index);

    void setFloat(int index, Float fl);

    Double getDouble(int index);

    void setDouble(int index, Double doub);


    BigDecimal getBigDecimal(int index);

    void setBigDecimal(int index, BigDecimal bigDecimal);

    Time getTime(int index);

    void setTime(int index, Time time);

    Date getDate(int index);

    void setDate(int index, Date date);

    Timestamp getTimestamp(int index);

    void setTimestamp(int index, Timestamp timestamp);
}
