package drds.plus.common.model;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class DataNode {
    @Setter
    @Getter
    private String applicationId;
    @Setter
    @Getter
    private String id;
    /**
     * 用于描述这组机器的类型
     */
    @Setter
    @Getter
    private RepositoryType repositoryType = RepositoryType.mysql;
    @Setter
    @Getter
    private Map<String, String> properties = new HashMap();
    @Setter
    @Getter
    private List<DatasourceWrapper> datasourceWrapperList = new ArrayList<DatasourceWrapper>();


}
