package drds.plus.common.model.hint;

public enum RouteType {
    /**
     * 连接关闭的时候清空
     */
    flush_on_close_connection,
    /**
     * 执行完成时就清空
     */
    flush_on_execute
}
