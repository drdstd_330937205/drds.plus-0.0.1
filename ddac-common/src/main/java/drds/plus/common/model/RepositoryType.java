package drds.plus.common.model;

public enum RepositoryType {
    bdb, mysql;

    public boolean isMysql() {
        return this == mysql;
    }


}
