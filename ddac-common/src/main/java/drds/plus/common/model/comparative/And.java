package drds.plus.common.model.comparative;

/**
 * <pre>
 * AND节点 ,在实际的SQL中，实际上是类似
 * [Comparative]              [comparative]
 *          \                  /
 *            \               /
 *             [ComparativeAnd]
 * 类似这样的节点出现
 * </pre>
 */
public class And extends List {

    public And(int function, Comparable<?> value) {
        super(function, value);
    }

    public And() {
    }

    public And(Comparative item) {
        super(item);
    }

    protected String getRelation() {
        return " and ";
    }

}
