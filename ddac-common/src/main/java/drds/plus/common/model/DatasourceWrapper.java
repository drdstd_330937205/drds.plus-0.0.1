package drds.plus.common.model;

import java.util.HashMap;
import java.util.Map;

/**
 * 类似tddl三层结构的atom概念，用于以后扩展第三方存储，目前扩展属性暂时使用properties代替
 */
public class DatasourceWrapper {

    private String name; // 类似于dbKey

    private Map<String, String> properties = new HashMap();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, String> getProperties() {
        return properties;
    }

    public void setProperties(Map<String, String> properties) {
        this.properties = properties;
    }

}
