package drds.plus.common.utils.convertor;

import drds.tools.ShouldNeverHappenException;

import java.util.Date;

/**
 * Long <-> Date对象之间的转换
 */
public class LongAndDateConvertor {

    public static class LongToDateConvertor extends AbastactConvertor {

        public Object convert(Object src, Class destClass) {
            if (src instanceof Long) { // 必须是Date类型
                Long time = (Long) src;
                // java.thread_local.Date
                if (destClass.equals(java.util.Date.class)) {
                    return new java.util.Date(time);
                }

                // java.sql_process.Date
                if (destClass.equals(java.sql.Date.class)) {
                    return new java.sql.Date(time);
                }

                // java.sql_process.Time
                if (destClass.equals(java.sql.Time.class)) {
                    return new java.sql.Time(time);
                }

                // java.sql_process.Timestamp
                if (destClass.equals(java.sql.Timestamp.class)) {
                    return new java.sql.Timestamp(time);
                }

            }
            throw new ShouldNeverHappenException("Unsupported convert: [" + src + "," + destClass.getName() + "]");
        }
    }

    public static class DateToLongConvertor extends AbastactConvertor {

        public Object convert(Object src, Class destClass) {
            if (src instanceof Date) { // 必须是Date类型
                Date date = (Date) src;
                return date.getTime();
            }

            throw new ShouldNeverHappenException("Unsupported convert: [" + src + "," + destClass.getName() + "]");
        }
    }

}
