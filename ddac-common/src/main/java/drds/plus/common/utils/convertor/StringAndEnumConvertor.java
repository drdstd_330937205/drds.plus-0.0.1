package drds.plus.common.utils.convertor;

import drds.tools.ShouldNeverHappenException;

/**
 * string <-> Enum 之间的转化
 */
public class StringAndEnumConvertor {

    /**
     * string -> Enum 对象的转化
     */
    public static class StringToEnum extends AbastactConvertor {

        public Object convert(Object src, Class destClass) {
            if (src instanceof String && destClass.isEnum()) {
                return Enum.valueOf(destClass, (String) src);
            }

            throw new ShouldNeverHappenException("Unsupported convert: [" + src.getClass() + "," + destClass.getName() + "]");

        }
    }

    /**
     * Enum -> String 对象的转化
     */
    public static class EnumToString extends AbastactConvertor {

        public Object convert(Object src, Class destClass) {
            if (src.getClass().isEnum() && destClass == String.class) {
                return ((Enum) src).name(); // 返回定义的enum id
            }

            throw new ShouldNeverHappenException("Unsupported convert: [" + src.getClass() + "," + destClass.getName() + "]");
        }
    }
}
