package drds.plus.common.utils.convertor;


import drds.tools.ShouldNeverHappenException;

public class AbastactConvertor implements Convertor {

    public Object convert(Object src, Class destClass) {
        throw new ShouldNeverHappenException("unSupport!");
    }

    public Object convertCollection(Object src, Class destClass, Class... componentClasses) {
        throw new ShouldNeverHappenException("unSupport!");
    }

}
