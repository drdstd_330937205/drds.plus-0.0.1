package drds.plus.common.utils.thread;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.AbstractExecutorService;
import java.util.concurrent.TimeUnit;

public class RightNowRunRunnableExecutorService extends AbstractExecutorService {
    public void execute(Runnable runnable) {
        runnable.run();
    }

    public void shutdown() {
    }

    public List<Runnable> shutdownNow() {
        return new ArrayList<Runnable>();
    }

    public boolean isShutdown() {
        return false;
    }

    public boolean isTerminated() {
        return false;
    }

    public boolean awaitTermination(long timeout, TimeUnit unit) throws InterruptedException {
        return false;
    }

}
