package drds.plus.common.utils.convertor;

/**
 * 数据类型转化
 */
public interface Convertor {

    Object convert(Object src, Class destClass);

    /**
     * 支持多级collection映射，需指定多级的componentClass
     */
    Object convertCollection(Object src, Class destClass, Class... componentClasses);

}
