package drds.plus.common.utils;

/**
 * <pre>
 * 一个做引用切换的Holder，使用时先set、后get
 * 用于运行时配置信息需要动态修改，实时生效的场景。
 * 所谓运行时配置信息，是指运行时被实时读取，并且影响运行时行为的配置信息。
 * 运行时配置信息动态修改时，协助使用者完成copyonwrite实现：
 * 。。。
 *
 * </pre>
 *
 * @param <Weight> 包含运行时配置信息的对象的类型
 */
public class WeightHolder<Weight> {

    private volatile Weight weight;

    /**
     * @return 上一次设入的包含运行时配置信息的对象。
     */
    public Weight get() {
        return weight;
    }

    /**
     * @param weight 包含运行时配置信息的对象。
     */
    public void set(Weight weight) {
        this.weight = weight;
    }
}
