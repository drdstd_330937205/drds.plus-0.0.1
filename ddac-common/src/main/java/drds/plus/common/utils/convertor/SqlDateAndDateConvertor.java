package drds.plus.common.utils.convertor;

import drds.tools.ShouldNeverHappenException;

import java.util.Date;

/**
 * Date <-> SqlDate 之间的转化
 */
public class SqlDateAndDateConvertor {

    public static class SqlDateToDateConvertor extends AbastactConvertor {

        public Object convert(Object src, Class destClass) {
            if (Date.class != destClass) {
                throw new ShouldNeverHappenException("Unsupported convert: [" + src + "," + destClass.getName() + "]");
            }

            if (src instanceof java.sql.Date) {
                return new Date(((java.sql.Date) src).getTime());
            }
            if (src instanceof java.sql.Timestamp) {
                return new Date(((java.sql.Timestamp) src).getTime());
            }
            if (src instanceof java.sql.Time) {
                return new Date(((java.sql.Time) src).getTime());
            }
            throw new ShouldNeverHappenException("Unsupported convert: [" + src + "," + destClass.getName() + "]");
        }
    }

    public static class DateToSqlDateConvertor extends AbastactConvertor {

        public Object convert(Object src, Class destClass) {
            if (src instanceof Date) { // 必须是Date类型
                Date date = (Date) src;
                long value = date.getTime();
                // java.sql_process.Date
                if (destClass.equals(java.sql.Date.class)) {
                    return new java.sql.Date(value);
                }

                // java.sql_process.Time
                if (destClass.equals(java.sql.Time.class)) {
                    return new java.sql.Time(value);
                }

                // java.sql_process.Timestamp
                if (destClass.equals(java.sql.Timestamp.class)) {
                    return new java.sql.Timestamp(value);
                }
            }

            throw new ShouldNeverHappenException("Unsupported convert: [" + src + "," + destClass.getName() + "]");
        }
    }
}
