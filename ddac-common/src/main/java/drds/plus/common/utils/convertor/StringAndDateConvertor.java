package drds.plus.common.utils.convertor;

import drds.plus.common.utils.DateUtils;
import drds.tools.ShouldNeverHappenException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * string <-> Date/Calendar 之间的转化
 */
public class StringAndDateConvertor {

    public static final String TIMESTAMP_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_FORMAT = "yyyy-MM-dd";
    public static final String TIME_FORMAT = "HH:mm:ss";

    /**
     * string -> Date
     */
    public static class StringToDate extends AbastactConvertor {

        public Object convert(Object src, Class destClass) {
            if (src instanceof String) { // 必须是字符串
                return DateUtils.str_to_time((String) src);
            }

            throw new ShouldNeverHappenException("Unsupported convert: [" + src + "," + destClass.getName() + "]");
        }
    }

    /**
     * string -> sql_process Date
     */
    public static class StringToSqlDate extends AbastactConvertor {

        public Object convert(Object src, Class destClass) {
            if (src instanceof String) { // 必须是字符串
                Date date = DateUtils.str_to_time((String) src);
                if (date == null) {
                    SimpleDateFormat format = new SimpleDateFormat(TIME_FORMAT);
                    try {
                        date = format.parse((String) src);
                    } catch (ParseException e) {
                        throw new ShouldNeverHappenException(e);
                    }
                }
                return ConvertorHelper.dateToSql.convert(date, destClass);
            }

            throw new ShouldNeverHappenException("Unsupported convert: [" + src + "," + destClass.getName() + "]");
        }
    }

    /**
     * string -> sql_process Date
     */
    public static class StringToSqlTime extends AbastactConvertor {

        public Object convert(Object src, Class destClass) {
            if (src instanceof String) { // 必须是字符串
                SimpleDateFormat format = new SimpleDateFormat(TIME_FORMAT);
                Date date = null;
                try {
                    date = format.parse((String) src);
                } catch (ParseException e) {
                    date = DateUtils.str_to_time((String) src);
                }
                return ConvertorHelper.dateToSql.convert(date, destClass);
            }

            throw new ShouldNeverHappenException("Unsupported convert: [" + src + "," + destClass.getName() + "]");
        }
    }

    /**
     * string-> Calendar
     */
    public static class StringToCalendar extends StringToDate {

        public Object convert(Object src, Class destClass) {
            if (src instanceof String) { // 必须是字符串
                Date dest = (Date) super.convert(src, Date.class);
                Calendar result = new GregorianCalendar();
                result.setTime(dest);
                return result;
            }

            throw new ShouldNeverHappenException("Unsupported convert: [" + src + "," + destClass.getName() + "]");
        }
    }

    /**
     * Date -> string(格式为："2010-10-01")
     */
    public static class SqlDateToString extends AbastactConvertor {

        public Object convert(Object src, Class destClass) {
            if (src instanceof Date) { // 必须是Date对象
                return new SimpleDateFormat(DATE_FORMAT).format((Date) src);
            }

            throw new ShouldNeverHappenException("Unsupported convert: [" + src + "," + destClass.getName() + "]");
        }

    }

    /**
     * Date -> string(格式为："00:00:00")
     */
    public static class SqlTimeToString extends AbastactConvertor {

        public Object convert(Object src, Class destClass) {
            if (src instanceof Date) { // 必须是Date对象
                return new SimpleDateFormat(TIME_FORMAT).format((Date) src);
            }

            throw new ShouldNeverHappenException("Unsupported convert: [" + src + "," + destClass.getName() + "]");
        }

    }

    /**
     * Date -> string(格式为："2010-10-01 00:00:00")
     */
    public static class SqlTimestampToString extends AbastactConvertor {

        public Object convert(Object src, Class destClass) {
            if (src instanceof Date) { // 必须是Date对象
                return new SimpleDateFormat(TIMESTAMP_FORMAT).format((Date) src);
            }

            throw new ShouldNeverHappenException("Unsupported convert: [" + src + "," + destClass.getName() + "]");
        }

    }

    /**
     * Calendar -> string(格式为："2010-10-01")
     */
    public static class CalendarToString extends AbastactConvertor {

        public Object convert(Object src, Class destClass) {
            if (src instanceof Calendar) { // 必须是Date对象
                return new SimpleDateFormat(TIMESTAMP_FORMAT).format(((Calendar) src).getTime());
            }

            throw new ShouldNeverHappenException("Unsupported convert: [" + src + "," + destClass.getName() + "]");
        }

    }

}
