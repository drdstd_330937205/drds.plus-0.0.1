package drds.plus.util;


import java.util.Map;

/**
 * 公共方便方法
 */
public class GeneralUtil {

    public static StackTraceElement split = new StackTraceElement("------- one sql_process exceptions-----", "", "", 0);


    public static String getTab(int count) {
        StringBuffer tab = new StringBuffer();
        for (int i = 0; i < count; i++)
            tab.append("    ");
        return tab.toString();
    }

    public static String getExtraCmdString(Map<String, Object> extraCmd, String key) {
        if (extraCmd == null) {
            return null;
        }

        if (key == null) {
            return null;
        }
        Object obj = extraCmd.get(key);
        if (obj != null) {
            return obj.toString().trim();
        } else {
            return null;
        }
    }

    public static boolean getExtraCmdBoolean(Map<String, Object> extraCmd, String key, boolean defaultValue) {
        String value = getExtraCmdString(extraCmd, key);
        if (value == null) {
            return defaultValue;
        } else {
            return Boolean.parseBoolean(value);
        }
    }

    public static long getExtraCmdLong(Map<String, Object> extraCmd, String key, long defaultValue) {
        String value = getExtraCmdString(extraCmd, key);
        if (value == null) {
            return defaultValue;
        } else {
            return Long.valueOf(value);
        }
    }


    public static void printlnToStringBuilder(StringBuilder sb, String v) {
        sb.append(v).append("\n");
    }

    public static void printAFieldToStringBuilder(StringBuilder sb, String field, Object v, String inden) {
        if (v == null || v.toString().equals("") || v.toString().equals("[]") || v.toString().equals("SEQUENTIAL") || v.toString().equals("SHARED_LOCK"))
            return;

        printlnToStringBuilder(sb, inden + field + ":" + v);
    }


}
