package drds.plus.util;

import java.util.HashMap;
import java.util.Map;

public class ExtraCmd extends HashMap {
    /**
     * 需要重写
     */
    public static boolean getExtraCmdBoolean(Map<String, Object> extraCmd, String mergeExpand, boolean b) {
        Object v = extraCmd.get(mergeExpand);
        if (v == null) {
            return b;
        } else {
            return Boolean.valueOf(String.valueOf(v));
        }
    }
}
