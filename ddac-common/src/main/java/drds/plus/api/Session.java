package drds.plus.api;

import drds.plus.common.jdbc.transaction_policy.ITransactionPolicy;

import java.sql.SQLException;
import java.util.List;

public interface Session {

    void kill() throws SQLException;

    void cancelQuery() throws SQLException;

    long getLastInsertId();

    void setLastInsertId(long id);

    List<Long> getGeneratedKeys();

    void setGeneratedKeys(List<Long> ids);

    ITransactionPolicy getTransactionPolicy();

    void setTransactionPolicy(ITransactionPolicy transactionPolicy);

    String getSqlMode();

    void setSqlMode(String sqlMode);


    void close() throws SQLException;

    void setAutoCommit(boolean b) throws SQLException;

    void commit() throws SQLException;

    void rollback() throws SQLException;

}
