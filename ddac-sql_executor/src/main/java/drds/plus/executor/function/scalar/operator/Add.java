package drds.plus.executor.function.scalar.operator;

import drds.plus.executor.ExecuteContext;
import drds.plus.executor.function.scalar.ScalarFunction;
import drds.plus.sql_process.type.Calculator;
import drds.plus.sql_process.type.Type;
import drds.plus.sql_process.type.impl.IntervalType;

/**
 * @since 5.0.0
 */
public class Add extends ScalarFunction {

    public Object compute(ExecuteContext executeContext, Object[] args) {
        Type type = this.getReturnType();
        Calculator calculator = type.getCalculator();
        // 如果加法中有出现IntervalType类型，转到时间函数的加法处理
        for (Object arg : args) {
            if (arg instanceof IntervalType) {
                calculator = Type.TimestampType.getCalculator();
            }
        }

        if (type.getCalculator() != calculator) {
            return type.convert(calculator.add(args[0], args[1]));
        } else {
            return calculator.add(args[0], args[1]);
        }
    }

    public Type getReturnType() {
        Object arg0 = function.getArgList().get(0);
        Object arg1 = function.getArgList().get(1);
        // 比如出现"2012-12-10" + interval_primary 1 DAY
        if (getType(arg0) instanceof IntervalType) {
            return getType(arg1);
        } else if (getType(arg1) instanceof IntervalType) {
            return getType(arg0);
        }

        return getArgListTypeAndUseLongTypeWhenOccureNonNumberType();
    }

    public String[] getFunctionNames() {
        return new String[]{"add", "+"};
    }

}
