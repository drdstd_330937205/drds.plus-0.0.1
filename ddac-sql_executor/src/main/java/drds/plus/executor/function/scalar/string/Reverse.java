package drds.plus.executor.function.scalar.string;

import drds.plus.common.utils.TStringUtil;
import drds.plus.executor.ExecuteContext;
import drds.plus.executor.function.scalar.ScalarFunction;
import drds.plus.executor.utils.Utils;
import drds.plus.sql_process.type.Type;

/**
 * <pre>
 * REVERSE(str)
 *
 * Returns the string str with the order of the characters reversed.
 *
 * mysql> SELECT REVERSE('abc');
 *         -> 'cba'
 * </pre>
 *
 * @author mengshi.sunmengshi 2014年4月11日 下午6:37:57
 * @since 5.1.0
 */
public class Reverse extends ScalarFunction {

    public Type getReturnType() {
        return Type.StringType;
    }

    public String[] getFunctionNames() {
        return new String[]{"REVERSE"};
    }

    public Object compute(ExecuteContext executeContext, Object[] args) {
        if (Utils.isNull(args[0])) {
            return null;
        }

        String str = Type.StringType.convert(args[0]);

        return TStringUtil.reverse(str);

    }
}
