package drds.plus.executor.function.scalar.datatime;

import drds.plus.executor.ExecuteContext;
import drds.plus.executor.function.scalar.ScalarFunction;
import drds.plus.executor.utils.Utils;
import drds.plus.sql_process.type.Type;

import java.util.Calendar;

/**
 * Returns the month for date, in the range 1 to 12 for January to December, or
 * 0 for dates such setAliasAndSetNeedBuild '0000-00-00' or '2008-00-00' that have a zero month
 * part.
 *
 * <pre>
 * mysql> SELECT MONTH('2008-02-03');
 *         -> 2
 * </pre>
 */
public class Month extends ScalarFunction {

    public Object compute(ExecuteContext executeContext, Object[] args) {
        for (Object arg : args) {
            if (Utils.isNull(arg)) {
                return null;
            }
        }

        java.sql.Timestamp timestamp = Type.TimestampType.convert(args[0]);
        Calendar cal = Calendar.getInstance();
        cal.setTime(timestamp);

        return cal.get(Calendar.MONTH) + 1;
    }

    public Type getReturnType() {
        return Type.LongType;
    }

    public String[] getFunctionNames() {
        return new String[]{"MONTH"};
    }
}
