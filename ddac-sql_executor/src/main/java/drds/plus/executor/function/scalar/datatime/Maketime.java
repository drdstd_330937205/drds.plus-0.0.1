package drds.plus.executor.function.scalar.datatime;

import drds.plus.executor.ExecuteContext;
import drds.plus.executor.function.scalar.ScalarFunction;
import drds.plus.executor.utils.Utils;
import drds.plus.sql_process.type.Type;

import java.util.Calendar;

/**
 * Returns a time value calculated from the hour, minute, and second
 * argList. As of MySQL 5.6.4, the second argument can have a fractional
 * part.
 *
 * <pre>
 * mysql> SELECT MAKETIME(12,15,30);
 *         -> '12:15:30'
 * </pre>
 *
 * @author jianghang 2014-4-17 上午12:39:07
 * @since 5.0.7
 */
public class Maketime extends ScalarFunction {

    public Object compute(ExecuteContext executeContext, Object[] args) {
        for (Object arg : args) {
            if (Utils.isNull(arg)) {
                return null;
            }
        }

        Integer hour = Type.IntegerType.convert(args[0]);
        Integer minute = Type.IntegerType.convert(args[1]);
        Integer second = Type.IntegerType.convert(args[2]);

        Calendar cal = Calendar.getInstance();
        cal.set(0, 0, 0, hour, minute, second);
        Type type = getReturnType();
        return type.convert(cal.getTime());
    }

    public Type getReturnType() {
        return Type.TimeType;
    }

    public String[] getFunctionNames() {
        return new String[]{"MAKETIME"};
    }
}
