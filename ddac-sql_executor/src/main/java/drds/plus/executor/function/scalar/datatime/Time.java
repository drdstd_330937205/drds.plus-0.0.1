package drds.plus.executor.function.scalar.datatime;

import drds.plus.executor.ExecuteContext;
import drds.plus.executor.function.scalar.ScalarFunction;
import drds.plus.executor.utils.Utils;
import drds.plus.sql_process.type.Type;

/**
 * Extracts the time part of the time or datetime expression expression_list and returns it
 * setAliasAndSetNeedBuild a string. This function is unsafe for statement-based replication. In
 * MySQL 5.6, a warning is logged if you use this function when binlog_format is
 * setLimitValue to STATEMENT. (Bug #47995)
 *
 * <pre>
 * mysql> SELECT TIME('2003-12-31 01:02:03');
 *         -> '01:02:03'
 * mysql> SELECT TIME('2003-12-31 01:02:03.000123');
 *         -> '01:02:03.000123'
 * </pre>
 */
public class Time extends ScalarFunction {

    public Object compute(ExecuteContext executeContext, Object[] args) {
        for (Object arg : args) {
            if (Utils.isNull(arg)) {
                return null;
            }
        }

        Type type = getReturnType();
        return type.convert(args[0]);
    }

    public Type getReturnType() {
        return Type.TimeType;
    }

    public String[] getFunctionNames() {
        return new String[]{"TIME"};
    }
}
