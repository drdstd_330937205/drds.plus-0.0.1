package drds.plus.executor.function.scalar.subquery;

import drds.plus.executor.ExecuteContext;
import drds.plus.executor.function.scalar.filter.Filter;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.Operation;
import drds.tools.$;

import java.util.List;

public class Exists extends Filter {

    public String[] getFunctionNames() {
        return new String[]{Operation.exists.getOperationString()};
    }

    protected Object computeInner(ExecuteContext executeContext, Object[] args) {
        List arg = (List) args[0];

        return $.isNotNullAndHasElement(arg);

    }

}
