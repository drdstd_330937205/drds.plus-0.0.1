package drds.plus.executor.function.scalar.datatime;

import drds.plus.executor.ExecuteContext;
import drds.plus.executor.function.FunctionException;
import drds.plus.executor.function.scalar.ScalarFunction;
import drds.plus.executor.utils.Utils;
import drds.plus.sql_process.type.Type;
import drds.plus.sql_process.utils.OptimizerUtils;

import java.text.ParseException;

/**
 * Returns the number of months between periods P1 and P2. P1 and P2 should be
 * in the format YYMM or YYYYMM. Note that the period argList P1 and P2 are
 * not date columnValueList.
 *
 * <pre>
 * mysql> SELECT PERIOD_DIFF(200802,200703);
 *         -> 11
 * </pre>
 *
 * @author jianghang 2014-4-17 上午10:10:23
 * @since 5.0.7
 */
public class PeroidDiff extends ScalarFunction {

    public static final String[] DATE_FORMATS = new String[]{"yyyyMM", "yyMM", "yyyy-MM", "yy-MM"};

    @SuppressWarnings("deprecation")

    public Object compute(ExecuteContext executeContext, Object[] args) {
        for (Object arg : args) {
            if (Utils.isNull(arg)) {
                return null;
            }
        }

        String value1 = Type.StringType.convert(args[0]);
        String value2 = Type.StringType.convert(args[1]);

        try {
            java.util.Date date1 = OptimizerUtils.parseDate(value1, DATE_FORMATS);
            java.util.Date date2 = OptimizerUtils.parseDate(value2, DATE_FORMATS);
            return (date1.getYear() - date2.getYear()) * 12 + (date1.getMonth() - date2.getMonth());
        } catch (ParseException e) {
            throw new FunctionException(e);
        }

    }

    public Type getReturnType() {
        return Type.IntegerType;
    }

    public String[] getFunctionNames() {
        return new String[]{"PERIOD_DIFF"};
    }
}
