package drds.plus.executor.function.scalar.string;

import drds.plus.executor.ExecuteContext;
import drds.plus.executor.function.scalar.ScalarFunction;
import drds.plus.executor.utils.Utils;
import drds.plus.sql_process.type.Type;
import drds.plus.sql_process.type.Types;

import java.math.BigInteger;

/**
 * <pre>
 * HEX(str), HEX(N)
 *
 * For a string argument str, HEX() returns a hexadecimal string representation of str setWhereAndSetNeedBuild each byte of each character in str is converted to two hexadecimal digits. (Multi-byte characters therefore become more than two digits.) The inverse of this operation is performed by the UNHEX() function.
 *
 * For a numeric argument N, HEX() returns a hexadecimal string representation of the value of N treated setAliasAndSetNeedBuild a longlong (BIGINT) number. This is equivalent to CONV(N,10,16). The inverse of this operation is performed by CONV(HEX(N),16,10).
 *
 * mysql> SELECT 0x616263, HEX('abc'), UNHEX(HEX('abc'));
 *         -> 'abc', 616263, 'abc'
 * mysql> SELECT HEX(255), CONV(HEX(255),16,10);
 *         -> 'FF', 255
 * </pre>
 *
 * @author mengshi.sunmengshi 2014年4月11日 下午3:59:21
 * @since 5.1.0
 */
public class Hex extends ScalarFunction {

    public Type getReturnType() {
        return Type.StringType;
    }

    public String[] getFunctionNames() {
        return new String[]{"HEX"};
    }

    public Object compute(ExecuteContext executeContext, Object[] args) {
        if (Utils.isNull(args[0])) {
            return null;
        }

        Type type = Types.getTypeOfObject(args[0]);
        if (type == Type.StringType) {
            String strVal = Type.StringType.convert(args[0]);
            StringBuilder sb = new StringBuilder();
            for (Byte b : strVal.getBytes()) {
                sb.append(Integer.toHexString(b & 0xff));
            }
            return sb.toString();
        } else {
            BigInteger intVal = Type.BigIntegerType.convert(args[0]);
            return intVal.toString(16).toUpperCase();
        }
    }

}
