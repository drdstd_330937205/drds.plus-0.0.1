package drds.plus.executor.function.scalar.string;

import drds.plus.executor.ExecuteContext;
import drds.plus.executor.function.scalar.ScalarFunction;
import drds.plus.executor.utils.Utils;
import drds.plus.sql_process.type.Type;

/**
 * <pre>
 * SPACE(N)
 *
 * Returns a string consisting of N space characters.
 *
 * mysql> SELECT SPACE(6);
 *         -> '      '
 * </pre>
 *
 * @author mengshi.sunmengshi 2014年4月11日 下午6:33:08
 * @since 5.1.0
 */
public class Space extends ScalarFunction {

    public Type getReturnType() {
        return Type.StringType;
    }

    public String[] getFunctionNames() {
        return new String[]{"SPACE"};
    }

    public Object compute(ExecuteContext executeContext, Object[] args) {
        if (Utils.isNull(args[0])) {
            return null;
        }

        Integer len = Type.IntegerType.convert(args[0]);
        if (len <= 0) {
            return "";

        }
        StringBuilder sb = new StringBuilder(len);

        for (int i = 0; i < len; i++) {
            sb.append(' ');
        }
        return sb.toString();

    }
}
