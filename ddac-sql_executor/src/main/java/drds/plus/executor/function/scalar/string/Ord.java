package drds.plus.executor.function.scalar.string;

import drds.plus.executor.ExecuteContext;
import drds.plus.executor.function.scalar.ScalarFunction;
import drds.plus.executor.utils.Utils;
import drds.plus.sql_process.type.Type;
import drds.tools.$;

import java.math.BigInteger;

/**
 * <pre>
 * ORD(str)
 *
 * If the leftmost character of the string str is a multi-byte character, returns the code for that character, calculated from the numeric columnValueList of its constituent bytes using this formula:
 *
 *   (1st byte code)
 * + (2nd byte code * 256)
 * + (3rd byte code * 2562) ...
 *
 * If the leftmost character is not a multi-byte character, ORD() returns the same value setAliasAndSetNeedBuild the ASCII() function.
 *
 * mysql> SELECT ORD('2');
 *         -> 50
 *
 * </pre>
 *
 * @author mengshi.sunmengshi 2014年4月15日 下午5:16:15
 * @since 5.1.0
 */
public class Ord extends ScalarFunction {

    public Type getReturnType() {
        return Type.BigIntegerType;
    }

    public String[] getFunctionNames() {
        return new String[]{"ORD"};
    }

    public Object compute(ExecuteContext executeContext, Object[] args) {
        Object arg = args[0];

        if (Utils.isNull(arg)) {
            return null;
        }

        String str = Type.StringType.convert(arg);

        if ($.isNullOrEmpty(str)) {
            return 0;
        }

        Character leftMost = str.charAt(0);

        byte[] bytes = leftMost.toString().getBytes();

        BigInteger value = BigInteger.valueOf(0l);
        for (int i = 0; i < bytes.length; i++) {
            value = value.add(BigInteger.valueOf(bytes[i] & 0xff).pow(i + 1));
        }

        return value;

    }

}
