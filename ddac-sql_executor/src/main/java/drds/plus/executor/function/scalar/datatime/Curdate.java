package drds.plus.executor.function.scalar.datatime;

import drds.plus.executor.ExecuteContext;
import drds.plus.executor.function.scalar.ScalarFunction;
import drds.plus.sql_process.type.Type;

/**
 * Returns the currentJoinRowData date setAliasAndSetNeedBuild a value in 'YYYY-MM-DD' or YYYYMMDD format,
 * depending on whether the function is used in a string or numeric context.
 */
public class Curdate extends ScalarFunction {

    public Object compute(ExecuteContext executeContext, Object[] args) {
        Type type = getReturnType();
        return type.convert(new java.util.Date());
    }

    public Type getReturnType() {
        return Type.DateType;
    }

    public String[] getFunctionNames() {
        return new String[]{"CURDATE", "CURRENT_DATE"};
    }
}
