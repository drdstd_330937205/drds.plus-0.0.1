package drds.plus.executor.function.scalar.string;

import drds.plus.executor.ExecuteContext;
import drds.plus.executor.function.scalar.ScalarFunction;
import drds.plus.executor.utils.Utils;
import drds.plus.sql_process.type.Type;

/**
 * <pre>
 * STRCMP(expr1,expr2)
 *
 * STRCMP() returns 0 if the strings are the same, -1 if the firstInNewCursor argument is smaller than the second according to the currentJoinRowData sort order, and 1 otherwise.
 *
 * mysql> SELECT STRCMP('text', 'text2');
 *         -> -1
 * mysql> SELECT STRCMP('text2', 'text');
 *         -> 1
 * mysql> SELECT STRCMP('text', 'text');
 *         -> 0
 * </pre>
 *
 * @author mengshi.sunmengshi 2014年4月11日 下午6:27:36
 * @since 5.1.0
 */
public class StrCmp extends ScalarFunction {

    public Type getReturnType() {
        return Type.IntegerType;
    }

    public String[] getFunctionNames() {
        return new String[]{"STRCMP"};
    }

    public Object compute(ExecuteContext executeContext, Object[] args) {
        for (Object arg : args) {
            if (Utils.isNull(arg)) {
                return null;
            }
        }
        String str1 = Type.StringType.convert(args[0]);
        String str2 = Type.StringType.convert(args[1]);

        return Type.StringType.compare(str1, str2);
    }

}
