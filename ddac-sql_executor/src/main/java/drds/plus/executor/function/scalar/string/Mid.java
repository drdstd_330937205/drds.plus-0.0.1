package drds.plus.executor.function.scalar.string;

import drds.plus.executor.ExecuteContext;

/**
 * <pre>
 * MID(str,pos,len)
 *
 * MID(str,pos,len) is a synonym for SUBSTRING(str,pos,len).
 * </pre>
 *
 * @author mengshi.sunmengshi 2014年4月15日 下午2:34:55
 * @since 5.1.0
 */
public class Mid extends SubString {

    public String[] getFunctionNames() {
        return new String[]{"MID"};
    }

    @SuppressWarnings("unused")

    public Object compute(ExecuteContext executeContext, Object[] args) {
        Object a = args[2];
        return super.compute(executeContext, args);
    }
}
