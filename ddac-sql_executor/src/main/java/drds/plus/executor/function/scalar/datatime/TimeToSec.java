package drds.plus.executor.function.scalar.datatime;

import drds.plus.executor.ExecuteContext;
import drds.plus.executor.function.scalar.ScalarFunction;
import drds.plus.executor.utils.Utils;
import drds.plus.sql_process.type.Type;

/**
 * Returns the time argument, converted to seconds.
 *
 * <pre>
 * mysql> SELECT TIME_TO_SEC('22:23:00');
 *         -> 80580
 * mysql> SELECT TIME_TO_SEC('00:39:38');
 *         -> 2378
 * </pre>
 *
 * @author jianghang 2014-4-16 下午11:02:01
 * @since 5.0.7
 */
public class TimeToSec extends ScalarFunction {

    @SuppressWarnings("deprecation")

    public Object compute(ExecuteContext executeContext, Object[] args) {
        for (Object arg : args) {
            if (Utils.isNull(arg)) {
                return null;
            }
        }

        java.sql.Time time = Type.TimeType.convert(args[0]);
        return time.getHours() * 60 * 60 + time.getMinutes() * 60 + time.getSeconds();
    }

    public Type getReturnType() {
        return Type.LongType;
    }

    public String[] getFunctionNames() {
        return new String[]{"TIME_TO_SEC"};
    }

}
