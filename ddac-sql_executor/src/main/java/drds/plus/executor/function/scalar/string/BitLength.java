package drds.plus.executor.function.scalar.string;

import drds.plus.executor.ExecuteContext;
import drds.plus.executor.function.scalar.ScalarFunction;
import drds.plus.executor.utils.Utils;
import drds.plus.sql_process.type.Type;
import drds.tools.$;

/**
 * <pre>
 * BIT_LENGTH(str)
 *
 * Returns the length of the string str in bits.
 *
 * mysql> SELECT BIT_LENGTH('text');
 *         -> 32
 * </pre>
 *
 * @author mengshi.sunmengshi 2014年4月11日 下午1:31:31
 * @since 5.1.0
 */
public class BitLength extends ScalarFunction {

    public Type getReturnType() {
        return Type.LongType;
    }

    public String[] getFunctionNames() {
        return new String[]{"BIT_LENGTH"};
    }

    public Object compute(ExecuteContext executeContext, Object[] args) {
        Object arg = args[0];

        if (Utils.isNull(arg)) {
            return null;
        }

        String str = Type.StringType.convert(arg);
        if ($.isNullOrEmpty(str)) {
            return 0;
        }
        return str.getBytes().length * 8;
    }
}
