package drds.plus.executor.function.scalar.datatime;

import drds.plus.executor.ExecuteContext;
import drds.plus.executor.function.scalar.ScalarFunction;
import drds.plus.executor.utils.Utils;
import drds.plus.sql_process.type.Type;

import java.util.Calendar;

/**
 * With a single argument, this function returns the date or datetime expression
 * expression_list setAliasAndSetNeedBuild a datetime value. With two argList, it adds the time
 * expression expr2 to the date or datetime expression expr1 and returns the
 * result setAliasAndSetNeedBuild a datetime value.
 *
 * <pre>
 * mysql> SELECT TIMESTAMP('2003-12-31');
 *         -> '2003-12-31 00:00:00'
 * mysql> SELECT TIMESTAMP('2003-12-31 12:00:00','12:00:00');
 *         -> '2004-01-01 00:00:00'
 * </pre>
 *
 * @author jianghang 2014-4-16 下午11:16:06
 * @since 5.0.7
 */
public class Timestamp extends ScalarFunction {

    @SuppressWarnings("deprecation")

    public Object compute(ExecuteContext executeContext, Object[] args) {
        for (Object arg : args) {
            if (Utils.isNull(arg)) {
                return null;
            }
        }

        java.sql.Timestamp timestamp = Type.TimestampType.convert(args[0]);
        if (args.length >= 2) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(timestamp);

            java.sql.Time time = Type.TimeType.convert(args[1]);

            cal.add(Calendar.HOUR_OF_DAY, time.getHours());
            cal.add(Calendar.MINUTE, time.getMinutes());
            cal.add(Calendar.SECOND, time.getSeconds());
            Type type = getReturnType();
            return type.convert(cal.getTime());
        } else {
            return timestamp;
        }
    }

    public Type getReturnType() {
        return Type.TimestampType;
    }

    public String[] getFunctionNames() {
        return new String[]{"TIMESTAMP"};
    }
}
