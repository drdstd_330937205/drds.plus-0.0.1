package drds.plus.executor.function.scalar.filter;

import drds.plus.executor.ExecuteContext;
import drds.plus.sql_process.type.Type;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Or extends Filter {

    protected Boolean computeInner(ExecuteContext executeContext, Object[] args) {
        for (Object arg : args) {
            if (Type.BooleanType.convert(arg)) {
                return true;
            }
        }
        return false;
    }

    public String[] getFunctionNames() {
        return new String[]{"or", "||"};
    }
}
