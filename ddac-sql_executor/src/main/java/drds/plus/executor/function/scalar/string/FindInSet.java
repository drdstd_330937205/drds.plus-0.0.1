package drds.plus.executor.function.scalar.string;

import drds.plus.executor.ExecuteContext;
import drds.plus.executor.function.scalar.ScalarFunction;
import drds.plus.executor.utils.Utils;
import drds.plus.sql_process.type.Type;

import java.util.Arrays;
import java.util.List;

public class FindInSet extends ScalarFunction {

    public Type getReturnType() {
        return Type.IntegerType;
    }

    public String[] getFunctionNames() {
        return new String[]{"FIND_IN_SET"};
    }

    public Object compute(ExecuteContext executeContext, Object[] args) {
        if (Utils.isNull(args[0]) || Utils.isNull(args[1])) {
            return null;
        }

        String str1 = Type.StringType.convert(args[0]);
        List strList = Arrays.asList((Type.StringType.convert(args[1]).split(",")));

        return strList.indexOf(str1) + 1;

    }

}
