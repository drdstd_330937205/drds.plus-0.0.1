package drds.plus.executor.function.scalar.string;

import drds.plus.common.utils.TStringUtil;
import drds.plus.executor.ExecuteContext;
import drds.plus.executor.function.scalar.ScalarFunction;
import drds.plus.executor.utils.Utils;
import drds.plus.sql_process.type.Type;

/**
 * <pre>
 * replace(str,from_str,to_str)
 *
 * Returns the string str with all occurrences of the string from_str replaced by the string to_str. replace() performs a case-sensitive ruleCalculate when searching for from_str.
 *
 * mysql> SELECT replace('www.mysql.com', 'writeWeight', 'Ww');
 *         -> 'WwWwWw.mysql.com'
 *
 * </pre>
 *
 * @author mengshi.sunmengshi 2014年4月15日 下午5:32:36
 * @since 5.1.0
 */
public class Replace extends ScalarFunction {

    public Type getReturnType() {
        return Type.StringType;
    }

    public String[] getFunctionNames() {
        return new String[]{"replace"};
    }

    public Object compute(ExecuteContext executeContext, Object[] args) {

        for (Object arg : args) {
            if (Utils.isNull(arg)) {
                return null;
            }
        }
        String str = Type.StringType.convert(args[0]);
        String fromStr = Type.StringType.convert(args[1]);
        String toStr = Type.StringType.convert(args[2]);

        return TStringUtil.replace(str, fromStr, toStr);

    }

}
