package drds.plus.executor.function.scalar.datatime;

import drds.plus.executor.ExecuteContext;
import drds.plus.executor.function.scalar.ScalarFunction;
import drds.plus.executor.utils.Utils;
import drds.plus.sql_process.type.Type;

/**
 * Extracts the date part of the date or datetime expression expression_list.
 *
 * <pre>
 * mysql> SELECT DATE('2003-12-31 01:02:03');
 *         -> '2003-12-31'
 * </pre>
 */
public class Date extends ScalarFunction {

    public Object compute(ExecuteContext executeContext, Object[] args) {
        for (Object arg : args) {
            if (Utils.isNull(arg)) {
                return null;
            }
        }

        Type type = getReturnType();
        return type.convert(args[0]);
    }

    public Type getReturnType() {
        return Type.DateType;
    }

    public String[] getFunctionNames() {
        return new String[]{"DATE"};
    }
}
