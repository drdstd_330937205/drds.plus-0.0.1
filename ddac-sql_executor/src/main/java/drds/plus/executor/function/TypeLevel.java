package drds.plus.executor.function;

import drds.plus.sql_process.type.Type;

public class TypeLevel {

    public static int OTHER = 100;
    public int level = OTHER;
    public Type type;

    public boolean isOther() {
        return level == OTHER;
    }
}
