package drds.plus.executor.function.aggregate_function;

import drds.plus.executor.ExecuteContext;
import drds.plus.sql_process.type.Type;


public class Count extends AggregateFunction {

    private long count = 0;

    public void map(ExecuteContext executeContext, Object[] args) {
        count++;
    }

    public void reduce(ExecuteContext executeContext, Object[] args) {
        Type type = this.getReturnType();
        Object arg = args[0];
        if (arg != null) {
            count += (Long) type.convert(arg);
        }
    }

    public Object getResult() {
        return count;
    }

    public void clear() {
        this.count = 0;
    }

    public Type getReturnType() {
        return Type.LongType;
    }

    public Type getMapReturnType() {
        return Type.LongType;
    }

    public String[] getFunctionNames() {
        return new String[]{"count"};
    }
}
