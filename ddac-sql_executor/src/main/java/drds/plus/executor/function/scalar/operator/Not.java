package drds.plus.executor.function.scalar.operator;

import drds.plus.executor.ExecuteContext;
import drds.plus.executor.function.scalar.ScalarFunction;
import drds.plus.sql_process.type.Type;

/**
 * @since 5.0.0
 */
public class Not extends ScalarFunction {

    public Object compute(ExecuteContext executeContext, Object[] args) {
        Type type = getReturnType();
        Object obj = type.convert(args[0]);
        if (obj instanceof Number) {
            return ((Number) args[0]).longValue() == 0 ? 1 : 0;
        } else if (obj instanceof Boolean) {
            return !(Boolean) obj;
        } else {
            // 非数字类型全为false，一般不会走到这逻辑
            return false;
        }
    }

    public Type getReturnType() {
        Type type = getFirstArgType();
        if (type == Type.BooleanType) {
            return Type.BooleanType;
        } else {
            return Type.LongType;
        }
    }

    public String[] getFunctionNames() {
        return new String[]{"NOT", "!"};
    }
}
