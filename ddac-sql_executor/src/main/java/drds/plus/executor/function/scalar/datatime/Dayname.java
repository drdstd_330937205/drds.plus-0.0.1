package drds.plus.executor.function.scalar.datatime;

import drds.plus.executor.ExecuteContext;
import drds.plus.executor.function.FunctionException;
import drds.plus.executor.function.scalar.ScalarFunction;
import drds.plus.executor.utils.Utils;
import drds.plus.sql_process.type.Type;

import java.util.Calendar;

/**
 * Returns the columnName of the weekday for date. As of MySQL 5.1.12, the language
 * used for the columnName is controlled by the value of the lc_time_names system
 * variable (Section 10.7, “MySQL Server Locale Support”).
 *
 * <pre>
 * mysql> SELECT DAYNAME('2007-02-03');
 *         -> 'Saturday'
 * </pre>
 *
 * @author jianghang 2014-4-16 下午5:52:04
 * @since 5.0.7
 */
public class Dayname extends ScalarFunction {

    public Object compute(ExecuteContext executeContext, Object[] args) {
        for (Object arg : args) {
            if (Utils.isNull(arg)) {
                return null;
            }
        }

        java.sql.Timestamp timestamp = Type.TimestampType.convert(args[0]);
        Calendar cal = Calendar.getInstance();
        cal.setTime(timestamp);

        int dayname = cal.get(Calendar.DAY_OF_WEEK);
        switch (dayname) {
            case Calendar.SUNDAY:
                return "Sunday";
            case Calendar.MONDAY:
                return "Monday";
            case Calendar.TUESDAY:
                return "Tuesday";
            case Calendar.WEDNESDAY:
                return "Wednesday";
            case Calendar.THURSDAY:
                return "Thursday";
            case Calendar.FRIDAY:
                return "Friday";
            case Calendar.SATURDAY:
                return "Saturday";
            default:
                throw new FunctionException("impossbile");
        }
    }

    public Type getReturnType() {
        return Type.StringType;
    }

    public String[] getFunctionNames() {
        return new String[]{"DAYNAME"};
    }
}
