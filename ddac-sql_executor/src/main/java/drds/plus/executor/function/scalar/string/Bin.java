package drds.plus.executor.function.scalar.string;

import drds.plus.executor.ExecuteContext;
import drds.plus.executor.function.scalar.ScalarFunction;
import drds.plus.executor.utils.Utils;
import drds.plus.sql_process.type.Type;

import java.math.BigInteger;

/**
 * <pre>
 * BIN(N)
 *
 * Returns a string representation of the binary value of N, setWhereAndSetNeedBuild N is a longlong (BIGINT) number. This is equivalent to CONV(N,10,2). Returns NULL_KEY if N is NULL_KEY.
 *
 * mysql> SELECT BIN(12);
 *         -> '1100'
 * </pre>
 *
 * @author mengshi.sunmengshi 2014年4月11日 下午1:29:30
 * @since 5.1.0
 */
public class Bin extends ScalarFunction {

    public static void main(String[] args) {
        System.out.println((new BigInteger("128").toString(2)));
    }

    public Type getReturnType() {
        return Type.StringType;
    }

    public String[] getFunctionNames() {
        return new String[]{"BIN"};
    }

    public Object compute(ExecuteContext executeContext, Object[] args) {
        Object arg = args[0];

        if (Utils.isNull(arg)) {
            return null;
        }

        BigInteger longlong = Type.BigIntegerType.convert(arg);

        if (longlong == null) {
            return "0";
        }

        return longlong.toString(2);
    }
}
