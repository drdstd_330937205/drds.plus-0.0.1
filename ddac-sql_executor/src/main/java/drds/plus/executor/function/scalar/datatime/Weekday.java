package drds.plus.executor.function.scalar.datatime;

import drds.plus.executor.ExecuteContext;
import drds.plus.executor.function.scalar.ScalarFunction;
import drds.plus.executor.utils.Utils;
import drds.plus.sql_process.type.Type;

import java.util.Calendar;

/**
 * Returns the weekday indexMapping for date (0 = Monday, 1 = Tuesday, … 6 =
 * Sunday).
 *
 * <pre>
 * mysql> SELECT WEEKDAY('2008-02-03 22:23:00');
 *         -> 6
 * mysql> SELECT WEEKDAY('2007-11-06');
 *         -> 1
 * </pre>
 *
 * @author jianghang 2014-4-16 下午6:50:22
 * @since 5.0.7
 */
public class Weekday extends ScalarFunction {

    public Object compute(ExecuteContext executeContext, Object[] args) {
        for (Object arg : args) {
            if (Utils.isNull(arg)) {
                return null;
            }
        }

        java.sql.Timestamp timestamp = Type.TimestampType.convert(args[0]);
        Calendar cal = Calendar.getInstance();
        cal.setTime(timestamp);

        int week = cal.get(Calendar.DAY_OF_WEEK);
        if (week == Calendar.SUNDAY) {
            return 6;
        } else {
            return week - 2;
        }
    }

    public Type getReturnType() {
        return Type.LongType;
    }

    public String[] getFunctionNames() {
        return new String[]{"WEEKDAY"};
    }
}
