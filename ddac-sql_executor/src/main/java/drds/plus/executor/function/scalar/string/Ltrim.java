package drds.plus.executor.function.scalar.string;

import drds.plus.executor.ExecuteContext;
import drds.plus.executor.function.scalar.ScalarFunction;
import drds.plus.executor.utils.Utils;
import drds.plus.sql_process.type.Type;

/**
 * <pre>
 * LTRIM(str)
 *
 * Returns the string str with leading space characters removed.
 *
 * mysql> SELECT LTRIM('  barbar');
 *         -> 'barbar'
 * </pre>
 *
 * @author mengshi.sunmengshi 2014年4月11日 下午5:56:44
 * @since 5.1.0
 */
public class Ltrim extends ScalarFunction {

    public Type getReturnType() {
        return Type.StringType;
    }

    public String[] getFunctionNames() {
        return new String[]{"LTRIM"};
    }

    public Object compute(ExecuteContext executeContext, Object[] args) {
        if (Utils.isNull(args[0])) {
            return null;
        }

        String str = Type.StringType.convert(args[0]);
        int st = 0;
        while (st < str.length() && str.charAt(st) <= ' ') {
            st++;
        }
        if (st > 0)
            return str.substring(st);
        else
            return str;
    }

}
