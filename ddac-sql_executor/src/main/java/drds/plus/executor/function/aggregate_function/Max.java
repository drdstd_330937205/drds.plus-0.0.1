package drds.plus.executor.function.aggregate_function;

import drds.plus.executor.ExecuteContext;
import drds.plus.sql_process.type.Type;
import drds.plus.sql_process.type.Types;


public class Max extends AggregateFunction {

    public Max() {
    }

    public void map(ExecuteContext executeContext, Object[] args) {
        doMax(args);
    }

    public void reduce(ExecuteContext executeContext, Object[] args) {
        doMax(args);
    }

    private void doMax(Object[] args) {
        Object arg = args[0];
        Type type = this.getReturnType();

        if (type == null) {
            if (arg != null) {
                type = Types.getTypeOfObject(arg);
            }
        }
        if (arg != null) {
            if (result == null) {
                result = arg;
            }
            if (type.compare(arg, result) > 0) {
                result = arg;
            }

        }
    }

    public int getArgSize() {
        return 1;
    }

    public Type getReturnType() {
        return this.getMapReturnType();
    }

    public Type getMapReturnType() {
        return getFirstArgType();
    }

    public String[] getFunctionNames() {
        return new String[]{"max"};
    }
}
