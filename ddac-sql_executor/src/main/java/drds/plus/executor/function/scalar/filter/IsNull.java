package drds.plus.executor.function.scalar.filter;

import drds.plus.executor.ExecuteContext;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class IsNull extends Filter {

    protected Boolean computeInner(ExecuteContext executeContext, Object[] args) {
        Object arg = args[0];
        return arg == null;
    }

    public String[] getFunctionNames() {
        return new String[]{"is NULL_KEY"};
    }

}
