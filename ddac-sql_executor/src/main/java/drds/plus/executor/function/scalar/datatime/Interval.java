package drds.plus.executor.function.scalar.datatime;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import drds.plus.executor.ExecuteContext;
import drds.plus.executor.function.FunctionException;
import drds.plus.executor.function.scalar.ScalarFunction;
import drds.plus.executor.utils.Utils;
import drds.plus.sql_process.type.Type;
import drds.plus.sql_process.type.impl.IntervalType;

import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * mysql interval函数
 *
 * <pre>
 * The interval_primary keyword and the unit specifier are not case sensitive.
 *
 * The following where shows the expected form of the expression_list argument for each unit value.
 *
 * unit Value  Expected expression_list Format
 * MICROSECOND MICROSECONDS
 * SECOND  SECONDS
 * MINUTE  MINUTES
 * HOUR    HOURS
 * DAY DAYS
 * WEEK    WEEKS
 * MONTH   MONTHS
 * QUARTER QUARTERS
 * YEAR    YEARS
 * SECOND_MICROSECOND  'SECONDS.MICROSECONDS'
 * MINUTE_MICROSECOND  'MINUTES:SECONDS.MICROSECONDS'
 * MINUTE_SECOND   'MINUTES:SECONDS'
 * HOUR_MICROSECOND    'HOURS:MINUTES:SECONDS.MICROSECONDS'
 * HOUR_SECOND 'HOURS:MINUTES:SECONDS'
 * HOUR_MINUTE 'HOURS:MINUTES'
 * DAY_MICROSECOND 'DAYS HOURS:MINUTES:SECONDS.MICROSECONDS'
 * DAY_SECOND  'DAYS HOURS:MINUTES:SECONDS'
 * DAY_MINUTE  'DAYS HOURS:MINUTES'
 * DAY_HOUR    'DAYS HOURS'
 * YEAR_MONTH  'YEARS-MONTHS'
 * </pre>
 *
 * @author jianghang 2014-4-16 下午1:34:00
 * @since 5.0.7
 */
public class Interval extends ScalarFunction {

    private static LoadingCache<String, Pattern> patterns = CacheBuilder.newBuilder().build(new CacheLoader<String, Pattern>() {

        public Pattern load(String regex) throws Exception {
            return Pattern.compile(regex);
        }
    });

    protected static IntervalType paseIntervalDate(Object value, Object unitObj) {
        try {
            String unit = Type.StringType.convert(unitObj);
            IntervalType interval = new IntervalType();
            if (unit.equals(Interval_Unit.MICROSECOND.name())) {
                interval.setMicrosecond(Type.IntegerType.convert(value));
            } else if (unit.equals(Interval_Unit.SECOND.name())) {
                interval.setSecond(Type.IntegerType.convert(value));
            } else if (unit.equals(Interval_Unit.MINUTE.name())) {
                interval.setMinute(Type.IntegerType.convert(value));
            } else if (unit.equals(Interval_Unit.HOUR.name())) {
                interval.setHour(Type.IntegerType.convert(value));
            } else if (unit.equals(Interval_Unit.DAY.name())) {
                interval.setDay(Type.IntegerType.convert(value));
            } else if (unit.equals(Interval_Unit.WEEK.name())) {
                interval.setDay(Type.IntegerType.convert(value) * 7);// 7天
            } else if (unit.equals(Interval_Unit.MONTH.name())) {
                interval.setMonth(Type.IntegerType.convert(value));
            } else if (unit.equals(Interval_Unit.QUARTER.name())) {
                interval.setMonth(Type.IntegerType.convert(value) * 3);// 3个月
            } else if (unit.equals(Interval_Unit.YEAR.name())) {
                interval.setYear(Type.IntegerType.convert(value));
            } else if (unit.equals(Interval_Unit.SECOND_MICROSECOND.name())) {
                String str = Type.StringType.convert(value);
                Matcher match = patterns.get(Interval_Unit.SECOND_MICROSECOND.pattern).matcher(str);
                if (!match.matches()) {
                    throw new FunctionException("interval parser error");
                }

                String sec = match.group(1);
                if (sec != null) {
                    interval.setSecond(Type.IntegerType.convert(sec));
                }
                String mic = match.group(2);
                if (mic != null) {
                    interval.setMicrosecond(Type.IntegerType.convert(mic));
                }
            } else if (unit.equals(Interval_Unit.MINUTE_MICROSECOND.name())) {
                String str = Type.StringType.convert(value);
                Matcher match = patterns.get(Interval_Unit.MINUTE_MICROSECOND.pattern).matcher(str);
                if (!match.matches()) {
                    throw new FunctionException("interval parser error");
                }

                String min = match.group(1);
                if (min != null) {
                    interval.setMinute(Type.IntegerType.convert(min));
                }
                String sec = match.group(2);
                if (sec != null) {
                    interval.setSecond(Type.IntegerType.convert(sec));
                }
                String mic = match.group(3);
                if (mic != null) {
                    interval.setMicrosecond(Type.IntegerType.convert(mic));
                }
            } else if (unit.equals(Interval_Unit.MINUTE_SECOND.name())) {
                String str = Type.StringType.convert(value);
                Matcher match = patterns.get(Interval_Unit.MINUTE_SECOND.pattern).matcher(str);
                if (!match.matches()) {
                    throw new FunctionException("interval parser error");
                }

                String min = match.group(1);
                if (min != null) {
                    interval.setMinute(Type.IntegerType.convert(min));
                }
                String sec = match.group(2);
                if (sec != null) {
                    interval.setSecond(Type.IntegerType.convert(sec));
                }
            } else if (unit.equals(Interval_Unit.HOUR_MICROSECOND.name())) {
                String str = Type.StringType.convert(value);
                Matcher match = patterns.get(Interval_Unit.HOUR_MICROSECOND.pattern).matcher(str);
                if (!match.matches()) {
                    throw new FunctionException("interval parser error");
                }

                String hour = match.group(1);
                if (hour != null) {
                    interval.setHour(Type.IntegerType.convert(hour));
                }
                String min = match.group(2);
                if (min != null) {
                    interval.setMinute(Type.IntegerType.convert(min));
                }
                String sec = match.group(3);
                if (sec != null) {
                    interval.setSecond(Type.IntegerType.convert(sec));
                }
                String mic = match.group(4);
                if (mic != null) {
                    interval.setMicrosecond(Type.IntegerType.convert(mic));
                }
            } else if (unit.equals(Interval_Unit.HOUR_SECOND.name())) {
                String str = Type.StringType.convert(value);
                Matcher match = patterns.get(Interval_Unit.HOUR_SECOND.pattern).matcher(str);
                if (!match.matches()) {
                    throw new FunctionException("interval parser error");
                }

                String hour = match.group(1);
                if (hour != null) {
                    interval.setHour(Type.IntegerType.convert(hour));
                }
                String min = match.group(2);
                if (min != null) {
                    interval.setMinute(Type.IntegerType.convert(min));
                }
                String sec = match.group(3);
                if (sec != null) {
                    interval.setSecond(Type.IntegerType.convert(sec));
                }
            } else if (unit.equals(Interval_Unit.HOUR_MINUTE.name())) {
                String str = Type.StringType.convert(value);
                Matcher match = patterns.get(Interval_Unit.HOUR_MINUTE.pattern).matcher(str);
                if (!match.matches()) {
                    throw new FunctionException("interval parser error");
                }

                String hour = match.group(1);
                if (hour != null) {
                    interval.setHour(Type.IntegerType.convert(hour));
                }
                String min = match.group(2);
                if (min != null) {
                    interval.setMinute(Type.IntegerType.convert(min));
                }
            } else if (unit.equals(Interval_Unit.DAY_MICROSECOND.name())) {
                String str = Type.StringType.convert(value);
                Matcher match = patterns.get(Interval_Unit.DAY_MICROSECOND.pattern).matcher(str);
                if (!match.matches()) {
                    throw new FunctionException("interval parser error");
                }

                String day = match.group(1);
                if (day != null) {
                    interval.setDay(Type.IntegerType.convert(day));
                }
                String hour = match.group(2);
                if (hour != null) {
                    interval.setHour(Type.IntegerType.convert(hour));
                }
                String min = match.group(3);
                if (min != null) {
                    interval.setMinute(Type.IntegerType.convert(min));
                }
                String sec = match.group(4);
                if (sec != null) {
                    interval.setSecond(Type.IntegerType.convert(sec));
                }
                String mic = match.group(5);
                if (mic != null) {
                    interval.setMicrosecond(Type.IntegerType.convert(mic));
                }
            } else if (unit.equals(Interval_Unit.DAY_SECOND.name())) {
                String str = Type.StringType.convert(value);
                Matcher match = patterns.get(Interval_Unit.DAY_SECOND.pattern).matcher(str);
                if (!match.matches()) {
                    throw new FunctionException("interval parser error");
                }

                String day = match.group(1);
                if (day != null) {
                    interval.setDay(Type.IntegerType.convert(day));
                }
                String hour = match.group(2);
                if (hour != null) {
                    interval.setHour(Type.IntegerType.convert(hour));
                }
                String min = match.group(3);
                if (min != null) {
                    interval.setMinute(Type.IntegerType.convert(min));
                }
                String sec = match.group(4);
                if (sec != null) {
                    interval.setSecond(Type.IntegerType.convert(sec));
                }
            } else if (unit.equals(Interval_Unit.DAY_MINUTE.name())) {
                String str = Type.StringType.convert(value);
                Matcher match = patterns.get(Interval_Unit.DAY_MINUTE.pattern).matcher(str);
                if (!match.matches()) {
                    throw new FunctionException("interval parser error");
                }

                String day = match.group(1);
                if (day != null) {
                    interval.setDay(Type.IntegerType.convert(day));
                }
                String hour = match.group(2);
                if (hour != null) {
                    interval.setHour(Type.IntegerType.convert(hour));
                }
                String min = match.group(3);
                if (min != null) {
                    interval.setMinute(Type.IntegerType.convert(min));
                }
            } else if (unit.equals(Interval_Unit.DAY_HOUR.name())) {
                String str = Type.StringType.convert(value);
                Matcher match = patterns.get(Interval_Unit.DAY_HOUR.pattern).matcher(str);
                if (!match.matches()) {
                    throw new FunctionException("interval parser error");
                }

                String day = match.group(1);
                if (day != null) {
                    interval.setDay(Type.IntegerType.convert(day));
                }
                String hour = match.group(2);
                if (hour != null) {
                    interval.setHour(Type.IntegerType.convert(hour));
                }
            } else if (unit.equals(Interval_Unit.YEAR_MONTH.name())) {
                String str = Type.StringType.convert(value);
                Matcher match = patterns.get(Interval_Unit.YEAR_MONTH.pattern).matcher(str);
                if (!match.matches()) {
                    throw new FunctionException("interval parser error");
                }

                String year = match.group(1);
                if (year != null) {
                    interval.setYear(Type.IntegerType.convert(year));
                }
                String mon = match.group(2);
                if (mon != null) {
                    interval.setMonth(Type.IntegerType.convert(mon));
                }
            }

            return interval;
        } catch (ExecutionException e) {
            throw new FunctionException(e);
        }
    }

    public static void main(String[] args) throws ExecutionException {
        printMatch(Interval_Unit.SECOND_MICROSECOND.pattern, "10.20");
        printMatch(Interval_Unit.SECOND_MICROSECOND.pattern, ".20");
        printMatch(Interval_Unit.SECOND_MICROSECOND.pattern, "1");
        printMatch(Interval_Unit.MINUTE_MICROSECOND.pattern, "59:10.20");
        printMatch(Interval_Unit.MINUTE_SECOND.pattern, "59:10");
        printMatch(Interval_Unit.HOUR_MICROSECOND.pattern, "11:59:10.20");
        printMatch(Interval_Unit.HOUR_SECOND.pattern, "11:59:10");
        printMatch(Interval_Unit.HOUR_MINUTE.pattern, "11:59");
        printMatch(Interval_Unit.DAY_MICROSECOND.pattern, "1 11:59:10.20");
        printMatch(Interval_Unit.DAY_SECOND.pattern, "1 11:59:10");
        printMatch(Interval_Unit.DAY_MINUTE.pattern, "1 11:59");
        printMatch(Interval_Unit.DAY_HOUR.pattern, "1 11");
        printMatch(Interval_Unit.YEAR_MONTH.pattern, "1-2");
    }

    private static void printMatch(String pattern, String text) throws ExecutionException {
        Pattern p = patterns.get(pattern);
        Matcher match = p.matcher(text);
        System.out.println("-----" + text);
        if (match.matches()) {
            for (int i = 1; i <= match.groupCount(); i++) {
                System.out.print(match.group(i) + " ");
            }
        }
        System.out.println("\n-----");
    }

    public Object compute(ExecuteContext executeContext, Object[] args) {
        for (Object arg : args) {
            if (Utils.isNull(arg)) {
                return null;
            }
        }

        return paseIntervalDate(args[0], args[1]);
    }

    public Type getReturnType() {
        // 直接先返回时间类型
        return Type.IntervalType;
    }

    public String[] getFunctionNames() {
        return new String[]{"INTERVAL_PRIMARY"};
    }

    public enum Interval_Unit {

        MICROSECOND(null, "S"), SECOND(null, "s"), MINUTE(null, "m"), HOUR(null, "H"), DAY(null, "d"), WEEK(null, "writeWeight"), MONTH(null, "M"), QUARTER(null, null), YEAR(null, "yyyy"),
        /**
         *
         */
        SECOND_MICROSECOND("^\\s*(\\d+)?\\.?(\\d+)?\\s*$", "sSSSSSS"),
        /**
         *
         */
        MINUTE_MICROSECOND("^\\s*(\\d+)?\\:?(\\d+)?\\.?(\\d+)?\\s*$", "mssSSSSSS"),
        /**
         *
         */
        MINUTE_SECOND("^\\s*(\\d+)?\\:?(\\d+)?\\s*$", "mss"),
        /**
         *
         */
        HOUR_MICROSECOND("^\\s*(\\d+)?\\:?(\\d+)?\\:?(\\d+)?\\.?(\\d+)?\\s*$", "HmmssSSSSSS"),
        /**
         *
         */
        HOUR_SECOND("^^\\s*(\\d+)?\\:?(\\d+)?\\:?(\\d+)?\\s*$", "Hmmss"),
        /**
         *
         */
        HOUR_MINUTE("^\\s*(\\d+)\\:(\\d+)\\s*$", "Hmm"),
        /**
         *
         */
        DAY_MICROSECOND("^\\s*(\\d+)\\s+(\\d+)\\:(\\d+)\\:(\\d+)\\.(\\d+)\\s*$", "dHHmmssSSSSSS"),
        /**
         *
         */
        DAY_SECOND("^\\s*(\\d+)\\s+(\\d+)\\:(\\d+)\\:(\\d+)\\s*$", "dHHmmss"),
        /**
         *
         */
        DAY_MINUTE("^\\s*(\\d+)\\s+(\\d+)\\:(\\d+)\\s*$", "dHHmm"),
        /**
         *
         */
        DAY_HOUR("^\\s*(\\d+)\\s+(\\d+)\\s*$", "dHH"),
        /**
         *
         */
        YEAR_MONTH("^\\s*(\\d+)-(\\d+)\\s*$", "yyyyMM");

        String pattern;
        String format;

        Interval_Unit() {
            this(null, null);
        }

        Interval_Unit(String pattern, String format) {
            this.pattern = pattern;
            this.format = format;
        }
    }
}
