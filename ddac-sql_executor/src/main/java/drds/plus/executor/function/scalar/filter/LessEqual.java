package drds.plus.executor.function.scalar.filter;

import drds.plus.executor.ExecuteContext;
import drds.plus.sql_process.type.Type;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LessEqual extends Filter {

    protected Boolean computeInner(ExecuteContext executeContext, Object[] args) {
        Type type = this.getArgType();
        return type.compare(args[0], args[1]) <= 0;
    }

    public String[] getFunctionNames() {
        return new String[]{"<="};
    }

    public Type getArgType() {
        return getArgListTypeAndUseFirstArgTypeWhenOccureNonNumberType();
    }
}
