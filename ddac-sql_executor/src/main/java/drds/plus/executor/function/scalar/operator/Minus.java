package drds.plus.executor.function.scalar.operator;

import drds.plus.executor.ExecuteContext;
import drds.plus.executor.function.scalar.ScalarFunction;
import drds.plus.sql_process.type.Type;

/**
 * mysql的Minus函数,取负数操作
 */
public class Minus extends ScalarFunction {

    public Object compute(ExecuteContext executeContext, Object[] args) {
        Type type = getReturnType();
        // -min(id) = min(id) * -1
        return type.getCalculator().multiply(args[0], -1);
    }

    public Type getReturnType() {
        return getFirstArgType();
    }

    public String[] getFunctionNames() {
        return new String[]{"minus"};
    }
}
