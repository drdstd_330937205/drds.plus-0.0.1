package drds.plus.executor.function.scalar.datatime;

import drds.plus.executor.ExecuteContext;
import drds.plus.executor.function.scalar.ScalarFunction;
import drds.plus.executor.utils.Utils;
import drds.plus.sql_process.type.Type;

import java.util.Calendar;

/**
 * SUBTIME() returns expr1 – expr2 expressed setAliasAndSetNeedBuild a value in the same format
 * setAliasAndSetNeedBuild expr1. expr1 is a time or datetime expression, and expr2 is a time
 * expression.
 *
 * <pre>
 * mysql> SELECT SUBTIME('2007-12-31 23:59:59','1:1:1');
 *         -> '2007-12-30 22:58:58.999997'
 * </pre>
 *
 * @author jianghang 2014-4-16 下午11:29:18
 * @since 5.0.7
 */
public class SubTime extends ScalarFunction {

    @SuppressWarnings("deprecation")

    public Object compute(ExecuteContext executeContext, Object[] args) {
        for (Object arg : args) {
            if (Utils.isNull(arg)) {
                return null;
            }
        }

        java.sql.Timestamp timestamp = Type.TimestampType.convert(args[0]);
        java.sql.Time time = Type.TimeType.convert(args[1]);

        Calendar cal = Calendar.getInstance();
        cal.setTime(timestamp);

        cal.add(Calendar.HOUR_OF_DAY, -1 * time.getHours());
        cal.add(Calendar.MINUTE, -1 * time.getMinutes());
        cal.add(Calendar.SECOND, -1 * time.getSeconds());
        Type type = getReturnType();
        return type.convert(cal.getTime());
    }

    public Type getReturnType() {
        return getFirstArgType();
    }

    public String[] getFunctionNames() {
        return new String[]{"SUBTIME"};
    }
}
