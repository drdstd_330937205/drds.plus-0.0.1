package drds.plus.executor.function.scalar.string;

import drds.plus.executor.ExecuteContext;
import drds.plus.executor.function.scalar.filter.Filter;

/**
 * 不支持
 *
 * @author mengshi.sunmengshi 2014年4月15日 下午5:44:59
 * @since 5.1.0
 */
public class SoundsLike extends Filter {

    public String[] getFunctionNames() {
        return new String[]{"SOUNDS like"};
    }

    protected Object computeInner(ExecuteContext executeContext, Object[] args) {

        throw new UnsupportedOperationException("如果没法下推，sounds like不支持");

    }

}
