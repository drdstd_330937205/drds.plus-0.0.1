package drds.plus.executor.function.scalar.datatime;

import drds.plus.executor.ExecuteContext;
import drds.plus.executor.function.scalar.ScalarFunction;
import drds.plus.sql_process.type.Type;


public class Now extends ScalarFunction {

    public Object compute(ExecuteContext executeContext, Object[] args) {
        Type type = getReturnType();
        return type.convert(new java.util.Date());
    }

    public Type getReturnType() {
        return Type.TimestampType;
    }

    public String[] getFunctionNames() {
        return new String[]{"NOW", "CURRENT_TIMESTAMP", "LOCALTIME", "LOCALTIMESTAMP", "SYSDATE"};
    }
}
