package drds.plus.executor.function.scalar;

import drds.plus.executor.ExecuteContext;
import drds.plus.sql_process.type.Type;


public class LastInsertId extends ScalarFunction {

    public Object compute(ExecuteContext executeContext, Object[] args) {
        return executeContext.getSession().getLastInsertId();
    }

    public Type getReturnType() {
        return Type.LongType;
    }

    public String[] getFunctionNames() {
        return new String[]{"LAST_INSERT_ID"};
    }
}
