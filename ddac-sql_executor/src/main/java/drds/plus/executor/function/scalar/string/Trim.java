package drds.plus.executor.function.scalar.string;

import drds.plus.common.utils.TStringUtil;
import drds.plus.executor.ExecuteContext;
import drds.plus.executor.function.scalar.ScalarFunction;
import drds.plus.executor.utils.Utils;
import drds.plus.sql_process.type.Type;

/**
 * <pre>
 * TRIM([{BOTH | LEADING | TRAILING} [remstr] FROM] str), TRIM([remstr FROM] str)
 *
 * Returns the string str with all remstr prefixes or suffixes removed. If none of the specifiers BOTH, LEADING, or TRAILING is given, BOTH is assumed. remstr is optional and, if not specified, spaces are removed.
 *
 * mysql> SELECT TRIM('  bar   ');
 *         -> 'bar'
 * mysql> SELECT TRIM(LEADING 'x' FROM 'xxxbarxxx');
 *         -> 'barxxx'
 * mysql> SELECT TRIM(BOTH 'x' FROM 'xxxbarxxx');
 *         -> 'bar'
 * mysql> SELECT TRIM(TRAILING 'xyz' FROM 'barxxyz');
 *         -> 'barx'
 *
 * </pre>
 *
 * @author mengshi.sunmengshi 2014年4月15日 下午4:07:06
 * @since 5.1.0
 */
public class Trim extends ScalarFunction {

    public Type getReturnType() {
        return Type.StringType;
    }

    public String[] getFunctionNames() {
        return new String[]{"TRIM"};
    }

    public Object compute(ExecuteContext executeContext, Object[] args) {

        for (Object arg : args) {
            if (Utils.isNull(arg)) {
                return null;
            }
        }
        String str = Type.StringType.convert(args[0]);

        if (args.length == 2) {
            str = str;
        }

        if (args.length == 3) {
            String trimStr = Type.StringType.convert(args[1]);
            String direction = Type.StringType.convert(args[2]);

            if ("BOTH".equals(direction)) {
                while (str.endsWith(trimStr)) {
                    str = TStringUtil.removeEnd(str, trimStr);
                }

                while (str.startsWith(trimStr)) {
                    str = TStringUtil.removeStart(str, trimStr);
                }
            } else if ("TRAILING".equals(direction)) {
                while (str.endsWith(trimStr)) {
                    str = TStringUtil.removeEnd(str, trimStr);
                }

            } else if ("LEADING".equals(direction)) {

                while (str.startsWith(trimStr)) {
                    str = TStringUtil.removeStart(str, trimStr);
                }

            }

        }

        return str;
    }
}
