package drds.plus.executor.function.scalar.filter.all;

import drds.plus.executor.ExecuteContext;
import drds.plus.executor.function.scalar.filter.Filter;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.Operation;
import drds.plus.sql_process.type.Type;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
public class NotEqualAll extends Filter {

    public String[] getFunctionNames() {
        return new String[]{Operation.not_equal_all.getOperationString()};
    }

    protected Object computeInner(ExecuteContext executeContext, Object[] args) {
        // 子查询没有被计算，返回自身
        if (!(args[1] instanceof List)) {
            return this;
        }

        Object left = args[0];
        List rights = (List) args[1];
        Type type = this.getArgType();
        for (Object right : rights) {
            if (type.compare(left, right) == 0) {
                return false;
            }
        }

        return true;
    }

}
