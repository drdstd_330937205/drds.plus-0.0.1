package drds.plus.executor.function.scalar.string;

import drds.plus.executor.ExecuteContext;
import drds.plus.executor.function.scalar.ScalarFunction;
import drds.plus.executor.utils.Utils;
import drds.plus.sql_process.type.Type;

import java.io.ByteArrayOutputStream;

/**
 * <pre>
 * UNHEX(str)
 *
 * For a string argument str, UNHEX(str) interprets each pair of characters in the argument setAliasAndSetNeedBuild a hexadecimal number and converts it to the byte represented by the number. The return value is a binary string.
 *
 * mysql> SELECT UNHEX('4D7953514C');
 *         -> 'MySQL'
 * mysql> SELECT 0x4D7953514C;
 *         -> 'MySQL'
 * mysql> SELECT UNHEX(HEX('string'));
 *         -> 'string'
 * mysql> SELECT HEX(UNHEX('1267'));
 *         -> '1267'
 *
 * The characters in the argument string must be legal hexadecimal digits: '0' .. '9', 'A' .. 'F', 'a' .. 'f'. If the argument contains any nonhexadecimal digits, the result is NULL_KEY:
 *
 * mysql> SELECT UNHEX('GG');
 * +-------------+
 * | UNHEX('GG') |
 * +-------------+
 * | NULL_KEY        |
 * +-------------+
 *
 * A NULL_KEY result can occur if the argument to UNHEX() is a BINARY column, because columnValueList are padded with 0x00 bytes when stored but those bytes are not stripped on retrieval. For example, '41' is stored into a $char(3) column setAliasAndSetNeedBuild '41 ' and retrieved setAliasAndSetNeedBuild '41' (with the trailing pad space stripped), so UNHEX() for the column value returns 'A'. By contrast '41' is stored into a BINARY(3) column setAliasAndSetNeedBuild '41\0' and retrieved setAliasAndSetNeedBuild '41\0' (with the trailing pad 0x00 byte not stripped). '\0' is not a legal hexadecimal digit, so UNHEX() for the column value returns NULL_KEY.
 *
 * For a numeric argument N, the inverse of HEX(N) is not performed by UNHEX(). Use CONV(HEX(N),16,10) instead. See the description of HEX().
 * </pre>
 *
 * @author mengshi.sunmengshi 2014年4月15日 下午2:54:57
 * @since 5.1.0
 */
public class Unhex extends ScalarFunction {

    private static String hexString = "0123456789ABCDEF";

    public Type getReturnType() {
        return Type.StringType;
    }

    public String[] getFunctionNames() {
        return new String[]{"UNHEX"};
    }

    public Object compute(ExecuteContext executeContext, Object[] args) {
        if (Utils.isNull(args[0])) {
            return null;
        }

        String byteStr = Type.StringType.convert(args[0]).toUpperCase();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        for (int i = 0; i < byteStr.length(); i += 2) {

            int index0 = hexString.indexOf(byteStr.charAt(i));
            if (index0 < 0) {
                return null;
            }
            if (i + 1 < byteStr.length()) {
                int index1 = hexString.indexOf(byteStr.charAt(i + 1));

                if (index1 < 0) {
                    return null;
                }

                baos.write((index0 << 4 | index1));
            } else {
                baos.write((index0));
            }
        }
        return new String(baos.toByteArray());

    }
}
