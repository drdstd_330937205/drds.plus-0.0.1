package drds.plus.executor.function.scalar.string;

/**
 * same setAliasAndSetNeedBuild lcase
 *
 * @author mengshi.sunmengshi 2014年4月11日 下午5:14:54
 * @since 5.1.0
 */
public class Lower extends Lcase {

    public String[] getFunctionNames() {
        return new String[]{"LOWER"};
    }

}
