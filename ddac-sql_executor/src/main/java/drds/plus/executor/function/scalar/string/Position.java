package drds.plus.executor.function.scalar.string;

/**
 * <pre>
 * POSITION(substr in str)
 *
 * POSITION(substr in str) is a synonym for LOCATE(substr,str).
 * </pre>
 *
 * @author mengshi.sunmengshi 2014年4月15日 下午2:37:28
 * @since 5.1.0
 */
public class Position extends Locate {

    public String[] getFunctionNames() {

        return new String[]{"POSITION"};
    }

}
