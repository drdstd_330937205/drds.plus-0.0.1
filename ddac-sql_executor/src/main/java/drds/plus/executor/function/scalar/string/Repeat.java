package drds.plus.executor.function.scalar.string;

import drds.plus.common.utils.TStringUtil;
import drds.plus.executor.ExecuteContext;
import drds.plus.executor.function.scalar.ScalarFunction;
import drds.plus.executor.utils.Utils;
import drds.plus.sql_process.type.Type;

/**
 * <pre>
 * REPEAT(str,count)
 *
 * Returns a string consisting of the string str repeated count times. If count is less than 1, returns an empty string. Returns NULL_KEY if str or count are NULL_KEY.
 *
 * mysql> SELECT REPEAT('MySQL', 3);
 *         -> 'MySQLMySQLMySQL'
 * </pre>
 *
 * @author mengshi.sunmengshi 2014年4月11日 下午6:39:43
 * @since 5.1.0
 */
public class Repeat extends ScalarFunction {

    public Type getReturnType() {
        return Type.StringType;
    }

    public String[] getFunctionNames() {
        return new String[]{"REPEAT"};
    }

    public Object compute(ExecuteContext executeContext, Object[] args) {
        for (Object arg : args) {
            if (Utils.isNull(arg)) {
                return null;
            }
        }
        String str = Type.StringType.convert(args[0]);
        Integer count = Type.IntegerType.convert(args[1]);
        if (count < 1) {
            return "";
        }

        return TStringUtil.repeat(str, count);

    }
}
