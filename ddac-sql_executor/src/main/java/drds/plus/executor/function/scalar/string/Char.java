package drds.plus.executor.function.scalar.string;

import drds.plus.executor.ExecuteContext;
import drds.plus.executor.function.scalar.ScalarFunction;
import drds.plus.executor.utils.Utils;
import drds.plus.sql_process.type.Type;

import java.util.ArrayList;
import java.util.List;

/**
 * http://dev.mysql.com/doc/refman/5.6/en/string-functions.html#function_char
 *
 * @author jianghang 2014-4-9 下午6:48:23
 * @since 5.0.7
 */
public class Char extends ScalarFunction {

    public Type getReturnType() {
        return Type.StringType;
    }

    public String[] getFunctionNames() {
        return new String[]{"$char"};
    }

    public Object compute(ExecuteContext executeContext, Object[] args) {
        List<Character> chs = new ArrayList<Character>();
        for (Object obj : args) {
            if (!Utils.isNull(obj)) {
                Long data = Type.LongType.convert(obj);
                chs.addAll(appendChars(data));
            }
        }

        if (chs.size() == 0) {
            return null;
        }

        StringBuilder builder = new StringBuilder();
        for (Character ch : chs) {
            builder.append(ch.charValue());
        }

        return builder.toString();
    }

    public List<Character> appendChars(Long data) {
        List<Character> chs = new ArrayList<Character>();
        while (true) {
            Character ch = Character.valueOf((char) (data % 256));
            chs.add(ch);

            if ((data >>= 8) == 0) {
                break;
            }
        }

        if (chs.size() <= 1) {
            return chs;
        }

        List<Character> result = new ArrayList<Character>();
        for (int i = chs.size() - 1; i >= 0; i--) {
            result.add(chs.get(i));
        }
        return result;
    }
}
