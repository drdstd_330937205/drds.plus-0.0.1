package drds.plus.executor.function.scalar.filter;

import drds.plus.executor.ExecuteContext;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;

@Slf4j
public class Row extends Filter {

    protected Object computeInner(ExecuteContext executeContext, Object[] args) {
        return new RowValueList(Arrays.asList(args));
    }

    public String[] getFunctionNames() {
        return new String[]{"row"};
    }

}
