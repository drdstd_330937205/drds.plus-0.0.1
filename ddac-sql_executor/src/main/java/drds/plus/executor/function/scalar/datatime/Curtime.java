package drds.plus.executor.function.scalar.datatime;

import drds.plus.executor.ExecuteContext;
import drds.plus.executor.function.scalar.ScalarFunction;
import drds.plus.sql_process.type.Type;

/**
 * Returns the currentJoinRowData time setAliasAndSetNeedBuild a value in 'HH:MM:SS' or HHMMSS.uuuuuu format,
 * depending on whether the function is used in a string or numeric context. The
 * value is expressed in the currentJoinRowData time zone.
 */
public class Curtime extends ScalarFunction {

    public Object compute(ExecuteContext executeContext, Object[] args) {
        Type type = getReturnType();
        return type.convert(new java.util.Date());
    }

    public Type getReturnType() {
        return Type.TimeType;
    }

    public String[] getFunctionNames() {
        return new String[]{"CURTIME", "CURRENT_TIME"};
    }
}
