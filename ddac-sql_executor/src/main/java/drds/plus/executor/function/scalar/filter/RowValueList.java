package drds.plus.executor.function.scalar.filter;

import java.util.List;

public class RowValueList {

    private List<Object> valueList;

    public RowValueList(List<Object> valueList) {
        this.valueList = valueList;
    }

    public List<Object> getValueList() {
        return valueList;
    }

    public void setValueList(List<Object> valueList) {
        this.valueList = valueList;
    }

}
