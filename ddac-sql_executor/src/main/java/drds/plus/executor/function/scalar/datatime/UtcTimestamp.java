package drds.plus.executor.function.scalar.datatime;

import drds.plus.executor.ExecuteContext;
import drds.plus.executor.function.scalar.ScalarFunction;
import drds.plus.sql_process.type.Type;

import java.util.TimeZone;


public class UtcTimestamp extends ScalarFunction {

    public Object compute(ExecuteContext executeContext, Object[] args) {
        Type type = getReturnType();
        long offest = TimeZone.getDefault().getRawOffset();
        return type.convert(System.currentTimeMillis() - offest);
    }

    public Type getReturnType() {
        return Type.TimestampType;
    }

    public String[] getFunctionNames() {
        return new String[]{"UTC_TIMESTAMP"};
    }

}
