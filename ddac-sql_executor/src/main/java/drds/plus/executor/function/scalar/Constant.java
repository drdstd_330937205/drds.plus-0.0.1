package drds.plus.executor.function.scalar;

import drds.plus.executor.ExecuteContext;
import drds.plus.sql_process.type.Type;

/**
 * 对应select 1中的1常量
 */
public class Constant extends ScalarFunction {

    public Type getReturnType() {
        return getFirstArgType();
    }

    public String[] getFunctionNames() {
        return new String[]{"constant"};
    }

    public Object compute(ExecuteContext executeContext, Object[] args) {
        return args[0];
    }

}
