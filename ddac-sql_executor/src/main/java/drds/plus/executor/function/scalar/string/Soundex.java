package drds.plus.executor.function.scalar.string;

import drds.plus.executor.ExecuteContext;
import drds.plus.executor.function.scalar.ScalarFunction;
import drds.plus.sql_process.type.Type;

/**
 * 不支持！太奇葩！
 *
 * @author mengshi.sunmengshi 2014年4月15日 下午5:40:09
 * @since 5.1.0
 */
public class Soundex extends ScalarFunction {

    public Type getReturnType() {
        return Type.StringType;
    }

    public String[] getFunctionNames() {
        return new String[]{"SOUNDEX"};
    }

    public Object compute(ExecuteContext executeContext, Object[] args) {
        throw new UnsupportedOperationException("如果没法下推，soundex不支持");

    }

}
