package drds.plus.executor.function.scalar;


import drds.plus.executor.ExecuteContext;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.extra_function.IExtraFunction;
import drds.plus.sql_process.type.Type;

/**
 * 假函数，不能参与任何运算。如果需要实现bdb的运算，需要额外的写实现放到map里，这个的作用就是mysql,直接发送下去的函数
 */
public class Dummy extends ScalarFunction implements IExtraFunction {

    public Type getReturnType() {
        return Type.UndecidedType;
    }

    public Object compute(ExecuteContext executeContext, Object[] args) {
        throw new RuntimeException("function " + this.function.getFunctionName());
    }

    public String[] getFunctionNames() {
        return new String[]{"dummy"};
    }

}
