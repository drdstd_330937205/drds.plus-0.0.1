package drds.plus.executor.function.scalar.datatime;

import drds.plus.executor.ExecuteContext;
import drds.plus.executor.function.scalar.ScalarFunction;
import drds.plus.sql_process.type.Type;

import java.util.TimeZone;

/**
 * @author jianghang 2014-4-16 下午10:27:52
 * @since 5.0.7
 */
public class UtcTime extends ScalarFunction {

    public Object compute(ExecuteContext executeContext, Object[] args) {
        Type type = getReturnType();
        long offest = TimeZone.getDefault().getRawOffset();
        return type.convert(System.currentTimeMillis() - offest);
    }

    public Type getReturnType() {
        return Type.TimeType;
    }

    public String[] getFunctionNames() {
        return new String[]{"UTC_TIME"};
    }

}
