package drds.plus.executor.function.scalar.datatime;

import drds.plus.executor.ExecuteContext;
import drds.plus.executor.function.scalar.ScalarFunction;
import drds.plus.executor.utils.Utils;
import drds.plus.sql_process.type.Type;

import java.util.Calendar;

/**
 * Returns the second for time, in the range 0 to 59.
 *
 * <pre>
 * mysql> SELECT SECOND('10:05:03');
 *         -> 3
 * </pre>
 */
public class Second extends ScalarFunction {

    public Object compute(ExecuteContext executeContext, Object[] args) {
        for (Object arg : args) {
            if (Utils.isNull(arg)) {
                return null;
            }
        }

        java.sql.Time time = Type.TimeType.convert(args[0]);
        Calendar cal = Calendar.getInstance();
        cal.setTime(time);

        return cal.get(Calendar.SECOND);
    }

    public Type getReturnType() {
        return Type.LongType;
    }

    public String[] getFunctionNames() {
        return new String[]{"SECOND"};
    }
}
