package drds.plus.executor.function.scalar.operator;

import drds.plus.executor.ExecuteContext;
import drds.plus.executor.function.scalar.ScalarFunction;
import drds.plus.sql_process.type.Type;

public class Multiply extends ScalarFunction {

    public Object compute(ExecuteContext executeContext, Object[] args) {
        Type type = this.getReturnType();
        return type.getCalculator().multiply(args[0], args[1]);
    }

    public Type getReturnType() {
        return getArgListTypeAndUseLongTypeWhenOccureNonNumberType();
    }

    public String[] getFunctionNames() {
        return new String[]{"*", "multiply"};
    }
}
