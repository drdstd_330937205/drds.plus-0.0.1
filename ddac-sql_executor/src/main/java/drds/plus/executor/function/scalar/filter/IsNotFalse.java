package drds.plus.executor.function.scalar.filter;

import drds.plus.executor.ExecuteContext;
import drds.plus.sql_process.type.Type;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class IsNotFalse extends Filter {

    protected Boolean computeInner(ExecuteContext executeContext, Object[] args) {
        Object arg = args[0];
        if (arg == null) {
            return false;
        }

        Long bool = Type.LongType.convert(arg);
        return (bool != 0) != false;
    }

    public String[] getFunctionNames() {
        return new String[]{"is NOT FALSE"};
    }

}
