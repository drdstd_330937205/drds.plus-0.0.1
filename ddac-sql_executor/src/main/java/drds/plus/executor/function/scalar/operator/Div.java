package drds.plus.executor.function.scalar.operator;

import drds.plus.executor.ExecuteContext;
import drds.plus.executor.function.scalar.ScalarFunction;
import drds.plus.sql_process.type.Type;

/**
 * 对应mysql的DIV函数，区别于/的出发
 *
 * <pre>
 * Integer division. Similar to FLOOR(), but is safe with BIGINT columnValueList.
 * In MySQL 5.6, if either operand has a noninteger type,
 * the expressionList are converted to DECIMAL and divided using DECIMAL arithmetic before converting the result to BIGINT.
 * If the result exceeds BIGINT range, an error occurs.
 *
 * mysql> SELECT 5 DIV 2;
 *         -> 2
 * </pre>
 */
public class Div extends ScalarFunction {

    public Object compute(ExecuteContext executeContext, Object[] args) {
        Type type = this.getReturnType();
        return type.getCalculator().divide(args[0], args[1]);
    }

    public Type getReturnType() {
        return Type.BigDecimalType;
    }

    public String[] getFunctionNames() {
        return new String[]{"/"};
    }
}
