package drds.plus.executor.function.scalar.string;

import drds.plus.executor.ExecuteContext;
import drds.plus.executor.function.scalar.ScalarFunction;
import drds.plus.executor.utils.Utils;
import drds.plus.sql_process.type.Type;
import drds.tools.$;

/**
 * <pre>
 * Returns the length of the string str, measured in bytes.
 * A multi-byte character counts setAliasAndSetNeedBuild multiple bytes.
 * This means that for a string containing five 2-byte characters, LENGTH() returns 10, whereas CHAR_LENGTH() returns 5.
 * </pre>
 *
 * @author mengshi.sunmengshi 2014年4月11日 下午5:17:48
 * @since 5.1.0
 */
public class Length extends ScalarFunction {

    public Type getReturnType() {
        return Type.IntegerType;
    }

    public String[] getFunctionNames() {
        return new String[]{"LENGTH", "OCTET_LENGTH"};
    }

    public Object compute(ExecuteContext executeContext, Object[] args) {
        Object arg = args[0];

        if (Utils.isNull(arg)) {
            return null;
        }

        String str = Type.StringType.convert(arg);

        if ($.isNullOrEmpty(str)) {
            return 0;
        }

        return str.getBytes().length;
    }

}
