package drds.plus.executor.function.scalar.operator;

import drds.plus.executor.ExecuteContext;
import drds.plus.executor.function.scalar.ScalarFunction;
import drds.plus.executor.utils.Utils;
import drds.plus.sql_process.type.Type;

/**
 * With two or more argList, returns the largest (maximum-valued) argument.
 * The argList are compared using the same rules setAliasAndSetNeedBuild for LEAST().
 *
 * <pre>
 * mysql> SELECT GREATEST(2,0);
 *         -> 2
 * mysql> SELECT GREATEST(34.0,3.0,5.0,767.0);
 *         -> 767.0
 * mysql> SELECT GREATEST('B','A','C');
 *         -> 'C'
 * </pre>
 */
public class Greatest extends ScalarFunction {

    public Object compute(ExecuteContext executeContext, Object[] args) {
        for (Object arg : args) {
            if (Utils.isNull(arg)) {
                return null;
            }
        }

        Type type = getReturnType();
        Object first = type.convert(args[0]);
        Object max = first;
        for (int i = 1; i < args.length; i++) {
            Object cp = type.convert(args[i]);
            max = (type.compare(max, cp) >= 0 ? max : cp);
        }

        return max;
    }

    public Type getReturnType() {
        return getArgListTypeAndUseStringTypeWhenOccureNonNumberType();
    }

    public String[] getFunctionNames() {
        return new String[]{"GREATEST"};
    }
}
