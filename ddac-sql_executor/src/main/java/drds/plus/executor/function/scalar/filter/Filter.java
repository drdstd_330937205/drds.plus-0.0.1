package drds.plus.executor.function.scalar.filter;

import drds.plus.executor.ExecuteContext;
import drds.plus.executor.function.scalar.ScalarFunction;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.Item;
import drds.plus.sql_process.type.Type;

public abstract class Filter extends ScalarFunction {

    Object result = false;

    protected abstract Object computeInner(ExecuteContext executeContext, Object[] args);

    public Object compute(ExecuteContext executeContext, Object[] args) {
        for (Object arg : args) {
            // 有一个没计算的，就直接返回
            if (arg instanceof Item) {
                return this;
            }
        }
        return this.computeInner(executeContext, args);
    }

    public Type getArgType() {
        return getFirstArgType();
    }

    public Type getReturnType() {
        return Type.BooleanType;
    }

}
