package drds.plus.executor.function.scalar.filter;

import drds.plus.executor.ExecuteContext;
import drds.plus.sql_process.type.Type;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class IsTrue extends Filter {

    protected Boolean computeInner(ExecuteContext executeContext, Object[] args) {
        Object arg = args[0];
        if (arg == null) {
            return false;
        }

        // mysql中数字1会被当作1处理,'true'字符串会被当作0处理
        Long bool = Type.LongType.convert(arg);
        return (bool != 0) == true;
    }

    public String[] getFunctionNames() {
        return new String[]{"is TRUE"};
    }

}
