package drds.plus.executor.function.scalar.string;

import drds.plus.executor.ExecuteContext;
import drds.plus.executor.function.scalar.ScalarFunction;
import drds.plus.executor.utils.Utils;
import drds.plus.sql_process.type.Type;

/**
 * <pre>
 * SUBSTRING(str,pos), SUBSTRING(str FROM pos), SUBSTRING(str,pos,len), SUBSTRING(str FROM pos FOR len)
 *
 * The forms without a len argument return a substring from string str starting at position pos. The forms with a len argument return a substring len characters long from string str, starting at position pos. The forms that use FROM are standard SQL parser. It is also possible to use a negative value for pos. In this case, the beginning of the substring is pos characters from the end of the string, rather than the beginning. A negative value may be used for pos in any of the forms of this function.
 *
 * For all forms of SUBSTRING(), the position of the firstInNewCursor character in the string from which the substring is to be extracted is reckoned setAliasAndSetNeedBuild 1.
 *
 * mysql> SELECT SUBSTRING('Quadratically',5);
 *         -> 'ratically'
 * mysql> SELECT SUBSTRING('foobarbar' FROM 4);
 *         -> 'barbar'
 * mysql> SELECT SUBSTRING('Quadratically',5,6);
 *         -> 'ratica'
 * mysql> SELECT SUBSTRING('Sakila', -3);
 *         -> 'ila'
 * mysql> SELECT SUBSTRING('Sakila', -5, 3);
 *         -> 'aki'
 * mysql> SELECT SUBSTRING('Sakila' FROM -4 FOR 2);
 *         -> 'ki'
 *
 * If len is less than 1, the result is the empty string.
 * </pre>
 *
 * @author mengshi.sunmengshi 2014年4月15日 下午1:39:12
 * @since 5.1.0
 */
public class SubString extends ScalarFunction {

    public Type getReturnType() {
        return Type.StringType;
    }

    public String[] getFunctionNames() {
        return new String[]{"SUBSTRING"};
    }

    public Object compute(ExecuteContext executeContext, Object[] args) {
        if (Utils.isNull(args[0])) {
            return null;
        }

        String str = Type.StringType.convert(args[0]);

        Integer pos = Type.IntegerType.convert(args[1]);

        Integer len = null;

        if (args.length == 3) {
            len = Type.IntegerType.convert(args[2]);
        }

        if (pos < 0) {
            pos = str.length() + pos + 1;
        }
        if (len == null) {
            return str.substring(pos - 1);
        } else {

            if (len < 1) {
                return "";

            } else {
                return str.substring(pos - 1, pos - 1 + len);
            }
        }

    }
}
