package drds.plus.executor.function.scalar.string;

import drds.plus.executor.ExecuteContext;
import drds.plus.executor.function.scalar.ScalarFunction;
import drds.plus.executor.utils.Utils;
import drds.plus.sql_process.type.Type;

/**
 * <pre>
 * insert(str,pos,len,newstr)
 *
 * Returns the string str, with the substring beginning at position pos and len characters long replaced by the string newstr. Returns the original string if pos is not within the length of the string. Replaces the rest of the string from position pos if len is not within the length of the rest of the string. Returns NULL_KEY if any argument is NULL_KEY.
 *
 * mysql> SELECT insert('Quadratic', 3, 4, 'What');
 *         -> 'QuWhattic'
 * mysql> SELECT insert('Quadratic', -1, 4, 'What');
 *         -> 'Quadratic'
 * mysql> SELECT insert('Quadratic', 3, 100, 'What');
 *         -> 'QuWhat'
 * </pre>
 *
 * @author mengshi.sunmengshi 2014年4月11日 下午4:17:42
 * @since 5.1.0
 */
public class Insert extends ScalarFunction {

    public Type getReturnType() {
        return Type.StringType;
    }

    public String[] getFunctionNames() {
        return new String[]{"insert"};
    }

    public Object compute(ExecuteContext executeContext, Object[] args) {
        for (Object arg : args) {
            if (Utils.isNull(arg)) {
                return null;

            }
        }

        String str1 = Type.StringType.convert(args[0]);

        Integer pos = Type.IntegerType.convert(args[1]);

        Integer len = Type.IntegerType.convert(args[2]);

        String str2 = Type.StringType.convert(args[3]);

        if (pos <= 0 || pos > str1.length()) {
            return str1;
        }
        StringBuilder newStr = new StringBuilder();
        if (pos + len > str1.length()) {

            newStr.append(str1, 0, pos - 1);
            newStr.append(str2);

            return newStr.toString();
        } else {
            newStr.append(str1, 0, pos - 1);
            newStr.append(str2);
            newStr.append(str1.substring(pos + len - 1));

            return newStr.toString();
        }

    }

}
