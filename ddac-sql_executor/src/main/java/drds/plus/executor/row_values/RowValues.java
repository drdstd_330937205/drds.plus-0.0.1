package drds.plus.executor.row_values;

import drds.plus.executor.cursor.cursor_metadata.CursorMetaData;

/**
 * 数据的核心接口，类似ResultSet一样的接口。下面有可能有join的实现，也可能有普通query或Merge实现。
 */
public interface RowValues extends drds.plus.common.model.RowValues {

    CursorMetaData getParentCursorMetaData();
}
