package drds.plus.executor.row_values;

import drds.plus.executor.cursor.cursor_metadata.CursorMetaData;

import java.util.Arrays;
import java.util.List;

/**
 * 基于数组的结果集。是最基本的一行数据的形式，效率最快。
 */
public class ArrayRowValues extends AbstractRowValues implements RowValues {

    Object[] objects;


    public ArrayRowValues(int capacity, CursorMetaData cursorMetaData) {
        super(cursorMetaData);
        objects = new Object[capacity];
    }

    public ArrayRowValues(CursorMetaData cursorMetaData, Object[] objects) {
        super(cursorMetaData);
        this.objects = objects;
    }


    public Object getObject(int index) {
        return objects[index];
    }

    public void setObject(int index, Object value) {
        objects[index] = value;
    }

    public List<Object> getValueList() {
        return Arrays.asList(objects);
    }

    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Arrays.hashCode(objects);
        return result;
    }

    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ArrayRowValues other = (ArrayRowValues) obj;
        return Arrays.equals(objects, other.objects);
    }

}
