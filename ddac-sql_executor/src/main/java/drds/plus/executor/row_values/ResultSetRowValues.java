package drds.plus.executor.row_values;


import drds.plus.executor.cursor.cursor_metadata.CursorMetaData;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


public class ResultSetRowValues extends AbstractRowValues implements RowValues {

    ResultSet resultSet = null;

    public ResultSetRowValues(CursorMetaData meta, ResultSet resultSet) {
        super(meta);
        this.resultSet = resultSet;
    }

    public Object getObject(int index) {
        try {
            Object object = resultSet.getObject(index + 1);
            if (object != null) {
                return object;
            }
            if (resultSet.wasNull()) {
                return null;
            }
            return object;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void setObject(int index, Object value) {
        throw new UnsupportedOperationException("ResultSetRowValues.setObject()");
    }

    public List<Object> getValueList() {
        List<Object> objectList = new ArrayList<Object>();
        for (int i = 0; i < getParentCursorMetaData().getColumnMetaDataList().size(); i++) {
            objectList.add(this.getObject(i));
        }
        return objectList;
    }

    public Boolean getBoolean(int index) {
        try {

            boolean aBoolean = resultSet.getBoolean(index + 1);
            if (resultSet.wasNull()) {
                return null;
            }
            return aBoolean;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public Date getDate(int index) {
        try {

            return resultSet.getDate(index + 1);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public Double getDouble(int index) {
        try {

            double aDouble = resultSet.getDouble(index + 1);
            if (resultSet.wasNull()) {
                return null;
            } else {
                return aDouble;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public Float getFloat(int index) {
        try {

            float aFloat = resultSet.getFloat(index + 1);
            if (resultSet.wasNull()) {
                return null;
            } else {
                return aFloat;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public Integer getInteger(int index) {
        try {

            int anInt = resultSet.getInt(index + 1);
            if (resultSet.wasNull()) {
                return null;
            }
            return anInt;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public Long getLong(int index) {
        try {

            long aLong = resultSet.getLong(index + 1);
            if (resultSet.wasNull()) {
                return null;
            }
            return aLong;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public Short getShort(int index) {
        try {

            short aShort = resultSet.getShort(index + 1);
            if (resultSet.wasNull()) {
                return null;
            }
            return aShort;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    public String getString(int index) {
        try {

            return resultSet.getString(index + 1);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public Timestamp getTimestamp(int index) {
        try {
            return resultSet.getTimestamp(index + 1);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


}
