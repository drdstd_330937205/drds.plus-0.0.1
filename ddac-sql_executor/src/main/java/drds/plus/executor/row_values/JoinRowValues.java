package drds.plus.executor.row_values;

import drds.plus.executor.cursor.cursor_metadata.CursorMetaData;

import java.util.ArrayList;
import java.util.List;


public class JoinRowValues extends AbstractRowValues implements RowValues {

    private int rightCursorOffset;
    private RowValues leftRowData;
    private RowValues rightRowData;

    public JoinRowValues(int rightCursorOffset, RowValues leftRowData, RowValues rightRowData, CursorMetaData cursorMetaData) {
        super(cursorMetaData);
        this.rightCursorOffset = rightCursorOffset;
        this.leftRowData = leftRowData;
        this.rightRowData = rightRowData;
    }

    public int getRightCursorOffset() {
        return rightCursorOffset;
    }

    public void setRightCursorOffset(int rightCursorOffset) {
        this.rightCursorOffset = rightCursorOffset;
    }

    public RowValues getLeftRowData() {
        return leftRowData;
    }

    public void setLeftRowData(RowValues leftRowData) {
        this.leftRowData = leftRowData;
    }

    public RowValues getRightRowData() {
        return rightRowData;
    }

    public void setRightRowData(RowValues rightRowData) {
        this.rightRowData = rightRowData;
    }

    public Object getObject(int index) {
        if (rightCursorOffset <= index) {
            if (rightRowData == null)
                return null;
            return rightRowData.getObject(index - rightCursorOffset);
        }

        if (leftRowData == null)
            return null;
        return leftRowData.getObject(index);
    }

    public void setObject(int index, Object value) {
        if (rightCursorOffset <= index) {
            rightRowData.setObject(index - rightCursorOffset, value);
            return;
        }
        leftRowData.setObject(index, value);
    }

    public List<Object> getValueList() {
        ArrayList<Object> values = new ArrayList<Object>();
        if (leftRowData == null && rightRowData == null) {
            int size = this.getParentCursorMetaData().getColumnMetaDataList().size();
            for (int i = 0; i < size; i++) {
                values.add(null);
            }
        } else if (rightRowData == null) {
            values.addAll(leftRowData.getValueList());
            for (int i = 0; i < this.getParentCursorMetaData().getColumnMetaDataList().size() - leftRowData.getValueList().size(); i++)
                values.add(null);

        } else if (leftRowData == null) {

            for (int i = 0; i < this.getParentCursorMetaData().getColumnMetaDataList().size() - rightRowData.getValueList().size(); i++)
                values.add(null);
            values.addAll(rightRowData.getValueList());

        } else {
            values.addAll(leftRowData.getValueList());
            values.addAll(rightRowData.getValueList());
        }
        return values;
    }

}
