package drds.plus.executor.row_values.row_value;

import drds.plus.executor.row_values.RowValues;

import java.util.Iterator;
import java.util.List;

public class RowValueIteratorImpl implements Iterator<Object> {

    private final RowValues rowData;
    private List<Integer> indexList;
    private int index;
    private int indexListSize;

    public RowValueIteratorImpl(List<Integer> indexList, RowValues rowData) {
        super();
        this.indexList = indexList;
        indexListSize = indexList.size();
        this.rowData = rowData;
    }

    public boolean hasNext() {
        return index < indexListSize;
    }

    public Object next() {
        Integer rowDataIndex = indexList.get(index);
        Object object = rowData.getObject(rowDataIndex);
        index++;
        return object;
    }

    public void remove() {
        throw new UnsupportedOperationException();

    }

}
