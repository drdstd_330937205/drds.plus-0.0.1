package drds.plus.executor.row_values;

import drds.plus.executor.cursor.cursor_metadata.CursorMetaData;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.List;

/**
 * 可以用来给列改名，去除一个列
 */
public class RowValuesWrapper extends AbstractRowValues implements RowValues {

    protected final CursorMetaData cursorMetaData;
    protected RowValues rowData;

    public RowValuesWrapper(CursorMetaData cursorMetaData, RowValues rowData) {
        super(cursorMetaData);
        this.cursorMetaData = cursorMetaData;
        this.rowData = rowData;
    }

    public Object getObject(int index) {
        return rowData.getObject(index);
    }

    public void setObject(int index, Object value) {
        rowData.setObject(index, value);
    }

    public Integer getInteger(int index) {
        return rowData.getInteger(index);
    }

    public void setInteger(int index, Integer value) {
        rowData.setInteger(index, value);
    }

    public Long getLong(int index) {
        return rowData.getLong(index);
    }

    public void setLong(int index, Long value) {
        rowData.setLong(index, value);
    }

    public List<Object> getValueList() {
        return rowData.getValueList();
    }

    public CursorMetaData getParentCursorMetaData() {
        return cursorMetaData;
    }

    public String getString(int index) {
        return rowData.getString(index);
    }

    public void setString(int index, String str) {
        rowData.setString(index, str);
    }

    public Boolean getBoolean(int index) {
        return rowData.getBoolean(index);
    }

    public void setBoolean(int index, Boolean bool) {
        rowData.setBoolean(index, bool);
    }

    public Short getShort(int index) {
        return rowData.getShort(index);
    }

    public void setShort(int index, Short shortval) {
        rowData.setShort(index, shortval);
    }

    public Float getFloat(int index) {
        return rowData.getFloat(index);
    }

    public void setFloat(int index, Float fl) {
        rowData.setFloat(index, fl);
    }

    public Double getDouble(int index) {
        return rowData.getDouble(index);
    }

    public void setDouble(int index, Double doub) {
        rowData.setDouble(index, doub);
    }


    public Date getDate(int index) {
        return rowData.getDate(index);
    }

    public void setDate(int index, Date date) {
        rowData.setDate(index, date);
    }

    public Timestamp getTimestamp(int index) {
        return rowData.getTimestamp(index);
    }

    public void setTimestamp(int index, Timestamp timestamp) {
        rowData.setTimestamp(index, timestamp);
    }

    public Time getTime(int index) {
        return rowData.getTime(index);
    }

    public void setTime(int index, Time time) {
        rowData.setTime(index, time);
    }

    public BigDecimal getBigDecimal(int index) {
        return rowData.getBigDecimal(index);
    }

    public void setBigDecimal(int index, BigDecimal bigDecimal) {
        rowData.setBigDecimal(index, bigDecimal);
    }


    public RowValues getRowData() {
        return rowData;
    }

    public void setRowData(RowValues rowData) {
        this.rowData = rowData;
    }

}
