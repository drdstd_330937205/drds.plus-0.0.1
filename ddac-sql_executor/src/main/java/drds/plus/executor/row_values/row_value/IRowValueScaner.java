package drds.plus.executor.row_values.row_value;

import drds.plus.executor.row_values.RowValues;

import java.util.Iterator;

/**
 * 单行结果遍历用。 大部分情况下，使用index比使用colName效率高，所以这个scaner主要作用就是 缓存一行中取到数据的index.加速访问
 */
public interface IRowValueScaner {


    /**
     * 非线程安全哦。。 返回数据的rowSet;
     */
    Iterator<Object> rowDataValueIterator(RowValues rowData);
}
