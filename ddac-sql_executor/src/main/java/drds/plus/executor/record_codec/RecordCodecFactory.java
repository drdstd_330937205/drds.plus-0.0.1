package drds.plus.executor.record_codec;

import drds.plus.sql_process.abstract_syntax_tree.configuration.ColumnMetaData;

import java.util.List;

public class RecordCodecFactory {

    public static RecordCodec newRecordCodec(List<ColumnMetaData> columnMetaDataList) {
        return new FixedLengthRecordCodec(columnMetaDataList);
    }

}
