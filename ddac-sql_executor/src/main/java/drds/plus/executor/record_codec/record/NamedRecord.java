package drds.plus.executor.record_codec.record;

import drds.plus.sql_process.type.Type;

import java.util.List;
import java.util.Map;


public class NamedRecord extends Record {

    String name;
    Record record;

    public NamedRecord(String name, Record record) {
        this.name = name;
        this.record = record;
    }

    public boolean equals(Object obj) {
        if (record != null) {
            return record.equals(obj);
        } else {
            return super.equals(obj);
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public IRecord getRecord() {
        return record;
    }

    public Object clone() {
        NamedRecord namedRecord = (NamedRecord) super.clone();
        if (namedRecord.record != null) {
            namedRecord.record = (Record) namedRecord.record.clone();
        }

        return namedRecord;
    }

    public void setValue(int index, Object value) {
        record.setValue(index, value);
    }

    public void putAll(Map<String, Object> map) {
        record.putAll(map);
    }

    public void put(String key, Object value) {
        record.put(key, value);
    }

    public List<Object> getValueList() {
        return record.getValueList();
    }

    public Object getValue(int index) {
        return record.getValue(index);
    }

    public Map<String, Object> getColumnNameToValueMap() {
        return record.getColumnNameToValueMap();
    }

    public Map<String, Integer> getColumnNameToIndexMap() {
        return record.getColumnNameToIndexMap();
    }

    public List<String> getColumnNameList() {
        return record.getColumnNameList();
    }

    public Object get(String key) {
        return record.get(key);
    }

    public void addAll(List<Object> valueList) {
        record.addAll(valueList);
    }

    public int compareTo(IRecord o) {
        return record.compareTo(((NamedRecord) o).record);
    }

    public String toString() {
        return "NamedRecord{" + "columnName=" + name + ", record=" + record + '}';
    }


    public int hashCode() {
        if (record != null) {
            return record.hashCode();
        } else {
            return super.hashCode();
        }
    }

    public Type getValueType(int index) {
        return record.getValueType(index);
    }

    public Type getValueType(String columnName) {
        return record.getValueType(columnName);
    }

}
