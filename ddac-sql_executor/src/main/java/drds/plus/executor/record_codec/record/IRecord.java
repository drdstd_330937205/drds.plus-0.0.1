package drds.plus.executor.record_codec.record;

import drds.plus.sql_process.type.Type;

import java.util.List;
import java.util.Map;


public interface IRecord extends Comparable<IRecord> {

    /**
     * 塞入一个值
     */
    void put(String key, Object value);

    /**
     * 获取一个值
     */
    Object get(String key);

    /**
     * 塞入所有值
     */
    void putAll(Map<String, Object> map);

    /**
     * 使用index 获取值，理论上来说，效率最高，因为使用下标
     */
    Object getValue(int index);

    /**
     * 设置某个值,理论上来说效率最高
     */
    void setValue(int index, Object value);

    void addAll(List<Object> valueList);


    Map<String, Integer> getColumnNameToIndexMap();

    /**
     * 遍历ColumnList
     */
    List<String> getColumnNameList();

    List<Object> getValueList();

    Map<String, Object> getColumnNameToValueMap();

    Type getValueType(int index);

    Type getValueType(String columnName);

}
