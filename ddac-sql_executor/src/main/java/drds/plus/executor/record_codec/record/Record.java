package drds.plus.executor.record_codec.record;

import drds.plus.executor.record_codec.RecordCodecFactory;

import java.util.Collections;

/**
 * CloneableRecord by czh
 */
public abstract class Record implements IRecord, Cloneable {

    public static Record getNewEmptyRecord() {
        return RecordCodecFactory.newRecordCodec(Collections.EMPTY_LIST).newRecord();
    }

    public abstract Object get(String key);


    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException ex) {
            throw new RuntimeException("Clone not supported: " + ex.getMessage());
        }
    }

}
