package drds.plus.executor.record_codec.record;


public class KeyValueRecordPair implements Comparable, Cloneable {

    private Record key;
    private Record value;

    public KeyValueRecordPair() {
        key = Record.getNewEmptyRecord();
        value = Record.getNewEmptyRecord();
    }

    public KeyValueRecordPair(Record key, Record value) {

        if (key != null) {
            this.key = key;
        } else {
            this.key = Record.getNewEmptyRecord();
        }
        if (value != null) {
            this.value = value;
        } else {
            this.value = Record.getNewEmptyRecord();
        }
    }

    public Record getKey() {
        return key;
    }

    public void setKey(Record key) {
        this.key = key;
    }

    public Record getValue() {
        return value;
    }

    public void setValue(Record value) {
        this.value = value;
    }

    public Object clone() {
        KeyValueRecordPair o = null;
        try {
            o = (KeyValueRecordPair) super.clone();
            if (key != null) {
                o.key = (Record) key.clone();
            }
            if (value != null) {
                o.value = (Record) value.clone();
            }
        } catch (CloneNotSupportedException ex) {
            throw new RuntimeException("Clone not supported: " + ex.getMessage());
        }
        return o;
    }

    public String toString() {
        return "KeyValueRecordPair{" + "columnName=" + key + ", value=" + value + '}';
    }

    public int compareTo(Object o) {
        return key.compareTo(((KeyValueRecordPair) o).key);
    }
}
