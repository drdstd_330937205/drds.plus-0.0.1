package drds.plus.executor.record_codec;

import drds.plus.executor.record_codec.record.Record;

public interface RecordCodec<T> {

    byte[] encode(Record record);

    Record decode(byte[] bytes);

    Record newRecord();

}
