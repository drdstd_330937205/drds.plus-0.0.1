package drds.plus.executor.transaction.strict_write_with_non_transaction_cross_database_read;

import drds.plus.datanode.api.Connection;
import drds.plus.datanode.api.DatasourceManager;
import drds.plus.executor.transaction.AbstractConnectionHolder;
import drds.plus.executor.transaction.ConnectionHolder;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

@Slf4j
public class StrictWriteWithNonTransactionCrossDatabaseReadConnectionHolder extends AbstractConnectionHolder {
    public Logger log() {
        return log;
    }

    private drds.plus.executor.transaction.ConnectionHolder connectionHolder2;
    private drds.plus.executor.transaction.ConnectionHolder connectionHolder1;

    public StrictWriteWithNonTransactionCrossDatabaseReadConnectionHolder(ConnectionHolder connectionHolder1, ConnectionHolder connectionHolder2) {
        this.connectionHolder1 = connectionHolder1;
        this.connectionHolder2 = connectionHolder2;
    }

    public Set<Connection> getConnectionSet() {

        Set<Connection> connectionSet = new HashSet<Connection>();
        connectionSet.addAll(connectionHolder1.getConnectionSet());
        connectionSet.addAll(connectionHolder2.getConnectionSet());

        return connectionSet;
    }

    public Connection getConnection(String dataNodeId, DatasourceManager datasourceManager) throws SQLException {
        throw new UnsupportedOperationException();
    }

    public void tryClose(String dataNodeId, Connection connection) throws SQLException {
        throw new UnsupportedOperationException();

    }

    public void kill() {
        connectionHolder1.kill();
        connectionHolder2.kill();
    }

    public void cancel() {
        connectionHolder1.cancel();
        connectionHolder2.cancel();
    }

}
