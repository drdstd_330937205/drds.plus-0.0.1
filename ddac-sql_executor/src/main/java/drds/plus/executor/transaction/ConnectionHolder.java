package drds.plus.executor.transaction;

import drds.plus.datanode.api.Connection;
import drds.plus.datanode.api.DatasourceManager;

import java.sql.SQLException;
import java.util.Set;

public interface ConnectionHolder {


    Set<Connection> getConnectionSet();

    void closeConnectionSet();

    Connection getConnection(String dataNodeId, DatasourceManager datasourceManager) throws SQLException;

    void tryClose(String dataNodeId, Connection connection) throws SQLException;

    void kill();

    void cancel();

    void restart();
}
