package drds.plus.executor.transaction.strict_write_with_non_transaction_cross_database_read;

public enum ReadWrite {
    read, write
}
