package drds.plus.executor.transaction.non_transaction_and_close_connection_when_commit;

import drds.plus.datanode.api.Connection;
import drds.plus.datanode.api.DatasourceManager;
import drds.plus.executor.transaction.AbstractConnectionHolder;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;

import java.sql.SQLException;

@Slf4j
public class NonTransactionAndCloseConnectionWhenCommitConnectionHolder extends AbstractConnectionHolder {
    public Logger log() {
        return log;
    }

    public NonTransactionAndCloseConnectionWhenCommitConnectionHolder() {

    }

    public Connection getConnection(String dataNodeId, DatasourceManager datasourceManager) throws SQLException {

        Connection connection = datasourceManager.getConnection();
        this.connectionSet.add(connection);
        return connection;

    }

    public void tryClose(String dataNodeId, Connection connection) throws SQLException {
        connection.close();
        this.connectionSet.remove(connection);
    }

}
