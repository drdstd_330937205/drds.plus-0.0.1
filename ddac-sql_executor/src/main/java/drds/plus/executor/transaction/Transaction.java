package drds.plus.executor.transaction;

import drds.plus.datanode.api.Connection;
import drds.plus.datanode.api.DatasourceManager;
import drds.plus.executor.ExecuteContext;
import drds.plus.executor.transaction.strict_write_with_non_transaction_cross_database_read.ReadWrite;

import java.sql.SQLException;

public interface Transaction {

    long getId();

    void setExecuteContext(ExecuteContext executeContext);

    drds.plus.executor.transaction.ConnectionHolder getConnectionHolder();

    Connection getConnection(String dataNodeId, DatasourceManager datasourceManager, ReadWrite readWrite) throws SQLException;

    void commit() throws RuntimeException;

    void rollback() throws RuntimeException;

    void close();

    boolean isClosed();

    void tryClose(String dataNodeId, Connection connection) throws SQLException;

    void tryClose() throws SQLException;

    void kill() throws SQLException;

    void cancel() throws SQLException;

}
