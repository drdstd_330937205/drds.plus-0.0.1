package drds.plus.executor.repository;

import java.util.Map;


public interface IRepositoryFactory {
    String mysql_repository_factory = "drds.plus.repository.mysql.spi.RepositoryFactory";
    String berkeley_repository_factory = "drds.plus.repository.berkeley.spi.RepositoryFactory";

    Repository buildRepository(Map repoProperties, Map connectionProperties);

}
