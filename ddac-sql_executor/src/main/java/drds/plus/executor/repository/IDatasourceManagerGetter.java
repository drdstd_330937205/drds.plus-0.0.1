package drds.plus.executor.repository;

import drds.plus.datanode.api.DatasourceManager;

/**
 * 从不同的存储中拿到相应的DataSource
 */
public interface IDatasourceManagerGetter {

    DatasourceManager getDatasourceManager(String dataNodeId);
}
