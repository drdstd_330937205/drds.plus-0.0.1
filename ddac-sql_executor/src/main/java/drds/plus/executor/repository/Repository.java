package drds.plus.executor.repository;

import drds.plus.common.lifecycle.Lifecycle;
import drds.plus.common.model.DataNode;
import drds.plus.executor.IExecutor;
import drds.plus.executor.command_handler.CommandHandlerFactory;
import drds.plus.executor.cursor.cursor_factory.CursorFactory;
import drds.plus.executor.data_node_executor.spi.DataNodeExecutor;
import drds.plus.executor.table.ITable;
import drds.plus.executor.table.ITemporaryTable;
import drds.plus.sql_process.abstract_syntax_tree.configuration.TableMetaData;

/**
 * 每个存储一个，主入口
 */
public interface Repository extends Lifecycle {

    /**
     * 获取对应的表结构
     */
    ITemporaryTable getTemporaryTable(TableMetaData tableMetaData) throws RuntimeException;

    /**
     * 获取一个表对象 ，在任何sql操作中都会根据table schema找到对应的数据库实例对象的。 表对象包含核心数据和对应的二级索引。
     */
    ITable getTable(String dataNodeId, TableMetaData tableMetaData) throws RuntimeException;

    /**
     * 获取当前存储引擎的一些配置信息。
     */
    RepositoryConfig getRepositoryConfig();


    /**
     * cursor实现类
     */
    CursorFactory getCursorFactory();

    /**
     * 获取对应的 handler构造器
     */
    CommandHandlerFactory getCommandExecutorFactory();

    /**
     * 获取对应的group {@link IExecutor}
     */
    DataNodeExecutor getDataNodeExecutor(DataNode dataNode);
}
