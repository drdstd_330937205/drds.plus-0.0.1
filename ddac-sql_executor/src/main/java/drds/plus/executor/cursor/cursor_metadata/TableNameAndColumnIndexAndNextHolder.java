package drds.plus.executor.cursor.cursor_metadata;


public class TableNameAndColumnIndexAndNextHolder {
    /**
     * 表名
     */
    public String tableName = null;
    /**
     * indexName
     */
    public Integer columnIndex = null;
    /**
     * 如果有同列名，不同表名，放这里， 预计不会出现很多这样的情况。
     */
    public TableNameAndColumnIndexAndNextHolder next = null;

    public TableNameAndColumnIndexAndNextHolder(String tableName, Integer columnIndex, TableNameAndColumnIndexAndNextHolder next) {
        super();
        this.tableName = tableName;
        this.columnIndex = columnIndex;
        this.next = next;
    }

}
