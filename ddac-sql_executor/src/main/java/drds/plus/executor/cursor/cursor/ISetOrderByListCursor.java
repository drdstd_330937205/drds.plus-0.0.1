package drds.plus.executor.cursor.cursor;

/**
 * setLimitValue request order by when cursor's addOrderByItemAndSetNeedBuild tableName is not equals request
 * addOrderByItemAndSetNeedBuild tableName
 */
public interface ISetOrderByListCursor extends ISortingCursor {

}
