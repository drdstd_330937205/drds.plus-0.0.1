package drds.plus.executor.cursor.cursor.impl;

import drds.plus.executor.row_values.RowValues;


/**
 * 一组重复的值的链表。 所有在同一个链表内的数据，都具备一组相同的key(key的意思就是在IRowSet中用来作为排序字段的哪些数据)
 */
public class DuplicateValueRowDataLinkedList {

    public RowValues rowData;
    public DuplicateValueRowDataLinkedList next = null;

    public DuplicateValueRowDataLinkedList(RowValues rowData) {
        super();
        this.rowData = rowData;

    }
}
