package drds.plus.executor.cursor.cursor;

import drds.plus.sql_process.abstract_syntax_tree.expression.order_by.OrderBy;

import java.util.List;

/**
 * 支持排序的的Cursor
 */
public interface ISortingCursor extends Cursor {

    List<OrderBy> getOrderByList() throws RuntimeException;

    /**
     * join查询可能存在多个可能的排序，比如sort sort join，会是左表或者右表的join列
     */
    List<List<OrderBy>> getJoinOrderByListList() throws RuntimeException;
}
