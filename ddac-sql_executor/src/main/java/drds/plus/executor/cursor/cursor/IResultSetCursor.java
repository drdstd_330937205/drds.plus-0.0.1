package drds.plus.executor.cursor.cursor;

import java.sql.ResultSet;

public interface IResultSetCursor {

    ResultSet getResultSet();
}
