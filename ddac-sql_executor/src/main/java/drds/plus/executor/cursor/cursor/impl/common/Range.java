package drds.plus.executor.cursor.cursor.impl.common;

import drds.plus.executor.row_values.RowValues;

public class Range {

    public RowValues from;
    public RowValues to;
}
