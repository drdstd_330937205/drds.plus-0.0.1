package drds.plus.executor.cursor.cursor;

import java.util.List;

public interface IMergeCursor extends ISortingCursor {

    List<ISortingCursor> getSortingCursorList();
}
