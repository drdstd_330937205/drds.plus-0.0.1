package drds.plus.executor.cursor.cursor.impl.join;

enum IteratorDirection {
    FORWARD, BACKWARD
}
