package drds.plus.executor.cursor.cursor.impl.common;

import drds.plus.executor.cursor.cursor.Cursor;
import drds.plus.executor.cursor.cursor.ISetOrderByListCursor;
import drds.plus.executor.cursor.cursor.impl.SortingCursor;
import drds.plus.executor.cursor.cursor_metadata.CursorMetaData;
import drds.plus.executor.utils.Utils;
import drds.plus.sql_process.abstract_syntax_tree.expression.order_by.OrderBy;
import drds.plus.util.GeneralUtil;

import java.util.List;

public class SetOrderByListCursor extends SortingCursor implements ISetOrderByListCursor {

    public SetOrderByListCursor(Cursor cursor, CursorMetaData cursorMetaData, List<OrderBy> orderByList) {
        super(cursor, null, orderByList);
    }

    public String toString() {
        return toStringWithInden(0);
    }

    public String toStringWithInden(int inden) {
        StringBuilder sb = new StringBuilder();
        String tab = GeneralUtil.getTab(inden);
        sb.append(tab).append("【setLimitValue order cursor .").append("\n");
        Utils.printOrderByList(orderByList, inden, sb);
        sb.append(super.toStringWithInden(inden));
        return sb.toString();
    }
}
