package drds.plus.executor.cursor.cursor.impl;


import drds.plus.executor.cursor.cursor.Cursor;
import drds.plus.executor.cursor.cursor.ISortingCursor;
import drds.plus.executor.cursor.cursor_metadata.CursorMetaData;
import drds.plus.executor.record_codec.record.KeyValueRecordPair;
import drds.plus.executor.record_codec.record.Record;
import drds.plus.executor.row_values.RowValues;
import drds.plus.sql_process.abstract_syntax_tree.configuration.ColumnMetaData;
import drds.plus.sql_process.abstract_syntax_tree.expression.order_by.OrderBy;
import drds.plus.util.GeneralUtil;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * 所有cursor的抽象基类，以前的作用是存放了当前cursor的schema信息和order by信息。 现在只存放order
 * by信息了。 schema因为可能会随着IRowSet动态变更，所以不在这里使用了。
 */
public class SortingCursor extends AbstractCursor implements ISortingCursor {

    public List<OrderBy> orderByList = Collections.emptyList();

    public SortingCursor(Cursor cursor) throws RuntimeException {
        super(cursor);
        if (cursor != null && cursor instanceof ISortingCursor) {
            orderByList = ((ISortingCursor) cursor).getOrderByList();
        } else {
            orderByList = Collections.emptyList();
        }
    }

    public SortingCursor(Cursor cursor, List<OrderBy> orderByList) {
        super(cursor);
        this.orderByList = orderByList;
    }

    public SortingCursor(Cursor cursor, CursorMetaData cursorMetaData, List<OrderBy> orderByList) {
        super(cursor);
        this.orderByList = orderByList;
    }

    public List<OrderBy> getOrderByList() throws RuntimeException {
        return orderByList;
    }

    public void setOrderByList(List<OrderBy> orderByList) {
        this.orderByList = orderByList;
    }

    public List<List<OrderBy>> getJoinOrderByListList() throws RuntimeException {
        return Arrays.asList(orderByList);
    }

    public boolean skipTo(Record keyRecord) throws RuntimeException {
        return parentCursorSkipTo(keyRecord);
    }

    public boolean skipTo(KeyValueRecordPair keyKeyValueRecordPair) throws RuntimeException {
        return parentCursorSkipTo(keyKeyValueRecordPair);
    }

    public RowValues current() throws RuntimeException {
        return parentCursorCurrent();
    }

    public RowValues next() throws RuntimeException {
        return parentCursorNext();
    }

    public RowValues prev() throws RuntimeException {
        return parentCursorPrev();
    }

    public RowValues first() throws RuntimeException {
        return parentCursorFirst();
    }

    public void beforeFirst() throws RuntimeException {
        parentCursorBeforeFirst();
    }

    public RowValues last() throws RuntimeException {
        return parentCursorLast();
    }

    public RowValues getNextDuplicateValueRowData() throws RuntimeException {
        return parentCursorGetNextDuplicateRowData();
    }

    public List<RuntimeException> close(List<RuntimeException> exceptions) {
        List<RuntimeException> ex = parentCursorClose(exceptions);
        return ex;
    }

    public Cursor getCursor() {
        return parentCursor();
    }

    public Map<Record, DuplicateValueRowDataLinkedList> mgetRecordToDuplicateValueRowDataLinkedListMap(List<Record> keyRecordList, boolean prefixMatch, boolean keyFilterOrValueFilter) throws RuntimeException {
        return parentCursorMgetRecordToDuplicateValueRowDataLinkedListMap(keyRecordList, prefixMatch, keyFilterOrValueFilter);
    }

    public List<DuplicateValueRowDataLinkedList> mgetDuplicateValueRowDataLinkedListList(List<Record> keyRecordList, boolean prefixMatch, boolean keyFilterOrValueFilter) throws RuntimeException {
        return parentCursorMgetDuplicateValueLinkedListList(keyRecordList, prefixMatch, keyFilterOrValueFilter);
    }

    public String toStringWithInden(int inden) {
        if (cursor != null) {
            return cursor.toStringWithInden(inden);
        } else {
            return GeneralUtil.getTab(inden) + "SortingCursor";
        }

    }

    public List<ColumnMetaData> getColumnMetaDataList() throws RuntimeException {
        return parentCursorReturnColumnMetaDataList();
    }

}
