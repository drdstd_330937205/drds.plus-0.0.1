package drds.plus.executor.cursor.cursor_metadata;

import drds.plus.sql_process.type.Type;

/**
 * 列信息
 */
public class Column {


    /**
     * 列名
     */
    protected final String columnName;
    /**
     * 当前列的类型
     */
    protected final Type type;

    /**
     * 是否准许为空
     */
    protected final boolean nullable;

    protected final boolean isAutoCreated;
    /**
     * 当前列的别名
     */
    protected final String alias;

    public Column(String columnName, Type type, String alias) {
        this(columnName, type, true, false, alias);
    }

    public Column(String columnName, Type type, boolean nullable, boolean isAutoCreated, String alias) {
        this.columnName = columnName;
        this.type = type;
        this.nullable = nullable;
        this.isAutoCreated = isAutoCreated;
        this.alias = alias;
    }

    public Column(String columnName, Type type) {
        this(columnName, type, null);
    }

    public boolean getNullable() {
        return nullable;
    }

    public String getAlias() {
        return alias;
    }

    public Type getType() {
        return type;
    }

    public String getColumnName() {
        return columnName;
    }


    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Column other = (Column) obj;
        if ((this.columnName == null) ? (other.columnName != null) : !this.columnName.equals(other.columnName)) {
            return false;
        }
        if (this.type != other.type) {
            return false;
        }
        if ((this.alias == null) ? (other.alias != null) : !this.alias.equals(other.alias)) {
            return false;
        }
        return this.nullable == other.nullable;
    }

    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + (this.columnName != null ? this.columnName.hashCode() : 0);
        hash = 23 * hash + (this.type != null ? this.type.hashCode() : 0);
        hash = 23 * hash + (this.alias != null ? this.alias.hashCode() : 0);
        hash = 23 * hash + (this.nullable ? 1 : 0);
        return hash;
    }

    public String toStringWithInden(String parentTableName) {
        StringBuilder sb = new StringBuilder();
        sb.append(columnName);
        if (alias != null) {
            sb.append(" setAliasAndSetNeedBuild ").append(alias);
        }

        return sb.toString();
    }

    public boolean isAutoCreated() {
        return isAutoCreated;
    }

}
