package drds.plus.executor.cursor.cursor.impl.common;

import drds.plus.sql_process.type.Type;

/**
 * 用来做一些值的处理工作
 */
public interface EqualValueProcessor {

    Object process(Object object, Type type);
}
