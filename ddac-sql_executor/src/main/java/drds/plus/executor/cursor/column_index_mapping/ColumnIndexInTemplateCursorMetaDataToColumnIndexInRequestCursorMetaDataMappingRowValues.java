package drds.plus.executor.cursor.column_index_mapping;

import drds.plus.executor.cursor.cursor_metadata.CursorMetaData;
import drds.plus.executor.row_values.RowValues;
import drds.plus.executor.row_values.RowValuesWrapper;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Map;

/**
 * 两个rowset内容相同，但是列顺序不同，可以用此转换
 */
public class ColumnIndexInTemplateCursorMetaDataToColumnIndexInRequestCursorMetaDataMappingRowValues extends RowValuesWrapper {

    final Map<Integer/* 返回列中的index位置 */, Integer/* 实际数据中的index位置 */> columnIndexInTemplateCursorMetaDataToColumnIndexInRequestCursorMetaDataMap;

    public ColumnIndexInTemplateCursorMetaDataToColumnIndexInRequestCursorMetaDataMappingRowValues(CursorMetaData cursorMetaData, RowValues rowData, Map<Integer/* 返回列中的index位置 */, Integer/* 实际数据中的index位置 */> columnIndexInTemplateCursorMetaDataToColumnIndexInRequestCursorMetaDataMap) {
        super(cursorMetaData, rowData);
        this.columnIndexInTemplateCursorMetaDataToColumnIndexInRequestCursorMetaDataMap = columnIndexInTemplateCursorMetaDataToColumnIndexInRequestCursorMetaDataMap;
    }

    public static RowValues wrap(CursorMetaData cursorMetaData, RowValues rowData, Map<Integer/* 返回列中的index位置 */, Integer/* 实际数据中的index位置 */> columnIndexInTemplateCursorMetaDataToColumnIndexInRequestCursorMetaDataMap) {
        if (rowData == null) {
            return null;
        }
        return new ColumnIndexInTemplateCursorMetaDataToColumnIndexInRequestCursorMetaDataMappingRowValues(cursorMetaData, rowData, columnIndexInTemplateCursorMetaDataToColumnIndexInRequestCursorMetaDataMap);
    }

    public Object getObject(int columnIndex) {
        Integer indexInCursor = getIndexInCursor(columnIndex);
        return rowData.getObject(indexInCursor);
    }

    public void setObject(int index, Object value) {
        throw new UnsupportedOperationException();
    }

    public Integer getInteger(int columnIndex) {
        Integer indexInCursor = getIndexInCursor(columnIndex);
        return rowData.getInteger(indexInCursor);
    }

    public void setInteger(int index, Integer value) {
        throw new UnsupportedOperationException();
    }

    public Long getLong(int columnIndex) {
        Integer indexInCursor = getIndexInCursor(columnIndex);
        return rowData.getLong(indexInCursor);
    }

    public void setLong(int index, Long value) {
        throw new UnsupportedOperationException();
    }

    public String getString(int columnIndex) {
        Integer indexInCursor = getIndexInCursor(columnIndex);
        return rowData.getString(indexInCursor);
    }

    public void setString(int index, String str) {
        throw new UnsupportedOperationException();
    }

    public Boolean getBoolean(int columnIndex) {
        Integer indexInCursor = getIndexInCursor(columnIndex);
        return rowData.getBoolean(indexInCursor);
    }

    public void setBoolean(int index, Boolean bool) {
        throw new UnsupportedOperationException();
    }

    public Short getShort(int columnIndex) {
        Integer indexInCursor = getIndexInCursor(columnIndex);
        return rowData.getShort(indexInCursor);
    }

    public void setShort(int index, Short shortval) {
        throw new UnsupportedOperationException();
    }

    public Float getFloat(int columnIndex) {
        Integer indexInCursor = getIndexInCursor(columnIndex);
        return rowData.getFloat(indexInCursor);
    }

    public void setFloat(int index, Float fl) {
        throw new UnsupportedOperationException();
    }

    public Double getDouble(int columnIndex) {
        Integer indexInCursor = getIndexInCursor(columnIndex);
        return rowData.getDouble(indexInCursor);
    }

    public void setDouble(int index, Double doub) {
        throw new UnsupportedOperationException();
    }


    public Date getDate(int index) {
        Integer indexReal = getIndexInCursor(index);
        return rowData.getDate(indexReal);
    }

    public void setDate(int index, Date date) {
        throw new UnsupportedOperationException();
    }

    public Timestamp getTimestamp(int index) {
        Integer indexReal = getIndexInCursor(index);
        return rowData.getTimestamp(indexReal);
    }

    public void setTimestamp(int index, Timestamp timestamp) {
        throw new UnsupportedOperationException();
    }

    public Time getTime(int index) {
        Integer indexReal = getIndexInCursor(index);
        return rowData.getTime(indexReal);
    }

    public void setTime(int index, Time time) {
        throw new UnsupportedOperationException();
    }

    public BigDecimal getBigDecimal(int index) {
        Integer indexReal = getIndexInCursor(index);
        return rowData.getBigDecimal(indexReal);
    }

    public void setBigDecimal(int index, BigDecimal bigDecimal) {
        throw new UnsupportedOperationException();
    }


    private Integer getIndexInCursor(int columnIndex) {
        Integer indexInCursor = columnIndexInTemplateCursorMetaDataToColumnIndexInRequestCursorMetaDataMap.get(columnIndex);
        if (indexInCursor == null) {
            indexInCursor = columnIndex;
        }
        return indexInCursor;
    }
}
