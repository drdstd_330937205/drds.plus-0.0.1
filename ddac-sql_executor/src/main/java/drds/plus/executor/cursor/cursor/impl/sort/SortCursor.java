package drds.plus.executor.cursor.cursor.impl.sort;

import drds.plus.executor.cursor.cursor.ISortingCursor;
import drds.plus.executor.cursor.cursor.impl.SortingCursor;
import drds.plus.executor.cursor.cursor_metadata.CursorMetaData;
import drds.plus.executor.row_values.RowValues;
import drds.plus.executor.row_values.row_value.RowValueScaners;
import drds.plus.sql_process.abstract_syntax_tree.expression.order_by.OrderBy;

import java.util.Comparator;
import java.util.List;

/**
 * 公共基类，用于排序
 */
public abstract class SortCursor extends SortingCursor {

    /**
     * 比较器
     */
    protected Comparator<RowValues> rowDataComparator;
    private boolean schemaInited = false;

    public SortCursor(ISortingCursor cursor, List<OrderBy> orderByList) throws RuntimeException {
        super(cursor, null, orderByList);
    }

    protected void initComparator(List<OrderBy> orderByList, CursorMetaData cursorMetaData) {
        if (schemaInited) {
            return;
        }
        schemaInited = true;
        //
        if (orderByList != null) {
            this.rowDataComparator = RowValueScaners.getRowDataComparator(cursorMetaData, orderByList);
        }
    }
}
