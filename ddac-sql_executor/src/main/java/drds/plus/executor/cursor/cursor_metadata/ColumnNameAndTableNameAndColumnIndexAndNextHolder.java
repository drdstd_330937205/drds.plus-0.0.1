package drds.plus.executor.cursor.cursor_metadata;

public class ColumnNameAndTableNameAndColumnIndexAndNextHolder {

    String columnName;
    TableNameAndColumnIndexAndNextHolder tableNameAndColumnIndexAndNextHolder;

    public String getTableName() {
        return tableNameAndColumnIndexAndNextHolder.tableName;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public Integer getIndex() {
        return tableNameAndColumnIndexAndNextHolder.columnIndex;
    }

    public TableNameAndColumnIndexAndNextHolder getTableNameAndColumnIndexAndNextHolder() {
        return tableNameAndColumnIndexAndNextHolder;
    }

    public void setTableNameAndColumnIndexAndNextHolder(TableNameAndColumnIndexAndNextHolder tableNameAndColumnIndexAndNextHolder) {
        this.tableNameAndColumnIndexAndNextHolder = tableNameAndColumnIndexAndNextHolder;
    }

}
