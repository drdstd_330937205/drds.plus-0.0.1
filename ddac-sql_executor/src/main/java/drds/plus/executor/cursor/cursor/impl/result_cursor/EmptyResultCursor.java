package drds.plus.executor.cursor.cursor.impl.result_cursor;

import drds.plus.executor.ExecuteContext;
import drds.plus.executor.row_values.RowValues;

import java.util.List;

public class EmptyResultCursor extends ResultCursor {

    public EmptyResultCursor(ExecuteContext executeContext, List columns) {
        super(executeContext);
        this.setOriginalSelectColumns(columns);
    }

    public String getException() {
        return null;
    }

    public Integer getResultID() {
        return null;
    }

    public void setSize(int n) {

    }

    public int getTotalCount() {
        return 0;
    }

    public Long getTxnID() {
        return null;
    }

    protected void closeStatus() {

    }

    protected void throwExceptionIfClosed() {

    }

    public void setTransactionID(Long txn_id) {

    }

    public void setResultId(Integer result_id) {

    }

    public void setException(String exception) {

    }

    public void setResultCount(Integer count) {

    }

    public void setResults(List<RowValues> results) {

    }

    public Object getIngoreTableName(RowValues rowData, String column, String columnAlias) {
        return null;
    }

    public Object get(RowValues rowData, String table, String column, String columnAlias) {
        return null;
    }

    public RowValues next() throws RuntimeException {
        return null;
    }

    public String getException(Exception e, ResultCursor cursor) {
        return null;
    }

    public void beforeFirst() throws RuntimeException {

    }

    public String toStringWithInden(int inden) {
        return "This is a empty ResultCursor";
    }

    public String toString() {
        return "This is a empty ResultCursor";
    }

}
