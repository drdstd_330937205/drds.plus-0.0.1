package drds.plus.executor.data_node_executor.nonspi;

import drds.plus.executor.IExecutor;

public interface IDataNodeExecutor extends IExecutor {

    /**
     * 该executor所在节点的名字
     */
    String getDataNode();

}
