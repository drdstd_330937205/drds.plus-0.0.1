package drds.plus.executor.data_node_executor.spi;

import drds.plus.common.model.DataNode;
import drds.plus.datanode.api.DatasourceManager;
import drds.plus.executor.IExecutor;

/**
 * 用于主备切换等group操作
 */
public interface DataNodeExecutor extends IExecutor {

    DataNode getDataNode();

    DatasourceManager getDatasourceManager();

}
