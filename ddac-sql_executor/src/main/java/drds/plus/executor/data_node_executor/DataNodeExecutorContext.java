package drds.plus.executor.data_node_executor;

import drds.plus.common.thread_local.ThreadLocalMap;
import drds.plus.executor.data_node_executor.nonspi.IDataNodeExecutor;
import drds.plus.executor.data_node_executor.spi.DataNodeExecutorManager;
import drds.plus.executor.repository.RepositoryNameToRepositoryMap;
import drds.plus.sql_process.sequence.ISequenceManager;
import lombok.Getter;
import lombok.Setter;

public class DataNodeExecutorContext {

    private static final String EXECUTOR_CONTEXT_KEY = "_executor_context_";
    @Setter
    @Getter
    private RepositoryNameToRepositoryMap repositoryNameToRepositoryMap = null;
    @Setter
    @Getter
    private DataNodeExecutorManager dataNodeExecutorManager = null;
    @Setter
    @Getter
    private IDataNodeExecutor dataNodeExecutor = null;
    @Setter
    @Getter
    private ISequenceManager sequenceManager = null;

    public static DataNodeExecutorContext getExecutorContext() {
        return (DataNodeExecutorContext) ThreadLocalMap.get(EXECUTOR_CONTEXT_KEY);
    }

    public static void setExecutorContext(DataNodeExecutorContext dataNodeExecutorContext) {
        ThreadLocalMap.put(EXECUTOR_CONTEXT_KEY, dataNodeExecutorContext);
    }

}
