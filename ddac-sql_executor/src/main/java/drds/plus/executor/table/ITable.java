package drds.plus.executor.table;

import drds.plus.executor.ExecuteContext;
import drds.plus.executor.cursor.cursor.ISortingCursor;
import drds.plus.executor.record_codec.record.Record;
import drds.plus.sql_process.abstract_syntax_tree.configuration.IndexMapping;
import drds.plus.sql_process.abstract_syntax_tree.configuration.TableMetaData;
import drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.QueryWithIndex;

/**
 * 表操作
 */
public interface ITable {

    /**
     * 获取表的描述信息数据。
     */
    TableMetaData getTableMetaData() throws RuntimeException;

    /**
     * 关闭table
     */
    void close() throws RuntimeException;
    //

    /**
     * 核心写接口-mysql使用replace into进行处理,bdb可以直接覆盖
     */
    void put(ExecuteContext executeContext, String databaseName, IndexMapping indexMapping, Record key, Record value) throws RuntimeException;

    /**
     * 删除一行数据
     */
    void delete(ExecuteContext executeContext, String databaseName, IndexMapping indexMapping, Record key) throws RuntimeException;

    /**
     * 获取一行数据。
     */
    Record get(ExecuteContext executeContext, String databaseName, IndexMapping indexMapping, Record key) throws RuntimeException;

    /**
     * 根据meta 源信息，获取一个表数据指针。这个指针应该是delegate的，这个方法不应该耗费太多资源。 cursor的隔离性
     */
    ISortingCursor getCursor(ExecuteContext executeContext, QueryWithIndex queryWithIndex, IndexMapping indexMapping) throws RuntimeException;

    ISortingCursor getCursor(ExecuteContext executeContext, String indexMetaName, IndexMapping indexMapping) throws RuntimeException;
}
