package drds.plus.executor.table;

import drds.plus.executor.repository.Repository;
import drds.plus.sql_process.abstract_syntax_tree.configuration.TableMetaData;

public abstract class AbstractTable implements ITable {

    protected TableMetaData tableMetaData;
    protected Repository repository;

    public AbstractTable(TableMetaData tableMetaData, Repository repository) {
        this.tableMetaData = tableMetaData;
        this.repository = repository;

    }

    public TableMetaData getTableMetaData() {
        return tableMetaData;
    }

}
