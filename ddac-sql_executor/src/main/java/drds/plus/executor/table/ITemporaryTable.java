package drds.plus.executor.table;

import drds.plus.executor.ExecuteContext;
import drds.plus.executor.cursor.cursor.ISortingCursor;
import drds.plus.executor.cursor.cursor_metadata.CursorMetaData;

/**
 * 临时表
 */
public interface ITemporaryTable extends ITable {


    CursorMetaData getCursorMetaData();

    void setCursorMetaData(CursorMetaData cursorMetaData);

    ISortingCursor getCursor(ExecuteContext executeContext) throws RuntimeException;
}
