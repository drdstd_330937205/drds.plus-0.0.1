package drds.plus.executor;

import drds.plus.common.lifecycle.Lifecycle;
import drds.plus.executor.cursor.cursor.ISortingCursor;
import drds.plus.executor.cursor.cursor.impl.result_cursor.ResultCursor;
import drds.plus.sql_process.abstract_syntax_tree.execute_plan.ExecutePlan;

import java.util.List;
import java.util.concurrent.Future;


public interface IExecutor extends Lifecycle {

    /**
     * 执行一个命令
     */
    ISortingCursor execute(ExecuteContext executeContext, ExecutePlan executePlan) throws RuntimeException;

    ResultCursor commit(ExecuteContext executeContext) throws RuntimeException;

    ResultCursor rollback(ExecuteContext executeContext) throws RuntimeException;

    /**
     * 并行执行一个命令
     */
    Future<ISortingCursor> executeWithFuture(ExecuteContext executeContext, ExecutePlan executePlan) throws RuntimeException;

    /**
     * 并行执行一组命令
     */
    Future<List<ISortingCursor>> executeWithFuture(ExecuteContext executeContext, List<ExecutePlan> executePlanList) throws RuntimeException;

    Future<ResultCursor> commitWithFuture(ExecuteContext executeContext) throws RuntimeException;

    Future<ResultCursor> rollbackWithFuture(ExecuteContext executeContext) throws RuntimeException;
}
