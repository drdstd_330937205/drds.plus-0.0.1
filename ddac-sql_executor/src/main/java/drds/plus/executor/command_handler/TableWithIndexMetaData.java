package drds.plus.executor.command_handler;

import drds.plus.executor.table.ITable;
import drds.plus.sql_process.abstract_syntax_tree.configuration.IndexMapping;

/**
 * 和索引相关的表
 */
public class TableWithIndexMetaData {
    public ITable table;
    public IndexMapping indexMapping;
}
