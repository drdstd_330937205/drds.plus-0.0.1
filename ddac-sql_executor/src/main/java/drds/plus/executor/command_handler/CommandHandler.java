package drds.plus.executor.command_handler;

import drds.plus.executor.ExecuteContext;
import drds.plus.executor.cursor.cursor.ISortingCursor;
import drds.plus.sql_process.abstract_syntax_tree.execute_plan.ExecutePlan;

/**
 * 命令执行处理器 比如增删改查 都属于一个命令，可能会被循环调用
 */
public interface CommandHandler {

    /**
     * 处理对应的一个命令 具体请看实现
     */
    ISortingCursor handle(ExecuteContext executeContext, ExecutePlan executePlan) throws RuntimeException;
}
