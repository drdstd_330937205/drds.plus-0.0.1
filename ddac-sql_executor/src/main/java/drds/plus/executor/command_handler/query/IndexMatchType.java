package drds.plus.executor.command_handler.query;

public class IndexMatchType {
    /**
     * 不匹配
     */
    public static int not_match = -1;
    /**
     * 前缀匹配
     */
    public static int prefix_match = 0;
    /**
     * 完全匹配
     */
    public static int match = 1;
}
