package drds.plus.executor.command_handler.query;

public enum OrderByStrategy {
    /**
     * 临时表
     */
    temporaryTable,//min
    /**
     * 需要反转
     */
    reverseCursor,
    /**
     * 正常
     */
    normal//max
}
