package drds.plus.repository.mysql.execute_plan_to_sql;

import lombok.Getter;
import lombok.Setter;

import java.util.LinkedList;
import java.util.List;

/**
 * 一组sql
 */
public class Sqls {
    @Setter
    @Getter
    List<Sql> sqlList = new LinkedList<Sql>();

    public int hashCode() {
        return sqlList.hashCode();
    }

    public boolean equals(Object arg0) {
        return sqlList.equals(arg0);
    }
    //


    public String toString() {
        final int maxLen = 10;
        StringBuilder sb = new StringBuilder();
        if (sqlList != null) {
            sb.append("sqlList:\n\t");
            sb.append(sqlList.subList(0, Math.min(sqlList.size(), maxLen)));
        }
        return sb.toString();
    }

}
