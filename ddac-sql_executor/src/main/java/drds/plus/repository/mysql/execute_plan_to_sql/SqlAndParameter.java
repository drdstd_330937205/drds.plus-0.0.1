package drds.plus.repository.mysql.execute_plan_to_sql;

import drds.plus.common.jdbc.SetParameterMethodAndArgs;

import java.util.Map;

public class SqlAndParameter {

    public String sql;
    public Map<Integer, SetParameterMethodAndArgs> indexToSetParameterMethodAndArgsMap;
    public Map<Integer, Integer> newIndexToOldIndexMap;
}
