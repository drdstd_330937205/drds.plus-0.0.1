package drds.plus.repository.mysql.function;

import drds.plus.common.jdbc.SetParameterMethodAndArgs;
import drds.plus.repository.mysql.execute_plan_to_sql.ExecutePlanVisitor;
import drds.plus.sql_process.abstract_syntax_tree.execute_plan.ExecutePlan;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.Function;

import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public interface FunctionStringConstructor {

    String constructColumnNameForFunction(ExecutePlan query, boolean bindVal, AtomicInteger bindValSequence, Map<Integer, SetParameterMethodAndArgs> paramMap, Function func, ExecutePlanVisitor parentVisitor);

    String[] getFunctionNames();
}
