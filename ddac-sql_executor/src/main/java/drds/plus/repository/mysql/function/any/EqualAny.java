package drds.plus.repository.mysql.function.any;

import drds.plus.common.jdbc.SetParameterMethodAndArgs;
import drds.plus.repository.mysql.execute_plan_to_sql.ExecutePlanVisitor;
import drds.plus.repository.mysql.function.FunctionStringConstructor;
import drds.plus.sql_process.abstract_syntax_tree.execute_plan.ExecutePlan;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.Function;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.Operation;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class EqualAny implements FunctionStringConstructor {

    public String constructColumnNameForFunction(ExecutePlan query, boolean bindVal, AtomicInteger bindValSequence, Map<Integer, SetParameterMethodAndArgs> paramMap, Function func, ExecutePlanVisitor parentVisitor) {
        StringBuilder sb = new StringBuilder();

        sb.append(parentVisitor.getNewExecutePlanVisitor(func.getArgList().get(0)).getString());

        Object right = func.getArgList().get(1);

        // 右边被提前计算过了，直接用in
        if (right instanceof List) {
            sb.append(" in ");
            sb.append(parentVisitor.getNewExecutePlanVisitor(func.getArgList().get(1)).getString());
        } else {
            // 右边没进行计算，还是个子查询，理论上不会发生
            sb.append(" = any (");
            sb.append(parentVisitor.getNewExecutePlanVisitor(func.getArgList().get(1)).getString());
            sb.append(")");
        }

        return sb.toString();

    }

    public String[] getFunctionNames() {
        return new String[]{Operation.equal_any.getOperationString()};
    }

}
