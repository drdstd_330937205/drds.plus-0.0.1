package drds.plus.repository.mysql.execute_plan_to_sql;

import drds.plus.sql_process.abstract_syntax_tree.expression.item.Item;
import drds.plus.sql_process.abstract_syntax_tree.expression.order_by.OrderBy;

import java.util.*;

public class DataNodeIdToSqlsMapMergeInfo {

    /**
     * order by 条件
     */
    protected List<OrderBy> orderByList = Collections.emptyList();

    /**
     * 进行合并后，是否要丢弃某些数据
     */
    protected Long limitFrom;

    /**
     * 进行合并后，取多少条记录
     */
    protected Long limitTo;

    /**
     * group by 什么条件
     */
    protected List<OrderBy> groupByList = Collections.emptyList();

    /**
     * 返回列的情况。可能是函数，可能是其他操作，也可能是嵌套的函数
     */
    protected List<Item> itemList;

    protected Map<String/* 执行节点 */, Sqls> dataNodeIdToSqlsMap;

    public List<OrderBy> getOrderByList() {
        return orderByList;
    }

    public void setOrderByList(List<OrderBy> orderByList) {
        this.orderByList = orderByList;
    }

    public Long getLimitFrom() {
        return limitFrom;
    }

    public void setLimitFrom(Comparable limitFrom) {
        this.limitFrom = (Long) limitFrom;
    }

    public Long getLimitTo() {
        return limitTo;
    }

    public void setLimitTo(Comparable limitTo) {
        this.limitTo = (Long) limitTo;
    }

    public List<OrderBy> getGroupByList() {
        return groupByList;
    }

    public void setGroupByList(List<OrderBy> groupByList) {
        this.groupByList = groupByList;
    }

    public List<Item> getItemList() {
        return itemList;
    }

    public void setItemList(List<Item> itemList) {
        this.itemList = itemList;
    }

    public Map<String, Sqls> getDataNodeIdToSqlsMap() {
        return dataNodeIdToSqlsMap;
    }

    public void setDataNodeIdToSqlsMap(Map<String, Sqls> dataNodeIdToSqlsMap) {
        this.dataNodeIdToSqlsMap = dataNodeIdToSqlsMap;
    }

    public String toString() {
        final int maxLen = 10;
        StringBuilder sb = new StringBuilder();
        sb.append("DataNodeIdToSqlsMapMergeInfo [\n\t");
        if (orderByList != null) {
            sb.append("addOrderByItemAndSetNeedBuild=");
            sb.append(toString(orderByList, maxLen));
            sb.append(", \n\t");
        }
        if (limitFrom != null) {
            sb.append("limitFrom=");
            sb.append(limitFrom);
            sb.append(", \n\t");
        }
        if (limitTo != null) {
            sb.append("limitTo=");
            sb.append(limitTo);
            sb.append(", \n\t");
        }
        if (groupByList != null) {
            sb.append("groupByList=");
            sb.append(toString(groupByList, maxLen));
            sb.append(", \n\t");
        }
        if (itemList != null) {
            sb.append("columnNameList=");
            sb.append(toString(itemList, maxLen));
            sb.append(", \n\t");
        }
        if (dataNodeIdToSqlsMap != null) {
            sb.append(toString(dataNodeIdToSqlsMap.entrySet(), maxLen));
        }
        sb.append("\n]");
        return sb.toString();
    }

    private String toString(Collection<?> collection, int maxLen) {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        int i = 0;
        for (Iterator<?> iterator = collection.iterator(); iterator.hasNext() && i < maxLen; i++) {
            if (i > 0)
                sb.append(", \n");
            sb.append(iterator.next());
        }
        sb.append("]");
        return sb.toString();
    }

}
