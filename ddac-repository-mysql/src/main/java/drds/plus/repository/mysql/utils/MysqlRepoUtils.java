package drds.plus.repository.mysql.utils;

import drds.plus.executor.utils.Utils;
import drds.plus.sql_process.abstract_syntax_tree.configuration.IndexMapping;
import drds.plus.sql_process.abstract_syntax_tree.execute_plan.ExecutePlan;
import drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.Query;
import drds.plus.sql_process.abstract_syntax_tree.expression.order_by.OrderBy;

import java.util.List;

public class MysqlRepoUtils {

    public static List<OrderBy> buildOrderBy(ExecutePlan executePlan, IndexMapping indexMapping) {
        Query query = ((Query) executePlan);
        List<OrderBy> orderByList = query.getOrderByList();
        if (orderByList == null || orderByList.isEmpty()) {
            List<OrderBy> groupBys = query.getGroupByList();
            orderByList = groupBys;
        }
        if (orderByList == null || orderByList.isEmpty()) {
            orderByList = Utils.getOrderByList(indexMapping);
        }
        return Utils.copyOrderByList(orderByList);
    }


}
