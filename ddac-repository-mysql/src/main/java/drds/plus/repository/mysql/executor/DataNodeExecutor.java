package drds.plus.repository.mysql.executor;

import drds.plus.datanode.api.DatasourceManager;
import drds.plus.executor.data_node_executor.spi.AbstractDataNodeExecutor;
import drds.plus.executor.repository.Repository;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;


@Slf4j
public class DataNodeExecutor extends AbstractDataNodeExecutor {
    public Logger log() {
        return log;
    }

    @Setter
    @Getter
    private DatasourceManager datasourceManager;

    public DataNodeExecutor(Repository repository) {
        super(repository);
    }

    protected void doInit() throws RuntimeException {
        super.doInit();
    }

    protected void doDestroy() throws RuntimeException {
        if (this.datasourceManager != null) {
            try {
                datasourceManager.destroyDataSource();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    public DatasourceManager getDatasourceManager() {
        return datasourceManager;
    }


}
