package drds.plus.repository.mysql.spi;

import drds.plus.common.model.RepositoryType;
import drds.plus.datanode.api.DatasourceManager;
import drds.plus.executor.data_node_executor.DataNodeExecutorContext;
import drds.plus.executor.data_node_executor.spi.DataNodeExecutor;
import drds.plus.executor.repository.IDatasourceManagerGetter;

public class DatasourceManagerGetterImpl implements IDatasourceManagerGetter {
    public DatasourceManager getDatasourceManager(String dataNodeId) {
        if ("undecided".equals(dataNodeId)) {
            return null;
        }
        DataNodeExecutor dataNodeExecutor = DataNodeExecutorContext.getExecutorContext().getDataNodeExecutorManager().getDataNodeExecutor(dataNodeId);
        if (dataNodeExecutor == null) {
            return null;
        }
        RepositoryType repositoryType = dataNodeExecutor.getDataNode().getRepositoryType();
        if (!RepositoryType.mysql.equals(repositoryType)) {
            throw new IllegalArgumentException("target query is not a validated Jdbc query");
        }
        DatasourceManager datasourceManager = dataNodeExecutor.getDatasourceManager();
        if (datasourceManager == null) {
            throw new NullPointerException("can't find datasourceManager by datanode columnName ");
        }
        return datasourceManager;
    }


}
