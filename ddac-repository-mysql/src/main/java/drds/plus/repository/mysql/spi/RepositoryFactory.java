package drds.plus.repository.mysql.spi;

import drds.plus.executor.repository.IRepositoryFactory;
import drds.plus.executor.repository.Repository;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

@Slf4j
public class RepositoryFactory implements IRepositoryFactory {
    static {
        log.info("存储初始化mysql数据库介质..." + RepositoryFactory.class.getName());
    }

    public Repository buildRepository(Map repoProperties, Map connectionProperties) {
        RepositoryImpl repository = new RepositoryImpl();
        repository.init();
        return repository;
    }


}
