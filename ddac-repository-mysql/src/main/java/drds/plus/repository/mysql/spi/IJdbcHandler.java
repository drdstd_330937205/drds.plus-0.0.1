package drds.plus.repository.mysql.spi;

import drds.plus.datanode.api.DatasourceManager;
import drds.plus.executor.ExecuteContext;
import drds.plus.executor.cursor.cursor.IAffectRowCursor;
import drds.plus.executor.cursor.cursor.ISortingCursor;
import drds.plus.executor.cursor.cursor_metadata.CursorMetaData;
import drds.plus.executor.record_codec.record.Record;
import drds.plus.executor.row_values.RowValues;
import drds.plus.executor.table.ITable;
import drds.plus.sql_process.abstract_syntax_tree.configuration.IndexMapping;
import drds.plus.sql_process.abstract_syntax_tree.execute_plan.dml.IPut;

import java.sql.SQLException;

/**
 * 集中所有jdbc操作到这个Handler. 这样才能支持同步化和异步化执行
 */
public interface IJdbcHandler {

    /**
     * 初始化结果集
     */
    void executeQuery(CursorMetaData cursorMetaData, boolean isStreaming) throws SQLException;

    /**
     * 获取结果集
     */
    ISortingCursor getResultCursor();

    /**
     * 设置dataSource
     */
    void setDatasourceManager(DatasourceManager datasourceManager);

    /**
     * 关闭,但不是真关闭
     */
    void close() throws SQLException;

    /**
     * 跳转（不过大部分指针都不支持这个）
     */
    boolean skipTo(Record key, CursorMetaData cursorMetaData) throws SQLException;

    /**
     * 指针下移 返回null则表示没有后项了
     */
    RowValues next() throws SQLException;

    /**
     * 最开始的row
     */
    RowValues first() throws SQLException;

    /**
     * 最后一个row
     */
    RowValues last() throws SQLException;

    /**
     * 获取当前row
     */
    RowValues getCurrentRowData();

    /**
     * 是否触发过initResultSet()这个方法。 与isDone不同的是这个主要是防止多次提交用。（估计）
     */
    boolean isInited();

    /**
     * 获取上一个数据
     */
    RowValues prev() throws SQLException;

    /**
     * 执行一个update语句
     */
    IAffectRowCursor executeUpdate(ExecuteContext executeContext, IPut put, ITable table, IndexMapping indexMapping) throws SQLException;

    /**
     * 用于异步化，等同于Future.isDone();
     */
    boolean isDone();

    /**
     * 用于异步化，等同于Future.cancel();
     */
    void cancel(boolean mayInterruptIfRunning);

    /**
     * 用于异步化，等同于Future.isCancel();
     */
    boolean isCanceled();

    /**
     * 指针前移
     */
    void beforeFirst() throws SQLException;

}
