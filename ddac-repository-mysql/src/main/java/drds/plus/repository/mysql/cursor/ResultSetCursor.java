package drds.plus.repository.mysql.cursor;

import drds.plus.executor.cursor.cursor.IResultSetCursor;
import drds.plus.executor.cursor.cursor.impl.result_cursor.ResultCursor;
import drds.plus.executor.row_values.RowValues;
import drds.plus.repository.mysql.spi.JdbcHandler;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ResultSetCursor extends ResultCursor implements IResultSetCursor {

    private ResultSet resultSet;
    private JdbcHandler jdbcHandler = null;

    public ResultSetCursor(ResultSet resultSet, JdbcHandler jdbcHandler) {
        super(null);
        this.resultSet = resultSet;
        this.jdbcHandler = jdbcHandler;
    }

    public ResultSet getResultSet() {
        return this.resultSet;
    }

    public List<RuntimeException> close(List<RuntimeException> RuntimeExceptionList) {
        if (RuntimeExceptionList == null) {
            RuntimeExceptionList = new ArrayList();
        }
        try {
            resultSet.close();
        } catch (Exception e) {
            RuntimeExceptionList.add(new RuntimeException(e));
        }

        return RuntimeExceptionList;
    }

    public RowValues first() throws RuntimeException {
        try {
            return jdbcHandler.first();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public RowValues prev() throws RuntimeException {
        try {
            return jdbcHandler.prev();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public RowValues next() throws RuntimeException {
        try {
            return jdbcHandler.next();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public RowValues current() throws RuntimeException {
        return jdbcHandler.getCurrentRowData();
    }

    public RowValues last() throws RuntimeException {
        try {
            return jdbcHandler.last();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
