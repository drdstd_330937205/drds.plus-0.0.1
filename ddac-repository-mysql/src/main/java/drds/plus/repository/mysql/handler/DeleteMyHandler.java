package drds.plus.repository.mysql.handler;

import drds.plus.executor.ExecuteContext;
import drds.plus.executor.cursor.cursor.ISortingCursor;
import drds.plus.executor.table.ITable;
import drds.plus.repository.mysql.spi.JdbcHandler;
import drds.plus.sql_process.abstract_syntax_tree.configuration.IndexMapping;
import drds.plus.sql_process.abstract_syntax_tree.execute_plan.dml.IPut;

import java.sql.SQLException;

public class DeleteMyHandler extends PutMyHandlerCommon {

    public DeleteMyHandler() {
        super();
    }

    protected ISortingCursor executePut(ExecuteContext executeContext, IPut put, ITable table, IndexMapping indexMapping, JdbcHandler jdbcHandler) throws RuntimeException {
        try {
            return jdbcHandler.executeUpdate(executeContext, put, table, indexMapping);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
