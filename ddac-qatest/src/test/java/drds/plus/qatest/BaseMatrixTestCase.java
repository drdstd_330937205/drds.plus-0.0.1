package drds.plus.qatest;

import drds.plus.api.SessionManager;
import drds.plus.common.properties.ConnectionProperties;
import drds.plus.qatest.util.LoadPropsUtil;
import drds.plus.qatest.util.PrepareData;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 基本测试类
 * <readPriority/>
 * Author By: zhuoxue.yll Created Date: 2012-2-16 下午2:05:24
 */
@Ignore(value = "提供初始化环境的实际方法")
public class BaseMatrixTestCase extends PrepareData {

    protected static final ExecutorService pool = Executors.newCachedThreadPool();
    // dbType为mysql运行mysql测试，bdb值为bdb运行bdb测试，如果为空则运行bdb和mysql测试
    protected static String dbType = "tddl-server";
    protected static boolean needPerparedData = true;
    private static String ruleFile = "V0#classpath:plus/";
    private static String rule = "routeOptimizer.xml";
    private static String schemaFile = "matrix/";
    private static String schema = "tableMetaData.xml";
    private static String sequence = "sequence.xml";
    private static String machineTapologyFile = "matrix/server_topology.xml";

    private static String typeFile = "db_type.properties";

    static {
        dbType = LoadPropsUtil.loadProps(typeFile).getProperty("dbType");
    }

    @BeforeClass
    public static void IEnvInit() throws Exception {

        // setMatrixMockInfo(MATRIX_DBGROUPS_PATH, TDDL_DBGROUPS);

        if (tddlDatasource == null) {
            // tddlDatasource = JDBCClient(dbType, false);
        }

        mysqlConnection = getConnection();
        // tddlConnection = getTddlServerConnection();
        tddlConnection = tddlDatasource.getConnection();
    }

    @AfterClass
    public static void cleanConnection() throws SQLException {
        if (mysqlConnection != null) {
            mysqlConnection.close();
            mysqlConnection = null;
        }
        if (tddlConnection != null) {
            tddlConnection.close();
            tddlConnection = null;
        }
    }

    public static void JDBCClient(String dbType) throws Exception {
        JDBCClient(dbType, false);
    }

    public static SessionManager JDBCClient(String dbTypeStack, boolean async) throws Exception {
        if (dbTypeStack.equals("tddl") || dbTypeStack.equals("mysql")) {
            SessionManager dataSource = null;
            dataSource = new SessionManager();
            dataSource.setApplicationId("andor_mysql_qatest");
            // tddlDatasource.setConfigMode("mock");

            Map<String, Object> cp = new HashMap<String, Object>();

            if ("hbase".equalsIgnoreCase(dbTypeStack)) {
                cp.put(ConnectionProperties.HBASE_MAPPING_FILE, "matrix/hbase_mapping.xml");
            }

            dataSource.setConnectionProperties(cp);
            try {
                dataSource.init();
            } catch (Exception e) {

            }

            return dataSource;
        }

        throw new IllegalAccessError();
    }


    @After
    public void clearDate() throws Exception {
        psConRcRsClose(rc, rs);
    }

}
