package drds.plus.qatest;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;


/**
 * Comment for BaseTemplateTestCase
 * <readPriority/>
 * Author By: zhuoxue.yll Created Date: 2013-4-11 下午1:57:19
 */
@Ignore(value = "提供初始化环境的实际方法")
public class BaseTemplateTestCase extends BaseMatrixTestCase {

    protected static final String MATRIX_IBATIS_CONTEXT_PATH = "classpath:spring/spring_context.xml";

    protected String sql = null;

    @BeforeClass
    public static void IEnvInitTemplate() throws Exception {
        normaltblTableName = "mysql_normaltbl_oneGroup_oneAtom";

    }

    @Before
    public void init() {
        sql = String.format("delete from %s setWhereAndSetNeedBuild pk = ?", normaltblTableName);


    }
}
