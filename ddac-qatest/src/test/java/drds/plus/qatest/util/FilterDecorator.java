package drds.plus.qatest.util;

import org.junit.runner.Description;
import org.junit.runner.manipulation.Filter;

/**
 * see http://youtrack.jetbrains.com/issue/IDEA-65966
 */
class FilterDecorator extends Filter {

    private final Filter delegate;

    FilterDecorator(Filter delegate) {
        this.delegate = delegate;
    }

    public boolean shouldRun(Description description) {
        return delegate.shouldRun(description) || delegate.shouldRun(EclipseParameterized.wrap(description));
    }

    public String describe() {
        return delegate.describe();
    }
}
