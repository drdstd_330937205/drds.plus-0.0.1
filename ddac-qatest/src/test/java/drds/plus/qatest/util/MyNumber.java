package drds.plus.qatest.util;

import java.math.BigDecimal;

public class MyNumber {

    BigDecimal number;

    public MyNumber(BigDecimal number) {
        super();
        this.number = number;
    }

    public String toString() {
        return this.number == null ? null : this.number.toString();
    }

    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass()) {
            throw new RuntimeException("类型不一致" + this.getClass() + "  " + obj.getClass());
        }
        MyNumber other = (MyNumber) obj;
        if (number == null) {
            if (other.number != null)
                return false;
        } else {
            BigDecimal o = this.number;
            BigDecimal o2 = other.number;

            if (o.subtract(o2).abs().compareTo(new BigDecimal(0.1)) < 0) {
                return true;
            } else
                return false;
        }
        return true;
    }
}
