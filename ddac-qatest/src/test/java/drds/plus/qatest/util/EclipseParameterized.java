package drds.plus.qatest.util;

import org.junit.runner.Description;
import org.junit.runner.manipulation.Filter;
import org.junit.runner.manipulation.NoTestsRemainException;
import org.junit.runners.Parameterized;

import java.lang.annotation.Annotation;

public class EclipseParameterized extends Parameterized {

    public EclipseParameterized(Class<?> klass) throws Throwable {
        super(klass);
    }

    public static Description wrap(Description description) {
        String name = description.getDisplayName();
        String fixedName = deparametrizedName(name);
        Description clonedDescription = Description.createSuiteDescription(fixedName, description.getAnnotations().toArray(new Annotation[0]));
        for (Description child : description.getChildren()) {
            clonedDescription.addChild(wrap(child));
        }
        return clonedDescription;
    }

    private static String deparametrizedName(String name) {
        // Each parameter is named setAliasAndSetNeedBuild [0], [1] etc
        if (name.startsWith("[")) {
            return name;
        }

        // Convert methodName[indexMapping](className) to
        // methodName(className)
        int indexOfOpenBracket = name.indexOf('[');
        int indexOfCloseBracket = name.indexOf(']') + 1;
        return name.substring(0, indexOfOpenBracket).concat(name.substring(indexOfCloseBracket));
    }

    public void filter(Filter filter) throws NoTestsRemainException {
        super.filter(new FilterDecorator(filter));
    }

}
