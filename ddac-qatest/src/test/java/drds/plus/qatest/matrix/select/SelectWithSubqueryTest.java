package drds.plus.qatest.matrix.select;

import drds.plus.qatest.BaseMatrixTestCase;
import drds.plus.qatest.BaseTestCase;
import drds.plus.qatest.ExecuteTableName;
import drds.plus.qatest.util.EclipseParameterized;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * 子查询测试 >,>=,<,<=,=,!=,like all any
 */
@RunWith(EclipseParameterized.class)
public class SelectWithSubqueryTest extends BaseMatrixTestCase {

    public SelectWithSubqueryTest(String normaltblTableName, String studentTableName) {
        BaseTestCase.normaltblTableName = normaltblTableName;
        BaseTestCase.studentTableName = studentTableName;
    }

    @Parameters(name = "{indexMapping}:table0={0},table1={1}")
    public static List<String[]> prepare() {
        return Arrays.asList(ExecuteTableName.normaltblStudentTable(dbType));
    }

    @Before
    public void prepareDate() throws Exception {
        normaltblPrepare(0, 20);
        studentPrepare(0, MAX_DATA_SIZE);
    }

    /**
     * in
     */
    @Test
    public void inTest() throws Exception {
        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild pk in (selectStatement id from " + studentTableName + ")";
        String[] columnParam = {"PK", "NAME"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * 等于子查询中的值
     */
    @Test
    public void equalTest() throws Exception {

        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild pk = (selectStatement id from " + studentTableName + " order by id limit 1)";
        String[] columnParam = {"PK", "NAME"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * 等于子查询中的最大值
     */
    @Test
    public void equalMaxTest() throws Exception {

        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild pk = (selectStatement max(id) from " + studentTableName + ")";
        String[] columnParam = {"PK", "NAME"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * 大于子查询中的值
     */
    @Test
    public void greaterTest() throws Exception {

        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild pk > (selectStatement id from " + studentTableName + " order by id asc limit 5,1)";
        String[] columnParam = {"PK", "NAME"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * 大于等于子查询中的值
     */
    @Test
    public void greaterEqualTest() throws Exception {
        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild pk >= (selectStatement id from " + studentTableName + " order by id asc limit 5,1)";
        String[] columnParam = {"PK", "NAME"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * 小于子查询中的值
     */
    @Test
    public void lessTest() throws Exception {
        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild pk < (selectStatement id from " + studentTableName + " order by id asc limit 5,1)";
        String[] columnParam = {"PK", "NAME"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * 小于等于子查询中的值
     */
    @Test
    public void lessEqualTest() throws Exception {
        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild pk <= (selectStatement id from " + studentTableName + " order by id asc limit 5,1)";
        String[] columnParam = {"PK", "NAME"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * 不等于子查询中的值
     */
    @Test
    public void notEqualTest() throws Exception {
        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild pk != (selectStatement id from " + studentTableName + " order by id asc limit 5,1)";
        String[] columnParam = {"PK", "NAME"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * like子查询中的值
     */
    @Test
    public void likeTest() throws Exception {
        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild columnName like (selectStatement columnName from " + studentTableName + " order by id asc limit 5,1)";
        String[] columnParam = {"PK", "NAME"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * 大于any子查询中的值
     */
    @Test
    public void greaterAnyTest() throws Exception {

        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild pk > any(selectStatement id from " + studentTableName + ")";
        String[] columnParam = {"PK", "NAME"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * 大于等于any子查询中的值
     */
    @Test
    public void greaterEqualAnyTest() throws Exception {
        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild pk >= any(selectStatement id from " + studentTableName + ")";
        String[] columnParam = {"PK", "NAME"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * 小于any子查询中的值
     */
    @Test
    public void lessAnyTest() throws Exception {
        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild pk < any(selectStatement id from " + studentTableName + ")";
        String[] columnParam = {"PK", "NAME"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * 小于等于any子查询中的值
     */
    @Test
    public void lessEqualAnyTest() throws Exception {
        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild pk <= any(selectStatement id from " + studentTableName + ")";
        String[] columnParam = {"PK", "NAME"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * 不等于any子查询中的值
     */
    @Test
    public void notEqualAnyTest() throws Exception {
        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild pk != any(selectStatement id from " + studentTableName + ")";
        String[] columnParam = {"PK", "NAME"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * 不等于any子查询中的一个值
     */
    @Test
    public void notEqualAnyOneValueTest() throws Exception {
        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild pk != any(selectStatement id from " + studentTableName + " setWhereAndSetNeedBuild id=1)";
        String[] columnParam = {"PK", "NAME"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * 等于any子查询中的值
     */
    @Test
    public void equalAnyTest() throws Exception {

        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild pk = any(selectStatement id from " + studentTableName + ")";
        String[] columnParam = {"PK", "NAME"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * 等于any子查询中的最大值
     */
    @Test
    public void equalAnyMaxTest() throws Exception {

        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild pk = any(selectStatement max(id) from " + studentTableName + " group by id)";
        String[] columnParam = {"PK", "NAME"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * 大于any子查询中的最大值
     */
    @Test
    public void greatAnyMaxTest() throws Exception {

        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild pk > any(selectStatement max(id) from " + studentTableName + " group by id)";
        String[] columnParam = {"PK", "NAME"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * 大于any子查询中的最大值
     */
    @Test
    public void greatEqualAnyMaxTest() throws Exception {

        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild pk >= any(selectStatement max(id) from " + studentTableName + " group by id)";
        String[] columnParam = {"PK", "NAME"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * 小于any子查询中的最大值
     */
    @Test
    public void lessAnyMaxTest() throws Exception {

        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild pk < any(selectStatement max(id) from " + studentTableName + " group by id)";
        String[] columnParam = {"PK", "NAME"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * 小于等于any子查询中的最大值
     */
    @Test
    public void lessEqualAnyMaxTest() throws Exception {

        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild pk <= any(selectStatement max(id) from " + studentTableName + " group by id)";
        String[] columnParam = {"PK", "NAME"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * 不等于any max 多个值的
     */
    @Test
    public void notEqualAnyMaxSomeValueTest() throws Exception {

        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild pk != any(selectStatement max(id) from " + studentTableName + " group by id)";
        String[] columnParam = {"PK", "NAME"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * 不等于any max 1个值的
     */
    @Test
    public void notEqualAnyMaxOneValueTest() throws Exception {

        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild pk != any(selectStatement max(id) from " + studentTableName + " group by id)";
        String[] columnParam = {"PK", "NAME"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * 等于all 最大值 多个值的
     */
    @Test
    public void equalAllMaxTest() throws Exception {

        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild pk = all(selectStatement max(id) from " + studentTableName + " group by id)";
        String[] columnParam = {"PK", "NAME"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * 等于all 最大值一个值的
     */
    @Test
    public void equalAllMaxOneValueTest() throws Exception {

        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild pk = all(selectStatement max(id) from " + studentTableName + " )";
        String[] columnParam = {"PK", "NAME"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * 大于all 最大值 多个值的
     */
    @Test
    public void greatAllMaxTest() throws Exception {

        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild pk > all(selectStatement max(id) from " + studentTableName + " group by id)";
        String[] columnParam = {"PK", "NAME"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * 大于等于all 最大值 多个值的
     */
    @Test
    public void greatEqualAllMaxTest() throws Exception {

        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild pk >= all(selectStatement max(id) from " + studentTableName + " group by id)";
        String[] columnParam = {"PK", "NAME"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * 小于all 最大值
     */
    @Test
    public void lessAllMaxTest() throws Exception {

        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild pk < all(selectStatement max(id) from " + studentTableName + " group by id)";
        String[] columnParam = {"PK", "NAME"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * 小于等于all 最大值
     */
    @Test
    public void lessEqualAllMaxTest() throws Exception {

        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild pk <= all(selectStatement max(id) from " + studentTableName + " group by id)";
        String[] columnParam = {"PK", "NAME"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * 不等于all 最大值
     */
    @Test
    public void notEqualAllMaxTest() throws Exception {

        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild pk != all(selectStatement max(id) from " + studentTableName + " group by id)";
        String[] columnParam = {"PK", "NAME"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * 等于all 一个值
     */
    @Test
    public void equalAllOneValueTest() throws Exception {

        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild pk = all(selectStatement id from " + studentTableName + " setWhereAndSetNeedBuild id=1)";
        String[] columnParam = {"PK", "NAME"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * 等于all 多个值
     */
    @Test
    public void equalAllSomeValueTest() throws Exception {

        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild pk = all(selectStatement id from " + studentTableName + ")";
        String[] columnParam = {"PK", "NAME"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }
}
