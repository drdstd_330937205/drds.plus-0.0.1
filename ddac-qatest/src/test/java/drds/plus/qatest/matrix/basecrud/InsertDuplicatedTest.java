package drds.plus.qatest.matrix.basecrud;

import drds.plus.qatest.BaseMatrixTestCase;
import drds.plus.qatest.BaseTestCase;
import drds.plus.qatest.ExecuteTableName;
import drds.plus.qatest.util.EclipseParameterized;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * insert重复数据
 */
@RunWith(EclipseParameterized.class)
public class InsertDuplicatedTest extends BaseMatrixTestCase {

    public InsertDuplicatedTest(String tableName) {
        BaseTestCase.normaltblTableName = tableName;
    }

    @Parameters(name = "{indexMapping}:where={0}")
    public static List<String[]> prepareData() {
        return Arrays.asList(ExecuteTableName.normaltblTable(dbType));
    }

    @Before
    public void initData() throws Exception {
        tddlUpdateData("delete from  " + normaltblTableName, null);
        mysqlUpdateData("delete from  " + normaltblTableName, null);
    }

    /**
     * insert所有字段
     */
    @Test
    public void insertAllFieldTest() throws Exception {

        if (normaltblTableName.startsWith("ob")) {
            // ob不支持批量更新
            return;
        }
        String sql = "insert into " + normaltblTableName + " columnValueList(?,?,?,?,?,?,?)";
        List<Object> param = new ArrayList<Object>();
        param.add(RANDOM_ID);
        param.add(RANDOM_INT);
        param.add(gmtDay);
        param.add(gmt);
        param.add(gmt);
        param.add(name);
        param.add(fl);
        execute(sql, param);

        sql = "insert into " + normaltblTableName + " columnValueList(?,?,?,?,?,?,?) ON DUPLICATE KEY update  columnName=?";
        param = new ArrayList<Object>();
        param.add(RANDOM_ID);
        param.add(RANDOM_INT);
        param.add(gmtDay);
        param.add(gmt);
        param.add(gmt);
        param.add(name);
        param.add(fl);
        param.add("kkkk");
        execute(sql, param);

        sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild pk=" + RANDOM_ID;
        String[] columnParam = {"PK", "ID", "GMT_CREATE", "NAME", "FLOATCOL", "GMT_TIMESTAMP", "GMT_DATETIME"};
        selectOrderAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * insert所有字段 带now()
     */
    @Test
    public void insertFunction() throws Exception {

        if (normaltblTableName.startsWith("ob")) {
            // ob不支持批量更新
            return;
        }
        String sql = "insert into " + normaltblTableName + " columnValueList(?,?,?,?,?,?,?)";
        List<Object> param = new ArrayList<Object>();
        param.add(RANDOM_ID);
        param.add(RANDOM_INT);
        param.add(gmtDay);
        param.add(gmt);
        param.add(gmt);
        param.add(name);
        param.add(fl);
        execute(sql, param);

        sql = "insert into " + normaltblTableName + " columnValueList(?,?,?,?,?,?,?) ON DUPLICATE KEY update  columnName=now()";
        param = new ArrayList<Object>();
        param.add(RANDOM_ID);
        param.add(RANDOM_INT);
        param.add(gmtDay);
        param.add(gmt);
        param.add(gmt);
        param.add(name);
        param.add(fl);
        // indexToSetParameterMethodAndArgsMap.add("kkkk");
        execute(sql, param);

        sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild pk=" + RANDOM_ID;
        String[] columnParam = {"PK", "ID", "GMT_CREATE", "NAME", "FLOATCOL", "GMT_TIMESTAMP", "GMT_DATETIME"};
        selectOrderAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * insert所有字段 update中带两个字段，其中一个带now()
     */
    @Test
    public void insertFunctions() throws Exception {

        if (normaltblTableName.startsWith("ob")) {
            // ob不支持批量更新
            return;
        }
        String sql = "insert into " + normaltblTableName + " columnValueList(?,?,?,?,?,?,?)";
        List<Object> param = new ArrayList<Object>();
        param.add(RANDOM_ID);
        param.add(RANDOM_INT);
        param.add(gmtDay);
        param.add(gmt);
        param.add(gmt);
        param.add(name);
        param.add(fl);
        execute(sql, param);

        sql = "insert into " + normaltblTableName + " columnValueList(?,?,?,?,?,?,?) ON DUPLICATE KEY update  columnName=?,gmt_create=now()";
        param = new ArrayList<Object>();
        param.add(RANDOM_ID);
        param.add(RANDOM_INT);
        param.add(gmtDay);
        param.add(gmt);
        param.add(gmt);
        param.add(name);
        param.add(fl);
        param.add("kkkk");
        execute(sql, param);

        sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild pk=" + RANDOM_ID;
        String[] columnParam = {"PK", "ID", "GMT_CREATE", "NAME", "FLOATCOL", "GMT_TIMESTAMP", "GMT_DATETIME"};
        selectOrderAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * batch insert所有字段, update中带两个字段，其中一个带now()
     */
    @Test
    public void insertBatchDuplicate() throws Exception {

        if (normaltblTableName.startsWith("ob")) {
            // ob不支持批量更新
            return;
        }
        String sql = "insert into " + normaltblTableName + " columnValueList(?,?,?,?,?,?,?)";

        List<List<Object>> params = new ArrayList();

        for (int i = 0; i < 100; i++) {
            List<Object> param = new ArrayList<Object>();
            param.add(i);
            param.add(i);
            param.add(gmtDay);
            param.add(gmt);
            param.add(gmt);
            param.add(name);
            param.add(fl);

            params.add(param);
        }
        executeBatch(sql, params);

        sql = "insert into " + normaltblTableName + " columnValueList(?,?,?,?,?,?,?) ON DUPLICATE KEY update  columnName=?,gmt_create=now()";

        params = new ArrayList();

        for (int i = 0; i < 100; i++) {
            List<Object> param = new ArrayList<Object>();
            param.add(i);
            param.add(i);
            param.add(gmtDay);
            param.add(gmt);
            param.add(gmt);
            param.add(name);
            param.add(fl);
            param.add(name + i);
            params.add(param);
        }
        executeBatch(sql, params);

        sql = "selectStatement * from " + normaltblTableName;
        String[] columnParam = {"PK", "ID", "GMT_CREATE", "NAME", "FLOATCOL", "GMT_TIMESTAMP", "GMT_DATETIME"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

}
