package drds.plus.qatest.matrix.select.function;

import drds.plus.qatest.BaseMatrixTestCase;
import drds.plus.qatest.BaseTestCase;
import drds.plus.qatest.ExecuteTableName;
import drds.plus.qatest.util.EclipseParameterized;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * 数据函数操作
 */
@RunWith(EclipseParameterized.class)
public class SelectWithMathFunctionTest extends BaseMatrixTestCase {

    public SelectWithMathFunctionTest(String tableName) {
        BaseTestCase.normaltblTableName = tableName;
    }

    @Parameters(name = "{indexMapping}:where={0}")
    public static List<String[]> prepareData() {
        return Arrays.asList(ExecuteTableName.normaltblTable(dbType));
    }

    @Before
    public void prepare() throws Exception {
        normaltblPrepare(0, 20);
    }

    @After
    public void destory() throws Exception {
        psConRcRsClose(rc, rs);
    }

    /**
     * min()
     */
    @Test
    public void minTest() throws Exception {
        String sql = "SELECT min(pk) setAliasAndSetNeedBuild m FROM " + normaltblTableName;
        String[] columnParam = {"m"};
        selectOrderAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "SELECT min(pk) setAliasAndSetNeedBuild m FROM " + normaltblTableName + " setWhereAndSetNeedBuild id>400";
        selectOrderAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * min() setAliasAndSetNeedBuild min
     */
    @Test
    public void minWithAliasTest() throws Exception {
        String sql = "SELECT min(pk) AS min FROM " + normaltblTableName;
        String[] columnParam = {"min"};
        selectOrderAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * max()
     */
    @Test
    public void maxTest() throws Exception {
        String sql = "SELECT max(pk) FROM " + normaltblTableName;
        String[] columnParam = {"max(pk)"};
        selectOrderAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "SELECT max(pk) FROM " + normaltblTableName + " setWhereAndSetNeedBuild id>400";
        selectOrderAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * min(),max()
     */
    @SuppressWarnings("unchecked")
    @Test
    public void maxMinTest() throws Exception {
        String sql = "SELECT max(pk),min(pk) FROM " + normaltblTableName;
        String[] columnParam = {"max(pk)", "min(pk)"};
        rc = null;
        selectOrderAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * sum(pk)
     */
    @Test
    public void sumTest() throws Exception {
        String sql = "SELECT sum(pk) FROM " + normaltblTableName;
        String[] columnParam = {"sum(pk)"};
        selectOrderAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "SELECT sum(pk) FROM " + normaltblTableName + " setWhereAndSetNeedBuild id>400";
        selectOrderAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * sum(float)
     */
    @Test
    public void sumFloatTest() throws Exception {
        String sql = "SELECT sum(floatCol) FROM " + normaltblTableName;
        String[] columnParam = {"sum(floatCol)"};
        selectOrderAssert(sql, columnParam, Collections.EMPTY_LIST);

    }

    /**
     * Sum函数字段中为int或者long类型统一返回long类型
     */
    @Test
    public void sumIntTest() throws Exception {
        String sql = "SELECT sum(id) FROM " + normaltblTableName;
        String[] columnParam = {"sum(id)"};
        selectOrderAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * 统计多个总数
     */
    @Test
    public void sumMutilTest() throws Exception {
        String sql = "SELECT sum(id),sum(pk),sum(floatCol) FROM " + normaltblTableName;
        String[] columnParam = {"sum(id)", "sum(pk)", "sum(floatCol)"};
        selectOrderAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * avg()
     */
    @Test
    public void avgLongTest() throws Exception {
        String sql = "SELECT avg(pk) FROM " + normaltblTableName;
        String[] columnParam = {"avg(PK)"};
        selectOrderAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "SELECT avg(pk) FROM " + normaltblTableName + " setWhereAndSetNeedBuild id >400";
        selectOrderAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * avg(int)
     */
    @Test
    public void avgIntTest() throws Exception {
        String sql = "SELECT avg(id) FROM " + normaltblTableName;
        String[] columnParam = {"avg(id)"};
        selectOrderAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * avg(float)
     */
    @Test
    public void avgFloatTest() throws Exception {
        String sql = "SELECT avg(floatCol) FROM " + normaltblTableName;
        String[] columnParam = {"avg(FLOATCOL)"};
        selectOrderAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * count(long)
     */
    @Test
    public void countTest() throws Exception {
        String sql = "SELECT count(pk) FROM " + normaltblTableName;
        String[] columnParam = {"count(PK)"};
        selectOrderAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * count(float)
     */
    @Test
    public void countNonPKTest() throws Exception {
        String sql = "SELECT count(floatCol) FROM " + normaltblTableName;
        String[] columnParam = {"count(FLOATCOL)"};
        selectOrderAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * count(*)
     */
    @Test
    public void countAllTest() throws Exception {
        String sql = "SELECT count(*) FROM " + normaltblTableName;
        String[] columnParam = {"count(*)"};
        selectOrderAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * count(1)
     */
    @Test
    public void count1Test() throws Exception {
        String sql = "SELECT count(1) FROM " + normaltblTableName;
        String[] columnParam = {"count(1)"};
        selectOrderAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * count(long) + setWhereAndSetNeedBuild
     */
    @Test
    public void countWithWhereTest() throws Exception {
        String sql = "SELECT count(pk) FROM " + normaltblTableName + " setWhereAndSetNeedBuild id>150";
        String[] columnParam = {"count(pk)"};
        selectOrderAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * count(distinct)
     */
    @Test
    public void countWithDistinctTest() throws Exception {
        String sql = "SELECT count(distinct columnName) FROM " + normaltblTableName;
        String[] columnParam = {"count(distinct columnName)"};
        selectOrderAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * count(distinct) + groupby
     */
    @Test
    public void countWithDistinctAndGroupByTest() throws Exception {
        String sql = "/* strict ALLOW_TEMPORARY_TABLE=True */ SELECT count(DISTINCT ID) c FROM " + normaltblTableName + " group by columnName";
        String[] columnParam = {"c"};
        selectOrderAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * count(distinct col1,col2)
     */
    @Test
    public void countWithDistinctMutilCloumnTest() throws Exception {
        if (!normaltblTableName.startsWith("ob")) {
            String sql = "SELECT count(distinct columnName,gmt_create) setAliasAndSetNeedBuild d FROM " + normaltblTableName;
            String[] columnParam1 = {"d"};
            selectOrderAssert(sql, columnParam1, Collections.EMPTY_LIST);
        }
    }

    /**
     * round()
     */
    @Test
    public void roundTest() throws Exception {
        if (!normaltblTableName.startsWith("ob")) {
            String sql = "selectStatement round(floatCol,2) setAliasAndSetNeedBuild a from " + normaltblTableName;
            String[] columnParam = {"a"};
            selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

            sql = "selectStatement round(floatCol) from " + normaltblTableName + " setWhereAndSetNeedBuild id >400";
            String[] columnParam1 = {"round(floatCol)"};
            selectContentSameAssert(sql, columnParam1, Collections.EMPTY_LIST);

            sql = "selectStatement round(id/pk,2) setAliasAndSetNeedBuild a from " + normaltblTableName + " setWhereAndSetNeedBuild columnName=?";
            List<Object> param = new ArrayList<Object>();
            param.add(name);
            String[] columnParam2 = {"a"};
            selectContentSameAssert(sql, columnParam2, param);
        }
    }

    /**
     * interval()
     */
    @Test
    public void intervalTest() throws Exception {
        if (!normaltblTableName.startsWith("ob")) {
            String sql = "selectStatement interval(pk,id) setAliasAndSetNeedBuild d FROM " + normaltblTableName;
            String[] columnParam1 = {"d"};
            selectContentSameAssert(sql, columnParam1, Collections.EMPTY_LIST);
        }
    }

    /**
     * sum() div sum()
     */
    @Test
    public void divTest() throws Exception {
        if (!normaltblTableName.startsWith("ob")) {
            String sql = "selectStatement sum(id) div sum(pk) setAliasAndSetNeedBuild d FROM " + normaltblTableName;
            String[] columnParam1 = {"d"};
            selectContentSameAssert(sql, columnParam1, Collections.EMPTY_LIST);
        }
    }

    /**
     * sum() div sum()
     */
    @Test
    public void divisionTest() throws Exception {
        if (!normaltblTableName.startsWith("ob")) {
            String sql = "selectStatement sum(id) / sum(pk) setAliasAndSetNeedBuild d FROM " + normaltblTableName;
            String[] columnParam1 = {"d"};
            selectContentSameAssert(sql, columnParam1, Collections.EMPTY_LIST);
        }
    }

    /**
     * sum() & sum()
     */
    @Test
    public void bitAndTest() throws Exception {
        if (!normaltblTableName.startsWith("ob")) {
            String sql = "selectStatement sum(id) & sum(pk) setAliasAndSetNeedBuild d FROM " + normaltblTableName;
            String[] columnParam1 = {"d"};
            selectContentSameAssert(sql, columnParam1, Collections.EMPTY_LIST);
        }
    }

    /**
     * sum() | sum()
     */
    @Test
    public void bitOrTest() throws Exception {
        if (!normaltblTableName.startsWith("ob")) {
            String sql = "selectStatement sum(id) | sum(pk) setAliasAndSetNeedBuild d FROM " + normaltblTableName;
            String[] columnParam1 = {"d"};
            selectContentSameAssert(sql, columnParam1, Collections.EMPTY_LIST);
        }
    }

    /**
     * sum() ^ sum()
     */
    @Test
    public void bitXorTest() throws Exception {
        if (!normaltblTableName.startsWith("ob")) {
            String sql = "selectStatement sum(id) ^ sum(pk) setAliasAndSetNeedBuild d FROM " + normaltblTableName;
            String[] columnParam1 = {"d"};
            selectContentSameAssert(sql, columnParam1, Collections.EMPTY_LIST);
        }
    }

    /**
     * sum() >> 2
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void bitLShiftTest() throws Exception {
        if (!normaltblTableName.startsWith("ob")) {
            String sql = "selectStatement sum(id) >> 2 setAliasAndSetNeedBuild d FROM " + normaltblTableName;
            String[] columnParam1 = {"d"};
            selectContentSameAssert(sql, columnParam1, Collections.EMPTY_LIST);
        }

    }

    /**
     * sum() << 2
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void bitRShiftTest() throws Exception {
        if (!normaltblTableName.startsWith("ob")) {
            String sql = "selectStatement sum(id) << 2 setAliasAndSetNeedBuild d FROM " + normaltblTableName;
            String[] columnParam1 = {"d"};
            selectContentSameAssert(sql, columnParam1, Collections.EMPTY_LIST);
        }
    }

}
