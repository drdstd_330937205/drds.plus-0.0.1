package drds.plus.qatest.matrix.select;

import drds.plus.qatest.BaseMatrixTestCase;
import drds.plus.qatest.BaseTestCase;
import drds.plus.qatest.ExecuteTableName;
import drds.plus.qatest.util.EclipseParameterized;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Null相关查询
 *
 * @author zhuoxue
 * @since 5.0.1
 */
@RunWith(EclipseParameterized.class)
public class SelectWithNullTest extends BaseMatrixTestCase {

    String[] columnParam = {"PK", "NAME", "ID"};

    public SelectWithNullTest(String tableName) {
        BaseTestCase.normaltblTableName = tableName;
    }

    @Parameters(name = "{indexMapping}:where={0}")
    public static List<String[]> prepareData() {
        return Arrays.asList(ExecuteTableName.normaltblTable(dbType));
    }

    @Before
    public void prepareDate() throws Exception {
        normaltblNullPrepare(0, 20);
    }

    /**
     * setWhereAndSetNeedBuild columnName is null
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void isNull() throws Exception {
        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild columnName is null";
        selectContentSameAssert(sql, columnParam, null);
    }

    /**
     * setWhereAndSetNeedBuild columnName is not null
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void isNotNull() throws Exception {
        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild columnName is not null";
        selectContentSameAssert(sql, columnParam, null);
    }

    /**
     * 等于null，mysql查询不出结果
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void equalNullTest() throws Exception {
        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild columnName = ?";
        List<Object> param = new ArrayList<Object>();
        param.add(null);
        selectContentSameAssert(sql, columnParam, param);

        sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild columnName =null";
        selectContentSameAssert(sql, columnParam, null);
    }

    /**
     * quote(null)
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    public void quoteNullTest() throws Exception {
        String sql = "selectStatement QUOTE(?) a from " + normaltblTableName;
        String[] columnParam = {"a"};
        List<Object> param = new ArrayList<Object>();
        param.add(null);
        selectContentSameAssert(sql, columnParam, param);

        sql = "selectStatement QUOTE(null) a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, null);
    }

    /**
     * ASCII
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void asciiNULLTest() throws Exception {
        if (!normaltblTableName.startsWith("ob")) {
            String sql = String.format("selectStatement ASCII(columnName) setAliasAndSetNeedBuild a from %s", normaltblTableName);
            String[] columnParam = {"a"};
            selectContentSameAssert(sql, columnParam, null);
        }
    }
}
