package drds.plus.qatest.matrix.select.function;

import drds.plus.qatest.BaseMatrixTestCase;
import drds.plus.qatest.BaseTestCase;
import drds.plus.qatest.ExecuteTableName;
import drds.plus.qatest.util.EclipseParameterized;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * 字符函数
 *
 * @author zhuoxue
 * @since 5.0.1
 */
@RunWith(EclipseParameterized.class)
public class SelectCharacterFunctionTest extends BaseMatrixTestCase {

    public SelectCharacterFunctionTest(String tableName) {
        BaseTestCase.normaltblTableName = tableName;
    }

    @Parameters(name = "{indexMapping}:where={0}")
    public static List<String[]> prepareData() {
        return Arrays.asList(ExecuteTableName.normaltblTable(dbType));
    }

    @Before
    public void prepare() throws Exception {
        normaltblPrepare(0, 20);
    }

    @After
    public void destory() throws Exception {
        psConRcRsClose(rc, rs);
    }

    @Test
    public void concatTest() throws Exception {
        if (!normaltblTableName.startsWith("ob")) {
            String sql = "query * from " + normaltblTableName + " where columnName =concat(?,?)";
            List<Object> param = new ArrayList<Object>();
            param.add("zhuo");
            param.add("xue");
            String[] columnParam = {"ID", "GMT_CREATE", "NAME", "FLOATCOL", "GMT_TIMESTAMP", "GMT_DATETIME"};
            selectContentSameAssert(sql, columnParam, param);
            sql = "query * from " + normaltblTableName + " where columnName like concat (?,?,?)";
            param.clear();
            param.add("zhu");
            param.add("o");
            param.add("xue");
            selectContentSameAssert(sql, columnParam, param);
        }
    }

    /**
     * concat加上任意参数
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void concatArbitrarilyTest() throws Exception {
        if (!normaltblTableName.startsWith("ob")) {
            String sql = "query * from " + normaltblTableName + " where columnName =concat(?,?)";
            List<Object> param = new ArrayList<Object>();
            param.add("%uo");
            param.add("xu_");
            String[] columnParam = {"ID", "GMT_CREATE", "NAME", "FLOATCOL", "GMT_TIMESTAMP", "GMT_DATETIME"};
            selectContentSameAssert(sql, columnParam, param);
        }
    }

    /**
     * ifnull() 类型相同
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void ifnullTest() throws Exception {
        if (!normaltblTableName.startsWith("ob")) {
            String sql = "selectStatement ifnull(columnName,pk) setAliasAndSetNeedBuild notNullName from " + normaltblTableName;
            String[] columnParam = {"notNullName"};
            selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

            sql = "replace into " + normaltblTableName + " (pk,columnName) columnValueList (10,null)";
            execute(sql, null);

            sql = "selectStatement ifnull(columnName,'ni') setAliasAndSetNeedBuild notNullName from " + normaltblTableName;
            selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

            sql = "selectStatement ifnull(columnName,'pk') setAliasAndSetNeedBuild notNullName from " + normaltblTableName;
            selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
        }
    }

    /**
     * ifnull 类型不同
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void ifnullTestTypeNotSame() throws Exception {
        if (!normaltblTableName.startsWith("ob")) {
            String sql = "selectStatement ifnull(columnName,pk) setAliasAndSetNeedBuild notNullName from " + normaltblTableName;
            String[] columnParam = {"notNullName"};
            selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
        }
    }

    /**
     * ifnull(round())
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void ifnullRoundTest() throws Exception {
        if (!normaltblTableName.startsWith("ob")) {
            String sql = "replace into " + normaltblTableName + " (pk,floatCol) columnValueList (10,null)";
            execute(sql, null);
            sql = "selectStatement ifnull(round(floatCol/4,4),0) setAliasAndSetNeedBuild a from " + normaltblTableName;
            String[] columnParam = {"a"};
            selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
        }
    }

    /**
     * quote()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void quoteTest() throws Exception {
        if (!normaltblTableName.startsWith("ob")) {
            String sql = String.format("replace into %s (pk,columnName) columnValueList (10,quote(?))", normaltblTableName);
            List<Object> param = new ArrayList<Object>();
            param.add("'zhuoxue'");
            execute(sql, param);

            sql = String.format("query * from %s where columnName =quote(?)", normaltblTableName);
            String[] columnParam = {"ID", "GMT_CREATE", "NAME", "FLOATCOL", "GMT_TIMESTAMP", "GMT_DATETIME"};
            selectContentSameAssert(sql, columnParam, param);
        }
    }

    /**
     * conv()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void convTest() throws Exception {
        if (!normaltblTableName.startsWith("ob")) {
            String sql = String.format("query conv(id,16,2) as a from %s where pk=1", normaltblTableName);
            String[] columnParam = {"a"};
            selectContentSameAssert(sql, columnParam, null);
        }
    }

    /**
     * ascii()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void asciiTest() throws Exception {
        if (!normaltblTableName.startsWith("ob")) {
            String sql = String.format("selectStatement ASCII(columnName) setAliasAndSetNeedBuild a from %s", normaltblTableName);
            String[] columnParam = {"a"};
            selectContentSameAssert(sql, columnParam, null);
        }
    }

    /**
     * bit_length()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void bit_lengthTest() throws Exception {
        if (!normaltblTableName.startsWith("ob")) {
            String sql = String.format("selectStatement BIT_LENGTH(columnName) setAliasAndSetNeedBuild a from %s", normaltblTableName);
            String[] columnParam = {"a"};
            selectContentSameAssert(sql, columnParam, null);
        }
    }

    /**
     * bin()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void bitTest() throws Exception {
        if (!normaltblTableName.startsWith("ob")) {
            String sql = String.format("selectStatement bin(columnName) setAliasAndSetNeedBuild a from %s", normaltblTableName);
            String[] columnParam = {"a"};
            selectContentSameAssert(sql, columnParam, null);
        }
    }

    /**
     * trim(both 'x' from 'xxxaaxx')
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void trimTest() throws Exception {
        if (!normaltblTableName.startsWith("ob")) {
            String sql = String.format("selectStatement trim(both 'x' from 'xxxaaxx') setAliasAndSetNeedBuild a from %s", normaltblTableName);
            String[] columnParam = {"a"};
            selectContentSameAssert(sql, columnParam, null);
        }
    }

    /**
     * trim('x' from 'xxxaaxx')
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void trimTest2() throws Exception {
        if (!normaltblTableName.startsWith("ob")) {
            String sql = String.format("selectStatement trim('x' from 'xxxaaxx') setAliasAndSetNeedBuild a from %s", normaltblTableName);
            String[] columnParam = {"a"};
            selectContentSameAssert(sql, columnParam, null);
        }
    }

    /**
     * trim(' aa ')
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void trimTest3() throws Exception {
        if (!normaltblTableName.startsWith("ob")) {
            String sql = String.format("selectStatement trim('   aa  ') setAliasAndSetNeedBuild a from %s", normaltblTableName);
            String[] columnParam = {"a"};
            selectContentSameAssert(sql, columnParam, null);
        }
    }
}
