package drds.plus.qatest.matrix.select.function;

import drds.plus.qatest.BaseMatrixTestCase;
import drds.plus.qatest.BaseTestCase;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;

/**
 * 函数测试
 *
 * @author zhuoxue
 * @since 5.0.1
 */
public class FunctionTest extends BaseMatrixTestCase {

    public FunctionTest() {
        BaseTestCase.normaltblTableName = "_tddl_";
    }

    @Before
    public void prepare() throws Exception {
        demoRepoPrepare(0, 20);
    }

    @After
    public void destory() throws Exception {
        psConRcRsClose(rc, rs);
    }

    /**
     * abs()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void absTest() throws Exception {
        String sql = "selectStatement abs(2) setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement abs(-32) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * acos()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void acosTest() throws Exception {
        String sql = "selectStatement acos(1) setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement acos(1.0001) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement acos(0) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * atan()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void atanTest() throws Exception {
        String sql = "selectStatement atan(2) setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement atan(-2) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement atan(-2,2) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement atan2(pi(),0) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * ceil() atan()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void ceilTest() throws Exception {
        String sql = "selectStatement ceil(1.23) setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement atan(-1.23) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * conv()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void convTest() throws Exception {
        String sql = "selectStatement conv('a',16,2) setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement conv('6E',18,8) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement conv(-17,10,-18) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement conv(10+'10'+'10'+10,10,10) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * cos
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void cosTest() throws Exception {
        String sql = "selectStatement cos(pi()) setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement cos(2.2) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * cot()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void cotTest() throws Exception {
        String sql = "selectStatement cot(12) setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement cot(0) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * crc32()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void crc32Test() throws Exception {
        String sql = "selectStatement CRC32('MySQL') setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement CRC32('mysql') setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * degrees()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void degreesTest() throws Exception {
        String sql = "selectStatement DEGREES(PI()) setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement DEGREES(PI()/2) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * exp()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void expTest() throws Exception {
        String sql = "selectStatement EXP(2) setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement EXP(-2) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement EXP(0) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * floor()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void floorTest() throws Exception {
        String sql = "selectStatement FLOOR(1.23) setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement FLOOR(-1.23) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * ln()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void lnTest() throws Exception {
        String sql = "selectStatement ln(2) setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement ln(-2) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * log()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void logTest() throws Exception {
        String sql = "selectStatement log(2) setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement log(-2) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement log(2,65536) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement log(10,100) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement log(1,100) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * log2()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void log2Test() throws Exception {
        String sql = "selectStatement log2(65536) setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement log2(-100) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * log10()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void log10Test() throws Exception {
        String sql = "selectStatement LOG10(2) setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement LOG10(100) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement LOG10(-100) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * mod()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void modTest() throws Exception {
        String sql = "selectStatement mod(234, 10) setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement 253 % 7 setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement mod(29,9) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement 29 mod 9 setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement mod(34.5,3) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement mod(100,0) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * pi()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void piTest() throws Exception {
        String sql = "selectStatement pi() setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement PI()+0.000000000000000000 setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * pow()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void powTest() throws Exception {
        String sql = "selectStatement POW(2,2) setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement POWER(2,-2) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * radians()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void radiansTest() throws Exception {
        String sql = "selectStatement radians(90) setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement radians(45) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * round()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void roundTest() throws Exception {
        String sql = "selectStatement ROUND(-1.23) setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement ROUND(-1.58) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement ROUND(1.58) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement ROUND(1.298, 1) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement ROUND(1.298, 0) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement ROUND(23.298, -1) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement ROUND(150.000,2) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement ROUND(150,2) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement ROUND(2.5) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        // chars = "selectStatement ROUND(25E-1) setAliasAndSetNeedBuild a from " +
        // normaltblTableName;
        // selectContentSameAssert(chars, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * sign()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void signTest() throws Exception {
        String sql = "selectStatement SIGN(-32) setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement SIGN(0) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement SIGN(234) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * sin(pin())
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void sinTest() throws Exception {
        String sql = "selectStatement SIN(PI()) setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement ROUND(SIN(PI())) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * sqrt()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void sqrtTest() throws Exception {
        String sql = "selectStatement SQRT(4) setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement SQRT(20) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement SQRT(-16) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * tan(pi())
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void tanTest() throws Exception {
        String sql = "selectStatement TAN(PI()) setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement TAN(PI()+1) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * truncate()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void truncateTest() throws Exception {
        String sql = "selectStatement TRUNCATE(1.223,1) setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement TRUNCATE(1.999,1) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement TRUNCATE(1.999,0) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement TRUNCATE(-1.999,1) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement TRUNCATE(122,-2) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement TRUNCATE(10.28*100,0) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * case when
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void caseTest() throws Exception {
        String sql = "selectStatement  CASE 1 WHEN 1 THEN 'one' WHEN 2 THEN 'two' ELSE 'more' END setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement CASE WHEN 1>0 THEN 'true' ELSE 'false' END setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement CASE 'b' WHEN 'a' THEN 1 WHEN 'b' THEN 2 END setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * if()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void ifTest() throws Exception {
        String sql = "selectStatement IF(1>2,2,3) setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement IF(1<2,'yes','no') setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement IF('test' = 'test1','no','yes') setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * ifnull()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void ifNullTest() throws Exception {
        String sql = "selectStatement IFNULL(1,0) setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement IFNULL(NULL_KEY,10) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement IFNULL(NULL_KEY,NULL_KEY) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement IFNULL(1/0,10) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement IFNULL(1/0,'yes') setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * nullif
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void nullIfTest() throws Exception {
        String sql = "selectStatement NULLIF(1,1) setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement NULLIF(1,2) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement NULLIF(null,1) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement NULLIF(1,null) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * date_add()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void adddateTest() throws Exception {
        String sql = "selectStatement DATE_ADD('2008-01-02 00:00:00', interval_primary 31 DAY) setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement '2008-12-31 23:59:59' + interval_primary 1 SECOND setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement interval_primary 1 DAY + '2008-12-31 00:00:00' setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement '2005-01-01 00:00:00' - interval_primary 1 SECOND setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement DATE_ADD('2000-12-31 23:59:59', interval_primary 1 SECOND) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement DATE_ADD('2010-12-31 23:59:59',interval_primary 1 DAY) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement DATE_SUB('2005-01-01 00:00:00',interval_primary '1 1:1:1' DAY_SECOND) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement DATE_ADD('1900-01-01 00:00:00',interval_primary '1 10' DAY_HOUR) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement DATE_SUB('1998-01-02 00:00:00', interval_primary 31 DAY) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * dayname（）
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void daynameTest() throws Exception {
        String sql = "selectStatement DAYNAME('2007-02-03') setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * dayofmonth()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void dayofmonthTest() throws Exception {
        String sql = "selectStatement DAYOFMONTH('2007-02-03') setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * dayofweek()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void dayofweekTest() throws Exception {
        String sql = "selectStatement DAYOFWEEK('2007-02-03') setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * dayofyear()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void dayofyearTest() throws Exception {
        String sql = "selectStatement DAYOFYEAR('2007-02-03') setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * from_days
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void fromdaysTest() throws Exception {
        String sql = "selectStatement FROM_DAYS(730669) setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * hour()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void hourTest() throws Exception {
        String sql = "selectStatement HOUR('10:05:03') setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * last_day()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void lastDayTest() throws Exception {
        String sql = "selectStatement LAST_DAY('2003-02-05 00:00:00') setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement LAST_DAY('2004-02-05 00:00:00') setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement LAST_DAY('2004-01-01 00:00:00') setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement LAST_DAY('2003-03-31 00:00:00') setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * month()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void monthTest() throws Exception {
        String sql = "selectStatement MONTH('2008-02-03') setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement MONTHNAME('2004-02-05 00:00:00') setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * quarter()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void quarterTest() throws Exception {
        String sql = "selectStatement QUARTER('2008-04-01') setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * second()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void secondTest() throws Exception {
        String sql = "selectStatement SECOND('10:05:03') setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        // chars = "selectStatement SEC_TO_TIME(2378) setAliasAndSetNeedBuild a from " +
        // normaltblTableName;
        // selectContentSameAssert(chars, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * weekday()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void weekTest() throws Exception {
        String sql = "selectStatement WEEKDAY('2008-02-03 22:23:00') setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement WEEKDAY('2007-11-06') setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement WEEKOFYEAR('2008-02-20') setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        // chars = "selectStatement WEEK('2008-02-20') setAliasAndSetNeedBuild a from " +
        // normaltblTableName;
        // selectContentSameAssert(chars, columnParam, Collections.EMPTY_LIST);
        //
        // chars = "selectStatement SELECT WEEK('2008-02-20',0) setAliasAndSetNeedBuild a
        // from " +
        // normaltblTableName;
        // selectContentSameAssert(chars, columnParam, Collections.EMPTY_LIST);
        //
        // chars = "selectStatement SELECT WEEK('2008-02-20',1) setAliasAndSetNeedBuild a
        // from " +
        // normaltblTableName;
        // selectContentSameAssert(chars, columnParam, Collections.EMPTY_LIST);
        //
        // chars = "selectStatement SELECT WEEK('2008-12-31',1) setAliasAndSetNeedBuild a
        // from " +
        // normaltblTableName;
        // selectContentSameAssert(chars, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * year()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void yearTest() throws Exception {
        String sql = "selectStatement YEAR('1987-01-01') setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        // chars = "selectStatement YEARWEEK('1987-01-01') setAliasAndSetNeedBuild a from
        // " +
        // normaltblTableName;
        // selectContentSameAssert(chars, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * time_to_sec()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void timeToSecondsTest() throws Exception {
        String sql = "selectStatement TIME_TO_SEC('22:23:00') setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement TIME_TO_SEC('00:39:38') setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * timestampadd()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void timestampaddTest() throws Exception {
        String sql = "selectStatement timestampadd(MINUTE,1,'2003-01-02 00:00:00') setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement timestampadd(WEEK,1,'2003-01-02 00:00:00') setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * timestamp()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void timestampTest() throws Exception {
        String sql = "selectStatement TIMESTAMP('2003-12-31') setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        // chars = "selectStatement TIMESTAMP('2003-12-31
        // 12:00:00','12:00:00') setAliasAndSetNeedBuild a from "
        // + normaltblTableName;
        // selectContentSameAssert(chars, columnParam, Collections.EMPTY_LIST);

        // chars = "selectStatement TIME('2003-12-31 01:02:03') setAliasAndSetNeedBuild a
        // from " +
        // normaltblTableName;
        // selectContentSameAssert(chars, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * subtime()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void subtimeTest() throws Exception {
        String sql = "selectStatement SUBTIME('2007-12-31 23:59:59','1:1:1') setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        // chars = "selectStatement TIMESTAMP('2003-12-31
        // 12:00:00','12:00:00') setAliasAndSetNeedBuild a from "
        // + normaltblTableName;
        // selectContentSameAssert(chars, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * date_format()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void dataFormatTest() throws Exception {
        String sql = "selectStatement DATE_FORMAT('2009-10-04 22:23:00', '%W %M %Y') setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement DATE_FORMAT('2007-10-04 22:23:00', '%H:%i:%s') setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement DATE_FORMAT('1997-10-04 22:23:00','%H %k %I %readWeight %T %S') setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement DATE_FORMAT('2006-06-01', '%d') setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement DATE_FORMAT('2003-10-03',get_format(DATE,'EUR')) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement STR_TO_DATE('10.31.2003',get_format(DATE,'USA')) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * from_unixtime()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void fromUnixtimeTest() throws Exception {
        // String chars =
        // "selectStatement FROM_UNIXTIME(UNIX_TIMESTAMP(),'%Y-%d-%m
        // %H:%i:%s') setAliasAndSetNeedBuild a from "
        // + normaltblTableName;
        // String[] columnParam = { "a" };
        // selectContentSameAssert(chars, columnParam, Collections.EMPTY_LIST);

        // chars = "selectStatement FROM_UNIXTIME(1196440219) setAliasAndSetNeedBuild a
        // from " +
        // normaltblTableName;
        // selectContentSameAssert(chars, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * makedate()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void makeDateTest() throws Exception {
        String sql = "selectStatement MAKEDATE(2011,31) setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement MAKEDATE(2011,32) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement MAKEDATE(2011,365) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement MAKEDATE(2014,365) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement MAKEDATE(2011,0) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        // chars = "selectStatement MAKETIME(12,15,30) setAliasAndSetNeedBuild a from " +
        // normaltblTableName;
        // selectContentSameAssert(chars, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * time_format()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void timeFormatTest() throws Exception {
        String sql = "selectStatement TIME_FORMAT('23:00:00', '%H %k %h %I %l') setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
        // chars = "selectStatement MAKETIME(12,15,30) setAliasAndSetNeedBuild a from " +
        // normaltblTableName;
        // selectContentSameAssert(chars, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * datediff()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void dataDiffTest() throws Exception {
        String sql = "selectStatement  DATEDIFF('2007-12-31 23:59:59','2007-12-30') setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement DATEDIFF('2010-11-30 23:59:59','2010-12-31') setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * period_add()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void peroidAddTest() throws Exception {
        String sql = "selectStatement PERIOD_ADD(200801,2) setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement PERIOD_ADD(200801,-2) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * period_diff()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void peroidDiffTest() throws Exception {
        String sql = "selectStatement PERIOD_DIFF(200802,200703) setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement PERIOD_DIFF(200703,200802) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * extract()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void extractTTest() throws Exception {
        String sql = "SELECT extract(MICROSECOND FROM '2009-07-02 01:02:03') setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement extract(SECOND FROM '2009-07-02 01:02:03') setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement extract(MINUTE FROM '2009-07-02 01:02:03') setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement extract(HOUR FROM '2009-07-02 01:02:03') setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement extract(DAY FROM '2009-07-02 01:02:03') setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        // chars = "selectStatement extract(WEEK FROM '2009-07-02
        // 01:02:03') setAliasAndSetNeedBuild a from " +
        // normaltblTableName;
        // selectContentSameAssert(chars, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement extract(MONTH FROM '2009-07-02 01:02:03') setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement extract(QUARTER FROM '2009-07-02 01:02:03') setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement extract(YEAR FROM '2009-07-02 01:02:03') setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement extract(SECOND_MICROSECOND FROM '2009-07-02 01:02:03') setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement extract(MINUTE_MICROSECOND FROM '2009-07-02 01:02:03') setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement extract(MINUTE_SECOND FROM '2009-07-02 01:02:03') setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement extract(HOUR_MICROSECOND FROM '2009-07-02 01:02:03') setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement extract(HOUR_SECOND FROM '2009-07-02 01:02:03') setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement extract(HOUR_MINUTE FROM '2009-07-02 01:02:03') setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement extract(DAY_MICROSECOND FROM '2009-07-02 01:02:03') setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement extract(DAY_SECOND FROM '2009-07-02 01:02:03') setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement extract(DAY_MINUTE FROM '2009-07-02 01:02:03') setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement extract(DAY_HOUR FROM '2009-07-02 01:02:03') setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement extract(YEAR_MONTH FROM '2009-07-02 01:02:03') setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * interval()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void intervalTest() throws Exception {
        String sql = "SELECT interval_primary(23, 1, 15, 17, 30, 44, 200) setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement interval_primary(10, 1, 10, 100, 1000) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement interval_primary(22, 23, 30, 44, 200) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement interval_primary(202, 23, 30, 44, 200) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * least() greatest()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void greatestTest() throws Exception {
        String sql = "SELECT LEAST(2,0) setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement LEAST(34.0,3.0,5.0,767.0) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement LEAST('B','A','C') setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement GREATEST(2,0) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement GREATEST(34.0,3.0,5.0,767.0) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement GREATEST('B','A','C') setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * coalesce()
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void CoalesceTest() throws Exception {
        String sql = "SELECT COALESCE(NULL_KEY,'a') setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement COALESCE(NULL_KEY,1,'1') setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement COALESCE(NULL_KEY,NULL_KEY,NULL_KEY) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * cast test
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void CastTest() throws Exception {
        String sql = "SELECT cast('123' setAliasAndSetNeedBuild binary) setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "SELECT cast('123' setAliasAndSetNeedBuild binary(2)) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "SELECT cast('123' setAliasAndSetNeedBuild binary(4)) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "SELECT cast('123' setAliasAndSetNeedBuild char(2)) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "SELECT cast('123' setAliasAndSetNeedBuild char(4)) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "SELECT cast('123.35' setAliasAndSetNeedBuild decimal(4)) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "SELECT cast('123.35' setAliasAndSetNeedBuild decimal(5,2)) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "SELECT cast('123.35' setAliasAndSetNeedBuild decimal) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "SELECT cast('123.35' setAliasAndSetNeedBuild decimal(0,0)) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "SELECT cast('-1' setAliasAndSetNeedBuild unsigned) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "SELECT cast('9223372036854775808' setAliasAndSetNeedBuild signed) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "SELECT cast(cast(1-2 AS UNSIGNED) AS SIGNED) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "SELECT cast('2012-01-01' setAliasAndSetNeedBuild date) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "SELECT cast('12:12:12' setAliasAndSetNeedBuild time) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "SELECT cast('2012-01-01 12:12:12' setAliasAndSetNeedBuild datetime) setAliasAndSetNeedBuild a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }
}
