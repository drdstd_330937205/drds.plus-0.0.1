package drds.plus.qatest.matrix.set;

import drds.plus.api.SessionImpl;
import drds.plus.qatest.BaseMatrixTestCase;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.sql.ResultSet;
import java.util.Collections;

public class SetSqlModeTest extends BaseMatrixTestCase {

    // HIGH_NOT_PRECEDENCE
    @Test
    public void testSet_HIGH_NOT_PRECEDENCE() throws Exception {
        setSqlMode("HIGH_NOT_PRECEDENCE");
        String sql = "SELECT NOT 1 BETWEEN -5 and 5 setAliasAndSetNeedBuild col";
        ResultSet rs = tddlQueryData(sql, null);
        Assert.assertTrue(rs.next());
        Assert.assertTrue(rs.getString("col").equals("1"));
        Assert.assertFalse(rs.next());

        setSqlMode(" ");
        sql = "SELECT NOT 1 BETWEEN -5 and 5 setAliasAndSetNeedBuild col";
        rs = tddlQueryData(sql, null);
        Assert.assertTrue(rs.next());
        Assert.assertTrue(rs.getString("col").equals("0"));
        Assert.assertFalse(rs.next());
    }

    // IGNORE_SPACE
    @Test
    public void testSet_IGNORE_SPACE() throws Exception {
        setSqlMode("IGNORE_SPACE");
        String sql = "selectStatement count (*) from MYSQL_NORMALTBL_ONEGROUP_ONEATOM";
        try {
            ResultSet rs = tddlQueryData(sql, null);
            while (rs.next()) {

            }
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }

        setSqlMode(" ");
        sql = "selectStatement count (*) from MYSQL_NORMALTBL_ONEGROUP_ONEATOM";
        try {
            ResultSet rs = tddlQueryData(sql, null);
            while (rs.next()) {

            }
            Assert.fail();
        } catch (Exception e) {
            Assert.assertTrue(e.getMessage().contains("execute error by You have an error in your SQL sqlParseManager"));
        }
    }

    // STRICT_TRANS_TABLES
    @Test
    public void testSet_STRICT_TRANS_TABLES() throws Exception {
        execute("delete from mysql_normaltbl_mutilgroup", null);

        // name字段值超长，strict模式应该报错
        setSqlMode("STRICT_TRANS_TABLES");
        String sql = "insert into mysql_normaltbl_mutilgroup(pk,columnName) columnValueList(1,'abcdeabcdeabcdeabcdeabcde')";
        try {
            tddlUpdateData(sql, null);
            Assert.fail();
        } catch (Exception e) {
            Assert.assertTrue(e.getMessage().contains("Data too long for column 'columnName'"));
        }

        setSqlMode(" ");
        sql = "insert into mysql_normaltbl_mutilgroup(pk,columnName) columnValueList(1,'abcdeabcdeabcdeabcdeabcde')";
        try {
            tddlUpdateData(sql, null);
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }

        sql = "selectStatement * from mysql_normaltbl_mutilgroup";
        ResultSet rs = tddlQueryData(sql, null);
        Assert.assertTrue(rs.next());
        Assert.assertTrue(rs.getString("columnName").equals("abcdeabcdeabcdeabcde"));
        Assert.assertFalse(rs.next());
    }

    // ANSI
    @Ignore("暂不支持")
    @Test
    public void testSet_ANSI() throws Exception {
        setSqlMode("ANSI");
        String sql = "SELECT * FROM mysql_normaltbl_mutilgroup t1 WHERE t1.id in " + "(SELECT max(t1.id) FROM mysql_normaltbl_onegroup_oneatom )";
        try {
            tddlQueryData(sql, null);
            Assert.fail();
        } catch (Exception e) {
            Assert.assertTrue(e.getMessage().contains("execute error by You have an error in your SQL sqlParseManager"));
        }

        setSqlMode(" ");
        sql = "SELECT * FROM mysql_normaltbl_mutilgroup t1 WHERE t1.id in " + "(SELECT max(t1.id) FROM mysql_normaltbl_onegroup_oneatom )";
        try {
            String[] columnParam = new String[]{"PK", "ID", "GMT_CREATE", "NAME", "FLOATCOL", "GMT_TIMESTAMP", "GMT_DATETIME"};
            selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
    }

    private void setSqlMode(String mode) throws Exception {
        if (isTddlServer()) {
            String sql = "SET session sql_mode = '" + mode + "'";
            tddlUpdateData(sql, null);
        } else {
            ((SessionImpl) tddlConnection).setSqlMode(mode);
        }
    }
}
