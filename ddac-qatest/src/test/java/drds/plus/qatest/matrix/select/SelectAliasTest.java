package drds.plus.qatest.matrix.select;

import drds.plus.qatest.BaseMatrixTestCase;
import drds.plus.qatest.BaseTestCase;
import drds.plus.qatest.ExecuteTableName;
import drds.plus.qatest.util.EclipseParameterized;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * select别名
 */
@RunWith(EclipseParameterized.class)
public class SelectAliasTest extends BaseMatrixTestCase {

    long pk = 1l;
    int id = 1;

    public SelectAliasTest(String normaltblTableName, String studentTableName) {
        BaseTestCase.normaltblTableName = normaltblTableName;
        BaseTestCase.studentTableName = studentTableName;
    }

    @Parameters(name = "{indexMapping}:table0={0},table1={1}")
    public static List<String[]> prepare() {
        return Arrays.asList(ExecuteTableName.normaltblStudentTable(dbType));
    }

    @Before
    public void prepareData() throws Exception {
        normaltblPrepare(0, MAX_DATA_SIZE);
        studentPrepare(0, MAX_DATA_SIZE);
    }

    /**
     * selectStatement 表别名
     */
    @Test
    public void aliasTableTest() throws Exception {
        String sql = "selectStatement * from  " + normaltblTableName + "  nor where nor.pk=?";
        sql = String.format("selectStatement * from  %s nor where nor.pk=?", normaltblTableName);
        List<Object> param = new ArrayList<Object>();
        param.add(pk);
        String[] columnParam = {"PK", "NAME", "ID"};
        assertAlias(sql, columnParam, "nor", param);
    }

    /**
     * select域常量别名
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void aliasScalarTest() throws Exception {
        String sql = "selectStatement 1 setAliasAndSetNeedBuild id from  " + normaltblTableName + " nor limit 1";
        String[] columnParam = {"ID"};
        assertAlias(sql, columnParam, "nor", null);
    }

    /**
     * selectStatement 表别名 where中用索引
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void aliasTableTestWithIndexQuery() throws Exception {
        String sql = "selectStatement * from  " + normaltblTableName + "  nor where nor.id=?";
        List<Object> param = new ArrayList<Object>();
        param.add(id);
        String[] columnParam = {"PK", "NAME", "ID"};
        assertAlias(sql, columnParam, "nor", param);
    }

    /**
     * selectStatement 带as表别名
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void aliasWithAsTableTest() throws Exception {
        String sql = "selectStatement * from  " + normaltblTableName + "  setAliasAndSetNeedBuild nor where nor.pk=?";
        List<Object> param = new ArrayList<Object>();
        param.add(pk);

        String[] columnParam = {"PK", "NAME", "ID"};
        assertAlias(sql, columnParam, "nor", param);
    }

    /**
     * selectStatement 列别名
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void aliasFiledTest() throws Exception {
        String sql = "selectStatement columnName xingming ,id pid ,pk ppk from  " + normaltblTableName + "  where pk=?";
        List<Object> param = new ArrayList<Object>();
        param.add(pk);

        String[] columnParam = {"ppk", "pid", "xingming"};
        assertAlias(sql, columnParam, "nor", param);
    }

    /**
     * selectStatement 带as列别名
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void aliasFieldWithAsTest() throws Exception {
        String sql = "selectStatement columnName setAliasAndSetNeedBuild xingming ,id setAliasAndSetNeedBuild pid from  " + normaltblTableName + "  where pk=?";
        List<Object> param = new ArrayList<Object>();
        param.add(pk);
        String[] columnParam = {"xingming", "pid"};
        assertAlias(sql, columnParam, "nor", param);
    }

    /**
     * selectStatement 表别名 列别名
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void aliasFiledTableTest() throws Exception {
        String sql = "selectStatement columnName xingming ,id pid from  " + normaltblTableName + "  setAliasAndSetNeedBuild nor where pk=?";
        List<Object> param = new ArrayList<Object>();
        param.add(pk);

        String[] columnParam = {"xingming", "pid"};
        assertAlias(sql, columnParam, "nor", param);
    }

    /**
     * selectStatement 表别名 列别名上带as
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void aliasFiledTableTest1() throws Exception {
        String sql = "selectStatement columnName setAliasAndSetNeedBuild xingming ,id pid from  " + normaltblTableName + "  nor where pk=?";
        List<Object> param = new ArrayList<Object>();
        param.clear();
        param.add(pk);
        String[] columnParam = {"xingming", "pid"};
        assertAlias(sql, columnParam, "nor", param);
    }

    /**
     * selectStatement 表别名 join查询
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void aliasTableWithJoinTest() throws Exception {
        String sql = "selectStatement n.columnName,s.columnName studentName,n.pk,s.id from  " + normaltblTableName + "  n , " + studentTableName + "  s where n.pk=s.id";
        String[] columnParam = {"columnName", "studentName", "pk", "id"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement n.columnName,s.columnName studentName,n.pk,s.id from  " + normaltblTableName + "  setAliasAndSetNeedBuild n , " + studentTableName + "  AS s where n.pk=s.id";
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * selectStatement 列别名 join查询
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void aliasWithFieldJoinTest() throws Exception {
        String sql = "selectStatement  " + normaltblTableName + ".columnName name1, " + studentTableName + ".columnName, " + normaltblTableName + ".pk pk1, " + studentTableName + ".id from  " + normaltblTableName + " , " + studentTableName + " where  " + normaltblTableName + ".pk= " + studentTableName + ".id";
        String[] columnParam = {"name1", "columnName", "pk1", "id"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement  " + normaltblTableName + ".columnName setAliasAndSetNeedBuild name1, " + studentTableName + ".columnName, " + normaltblTableName + ".pk setAliasAndSetNeedBuild pk1, " + studentTableName + ".id from  " + normaltblTableName + " , " + studentTableName + " where  " + normaltblTableName + ".pk= " + studentTableName + ".id";
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * selectStatement 表别名 列别名 join查询
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void aliasWithFieldTableJoinTest() throws Exception {
        String sql = "selectStatement n.columnName name1, " + studentTableName + ".columnName,n.pk pk1, " + studentTableName + ".id from  " + normaltblTableName + "  n, " + studentTableName + " where n.pk= " + studentTableName + ".id";
        String[] columnParam = {"pk1", "id", "name1", "columnName"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement n.columnName setAliasAndSetNeedBuild name1, " + studentTableName + ".columnName,n.pk pk1, " + studentTableName + ".id from  " + normaltblTableName + "  setAliasAndSetNeedBuild n, " + studentTableName + " where n.pk= " + studentTableName + ".id";
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * selectStatement 列别名 groupby
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void aliasWithGroupByTest() throws Exception {
        String sql = "selectStatement count(pk) ,columnName as n from  " + normaltblTableName + "   group by n";
        String[] columnParam = {"count(pk)", "n"};
        try {
            selectOrderAssert(sql, columnParam, Collections.EMPTY_LIST);
        } catch (Exception ex) {
            System.out.print(ex);
        }
    }

    /**
     * selectStatement 列别名 orderby
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void aliasWithOrderByTest() throws Exception {
        String sql = "selectStatement pk setAliasAndSetNeedBuild readPriority ,columnName setAliasAndSetNeedBuild n from  " + normaltblTableName + "   order by readPriority asc";
        String[] columnParam = {"readPriority", "n"};
        selectOrderAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * selectStatement count函数别名
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @SuppressWarnings("unchecked")
    @Test
    public void aliasWithFuncByTest() throws Exception {
        String sql = "selectStatement count(pk) setAliasAndSetNeedBuild con from  " + normaltblTableName + " ";
        String[] columnParam = {"con"};
        selectOrderAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * selectStatement sum函数别名
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void aliasWithFuncWithGroupBy() throws Exception {
        String sql = "selectStatement id ,sum(pk) as readPriority from " + normaltblTableName + " as a group by id";
        String[] columnParam = {"id", "readPriority"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * selectStatement 子查询别名
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    // TODO(目前子查询不支持)
    @Test
    public void aliasWithSubQueryTest() throws Exception {

    }

}
