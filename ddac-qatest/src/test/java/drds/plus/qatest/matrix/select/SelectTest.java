package drds.plus.qatest.matrix.select;

import drds.plus.qatest.BaseMatrixTestCase;
import drds.plus.qatest.BaseTestCase;
import drds.plus.qatest.ExecuteTableName;
import drds.plus.qatest.util.EclipseParameterized;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * LocalServerSelectTest
 */
@RunWith(EclipseParameterized.class)
public class SelectTest extends BaseMatrixTestCase {

    public SelectTest(String studentTableName) {
        BaseTestCase.studentTableName = studentTableName;
    }

    @Parameters(name = "{indexMapping}:where={0}")
    public static List<String[]> prepare() {
        return Arrays.asList(ExecuteTableName.studentTable(dbType));
    }

    @Before
    public void initData() throws Exception {
        tddlUpdateData("delete from  " + studentTableName, null);
        mysqlUpdateData("delete from  " + studentTableName, null);
    }

    /**
     * 查询所有列
     */
    @Test
    public void selectAllFieldTest() throws Exception {
        tddlUpdateData("insert into " + studentTableName + " (id,columnName,school) columnValueList (?,?,?)", Arrays.asList(new Object[]{RANDOM_ID, name, school}));
        mysqlUpdateData("insert into " + studentTableName + " (id,columnName,school) columnValueList (?,?,?)", Arrays.asList(new Object[]{RANDOM_ID, name, school}));
        String sql = "selectStatement * from " + studentTableName + " setWhereAndSetNeedBuild id=" + RANDOM_ID;
        String[] columnParam = {"NAME", "SCHOOL"};
        selectOrderAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * 查询所有列 带count(*)
     */
    @Test
    public void selectAllFieldWithFuncTest() throws Exception {
        tddlUpdateData("insert into " + studentTableName + " (id,columnName,school) columnValueList (?,?,?)", Arrays.asList(new Object[]{RANDOM_ID, name, school}));
        mysqlUpdateData("insert into " + studentTableName + " (id,columnName,school) columnValueList (?,?,?)", Arrays.asList(new Object[]{RANDOM_ID, name, school}));
        String sql = "selectStatement *,count(*) from " + studentTableName + " setWhereAndSetNeedBuild id=" + RANDOM_ID;
        String[] columnParam = {"NAME", "SCHOOL", "id", "count(*)"};
        selectOrderAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * 查询部分列
     */
    @Test
    public void selectSomeFieldTest() throws Exception {
        tddlUpdateData("insert into " + studentTableName + " (id,columnName,school) columnValueList (?,?,?)", Arrays.asList(new Object[]{RANDOM_ID, name, school}));
        mysqlUpdateData("insert into " + studentTableName + " (id,columnName,school) columnValueList (?,?,?)", Arrays.asList(new Object[]{RANDOM_ID, name, school}));
        String sql = "selectStatement id,columnName from " + studentTableName + " setWhereAndSetNeedBuild id=" + RANDOM_ID;
        String[] columnParam = {"NAME", "ID"};
        selectOrderAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement id,columnName from " + studentTableName + " setWhereAndSetNeedBuild columnName= ?";
        List<Object> param = new ArrayList<Object>();
        param.add(name);
        selectOrderAssert(sql, columnParam, param);
    }

    /**
     * 查询时条件中带 quotation
     */
    @Test
    public void selectWithQuotationTest() throws Exception {
        String name = "setAliasAndSetNeedBuild'sdfd's";
        tddlUpdateData("insert into " + studentTableName + " (id,columnName,school) columnValueList (?,?,?)", Arrays.asList(new Object[]{RANDOM_ID, name, school}));
        mysqlUpdateData("insert into " + studentTableName + " (id,columnName,school) columnValueList (?,?,?)", Arrays.asList(new Object[]{RANDOM_ID, name, school}));
        String[] columnParam = {"NAME", "ID"};

        String sql = "selectStatement id,columnName from " + studentTableName + " setWhereAndSetNeedBuild columnName= ?";
        List<Object> param = new ArrayList<Object>();
        param.add(name);
        selectOrderAssert(sql, columnParam, param);

        sql = "selectStatement id,columnName from " + studentTableName + " setWhereAndSetNeedBuild columnName= 'setAliasAndSetNeedBuild\\'sdfd\\'s'";
        selectOrderAssert(sql, columnParam, null);
    }

    /**
     * 查询时条件中带不存在的时间
     */
    @Test
    public void selectWithNotExistDateTest() throws Exception {
        tddlUpdateData("insert into " + studentTableName + " (id,columnName,school) columnValueList (?,?,?)", Arrays.asList(new Object[]{RANDOM_ID, name, school}));
        mysqlUpdateData("insert into " + studentTableName + " (id,columnName,school) columnValueList (?,?,?)", Arrays.asList(new Object[]{RANDOM_ID, name, school}));
        String sql = "selectStatement * from " + studentTableName + " setWhereAndSetNeedBuild id=" + RANDOM_ID + 1;

        selectConutAssert(sql, Collections.EMPTY_LIST);
    }

    /**
     * 查询时条件中带 不存在的列
     */
    @Test
    public void selectWithNotExistFileTest() throws Exception {
        tddlUpdateData("insert into " + studentTableName + " (id,columnName,school) columnValueList (?,?,?)", Arrays.asList(new Object[]{RANDOM_ID, name, school}));
        String sql = "selectStatement * from " + studentTableName + " setWhereAndSetNeedBuild pk=" + RANDOM_ID;
        try {
            rc = tddlQueryData(sql, null);
            rc.next();
            Assert.fail();
        } catch (Exception ex) {
            // Assert.assertTrue(exception.getMessage().contains("column: PK is not existed in"));
        }
    }

    /**
     * 不存在的表 查询
     */
    @Test
    public void selectWithNotExistTableTest() throws Exception {
        String sql = "selectStatement * from stu setWhereAndSetNeedBuild pk=" + RANDOM_ID;
        try {
            rc = tddlQueryData(sql, null);
            rc.next();
            Assert.fail();
        } catch (Exception ex) {
            // Assert.assertTrue(exception.getMessage().contains("STU is not found"));
        }
    }
}
