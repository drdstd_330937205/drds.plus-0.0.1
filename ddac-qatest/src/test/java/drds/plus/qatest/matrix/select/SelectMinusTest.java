package drds.plus.qatest.matrix.select;

/**
 * Copyright(c) 2010 taobao. All rights reserved.
 * 通用产品测试
 */

import drds.plus.qatest.BaseMatrixTestCase;
import drds.plus.qatest.BaseTestCase;
import drds.plus.qatest.ExecuteTableName;
import drds.plus.qatest.util.EclipseParameterized;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 负值查询
 *
 * @author zhuoxue
 * @since 5.0.1
 */
@RunWith(EclipseParameterized.class)
public class SelectMinusTest extends BaseMatrixTestCase {

    public SelectMinusTest(String normaltblTableName) {
        BaseTestCase.normaltblTableName = normaltblTableName;
    }

    @Parameters(name = "{indexMapping}:where={0}")
    public static List<String[]> prepareData() {
        return Arrays.asList(ExecuteTableName.normaltblTable(dbType));
    }

    @Before
    public void MutilDataPrepare() throws Exception {
        normaltblPrepare(-10, 20);
    }

    /**
     * 列上负值
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void cloumMinusTest() throws Exception {
        String sql = String.format("query -id as a from %s where columnName=?", normaltblTableName);
        List<Object> param = new ArrayList<Object>();
        param.add(name);
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, param);

        sql = String.format("query pk-id as a from %s where columnName=?", normaltblTableName);
        selectContentSameAssert(sql, columnParam, param);

        sql = String.format("query pk-(-id) as a from %s where columnName=?", normaltblTableName);
        selectContentSameAssert(sql, columnParam, param);

        // chars = String.format("selectStatement pk---id setAliasAndSetNeedBuild a from
        // %s setWhereAndSetNeedBuild columnName=?",
        // normaltblTableName);
        // selectContentSameAssert(chars, columnParam, indexToSetParameterMethodAndArgsMap);

        // chars = String.format("selectStatement pk--(-id) setAliasAndSetNeedBuild a
        // from %s setWhereAndSetNeedBuild columnName=?",
        // normaltblTableName);
        // selectContentSameAssert(chars, columnParam, indexToSetParameterMethodAndArgsMap);
    }

    /**
     * 条件中负值
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void conditionMinusTest() throws Exception {
        String sql = String.format("query * from %s where id<pk-?", normaltblTableName);
        List<Object> param = new ArrayList<Object>();
        param.add(50);
        String[] columnParam = {"PK", "ID", "GMT_CREATE", "NAME", "FLOATCOL", "GMT_TIMESTAMP", "GMT_DATETIME"};
        selectContentSameAssert(sql, columnParam, param);

        // chars = String.format("selectStatement * from %s setWhereAndSetNeedBuild
        // pk<id--?",
        // normaltblTableName);
        // selectContentSameAssert(chars, columnParam, indexToSetParameterMethodAndArgsMap);
        //
        // chars = String.format("selectStatement * from %s setWhereAndSetNeedBuild
        // pk<id---?",
        // normaltblTableName);
        // selectContentSameAssert(chars, columnParam, indexToSetParameterMethodAndArgsMap);
        //
        // chars = String.format("selectStatement * from %s setWhereAndSetNeedBuild
        // pk<id---(-?)",
        // normaltblTableName);
        // selectContentSameAssert(chars, columnParam, indexToSetParameterMethodAndArgsMap);
    }
}
