package drds.plus.qatest.matrix.join;

import drds.plus.qatest.BaseMatrixTestCase;
import drds.plus.qatest.BaseTestCase;
import drds.plus.qatest.ExecuteTableName;
import drds.plus.qatest.util.EclipseParameterized;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * rightNode join测试，bdb不支持LeftJoin，只有当dbType="mysql"时测试用例才会运行
 *
 * @author zhuoxue
 * @since 5.0.1
 */
@RunWith(EclipseParameterized.class)
public class RightJoinTest extends BaseMatrixTestCase {

    String[] columnParam = {"host_id", "host_name", "hostgroup_id", "hostgroup_name"};

    public RightJoinTest(String monitor_host_infoTableName, String monitor_hostgroup_infoTableName) throws Exception {
        BaseTestCase.host_info = monitor_host_infoTableName;
        BaseTestCase.hostgroup = monitor_hostgroup_infoTableName;
        initData();
    }

    @Parameters(name = "{indexMapping}:table0={0},table1={1}")
    public static List<String[]> prepareDate() {
        return Arrays.asList(ExecuteTableName.mysqlHostinfoHostgroupTable(dbType));
    }

    public void initData() throws Exception {

        hostgroupPrepare(5, 20);
        hostinfoPrepare(0, 10);
        hostgroupDataAdd(20, 30, 8l);
    }

    @After
    public void destory() throws Exception {
        psConRcRsClose(rc, rs);
    }

    /**
     * rightNode join测试
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void rightJoinTest() throws Exception {
        String sql = "selectStatement " + host_info + ".host_id," + host_info + ".host_name," + host_info + ".hostgroup_id," + hostgroup + ".hostgroup_name from " + host_info + " rightNode join " + hostgroup + "  " + "on " + hostgroup + ".module_id=" + host_info + ".hostgroup_id";
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * rightNode join测试 带where条件
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void rightJoinWithWhereTest() throws Exception {
        String sql = "query " + host_info + ".host_id," + host_info + ".host_name," + host_info + ".hostgroup_id," + hostgroup + ".hostgroup_name from " + host_info + " rightNode join " + hostgroup + "  " + "on " + hostgroup + ".module_id=" + host_info + ".hostgroup_id setWhereAndSetNeedBuild " + hostgroup + ".hostgroup_name='hostgroupname0'";
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * rightNode join测试 where条件中有and连接
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void rightJoinWithAndTest() throws Exception {
        String sql = "query " + host_info + ".host_id," + host_info + ".host_name," + host_info + ".hostgroup_id," + hostgroup + ".hostgroup_name from " + host_info + " rightNode join " + hostgroup + "  " + "on " + hostgroup + ".module_id=" + host_info + ".hostgroup_id setWhereAndSetNeedBuild " + hostgroup + ".hostgroup_name='hostgroupname0'" + " and " + host_info + ".host_name='hostname0'";
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * rightNode join测试 where条件中有or连接
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void rightJoinWithOrTest() throws Exception {
        String sql = "query " + host_info + ".host_id," + host_info + ".host_name," + host_info + ".hostgroup_id," + hostgroup + ".hostgroup_name from " + host_info + " rightNode join " + hostgroup + "  " + "on " + hostgroup + ".module_id=" + host_info + ".hostgroup_id setWhereAndSetNeedBuild " + hostgroup + ".hostgroup_name='hostgroupname0'" + " or " + hostgroup + ".hostgroup_name='hostgroupname1'";
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * rightNode join测试 where条件后有limit
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void rightJoinWithLimitTest() throws Exception {
        String sql = "query " + host_info + ".host_id," + host_info + ".host_name," + host_info + ".hostgroup_id," + hostgroup + ".hostgroup_name from " + host_info + " rightNode join " + hostgroup + "  " + "on " + hostgroup + ".module_id=" + host_info + ".hostgroup_id setWhereAndSetNeedBuild " + hostgroup + ".hostgroup_name='hostgroupname0'" + " limit 1";
        selectConutAssert(sql, Collections.EMPTY_LIST);
    }

    /**
     * rightNode join测试 不带as的别名
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void rightJoinWithAliasTest() throws Exception {
        String sql = "query a.host_id,a.host_name,a.hostgroup_id,b.hostgroup_name from  " + host_info + " a rightNode join  " + hostgroup + " b " + "on b.module_id=a.hostgroup_id";
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * rightNode join测试 带as的别名
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void rightJoinWithAliasAsTest() throws Exception {
        String sql = "query a.host_id,a.host_name,a.hostgroup_id,b.hostgroup_name from " + host_info + " setAliasAndSetNeedBuild a rightNode join  " + hostgroup + " setAliasAndSetNeedBuild b " + "on b.module_id=a.hostgroup_id";
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * rightNode join测试 orderby
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void rightJoinWithOrderByTest() throws Exception {
        String sql = "/* ANDOR ALLOW_TEMPORARY_TABLE=True */query a.host_id,a.host_name,a.hostgroup_id,b.hostgroup_name from " + host_info + " setAliasAndSetNeedBuild a rightNode join " + hostgroup + " setAliasAndSetNeedBuild b " + "on b.hostgroup_id=a.hostgroup_id setWhereAndSetNeedBuild b.hostgroup_name='hostgroupname0' order by a.host_id";
        selectOrderAssert(sql, columnParam, Collections.EMPTY_LIST);
        sql = "/* ANDOR ALLOW_TEMPORARY_TABLE=True */query a.host_id,a.host_name,a.hostgroup_id,b.hostgroup_name from " + host_info + " setAliasAndSetNeedBuild a rightNode join " + hostgroup + " setAliasAndSetNeedBuild b " + "on b.hostgroup_id=a.hostgroup_id setWhereAndSetNeedBuild b.hostgroup_name='hostgroupname0' order by a.host_id asc";
        selectOrderAssert(sql, columnParam, Collections.EMPTY_LIST);
        sql = "/* ANDOR ALLOW_TEMPORARY_TABLE=True */query a.host_id,a.host_name,a.hostgroup_id,b.hostgroup_name from " + host_info + " setAliasAndSetNeedBuild a rightNode join " + hostgroup + " setAliasAndSetNeedBuild b " + "on b.hostgroup_id=a.hostgroup_id setWhereAndSetNeedBuild b.hostgroup_name='hostgroupname0' order by a.host_id desc";
        selectOrderAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * rightNode join测试 带子查询
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void rightJoinWithSubQueryTest() throws Exception {
        if (hostgroup.startsWith("ob") && host_info.startsWith("ob")) {
            // TODO:ob join bug，对别名支持不好
            return;
        }
        // {
        // String chars =
        // "/* ANDOR ALLOW_TEMPORARY_TABLE=True */selectStatement
        // sum(host_id) setAliasAndSetNeedBuild sumId,host_name,hostgroup_id from "
        // + host_info
        // +
        // " setWhereAndSetNeedBuild host_id BETWEEN ? and ? GROUP BY host_name ORDER BY hostgroup_id
        // LIMIT ?";
        // List<Object> indexToSetParameterMethodAndArgsMap = new ArrayList<Object>();
        // indexToSetParameterMethodAndArgsMap.add(0);
        // indexToSetParameterMethodAndArgsMap.add(100);
        // indexToSetParameterMethodAndArgsMap.add(10);
        // // indexToSetParameterMethodAndArgsMap.add(1);
        // // indexToSetParameterMethodAndArgsMap.add(20);
        // String[] columnParam = { "sumId", "host_name", "hostgroup_id" };
        // selectContentSameAssert(chars, columnParam, indexToSetParameterMethodAndArgsMap);
        // }

        {
            String sql = "/* ANDOR ALLOW_TEMPORARY_TABLE=True */SELECT sumId ,host_name , a.hostgroup_id setAliasAndSetNeedBuild aid ,b.hostgroup_id  setAliasAndSetNeedBuild bid from " + "( query  sum(host_id) setAliasAndSetNeedBuild sumId,host_name,hostgroup_id from " + host_info + " setWhereAndSetNeedBuild host_id BETWEEN ? and ? GROUP BY host_name ORDER BY hostgroup_id LIMIT ?) setAliasAndSetNeedBuild a " + "RIGHT JOIN (SELECT sum(hostgroup_id) , hostgroup_name,hostgroup_id  from " + hostgroup + " setWhereAndSetNeedBuild hostgroup_id " + "BETWEEN ? and ? GROUP BY hostgroup_name ) setAliasAndSetNeedBuild b ON a.hostgroup_id=b.hostgroup_id ORDER BY sumId desc";
            List<Object> param = new ArrayList<Object>();
            param.add(0);
            param.add(100);
            param.add(10);
            param.add(1);
            param.add(20);
            String[] columnParam = {"sumId", "host_name", "aid", "bid"};
            selectContentSameAssert(sql, columnParam, param);
        }
    }
}
