package drds.plus.qatest.matrix.select.function.aggregate;

import drds.plus.qatest.BaseMatrixTestCase;
import drds.plus.qatest.BaseTestCase;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;

/**
 * 不能下推的聚合函数测试
 */
public class AggregateFunctionTest extends BaseMatrixTestCase {

    public AggregateFunctionTest() {
        BaseTestCase.normaltblTableName = "_tddl_";
    }

    @Before
    public void prepare() throws Exception {
        demoRepoPrepare(0, 20);
    }

    @After
    public void destory() throws Exception {
        psConRcRsClose(rc, rs);
    }

    /**
     * count(*)
     */
    @Test
    public void countTest() throws Exception {
        String sql = "selectStatement count(*) setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

    }

    /**
     * sum(id)
     */
    @Test
    public void sumTest() throws Exception {
        String sql = "selectStatement sum(id) setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

    }

    /**
     * sum(id)/count(id)
     */
    @Test
    public void operationTest() throws Exception {
        String sql = "selectStatement sum(id)/count(id) setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

    }

    /**
     * sum(id+1)/count(id)
     */
    @Test
    public void operationTest1() throws Exception {
        String sql = "selectStatement sum(id+1)/count(id) setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

    }

    /**
     * sum(id+1)/count(id) setAliasAndSetNeedBuild a,count(id)+1
     */
    @Test
    public void operationTest2() throws Exception {
        String sql = "selectStatement sum(id+1)/count(id) setAliasAndSetNeedBuild a,count(id)+1 setAliasAndSetNeedBuild b  from " + normaltblTableName;
        String[] columnParam = {"a", "b"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

    }

    /**
     * sum(id+1)/count(id) setAliasAndSetNeedBuild a,count(id)+1
     */
    @Test
    public void operationTest3() throws Exception {
        String sql = "selectStatement sum(id+1)/count(id) setAliasAndSetNeedBuild a,count(id)+1 setAliasAndSetNeedBuild b  from " + normaltblTableName;
        String[] columnParam = {"a", "b"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

    }

    /**
     * count(id)+1 setAliasAndSetNeedBuild b ,count(id)
     */
    @Test
    public void operationTest4() throws Exception {
        String sql = "selectStatement count(id)+1 setAliasAndSetNeedBuild b ,count(id) setAliasAndSetNeedBuild c from " + normaltblTableName;
        String[] columnParam = {"b", "c"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

    }

    /**
     * count(distinct id)
     */
    @Test
    public void distinctTest() throws Exception {
        String sql = "selectStatement count(distinct id) b,count(id) c from " + normaltblTableName;
        String[] columnParam = {"b", "c"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

    }

    /**
     * order by id asc
     */
    @Test
    public void orderByAscTest() throws Exception {
        String sql = "selectStatement id,columnName from _tddl_ order by id asc";
        String[] columnParam = {"id", "columnName"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

    }

    /**
     * order by id desc
     */
    @Test
    public void orderByDescTest1() throws Exception {
        String sql = "selectStatement id,columnName from _tddl_ order by id desc";
        String[] columnParam = {"id", "columnName"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

    }

}
