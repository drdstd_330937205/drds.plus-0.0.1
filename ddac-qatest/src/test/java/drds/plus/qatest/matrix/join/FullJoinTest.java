package drds.plus.qatest.matrix.join;

import drds.plus.qatest.BaseMatrixTestCase;
import drds.plus.qatest.BaseTestCase;
import drds.plus.qatest.ExecuteTableName;
import drds.plus.qatest.util.EclipseParameterized;
import org.junit.After;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.List;

/**
 * full join目前不支持
 *
 * @author zhuoxue
 * @since 5.0.1
 */

@Ignore("目前不支持")
@RunWith(EclipseParameterized.class)
public class FullJoinTest extends BaseMatrixTestCase {

    public FullJoinTest(String normaltblTableName, String studentTableName) throws Exception {
        BaseTestCase.normaltblTableName = normaltblTableName;
        BaseTestCase.studentTableName = studentTableName;
        prepareDate();
    }

    @Parameters(name = "{indexMapping}:table0={0},table1={1}")
    public static List<String[]> prepare() {
        return Arrays.asList(ExecuteTableName.normaltblStudentTable(dbType));
    }

    public void prepareDate() throws Exception {
        tddlUpdateData("delete from " + normaltblTableName, null);
        tddlUpdateData("delete from " + studentTableName, null);
        normaltblPrepare(0, 20);
        studentPrepare(0, 20);
    }

    @After
    public void clearDate() throws Exception {
        if (rc != null) {
            rc.close();
            rc = null;
        }
    }

    @Ignore("目前不支持")
    @Test
    public void fullJoinTest() throws Exception {

    }

}
