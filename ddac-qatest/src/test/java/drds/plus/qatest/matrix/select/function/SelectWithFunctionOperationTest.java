package drds.plus.qatest.matrix.select.function;

import drds.plus.qatest.BaseMatrixTestCase;
import drds.plus.qatest.BaseTestCase;
import drds.plus.qatest.ExecuteTableName;
import drds.plus.qatest.util.EclipseParameterized;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * 函数操作
 *
 * @author zhuoxue
 * @since 5.0.1
 */
@RunWith(EclipseParameterized.class)
public class SelectWithFunctionOperationTest extends BaseMatrixTestCase {

    public SelectWithFunctionOperationTest(String tableName) {
        BaseTestCase.normaltblTableName = tableName;
    }

    @Parameters(name = "{indexMapping}:where={0}")
    public static List<String[]> prepareData() {
        return Arrays.asList(ExecuteTableName.normaltblTable(dbType));
    }

    @Before
    public void prepare() throws Exception {
        normaltblPrepare(0, 20);
    }

    @After
    public void destory() throws Exception {
        psConRcRsClose(rc, rs);
    }

    /**
     * 函数之间的加减操作
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void testFunctionPlusMusTest() throws Exception {
        String sql = "SELECT max(pk)+min(pk)  setAliasAndSetNeedBuild a FROM " + normaltblTableName;
        String[] columnParam = {"a"};
        selectOrderAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "SELECT max(pk)+min(pk) setAliasAndSetNeedBuild plus FROM " + normaltblTableName;
        String[] columnParamp = {"plus"};
        selectOrderAssert(sql, columnParamp, Collections.EMPTY_LIST);

        sql = "SELECT max(pk)-min(pk) setAliasAndSetNeedBuild a FROM " + normaltblTableName;
        String[] columnParam1 = {"a"};
        selectOrderAssert(sql, columnParam1, Collections.EMPTY_LIST);

        sql = "SELECT max(pk)-min(pk) mus FROM " + normaltblTableName;
        String[] columnParamm = {"mus"};
        selectOrderAssert(sql, columnParamm, Collections.EMPTY_LIST);

        sql = "SELECT max(pk)--min(pk) mus FROM " + normaltblTableName;
        selectOrderAssert(sql, columnParamm, Collections.EMPTY_LIST);
    }

    /**
     * 函数之间的乘除操作
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void testFunctionMultiplicationDivisionTest() throws Exception {
        String sql = "SELECT sum(pk)/max(id)  setAliasAndSetNeedBuild a FROM " + normaltblTableName;
        String[] columnParam = {"a"};
        selectOrderAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "SELECT sum(pk)/count(*) setAliasAndSetNeedBuild Division FROM " + normaltblTableName;
        String[] columnParamp = {"Division"};
        selectOrderAssert(sql, columnParamp, Collections.EMPTY_LIST);

        sql = "SELECT max(id)*count(id) setAliasAndSetNeedBuild c FROM " + normaltblTableName;
        String[] columnParam1 = {"c"};
        selectOrderAssert(sql, columnParam1, Collections.EMPTY_LIST);

        sql = "SELECT avg(id)*count(*) Multiplication FROM " + normaltblTableName;
        String[] columnParamm = {"Multiplication"};
        selectOrderAssert(sql, columnParamm, Collections.EMPTY_LIST);
    }

    /**
     * 函数之间的模操作
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void testFunctionModTest() throws Exception {
        String sql = "SELECT sum(pk)%count(*) sd FROM " + normaltblTableName;
        String[] columnParamm2 = {"sd"};
        selectOrderAssert(sql, columnParamm2, Collections.EMPTY_LIST);
    }

    /**
     * 函数之间的加法操作
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void testFunctionPlus() throws Exception {
        String sql = "SELECT pk FROM " + normaltblTableName + " where pk=1+1";
        String[] columnParamm2 = {"pk"};
        selectOrderAssert(sql, columnParamm2, Collections.EMPTY_LIST);
    }

    /**
     * 函数之间的乘除操作
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void testFunctionMixTest() throws Exception {

        String sql = "SELECT sum(pk)/max(id)+count(*) setAliasAndSetNeedBuild b  FROM " + normaltblTableName;
        String[] columnParam = {"b"};
        selectOrderAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "SELECT max(id)*count(id)/8-count(*)*2+min(id) setAliasAndSetNeedBuild c FROM " + normaltblTableName;
        String[] columnParam1 = {"c"};
        selectOrderAssert(sql, columnParam1, Collections.EMPTY_LIST);
    }

    /**
     * 函数之间的加法
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void testFunctionAddDupTest() throws Exception {
        String sql = "SELECT pk+1 setAliasAndSetNeedBuild b  FROM " + normaltblTableName + " order by pk";
        String[] columnParam = {"b"};
        selectOrderAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

}
