package drds.plus.qatest.matrix.select.function;

import drds.plus.qatest.BaseMatrixTestCase;
import drds.plus.qatest.BaseTestCase;
import drds.plus.qatest.ExecuteTableName;
import drds.plus.qatest.util.EclipseParameterized;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 日期函数
 */
@RunWith(EclipseParameterized.class)
public class SelectDateFunctionTest extends BaseMatrixTestCase {

    public SelectDateFunctionTest(String tableName) {
        BaseTestCase.normaltblTableName = tableName;
    }

    @Parameters(name = "{indexMapping}:where={0}")
    public static List<String[]> prepareData() {
        return Arrays.asList(ExecuteTableName.normaltblTable(dbType));
    }

    @Before
    public void prepare() throws Exception {
        normaltblPrepare(0, 20);
    }

    @After
    public void destory() throws Exception {
        psConRcRsClose(rc, rs);
    }

    /**
     * to_days()
     */
    @Test
    public void to_daysTest() throws Exception {
        if (!normaltblTableName.startsWith("ob")) {
            String sql = "selectStatement id,columnName from " + normaltblTableName + " setWhereAndSetNeedBuild TO_DAYS(gmt_create)-TO_DAYS('2011-05-15')>30";
            rs = mysqlQueryData(sql, null);
            rc = tddlQueryData(sql, null);

            String[] columnParam1 = {"id", "columnName"};
            assertContentSame(rs, rc, columnParam1);

            sql = "selectStatement TO_DAYS(gmt_create) setAliasAndSetNeedBuild da,columnName from " + normaltblTableName + " setWhereAndSetNeedBuild pk=1";

            rs = mysqlQueryData(sql, null);
            rc = tddlQueryData(sql, null);

            String[] columnParam2 = {"da", "columnName"};
            assertContentSame(rs, rc, columnParam2);
        }
    }

    /**
     * from_days()
     */
    @Test
    public void from_daysTest() throws Exception {
        if (!normaltblTableName.startsWith("ob")) {
            String sql = "selectStatement FROM_DAYS(TO_DAYS(gmt_create)) setAliasAndSetNeedBuild da from " + normaltblTableName + " setWhereAndSetNeedBuild pk=0";

            rs = mysqlQueryData(sql, null);
            rc = tddlQueryData(sql, null);

            String[] columnParam1 = {"da"};
            assertContentSame(rs, rc, columnParam1);
        }

    }

    /**
     * dayofweek()
     */
    @Test
    public void dayofweekTest() throws Exception {
        if (!normaltblTableName.startsWith("ob")) {
            String sql = "selectStatement DAYOFWEEK(gmt_create)  setAliasAndSetNeedBuild da from " + normaltblTableName + " setWhereAndSetNeedBuild pk=0";
            rs = mysqlQueryData(sql, null);
            rc = tddlQueryData(sql, null);

            String[] columnParam1 = {"da"};
            assertContentSame(rs, rc, columnParam1);
        }
    }

    /**
     * weekday()
     */
    @Test
    public void weekdayTest() throws Exception {
        if (!normaltblTableName.startsWith("ob")) {
            String sql = "selectStatement WEEKDAY(gmt_create)  setAliasAndSetNeedBuild da from " + normaltblTableName + " setWhereAndSetNeedBuild pk=0";

            rs = mysqlQueryData(sql, null);
            rc = tddlQueryData(sql, null);

            String[] columnParam1 = {"da"};
            assertContentSame(rs, rc, columnParam1);
        }
    }

    /**
     * dayofyear()
     */
    @Test
    public void dayofyearTest() throws Exception {
        if (!normaltblTableName.startsWith("ob")) {
            String sql = "selectStatement DAYOFYEAR(gmt_create)  setAliasAndSetNeedBuild da from " + normaltblTableName + " setWhereAndSetNeedBuild pk=0";

            rs = mysqlQueryData(sql, null);
            rc = tddlQueryData(sql, null);

            String[] columnParam1 = {"da"};
            assertContentSame(rs, rc, columnParam1);
        }
    }

    /**
     * month()
     */
    @Test
    public void monthTest() throws Exception {
        if (!normaltblTableName.startsWith("ob")) {
            String sql = "selectStatement MONTH(gmt_create) setAliasAndSetNeedBuild da from " + normaltblTableName + " setWhereAndSetNeedBuild pk=0";

            rs = mysqlQueryData(sql, null);
            rc = tddlQueryData(sql, null);

            String[] columnParam1 = {"da"};
            assertContentSame(rs, rc, columnParam1);
        }
    }

    /**
     * monthname()
     */
    @Test
    public void monthnameTest() throws Exception {
        if (!normaltblTableName.startsWith("ob")) {
            String sql = "selectStatement MONTHNAME(gmt_create)  setAliasAndSetNeedBuild da from " + normaltblTableName + " setWhereAndSetNeedBuild pk=0";

            rs = mysqlQueryData(sql, null);
            rc = tddlQueryData(sql, null);

            String[] columnParam1 = {"da"};
            assertContentSame(rs, rc, columnParam1);
        }
    }

    /**
     * quarter()
     */
    @Test
    public void quarterTest() throws Exception {
        if (!normaltblTableName.startsWith("ob")) {
            String sql = "selectStatement QUARTER(gmt_create)  setAliasAndSetNeedBuild da from " + normaltblTableName + " setWhereAndSetNeedBuild pk=0";

            rs = mysqlQueryData(sql, null);
            rc = tddlQueryData(sql, null);

            String[] columnParam1 = {"da"};
            assertContentSame(rs, rc, columnParam1);
        }
    }

    /**
     * week()
     */
    @Test
    public void weekTest() throws Exception {
        if (!normaltblTableName.startsWith("ob")) {
            String sql = "selectStatement WEEK(gmt_create) setAliasAndSetNeedBuild da from " + normaltblTableName + " setWhereAndSetNeedBuild pk=0";

            rs = mysqlQueryData(sql, null);
            rc = tddlQueryData(sql, null);

            String[] columnParam1 = {"da"};
            assertContentSame(rs, rc, columnParam1);
        }
    }

    /**
     * year() hour() minute() second()
     */
    @Test
    public void timeTest() throws Exception {
        if (!normaltblTableName.startsWith("ob")) {
            String[] columnParam1 = {"da"};
            String sql = "selectStatement YEAR(gmt_create)  setAliasAndSetNeedBuild da from " + normaltblTableName + " setWhereAndSetNeedBuild pk=0";

            rs = mysqlQueryData(sql, null);
            rc = tddlQueryData(sql, null);

            assertContentSame(rs, rc, columnParam1);

            sql = "selectStatement HOUR(gmt_create)  setAliasAndSetNeedBuild da from " + normaltblTableName + " setWhereAndSetNeedBuild pk=0";

            rs = mysqlQueryData(sql, null);
            rc = tddlQueryData(sql, null);
            assertContentSame(rs, rc, columnParam1);

            sql = "selectStatement MINUTE(gmt_create) setAliasAndSetNeedBuild da from " + normaltblTableName + " setWhereAndSetNeedBuild pk=0";

            rs = mysqlQueryData(sql, null);
            rc = tddlQueryData(sql, null);
            assertContentSame(rs, rc, columnParam1);

            sql = "selectStatement SECOND(gmt_create) setAliasAndSetNeedBuild da from " + normaltblTableName + " setWhereAndSetNeedBuild pk=0";

            rs = mysqlQueryData(sql, null);
            rc = tddlQueryData(sql, null);
            assertContentSame(rs, rc, columnParam1);
        }
    }

    /**
     * dayname()
     */
    @Test
    public void daynameTest() throws Exception {
        if (!normaltblTableName.startsWith("ob")) {
            String sql = "selectStatement DAYNAME(gmt_create) setAliasAndSetNeedBuild da from " + normaltblTableName + " setWhereAndSetNeedBuild pk=0";

            rs = mysqlQueryData(sql, null);
            rc = tddlQueryData(sql, null);

            String[] columnParam1 = {"da"};
            assertContentSame(rs, rc, columnParam1);
        }
    }

    /**
     * dayofmonth()
     */
    @Test
    public void dayofmonthTest() throws Exception {
        if (!normaltblTableName.startsWith("ob")) {
            String sql = "selectStatement DAYOFMONTH(gmt_create) setAliasAndSetNeedBuild da from " + normaltblTableName + " setWhereAndSetNeedBuild pk=0";

            rs = mysqlQueryData(sql, null);
            rc = tddlQueryData(sql, null);

            String[] columnParam1 = {"da"};
            assertContentSame(rs, rc, columnParam1);
        }
    }

    /**
     * date_sub()
     */
    @Test
    public void date_subTest() throws Exception {
        if (!normaltblTableName.startsWith("ob")) {
            String sql = "selectStatement DATE_SUB(gmt_create,interval_primary 31 DAY)  setAliasAndSetNeedBuild da from " + normaltblTableName + " setWhereAndSetNeedBuild pk=1";

            rs = mysqlQueryData(sql, null);
            rc = tddlQueryData(sql, null);

            String[] columnParam1 = {"da"};
            assertContentSame(rs, rc, columnParam1);
        }

    }

    /**
     * date_sub()
     */
    public void date_subTestBindVal() throws Exception {
        String sql = "selectStatement DATE_SUB(gmt_create,interval_primary ? DAY)  setAliasAndSetNeedBuild da from " + normaltblTableName + " setWhereAndSetNeedBuild pk=1";
        List<Object> param = new ArrayList<Object>();
        param.add(31);
        rs = mysqlQueryData(sql, param);
        rc = tddlQueryData(sql, param);

        String[] columnParam1 = {"da"};
        assertContentSame(rs, rc, columnParam1);

    }

    /**
     * date_add()
     */
    @Test
    public void date_addTest() throws Exception {
        if (!normaltblTableName.startsWith("ob")) {
            String sql = "selectStatement DATE_ADD(gmt_create,interval_primary 31 DAY) setAliasAndSetNeedBuild da from " + normaltblTableName + " setWhereAndSetNeedBuild pk=1";
            String[] columnParam = {"da"};
            selectContentSameAssert(sql, columnParam, null);

            sql = "selectStatement ADDDATE(gmt_create,interval_primary 31 DAY) setAliasAndSetNeedBuild da from " + normaltblTableName + " setWhereAndSetNeedBuild pk=1";
            selectContentSameAssert(sql, columnParam, null);

            sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild gmt_create > DATE_ADD(Date(?),interval_primary 31 DAY)";
            List<Object> param = new ArrayList<Object>();
            param.add("2011-5-5");
            String[] columnParam1 = {"columnName", "id", "pk"};
            selectContentSameAssert(sql, columnParam1, param);

            sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild gmt_create > ADDDATE(Date(?),interval_primary 31 DAY)";
            selectContentSameAssert(sql, columnParam1, param);
        }
    }

    /**
     * to_days()
     */
    @Test
    public void TO_DAYSTest() throws Exception {
        if (!normaltblTableName.startsWith("ob")) {
            String sql = "selectStatement TO_DAYS(gmt_create) setAliasAndSetNeedBuild da from " + normaltblTableName + " setWhereAndSetNeedBuild pk=1";

            rs = mysqlQueryData(sql, null);
            rc = tddlQueryData(sql, null);

            String[] columnParam1 = {"da"};
            assertContentSame(rs, rc, columnParam1);
        }
    }

    /**
     * addtime()
     */
    @Test
    public void addtimeTest() throws Exception {
        if (!normaltblTableName.startsWith("ob")) {
            String sql = "selectStatement ADDTIME(gmt_create,'1 1:1:1.000002')  setAliasAndSetNeedBuild da from " + normaltblTableName + " setWhereAndSetNeedBuild pk=1";

            rs = mysqlQueryData(sql, null);
            rc = tddlQueryData(sql, null);

            String[] columnParam1 = {"da"};
            assertContentSame(rs, rc, columnParam1);
        }
    }

    /**
     * timestampdiff()
     */
    @Test
    public void timestampdiffTest() throws Exception {
        if (!normaltblTableName.startsWith("ob")) {
            String sql = "selectStatement timestampdiff(second,now(),1)  setAliasAndSetNeedBuild da from " + normaltblTableName + " setWhereAndSetNeedBuild pk=1";

            rs = mysqlQueryData(sql, null);
            rc = tddlQueryData(sql, null);

            String[] columnParam1 = {"da"};
            assertContentSame(rs, rc, columnParam1);
        }
    }

    /**
     * timestampadd()
     */
    @Test
    public void timestampAddTest() throws Exception {
        if (!normaltblTableName.startsWith("ob")) {
            String sql = "selectStatement timestampadd(second,now(),1)  setAliasAndSetNeedBuild da from " + normaltblTableName + " setWhereAndSetNeedBuild pk=1";

            rs = mysqlQueryData(sql, null);
            rc = tddlQueryData(sql, null);

            String[] columnParam1 = {"da"};
            assertContentSame(rs, rc, columnParam1);
        }
    }

    /**
     * get_format()
     */
    @Test
    public void getFromatTest() throws Exception {
        if (!normaltblTableName.startsWith("ob")) {
            String sql = "selectStatement get_format(time,'usa') setAliasAndSetNeedBuild da from " + normaltblTableName + " setWhereAndSetNeedBuild pk=1";

            rs = mysqlQueryData(sql, null);
            rc = tddlQueryData(sql, null);

            String[] columnParam1 = {"da"};
            assertContentSame(rs, rc, columnParam1);
        }
    }

    /**
     * date()
     */
    @Test
    public void dateTest() throws Exception {
        if (!normaltblTableName.startsWith("ob")) {
            String sql = "selectStatement DATE(gmt_create)  setAliasAndSetNeedBuild da from " + normaltblTableName + " setWhereAndSetNeedBuild pk=1";

            rs = mysqlQueryData(sql, null);
            rc = tddlQueryData(sql, null);

            String[] columnParam1 = {"da"};
            assertContentSame(rs, rc, columnParam1);
        }
    }

    /**
     * date函数通过绑定变量传值进去
     */
    @Test
    public void dateWithParamTest() throws Exception {
        if (!normaltblTableName.startsWith("ob")) {
            String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild gmt_create >=Date(?)";
            List<Object> param = new ArrayList<Object>();
            param.add("2011-1-1");
            String[] columnParam = {"columnName", "pk", "id"};
            selectContentSameAssert(sql, columnParam, param);
        }
    }

    // @Test
    // public void str_to_dateTest() throws Exception {
    // String chars = "selectStatement * from " +
    // normaltblTableName +
    // " setWhereAndSetNeedBuild gmt_create between str_to_date(?,'%y-%m-%d')" +
    // " and str_to_date(?,'%y-%m-%d')";
    // List<Object> indexToSetParameterMethodAndArgsMap =new ArrayList<Object>();
    // indexToSetParameterMethodAndArgsMap.add("2001-1-1");
    // indexToSetParameterMethodAndArgsMap.add("2013-1-1");
    // String [] columnParam={"columnName","pk","id"};
    // selectContentSameAssert(chars, columnParam, indexToSetParameterMethodAndArgsMap);
    // }

    /**
     * now()
     */
    @Test
    public void nowTest() throws Exception {
        String sql = "selectStatement id,now() setAliasAndSetNeedBuild dd from " + normaltblTableName + " setWhereAndSetNeedBuild pk=1";
        rs = mysqlQueryData(sql, null);
        rc = tddlQueryData(sql, null);

        String[] columnParam1 = {"dd", "id"};
        assertContentSame(rs, rc, columnParam1);

        sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild gmt_create = now()";
        rs = mysqlQueryData(sql, null);
        rc = tddlQueryData(sql, null);
        String[] columnParam2 = {"gmt_create", "id"};
        assertContentSame(rs, rc, columnParam2);

        sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild now()>gmt_create";
        rs = mysqlQueryData(sql, null);
        rc = tddlQueryData(sql, null);
        assertContentSame(rs, rc, columnParam2);

        sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild gmt_timestamp = now()";
        rs = mysqlQueryData(sql, null);
        rc = tddlQueryData(sql, null);
        String[] columnParamTimestamp = {"gmt_timestamp", "id"};
        assertContentSame(rs, rc, columnParamTimestamp);

        sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild now()<=gmt_timestamp";
        rs = mysqlQueryData(sql, null);
        rc = tddlQueryData(sql, null);
        assertContentSame(rs, rc, columnParamTimestamp);

        sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild gmt_datetime = now()";
        rs = mysqlQueryData(sql, null);
        rc = tddlQueryData(sql, null);
        String[] columnParamDatetime = {"gmt_datetime", "id"};
        assertContentSame(rs, rc, columnParamDatetime);

        sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild now()>=gmt_datetime";
        rs = mysqlQueryData(sql, null);
        rc = tddlQueryData(sql, null);
        assertContentSame(rs, rc, columnParamDatetime);

        if (!normaltblTableName.startsWith("ob")) {
            sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild gmt_create >( now()- interval_primary ? day )";
            List<Object> param = new ArrayList<Object>();
            param.add(12);
            rs = mysqlQueryData(sql, param);
            rc = tddlQueryData(sql, param);
            assertContentSame(rs, rc, columnParamDatetime);
        }
    }

}
