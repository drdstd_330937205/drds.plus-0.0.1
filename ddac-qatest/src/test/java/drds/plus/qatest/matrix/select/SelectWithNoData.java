package drds.plus.qatest.matrix.select;

import drds.plus.qatest.BaseMatrixTestCase;
import drds.plus.qatest.BaseTestCase;
import drds.plus.qatest.ExecuteTableName;
import drds.plus.qatest.util.EclipseParameterized;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Select没数据
 *
 * @author zhuoxue
 * @since 5.0.1
 */
@RunWith(EclipseParameterized.class)
public class SelectWithNoData extends BaseMatrixTestCase {

    long pk = 1l;
    int id = 1;

    public SelectWithNoData(String normaltblTableName) {
        BaseTestCase.normaltblTableName = normaltblTableName;
    }

    @Parameters(name = "{indexMapping}:table0={0}")
    public static List<String[]> prepare() {
        return Arrays.asList(ExecuteTableName.normaltblTable(dbType));
    }

    @Before
    public void prepareData() throws Exception {
        tddlUpdateData("delete from  " + normaltblTableName, null);
        mysqlUpdateData("delete from  " + normaltblTableName, null);
    }

    /**
     * 列别名 表别名
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void aliasTest() throws Exception {
        String sql = "selectStatement columnName setAliasAndSetNeedBuild xingming ,id setAliasAndSetNeedBuild pid from  " + normaltblTableName + "  setAliasAndSetNeedBuild nor setWhereAndSetNeedBuild pk=?";
        List<Object> param = new ArrayList<Object>();
        param.add(pk);
        String[] columnParam = {"xingming", "pid"};
        assertAlias(sql, columnParam, "nor", param);
    }

    /**
     * where条件中or两边为不同的列
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void OrWithDifFiledTest() throws Exception {
        long pk = 2l;
        int id = 3;

        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild pk= ? or id=?";
        List<Object> param = new ArrayList<Object>();
        param.add(pk);
        param.add(id);
        String[] columnParam = {"columnName", "pk", "id"};
        selectContentSameAssert(sql, columnParam, param);
    }

    /**
     * setWhereAndSetNeedBuild pk>? order by pk
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void conditionWithGreaterTest() throws Exception {
        String[] columnParam = {"PK", "NAME", "ID"};
        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild pk>? order by pk";
        List<Object> param = new ArrayList<Object>();
        param.add(Long.parseLong(0 + ""));
        selectOrderAssert(sql, columnParam, param);

        sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild pk>=? order by pk";
        param.clear();
        param.add(Long.parseLong(0 + ""));
        selectOrderAssert(sql, columnParam, param);
    }

    /**
     * in
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void InTest() throws Exception {
        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild pk in (1,2,3)";
        String[] columnParam = {"PK", "NAME"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * like匹配
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void LikeAnyTest() throws Exception {
        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild columnName like 'zhuo%'";
        String[] columnParam = {"ID", "NAME"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild columnName like '%uo%'";
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild columnName like '%uo%u%'";
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * orderby升序
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void OrderByAscTest() throws Exception {
        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild columnName= ? order by id asc";
        List<Object> param = new ArrayList<Object>();
        param.add(name);
        String[] columnParam = {"PK", "ID", "NAME"};
        selectOrderAssertNotKeyCloumn(sql, columnParam, param, "id");
    }

    /**
     * group by
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void GroupByTest() throws Exception {
        String sql = "query count(pk) ,columnName as n from  " + normaltblTableName + "   group by n";
        String[] columnParam = {"count(pk)", "n"};
        selectOrderAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * limit start,num
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void LimitWithStart() throws Exception {
        int start = 5;
        int limit = 6;
        String sql = "SELECT * FROM " + normaltblTableName + " order by pk LIMIT " + start + "," + limit;
        String[] columnParam = {"columnName", "pk", "id"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

}
