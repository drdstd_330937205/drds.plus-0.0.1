package drds.plus.qatest.matrix.select;

import drds.plus.qatest.BaseMatrixTestCase;
import drds.plus.qatest.BaseTestCase;
import drds.plus.qatest.ExecuteTableName;
import drds.plus.qatest.util.EclipseParameterized;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * 带条件的选择查询
 *
 * @author zhuoxue
 * @since 5.0.1
 */
@RunWith(EclipseParameterized.class)
public class SelectWithConditionTest extends BaseMatrixTestCase {

    public SelectWithConditionTest(String normaltblTableName, String studentTableName) {
        BaseTestCase.normaltblTableName = normaltblTableName;
        BaseTestCase.studentTableName = studentTableName;
    }

    @Parameters(name = "{indexMapping}:table0={0},table1={1}")
    public static List<String[]> prepare() {
        return Arrays.asList(ExecuteTableName.normaltblStudentTable(dbType));
    }

    @Before
    public void prepareDate() throws Exception {
        normaltblPrepare(0, 20);
        studentPrepare(0, MAX_DATA_SIZE);
    }

    /**
     * 条件为in
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void inTest() throws Exception {
        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild pk in (1,2,3)";
        String[] columnParam = {"PK", "NAME"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * 条件为between
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void betweenTest() throws Exception {
        int start = 5;
        int end = 15;
        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild id between ? and ?";
        List<Object> param = new ArrayList<Object>();
        param.add(start);
        param.add(end);
        String[] columnParam = {"PK", "NAME", "ID"};
        selectContentSameAssert(sql, columnParam, param);
    }

    /**
     * 列上绑定变量
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void columnBindTest() throws Exception {
        int start = 5;
        int end = 15;
        String sql = "selectStatement ?,?,? from " + normaltblTableName + " setWhereAndSetNeedBuild id between ? and ?";
        List<Object> param = new ArrayList<Object>();
        param.add("PK");
        param.add("NAME");
        param.add("ID");
        param.add(start);
        param.add(end);
        String[] columnParam = {"PK", "NAME", "ID"};
        selectContentSameAssert(sql, columnParam, param);
    }

    /**
     * 列为常量
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void constant() throws Exception {
        String sql = "selectStatement 1 a,2 from " + normaltblTableName;
        String[] columnParam = {"a", "2"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * 列为过滤器 selectStatement id=id
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void selectFilterTest() throws Exception {
        String sql = "selectStatement id=id setAliasAndSetNeedBuild a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * 列为过滤器 selectStatement id=1
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void selectFilterTest2() throws Exception {
        String sql = "selectStatement id=1 a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * 列为过滤器 selectStatement id and 1 a from tbl
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void selectFilterTest3() throws Exception {
        String sql = "selectStatement id and 1 a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * 列为过滤器 selectStatement 1=1 a from tbl
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void selectFilterTest4() throws Exception {
        String sql = "selectStatement 1=1 a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * 列为过滤器 selectStatement id in (1,2,3) a from tbl
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void selectFilterTest5() throws Exception {
        String sql = "selectStatement id in (1,2,3) a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * in后面绑定变量
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void inWithParamTest() throws Exception {
        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild pk in (?,?,?)";
        List<Object> param = new ArrayList<Object>();
        param.add(Long.parseLong(1 + ""));
        param.add(Long.parseLong(2 + ""));
        param.add(Long.parseLong(3 + ""));
        String[] columnParam = {"PK", "NAME"};
        selectContentSameAssert(sql, columnParam, param);
    }

    /**
     * not in
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void NotInTest() throws Exception {
        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild pk not in (?,?,?)";
        List<Object> param = new ArrayList<Object>();
        param.add(Long.parseLong(1 + ""));
        param.add(Long.parseLong(2 + ""));
        param.add(Long.parseLong(3 + ""));
        String[] columnParam = {"ID", "NAME"};
        selectContentSameAssert(sql, columnParam, param);
    }

    /**
     * is true
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void isTrueTest() throws Exception {
        if (!normaltblTableName.startsWith("ob")) {
            String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild pk is true";
            String[] columnParam = {"ID", "NAME", "PK"};
            selectContentSameAssert(sql, columnParam, null);
            sql = "selectStatement id is true a,columnName from " + normaltblTableName;
            String[] columnParam1 = {"a", "NAME",};
            selectContentSameAssert(sql, columnParam1, null);
        }
    }

    /**
     * selectStatement not pk a from tbl
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void selectNotTest() throws Exception {
        String sql = "selectStatement not pk a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, null);
    }

    /**
     * selectStatement not max(pk) a from tbl
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void selectNotFunTest() throws Exception {
        String[] columnParam = {"a"};
        String sql = "selectStatement not max(pk) a from " + normaltblTableName;
        selectContentSameAssert(sql, columnParam, null);
    }

    /**
     * selectStatement not (1+1) a from tbl
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void selectNotConstantTest() throws Exception {
        String sql = "selectStatement not (1+1) a from " + normaltblTableName;
        String[] columnParam = {"a"};
        selectContentSameAssert(sql, columnParam, null);
    }

    /**
     * setWhereAndSetNeedBuild not (id=1)
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void selectNotWhereConstantTest() throws Exception {
        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild not (id=1)";
        String[] columnParam = {"ID", "NAME", "PK"};
        selectContentSameAssert(sql, columnParam, null);
    }

    /**
     * setWhereAndSetNeedBuild not (id=1 or id=2)
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void selectNotWhereConstantOrTest() throws Exception {
        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild not (id=1 or id=2)";
        String[] columnParam = {"ID", "NAME", "PK"};
        selectContentSameAssert(sql, columnParam, null);
    }

    /**
     * setWhereAndSetNeedBuild pk is not true
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void isNotTrueTest() throws Exception {
        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild pk is not true";
        String[] columnParam = {"ID", "NAME", "PK"};
        selectContentSameAssert(sql, columnParam, null);
    }

    /**
     * count(pk) groupby
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void groupByWithCountTest() throws Exception {
        tddlUpdateData("insert into " + normaltblTableName + " (pk,columnName) columnValueList(?,?)", Arrays.asList(new Object[]{RANDOM_ID, newName}));
        mysqlUpdateData("insert into " + normaltblTableName + " (pk,columnName) columnValueList(?,?)", Arrays.asList(new Object[]{RANDOM_ID, newName}));
        String sql = "query count(pk),columnName from " + normaltblTableName + " group by columnName";
        String[] columnParam = {"count(PK)", "NAME"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * count(pk) groupby升序
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void groupByWithAscTest() throws Exception {
        tddlUpdateData("insert into " + normaltblTableName + " (pk,columnName) columnValueList(?,?)", Arrays.asList(new Object[]{RANDOM_ID, newName}));
        String sql = "query count(pk),columnName from " + normaltblTableName + " group by columnName asc";

        selectOrderAssert(sql, new String[]{}, Collections.EMPTY_LIST);
    }

    @Ignore("ob暂时不支持desc排序，而mysql会计算desc")
    @Test
    public void groupByWithDescTest() throws Exception {
        tddlUpdateData("insert into " + normaltblTableName + " (pk,columnName) columnValueList(?,?)", Arrays.asList(new Object[]{RANDOM_ID, newName}));
        String sql = "query count(pk),columnName from " + normaltblTableName + " group by columnName desc";
        selectOrderAssert(sql, new String[]{}, Collections.EMPTY_LIST);
    }

    /**
     * max min groupby
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void groupByWithMinMaxTest() throws Exception {
        tddlUpdateData("insert into " + normaltblTableName + " (pk,columnName) columnValueList(?,?)", Arrays.asList(new Object[]{RANDOM_ID, newName}));
        mysqlUpdateData("insert into " + normaltblTableName + " (pk,columnName) columnValueList(?,?)", Arrays.asList(new Object[]{RANDOM_ID, newName}));
        String sql = "query columnName,min(pk) from " + normaltblTableName + " group by columnName";

        String[] columnParam = {"columnName", "min(pk)"};
        selectOrderAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "query columnName,max(pk) from " + normaltblTableName + " group by columnName";
        String[] param = {"columnName", "max(pk)"};
        selectOrderAssert(sql, param, Collections.EMPTY_LIST);

    }

    /**
     * avg groupby
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void groupByAvgTest() throws Exception {
        tddlUpdateData("insert into " + normaltblTableName + " (pk,columnName) columnValueList(?,?)", Arrays.asList(new Object[]{RANDOM_ID, newName}));
        mysqlUpdateData("insert into " + normaltblTableName + " (pk,columnName) columnValueList(?,?)", Arrays.asList(new Object[]{RANDOM_ID, newName}));

        String sql = "query columnName,avg(pk) from " + normaltblTableName + " group by columnName";
        String[] columnParam = {"columnName", "avg(pk)"};
        selectOrderAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * sum groupby
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void groupBySumTest() throws Exception {
        tddlUpdateData("insert into " + normaltblTableName + " (pk,columnName) columnValueList(?,?)", Arrays.asList(new Object[]{RANDOM_ID, newName}));
        mysqlUpdateData("insert into " + normaltblTableName + " (pk,columnName) columnValueList(?,?)", Arrays.asList(new Object[]{RANDOM_ID, newName}));

        String sql = "query columnName,sum(pk) from " + normaltblTableName + " group by columnName";
        String[] columnParam = {"columnName", "sum(pk)"};
        selectOrderAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * sum(pk) groupby pk
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void groupByShardColumnsSumTest() throws Exception {
        tddlUpdateData("insert into " + normaltblTableName + " (pk,columnName) columnValueList(?,?)", Arrays.asList(new Object[]{RANDOM_ID, newName}));
        mysqlUpdateData("insert into " + normaltblTableName + " (pk,columnName) columnValueList(?,?)", Arrays.asList(new Object[]{RANDOM_ID, newName}));

        String sql = "query columnName,sum(pk) from " + normaltblTableName + " group by pk";
        String[] columnParam = {"columnName", "sum(pk)"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * 时间函数 groupby日期
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void groupDateFunctionTest() throws Exception {

        /**
         * ob不支持group by函数
         */
        if (normaltblTableName.startsWith("ob")) {
            return;
        }
        tddlUpdateData("insert into " + normaltblTableName + " (pk,columnName) columnValueList(?,?)", Arrays.asList(new Object[]{RANDOM_ID, newName}));
        mysqlUpdateData("insert into " + normaltblTableName + " (pk,columnName) columnValueList(?,?)", Arrays.asList(new Object[]{RANDOM_ID, newName}));

        String sql = "query date_format(gmt_create,'%m-%d') label,sum(id)/100 value ,date(gmt_create) from " + normaltblTableName + " group by date(gmt_create)";
        String[] columnParam = {"label", "value", "date(gmt_create)"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * distinct主键
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void distinctShardColumns() throws Exception {
        tddlUpdateData("insert into " + normaltblTableName + " (pk,columnName) columnValueList(?,?)", Arrays.asList(new Object[]{RANDOM_ID, newName}));
        mysqlUpdateData("insert into " + normaltblTableName + " (pk,columnName) columnValueList(?,?)", Arrays.asList(new Object[]{RANDOM_ID, newName}));

        String sql = "selectStatement distinct pk from " + normaltblTableName;
        String[] columnParam = {"pk"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * count(distinct pk) 带别名
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void countDistinctShardColumns() throws Exception {
        tddlUpdateData("insert into " + normaltblTableName + " (pk,columnName) columnValueList(?,?)", Arrays.asList(new Object[]{RANDOM_ID, newName}));
        mysqlUpdateData("insert into " + normaltblTableName + " (pk,columnName) columnValueList(?,?)", Arrays.asList(new Object[]{RANDOM_ID, newName}));

        String sql = "selectStatement count(distinct pk) c from " + normaltblTableName;
        String[] columnParam = {"c"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * count(distinct pk) 不带别名
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void countDistinctShardColumnsWithoutAlias() throws Exception {
        tddlUpdateData("insert into " + normaltblTableName + " (pk,columnName) columnValueList(?,?)", Arrays.asList(new Object[]{RANDOM_ID, newName}));
        mysqlUpdateData("insert into " + normaltblTableName + " (pk,columnName) columnValueList(?,?)", Arrays.asList(new Object[]{RANDOM_ID, newName}));

        String sql = "selectStatement count(distinct pk) from " + normaltblTableName;
        String[] columnParam = {"count(distinct pk)"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * having测试
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void havingTest() throws Exception {
        String sql = "query columnName,count(pk) from " + normaltblTableName + " group by columnName having count(pk)>? order by columnName ";
        List<Object> param = new ArrayList<Object>();
        param.add(5L);
        String[] columnParam = {"NAME", "count(pk)"};
        selectOrderAssert(sql, columnParam, param);
    }

    /**
     * 查询字段没有带函数，having过滤带字段带函数，暂时只支持单机的
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void havingFunTest() throws Exception {
        if (normaltblTableName.contains("oneGroup_oneAtom")) {
            String sql = "query columnName from " + normaltblTableName + " group by columnName having sum(pk) > ?";
            List<Object> param = new ArrayList<Object>();
            param.add(40l);
            String[] columnParam = {"columnName"};
            selectContentSameAssert(sql, columnParam, param);
        }
    }

    /**
     * order by
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void orderByTest() throws Exception {
        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild columnName= ? order by pk";
        List<Object> param = new ArrayList<Object>();
        param.add(name);
        String[] columnParam = {"PK", "ID", "NAME"};
        selectOrderAssert(sql, columnParam, param);
    }

    /**
     * order by升序
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void orderByAscTest() throws Exception {
        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild columnName= ? order by id asc";
        List<Object> param = new ArrayList<Object>();
        param.add(name);
        String[] columnParam = {"PK", "ID", "NAME"};
        selectOrderAssertNotKeyCloumn(sql, columnParam, param, "id");
    }

    /**
     * order by 降序
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void orderByDescTest() throws Exception {
        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild columnName= ? order by id desc";
        List<Object> param = new ArrayList<Object>();
        param.add(name);
        String[] columnParam = {"PK", "ID", "NAME"};
        selectOrderAssertNotKeyCloumn(sql, columnParam, param, "id");
    }

    /**
     * order by 非主键
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void orderByNotAppointFieldTest() throws Exception {
        String sql = "selectStatement columnName,pk from " + normaltblTableName + " setWhereAndSetNeedBuild columnName= ? order by id";
        List<Object> param = new ArrayList<Object>();
        param.add(name);
        String[] columnParam = {"columnName", "pk"};
        selectContentSameAssert(sql, columnParam, param);
    }

    /**
     * order by 后面跟多个排序字段
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void orderByMutilValueTest() throws Exception {
        String sql = "selectStatement columnName,pk from " + normaltblTableName + " setWhereAndSetNeedBuild columnName= ? order by gmt_create,id";
        List<Object> param = new ArrayList<Object>();
        param.add(name);
        String[] columnParam = {"columnName", "pk"};
        selectContentSameAssert(sql, columnParam, param);
    }

    /**
     * groupby orderby
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void groupByOrderbyTest() throws Exception {
        tddlUpdateData("insert into " + normaltblTableName + " (pk,columnName) columnValueList(?,?)", Arrays.asList(new Object[]{RANDOM_ID, newName}));
        mysqlUpdateData("insert into " + normaltblTableName + " (pk,columnName) columnValueList(?,?)", Arrays.asList(new Object[]{RANDOM_ID, newName}));

        String sql = "query columnName,count(a.pk) as c from " + normaltblTableName + " as a group by columnName order by columnName";
        String[] columnParam = {"columnName", "c"};
        selectOrderAssertNotKeyCloumn(sql, columnParam, Collections.EMPTY_LIST, "columnName");
    }

    /**
     * groupby order by后跟count(pk)
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void groupByOrderbyFunctionCloumTest() throws Exception {
        tddlUpdateData("insert into " + normaltblTableName + " (pk,columnName) columnValueList(?,?)", Arrays.asList(new Object[]{RANDOM_ID, newName}));
        mysqlUpdateData("insert into " + normaltblTableName + " (pk,columnName) columnValueList(?,?)", Arrays.asList(new Object[]{RANDOM_ID, newName}));

        String sql = "/* ANDOR ALLOW_TEMPORARY_TABLE=True */query columnName,count(pk) from " + normaltblTableName + " group by columnName order by count(pk)";
        String[] columnParam = {"columnName", "count(pk)"};
        selectOrderAssertNotKeyCloumn(sql, columnParam, Collections.EMPTY_LIST, "count(pk)");
    }

    /**
     * where后为and
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void AndTest() throws Exception {
        int i = 2;
        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild columnName= ? and id= ?";
        List<Object> param = new ArrayList<Object>();
        param.add(name);
        param.add(i);
        String[] columnParam = {"columnName", "pk"};
        selectContentSameAssert(sql, columnParam, param);
    }

    /**
     * where后为and or组合
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void AndOrTest() throws Exception {
        int i = 2;
        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild (columnName= ? or columnName in (?)) and (id= ? or id > ?)";
        List<Object> param = new ArrayList<Object>();
        param.add(name);
        param.add(name + i);
        param.add(i);
        param.add(i + 1);
        String[] columnParam = {"columnName", "pk"};
        selectContentSameAssert(sql, columnParam, param);
    }

    /**
     * where后 or两边为相同字段
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void orWithSameFiledTest() throws Exception {
        long pk = 2l;
        long opk = 3l;

        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild pk= ? or pk=?";
        List<Object> param = new ArrayList<Object>();
        param.add(pk);
        param.add(opk);
        String[] columnParam = {"columnName", "pk", "id"};
        selectContentSameAssert(sql, columnParam, param);
    }

    /**
     * distinct
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void distinctTest() throws Exception {
        String sql = "selectStatement distinct columnName from " + normaltblTableName;
        String[] columnParam = {"columnName"};
        selectContentSameAssert(sql, columnParam, null);
    }

    /**
     * count(distinct id)
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void distinctWithCountDistinctTest() throws Exception {
        {
            String sql = "selectStatement count(distinct id) from " + normaltblTableName + " setAliasAndSetNeedBuild t1 setWhereAndSetNeedBuild pk>1";
            String[] columnParam = {"count(distinct id)"};
            selectContentSameAssert(sql, columnParam, null);
        }

        {
            String sql = "selectStatement count(distinct id) from " + normaltblTableName + " setWhereAndSetNeedBuild pk>1";
            String[] columnParam = {"count(distinct id)"};
            selectContentSameAssert(sql, columnParam, null);
        }

        {
            String sql = "selectStatement count(distinct id) k from " + normaltblTableName + " setAliasAndSetNeedBuild t1 setWhereAndSetNeedBuild pk>1";
            String[] columnParam = {"k"};
            selectContentSameAssert(sql, columnParam, null);
        }

    }

    /**
     * distinct orderby, order by和distinct不一致，只能用临时表
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void distinctOrderByTest() throws Exception {
        // order by和distinct不一致，只能用临时表
        String sql = "/* ANDOR ALLOW_TEMPORARY_TABLE=True */selectStatement  distinct columnName from " + normaltblTableName + " setWhereAndSetNeedBuild columnName=? order by id";
        List<Object> param = new ArrayList<Object>();
        param.add(name);
        try {
            rc = tddlQueryData(sql, param);
            Assert.assertEquals(1, resultsSize(rc));
        } finally {
            if (rc != null) {
                rc.close();
            }

        }
    }

    @Ignore("目前不支持distinctrow")
    @Test
    public void distinctrowTest() throws Exception {
        String sql = "selectStatement distinctrow columnName from " + normaltblTableName + " setWhereAndSetNeedBuild columnName=?";
        List<Object> param = new ArrayList<Object>();
        param.add(name);
        try {
            rc = tddlQueryData(sql, param);
            Assert.assertEquals(1, resultsSize(rc));
        } finally {
            rc.close();
        }
    }

    /**
     * where后or两边为不同字段
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void orWithDifFiledTest() throws Exception {
        long pk = 2l;
        int id = 3;

        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild pk= ? or id=?";
        List<Object> param = new ArrayList<Object>();
        param.add(pk);
        param.add(id);
        String[] columnParam = {"columnName", "pk", "id"};
        selectContentSameAssert(sql, columnParam, param);
    }

    /**
     * where后or两边为同字段
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void orWithGroupTest() throws Exception {
        long pk = 2l;
        int id = 3;

        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild (pk= ? or pk > ?) or id=?";
        List<Object> param = new ArrayList<Object>();
        param.add(pk);
        param.add(pk + 1);
        param.add(id);
        String[] columnParam = {"columnName", "pk", "id"};
        selectContentSameAssert(sql, columnParam, param);
    }

    /**
     * orderby pk limit start,num
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void limitWithStart() throws Exception {
        int start = 5;
        int limit = 6;
        String sql = "SELECT * FROM " + normaltblTableName + " order by pk LIMIT " + start + "," + limit;
        String[] columnParam = {"columnName", "pk", "id"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

    }

    /**
     * 子查询中使用limit
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void selectLimit() throws Exception {
        int start = 5;
        int limit = 1;
        String sql = "selectStatement * from " + studentTableName + " setAliasAndSetNeedBuild nor1 ,(selectStatement pk from " + normaltblTableName + " setWhereAndSetNeedBuild columnName=? limit ?,?) setAliasAndSetNeedBuild nor2 setWhereAndSetNeedBuild nor1.id=nor2.pk";

        List<Object> param = new ArrayList<Object>();
        param.add(name);
        param.add(start);
        param.add(limit);
        selectConutAssert(sql, param);
    }

    /**
     * limit num
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void limitWithoutStart() throws Exception {
        int limit = 50;
        String sql = "SELECT * FROM " + normaltblTableName + " LIMIT " + limit;
        String[] columnParam = {"columnName", "pk", "id"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * 狄龙项目的 groupby 时间函数
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void groupByScalarFunction() throws Exception {
        if (!normaltblTableName.startsWith("ob")) { // ob不支持
            String sql = "SELECT  count(1) daily_illegal,DATE_FORMAT(gmt_create, '%Y-%m-%d') d ,columnName FROM " + normaltblTableName + " group by DATE_FORMAT(gmt_create, '%Y-%m-%d'),columnName";
            String[] columnParam = {"daily_illegal", "d", "columnName"};
            selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
        }
    }

    /**
     * groupby columnName limit 1
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void groupByLimitTest() throws Exception {
        tddlUpdateData("insert into " + normaltblTableName + " (pk,columnName) columnValueList(?,?)", Arrays.asList(new Object[]{RANDOM_ID, newName}));
        mysqlUpdateData("insert into " + normaltblTableName + " (pk,columnName) columnValueList(?,?)", Arrays.asList(new Object[]{RANDOM_ID, newName}));

        String sql = "query columnName,count(pk) from " + normaltblTableName + " group by columnName limit 1";
        String[] columnParam = {"columnName", "count(pk)"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

    }

    /**
     * order by pk limit 10
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void orderByLimitTest() throws Exception {
        tddlUpdateData("insert into " + normaltblTableName + " (pk,columnName) columnValueList(?,?)", Arrays.asList(new Object[]{RANDOM_ID, newName}));
        mysqlUpdateData("insert into " + normaltblTableName + " (pk,columnName) columnValueList(?,?)", Arrays.asList(new Object[]{RANDOM_ID, newName}));

        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild columnName=? order by pk limit 10 ";
        List<Object> param = new ArrayList<Object>();
        param.add(name);
        String[] columnParam = {"columnName", "pk"};
        selectOrderAssert(sql, columnParam, param);

        sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild columnName=? order by pk desc limit 10 ";
        selectOrderAssert(sql, columnParam, param);
    }

    /**
     * order by gmt_timestamp desc limit 2,5
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void dateTypeWithLimit() throws Exception {
        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild gmt_timestamp>? and gmt_timestamp <? and columnName like ? order by gmt_timestamp desc limit 2,5";
        List<Object> param = new ArrayList<Object>();
        param.add(gmtBefore);
        param.add(gmtNext);
        param.add(name);
        selectConutAssert(sql, param);

        sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild gmt_timestamp>=? and gmt_timestamp <=? and columnName like ? order by gmt_timestamp desc limit 2,5";
        param.clear();
        param.add(gmtBefore);
        param.add(gmtNext);
        param.add(name);
        selectConutAssert(sql, param);

        sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild gmt_timestamp>? and gmt_timestamp <? and columnName like ? order by gmt_timestamp desc limit 10,5";

        param.clear();
        param.add(gmtBefore);
        param.add(gmtNext);
        param.add(name);
        selectConutAssert(sql, param);
    }

    /**
     * order by gmt_timestamp desc limit 2,5
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void timestampTypeWithLimit() throws Exception {
        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild gmt_timestamp>? and gmt_timestamp <? and columnName like ? order by gmt_timestamp desc limit 2,5";
        List<Object> param = new ArrayList<Object>();
        param.add(gmtBefore);
        param.add(gmtNext);
        param.add(name);
        selectConutAssert(sql, param);

        sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild gmt_timestamp>=? and gmt_timestamp <=? and columnName like ? order by gmt_timestamp desc limit 2,5";
        param.clear();
        param.add(gmtBefore);
        param.add(gmtNext);
        param.add(name);
        selectConutAssert(sql, param);

        sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild gmt_timestamp>? and gmt_timestamp <? and columnName like ? order by gmt_timestamp desc limit 10,5";
        param.clear();
        param.add(gmtBefore);
        param.add(gmtNext);
        param.add(name);
        selectConutAssert(sql, param);
    }

    /**
     * order by gmt_timestamp desc limit 2,5
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void datetimeTypeWithLimit() throws Exception {
        String sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild gmt_datetime>? and gmt_datetime <? and columnName like ? order by gmt_datetime desc limit 2,5";
        List<Object> param = new ArrayList<Object>();
        param.add(gmtNext);
        param.add(gmtBefore);
        param.add(name);
        selectConutAssert(sql, param);

        sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild gmt_datetime>=? and gmt_datetime <=? and columnName like ? order by gmt_datetime desc limit 2,5";
        param.clear();
        param.add(gmtNext);
        param.add(gmtBefore);
        param.add(name);
        selectConutAssert(sql, param);

        sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild gmt_datetime>? and gmt_datetime <? and columnName like ? order by gmt_datetime desc limit 10,5";
        param.clear();
        param.add(gmtNext);
        param.add(gmtBefore);
        param.add(name);
        selectConutAssert(sql, param);

        sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild (gmt_datetime>=? or gmt_datetime <=?) and columnName like ? order by gmt_datetime desc limit 2,5";
        param.clear();
        param.add(gmtNext);
        param.add(gmtBefore);
        param.add(name);
        selectConutAssert(sql, param);

        sql = "selectStatement * from " + normaltblTableName + " setWhereAndSetNeedBuild (gmt_datetime>? or gmt_datetime <?) and columnName like ? order by gmt_datetime desc limit 10,5";
        param.clear();
        param.add(gmtNext);
        param.add(gmtBefore);
        param.add(name);
        selectConutAssert(sql, param);
    }

    /**
     * limit语法错误
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void limitError() throws Exception {
        int limit = -3;
        List<Object> param = new ArrayList<Object>();
        param.add(limit);
        String sql = "SELECT * FROM " + normaltblTableName + " LIMIT ";
        try {
            rc = tddlQueryData(sql, param);
            Assert.fail();
        } catch (Exception e) {
            Assert.assertNotNull(e.getMessage());
        }
    }

    @Ignore("目前不支持union")
    @Test
    public void unionTest() throws Exception {
    }

}
