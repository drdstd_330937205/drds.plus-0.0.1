package drds.plus.qatest.matrix.select;

import drds.plus.qatest.BaseMatrixTestCase;
import drds.plus.qatest.BaseTestCase;
import drds.plus.qatest.ExecuteTableName;
import drds.plus.qatest.util.EclipseParameterized;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * selectStatement like
 *
 * @author zhuoxue
 * @since 5.0.1
 */
@RunWith(EclipseParameterized.class)
public class SelectWithLike extends BaseMatrixTestCase {

    public SelectWithLike(String tableName) {
        BaseTestCase.normaltblTableName = tableName;
    }

    @Parameters(name = "{indexMapping}:where={0}")
    public static List<String[]> prepareData() {
        return Arrays.asList(ExecuteTableName.normaltblTable(dbType));
    }

    @Before
    public void prepareDate() throws Exception {
        normaltblPrepare(0, 20);
        normaltblTwoPrepare();
    }

    /**
     * like模糊匹配 '%'匹配符测试
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void LikeAnyTest() throws Exception {
        String sql = "query * from " + normaltblTableName + "  having columnName like 'zhuo%'";
        String[] columnParam = {"ID", "NAME"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "query * from " + normaltblTableName + " having columnName like '%uo%'";
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "query * from " + normaltblTableName + " having columnName like '%uo%u%'";
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * like模糊匹配 '_'匹配符测试
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void LikeOneTest() throws Exception {
        String sql = "query * from " + normaltblTableName + " having columnName like 'zhuoxu_'";
        String[] columnParam = {"ID", "NAME"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "query * from " + normaltblTableName + " having columnName like '_huoxu_'";
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "query * from " + normaltblTableName + " having columnName like '_hu_xu_'";
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * like对特定字符测试，like匹配不区别大小写
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void LikeSpecificTest() throws Exception {
        String sql = "query * from " + normaltblTableName + " having columnName like 'zhuoxue'";
        String[] columnParam = {"ID", "NAME"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        if (!normaltblTableName.contains("ob_")) {
            sql = "query * from " + normaltblTableName + " having columnName like 'ZHuoXUE'";
            selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
        }
    }

    /**
     * like对特定字符测试，like binary 区别大小写 暂时不支持
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Ignore("like binary暂时不支持")
    @Test
    public void LikeBinaryTest() throws Exception {
        String sql = "query * from " + normaltblTableName + " having columnName like binary 'zhuoxue'";
        rs = mysqlQueryData(sql, null);
        rc = tddlQueryData(sql, null);

        Assert.assertEquals(resultsSize(rs), resultsSize(rc));
    }

    /**
     * like对包含_和%匹配字段的匹配
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void MatchCharTest() throws Exception {
        String sql = "query * from " + normaltblTableName + " having columnName like 'zhuoxue\\_yll'";
        String[] columnParam = {"ID", "NAME"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "query * from " + normaltblTableName + " having columnName like 'zhuoxue\\%yll'";
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * NotLike测试，使用比较少，所以不支持
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Ignore("NotLike测试，使用比较少，所以不支持")
    @Test
    public void NotLikeTest() throws Exception {
        String sql = "query * from " + normaltblTableName + " having columnName not like 'zhuo%'";
        rs = mysqlQueryData(sql, null);
        rc = tddlQueryData(sql, null);
        Assert.assertEquals(resultsSize(rs), resultsSize(rc));

        sql = "query * from " + normaltblTableName + " having columnName not like 'uo%'";
        rs = mysqlQueryData(sql, null);
        rc = tddlQueryData(sql, null);
        Assert.assertEquals(resultsSize(rs), resultsSize(rc));

        sql = "query * from " + normaltblTableName + " having columnName not like 'zhuoxu_'";
        rs = mysqlQueryData(sql, null);
        rc = tddlQueryData(sql, null);
        Assert.assertEquals(resultsSize(rs), resultsSize(rc));

        sql = "query * from " + normaltblTableName + " having columnName not like 'uoxu_'";
        rs = mysqlQueryData(sql, null);
        rc = tddlQueryData(sql, null);
        Assert.assertEquals(resultsSize(rs), resultsSize(rc));

        sql = "query * from " + normaltblTableName + " having columnName not like 'ZHuoXUE'";
        rs = mysqlQueryData(sql, null);
        rc = tddlQueryData(sql, null);
        Assert.assertEquals(resultsSize(rs), resultsSize(rc));
    }

    /**
     * like和其他字段一起的测试
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void likeWithAndTest() throws Exception {
        int id = 500;
        String sql = "query * from " + normaltblTableName + " having columnName like 'zhuoxue' and id>" + id;
        String[] columnParam = {"ID", "NAME"};
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

        sql = "query * from " + normaltblTableName + " having columnName like 'zhuoxue\\%yll' and id <" + id;
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * like limit
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void likeWithLimit() throws Exception {
        String sql = "query * from " + normaltblTableName + " having columnName like ? limit ?";
        List<Object> param = new ArrayList<Object>();
        param.add("zhuo%");
        param.add(10);
        selectConutAssert(sql, param);

        sql = "query * from " + normaltblTableName + " having columnName like ? limit ?,?";
        param.clear();
        param.add("%uo%");
        param.add(15);
        param.add(25);
        selectConutAssert(sql, param);

        sql = "query * from " + normaltblTableName + " setAliasAndSetNeedBuild tab  setWhereAndSetNeedBuild columnName like ? and gmt_timestamp > ? limit ?,?";
        param.clear();
        param.add("%uo%");
        param.add(gmt);
        param.add(15);
        param.add(25);
        selectConutAssert(sql, param);

        sql = "query * from " + normaltblTableName + " setAliasAndSetNeedBuild tab setWhereAndSetNeedBuild (columnName like ? and gmt_timestamp > ?) limit ?,?";
        selectConutAssert(sql, param);
    }

    /**
     * like orderby
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void likeWithOrder() throws Exception {
        String[] columnParam = {"columnName", "gmt_timestamp"};
        String notKeyCloumn = "gmt_timestamp";
        String sql = "SELECT * from " + normaltblTableName + " having columnName like ? and gmt_timestamp> ? and gmt_timestamp< ? order by gmt_timestamp";
        List<Object> param = new ArrayList<Object>();
        param.add("%zh%xue%");
        param.add(gmtBefore);
        param.add(gmtNext);
        selectOrderAssertNotKeyCloumn(sql, columnParam, param, notKeyCloumn);

        sql = "SELECT * from " + normaltblTableName + " having columnName like ? and gmt_timestamp> ? and gmt_timestamp< ? order by gmt_timestamp desc";
        selectOrderAssertNotKeyCloumn(sql, columnParam, param, notKeyCloumn);

        sql = "SELECT * from " + normaltblTableName + " setAliasAndSetNeedBuild tab having (columnName like ? and (gmt_timestamp> ? and gmt_timestamp< ?)) order by  gmt_timestamp desc";
        selectOrderAssertNotKeyCloumn(sql, columnParam, param, notKeyCloumn);

        sql = "SELECT * from " + normaltblTableName + " setAliasAndSetNeedBuild tab having (columnName like '%zhuo%' and (gmt_timestamp> '2011-1-1' and gmt_timestamp< '2018-7-9')) order by  gmt_timestamp desc limit 10";
        selectOrderAssertNotKeyCloumn(sql, columnParam, Collections.EMPTY_LIST, notKeyCloumn);

        sql = "SELECT * from " + normaltblTableName + " setAliasAndSetNeedBuild tab having (columnName like ? and (gmt_timestamp> ? and gmt_timestamp< ?)) order by  gmt_timestamp desc limit ?,?";
        param.clear();
        param.add("%zh%xue%");
        param.add(gmtBefore);
        param.add(gmtNext);
        param.add(15);
        param.add(25);
        selectOrderAssertNotKeyCloumn(sql, columnParam, param, notKeyCloumn);
    }

}
