package drds.plus.qatest.matrix.template;

import drds.plus.qatest.BaseTemplateTestCase;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;
import java.util.Map;

/**
 * JDBCTemplateTest
 *
 * @author zhuoxue
 * @since 5.0.1
 */

public class JDBCTemplateTest extends BaseTemplateTestCase {

    /**
     * CRUD
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void CRUDTest() throws Exception {
        sql = String.format("insert into %s (pk,columnName) columnValueList(?,?)", normaltblTableName);
        //andorJT.update(sql_process, new Object[]{RANDOM_ID, id});

        sql = String.format("selectStatement * from %s setWhereAndSetNeedBuild pk= ?", normaltblTableName);
        Map re = null;// andorJT.queryForMap(sql_process, new Object[]{RANDOM_ID});
        Assert.assertEquals(name, String.valueOf(re.get("columnName")));

        sql = String.format("update %s setLimitValue columnName =? setWhereAndSetNeedBuild pk=? ", normaltblTableName);
        //andorJT.update(sql_process, new Object[]{name1, RANDOM_ID});

        sql = String.format("selectStatement * from %s setWhereAndSetNeedBuild pk= ?", normaltblTableName);
        //re = andorJT.queryForMap(sql_process, new Object[]{RANDOM_ID});
        Assert.assertEquals(name1, String.valueOf(re.get("columnName")));

        sql = String.format("delete from %s setWhereAndSetNeedBuild pk = ?", normaltblTableName);
        //andorJT.update(sql_process, new Object[]{RANDOM_ID});

        sql = String.format("selectStatement * from %s setWhereAndSetNeedBuild pk= ?", normaltblTableName);
        List le = null;// andorJT.queryForList(sql_process, new Object[]{RANDOM_ID});
        Assert.assertEquals(0, le.size());
    }

    /**
     * traction Commit
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void tractionCommitTest() {

        sql = String.format("insert into %s (pk,columnName) columnValueList(?,?)", normaltblTableName);
        //andorJT.update(sql_process, new Object[]{RANDOM_ID, id});
        sql = String.format("selectStatement * from %s setWhereAndSetNeedBuild pk= ?", normaltblTableName);
        Map re = null;// andorJT.queryForMap(sql_process, new Object[]{RANDOM_ID});
        Assert.assertEquals(name, String.valueOf(re.get("columnName")));
        sql = String.format("selectStatement * from %s setWhereAndSetNeedBuild pk= ?", normaltblTableName);
        //Map re =null;// andorJT.queryForMap(sql_process, new Object[]{RANDOM_ID});
        Assert.assertEquals(name, String.valueOf(re.get("columnName")));
    }

    /**
     * traction RollBack
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void tractionRollBackTest() {
        sql = String.format("insert into %s (pk,columnName) columnValueList(?,?)", normaltblTableName);
        //andorJT.update(sql_process, new Object[]{RANDOM_ID, id});
        sql = String.format("selectStatement * from %s setWhereAndSetNeedBuild pk= ?", normaltblTableName);
        Map re = null;// andorJT.queryForMap(sql_process, new Object[]{RANDOM_ID});
        Assert.assertEquals(name, String.valueOf(re.get("columnName")));
        // 回滚
        //transactionManager.rollback(ts);
        // 验证查询不到数据
        sql = String.format("selectStatement * from %s setWhereAndSetNeedBuild pk= ?", normaltblTableName);
        List le = null;// andorJT.queryForList(sql_process, new Object[]{RANDOM_ID});
        Assert.assertEquals(0, le.size());
    }
}
