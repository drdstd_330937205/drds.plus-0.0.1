package drds.plus.qatest.matrix.join;

import drds.plus.qatest.BaseMatrixTestCase;
import drds.plus.qatest.BaseTestCase;
import drds.plus.qatest.ExecuteTableName;
import drds.plus.qatest.util.EclipseParameterized;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * 带function的join
 *
 * @author zhuoxue
 * @since 5.0.1
 */
@RunWith(EclipseParameterized.class)
public class JoinWithFunctionTest extends BaseMatrixTestCase {

    public JoinWithFunctionTest(String monitor_host_infoTableName, String monitor_hostgroup_infoTableName, String studentTableName, String monitor_module_infoTableName, String monitor_module_hostTableName) throws Exception {
        BaseTestCase.host_info = monitor_host_infoTableName;
        BaseTestCase.hostgroup_info = monitor_hostgroup_infoTableName;
        BaseTestCase.studentTableName = studentTableName;
        BaseTestCase.module_info = monitor_module_infoTableName;
        BaseTestCase.module_host = monitor_module_hostTableName;
        initData();
    }

    @Parameters(name = "{indexMapping}:table0={0},table1={1},table2={2},table3={3},table4={4}")
    public static List<String[]> prepareDate() {
        return Arrays.asList(ExecuteTableName.hostinfoHostgoupStudentModuleinfoModulehostTable(dbType));
    }

    public void initData() throws Exception {
        hostinfoPrepare(0, 100);
        hostgroupInfoPrepare(50, 200);
        module_infoPrepare(0, 40);
        module_hostPrepare(1, 80);
        studentPrepare(65, 80);
    }

    @After
    public void destory() throws Exception {
        psConRcRsClose(rc, rs);
    }

    /**
     * 带max min 的join
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void joinMaxMinTest() throws Exception {
        String[] columnParamMax = {"max(" + host_info + ".host_id)"};
        String sql = "query max(" + host_info + ".host_id)," + host_info + ".host_name," + host_info + ".hostgroup_id," + hostgroup_info + ".hostgroup_name " + " from " + host_info + " inner join " + hostgroup_info + "  " + " on " + host_info + ".hostgroup_id=" + hostgroup_info + ".hostgroup_id";
        selectContentSameAssert(sql, columnParamMax, Collections.EMPTY_LIST);

        String[] columnParamMin = {"min(" + host_info + ".host_id)"};
        sql = "query min(" + host_info + ".host_id)," + host_info + ".host_name," + host_info + ".hostgroup_id," + hostgroup_info + ".hostgroup_name " + " from " + host_info + " inner join " + hostgroup_info + "  " + " on " + host_info + ".hostgroup_id=" + hostgroup_info + ".hostgroup_id";
        selectContentSameAssert(sql, columnParamMin, Collections.EMPTY_LIST);

        String[] columnParamAlias = {"min"};
        sql = "query min(" + host_info + ".host_id) min," + host_info + ".host_name," + host_info + ".hostgroup_id," + hostgroup_info + ".hostgroup_name " + " from " + host_info + " inner join " + hostgroup_info + "  " + " on " + host_info + ".hostgroup_id=" + hostgroup_info + ".hostgroup_id";
        selectContentSameAssert(sql, columnParamAlias, Collections.EMPTY_LIST);
    }

    /**
     * 带sum 的join
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void joinSumTest() throws Exception {
        String[] columnParamSum = {"sum(" + host_info + ".host_id)"};
        String sql = "selectStatement sum(" + host_info + ".host_id)," + host_info + ".host_name," + host_info + ".hostgroup_id," + hostgroup_info + ".hostgroup_name " + " from " + host_info + " inner join " + hostgroup_info + "  " + " on " + host_info + ".hostgroup_id=" + hostgroup_info + ".hostgroup_id";
        selectContentSameAssert(sql, columnParamSum, Collections.EMPTY_LIST);
    }

    /**
     * 带avg的join
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void joinAvgTest() throws Exception {
        String[] columnParamAvg = {"avg(" + host_info + ".host_id)"};
        String sql = "selectStatement avg(" + host_info + ".host_id)," + host_info + ".host_name," + host_info + ".hostgroup_id," + hostgroup_info + ".hostgroup_name " + " from " + host_info + " inner join " + hostgroup_info + "  " + " on " + host_info + ".hostgroup_id=" + hostgroup_info + ".hostgroup_id";
        selectContentSameAssert(sql, columnParamAvg, Collections.EMPTY_LIST);
    }

    /**
     * 带count 的join
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void joinCountTest() throws Exception {
        String[] columnParamCount = {"count(" + host_info + ".host_id)"};
        String sql = "selectStatement count(" + host_info + ".host_id)," + host_info + ".host_name," + host_info + ".hostgroup_id," + hostgroup_info + ".hostgroup_name " + " from " + host_info + " inner join " + hostgroup_info + "  " + " on " + host_info + ".hostgroup_id=" + hostgroup_info + ".hostgroup_id";
        selectContentSameAssert(sql, columnParamCount, Collections.EMPTY_LIST);

        String[] columnCount = {"count(*)"};
        sql = "selectStatement count(*)," + host_info + ".host_name," + host_info + ".hostgroup_id," + hostgroup_info + ".hostgroup_name " + " from " + host_info + " inner join " + hostgroup_info + "  " + " on " + host_info + ".hostgroup_id=" + hostgroup_info + ".hostgroup_id";
        selectContentSameAssert(sql, columnCount, Collections.EMPTY_LIST);
    }

    /**
     * 带max 的join, 加where条件
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void JoinWithAndMaxTest() throws Exception {
        String[] columnParam = {"max(" + host_info + ".host_id)"};
        String sql = "selectStatement max(" + host_info + ".host_id)," + "" + host_info + ".host_name," + host_info + ".hostgroup_id," + hostgroup_info + ".hostgroup_name " + "from " + hostgroup_info + " inner join " + host_info + "  " + "on " + host_info + ".hostgroup_id=" + hostgroup_info + ".hostgroup_id where " + host_info + ".host_name='hostname52'";
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * 带sum 的join, 加where条件
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void JoinWithWhereAndSumTest() throws Exception {
        String[] columnParam = {"sum(" + host_info + ".host_id)"};
        String sql = "selectStatement sum(" + host_info + ".host_id)," + "" + host_info + ".host_name," + host_info + ".hostgroup_id," + hostgroup_info + ".hostgroup_name " + "from " + hostgroup_info + " inner join " + host_info + "  " + "on " + host_info + ".hostgroup_id=" + hostgroup_info + ".hostgroup_id where " + host_info + ".host_name='hostname80' and " + hostgroup_info + ".hostgroup_name='hostgroupname80'";
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * 带count 的join, where条件中为or
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void JoinWithWhereOrCountTest() throws Exception {
        String[] columnParam = {"count(" + host_info + ".host_id)"};
        // bdb数据库join测试以下测试用例抛出异常，原因目前只支持单值查询
        if (host_info.contains("mysql")) {
            String sql = "selectStatement count(" + host_info + ".host_id)," + "" + host_info + ".host_name," + host_info + ".hostgroup_id," + hostgroup_info + ".hostgroup_name " + "from " + hostgroup_info + " inner join " + host_info + "  " + "on " + host_info + ".hostgroup_id=" + hostgroup_info + ".hostgroup_id where " + host_info + ".host_name='hostname50' or " + hostgroup_info + ".hostgroup_name='hostgroupname51'";
            selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
        }
    }

    /**
     * 带count 的join, where条件中为between
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void JoinWithWhereBetweenSumTest() throws Exception {
        String[] columnParam = {"sum(" + host_info + ".host_id)"};
        String sql = "selectStatement sum(" + host_info + ".host_id)," + "" + host_info + ".host_name," + host_info + ".hostgroup_id," + hostgroup_info + ".hostgroup_name " + "from " + hostgroup_info + " inner join " + host_info + "  " + "on " + host_info + ".hostgroup_id=" + hostgroup_info + ".hostgroup_id where " + host_info + ".hostgroup_id between 40 and 70";
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * 带count 的join, where条件中为between
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void JoinWithWhereLimitCountTest() throws Exception {
        String sql = "selectStatement count(" + host_info + ".host_id)," + "" + host_info + ".host_name," + host_info + ".hostgroup_id," + hostgroup_info + ".hostgroup_name " + "from " + hostgroup_info + " inner join " + host_info + "  " + "on " + host_info + ".hostgroup_id=" + hostgroup_info + ".hostgroup_id setWhereAndSetNeedBuild " + host_info + ".hostgroup_id between 40 and 70 limit 10";

        selectConutAssert(sql, Collections.EMPTY_LIST);
    }

    /**
     * 带min 的join, where条件中为orderby + limit
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void JoinWithWhereOrderByLimitMinTest() throws Exception {
        String[] columnParam = {"min(" + host_info + ".host_id)"};
        String sql = "selectStatement min(" + host_info + ".host_id)," + "" + host_info + ".host_name," + host_info + ".hostgroup_id," + hostgroup_info + ".hostgroup_name " + "from " + host_info + " inner join " + hostgroup_info + "  " + "on " + host_info + ".hostgroup_id=" + hostgroup_info + ".hostgroup_id setWhereAndSetNeedBuild " + host_info + ".hostgroup_id order by " + host_info + ".hostgroup_id limit 10";
        selectOrderAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * 带min 的join, group by
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void InnerJoinWithGroupbyTest() throws Exception {
        String[] columnParam = {"min(" + host_info + ".host_id)"};
        String sql = "selectStatement min(" + host_info + ".host_id)," + "" + host_info + ".host_name columnName from " + host_info + " inner join " + hostgroup_info + " on " + host_info + ".hostgroup_id=" + hostgroup_info + ".hostgroup_id group by columnName";
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    /**
     * 带min 的join, orderby, group by
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void InnerJoinWithOrderGroupbyTest() throws Exception {
        String[] columnParam = {"min(" + host_info + ".host_id)"};
        String sql = "/* ANDOR ALLOW_TEMPORARY_TABLE=True */ selectStatement min(" + host_info + ".host_id)," + "" + host_info + ".host_name columnName from " + hostgroup_info + " inner join " + host_info + " on " + host_info + ".hostgroup_id=" + hostgroup_info + ".hostgroup_id group by columnName order by " + host_info + ".hostgroup_id";
        selectOrderAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

    @Test
    public void JoinThreeTableWithWhereCount() throws Exception {
        String[] columnParam = {"count(*)"};
        String sql = "SELECT count(*) from " + host_info + " INNER JOIN " + module_host + " ON " + host_info + ".host_id=" + module_host + ".host_id INNER JOIN " + module_info + "  ON " + module_info + ".module_id=" + module_host + ".module_id setWhereAndSetNeedBuild " + module_info + ".module_name='module1' or " + module_info + ".module_name='module4'";
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);

    }

    /**
     * 带sum别名 的join
     *
     * @author zhuoxue
     * @since 5.0.1
     */
    @Test
    public void JoinWithAliasSumTest() throws Exception {
        String[] columnParam = {"sum(a.host_id)"};
        String sql = "selectStatement sum(a.host_id),a.host_name,a.hostgroup_id,b.hostgroup_name from " + host_info + " a inner join " + hostgroup_info + " b " + "on a.hostgroup_id=b.hostgroup_id";
        selectContentSameAssert(sql, columnParam, Collections.EMPTY_LIST);
    }

}
