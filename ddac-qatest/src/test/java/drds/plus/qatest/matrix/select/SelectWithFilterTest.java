package drds.plus.qatest.matrix.select;

import drds.plus.qatest.BaseMatrixTestCase;
import drds.plus.qatest.BaseTestCase;
import drds.plus.qatest.ExecuteTableName;
import drds.plus.qatest.util.EclipseParameterized;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 带条件的选择查询
 */

@RunWith(EclipseParameterized.class)
public class SelectWithFilterTest extends BaseMatrixTestCase {

    public SelectWithFilterTest(String normaltblTableName) {
        BaseTestCase.normaltblTableName = normaltblTableName;
    }

    @Parameters(name = "{indexMapping}:table0={0}")
    public static List<String[]> prepare() {
        return Arrays.asList(ExecuteTableName.normaltblTable(dbType));
    }

    @Before
    public void prepareDate() throws Exception {
        normaltblPrepare(0, 20);
    }

    /**
     * 大于
     */
    @Test
    public void greaterTest() throws Exception {
        String sql = "selectStatement columnName,count(pk) from " + normaltblTableName + " group by columnName having count(pk)>? order by columnName ";
        List<Object> param = new ArrayList<Object>();
        param.add(5L);
        String[] columnParam = {"NAME", "count(pk)"};
        selectOrderAssert(sql, columnParam, param);
    }

    /**
     * 大于等于
     */
    @Test
    public void greaterEqualTest() throws Exception {
        String sql = "selectStatement columnName,count(pk) from " + normaltblTableName + " group by columnName having count(pk)>=? order by columnName ";
        List<Object> param = new ArrayList<Object>();
        param.add(10L);
        String[] columnParam = {"NAME", "count(pk)"};
        selectOrderAssert(sql, columnParam, param);
    }

    /**
     * 小于
     */
    @Test
    public void lessTest() throws Exception {
        String sql = "selectStatement columnName,count(pk) from " + normaltblTableName + " group by columnName having count(pk)<? order by columnName ";
        List<Object> param = new ArrayList<Object>();
        param.add(5L);
        String[] columnParam = {"NAME", "count(pk)"};
        selectOrderAssert(sql, columnParam, param);
    }

    /**
     * 小于等于
     */
    @Test
    public void lessEqualTest() throws Exception {
        String sql = "selectStatement columnName,count(pk) from " + normaltblTableName + " group by columnName having count(pk)<=? order by columnName ";
        List<Object> param = new ArrayList<Object>();
        param.add(9L);
        String[] columnParam = {"NAME", "count(pk)"};
        selectOrderAssert(sql, columnParam, param);
    }

    /**
     * 等于
     */
    @Test
    public void equalTest() throws Exception {
        String sql = "query columnName,count(pk) from " + normaltblTableName + " group by columnName having count(pk)=? order by columnName ";
        List<Object> param = new ArrayList<Object>();
        param.add(10L);
        String[] columnParam = {"NAME", "count(pk)"};
        selectOrderAssert(sql, columnParam, param);
    }

    /**
     * in
     */
    @Test
    public void inTest() throws Exception {
        String sql = "query columnName,count(pk) from " + normaltblTableName + " group by columnName having count(pk) in (?) order by columnName ";
        List<Object> param = new ArrayList<Object>();
        param.add(10L);
        String[] columnParam = {"NAME", "count(pk)"};
        selectOrderAssert(sql, columnParam, param);
    }

    /**
     * 不等于
     */
    @Test
    public void notEqualTest() throws Exception {
        String sql = "query columnName,count(pk) from " + normaltblTableName + " group by columnName having count(pk) != ? order by columnName ";
        List<Object> param = new ArrayList<Object>();
        param.add(10L);
        String[] columnParam = {"NAME", "count(pk)"};
        selectOrderAssert(sql, columnParam, param);
    }

    /**
     * having中带and
     */
    @Test
    public void andTest() throws Exception {
        String sql = "query columnName,count(pk) from " + normaltblTableName + " group by columnName having count(pk) != ? and count(pk) < ? order by columnName ";
        List<Object> param = new ArrayList<Object>();
        param.add(10L);
        param.add(11L);
        String[] columnParam = {"NAME", "count(pk)"};
        selectOrderAssert(sql, columnParam, param);
    }

    /**
     * group or
     */
    @Test
    public void groupOrTest() throws Exception {
        String sql = "query columnName,count(pk) from " + normaltblTableName + " where (pk > 2 or pk in (1,3,5,7)) and columnName is not null group by columnName having count(pk) in (?) order by columnName ";
        List<Object> param = new ArrayList<Object>();
        param.add(10L);
        String[] columnParam = {"NAME", "count(pk)"};
        selectOrderAssert(sql, columnParam, param);
    }

    /**
     * having中带or
     */
    @Test
    public void orTest() throws Exception {
        String sql = "query columnName,count(pk) from " + normaltblTableName + " group by columnName having count(pk) != ? or count(pk) < ? order by columnName ";
        List<Object> param = new ArrayList<Object>();
        param.add(10L);
        param.add(11L);
        String[] columnParam = {"NAME", "count(pk)"};
        selectOrderAssert(sql, columnParam, param);
    }

    /**
     * having中带and、or
     */
    @Test
    public void andOrTest() throws Exception {
        String sql = "query columnName,count(pk) from " + normaltblTableName + " group by columnName having (count(pk) != ? or count(pk) < ?) and count(pk)>? order by columnName ";
        List<Object> param = new ArrayList<Object>();
        param.add(10L);
        param.add(11L);
        param.add(5L);
        String[] columnParam = {"NAME", "count(pk)"};
        selectOrderAssert(sql, columnParam, param);
    }

    /**
     * having中带常量
     */
    @Test
    public void constantTest() throws Exception {
        String sql = "query columnName,count(pk) from " + normaltblTableName + " group by columnName having 1 order by columnName ";
        List<Object> param = new ArrayList<Object>();

        String[] columnParam = {"NAME", "count(pk)"};
        selectOrderAssert(sql, columnParam, param);
    }

    /**
     * having中带true常量
     */
    @Test
    public void constantTest2() throws Exception {
        String sql = "selectStatement columnName,count(pk) from " + normaltblTableName + " group by columnName having true order by columnName ";
        List<Object> param = new ArrayList<Object>();

        String[] columnParam = {"NAME", "count(pk)"};
        selectOrderAssert(sql, columnParam, param);
    }

    /**
     * having中带‘true’常量
     */
    @Test
    public void constantTest3() throws Exception {
        String sql = "query columnName,count(pk) from " + normaltblTableName + " group by columnName having 'true' order by columnName ";
        List<Object> param = new ArrayList<Object>();

        String[] columnParam = {"NAME", "count(pk)"};
        selectOrderAssert(sql, columnParam, param);
    }

    /**
     * having中带 常量and常量
     */
    @Test
    public void constantAndTest() throws Exception {
        String sql = "query columnName,count(pk) from " + normaltblTableName + " group by columnName having 1 and 2 order by columnName ";
        List<Object> param = new ArrayList<Object>();

        String[] columnParam = {"NAME", "count(pk)"};
        selectOrderAssert(sql, columnParam, param);
    }

    /**
     * having中带 常量or常量
     */
    @Test
    public void constantOrTest() throws Exception {
        String sql = "query columnName,count(pk) from " + normaltblTableName + " group by columnName having 1 or 2 order by columnName ";
        List<Object> param = new ArrayList<Object>();

        String[] columnParam = {"NAME", "count(pk)"};
        selectOrderAssert(sql, columnParam, param);
    }

}
