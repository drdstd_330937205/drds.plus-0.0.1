package drds.plus.qatest.matrix.transaction;

import drds.plus.api.SessionImpl;
import drds.plus.common.jdbc.transaction_policy.ITransactionPolicy;
import drds.plus.qatest.BaseMatrixTestCase;
import drds.plus.qatest.BaseTestCase;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * <pre>
 * policy_1 : 完全禁止跨库事务
 *  policy_2 : autocommit=true时跨库写在多个库上commit  先不測
 *  policy_3 : 禁止跨库写，允许跨库读
 *  policy_4 : policy_2的基础上允许跨库读 			先不測
 *  policy_5 : 完全允许跨库事务
 * </pre>
 */
public class TransactionMultiGroupTest extends BaseMatrixTestCase {

    public TransactionMultiGroupTest() {
        BaseTestCase.normaltblTableName = "mysql_normaltbl_mutilGroup";
    }

    @Before
    public void initData() throws Exception {
        tddlUpdateData("delete FROM " + normaltblTableName, null);
        mysqlUpdateData("delete FROM " + normaltblTableName, null);
        setTxPolicy(ITransactionPolicy.strict_write_with_non_transaction_cross_database_read);
    }

    @After
    public void destory() throws Exception {
        psConRcRsClose(rc, rs);
        tddlConnection.setAutoCommit(true);
        mysqlConnection.setAutoCommit(true);
        setTxPolicy(ITransactionPolicy.strict_write_with_non_transaction_cross_database_read);
    }

    // policy_1 : 完全禁止跨库事务
    @Test
    public void testPolicy1Query() throws Exception {
        normaltblPrepare(0, MAX_DATA_SIZE);
        setTxPolicy(ITransactionPolicy.strict);

        String sql = "selectStatement * from " + normaltblTableName;
        tddlConnection.setAutoCommit(false);
        try {
            ResultSet rs = tddlQueryData(sql, null);
            while (rs.next()) {
            }
            Assert.fail();
        } catch (Exception e) {
            // e.printStackTrace();
        }
        tddlConnection.commit();
        tddlConnection.setAutoCommit(true);

    }

    // policy_1 : 完全禁止跨库事务, 单个insert应该没问题
    @Test
    public void testPolicy1SingleInsert() throws Exception {
        setTxPolicy(ITransactionPolicy.strict);
        tddlConnection.setAutoCommit(false);
        mysqlConnection.setAutoCommit(false);
        String sql = "insert into " + normaltblTableName + " columnValueList(?,?,?,?,?,?,?)";
        List<Object> param = new ArrayList<Object>();
        param.add(RANDOM_ID);
        param.add(RANDOM_INT);
        param.add(gmtDay);
        param.add(gmt);
        param.add(gmt);
        param.add(null);
        param.add(fl);

        try {
            execute(sql, param);
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }

        sql = "query * from " + normaltblTableName + " where pk=" + RANDOM_ID;
        String[] columnParam = {"PK", "NAME", "ID", "gmt_create", "GMT_TIMESTAMP", "GMT_DATETIME", "floatCol"};
        selectContentSameAssert(sql, columnParam, null);
        tddlConnection.commit();
        mysqlConnection.commit();
        tddlConnection.setAutoCommit(true);
        mysqlConnection.setAutoCommit(true);

        selectContentSameAssert(sql, columnParam, null);

    }

    // policy_1 : 完全禁止跨库事务, 涉及到多库的多次insert应该报错
    @Test
    public void testPolicy1MultiInsertCommit() throws Exception {
        setTxPolicy(ITransactionPolicy.strict);
        tddlConnection.setAutoCommit(false);

        String sql = "insert into " + normaltblTableName + " columnValueList(?,?,?,?,?,?,?)";
        List<Object> param = new ArrayList<Object>();
        param.add(RANDOM_ID);
        param.add(RANDOM_INT);
        param.add(gmtDay);
        param.add(gmt);
        param.add(gmt);
        param.add(null);
        param.add(fl);

        List<Object> param1 = new ArrayList<Object>();
        param1.add(RANDOM_ID + 2);
        param1.add(RANDOM_INT);
        param1.add(gmtDay);
        param1.add(gmt);
        param1.add(gmt);
        param1.add(null);
        param1.add(fl);

        try {
            tddlUpdateData(sql, param);
            tddlUpdateData(sql, param1);
            Assert.fail();
        } catch (Exception e) {
            // e.printStackTrace();
        }
        tddlConnection.commit();
        tddlConnection.setAutoCommit(true);

        // 第一条数据应该插入成功
        sql = "query * from " + normaltblTableName + " where pk=" + RANDOM_ID;
        ResultSet rs = tddlQueryData(sql, null);
        Assert.assertTrue(rs.next());
        Assert.assertFalse(rs.next());

        // 第二条数据应该失败
        sql = "query * from " + normaltblTableName + " where pk=" + (RANDOM_ID + 2);
        rs = tddlQueryData(sql, null);
        Assert.assertFalse(rs.next());
    }

    // policy_1 : 完全禁止跨库事务, 涉及到多库的多次insert应该报错
    @Test
    public void testPolicy1MultiInsertRollback() throws Exception {
        setTxPolicy(ITransactionPolicy.strict);
        tddlConnection.setAutoCommit(false);

        String sql = "insert into " + normaltblTableName + " columnValueList(?,?,?,?,?,?,?)";
        List<Object> param = new ArrayList<Object>();
        param.add(RANDOM_ID);
        param.add(RANDOM_INT);
        param.add(gmtDay);
        param.add(gmt);
        param.add(gmt);
        param.add(null);
        param.add(fl);

        List<Object> param1 = new ArrayList<Object>();
        param1.add(RANDOM_ID + 2);
        param1.add(RANDOM_INT);
        param1.add(gmtDay);
        param1.add(gmt);
        param1.add(gmt);
        param1.add(null);
        param1.add(fl);

        try {
            tddlUpdateData(sql, param);
            tddlUpdateData(sql, param1);
            Assert.fail();
        } catch (Exception e) {

        }
        tddlConnection.rollback();
        tddlConnection.setAutoCommit(true);

        // 第一条数据应该插入失败
        sql = "query * from " + normaltblTableName + " where pk=" + RANDOM_ID;
        ResultSet rs = tddlQueryData(sql, null);
        Assert.assertFalse(rs.next());

        // 第二条数据应该失败
        sql = "query * from " + normaltblTableName + " where pk=" + (RANDOM_ID + 2);
        rs = tddlQueryData(sql, null);
        Assert.assertFalse(rs.next());
    }

    // policy_1 : 完全禁止跨库事务, update涉及多库, autocommit不是跨庫事務，不會報錯
    @Test
    public void testPolicy1UpdateWithAutoCommit() throws Exception {
        normaltblPrepare(0, MAX_DATA_SIZE);
        setTxPolicy(ITransactionPolicy.strict);
        String sql = "update " + normaltblTableName + " setLimitValue id=?, gmt_create=?,floatCol=?";
        List<Object> param = new ArrayList<Object>();
        param.add(RANDOM_INT);
        param.add(gmtDay);
        param.add(fl);
        try {
            execute(sql, param);
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }

        sql = "selectStatement * from " + normaltblTableName + " where pk=" + RANDOM_ID;
        String[] columnParam = {"PK", "NAME", "ID", "gmt_create", "GMT_TIMESTAMP", "GMT_DATETIME", "floatCol"};
        selectContentSameAssert(sql, columnParam, null);

    }

    // policy_1 : 完全禁止跨库事务, update涉及多库时，应该报错
    @Test
    public void testPolicy1UpdateWithNotAutoCommit() throws Exception {
        setTxPolicy(ITransactionPolicy.strict);
        tddlConnection.setAutoCommit(false);
        String sql = "update " + normaltblTableName + " setLimitValue id=?, gmt_create=?,floatCol=?";
        List<Object> param = new ArrayList<Object>();
        param.add(RANDOM_INT);
        param.add(gmtDay);
        param.add(fl);
        try {
            tddlUpdateData(sql, param);
            Assert.fail();
        } catch (Exception e) {

        }
        tddlConnection.commit();
        tddlConnection.setAutoCommit(true);

    }

    // policy_3 : 禁止跨库写，允许跨库读
    @Test
    public void testPolicy3Query() throws Exception {
        normaltblPrepare(0, MAX_DATA_SIZE);
        setTxPolicy(ITransactionPolicy.strict_write_with_non_transaction_cross_database_read);

        String sql = "selectStatement * from " + normaltblTableName;
        String[] columnParam = {"PK", "NAME", "ID", "gmt_create", "GMT_TIMESTAMP", "GMT_DATETIME", "floatCol"};
        tddlConnection.setAutoCommit(false);
        mysqlConnection.setAutoCommit(false);
        try {
            selectContentSameAssert(sql, columnParam, null);
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
        tddlConnection.commit();
        mysqlConnection.commit();
        tddlConnection.setAutoCommit(true);
        mysqlConnection.setAutoCommit(true);

    }

    // policy_3 : 禁止跨库写，允许跨库读
    @Test
    public void testPolicy3Insert() throws Exception {
        setTxPolicy(ITransactionPolicy.strict_write_with_non_transaction_cross_database_read);
        tddlConnection.setAutoCommit(false);
        mysqlConnection.setAutoCommit(false);
        String sql = "insert into " + normaltblTableName + " columnValueList(?,?,?,?,?,?,?)";
        List<Object> param = new ArrayList<Object>();
        param.add(RANDOM_ID);
        param.add(RANDOM_INT);
        param.add(gmtDay);
        param.add(gmt);
        param.add(gmt);
        param.add(null);
        param.add(fl);

        try {
            execute(sql, param);
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }

        sql = "selectStatement * from " + normaltblTableName + " where pk=" + RANDOM_ID;
        String[] columnParam = {"PK", "NAME", "ID", "gmt_create", "GMT_TIMESTAMP", "GMT_DATETIME", "floatCol"};
        selectContentSameAssert(sql, columnParam, null);
        tddlConnection.commit();
        mysqlConnection.commit();
        tddlConnection.setAutoCommit(true);
        mysqlConnection.setAutoCommit(true);

        selectContentSameAssert(sql, columnParam, null);

    }

    // policy_3 : 禁止跨库写，允许跨库读
    @Test
    public void testPolicy3Update() throws Exception {
        setTxPolicy(ITransactionPolicy.strict_write_with_non_transaction_cross_database_read);
        String sql = "update " + normaltblTableName + " setLimitValue id=?, gmt_create=?,floatCol=?";
        List<Object> param = new ArrayList<Object>();
        param.add(RANDOM_INT);
        param.add(gmtDay);
        param.add(fl);
        tddlConnection.setAutoCommit(false);
        try {
            tddlUpdateData(sql, param);
            Assert.fail();
        } catch (Exception e) {
        }

        tddlConnection.commit();
        tddlConnection.setAutoCommit(true);

    }

    // policy_5 : 完全允许跨库事务
    @Test
    public void testPolicy5Query() throws Exception {
        normaltblPrepare(0, MAX_DATA_SIZE);
        setTxPolicy(ITransactionPolicy.cobar_style);

        String sql = "selectStatement * from " + normaltblTableName;
        String[] columnParam = {"PK", "NAME", "ID", "gmt_create", "GMT_TIMESTAMP", "GMT_DATETIME", "floatCol"};
        tddlConnection.setAutoCommit(false);
        mysqlConnection.setAutoCommit(false);
        try {
            selectContentSameAssert(sql, columnParam, null);
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
        tddlConnection.commit();
        mysqlConnection.commit();
        tddlConnection.setAutoCommit(true);
        mysqlConnection.setAutoCommit(true);

    }

    // policy_5 : 完全允许跨库事务
    @Test
    public void testPolicy5Insert() throws Exception {
        setTxPolicy(ITransactionPolicy.cobar_style);
        tddlConnection.setAutoCommit(false);
        mysqlConnection.setAutoCommit(false);
        String sql = "insert into " + normaltblTableName + " columnValueList(?,?,?,?,?,?,?)";
        List<Object> param = new ArrayList<Object>();
        param.add(RANDOM_ID);
        param.add(RANDOM_INT);
        param.add(gmtDay);
        param.add(gmt);
        param.add(gmt);
        param.add(null);
        param.add(fl);

        try {
            execute(sql, param);
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }

        sql = "selectStatement * from " + normaltblTableName + " where pk=" + RANDOM_ID;
        String[] columnParam = {"PK", "NAME", "ID", "gmt_create", "GMT_TIMESTAMP", "GMT_DATETIME", "floatCol"};
        selectContentSameAssert(sql, columnParam, null);
        tddlConnection.commit();
        mysqlConnection.commit();
        tddlConnection.setAutoCommit(true);
        mysqlConnection.setAutoCommit(true);

        selectContentSameAssert(sql, columnParam, null);

    }

    // policy_5 : 完全允许跨库事务
    @Test
    public void testPolicy5UpdateWithAutoCommit() throws Exception {
        setTxPolicy(ITransactionPolicy.cobar_style);
        String sql = "update " + normaltblTableName + " setLimitValue id=?, gmt_create=?,floatCol=?";
        List<Object> param = new ArrayList<Object>();
        param.add(RANDOM_INT);
        param.add(gmtDay);
        param.add(fl);

        try {
            execute(sql, param);
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }

        sql = "selectStatement * from " + normaltblTableName + " where pk=" + RANDOM_ID;
        String[] columnParam = {"PK", "NAME", "ID", "gmt_create", "GMT_TIMESTAMP", "GMT_DATETIME", "floatCol"};
        selectContentSameAssert(sql, columnParam, null);

    }

    // policy_5 : 完全允许跨库事务
    @Test
    public void testPolicy5UpdateWithNotAutoCommit() throws Exception {
        setTxPolicy(ITransactionPolicy.cobar_style);
        String sql = "update " + normaltblTableName + " setLimitValue id=?, gmt_create=?,floatCol=?";
        List<Object> param = new ArrayList<Object>();
        param.add(RANDOM_INT);
        param.add(gmtDay);
        param.add(fl);

        tddlConnection.setAutoCommit(false);
        mysqlConnection.setAutoCommit(false);
        try {
            execute(sql, param);

            sql = "selectStatement * from " + normaltblTableName + " where pk=" + RANDOM_ID;
            String[] columnParam = {"PK", "NAME", "ID", "gmt_create", "GMT_TIMESTAMP", "GMT_DATETIME", "floatCol"};
            selectContentSameAssert(sql, columnParam, null);
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
        tddlConnection.commit();
        mysqlConnection.commit();
        tddlConnection.setAutoCommit(true);
        mysqlConnection.setAutoCommit(true);

        sql = "selectStatement * from " + normaltblTableName + " where pk=" + RANDOM_ID;
        String[] columnParam = {"PK", "NAME", "ID", "gmt_create", "GMT_TIMESTAMP", "GMT_DATETIME", "floatCol"};
        selectContentSameAssert(sql, columnParam, null);
    }

    private void setTxPolicy(ITransactionPolicy transactionPolicy) throws SQLException {
        if (tddlConnection instanceof SessionImpl) {
            ((SessionImpl) tddlConnection).setTransactionPolicy(transactionPolicy);
        } else {
            Statement statement = tddlConnection.createStatement();
            int code = 1;
            if (transactionPolicy == ITransactionPolicy.strict) {
                code = 1;
            } else if (transactionPolicy == ITransactionPolicy.strict_write_with_non_transaction_cross_database_read) {
                code = 3;
            } else if (transactionPolicy == ITransactionPolicy.cobar_style) {
                code = 5;
            }
            statement.execute("set transaction policy " + code);
        }
    }
}
