package drds.plus.qatest.group;

import drds.plus.common.GroupDataSourceRouteHelper;
import drds.plus.datanode.api.DatasourceManager;
import drds.plus.qatest.BaseAtomGroupTestCase;
import drds.plus.qatest.util.LoadPropsUtil;
import org.junit.*;

import java.io.File;
import java.util.HashMap;

public class GroupTestCase extends BaseAtomGroupTestCase {


    protected static DatasourceManager tds;

    @BeforeClass
    public static void setUp() throws Exception {
        setGroupMockInfo(GROUP_NORMAL_COMPLEX_PATH, GROUPKEY_COMPLEX);
    }

    @AfterClass
    public static void tearDown() {
        tds = null;

    }

    protected static void setGroupMockInfo(String groupPath, String key) throws Exception {
        // 获取group信息
        String groupStr = LoadPropsUtil.loadProps2OneLine(groupPath, key);
        if (groupStr == null) {
            throw new Exception("指定path = " + groupPath + ",columnName = " + key + "的group信息为null或者为空字符。");
        }

        // 获取atom信息
        dataMap = new HashMap<String, String>();
        String[] atomArr = groupStr.split(",");
        for (String atom : atomArr) {
            atom = atom.trim();
            atom = atom.substring(0, atom.indexOf(":"));
            initAtomConfig(ATOM_PATH + File.separator + atom, APPNAME, atom);
        }

        // 获取groupkey
        //dataMap.add(DatasourceManager.getFullDbGroupKey(columnName), groupStr);

        // 获取JdbcTemplate
        //tddlJT = getJT(columnName);
    }

//    protected static JdbcTemplate getJT(String dbGroupKey) {
//        tds = new DatasourceManager();
//        tds.setApplicationId(APPNAME);
//        tds.setDataNodeId(dbGroupKey);
//        try {
//            tds.init();
//        } catch (RuntimeException e) {
//            Assert.fail();
//        }
//        return new JdbcTemplate(null);
//    }

    @Before
    public void init() throws Exception {
        // 清数据防止干扰
        String sql = "delete from normaltbl_0001 setWhereAndSetNeedBuild pk=?";
        GroupDataSourceRouteHelper.executeByGroupDataSourceIndex(0);
        //clearData(tddlJT, sql_process, new Object[]{RANDOM_ID});
        GroupDataSourceRouteHelper.executeByGroupDataSourceIndex(1);
        //clearData(tddlJT, sql_process, new Object[]{RANDOM_ID});
        GroupDataSourceRouteHelper.executeByGroupDataSourceIndex(2);
        //clearData(tddlJT, sql_process, new Object[]{RANDOM_ID});
    }

    @After
    public void destroy() {
        // 清数据防止干扰
        String sql = "delete from normaltbl_0001 setWhereAndSetNeedBuild pk=?";
        GroupDataSourceRouteHelper.executeByGroupDataSourceIndex(0);
        //clearData(tddlJT, sql_process, new Object[]{RANDOM_ID});
        GroupDataSourceRouteHelper.executeByGroupDataSourceIndex(1);
        //clearData(tddlJT, sql_process, new Object[]{RANDOM_ID});
        GroupDataSourceRouteHelper.executeByGroupDataSourceIndex(2);
        //clearData(tddlJT, sql_process, new Object[]{RANDOM_ID});
    }

    protected void checkWeight(int total, int times, double percent) {
        Assert.assertTrue(1.0 * times / total >= percent - 0.1 && 1.0 * times / total <= percent + 0.1);
    }
}
