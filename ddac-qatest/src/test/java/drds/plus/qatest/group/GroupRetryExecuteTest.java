package drds.plus.qatest.group;

import org.junit.*;

import java.util.concurrent.TimeUnit;

/**
 * Comment for GroupRetryByRealExecuteTest
 * <readPriority/>
 * Created Date: 2010-12-8 03:19:04
 */
@Ignore("暂时屏蔽,手工跑")
public class GroupRetryExecuteTest extends GroupTestCase {

    private String sql = "query * from normaltbl_0000 where pk = 0";

    @Before
    public void init() throws Exception {
        super.setUp();
    }

    @After
    public void destroy() {
        // do nothing
    }

    @Test
    public void oneOfAtomDssInGropuIsOkTest() throws Exception {
        // 改变group中的rw状态(确保推送成功)
        for (int i = 0; i < 2; i++) {
            // MockServer.setConfigInfo(tds.getFullDbGroupKey(),
            // "qatest_normal_0:NA,qatest_normal_0_bac:readWeight,qatest_normal_1_bac:readWeight");
            TimeUnit.SECONDS.sleep(SLEEP_TIME);
        }

        int successCnt = 0;
        for (int i = 0; i < 20; i++) {
            //tddlJT.queryForList(sql_process);
            successCnt++;
        }
        Assert.assertEquals(20, successCnt);

        // qatest_normal_0状态改为只读(确保推送成功)
        for (int i = 0; i < 2; i++) {
            // MockServer.setConfigInfo(ConnectionPropertiesConfiguration.getGlobalDataId(DBKEY_0),
            // "ip=10.232.31.154\readWeight\nport=3306\readWeight\ndbName=qatest_normal_0\readWeight\ndbType=mysql\readWeight\ndbStatus=NA");
            // MockServer.setConfigInfo(tds.getFullDbGroupKey(),
            // "qatest_normal_0:wr,qatest_normal_0_bac:readWeight,qatest_normal_1_bac:readWeight");
            TimeUnit.SECONDS.sleep(SLEEP_TIME);
        }

        successCnt = 0;
        for (int i = 0; i < 20; i++) {
            //tddlJT.queryForList(sql_process);
            successCnt++;
        }
        Assert.assertEquals(20, successCnt);
    }

    @Test
    public void noneOfAtomDssInGropuIsOkTest() throws Exception {
        // 改变group中的rw状态(确保推送成功)
        for (int i = 0; i < 2; i++) {
            // MockServer.setConfigInfo(tds.getFullDbGroupKey(),
            // "qatest_normal_0:NA,qatest_normal_0_bac:NA,qatest_normal_1_bac:NA");
            TimeUnit.SECONDS.sleep(SLEEP_TIME);
        }
        for (int i = 0; i < 20; i++) {
            try {
                //tddlJT.queryForList(sql_process);
                Assert.fail();
            } catch (Exception e) {
                System.out.println("qatest_normal_0's dataBaseStatus=NA");
            }
        }

        // 改变atomDs的状态(确保推送成功)
        for (int i = 0; i < 2; i++) {
//            MockServer.setConfigInfo(ConnectionPropertiesConfiguration.getGlobalDataId(DBKEY_0), "ip=10.232.31.154\readWeight\nport=3306\readWeight\ndbName=qatest_normal_0\readWeight\ndbType=mysql\readWeight\ndbStatus=NA");
//            MockServer.setConfigInfo(ConnectionPropertiesConfiguration.getGlobalDataId(DBKEY_0_BAC), "ip=10.232.31.154\readWeight\nport=3306\readWeight\ndbName=qatest_normal_0_bac\readWeight\ndbType=mysql\readWeight\ndbStatus=NA");
//            MockServer.setConfigInfo(ConnectionPropertiesConfiguration.getGlobalDataId(DBKEY_1_BAC), "ip=10.232.31.154\readWeight\nport=3306\readWeight\ndbName=qatest_normal_1_bac\readWeight\ndbType=mysql\readWeight\ndbStatus=NA");
//            MockServer.setConfigInfo(tds.getFullDbGroupKey(), "qatest_normal_0:wr,qatest_normal_0_bac:readWeight,qatest_normal_1_bac:readWeight");
            TimeUnit.SECONDS.sleep(SLEEP_TIME);
        }
        for (int i = 0; i < 20; i++) {
            try {
                //tddlJT.queryForList(sql_process);
                Assert.fail();
            } catch (Exception e) {
                System.out.println("qatest_normal_0's dataBaseStatus=NA");
            }
        }
    }
}
