package drds.plus.qatest;

import drds.plus.qatest.matrix.basecrud.RandomStringUtils;
import drds.plus.qatest.util.DateUtil;
import drds.plus.qatest.util.Validator;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;

import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.Properties;
import java.util.Random;

/**
 * 基本测试类，提供创建AndorServer和AndorClient方法
 * <readPriority/>
 */
@Ignore(value = "提供初始化环境的实际方法")
public class BaseTestCase extends Validator {

    // --------------公用变量
    protected static final long RANDOM_ID = Long.valueOf(RandomStringUtils.randomNumeric(8));
    protected static final Properties properties = new Properties();
    // --------------公用表名
    protected static String normaltblTableName;
    protected static String studentTableName;
    protected static String host_info;
    protected static String hostgroup;
    protected static String hostgroup_info;
    protected static String module_info;
    protected static String module_host;
    protected final int MAX_DATA_SIZE = 20;
    protected final int RANDOM_INT = Integer.valueOf(RandomStringUtils.randomNumeric(8) + "");
    protected final float fl = 0.01f;
    protected final String name = "zhuoxue";
    protected final String name1 = "zhuoxue_yll";
    protected final String name2 = "zhuoxue%yll";
    protected final String newName = "zhuoxue.yll";
    protected final String school = "taobao";
    protected final Date gmtDay = new Date(1350230400000l);
    protected final Date gmtDayBefore = new Date(1150300800000l);
    protected final Date gmtDayNext = new Date(1550246400000l);
    protected final Date gmt = new Date(1350304585380l);
    protected final Date gmtNext = new Date(1550304585380l);
    protected final Date gmtBefore = new Date(1150304585380l);
    protected final NumberFormat nf = new DecimalFormat("#.#");
    protected Random rand = new Random();
    // datasource为static，一个测试类只启动一次
    // protected static SessionManager us;
    protected ResultSet rc = null;
    protected ResultSet rs = null;
    protected String timeString = DateUtil.formatDate(new Date(0), DateUtil.DATE_FULLHYPHEN);

    @BeforeClass
    public static void diamondSetUp() {

    }

    @AfterClass
    public static void diamondTearDown() {

        // DataSource.cleanAllDataSource();
    }

}
