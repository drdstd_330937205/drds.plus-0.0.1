package drds.plus.executor.cursor.test;

import drds.plus.executor.cursor.cursor.ISortingCursor;
import drds.plus.executor.cursor.cursor.impl.SortingCursor;
import drds.plus.executor.cursor.cursor.impl.merge.MergeCursor;
import drds.plus.executor.row_values.RowValues;
import drds.plus.sql_process.abstract_syntax_tree.ObjectCreateFactory;
import drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.MergeQuery;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.column.Column;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.column.ColumnImpl;
import drds.plus.sql_process.abstract_syntax_tree.expression.order_by.OrderBy;
import drds.plus.sql_process.abstract_syntax_tree.expression.order_by.OrderByImpl;
import drds.plus.sql_process.type.Type;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class MergeCursorTest {

    MockArrayCursor getCursor(String tableName, Integer[] ids) throws RuntimeException {
        MockArrayCursor cursor = new MockArrayCursor(tableName);
        cursor.addColumn("id", Type.IntegerType);
        cursor.addColumn("columnName", Type.StringType);
        cursor.addColumn("school", Type.StringType);
        cursor.initMeta();
        for (Integer id : ids) {
            cursor.addRow(new Object[]{id, "columnName" + id, "school" + id});
        }

        cursor.init();
        return cursor;
    }

    @Test
    public void testMergeNormal() throws RuntimeException {
        MockArrayCursor mockCursor1 = this.getCursor("T1", new Integer[]{1, 3, 5, 8, 8, 9, 10});
        MockArrayCursor mockCursor2 = this.getCursor("T1", new Integer[]{2, 2, 4, 5, 6, 7, 7, 9, 9, 10, 13});
        OrderBy order = new OrderByImpl();
        Column column = new ColumnImpl();
        column.setColumnName("ID");
        column.setTableName("T1");
        column.setType(Type.IntegerType);
        order.setColumn(column);
        List<OrderBy> orderBys = new ArrayList();

        orderBys.add(order);

        List<ISortingCursor> cursors = new ArrayList();
        cursors.add(new SortingCursor(mockCursor1, orderBys));
        cursors.add(new SortingCursor(mockCursor2, orderBys));

        MergeQuery mergeQuery = ObjectCreateFactory.createMergeQuery();
        mergeQuery.setSharded(true);
        MergeCursor mergeCursor = new MergeCursor(null, mergeQuery, cursors);
        Object[] expected = new Object[]{1, 3, 5, 8, 8, 9, 10, 2, 2, 4, 5, 6, 7, 7, 9, 9, 10, 13};
        List actual = new ArrayList();

        RowValues row = null;
        while ((row = mergeCursor.next()) != null) {
            System.out.println(row);
            actual.add(row.getObject(0));
        }
        mergeCursor.close(new ArrayList());
        Assert.assertArrayEquals(expected, actual.toArray());
        Assert.assertTrue(mockCursor1.isClosed());
    }

    @Test
    public void testGetOrderBysBeforeNext() throws RuntimeException {
        MockArrayCursor mockCursor1 = this.getCursor("T1", new Integer[]{1, 3, 5, 8, 8, 9, 10});
        MockArrayCursor mockCursor2 = this.getCursor("T1", new Integer[]{2, 2, 4, 5, 6, 7, 7, 9, 9, 10, 13});
        OrderBy order = new OrderByImpl();
        Column column = new ColumnImpl();
        column.setColumnName("ID");
        column.setTableName("T1");
        column.setType(Type.IntegerType);
        order.setColumn(column);
        List<OrderBy> orderBys = new ArrayList();

        orderBys.add(order);

        List<ISortingCursor> cursors = new ArrayList();
        cursors.add(new SortingCursor(mockCursor1, orderBys));
        cursors.add(new SortingCursor(mockCursor2, orderBys));
        MergeQuery mergeQuery = ObjectCreateFactory.createMergeQuery();
        mergeQuery.setSharded(true);
        MergeCursor c = new MergeCursor(null, mergeQuery, cursors);

        Assert.assertEquals("[T1.ID, T1.NAME, T1.SCHOOL]", c.getColumnMetaDataList().toString());
        Assert.assertEquals("[OrderByImpl [columnName=T1.ID, direction=true]]", c.getOrderByList().toString());

    }

    @Test
    public void testGetOrderBysAfterNext() throws RuntimeException {
        MockArrayCursor mockCursor1 = this.getCursor("T1", new Integer[]{1, 3, 5, 8, 8, 9, 10});
        MockArrayCursor mockCursor2 = this.getCursor("T1", new Integer[]{2, 2, 4, 5, 6, 7, 7, 9, 9, 10, 13});
        OrderBy order = new OrderByImpl();
        Column column = new ColumnImpl();
        column.setColumnName("ID");
        column.setTableName("T1");
        column.setType(Type.IntegerType);
        order.setColumn(column);
        List<OrderBy> orderBys = new ArrayList();

        orderBys.add(order);

        List<ISortingCursor> cursors = new ArrayList();
        cursors.add(new SortingCursor(mockCursor1, orderBys));
        cursors.add(new SortingCursor(mockCursor2, orderBys));
        MergeQuery mergeQuery = ObjectCreateFactory.createMergeQuery();
        mergeQuery.setSharded(true);
        MergeCursor c = new MergeCursor(null, mergeQuery, cursors);

        c.next();

        Assert.assertEquals("[T1.ID, T1.NAME, T1.SCHOOL]", c.getColumnMetaDataList().toString());
        Assert.assertEquals("[OrderByImpl [columnName=T1.ID, direction=true]]", c.getOrderByList().toString());
    }

}
