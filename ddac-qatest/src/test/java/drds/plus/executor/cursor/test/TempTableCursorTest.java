package drds.plus.executor.cursor.test;

import drds.plus.executor.ExecuteContext;
import drds.plus.executor.cursor.cursor.impl.SortingCursor;
import drds.plus.executor.cursor.cursor.impl.sort.TemporaryTableCursor;
import drds.plus.executor.repository.Repository;
import drds.plus.executor.repository.RepositoryNameToRepositoryMap;
import drds.plus.executor.row_values.RowValues;
import drds.plus.sql_process.abstract_syntax_tree.configuration.ColumnMetaData;
import drds.plus.sql_process.abstract_syntax_tree.configuration.manager.StaticSchemaManager;
import drds.plus.sql_process.type.Type;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class TempTableCursorTest {

    static ExecuteContext ec = new ExecuteContext();

    MockArrayCursor getCursor(String tableName, Integer[] ids) throws RuntimeException {
        MockArrayCursor cursor = new MockArrayCursor(tableName);
        cursor.addColumn("id", Type.IntegerType);
        cursor.addColumn("columnName", Type.StringType);
        cursor.addColumn("school", Type.StringType);
        cursor.initMeta();

        for (Integer id : ids) {
            cursor.addRow(new Object[]{id, "columnName" + id, "school" + id});

        }

        cursor.init();

        return cursor;

    }

    @Test
    public void testWithOutColumns() throws RuntimeException {
        RepositoryNameToRepositoryMap repoHolder = new RepositoryNameToRepositoryMap();
        StaticSchemaManager sm = new StaticSchemaManager("test_schema.xml");
        sm.init();
        Repository bdbRepo = repoHolder.getRepository("bdb", Collections.EMPTY_MAP, Collections.EMPTY_MAP);
        MockArrayCursor mockCursor = this.getCursor("T1", new Integer[]{5, 5, 4, 3, 2, 1, 1});
        SortingCursor subCursor = new SortingCursor(mockCursor);

        TemporaryTableCursor c = new TemporaryTableCursor(ec, bdbRepo, 1, subCursor, true);
        Object[] expected = new Object[]{1, 1, 2, 3, 4, 5, 5};
        List actual = new ArrayList();

        RowValues row = null;
        while ((row = c.next()) != null) {

            System.out.println(row);
            actual.add(row.getObject(0));
        }

        Assert.assertArrayEquals(expected, actual.toArray());
        Assert.assertTrue(mockCursor.isClosed());
    }

    @Test
    public void testWithColumns() throws RuntimeException {
        RepositoryNameToRepositoryMap repoHolder = new RepositoryNameToRepositoryMap();
        StaticSchemaManager sm = new StaticSchemaManager("test_schema.xml");
        sm.init();
        Repository bdbRepo = repoHolder.getRepository("bdb", Collections.EMPTY_MAP, Collections.EMPTY_MAP);
        MockArrayCursor mockCursor = this.getCursor("T1", new Integer[]{5, 5, 4, 3, 2, 1});
        SortingCursor subCursor = new SortingCursor(mockCursor);

        ColumnMetaData cm = new ColumnMetaData("T1", "NAME", null, Type.IntegerType, true);
        TemporaryTableCursor c = new TemporaryTableCursor(ec, bdbRepo, 2, subCursor, true, Arrays.asList(cm));
        Object[] expected = new Object[]{"name1", "name2", "name3", "name4", "name5", "name5"};
        List actual = new ArrayList();

        RowValues row = null;
        while ((row = c.next()) != null) {

            System.out.println(row);
            actual.add(row.getObject(0));
        }

        Assert.assertArrayEquals(expected, actual.toArray());
        Assert.assertTrue(mockCursor.isClosed());
    }

    @Test
    public void testWithOutColumnsBeforeFirst() throws RuntimeException {
        RepositoryNameToRepositoryMap repoHolder = new RepositoryNameToRepositoryMap();
        StaticSchemaManager sm = new StaticSchemaManager("test_schema.xml");
        sm.init();
        Repository bdbRepo = repoHolder.getRepository("bdb", Collections.EMPTY_MAP, Collections.EMPTY_MAP);
        MockArrayCursor mockCursor = this.getCursor("T1", new Integer[]{5, 5, 4, 3, 2, 1});
        SortingCursor subCursor = new SortingCursor(mockCursor);

        TemporaryTableCursor c = new TemporaryTableCursor(ec, bdbRepo, 3, subCursor, true);
        Object[] expected = new Object[]{1, 2, 3, 4, 5, 5, 1, 2, 3, 4, 5, 5};
        List actual = new ArrayList();

        RowValues row = null;
        while ((row = c.next()) != null) {

            System.out.println(row);
            actual.add(row.getObject(0));
        }

        c.beforeFirst();
        while ((row = c.next()) != null) {

            System.out.println(row);
            actual.add(row.getObject(0));
        }
        Assert.assertArrayEquals(expected, actual.toArray());
        Assert.assertTrue(mockCursor.isClosed());
    }

}
