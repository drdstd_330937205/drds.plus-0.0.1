package drds.plus.executor.cursor.test;

import drds.plus.executor.cursor.cursor.ISortingCursor;
import drds.plus.executor.cursor.cursor.impl.SortingCursor;
import drds.plus.executor.cursor.cursor.impl.join.SortMergeJoinCursor;
import drds.plus.executor.row_values.RowValues;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.column.ColumnImpl;
import drds.plus.sql_process.type.Type;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class SortMergeCursorTest {

    MockArrayCursor getCursor(String tableName, Integer[] ids) throws RuntimeException {
        MockArrayCursor cursor = new MockArrayCursor(tableName);
        cursor.addColumn("id", Type.IntegerType);
        cursor.addColumn("columnName", Type.StringType);
        cursor.addColumn("school", Type.StringType);
        cursor.initMeta();

        for (Integer id : ids) {
            cursor.addRow(new Object[]{id, "columnName" + id, "school" + id});

        }

        cursor.init();

        return cursor;

    }

    @Test
    public void testInnerJoin() throws RuntimeException {

        ISortingCursor left_cursor = new SortingCursor(this.getCursor("T1", new Integer[]{1, 1, 1, 2, 3, 4, 5, 6, 7}));
        ISortingCursor right_cursor = new SortingCursor(this.getCursor("T2", new Integer[]{1, 1, 2, 2, 4, 5, 6, 7}));

        List leftJoinOnColumns = new ArrayList();

        leftJoinOnColumns.add(new ColumnImpl());//.setColumnName("ID").setType(Type.IntegerType).setTableName("T1"));

        List rightJoinOnColumns = new ArrayList();

        rightJoinOnColumns.add(new ColumnImpl());//.setColumnName("ID").setType(Type.IntegerType).setTableName("T2"));

        SortMergeJoinCursor c = new SortMergeJoinCursor(left_cursor, right_cursor, leftJoinOnColumns, rightJoinOnColumns);

        Object[] expected = new Object[]{1, 1, 1, 1, 1, 1, 2, 2, 4, 5, 6, 7};
        List actual = new ArrayList();
        RowValues row = null;
        while ((row = c.next()) != null) {
            actual.add(row.getObject(0));
            System.out.println(row);
        }

        Assert.assertArrayEquals(expected, actual.toArray());
    }

    @Test
    public void testInnerJoinWithRightEmpty() throws RuntimeException {

        ISortingCursor left_cursor = new SortingCursor(this.getCursor("T1", new Integer[]{1, 1, 1, 2, 3, 4, 5, 6, 7}));
        ISortingCursor right_cursor = new SortingCursor(this.getCursor("T2", new Integer[]{}));

        List leftJoinOnColumns = new ArrayList();

        leftJoinOnColumns.add(new ColumnImpl());//.setColumnName("ID").setType(Type.IntegerType).setTableName("T1"));

        List rightJoinOnColumns = new ArrayList();

        rightJoinOnColumns.add(new ColumnImpl());//.setColumnName("ID").setType(Type.IntegerType).setTableName("T2"));

        SortMergeJoinCursor c = new SortMergeJoinCursor(left_cursor, right_cursor, leftJoinOnColumns, rightJoinOnColumns);

        Object[] expected = new Object[]{};
        List actual = new ArrayList();
        RowValues row = null;
        while ((row = c.next()) != null) {
            actual.add(row.getObject(0));
            System.out.println(row);
        }

        Assert.assertArrayEquals(expected, actual.toArray());
    }

    @Test
    public void testInnerJoinWithBothEmpty() throws RuntimeException {

        ISortingCursor left_cursor = new SortingCursor(this.getCursor("T1", new Integer[]{}));
        ISortingCursor right_cursor = new SortingCursor(this.getCursor("T2", new Integer[]{}));

        List leftJoinOnColumns = new ArrayList();

        leftJoinOnColumns.add(new ColumnImpl());//.setColumnName("ID").setType(Type.IntegerType).setTableName("T1"));

        List rightJoinOnColumns = new ArrayList();

        rightJoinOnColumns.add(new ColumnImpl());//.setColumnName("ID").setType(Type.IntegerType).setTableName("T2"));

        SortMergeJoinCursor c = new SortMergeJoinCursor(left_cursor, right_cursor, leftJoinOnColumns, rightJoinOnColumns);

        Object[] expected = new Object[]{};
        List actual = new ArrayList();
        RowValues row = null;
        while ((row = c.next()) != null) {
            actual.add(row.getObject(0));
            System.out.println(row);
        }

        Assert.assertArrayEquals(expected, actual.toArray());
    }

    @Test
    public void testInnerJoinWithTwoJoinOnColumns() throws RuntimeException {

        ISortingCursor left_cursor = new SortingCursor(this.getCursor("T1", new Integer[]{1, 1, 1, 2, 3, 4, 5, 6, 7}));
        ISortingCursor right_cursor = new SortingCursor(this.getCursor("T2", new Integer[]{1, 1, 2, 2, 4, 5, 6, 7}));

        List leftJoinOnColumns = new ArrayList();

        leftJoinOnColumns.add(new ColumnImpl());//.setColumnName("ID").setType(Type.IntegerType).setTableName("T1"));
        leftJoinOnColumns.add(new ColumnImpl());//.setColumnName("NAME").setType(Type.StringType).setTableName("T1"));

        List rightJoinOnColumns = new ArrayList();

        rightJoinOnColumns.add(new ColumnImpl());//.setColumnName("ID").setType(Type.IntegerType).setTableName("T2"));
        rightJoinOnColumns.add(new ColumnImpl());//.setColumnName("NAME").setType(Type.StringType).setTableName("T2"));

        SortMergeJoinCursor c = new SortMergeJoinCursor(left_cursor, right_cursor, leftJoinOnColumns, rightJoinOnColumns);

        Object[] expected = new Object[]{1, 1, 1, 1, 1, 1, 2, 2, 4, 5, 6, 7};
        List actual = new ArrayList();
        RowValues row = null;
        while ((row = c.next()) != null) {
            actual.add(row.getObject(0));
            System.out.println(row);
        }

        Assert.assertArrayEquals(expected, actual.toArray());
    }

    @Test
    public void testLeftJoin() throws RuntimeException {

        ISortingCursor left_cursor = new SortingCursor(this.getCursor("T1", new Integer[]{2, 3, 4, 5}));
        ISortingCursor right_cursor = new SortingCursor(this.getCursor("T2", new Integer[]{2, 4, 5}));

        List leftJoinOnColumns = new ArrayList();

        leftJoinOnColumns.add(new ColumnImpl());//.setColumnName("ID").setType(Type.IntegerType).setTableName("T1"));

        List rightJoinOnColumns = new ArrayList();

        rightJoinOnColumns.add(new ColumnImpl());//.setColumnName("ID").setType(Type.IntegerType).setTableName("T2"));

        SortMergeJoinCursor c = new SortMergeJoinCursor(left_cursor, right_cursor, leftJoinOnColumns, rightJoinOnColumns);

        c.setLeftJoin(true);

        Object[] leftExpected = new Object[]{2, 3, 4, 5};
        Object[] rightExpected = new Object[]{2, null, 4, 5};
        List leftActual = new ArrayList();
        List rightActual = new ArrayList();
        RowValues row = null;
        while ((row = c.next()) != null) {
            leftActual.add(row.getObject(0));
            rightActual.add(row.getObject(3));
            System.out.println(row);
        }

        Assert.assertArrayEquals(leftExpected, leftActual.toArray());
        Assert.assertArrayEquals(rightExpected, rightActual.toArray());
    }

    @Test
    public void testLeftJoin2() throws RuntimeException {

        ISortingCursor left_cursor = new SortingCursor(this.getCursor("T1", new Integer[]{2, 4, 5}));
        ISortingCursor right_cursor = new SortingCursor(this.getCursor("T2", new Integer[]{2, 3, 4, 5}));

        List leftJoinOnColumns = new ArrayList();

        leftJoinOnColumns.add(new ColumnImpl());//.setColumnName("ID").setType(Type.IntegerType).setTableName("T1"));

        List rightJoinOnColumns = new ArrayList();

        rightJoinOnColumns.add(new ColumnImpl());//.setColumnName("ID").setType(Type.IntegerType).setTableName("T2"));

        SortMergeJoinCursor c = new SortMergeJoinCursor(left_cursor, right_cursor, leftJoinOnColumns, rightJoinOnColumns);

        c.setLeftJoin(true);

        Object[] leftExpected = new Object[]{2, 4, 5};
        Object[] rightExpected = new Object[]{2, 4, 5};
        List leftActual = new ArrayList();
        List rightActual = new ArrayList();
        RowValues row = null;
        while ((row = c.next()) != null) {
            leftActual.add(row.getObject(0));
            rightActual.add(row.getObject(3));
            System.out.println(row);
        }

        Assert.assertArrayEquals(leftExpected, leftActual.toArray());
        Assert.assertArrayEquals(rightExpected, rightActual.toArray());
    }

    @Test
    public void testRightJoin() throws RuntimeException {

        ISortingCursor left_cursor = new SortingCursor(this.getCursor("T1", new Integer[]{2, 3, 4, 5}));
        ISortingCursor right_cursor = new SortingCursor(this.getCursor("T2", new Integer[]{2, 4, 5}));

        List leftJoinOnColumns = new ArrayList();

        leftJoinOnColumns.add(new ColumnImpl());//.setColumnName("ID").setType(Type.IntegerType).setTableName("T1"));

        List rightJoinOnColumns = new ArrayList();

        rightJoinOnColumns.add(new ColumnImpl());//.setColumnName("ID").setType(Type.IntegerType).setTableName("T2"));

        SortMergeJoinCursor c = new SortMergeJoinCursor(left_cursor, right_cursor, leftJoinOnColumns, rightJoinOnColumns);

        c.setRightJoin(true);

        Object[] leftExpected = new Object[]{2, 4, 5};
        Object[] rightExpected = new Object[]{2, 4, 5};
        List leftActual = new ArrayList();
        List rightActual = new ArrayList();
        RowValues row = null;
        while ((row = c.next()) != null) {
            leftActual.add(row.getObject(0));
            rightActual.add(row.getObject(3));
            System.out.println(row);
        }

        Assert.assertArrayEquals(leftExpected, leftActual.toArray());
        Assert.assertArrayEquals(rightExpected, rightActual.toArray());
    }

    @Test
    public void testRightJoin2() throws RuntimeException {

        ISortingCursor left_cursor = new SortingCursor(this.getCursor("T1", new Integer[]{2, 4, 5}));
        ISortingCursor right_cursor = new SortingCursor(this.getCursor("T2", new Integer[]{2, 3, 4, 5}));

        List leftJoinOnColumns = new ArrayList();

        leftJoinOnColumns.add(new ColumnImpl());//.setColumnName("ID").setType(Type.IntegerType).setTableName("T1"));

        List rightJoinOnColumns = new ArrayList();

        rightJoinOnColumns.add(new ColumnImpl());//.setColumnName("ID").setType(Type.IntegerType).setTableName("T2"));

        SortMergeJoinCursor c = new SortMergeJoinCursor(left_cursor, right_cursor, leftJoinOnColumns, rightJoinOnColumns);

        c.setRightJoin(true);

        Object[] leftExpected = new Object[]{2, null, 4, 5};
        Object[] rightExpected = new Object[]{2, 3, 4, 5};
        List leftActual = new ArrayList();
        List rightActual = new ArrayList();
        RowValues row = null;
        while ((row = c.next()) != null) {
            leftActual.add(row.getObject(0));
            rightActual.add(row.getObject(3));
            System.out.println(row);
        }

        Assert.assertArrayEquals(leftExpected, leftActual.toArray());
        Assert.assertArrayEquals(rightExpected, rightActual.toArray());
    }

    @Test
    public void testFullOutterJoin() throws RuntimeException {

        ISortingCursor left_cursor = new SortingCursor(this.getCursor("T1", new Integer[]{2, 3, 4, 5}));
        ISortingCursor right_cursor = new SortingCursor(this.getCursor("T2", new Integer[]{2, 4, 5}));

        List leftJoinOnColumns = new ArrayList();

        leftJoinOnColumns.add(new ColumnImpl());//.setColumnName("ID").setType(Type.IntegerType).setTableName("T1"));

        List rightJoinOnColumns = new ArrayList();

        rightJoinOnColumns.add(new ColumnImpl());//.setColumnName("ID").setType(Type.IntegerType).setTableName("T2"));

        SortMergeJoinCursor c = new SortMergeJoinCursor(left_cursor, right_cursor, leftJoinOnColumns, rightJoinOnColumns);

        c.setRightJoin(true);
        c.setLeftJoin(true);

        Object[] leftExpected = new Object[]{2, 3, 4, 5};
        Object[] rightExpected = new Object[]{2, null, 4, 5};
        List leftActual = new ArrayList();
        List rightActual = new ArrayList();
        RowValues row = null;
        while ((row = c.next()) != null) {
            leftActual.add(row.getObject(0));
            rightActual.add(row.getObject(3));
            System.out.println(row);
        }

        Assert.assertArrayEquals(leftExpected, leftActual.toArray());
        Assert.assertArrayEquals(rightExpected, rightActual.toArray());
    }

    @Test
    public void testFullOutterJoin2() throws RuntimeException {

        ISortingCursor left_cursor = new SortingCursor(this.getCursor("T1", new Integer[]{2, 4, 5}));
        ISortingCursor right_cursor = new SortingCursor(this.getCursor("T2", new Integer[]{2, 3, 4, 5}));

        List leftJoinOnColumns = new ArrayList();

        leftJoinOnColumns.add(new ColumnImpl());//.setColumnName("ID").setType(Type.IntegerType).setTableName("T1"));

        List rightJoinOnColumns = new ArrayList();

        rightJoinOnColumns.add(new ColumnImpl());//.setColumnName("ID").setType(Type.IntegerType).setTableName("T2"));

        SortMergeJoinCursor c = new SortMergeJoinCursor(left_cursor, right_cursor, leftJoinOnColumns, rightJoinOnColumns);

        c.setRightJoin(true);
        c.setLeftJoin(true);
        Object[] leftExpected = new Object[]{2, null, 4, 5};
        Object[] rightExpected = new Object[]{2, 3, 4, 5};
        List leftActual = new ArrayList();
        List rightActual = new ArrayList();
        RowValues row = null;
        while ((row = c.next()) != null) {
            leftActual.add(row.getObject(0));
            rightActual.add(row.getObject(3));
            System.out.println(row);
        }

        Assert.assertArrayEquals(leftExpected, leftActual.toArray());
        Assert.assertArrayEquals(rightExpected, rightActual.toArray());
    }

    @Test
    public void testFullOutterJoin3() throws RuntimeException {

        ISortingCursor left_cursor = new SortingCursor(this.getCursor("T1", new Integer[]{2, 4, 5, 6}));
        ISortingCursor right_cursor = new SortingCursor(this.getCursor("T2", new Integer[]{2, 3, 4, 5}));

        List leftJoinOnColumns = new ArrayList();

        leftJoinOnColumns.add(new ColumnImpl());//.setColumnName("ID").setType(Type.IntegerType).setTableName("T1"));

        List rightJoinOnColumns = new ArrayList();

        rightJoinOnColumns.add(new ColumnImpl());//.setColumnName("ID").setType(Type.IntegerType).setTableName("T2"));

        SortMergeJoinCursor c = new SortMergeJoinCursor(left_cursor, right_cursor, leftJoinOnColumns, rightJoinOnColumns);

        c.setRightJoin(true);
        c.setLeftJoin(true);
        Object[] leftExpected = new Object[]{2, null, 4, 5, 6};
        Object[] rightExpected = new Object[]{2, 3, 4, 5, null};
        List leftActual = new ArrayList();
        List rightActual = new ArrayList();
        RowValues row = null;
        while ((row = c.next()) != null) {
            leftActual.add(row.getObject(0));
            rightActual.add(row.getObject(3));
            System.out.println(row);
        }

        Assert.assertArrayEquals(leftExpected, leftActual.toArray());
        Assert.assertArrayEquals(rightExpected, rightActual.toArray());
    }

    @Test
    public void testFullOutterJoin4() throws RuntimeException {

        ISortingCursor left_cursor = new SortingCursor(this.getCursor("T1", new Integer[]{2, 4, 5, 6}));
        ISortingCursor right_cursor = new SortingCursor(this.getCursor("T2", new Integer[]{2, 3, 4, 5, 9}));

        List leftJoinOnColumns = new ArrayList();

        leftJoinOnColumns.add(new ColumnImpl());//.setColumnName("ID").setType(Type.IntegerType).setTableName("T1"));

        List rightJoinOnColumns = new ArrayList();

        rightJoinOnColumns.add(new ColumnImpl());//.setColumnName("ID").setType(Type.IntegerType).setTableName("T2"));

        SortMergeJoinCursor c = new SortMergeJoinCursor(left_cursor, right_cursor, leftJoinOnColumns, rightJoinOnColumns);

        c.setRightJoin(true);
        c.setLeftJoin(true);
        Object[] leftExpected = new Object[]{2, null, 4, 5, 6, null};
        Object[] rightExpected = new Object[]{2, 3, 4, 5, null, 9};
        List leftActual = new ArrayList();
        List rightActual = new ArrayList();
        RowValues row = null;
        while ((row = c.next()) != null) {
            leftActual.add(row.getObject(0));
            rightActual.add(row.getObject(3));
            System.out.println(row);
        }

        Assert.assertArrayEquals(leftExpected, leftActual.toArray());
        Assert.assertArrayEquals(rightExpected, rightActual.toArray());
    }

    @Test
    public void testFullOutterJoin5() throws RuntimeException {

        ISortingCursor left_cursor = new SortingCursor(this.getCursor("T1", new Integer[]{1, 1, 1, 2, 4, 5, 6, 6, 6}));
        ISortingCursor right_cursor = new SortingCursor(this.getCursor("T2", new Integer[]{}));

        List leftJoinOnColumns = new ArrayList();

        leftJoinOnColumns.add(new ColumnImpl());//.setColumnName("ID").setType(Type.IntegerType).setTableName("T1"));

        List rightJoinOnColumns = new ArrayList();

        rightJoinOnColumns.add(new ColumnImpl());//.setColumnName("ID").setType(Type.IntegerType).setTableName("T2"));

        SortMergeJoinCursor c = new SortMergeJoinCursor(left_cursor, right_cursor, leftJoinOnColumns, rightJoinOnColumns);

        c.setRightJoin(true);
        c.setLeftJoin(true);
        Object[] leftExpected = new Object[]{1, 1, 1, 2, 4, 5, 6, 6, 6};
        Object[] rightExpected = new Object[]{null, null, null, null, null, null, null, null, null};
        List leftActual = new ArrayList();
        List rightActual = new ArrayList();
        RowValues row = null;
        while ((row = c.next()) != null) {
            leftActual.add(row.getObject(0));
            rightActual.add(row.getObject(3));
            System.out.println(row);
        }

        Assert.assertArrayEquals(leftExpected, leftActual.toArray());
        Assert.assertArrayEquals(rightExpected, rightActual.toArray());
    }

    @Test
    public void testFullOutterJoin6() throws RuntimeException {

        ISortingCursor left_cursor = new SortingCursor(this.getCursor("T1", new Integer[]{}));
        ISortingCursor right_cursor = new SortingCursor(this.getCursor("T2", new Integer[]{}));

        List leftJoinOnColumns = new ArrayList();

        leftJoinOnColumns.add(new ColumnImpl());//.setColumnName("ID").setType(Type.IntegerType).setTableName("T1"));

        List rightJoinOnColumns = new ArrayList();

        rightJoinOnColumns.add(new ColumnImpl());//.setColumnName("ID").setType(Type.IntegerType).setTableName("T2"));

        SortMergeJoinCursor c = new SortMergeJoinCursor(left_cursor, right_cursor, leftJoinOnColumns, rightJoinOnColumns);

        c.setRightJoin(true);
        c.setLeftJoin(true);
        Object[] leftExpected = new Object[]{};
        Object[] rightExpected = new Object[]{};
        List leftActual = new ArrayList();
        List rightActual = new ArrayList();
        RowValues row = null;
        while ((row = c.next()) != null) {
            leftActual.add(row.getObject(0));
            rightActual.add(row.getObject(3));
            System.out.println(row);
        }

        Assert.assertArrayEquals(leftExpected, leftActual.toArray());
        Assert.assertArrayEquals(rightExpected, rightActual.toArray());
    }

    @Test
    public void testClose() throws RuntimeException {

        MockArrayCursor leftMockCursor = this.getCursor("T1", new Integer[]{2, 4, 5, 6});
        MockArrayCursor rightMockCursor = this.getCursor("T2", new Integer[]{2, 3, 4, 5, 9});
        ISortingCursor left_cursor = new SortingCursor(leftMockCursor);
        ISortingCursor right_cursor = new SortingCursor(rightMockCursor);

        List leftJoinOnColumns = new ArrayList();

        leftJoinOnColumns.add(new ColumnImpl());//.setColumnName("ID").setType(Type.IntegerType).setTableName("T1"));

        List rightJoinOnColumns = new ArrayList();

        rightJoinOnColumns.add(new ColumnImpl());//.setColumnName("ID").setType(Type.IntegerType).setTableName("T2"));

        SortMergeJoinCursor c = new SortMergeJoinCursor(left_cursor, right_cursor, leftJoinOnColumns, rightJoinOnColumns);

        c.setRightJoin(true);
        c.setLeftJoin(true);
        // Object[] leftExpected = new Object[] { 2, null, 4, 5, 6, null };
        // Object[] rightExpected = new Object[] { 2, 3, 4, 5, null, 9 };
        List leftActual = new ArrayList();
        List rightActual = new ArrayList();
        RowValues row = null;
        while ((row = c.next()) != null) {
            leftActual.add(row.getObject(0));
            rightActual.add(row.getObject(3));
            System.out.println(row);
        }

        c.close(new ArrayList());

        Assert.assertTrue(leftMockCursor.isClosed());

        Assert.assertTrue(rightMockCursor.isClosed());
    }

    @Test
    public void testGetReturnColumns() throws RuntimeException {

        MockArrayCursor leftMockCursor = this.getCursor("T1", new Integer[]{2, 4, 5, 6});
        MockArrayCursor rightMockCursor = this.getCursor("T2", new Integer[]{2, 3, 4, 5, 9});
        ISortingCursor left_cursor = new SortingCursor(leftMockCursor);
        ISortingCursor right_cursor = new SortingCursor(rightMockCursor);

        List leftJoinOnColumns = new ArrayList();

        leftJoinOnColumns.add(new ColumnImpl());//.setColumnName("ID").setType(Type.IntegerType).setTableName("T1"));

        List rightJoinOnColumns = new ArrayList();

        rightJoinOnColumns.add(new ColumnImpl());//.setColumnName("ID").setType(Type.IntegerType).setTableName("T2"));

        SortMergeJoinCursor c = new SortMergeJoinCursor(left_cursor, right_cursor, leftJoinOnColumns, rightJoinOnColumns);

        c.setRightJoin(true);
        c.setLeftJoin(true);
        // Object[] leftExpected = new Object[] { 2, null, 4, 5, 6, null };
        // Object[] rightExpected = new Object[] { 2, 3, 4, 5, null, 9 };
        List leftActual = new ArrayList();
        List rightActual = new ArrayList();
        RowValues row = null;
        while ((row = c.next()) != null) {
            leftActual.add(row.getObject(0));
            rightActual.add(row.getObject(3));
            System.out.println(row);
        }

        Assert.assertEquals("[T1.ID, T1.NAME, T1.SCHOOL, T2.ID, T2.NAME, T2.SCHOOL]", c.getColumnMetaDataList().toString());

    }

}
