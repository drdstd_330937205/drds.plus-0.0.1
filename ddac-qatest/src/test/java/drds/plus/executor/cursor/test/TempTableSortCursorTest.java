package drds.plus.executor.cursor.test;

import drds.plus.executor.ExecuteContext;
import drds.plus.executor.cursor.cursor.impl.SortingCursor;
import drds.plus.executor.cursor.cursor.impl.sort.TemporaryTableSortCursor;
import drds.plus.executor.cursor.cursor_factory.CursorFactory;
import drds.plus.executor.cursor.cursor_factory.CursorFactoryImpl;
import drds.plus.executor.repository.Repository;
import drds.plus.executor.repository.RepositoryNameToRepositoryMap;
import drds.plus.executor.row_values.RowValues;
import drds.plus.sql_process.abstract_syntax_tree.configuration.manager.StaticSchemaManager;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.column.Column;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.column.ColumnImpl;
import drds.plus.sql_process.abstract_syntax_tree.expression.order_by.OrderBy;
import drds.plus.sql_process.abstract_syntax_tree.expression.order_by.OrderByImpl;
import drds.plus.sql_process.type.Type;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TempTableSortCursorTest {

    MockArrayCursor getCursor(String tableName, Integer[] ids) throws RuntimeException {
        MockArrayCursor cursor = new MockArrayCursor(tableName);
        cursor.addColumn("id", Type.IntegerType);
        cursor.addColumn("columnName", Type.StringType);
        cursor.addColumn("school", Type.StringType);
        cursor.initMeta();

        for (Integer id : ids) {
            cursor.addRow(new Object[]{id, "columnName" + id, "school" + id});

        }

        cursor.init();

        return cursor;

    }

    @Test
    public void testSort() throws RuntimeException {
        RepositoryNameToRepositoryMap repoHolder = new RepositoryNameToRepositoryMap();
        StaticSchemaManager sm = new StaticSchemaManager("test_schema.xml");
        sm.init();
        Repository bdbRepo = repoHolder.getRepository("bdb", Collections.EMPTY_MAP, Collections.EMPTY_MAP);
        CursorFactory cf = new CursorFactoryImpl();
        MockArrayCursor mockCursor = this.getCursor("T1", new Integer[]{5, 5, 4, 3, 2, 1});
        SortingCursor subCursor = new SortingCursor(mockCursor);

        OrderBy order = new OrderByImpl();
        Column column = new ColumnImpl();
        column.setColumnName("ID");
        column.setTableName("T1");
        column.setType(Type.IntegerType);
        order.setColumn(column);
        List<OrderBy> orderBys = new ArrayList();

        orderBys.add(order);
        TemporaryTableSortCursor c = new TemporaryTableSortCursor(new ExecuteContext(), cf, bdbRepo, 0, subCursor, orderBys, true);
        Object[] expected = new Object[]{1, 2, 3, 4, 5, 5};
        List actual = new ArrayList();

        RowValues row = null;
        while ((row = c.next()) != null) {

            System.out.println(row);
            actual.add(row.getObject(0));
        }

        Assert.assertArrayEquals(expected, actual.toArray());
        Assert.assertTrue(mockCursor.isClosed());
    }

    @Test
    public void testNull() throws RuntimeException {
        RepositoryNameToRepositoryMap repoHolder = new RepositoryNameToRepositoryMap();
        StaticSchemaManager sm = new StaticSchemaManager("test_schema.xml");
        sm.init();
        Repository bdbRepo = repoHolder.getRepository("bdb", Collections.EMPTY_MAP, Collections.EMPTY_MAP);
        CursorFactory cf = new CursorFactoryImpl();

        SortingCursor subCursor = new SortingCursor(this.getCursor("T1", new Integer[]{5, null, 4, 3, 2, null, 1}));

        OrderBy order = new OrderByImpl();
        Column column = new ColumnImpl();
        column.setColumnName("ID");
        column.setTableName("T1");
        column.setType(Type.IntegerType);
        order.setColumn(column);
        List<OrderBy> orderBys = new ArrayList();

        orderBys.add(order);
        TemporaryTableSortCursor c = new TemporaryTableSortCursor(new ExecuteContext(), cf, bdbRepo, 0, subCursor, orderBys, true);
        Object[] expected = new Object[]{1, 2, 3, 4, 5, null, null};
        List actual = new ArrayList();

        RowValues row = null;
        while ((row = c.next()) != null) {

            System.out.println(row);
            actual.add(row.getObject(0));
        }

        Assert.assertArrayEquals(expected, actual.toArray());

    }

    @Test
    public void testEmpty() throws RuntimeException {
        RepositoryNameToRepositoryMap repoHolder = new RepositoryNameToRepositoryMap();
        StaticSchemaManager sm = new StaticSchemaManager("test_schema.xml");
        sm.init();
        Repository bdbRepo = repoHolder.getRepository("bdb", Collections.EMPTY_MAP, Collections.EMPTY_MAP);
        CursorFactory cf = new CursorFactoryImpl();

        SortingCursor subCursor = new SortingCursor(this.getCursor("T1", new Integer[]{}));

        OrderBy order = new OrderByImpl();
        Column column = new ColumnImpl();
        column.setColumnName("ID");
        column.setTableName("T1");
        column.setType(Type.IntegerType);
        order.setColumn(column);
        List<OrderBy> orderBys = new ArrayList();

        orderBys.add(order);
        TemporaryTableSortCursor c = new TemporaryTableSortCursor(new ExecuteContext(), cf, bdbRepo, 0, subCursor, orderBys, true);
        Object[] expected = new Object[]{};
        List actual = new ArrayList();

        RowValues row = null;
        while ((row = c.next()) != null) {

            System.out.println(row);
            actual.add(row.getObject(0));
        }

        Assert.assertArrayEquals(expected, actual.toArray());
    }

    @Test
    public void testGetReturnColumnsBeforeNext() throws RuntimeException {
        RepositoryNameToRepositoryMap repoHolder = new RepositoryNameToRepositoryMap();
        StaticSchemaManager sm = new StaticSchemaManager("test_schema.xml");
        sm.init();
        Repository bdbRepo = repoHolder.getRepository("bdb", Collections.EMPTY_MAP, Collections.EMPTY_MAP);
        CursorFactory cf = new CursorFactoryImpl();
        MockArrayCursor mockCursor = this.getCursor("T1", new Integer[]{5, 5, 4, 3, 2, 1});
        SortingCursor subCursor = new SortingCursor(mockCursor);

        OrderBy order = new OrderByImpl();
        Column column = new ColumnImpl();
        column.setColumnName("ID");
        column.setTableName("T1");
        column.setType(Type.IntegerType);
        order.setColumn(column);
        List<OrderBy> orderBys = new ArrayList();

        orderBys.add(order);
        TemporaryTableSortCursor c = new TemporaryTableSortCursor(new ExecuteContext(), cf, bdbRepo, 0, subCursor, orderBys, true);
        Assert.assertEquals("[T1.ID, T1.NAME, T1.SCHOOL]", c.getColumnMetaDataList().toString());
        Assert.assertEquals("[OrderByImpl [columnName=T1.ID, direction=true]]", c.getOrderByList().toString());

    }

    @Test
    public void testGetReturnColumnsAfterNext() throws RuntimeException {
        RepositoryNameToRepositoryMap repoHolder = new RepositoryNameToRepositoryMap();
        StaticSchemaManager sm = new StaticSchemaManager("test_schema.xml");
        sm.init();
        Repository bdbRepo = repoHolder.getRepository("bdb", Collections.EMPTY_MAP, Collections.EMPTY_MAP);
        CursorFactory cf = new CursorFactoryImpl();
        MockArrayCursor mockCursor = this.getCursor("T1", new Integer[]{5, 5, 4, 3, 2, 1});
        SortingCursor subCursor = new SortingCursor(mockCursor);

        OrderBy order = new OrderByImpl();
        Column column = new ColumnImpl();
        column.setColumnName("ID");
        column.setTableName("T1");
        column.setType(Type.IntegerType);
        order.setColumn(column);
        List<OrderBy> orderBys = new ArrayList();

        orderBys.add(order);
        TemporaryTableSortCursor c = new TemporaryTableSortCursor(new ExecuteContext(), cf, bdbRepo, 0, subCursor, orderBys, true);

        while (c.next() != null)
            ;

        Assert.assertEquals("[T1.ID, T1.NAME, T1.SCHOOL]", c.getColumnMetaDataList().toString());
        Assert.assertEquals("[OrderByImpl [columnName=T1.ID, direction=true]]", c.getOrderByList().toString());
    }

}
