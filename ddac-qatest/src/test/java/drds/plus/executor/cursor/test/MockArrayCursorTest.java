package drds.plus.executor.cursor.test;

import drds.plus.executor.row_values.RowValues;
import drds.plus.sql_process.type.Type;
import org.junit.Assert;
import org.junit.Test;

public class MockArrayCursorTest {

    @Test
    public void test1() throws RuntimeException {
        MockArrayCursor cursor = new MockArrayCursor("table1");
        cursor.addColumn("id", Type.IntegerType);
        cursor.addColumn("columnName", Type.StringType);
        cursor.addColumn("school", Type.StringType);
        cursor.initMeta();

        cursor.addRow(new Object[]{1, "name1", "school1"});
        cursor.addRow(new Object[]{2, "name2", "school2"});
        cursor.addRow(new Object[]{3, "name3", "school3"});
        cursor.addRow(new Object[]{4, "name4", "school4"});
        cursor.addRow(new Object[]{5, "name5", "school5"});
        cursor.addRow(new Object[]{6, "name6", "school6"});
        cursor.addRow(new Object[]{7, "name7", "school7"});

        cursor.init();

        RowValues row = null;
        int count = 0;
        while ((row = cursor.next()) != null) {
            System.out.println(row);
            count++;

        }

        Assert.assertEquals(7, count);
    }
}
