package drds.plus.executor.cursor.test;

import drds.plus.common.lifecycle.AbstractLifecycle;
import drds.plus.executor.cursor.cursor.Cursor;
import drds.plus.executor.cursor.cursor.impl.DuplicateValueRowDataLinkedList;
import drds.plus.executor.cursor.cursor_metadata.CursorMetaData;
import drds.plus.executor.cursor.cursor_metadata.CursorMetaDataImpl;
import drds.plus.executor.record_codec.record.KeyValueRecordPair;
import drds.plus.executor.record_codec.record.Record;
import drds.plus.executor.row_values.ArrayRowValues;
import drds.plus.executor.row_values.RowValues;
import drds.plus.sql_process.abstract_syntax_tree.configuration.ColumnMetaData;
import drds.plus.sql_process.type.Type;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Slf4j
public class MockArrayCursor extends AbstractLifecycle implements Cursor {

    public Logger log() {
        return log;
    }

    private final String tableName;
    List<RowValues> rowDataList = new ArrayList();
    Iterator<RowValues> rowDataIterator = null;
    List<ColumnMetaData> columnMetaDataList = new ArrayList();
    private CursorMetaData cursorMetaData;
    private RowValues current;
    private boolean closed = false;

    public MockArrayCursor(String tableName) {
        this.tableName = tableName;
    }

    public void addColumn(String columnName, Type type) {
        ColumnMetaData columnMetaData = new ColumnMetaData(this.tableName, columnName, null, type, true);
        columnMetaDataList.add(columnMetaData);

    }

    public void addRow(Object[] values) {
        ArrayRowValues arrayRowData = new ArrayRowValues(this.cursorMetaData, values);
        rowDataList.add(arrayRowData);
    }

    public void doInit() {

        rowDataIterator = rowDataList.iterator();
    }

    public boolean skipTo(Record keyRecord) throws RuntimeException {
        // TODO Auto-generated method stub
        return false;
    }

    public boolean skipTo(KeyValueRecordPair keyKeyValueRecordPair) throws RuntimeException {
        // TODO Auto-generated method stub
        return false;
    }

    public RowValues current() throws RuntimeException {
        return this.current;
    }

    public RowValues next() throws RuntimeException {
        if (rowDataIterator.hasNext()) {
            current = rowDataIterator.next();
            return current;
        }
        current = null;
        return null;
    }

    public RowValues prev() throws RuntimeException {
        // TODO Auto-generated method stub
        return null;
    }

    public RowValues first() throws RuntimeException {
        if (rowDataList.isEmpty())
            return null;
        return rowDataList.get(0);
    }

    public void beforeFirst() throws RuntimeException {
        rowDataIterator = rowDataList.iterator();
        current = null;

    }

    public RowValues last() throws RuntimeException {
        if (rowDataList.isEmpty())
            return null;

        return rowDataList.get(rowDataList.size() - 1);
    }

    public List<RuntimeException> close(List<RuntimeException> exceptions) {
        this.closed = true;
        if (exceptions == null)
            exceptions = new ArrayList();
        return exceptions;
    }

    public boolean delete() throws RuntimeException {
        // TODO Auto-generated method stub
        return false;
    }

    public RowValues getNextDuplicateValueRowData() throws RuntimeException {
        // TODO Auto-generated method stub
        return null;
    }

    public void put(Record key, Record value) throws RuntimeException {
        // TODO Auto-generated method stub

    }

    public Map<Record, DuplicateValueRowDataLinkedList> mgetRecordToDuplicateValueRowDataLinkedListMap(List<Record> keyRecordList, boolean prefixMatch, boolean keyFilterOrValueFilter) throws RuntimeException {
        // TODO Auto-generated method stub
        return null;
    }

    public List<DuplicateValueRowDataLinkedList> mgetDuplicateValueRowDataLinkedListList(List<Record> keyRecordList, boolean prefixMatch, boolean keyFilterOrValueFilter) throws RuntimeException {
        // TODO Auto-generated method stub
        return null;
    }

    public boolean isDone() {
        // TODO Auto-generated method stub
        return false;
    }

    public String toStringWithInden(int inden) {
        // TODO Auto-generated method stub
        return null;
    }

    public List<ColumnMetaData> getColumnMetaDataList() throws RuntimeException {
        return columnMetaDataList;
    }

    public void initMeta() {
        this.cursorMetaData = CursorMetaDataImpl.buildCursorMetaData(columnMetaDataList);

    }

    public boolean isClosed() {
        return this.closed;
    }

}
