package drds.plus.sql_process;

import drds.plus.repository.mysql.execute_plan_to_sql.DataNodeIdToSqlsMapMergeInfo;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Dreamond
 */
public class SqlSelectSingleOptimizerTest extends BaseSqlOptimizerTest {

    @Test
    public void testQuerySelectConstant() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement 'avs' setAliasAndSetNeedBuild a from STUDENT  setWhereAndSetNeedBuild ID = 1");
        Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
        Assert.assertEquals("selectStatement (?) setAliasAndSetNeedBuild A from student setWhereAndSetNeedBuild (STUDENT.ID = ?)", getSql0(node));

    }

    @Test
    public void testQueryCountString() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement count('avs') from STUDENT  setWhereAndSetNeedBuild ID = 1");
        System.out.println(node);
        Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
        Assert.assertEquals("selectStatement count(?) from student setWhereAndSetNeedBuild (STUDENT.ID = ?)", getSql0(node));
    }

    @Test
    public void testQuerySelectFilter() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement 1+1 from STUDENT  setWhereAndSetNeedBuild ID = 1");
        System.out.println(node);
        Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
        Assert.assertEquals("selectStatement (?) from student setWhereAndSetNeedBuild (STUDENT.ID = ?)", getSql0(node));
    }

    @Test
    public void testQueryAddArg() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement count(1+1) from STUDENT  setWhereAndSetNeedBuild ID = 1");
        System.out.println(node);
        Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
        Assert.assertEquals("selectStatement count(?) from student setWhereAndSetNeedBuild (STUDENT.ID = ?)", getSql0(node));
    }

    @Test
    public void testQueryFilterArg() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement count(1=1) from STUDENT");
        System.out.println(node);
        Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
        Assert.assertEquals("selectStatement count(?) from student", getSql0(node));
    }

    @Test
    public void testQueryFilterArgInWhere() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement count(1=1) from STUDENT  setWhereAndSetNeedBuild ID = date(1=1)");
        System.out.println(node);
        Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
        Assert.assertEquals("selectStatement count(?) from student setWhereAndSetNeedBuild (STUDENT.ID = DATE(?))", getSql0(node));
    }

    @Test
    public void testQueryFilterInSelect() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement 1=1 from STUDENT");
        System.out.println(node);
        Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
        Assert.assertEquals("selectStatement (?) from student", getSql0(node));
    }

    @Test
    public void testQueryWithDuplicatedColumn() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement id,id setAliasAndSetNeedBuild id1,sum(id)  ,sum(id) setAliasAndSetNeedBuild sum1 from STUDENT  setWhereAndSetNeedBuild columnName = 1 group  by id order by id");
        System.out.println(node);
        Assert.assertEquals(1, node.getDataNodeIdToSqlsMap().size());
        Assert.assertEquals("selectStatement STUDENT.ID,STUDENT.ID setAliasAndSetNeedBuild ID1,sum(STUDENT.ID),sum(STUDENT.ID) setAliasAndSetNeedBuild SUM1 from student setWhereAndSetNeedBuild (STUDENT.NAME = ?) group by STUDENT.ID order by STUDENT.ID asc ", getSql0(node));
    }

    @Test
    public void testQueryWithSameColumnTwiceSingleDB() throws Exception {
        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement id,id from STUDENT  setWhereAndSetNeedBuild columnName = 1");
            System.out.println(node);
            Assert.assertEquals(1, node.getDataNodeIdToSqlsMap().size());
            Assert.assertEquals("selectStatement STUDENT.ID,STUDENT.ID from student setWhereAndSetNeedBuild (STUDENT.NAME = ?)", getSql0(node));
        }

        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement id,id from STUDENT  setWhereAndSetNeedBuild columnName = 1 and id=1");
            System.out.println(node);
            Assert.assertEquals(1, node.getDataNodeIdToSqlsMap().size());
            Assert.assertEquals("selectStatement STUDENT.ID,STUDENT.ID from student setWhereAndSetNeedBuild ((STUDENT.ID = ?) and (STUDENT.NAME = ?))", getSql0(node));
        }
    }

    @Test
    public void testQueryWithSameColumnTwiceMultiDB() throws Exception {
        try {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement id,id from STUDENT");
            System.out.println(node);
        } catch (Exception e) {
            Assert.assertTrue(e.getMessage().contains("'STUDENT.ID' is ambiguous"));
        }
    }

    @Test
    public void testQueryWithValueEqualColumn() throws Exception {
        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement * from STUDENT  setWhereAndSetNeedBuild 1=id");
            System.out.println(node);
            Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
            Assert.assertEquals("selectStatement STUDENT.ID,STUDENT.NAME,STUDENT.SCHOOL from student setWhereAndSetNeedBuild (STUDENT.ID = ?)", getSql0(node));
        }

        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement * from STUDENT  setWhereAndSetNeedBuild 1=columnName");
            System.out.println(node);
            Assert.assertEquals(1, node.getDataNodeIdToSqlsMap().size());
            Assert.assertEquals("selectStatement STUDENT.ID,STUDENT.NAME,STUDENT.SCHOOL from student setWhereAndSetNeedBuild (STUDENT.NAME = ?)", getSql0(node));
        }

        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement * from STUDENT  setWhereAndSetNeedBuild 1<id");
            System.out.println(node);
            Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
            Assert.assertEquals("selectStatement STUDENT.ID,STUDENT.NAME,STUDENT.SCHOOL from student setWhereAndSetNeedBuild (STUDENT.ID > ?)", getSql0(node));
        }
    }

    @Test
    public void testQueryWithFunctionEqualColumn() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement * from STUDENT  setWhereAndSetNeedBuild now()=id");
        System.out.println(node);
        Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
        Assert.assertEquals("selectStatement STUDENT.ID,STUDENT.NAME,STUDENT.SCHOOL from student setWhereAndSetNeedBuild (NOW() = STUDENT.ID)", getSql0(node));
    }

    @Test
    public void testQueryWithCompExpr() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement 1>1,1||1,1&1,1=1,1<=>1 from student");
        System.out.println(node);
        Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
        Assert.assertEquals("selectStatement (? > ?),(?),(? & ?),(?),(?) from student", getSql0(node));
    }

    @Test
    public void testQueryWithBetweenInSelect() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("SELECT 1 BETWEEN 2 and 3 from student");
        System.out.println(node);
        Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
        Assert.assertEquals("selectStatement ((? >= ?) and (? <= ?)) from student", getSql0(node));

    }

    @Test
    public void testQueryWithXOR() throws Exception {
        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("SELECT 1 ^ 1 from STUDENT setWhereAndSetNeedBuild  1 + 1 limit 1");
            System.out.println(node);
            Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
            Assert.assertEquals("selectStatement (? ^ ?) from student setWhereAndSetNeedBuild (?) limit ?,?", getSql0(node));// bitxor
        }
        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("SELECT 1 xor 1 from STUDENT setWhereAndSetNeedBuild 1 ^ 1");
            System.out.println(node);
            Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
            Assert.assertEquals("selectStatement (?) from student setWhereAndSetNeedBuild ((? ^ ?))", getSql0(node)); // logicalxor
        }
    }

    @Test
    public void testQueryWithInInSelect() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement  'wefwf' in (0,3,5,'wefwf') from student setWhereAndSetNeedBuild 'wefwf' in (0,3,5,'wefwf')");
        System.out.println(node);
        Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
        Assert.assertEquals("selectStatement (? in (?,?,?,?)) from student setWhereAndSetNeedBuild (? in (?,?,?,?))", getSql0(node));
    }

    @Test
    public void testQueryWithJoinAndColumnAndStar() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement *,count(*) from STUDENT s join STUDENT t on s.columnName = t.columnName ");
        System.out.println(node);
        Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
        Assert.assertEquals("selectStatement S.ID,S.NAME,S.SCHOOL,T.ID,T.NAME,T.SCHOOL,count(*) from student S join student T on S.NAME = T.NAME", getSql0(node));
    }

    @Test
    public void testQueryWithEqualNull() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement *  from STUDENT setWhereAndSetNeedBuild id = null");
        System.out.println(node);
        Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
        Assert.assertEquals("selectStatement STUDENT.ID,STUDENT.NAME,STUDENT.SCHOOL from student setWhereAndSetNeedBuild (STUDENT.ID = null)", getSql0(node));
    }

    @Test
    public void testQueryWithNotNull() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement not null from STUDENT setWhereAndSetNeedBuild id = null");
        System.out.println(node);
        Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
        Assert.assertEquals("selectStatement NOT(null) from student setWhereAndSetNeedBuild (STUDENT.ID = null)", getSql0(node));
    }

    @Test
    public void testQueryWithAndNull() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement 1 && NULL_KEY from STUDENT setWhereAndSetNeedBuild id = null");
        System.out.println(node);
        Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
        Assert.assertEquals("selectStatement ((?) and (null)) from student setWhereAndSetNeedBuild (STUDENT.ID = null)", getSql0(node));
    }

    @Test
    public void testQueryWithIsTrue() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement 1 is true from STUDENT setWhereAndSetNeedBuild id = null");
        System.out.println(node);
        Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
        Assert.assertEquals("selectStatement ? is TRUE from student setWhereAndSetNeedBuild (STUDENT.ID = null)", getSql0(node));
    }

    @Test
    public void testQueryWithIsNotTrue() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement 1 is not true from STUDENT setWhereAndSetNeedBuild id = null");
        System.out.println(node);
        Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
        Assert.assertEquals("selectStatement ? is NOT TRUE from student setWhereAndSetNeedBuild (STUDENT.ID = null)", getSql0(node));
    }

    @Test
    public void testQueryWithConv() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("SELECT CONV(-17,10,-18) from STUDENT setWhereAndSetNeedBuild id = null");
        System.out.println(node);
        Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
        Assert.assertEquals("selectStatement CONV(?,?,?) from student setWhereAndSetNeedBuild (STUDENT.ID = null)", getSql0(node));
    }

    @Test
    public void testQueryWith取反结果() throws Exception {
        try {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("SELECT !(1+ID) from STUDENT setWhereAndSetNeedBuild id = null");
            System.out.println(node);
            Assert.fail();
        } catch (Exception e) {
            // 暂时不支持
        }
    }

    @Test
    public void testQueryWith取反结果_提前计算() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("SELECT !(1+1) from STUDENT setWhereAndSetNeedBuild id = null");
        System.out.println(node);
        Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
        Assert.assertEquals("selectStatement (?) from student setWhereAndSetNeedBuild (STUDENT.ID = null)", getSql0(node));
    }

    @Test
    public void testQueryWithKuohao() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("SELECT 102/(1-ID) from STUDENT setWhereAndSetNeedBuild id = null");
        System.out.println(node);
        Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
        Assert.assertEquals("selectStatement (? / (? - STUDENT.ID)) from student setWhereAndSetNeedBuild (STUDENT.ID = null)", getSql0(node));
    }

    @Test
    public void testQueryWithInterval() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("SELECT interval_primary(23, 1, 15, 17, 30, 44, 200) from STUDENT setWhereAndSetNeedBuild id = null");
        System.out.println(node);
        Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
        Assert.assertEquals("selectStatement interval_primary(?,?,?,?,?,?,?) from student setWhereAndSetNeedBuild (STUDENT.ID = null)", getSql0(node));
    }

    @Test
    public void testQueryWithDateInterval() throws Exception {
        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("SELECT ADDDATE('2008-01-02', interval_primary 31 DAY) from STUDENT setWhereAndSetNeedBuild id = null");
            System.out.println(node);
            Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
            Assert.assertEquals("selectStatement ADDDATE(?,interval_primary ? DAY) from student setWhereAndSetNeedBuild (STUDENT.ID = null)", getSql0(node));
        }

        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("SELECT '2008-12-31 23:59:59' + interval_primary 1 SECOND from STUDENT setWhereAndSetNeedBuild id = null");
            System.out.println(node);
            Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
            Assert.assertEquals("selectStatement (? + interval_primary ? SECOND) from student setWhereAndSetNeedBuild (STUDENT.ID = null)", getSql0(node));
        }

        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("SELECT DATE_ADD('1992-12-31 23:59:59.000002' , interval_primary '1.999999' SECOND_MICROSECOND)  from STUDENT setWhereAndSetNeedBuild id = null");
            System.out.println(node);
            Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
            Assert.assertEquals("selectStatement DATE_ADD(?,interval_primary ? SECOND_MICROSECOND) from student setWhereAndSetNeedBuild (STUDENT.ID = null)", getSql0(node));
        }

    }

    @Test
    public void testQueryWithStringAndFloatCopmare() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("SELECT '.01' = 0.01 setAliasAndSetNeedBuild T from STUDENT setWhereAndSetNeedBuild id = null");
        System.out.println(node);
        Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
        Assert.assertEquals("selectStatement (?) setAliasAndSetNeedBuild T from student setWhereAndSetNeedBuild (STUDENT.ID = null)", getSql0(node));
    }

    @Test
    public void testQueryWithIf() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement sum(IF (t.id=-1,1,0))  from STUDENT t setWhereAndSetNeedBuild id = 1");
        System.out.println(node);
        Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
        Assert.assertEquals("selectStatement sum(IF((T.ID = ?),?,?)) from student T setWhereAndSetNeedBuild (T.ID = ?)", getSql0(node));
    }

    @Test
    public void testQueryWithOperatorEqualColumn() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement * from STUDENT  setWhereAndSetNeedBuild 1+1=id");
        System.out.println(node);
        Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
        Assert.assertEquals("selectStatement STUDENT.ID,STUDENT.NAME,STUDENT.SCHOOL from student setWhereAndSetNeedBuild (STUDENT.ID = ?)", getSql0(node));
    }

    @Test
    public void testPartitionColumnQuery() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement * from student setWhereAndSetNeedBuild columnName=1");
        System.out.println(node);
        Assert.assertEquals(1, node.getDataNodeIdToSqlsMap().size());
        Assert.assertEquals("selectStatement STUDENT.ID,STUDENT.NAME,STUDENT.SCHOOL from student setWhereAndSetNeedBuild (STUDENT.NAME = ?)", getSql0(node));
    }

    @Test
    public void testQueryWithNullArg() throws Exception {
        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("SELECT ASCII(NULL_KEY) from student");
            System.out.println(node);
            Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
            Assert.assertEquals("selectStatement ASCII(null) from student", getSql0(node));
        }
        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("SELECT BIT_LENGTH(null) from student");
            System.out.println(node);
            Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
            Assert.assertEquals("selectStatement BIT_LENGTH(null) from student", getSql0(node));
        }

        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("SELECT BIN(null) from student");
            System.out.println(node);
            Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
            Assert.assertEquals("selectStatement BIN(null) from student", getSql0(node));
        }
        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("SELECT QUOTE(NULL_KEY) from student");
            System.out.println(node);
            Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
            Assert.assertEquals("selectStatement QUOTE(null) from student", getSql0(node));
        }
    }

    @Test
    public void testQueryWithIsNull() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("SELECT 1 is NULL_KEY, 0 is NULL_KEY, NULL_KEY is NULL_KEY from student");
        System.out.println(node);
        Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
        Assert.assertEquals("selectStatement ? is NULL_KEY,? is NULL_KEY,null is NULL_KEY from student", getSql0(node));
    }

    @Test
    public void testQueryWithSelectNull() throws Exception {
        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement (null) from student");
            System.out.println(node);
            Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
            Assert.assertEquals("selectStatement (null) from student", getSql0(node));
        }

        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("SELECT null from student setWhereAndSetNeedBuild id=null");
            System.out.println(node);
            Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
            Assert.assertEquals("selectStatement (null) from student setWhereAndSetNeedBuild (STUDENT.ID = null)", getSql0(node));
        }
    }

    @Test
    public void testQueryWithIsNotNull() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("SELECT 1 is NOT NULL_KEY, 0 is NOT NULL_KEY, NULL_KEY is NOT NULL_KEY from student");
        System.out.println(node);
        Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
        Assert.assertEquals("selectStatement ? is NOT NULL_KEY,? is NOT NULL_KEY,null is NOT NULL_KEY from student", getSql0(node));
    }

    @Test
    public void testQueryWithoutCondition() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement * from student");
        System.out.println(node);
        Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
        Assert.assertEquals("selectStatement STUDENT.ID,STUDENT.NAME,STUDENT.SCHOOL from student", getSql0(node));
    }

    @Test
    public void testQueryWithMinusOperatorInt() throws Exception {
        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement ID-1, ID--1 from student");
            System.out.println(node);
            Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
            Assert.assertEquals("selectStatement (STUDENT.ID - ?),(STUDENT.ID - ?) from student", getSql0(node));
        }

        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement ID-1, ID-1 from student");
            System.out.println(node);
            Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
            Assert.assertEquals("selectStatement (STUDENT.ID - ?),(STUDENT.ID - ?) from student", getSql0(node));
        }

        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement ID-1, ID---1 from student");
            System.out.println(node);
            Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
            Assert.assertEquals("selectStatement (STUDENT.ID - ?),(STUDENT.ID - ?) from student", getSql0(node));
        }

        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement (ID - 1),(ID - -(-(-(1)))) from STUDENT ");
            System.out.println(node);
            Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
            Assert.assertEquals("selectStatement (STUDENT.ID - ?),(STUDENT.ID - ?) from student", getSql0(node));
        }

    }

    @Test
    public void testQueryWithMinusOperatorFloat() throws Exception {
        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement 1-1.1 from student");
            System.out.println(node);
            Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
            Assert.assertEquals("selectStatement (?) from student", getSql0(node));
        }

        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement 1--1.1 from student");
            System.out.println(node);
            Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
            Assert.assertEquals("selectStatement (?) from student", getSql0(node));
        }

        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement 1-1, 1---1.1 from student");
            System.out.println(node);
            Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
            Assert.assertEquals("selectStatement (?),(?) from student", getSql0(node));
        }

        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement 1-1, 1----1.1 from student");
            System.out.println(node);
            Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
            Assert.assertEquals("selectStatement (?),(?) from student", getSql0(node));
        }

    }

    @Test
    public void testQueryWithMinusOperatorCount() throws Exception {
        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement -(id) from student");
            System.out.println(node);
            Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
            Assert.assertEquals("selectStatement -(STUDENT.ID) from student", getSql0(node));
        }

        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement --id from student");
            System.out.println(node);
            Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
            Assert.assertEquals("selectStatement -(-(STUDENT.ID)) from student", getSql0(node));
        }

        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement --date(-id) from student");
            System.out.println(node);
            Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
            Assert.assertEquals("selectStatement -(-(DATE(-(STUDENT.ID)))) from student", getSql0(node));
        }

    }

    @Test
    public void testQueryWithConditionNotPartitionColumn() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement * from student setWhereAndSetNeedBuild school=1");
        System.out.println(node);
        Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
        Assert.assertEquals("selectStatement STUDENT.ID,STUDENT.NAME,STUDENT.SCHOOL from student setWhereAndSetNeedBuild (STUDENT.SCHOOL = ?)", getSql0(node));
    }

    @Test
    public void testQueryWithBlob() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement * from student setWhereAndSetNeedBuild school=_binary'asdasdasdsadsa'");
        System.out.println(node);
        Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
        Assert.assertEquals("selectStatement STUDENT.ID,STUDENT.NAME,STUDENT.SCHOOL from student setWhereAndSetNeedBuild (STUDENT.SCHOOL = ?)", getSql0(node));
    }

    @Test
    public void testQueryWithBit() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement * from student setWhereAndSetNeedBuild school=b'101010101010'");
        System.out.println(node);
        Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
        Assert.assertEquals("selectStatement STUDENT.ID,STUDENT.NAME,STUDENT.SCHOOL from student setWhereAndSetNeedBuild (STUDENT.SCHOOL = ?)", getSql0(node));
    }

    @Test
    public void testQueryWithValueEqualValue() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement * from student setWhereAndSetNeedBuild id=1 and 1=1");
        System.out.println(node);
        Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
        Assert.assertEquals("selectStatement STUDENT.ID,STUDENT.NAME,STUDENT.SCHOOL from student setWhereAndSetNeedBuild (STUDENT.ID = ?)", getSql0(node));

    }

    @Test
    public void testQueryWithTrueEqualTrue() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement * from student setWhereAndSetNeedBuild id=1 and true=true");
        System.out.println(node);
        Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
        Assert.assertEquals("selectStatement STUDENT.ID,STUDENT.NAME,STUDENT.SCHOOL from student setWhereAndSetNeedBuild (STUDENT.ID = ?)", getSql0(node));

    }

    @Test
    public void testQueryWithTrueAndTrue() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement * from student setWhereAndSetNeedBuild id=1 and true");
        System.out.println(node);
        Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
        Assert.assertEquals("selectStatement STUDENT.ID,STUDENT.NAME,STUDENT.SCHOOL from student setWhereAndSetNeedBuild (STUDENT.ID = ?)", getSql0(node));
    }

    @Test
    public void testQueryWithTrueAndFalse() throws Exception {
        try {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement * from student setWhereAndSetNeedBuild id=1 and false");
            System.out.println(node);
            Assert.fail();
        } catch (Exception e) {
            // 空结果
            // Assert.assertTrue(e.getMessage().contains("EmptyResultFilterException"));
        }
    }

    @Test
    public void testQueryWithTrueOrFalse() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement * from student setWhereAndSetNeedBuild id=1 or false");
        System.out.println(node);
        Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
        Assert.assertEquals("selectStatement STUDENT.ID,STUDENT.NAME,STUDENT.SCHOOL from student setWhereAndSetNeedBuild (STUDENT.ID = ?)", getSql0(node));
    }

    @Test
    public void testQueryWithTrueOrTrue() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement * from student setWhereAndSetNeedBuild id=1 or true");
        System.out.println(node);
        Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
        Assert.assertEquals("selectStatement STUDENT.ID,STUDENT.NAME,STUDENT.SCHOOL from student setWhereAndSetNeedBuild (?)", getSql0(node));
    }

    @Test
    public void testQueryWithRange() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement * from student setWhereAndSetNeedBuild id>1");
        System.out.println(node);
        Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
        Assert.assertEquals("selectStatement STUDENT.ID,STUDENT.NAME,STUDENT.SCHOOL from student setWhereAndSetNeedBuild (STUDENT.ID > ?)", getSql0(node));

    }

    @Test
    public void testQueryWithOr() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement * from student setWhereAndSetNeedBuild columnName=1 or columnName='sadasd'");
        System.out.println(node);
        Assert.assertEquals("selectStatement STUDENT.ID,STUDENT.NAME,STUDENT.SCHOOL from student setWhereAndSetNeedBuild (STUDENT.NAME in (?,?))", getSql0(node));
    }

    @Test
    public void testQueryWithIn() throws Exception {
        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement * from student setWhereAndSetNeedBuild NAME In (1)");
            System.out.println(node);
            Assert.assertEquals(1, node.getDataNodeIdToSqlsMap().size());
            Assert.assertEquals("selectStatement STUDENT.ID,STUDENT.NAME,STUDENT.SCHOOL from student setWhereAndSetNeedBuild (STUDENT.NAME in (?))", getSql0(node));
        }

        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement * from student setWhereAndSetNeedBuild NAME In (1) and id=1");
            System.out.println(node);
            Assert.assertEquals(1, node.getDataNodeIdToSqlsMap().size());
            Assert.assertEquals("selectStatement STUDENT.ID,STUDENT.NAME,STUDENT.SCHOOL from student setWhereAndSetNeedBuild ((STUDENT.ID = ?) and (STUDENT.NAME in (?)))", getSql0(node));
        }
        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement * from student setWhereAndSetNeedBuild columnName in (1,2)");
            System.out.println(node);
            Assert.assertEquals(1, node.getDataNodeIdToSqlsMap().size());
            Assert.assertEquals("selectStatement STUDENT.ID,STUDENT.NAME,STUDENT.SCHOOL from student setWhereAndSetNeedBuild (STUDENT.NAME in (?,?))", getSql0(node));
        }

        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement * from student setWhereAndSetNeedBuild columnName in (1,2,3,4,5,6,7,8,9,0)");
            System.out.println(node);
            Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
            Assert.assertEquals("selectStatement STUDENT.ID,STUDENT.NAME,STUDENT.SCHOOL from student setWhereAndSetNeedBuild (STUDENT.NAME in (?,?,?,?,?,?))", getSql0(node));

            Assert.assertEquals("selectStatement STUDENT.ID,STUDENT.NAME,STUDENT.SCHOOL from student setWhereAndSetNeedBuild (STUDENT.NAME in (?,?,?,?))", getSql1(node));
        }
    }

    @Test
    public void testQueryWithPartitionColumnAndNotPartitionColumn() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement * from student setWhereAndSetNeedBuild columnName=1 and school=1");
        System.out.println(node);
        Assert.assertEquals(1, node.getDataNodeIdToSqlsMap().size());
        Assert.assertEquals("selectStatement STUDENT.ID,STUDENT.NAME,STUDENT.SCHOOL from student setWhereAndSetNeedBuild ((STUDENT.NAME = ?) and (STUDENT.SCHOOL = ?))", getSql0(node));

    }

    @Test
    public void testQueryWithPartitionColumnOrNotPartitionColumn() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement * from student setWhereAndSetNeedBuild columnName=1 or school=1");
        System.out.println(node);
        Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
        Assert.assertEquals("selectStatement STUDENT.ID,STUDENT.NAME,STUDENT.SCHOOL from student setWhereAndSetNeedBuild ((STUDENT.NAME = ?) or (STUDENT.SCHOOL = ?))", getSql0(node));
    }

    @Test
    public void testQueryWithOrderByPk() throws Exception {
        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement * from student order by id");
            System.out.println(node);
            Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
            Assert.assertEquals(1, node.getOrderByList().size());
            Assert.assertEquals("ID", node.getOrderByList().get(0).getItem().getColumnName());
            Assert.assertEquals((Boolean) true, node.getOrderByList().get(0).getAsc());
            Assert.assertEquals("selectStatement STUDENT.ID,STUDENT.NAME,STUDENT.SCHOOL from student order by STUDENT.ID asc ", getSql0(node));

        }
        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement id setAliasAndSetNeedBuild readPriority from student order by readPriority");
            System.out.println(node);
            Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
            Assert.assertEquals(1, node.getOrderByList().size());
            Assert.assertEquals("P", node.getOrderByList().get(0).getItem().getColumnName());
            Assert.assertEquals((Boolean) true, node.getOrderByList().get(0).getAsc());
            Assert.assertEquals("selectStatement STUDENT.ID setAliasAndSetNeedBuild P from student order by STUDENT.ID asc ", getSql0(node));
        }

        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement id setAliasAndSetNeedBuild readPriority from student order by id");
            System.out.println(node);
            Assert.assertEquals(2, node.getDataNodeIdToSqlsMap().size());
            Assert.assertEquals(1, node.getOrderByList().size());
            Assert.assertEquals("P", node.getOrderByList().get(0).getItem().getColumnName());
            Assert.assertEquals((Boolean) true, node.getOrderByList().get(0).getAsc());
            Assert.assertEquals("selectStatement STUDENT.ID setAliasAndSetNeedBuild P from student order by STUDENT.ID asc ", getSql0(node));

        }
    }

    @Test
    public void testQueryWithAvgSingleDb() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement avg(id) from student setWhereAndSetNeedBuild columnName=1");
        System.out.println(node);
        // Assert.assertEquals(1, query.getAggs().size());
        // Assert.assertEquals("avg(id)",
        // query.getAggs().getDataNodeExecutor(0).getColumnName());
        Assert.assertEquals("selectStatement avg(STUDENT.ID) from student setWhereAndSetNeedBuild (STUDENT.NAME = ?)", getSql0(node));
    }

    @Test
    public void testQueryWithRollUp() throws Exception {
        try {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement * from student group by id with rollup");
            System.out.println(node);
            Assert.fail();
        } catch (Exception e) {
            Assert.assertTrue(e.getMessage().contains("with rollup is not supported yet!"));
        }
    }

    @Test
    public void testQueryWithLimitZero() throws Exception {
        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement * from student");
            System.out.println(node);
            Assert.assertEquals(null, node.getLimitFrom());
            Assert.assertEquals(null, node.getLimitTo());
            Assert.assertEquals("selectStatement STUDENT.ID,STUDENT.NAME,STUDENT.SCHOOL from student", getSql0(node));
        }

        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement * from student limit 0,0");
            System.out.println(node);
            Assert.assertEquals((Long) 0L, node.getLimitFrom());
            Assert.assertEquals((Long) 0L, node.getLimitTo());
            Assert.assertEquals("selectStatement STUDENT.ID,STUDENT.NAME,STUDENT.SCHOOL from student limit ?,?", getSql0(node));
        }

        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement * from student limit 0");
            System.out.println(node);
            Assert.assertEquals((Long) 0L, node.getLimitFrom());
            Assert.assertEquals((Long) 0L, node.getLimitTo());
            Assert.assertEquals("selectStatement STUDENT.ID,STUDENT.NAME,STUDENT.SCHOOL from student limit ?,?", getSql0(node));
        }

        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement * from student limit 15");
            System.out.println(node);
            Assert.assertEquals((Long) 0L, node.getLimitFrom());
            Assert.assertEquals((Long) 15L, node.getLimitTo());
            Assert.assertEquals("selectStatement STUDENT.ID,STUDENT.NAME,STUDENT.SCHOOL from student limit ?,?", getSql0(node));
        }
    }

    @Test
    public void testQueryWithAvgSingleQuery() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement avg(id) from student setWhereAndSetNeedBuild columnName=1");
        System.out.println(node);
        // Assert.assertEquals(1, query.getAggs().size());
        // Assert.assertEquals("avg(id)",
        // query.getAggs().getDataNodeExecutor(0).getColumnName());
        Assert.assertEquals("selectStatement avg(STUDENT.ID) from student setWhereAndSetNeedBuild (STUDENT.NAME = ?)", getSql0(node));
    }

    @Test
    public void testQueryWithCount() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement count(id) from student");
        System.out.println(node);
        // Assert.assertEquals(1, query.getAggs().size());
        // Assert.assertEquals("count(id)",
        // query.getAggs().getDataNodeExecutor(0).getColumnName());
        Assert.assertEquals("selectStatement count(STUDENT.ID) from student", getSql0(node));
        Assert.assertEquals("selectStatement count(STUDENT.ID) from student", getSql1(node));

    }

    @Test
    public void testQueryWithMax() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement max(id) from student");
        System.out.println(node);
        // Assert.assertEquals(1, query.getAggs().size());
        // Assert.assertEquals("max(id)",
        // query.getAggs().getDataNodeExecutor(0).getColumnName());

        Assert.assertEquals("selectStatement max(STUDENT.ID) from student", getSql0(node));
        Assert.assertEquals("selectStatement max(STUDENT.ID) from student", getSql1(node));

    }

    @Test
    public void testQueryWithMin() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement min(id) from student");
        System.out.println(node);
        Assert.assertEquals("selectStatement min(STUDENT.ID) from student", getSql0(node));
        Assert.assertEquals("selectStatement min(STUDENT.ID) from student", getSql1(node));

    }

    @Test
    public void testQueryWithSum() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement sum(id) from student");
        System.out.println(node);
        // Assert.assertEquals(1, query.getAggs().size());
        // Assert.assertEquals("sum(id)",
        // query.getAggs().getDataNodeExecutor(0).getColumnName());
        Assert.assertEquals("selectStatement sum(STUDENT.ID) from student", getSql0(node));

    }

    @Test
    public void testQueryWithScalarFunction() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement date(id) from student");
        System.out.println(node);
        // Assert.assertEquals(0, query.getAggs().size());
        Assert.assertEquals("selectStatement DATE(STUDENT.ID) from student", getSql0(node));

    }

    @Test
    public void testQueryWithAggregateFunctionWithGroupBy() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement sum(id),columnName from student group by columnName");
        System.out.println(node);
        Assert.assertEquals(1, node.getGroupByList().size());
        Assert.assertEquals("NAME", node.getGroupByList().get(0).getColumnName());
        Assert.assertEquals("selectStatement sum(STUDENT.ID),STUDENT.NAME from student group by STUDENT.NAME", getSql0(node));

    }

    @Test
    public void testQueryWithAggregateFunctionWithGroupByOrderBy() throws Exception {
        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement sum(id),columnName from student setWhereAndSetNeedBuild columnName=1 group by columnName order by sum(id)");
            System.out.println(node);
            Assert.assertEquals(1, node.getGroupByList().size());
            Assert.assertEquals("NAME", node.getGroupByList().get(0).getColumnName());
            // Assert.assertEquals(1, query.getAggs().size());
            // Assert.assertEquals("sum(id)",
            // query.getAggs().getDataNodeExecutor(0).getColumnName());

            Assert.assertEquals(1, node.getDataNodeIdToSqlsMap().size());
            Assert.assertEquals(1, node.getOrderByList().size());
            Assert.assertEquals("sum(ID)", node.getOrderByList().get(0).getItem().getColumnName());

            Assert.assertEquals("selectStatement sum(STUDENT.ID),STUDENT.NAME from student setWhereAndSetNeedBuild (STUDENT.NAME = ?) group by STUDENT.NAME order by sum(STUDENT.ID) asc ", getSql0(node));
        }

        // {
        // try {
        // getMergeNode("selectStatement sum(id) ,id from student
        // group by id order by sum(id)");
        // Assert.fail("order by and group by is not matched and
        // is not a single group executePlanNode");
        // } catch (IllegalArgumentException e) {
        //
        // }
        // }
    }

    @Test
    public void testQueryWithLimit() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement * from student limit 5,15");
        System.out.println(node);
        Assert.assertEquals((Long) 5L, (Long) node.getLimitFrom());
        Assert.assertEquals((Long) 15L, (Long) node.getLimitTo());
        Assert.assertEquals("selectStatement STUDENT.ID,STUDENT.NAME,STUDENT.SCHOOL from student limit ?,?", getSql0(node));
    }

    @Test
    public void testQueryWithAggregateFunctionWithoutGroupBySelected() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement sum(id) from student group by columnName");
        System.out.println(node);
    }

    @Test
    public void testQueryWithTempColumns() throws Exception {
        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement school from student order by id");
            System.out.println(node);
            Assert.assertEquals(1, node.getItemList().size());
            Assert.assertEquals("selectStatement STUDENT.SCHOOL,STUDENT.ID from student order by STUDENT.ID asc ", getSql0(node));
        }

        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement school from student group by id");
            System.out.println(node);
            Assert.assertEquals(1, node.getItemList().size());
            Assert.assertEquals("selectStatement STUDENT.SCHOOL,STUDENT.ID from student group by STUDENT.ID order by STUDENT.ID asc ", getSql0(node));
        }
        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement school from student setWhereAndSetNeedBuild columnName=1 order by id");
            System.out.println(node);
            Assert.assertEquals("selectStatement STUDENT.SCHOOL from student setWhereAndSetNeedBuild (STUDENT.NAME = ?) order by STUDENT.ID asc ", getSql0(node));
        }

        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement school from student setWhereAndSetNeedBuild columnName=1 group by id");
            System.out.println(node);
            Assert.assertEquals("selectStatement STUDENT.SCHOOL from student setWhereAndSetNeedBuild (STUDENT.NAME = ?) group by STUDENT.ID", getSql0(node));
        }

    }

    @Test
    public void testQueryWithDistinctSingleDB() throws Exception {
        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement distinct(school) from student setWhereAndSetNeedBuild columnName=1");
            System.out.println(node);
            Assert.assertEquals(1, node.getItemList().size());
            Assert.assertEquals("selectStatement  distinct STUDENT.SCHOOL from student setWhereAndSetNeedBuild (STUDENT.NAME = ?)", getSql0(node));
        }

        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement count(distinct school) from student setWhereAndSetNeedBuild columnName=1");
            System.out.println(node);
            Assert.assertEquals(1, node.getItemList().size());
            Assert.assertEquals("selectStatement count( distinct STUDENT.SCHOOL) from student setWhereAndSetNeedBuild (STUDENT.NAME = ?)", getSql0(node));
        }
    }

    @Test
    public void testQueryWithGroupByMultiCol() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement * from student group by columnName,id,school");
        System.out.println(node);
        Assert.assertEquals("selectStatement STUDENT.ID,STUDENT.NAME,STUDENT.SCHOOL from student group by STUDENT.NAME,STUDENT.ID,STUDENT.SCHOOL", getSql0(node));

    }

    @Test
    public void testQueryWithOrderByMultiCol() throws Exception {
        DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement * from student order by columnName,id,school");
        System.out.println(node);
        Assert.assertEquals("selectStatement STUDENT.ID,STUDENT.NAME,STUDENT.SCHOOL from student order by STUDENT.NAME asc ,STUDENT.ID asc ,STUDENT.SCHOOL asc ", getSql0(node));
    }

    @Test
    public void testQueryWithGroupByAndOrderBy() throws Exception {
        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement sum(id),NAME from STUDENT  group by NAME asc order by NAME asc");
            System.out.println(node);
            Assert.assertEquals("selectStatement sum(STUDENT.ID),STUDENT.NAME from student group by STUDENT.NAME order by STUDENT.NAME asc ", getSql0(node));
        }

        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement sum(id) ,columnName from student group by columnName order by columnName desc");
            System.out.println(node);
            Assert.assertEquals("selectStatement sum(STUDENT.ID),STUDENT.NAME from student group by STUDENT.NAME order by STUDENT.NAME desc ", getSql0(node));
        }

        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement sum(id) ,columnName from student group by columnName order by sum(id) desc");
            System.out.println(node);
            Assert.assertEquals("selectStatement sum(STUDENT.ID),STUDENT.NAME from student group by STUDENT.NAME order by STUDENT.NAME asc ", getSql0(node));
        }

        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement * from student group by columnName,id order by id,columnName");
            System.out.println(node);
            Assert.assertEquals("selectStatement STUDENT.ID,STUDENT.NAME,STUDENT.SCHOOL from student group by STUDENT.ID,STUDENT.NAME order by STUDENT.ID asc ,STUDENT.NAME asc ", getSql0(node));
        }
    }

    @Test
    public void testQueryWithHavingCrossDb() throws Exception {
        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement * from student setWhereAndSetNeedBuild id>1 having id<2");
            System.out.println(node);
            Assert.assertEquals("selectStatement STUDENT.ID,STUDENT.NAME,STUDENT.SCHOOL from student setWhereAndSetNeedBuild (STUDENT.ID > ?) having (STUDENT.ID < ?)", getSql0(node));
        }

        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement max(id),columnName from student group by columnName having max(id)<2");
            System.out.println(node);
            Assert.assertEquals("selectStatement max(STUDENT.ID),STUDENT.NAME from student group by STUDENT.NAME having (max(STUDENT.ID) < ?)", getSql0(node));
        }

        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement max(id) m,columnName from student group by columnName having m<2");
            System.out.println(node);
            Assert.assertEquals("selectStatement max(STUDENT.ID) setAliasAndSetNeedBuild M,STUDENT.NAME from student group by STUDENT.NAME having (max(STUDENT.ID) < ?)", getSql0(node));
        }

    }

    @Test
    public void testQueryWithHavingSingleDb() throws Exception {
        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement max(id) m,columnName from student setWhereAndSetNeedBuild columnName=2 group by columnName having m<2");
            System.out.println(node);

            Assert.assertEquals("selectStatement max(STUDENT.ID) setAliasAndSetNeedBuild M,STUDENT.NAME from student setWhereAndSetNeedBuild (STUDENT.NAME = ?) group by STUDENT.NAME having (max(STUDENT.ID) < ?)", getSql0(node));
        }

    }

    @Test
    public void testQueryWithScalarAggregateFunctionSingleDb() throws Exception {
        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement max(id)+min(id) from student setWhereAndSetNeedBuild columnName=2");
            System.out.println(node);
            Assert.assertEquals("selectStatement (max(STUDENT.ID) + min(STUDENT.ID)) from student setWhereAndSetNeedBuild (STUDENT.NAME = ?)", getSql0(node));
        }

        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement -min(id) from student setWhereAndSetNeedBuild columnName=2");
            System.out.println(node);
            Assert.assertEquals("selectStatement -(min(STUDENT.ID)) from student setWhereAndSetNeedBuild (STUDENT.NAME = ?)", node.getDataNodeIdToSqlsMap().get("group0").getSqlList().get(0).getSql());

        }

    }

    // @Test
    // public void testQueryWithScalarAggregateFunctionMultiDb2() throws
    // Exception {
    // try {
    // DataNodeIdToSqlsMapMergeInfo query = getMergeNode(
    // "SELECT DATE_FORMAT('2003-10-03',get_format(DATE,'EUR')) from student",
    // tableMetaData, null, null);
    // System.out.println(query);
    // Assert.fail();
    // } catch (Exception e) {
    // Assert.assertEquals(
    // "FunctionImpl using like this: scalar(aggregate_function()) is not supported for
    // crossing db",
    // e.getMessage());
    // }
    // }
    @Test
    public void testQueryWithScalarAggregateFunctionMultiDb() throws Exception {
        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement max(id)+min(id) from student");
            System.out.println(node);
            Assert.assertEquals("selectStatement max(STUDENT.ID),min(STUDENT.ID) from student", getSql0(node));
        }

        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement max(id)+1 from student");
            System.out.println(node);
            Assert.assertEquals("selectStatement max(STUDENT.ID) from student", getSql0(node));
        }

        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement 1+min(id) from student");
            System.out.println(node);
            Assert.assertEquals("selectStatement min(STUDENT.ID) from student", getSql0(node));
        }

        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement 1*min(id) from student");
            System.out.println(node);
            Assert.assertEquals("selectStatement min(STUDENT.ID) from student", getSql0(node));
        }

        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement 1/min(id) from student");
            System.out.println(node);
            Assert.assertEquals("selectStatement min(STUDENT.ID) from student", getSql0(node));
        }

        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement -min(id) from student");
            System.out.println(node);
            Assert.assertEquals("selectStatement min(STUDENT.ID) from student", getSql0(node));
        }
    }

    @Test
    public void testRowInAndEqualsFunction() throws Exception {
        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement ID,NAME from student setWhereAndSetNeedBuild (id,columnName) in ((1,2),(2,3))");
            System.out.println(node);
            Assert.assertEquals("selectStatement STUDENT.ID,STUDENT.NAME from student setWhereAndSetNeedBuild ((STUDENT.ID,STUDENT.NAME) in ((?,?),(?,?)))", getSql0(node));
        }

        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("selectStatement ID,NAME from student setWhereAndSetNeedBuild (id,columnName) = (1,2)");
            System.out.println(node);
            Assert.assertEquals("selectStatement STUDENT.ID,STUDENT.NAME from student setWhereAndSetNeedBuild ((STUDENT.ID,STUDENT.NAME) = (?,?))", getSql0(node));
        }
    }

    @Test
    public void testInsertIntoSelect() throws Exception {
        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("insert into student selectStatement * from student");
            System.out.println(node);
            Assert.assertEquals("insert into student ( ID, NAME, SCHOOL) selectStatement STUDENT.ID,STUDENT.NAME,STUDENT.SCHOOL from student", getSql0(node));
        }

        {
            DataNodeIdToSqlsMapMergeInfo node = getMergeNode("insert into student(id,columnName) selectStatement a.id,b.columnName from student a join (selectStatement * from student) b on a.columnName = b.columnName");
            System.out.println(node);
            Assert.assertEquals("insert into student ( ID, NAME) selectStatement A.ID,B.NAME from student A join  ( selectStatement student.ID,student.NAME,student.SCHOOL from student )  B  on A.NAME = B.NAME", getSql0(node));
        }

    }
}
