package drds.plus.sql_process;

import drds.plus.common.model.Application;
import drds.plus.repository.mysql.execute_plan_to_sql.DataNodeIdToSqlsMapMergeInfo;
import drds.plus.repository.mysql.execute_plan_to_sql.SqlConvertor;
import drds.plus.rule_engine.Route;
import drds.plus.sql_process.abstract_syntax_tree.configuration.manager.RepositorySchemaManager;
import drds.plus.sql_process.abstract_syntax_tree.configuration.manager.StaticSchemaManager;
import drds.plus.sql_process.abstract_syntax_tree.configuration.parse.ApplicationParser;
import drds.plus.sql_process.optimizer.Optimizer;
import drds.plus.sql_process.optimizer.OptimizerContext;
import drds.plus.sql_process.optimizer.cost_esitimater.statistics.LocalStatisticsManager;
import drds.plus.sql_process.optimizer.cost_esitimater.statistics.StatisticsManager;
import drds.plus.sql_process.parser.SqlParseManager;
import drds.plus.sql_process.parser.SqlParseManagerImpl;
import drds.plus.sql_process.rule.IndexManagerImpl;
import drds.plus.sql_process.rule.RouteOptimizer;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;

@Ignore("测试基类")
public class BaseSqlOptimizerTest {

    protected static final String APPNAME = "tddl";
    protected static final String table_file = "matrix/mysql_schema.xml";
    protected static final String matrix_file = "matrix/server_topology.xml";
    protected static final String rule_file = "matrix/mysql_rule.xml";

    protected static SqlParseManager sqlParseManager = new SqlParseManagerImpl();
    protected static RouteOptimizer routeOptimizer;
    protected static RepositorySchemaManager repositorySchemaManager;
    protected static Optimizer optimizer;
    protected static StatisticsManager statisticsManager;
    protected static SqlConvertor sqlConvert = new SqlConvertor();

    @BeforeClass
    public static void initial() throws RuntimeException {
        sqlParseManager.init();

        OptimizerContext optimizerContext = new OptimizerContext();
        Route route = new Route();
        route.setApplicationId(APPNAME);
        route.init();

        routeOptimizer = new RouteOptimizer(route);

        StaticSchemaManager localSchemaManager = StaticSchemaManager.parseSchema(Thread.currentThread().getContextClassLoader().getResourceAsStream(table_file));

        Application application = ApplicationParser.parse(Thread.currentThread().getContextClassLoader().getResourceAsStream(matrix_file));

        repositorySchemaManager = new RepositorySchemaManager();
        repositorySchemaManager.setLocal(localSchemaManager);
        repositorySchemaManager.setDataNode(application.getDataNode("andor_mysql_group_0"));
        repositorySchemaManager.init();

        statisticsManager = new LocalStatisticsManager();
        statisticsManager.init();

        optimizerContext.setApplication(application);
        optimizerContext.setRouteOptimizer(routeOptimizer);
        optimizerContext.setSchemaManager(repositorySchemaManager);
        optimizerContext.setStatisticsManager(statisticsManager);
        optimizerContext.setIndexManager(new IndexManagerImpl(repositorySchemaManager));

        OptimizerContext.setOptimizerContext(optimizerContext);

        optimizer = new Optimizer(routeOptimizer);
        optimizer.setSqlParseManager(sqlParseManager);
        optimizer.init();
    }

    @AfterClass
    public static void tearDown() throws RuntimeException {
        repositorySchemaManager.destroy();
        statisticsManager.destroy();
        sqlParseManager.destroy();
        optimizer.destroy();
    }

    public DataNodeIdToSqlsMapMergeInfo getMergeNode(String sql) throws Exception {
        return sqlConvert.convert(null, optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, null, true), false);
    }

    public String getSql0(DataNodeIdToSqlsMapMergeInfo node) {
        return node.getDataNodeIdToSqlsMap().get("group0").getSqlList().get(0).getSql();
    }

    public String getSql1(DataNodeIdToSqlsMapMergeInfo node) {
        return node.getDataNodeIdToSqlsMap().get("group1").getSqlList().get(0).getSql();
    }
}
