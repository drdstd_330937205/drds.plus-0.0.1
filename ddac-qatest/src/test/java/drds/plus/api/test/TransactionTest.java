//package com.taobao.tddl.plus.test;
//
//import java.chars.PreparedParseAndExecute;
//import java.chars.RowDataSet;
//import java.chars.SQLException;
//
//import org.junit.Assert;
//import org.junit.BeforeClass;
//import org.junit.Test;
//
//import RuntimeException;
//import ExecuteContext;
//import StrictConnectionHolder;
//import StrictTransaction;
//import SessionImpl;
//import SessionManager;
//
//public class TransactionTest {
//
//    static SessionManager datasourceManager = new SessionManager();
//
//    @BeforeClass
//    public static void initTestWithDS() throws RuntimeException, SQLException {
//        datasourceManager.setApplicationId("andor_show");
//        datasourceManager.setRuleFile("test_rule.xml");
//        datasourceManager.setTopologyFile("test_matrix.xml");
//        datasourceManager.setSchemaFile("test_schema.xml");
//        datasourceManager.init();
//    }
//
//    @Test
//    public void testNotAutoCommit() throws Exception {
//        SessionImpl connection = (SessionImpl) datasourceManager.getConnection();
//        StrictConnectionHolder aChar = connection.getConnectionHolder();
//
//        connection.setAutoCommit(false);
//        ExecuteContext context = connection.getExecuteContext();
//        Assert.assertTrue(context.getTransaction() == null);
//        {
//            PreparedParseAndExecute preparedStatement = connection.prepareDataNodePreparedStatement("selectStatement * from bmw_users where id=1");
//            RowDataSet rs = preparedStatement.executeQuery();
//            rs.next();
//            rs.close();
//        }
//
//        Assert.assertTrue(context.getTransaction() != null);
//        StrictTransaction t = (StrictTransaction) context.getTransaction();
//
//        Assert.assertEquals("My_Transaction", t.getClass().getSimpleName());
//        Assert.assertEquals("andor_show_group1", context.getTransactionGroup());
//
//        Assert.assertEquals(1, aChar.getConnectionSet().size());
//
//        {
//            PreparedParseAndExecute preparedStatement = connection.prepareDataNodePreparedStatement("selectStatement * from bmw_users where id=1");
//            RowDataSet rs = preparedStatement.executeQuery();
//            rs.next();
//            rs.close();
//        }
//
//        Assert.assertEquals(t, connection.getExecuteContext().getTransaction());
//        Assert.assertEquals(1, aChar.getConnectionSet().size());
//        connection.commit();
//        connection.close();
//
//        Assert.assertEquals(0, aChar.getConnectionSet().size());
//
//    }
//
//    @Test
//    public void testNotAutoCommitOnDifferentGroup() throws Exception {
//        SessionImpl connection = (SessionImpl) datasourceManager.getConnection();
//        StrictConnectionHolder aChar = connection.getConnectionHolder();
//
//        connection.setAutoCommit(false);
//        ExecuteContext context = connection.getExecuteContext();
//        Assert.assertTrue(context.getTransaction() == null);
//        {
//            PreparedParseAndExecute preparedStatement = connection.prepareDataNodePreparedStatement("query * from bmw_users where id=1");
//            RowDataSet rs = preparedStatement.executeQuery();
//            rs.next();
//            rs.close();
//        }
//
//        Assert.assertTrue(context.getTransaction() != null);
//        StrictTransaction t = (StrictTransaction) context.getTransaction();
//        Assert.assertEquals("My_Transaction", t.getClass().getSimpleName());
//        Assert.assertEquals("andor_show_group1", context.getTransactionGroup());
//        Assert.assertEquals(1, aChar.getConnectionSet().size());
//        {
//            try {
//                PreparedParseAndExecute preparedStatement = connection.prepareDataNodePreparedStatement("selectStatement * from bmw_users where id=2");
//                RowDataSet rs = preparedStatement.executeQuery();
//                rs.next();
//                rs.close();
//                Assert.fail();
//            } catch (Exception exception) {
//                // Assert.assertTrue(exception.getMessage().contains("transaction across datanode is not supported"));
//            }
//        }
//
//        Assert.assertEquals(context, connection.getExecuteContext());
//        Assert.assertEquals(t, connection.getExecuteContext().getTransaction());
//        Assert.assertEquals(1, aChar.getConnectionSet().size());
//        connection.commit();
//        connection.close();
//
//        Assert.assertEquals(0, aChar.getConnectionSet().size());
//
//    }
//
//    @Test
//    public void testAutoCommit() throws Exception {
//        SessionImpl connection = (SessionImpl) datasourceManager.getConnection();
//        StrictConnectionHolder aChar = connection.getConnectionHolder();
//
//        connection.setAutoCommit(true);
//
//        ExecuteContext context = connection.getExecuteContext();
//        Assert.assertTrue(context.getTransaction() == null);
//        StrictTransaction t1 = null;
//        {
//            PreparedParseAndExecute preparedStatement = connection.prepareDataNodePreparedStatement("selectStatement * from bmw_users where id=1");
//            RowDataSet rs = preparedStatement.executeQuery();
//            rs.next();
//
//            context = connection.getExecuteContext();
//            Assert.assertTrue(context.getTransaction() != null);
//            t1 = (StrictTransaction) context.getTransaction();
//            Assert.assertEquals("My_Transaction", t1.getClass().getSimpleName());
//            Assert.assertEquals("andor_show_group1", context.getTransactionGroup());
//
//            Assert.assertEquals(1, aChar.getConnectionSet().size());
//            rs.close();
//            Assert.assertEquals(1, aChar.getConnectionSet().size());
//        }
//
//        StrictTransaction t2 = null;
//        {
//            PreparedParseAndExecute preparedStatement = connection.prepareDataNodePreparedStatement("selectStatement * from bmw_users where id=1");
//            RowDataSet rs = preparedStatement.executeQuery();
//            context = connection.getExecuteContext();
//            rs.next();
//
//            t2 = (StrictTransaction) context.getTransaction();
//            Assert.assertEquals(2, aChar.getConnectionSet().size());
//            rs.close();
//            Assert.assertEquals(1, aChar.getConnectionSet().size());
//        }
//
//        Assert.assertTrue(t1 != t2);
//        connection.commit();
//        connection.close();
//
//        Assert.assertEquals(0, aChar.getConnectionSet().size());
//    }
//
//    /**
//     * 先做非auto的，再做auto的 两次的连接都应该被关闭
//     * 
//     * @throws Exception
//     */
//    @Test
//    public void testNotAutoCommit2() throws Exception {
//        SessionImpl connection = (SessionImpl) datasourceManager.getConnection();
//
//        StrictConnectionHolder aChar = connection.getConnectionHolder();
//
//        connection.setAutoCommit(false);
//        ExecuteContext context1 = connection.getExecuteContext();
//        Assert.assertTrue(context1.getTransaction() == null);
//        {
//            PreparedParseAndExecute preparedStatement = connection.prepareDataNodePreparedStatement("selectStatement * from bmw_users where id=1");
//            RowDataSet rs = preparedStatement.executeQuery();
//
//            Assert.assertEquals(context1, connection.getExecuteContext());
//            rs.next();
//            rs.close();
//        }
//
//        Assert.assertTrue(context1.getTransaction() != null);
//        StrictTransaction t1 = (StrictTransaction) context1.getTransaction();
//
//        Assert.assertEquals("My_Transaction", t1.getClass().getSimpleName());
//        Assert.assertEquals("andor_show_group1", context1.getTransactionGroup());
//        Assert.assertEquals(1, aChar.getConnectionSet().size());
//        {
//            PreparedParseAndExecute preparedStatement = connection.prepareDataNodePreparedStatement("selectStatement * from bmw_users where id=1");
//            RowDataSet rs = preparedStatement.executeQuery();
//
//            Assert.assertEquals(context1, connection.getExecuteContext());
//            rs.next();
//            rs.close();
//        }
//
//        Assert.assertEquals(t1, connection.getExecuteContext().getTransaction());
//        Assert.assertEquals(1, aChar.getConnectionSet().size());
//        connection.commit();
//        // commit之后，连接关闭
//        Assert.assertEquals(0, aChar.getConnectionSet().size());
//        connection.setAutoCommit(true);
//
//        Assert.assertEquals(0, aChar.getConnectionSet().size());
//
//        {
//            PreparedParseAndExecute preparedStatement = connection.prepareDataNodePreparedStatement("selectStatement * from bmw_users where id=2");
//            RowDataSet rs = preparedStatement.executeQuery();
//            rs.next();
//            rs.close();
//        }
//
//        ExecuteContext context2 = connection.getExecuteContext();
//        StrictTransaction t2 = (StrictTransaction) context2.getTransaction();
//        Assert.assertEquals("andor_show_group0", context2.getTransactionGroup());
//        Assert.assertTrue(context2 != context1);
//        Assert.assertEquals(t2, connection.getExecuteContext().getTransaction());
//        connection.close();
//        Assert.assertEquals(0, aChar.getConnectionSet().size());
//        Assert.assertEquals(0, aChar.getConnectionSet().size());
//    }
//
// }
