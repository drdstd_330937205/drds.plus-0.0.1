package drds.plus.api.test;

import drds.plus.executor.data_node_executor.DataNodeExecutorContext;
import drds.plus.executor.data_node_executor.spi.DataNodeExecutorManager;
import drds.plus.rule_engine.Route;
import drds.plus.sql_process.abstract_syntax_tree.configuration.manager.StaticSchemaManager;
import drds.plus.sql_process.rule.RouteOptimizer;
import drds.plus.sql_process.rule.RuleSchemaManager;
import org.junit.Assert;
import org.junit.Test;

public class SchemaManagerTest {

    @Test
    public void initTestStaticSchemaManager() throws RuntimeException {
        DataNodeExecutorContext dataNodeExecutorContext = new DataNodeExecutorContext();
        DataNodeExecutorContext.setExecutorContext(dataNodeExecutorContext);

        StaticSchemaManager s = new StaticSchemaManager(null);
        s.init();

        Assert.assertTrue(s.getTableMetaData("BMW_USERS") != null);
        Assert.assertEquals(9, s.getAllTables().size());
    }

    @Test
    public void initTestStaticSchemaManagerWithSchemaFile() throws RuntimeException {
        DataNodeExecutorContext dataNodeExecutorContext = new DataNodeExecutorContext();
        DataNodeExecutorContext.setExecutorContext(dataNodeExecutorContext);
        StaticSchemaManager s = new StaticSchemaManager("test_schema.xml");
        s.init();

        // Assert.assertTrue(s.getTableStatistics("BMW_USERS") != null);
        Assert.assertEquals(1, s.getAllTables().size());
    }

    @Test
    public void initTestRuleSchemaManager() throws RuntimeException {
        DataNodeExecutorContext dataNodeExecutorContext = new DataNodeExecutorContext();
        DataNodeExecutorContext.setExecutorContext(dataNodeExecutorContext);
        DataNodeExecutorManager topology = new DataNodeExecutorManager("andor_show");
        dataNodeExecutorContext.setDataNodeExecutorManager(topology);

        topology.init();
        Route route = new Route();
        route.setApplicationId("andor_show");
        route.init();

        RouteOptimizer routeOptimizer = new RouteOptimizer(route);
        RuleSchemaManager s = new RuleSchemaManager(routeOptimizer, topology.getApplication());
        s.init();
        // Assert.assertTrue(s.getTableStatistics("BMW_USERS") != null);
    }
}
