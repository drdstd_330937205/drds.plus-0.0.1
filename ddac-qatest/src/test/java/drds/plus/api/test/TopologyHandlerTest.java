package drds.plus.api.test;

import drds.plus.executor.data_node_executor.DataNodeExecutorContext;
import drds.plus.executor.data_node_executor.spi.DataNodeExecutorManager;
import org.junit.Test;

public class TopologyHandlerTest {

    @Test
    public void initTestWithTopologyFileHasAllConfig() {
        DataNodeExecutorContext dataNodeExecutorContext = new DataNodeExecutorContext();
        DataNodeExecutorContext.setExecutorContext(dataNodeExecutorContext);
        DataNodeExecutorManager topology = new DataNodeExecutorManager("andor_show");
        try {
            topology.init();
        } catch (RuntimeException e) {

        }

    }

    @Test
    public void initTestWithTopologyFileHasNoGroupConfig() {
        DataNodeExecutorContext dataNodeExecutorContext = new DataNodeExecutorContext();
        DataNodeExecutorContext.setExecutorContext(dataNodeExecutorContext);
        DataNodeExecutorManager topology = new DataNodeExecutorManager("andor_show");
        try {
            topology.init();
        } catch (RuntimeException e) {

        }

    }

    @Test
    public void initTestWithAppName() {
        DataNodeExecutorContext dataNodeExecutorContext = new DataNodeExecutorContext();
        DataNodeExecutorContext.setExecutorContext(dataNodeExecutorContext);
        DataNodeExecutorManager topology = new DataNodeExecutorManager("andor_show", null);
        try {
            topology.init();
        } catch (RuntimeException e) {

        }
    }
}
