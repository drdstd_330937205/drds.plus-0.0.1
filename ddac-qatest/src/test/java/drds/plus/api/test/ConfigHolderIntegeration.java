package drds.plus.api.test;

import drds.plus.api.ConfigHolder;
import drds.plus.executor.ExecuteContext;
import drds.plus.executor.Executor;
import drds.plus.executor.cursor.cursor.impl.result_cursor.ResultCursor;
import drds.plus.executor.row_values.RowValues;
import org.junit.Ignore;
import org.junit.Test;

import java.util.concurrent.Executors;

@Ignore("实际业务用例")
public class ConfigHolderIntegeration {

    @Test
    public void initTestWithConfigHolder() throws RuntimeException {
        ConfigHolder configHolder = new ConfigHolder();
        configHolder.setApplicationId("DEV_SUBWAY_MYSQL");
        // configHolder.setTopologyFilePath("test_matrix.xml");
        // configHolder.setSchemaFilePath("test_schema.xml");

        Executor me = new Executor();

        ExecuteContext context = new ExecuteContext();
        context.setExecutorService(Executors.newCachedThreadPool());
        // ResultCursor rc = me.execute("query * from
        // bmw_users limit 10",
        // context);
        ResultCursor rc = me.execute(context, "query * from lunaadgroup limit 10");
        RowValues row = null;
        while ((row = rc.next()) != null) {
            System.out.println(row);
        }

        System.out.println("ok");
    }

}
