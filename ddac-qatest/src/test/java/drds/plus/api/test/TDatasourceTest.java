package drds.plus.api.test;

import drds.plus.api.SessionManager;
import org.junit.Assert;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TDatasourceTest {

    @Test
    public void initTestWithDS() throws RuntimeException, SQLException {

        SessionManager ds = new SessionManager();
        ds.setApplicationId("andor_show");

        ds.init();

        Connection conn = null;// sessionManager.getConnection();
        PreparedStatement ps = conn.prepareStatement("select * from bmw_users limit 10");
        ResultSet rs = ps.executeQuery();

        Assert.assertTrue(rs.next());

        rs.close();
        ps.close();
        conn.close();
    }
}
