package drds.plus.hint.test;

import drds.plus.api.ConfigHolder;
import drds.plus.executor.ExecuteContext;
import drds.plus.executor.Executor;
import drds.plus.executor.cursor.cursor.impl.result_cursor.ResultCursor;
import drds.plus.executor.row_values.RowValues;
import drds.plus.sql_process.abstract_syntax_tree.execute_plan.ExecutePlanImpl;
import drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.MergeQueryImpl;
import drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.QueryWithIndexImpl;
import org.junit.Test;

import java.util.concurrent.Executors;

public class HintExecuteTest {

    @Test
    public void initTestWithConfigHolder() throws RuntimeException {
        ConfigHolder configHolder = new ConfigHolder();
        configHolder.setApplicationId("andor_show");

        Executor executor = new Executor();

        ExecuteContext context = new ExecuteContext();
        context.setExecutorService(Executors.newCachedThreadPool());

        MergeQueryImpl mergeImpl = new MergeQueryImpl();
        ExecutePlanImpl sub1 = new QueryWithIndexImpl();
        sub1.setSql("selectStatement * from bmw_users_0003 limit 10");
        sub1.setDataNodeId("andor_show_group0");

        ExecutePlanImpl sub2 = new QueryWithIndexImpl();
        sub2.setSql("selectStatement * from bmw_users_0001 limit 10");
        sub2.setDataNodeId("andor_show_group0");

        mergeImpl.addExecutePlan(sub1);
        mergeImpl.addExecutePlan(sub2);
        mergeImpl.setSql("chars");
        mergeImpl.setDataNodeId("andor_show_group0");

        ResultCursor rc = executor.execute(context, mergeImpl);
        RowValues row = null;
        while ((row = rc.next()) != null) {
            System.out.println(row);
        }

        System.out.println("ok");
    }
}
