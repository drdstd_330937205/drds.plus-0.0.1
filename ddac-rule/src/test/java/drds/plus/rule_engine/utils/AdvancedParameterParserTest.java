package drds.plus.rule_engine.utils;

import drds.plus.rule_engine.rule.Range;
import drds.plus.rule_engine.rule.RangeRuleColumn;
import org.junit.Assert;
import org.junit.Test;

public class AdvancedParameterParserTest {

    @Test
    public void test_正常() {
        String param = "id,1_number,1024";
        RangeRuleColumn result = AdvancedParameterParser.getAdvancedParamByParamTokenNew(param, false);
        testResult(result, RangeRuleColumn.AtomIncreaseType.NUMBER, 1, 1024);

        param = "id,1024";
        result = AdvancedParameterParser.getAdvancedParamByParamTokenNew(param, false);
        testResult(result, RangeRuleColumn.AtomIncreaseType.NUMBER, 1, 1024);
    }

    @Test
    public void test_范围() {
        String param = "id,1_number,0_1024|1m_1g";
        RangeRuleColumn result = AdvancedParameterParser.getAdvancedParamByParamTokenNew(param, false);
        testResult(result, RangeRuleColumn.AtomIncreaseType.NUMBER, new Range[]{getRange(0, 1024), getRange(1 * 1000000, 1 * 1000000000)}, 1);

        param = "id,0_1024|1m_1g";
        result = AdvancedParameterParser.getAdvancedParamByParamTokenNew(param, false);
        testResult(result, RangeRuleColumn.AtomIncreaseType.NUMBER, new Range[]{getRange(0, 1024), getRange(1 * 1000000, 1 * 1000000000)}, 1);
    }

    private void testResult(RangeRuleColumn result, RangeRuleColumn.AtomIncreaseType type, Comparable atomicIncreateValue, Integer cumulativeTimes) {
        Assert.assertEquals(result.atomicIncreateType, type);
        Assert.assertEquals(result.atomicIncreateValue, atomicIncreateValue);
        Assert.assertEquals(result.cumulativeTimes, cumulativeTimes);
    }

    private void testResult(RangeRuleColumn result, RangeRuleColumn.AtomIncreaseType type, Range[] rangeValue, Integer atomicIncreateValue) {
        Assert.assertEquals(result.atomicIncreateType, type);
        Assert.assertEquals(result.atomicIncreateValue, atomicIncreateValue);
        int i = 0;
        for (Range range : result.rangeArray) {
            Assert.assertEquals(range.start, rangeValue[i].start);
            Assert.assertEquals(range.end, rangeValue[i].end);
            i++;
        }
    }

    private Range getRange(int start, int end) {
        return new Range(start, end);
    }

}
