package drds.plus.rule_engine;

import drds.plus.common.model.comparative.And;
import drds.plus.common.model.comparative.Comparative;
import drds.plus.common.model.comparative.Or;
import org.junit.Ignore;

@Ignore
public class BaseRuleTest {

    protected Comparative or(Comparable... values) {
        Or and = new Or();
        for (Comparable obj : values) {
            and.addComparative(new Comparative(Comparative.equal, obj));
        }
        return and;
    }

    protected Comparative and(Comparative... values) {
        And and = new And();
        for (Comparative obj : values) {
            and.addComparative(obj);
        }
        return and;
    }

}
