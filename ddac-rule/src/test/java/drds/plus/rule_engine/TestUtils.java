package drds.plus.rule_engine;

import drds.plus.common.model.comparative.And;
import drds.plus.common.model.comparative.Comparative;
import drds.plus.common.model.comparative.Or;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestUtils {

    public static final int GreaterThan = Comparative.greater_than;
    public static final int GreaterThanOrEqual = Comparative.greater_than_or_equal;
    public static final int Equivalent = Comparative.equal;
    public static final int NotEquivalent = Comparative.not_equal;
    public static final int LessThan = Comparative.less_than;
    public static final int LessThanOrEqual = Comparative.less_than_or_equal;

    public static Comparative gor(Comparative parent, Comparative target) {
        if (parent == null) {
            Or or = new Or();
            or.addComparative(target);
            return or;
        } else {
            if (parent instanceof Or) {
                ((Or) parent).addComparative(target);
                return parent;
            } else {
                Or or = new Or();
                or.addComparative(parent);
                or.addComparative(target);
                return or;
            }
        }
    }

    public static Comparative gand(Comparative parent, Comparative target) {
        if (parent == null) {

            And and = new And();
            and.addComparative(target);
            return and;
        } else {
            if (parent instanceof And) {

                And and = ((And) parent);
                if (and.getComparativeList().size() == 1) {
                    and.addComparative(target);
                    return and;
                } else {
                    And andNew = new And();
                    andNew.addComparative(and);
                    andNew.addComparative(target);
                    return andNew;
                }

            } else {
                And and = new And();
                and.addComparative(parent);
                and.addComparative(target);
                return and;
            }
        }
    }

    public static Comparative gcomp(Comparable comp, int sym) {
        return new Comparative(sym, comp);
    }

    public static void testSet(Object[] target, Set<? extends Object> beTestedSet) {
        assertEquals(target.length, beTestedSet.size());
        int index = 0;

        for (Object obj : target) {
            assertTrue("index:" + String.valueOf(index) + "-value:" + obj + "|set:" + beTestedSet, beTestedSet.contains(obj));
            index++;
        }
    }

    public static void testSetDate(Date[] target, Set<Object> beTestedSet) {
        assertEquals(target.length, beTestedSet.size());
        Set<String> dateStr = new HashSet<String>();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
        for (Object date : beTestedSet) {
            String formated = format.format(((Date) date));
            dateStr.add(formated);
        }
        Set<String> targetDateString = new HashSet<String>();
        for (Date date : target) {
            String formated = format.format(((Date) date));
            targetDateString.add(formated);
        }
        int index = 0;

        Iterator<String> strs = dateStr.iterator();
        while (strs.hasNext()) {

            boolean isTrue = false;
            StringBuilder sb = new StringBuilder();

            String str = strs.next();
            sb.append(str).append("|");
            for (String obj : targetDateString) {

                if (str.trim().equals(obj.trim())) {
                    strs.remove();
                    isTrue = true;
                }
            }

            assertTrue("index:" + String.valueOf(index) + "-value:" + targetDateString + " target:" + sb.toString(), isTrue);
            index++;

        }
    }
}
