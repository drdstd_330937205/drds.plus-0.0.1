package drds.plus.rule_engine;

import com.google.common.collect.Lists;
import drds.plus.rule_engine.rule_calculate.DataNodeDataScatterInfo;
import drds.plus.rule_engine.rule_calculate.RuleCalculateDiffrentException;
import drds.plus.rule_engine.rule_calculate.RuleCalculateResult;
import drds.plus.rule_engine.utils.ComparativeStringAnalyser;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class RemoteRuleIntegration extends BaseRuleTest {

    static Route route;

    @BeforeClass
    public static void setUp() {

    }

    @AfterClass
    public static void tearDown() {
        route.destroy();
    }

    @Test
    public void testTddlRuleMuRulesAndCompare_Select() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String conditionStr = "message_id >=25:int and message_id<=26:int;gmt_create>=" + sdf.format(new Date()) + ":date";

        try {
            List<DataNodeDataScatterInfo> db = null;
            RuleCalculateResult result = route.routes(true, "nserch", new Choicer(ComparativeStringAnalyser.decodeComparativeString2Map(conditionStr)), Lists.newArrayList());
            db = result.getDataNodeDataScatterInfoList();
            for (DataNodeDataScatterInfo database : db) {
                StringBuilder sb = new StringBuilder("目标库:");
                sb.append(database.getDataNodeId());
                sb.append(" 所要执行的表:");
                for (String table : database.getTableNameSet()) {
                    sb.append(table);
                    sb.append(" ");
                }
                System.out.println(sb.toString());
            }
        } catch (RuleCalculateDiffrentException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void testTddlRuleMuRulesAndCompare_Insert() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String conditionStr = "message_id >=25:int and message_id<=26:int;gmt_create>=" + sdf.format(new Date()) + ":date";

        try {
            List<DataNodeDataScatterInfo> db = null;
            RuleCalculateResult result = route.routes(true, "nserch", new Choicer(ComparativeStringAnalyser.decodeComparativeString2Map(conditionStr)), Lists.newArrayList());
            db = result.getDataNodeDataScatterInfoList();
            for (DataNodeDataScatterInfo database : db) {
                StringBuilder sb = new StringBuilder("目标库:");
                sb.append(database.getDataNodeId());
                sb.append(" 所要执行的表:");
                for (String table : database.getTableNameSet()) {
                    sb.append(table);
                    sb.append(" ");
                }
                System.out.println(sb.toString());
            }
        } catch (RuleCalculateDiffrentException e) {
            e.printStackTrace();
        }
    }
}
