package drds.plus.rule_engine;

import com.google.common.collect.Lists;
import drds.plus.common.model.comparative.Comparative;
import drds.plus.rule_engine.rule_calculate.DataNodeDataScatterInfo;
import drds.plus.rule_engine.rule_calculate.RuleCalculateDiffrentException;
import drds.plus.rule_engine.rule_calculate.RuleCalculateResult;
import drds.plus.rule_engine.utils.MatchResultCompare;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 一些常见的本地rule测试
 */
public class LocalRuleTest {

    static Route rule;
    static Route mvrRule;

    @BeforeClass
    public static void setUp() {

    }

    @AfterClass
    public static void tearDown() {
        rule.destroy();
        mvrRule.destroy();
    }

    @Test
    public void testRule_equals() {
        RuleCalculateResult result = rule.routes("nserch", "message_id = 1");
        List<DataNodeDataScatterInfo> dbs = result.getDataNodeDataScatterInfoList();
        Assert.assertEquals(1, dbs.size());
        Assert.assertEquals("NSEARCH_GROUP_2", dbs.get(0).getDataNodeId());
        Assert.assertEquals(1, dbs.get(0).getTableNameSet().size());
        Assert.assertEquals("nserch_1", dbs.get(0).getTableNameSet().iterator().next());
    }

    @Test
    public void testStringRule_equals() {
        System.out.println("ljh".hashCode() % 24);
        RuleCalculateResult result = rule.routes("string", "id = ljh:s");
        List<DataNodeDataScatterInfo> dbs = result.getDataNodeDataScatterInfoList();
        Assert.assertEquals(1, dbs.size());
        Assert.assertEquals("STRING_1", dbs.get(0).getDataNodeId());
        Assert.assertEquals(1, dbs.get(0).getTableNameSet().size());
        Assert.assertEquals("string_18", dbs.get(0).getTableNameSet().iterator().next());
    }

    @Test
    public void testRule_in() {
        String conditionStr = "message_id in (996,997,998,999,1000,1001,1002,1003,1004):int";
        RuleCalculateResult result = rule.routes("nserch", conditionStr);
        List<DataNodeDataScatterInfo> dbs = result.getDataNodeDataScatterInfoList();
        Assert.assertEquals(3, dbs.size());
        StringBuilder sb = new StringBuilder("目标库:");
        sb.append(dbs.get(0).getDataNodeId());
        sb.append(" 所要执行的表:");
        for (String table : dbs.get(0).getTableNameSet()) {
            sb.append(table);
            sb.append(" ");
        }

        Assert.assertEquals("目标库:NSEARCH_GROUP_1 所要执行的表:nserch_18 nserch_15 nserch_12 ", sb.toString());
    }


    public void testRoute_noVersion() {
        // 不存在该version版本
        rule.routes("nserch", "message_id = 1", "V1");
    }

    @Test
    public void testRule_Trim() {
        String conditionStr = "message_id in (996 ,997 , 998,999,1000 ,1001, 1002,1003,1004):int";
        RuleCalculateResult result = rule.routes("nserch", conditionStr);

        List<DataNodeDataScatterInfo> dbs = result.getDataNodeDataScatterInfoList();
        Assert.assertEquals(3, dbs.size());
        StringBuilder sb = new StringBuilder("目标库:");
        sb.append(dbs.get(0).getDataNodeId());
        sb.append(" 所要执行的表:");
        for (String table : dbs.get(0).getTableNameSet()) {
            sb.append(table);
            sb.append(" ");
        }

        Assert.assertEquals("目标库:NSEARCH_GROUP_1 所要执行的表:nserch_18 nserch_15 nserch_12 ", sb.toString());
    }

    @Test
    public void testRule_date() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String conditionStr = "message_id >24:int and message_id<=26:int;gmt_create>=" + sdf.format(new Date()) + ":date";
        RuleCalculateResult result = rule.routes("nserch", conditionStr);
        Assert.assertEquals(true, MatchResultCompare.oriDbTabCompareWithMatchResult(result, "NSEARCH_GROUP_2", "nserch_1"));
    }

    @Test
    public void testRule_FullTableScan() {
        String conditionStr = "";
        RuleCalculateResult result = rule.routes("nserch", conditionStr);
        List<DataNodeDataScatterInfo> dbs = result.getDataNodeDataScatterInfoList();
        StringBuilder sb = new StringBuilder("目标库:");
        sb.append(dbs.get(0).getDataNodeId());
        sb.append(" 所要执行的表:");
        for (String table : dbs.get(0).getTableNameSet()) {
            sb.append(table);
            sb.append(" ");
        }
        System.out.println(sb.toString());

        StringBuilder sb2 = new StringBuilder("目标库:");
        sb2.append(dbs.get(1).getDataNodeId());
        sb2.append(" 所要执行的表:");
        for (String table : dbs.get(1).getTableNameSet()) {
            sb2.append(table);
            sb2.append(" ");
        }
        System.out.println(sb2.toString());

        StringBuilder sb3 = new StringBuilder("目标库:");
        sb3.append(dbs.get(2).getDataNodeId());
        sb3.append(" 所要执行的表:");
        for (String table : dbs.get(2).getTableNameSet()) {
            sb3.append(table);
            sb3.append(" ");
        }
        System.out.println(sb3.toString());
    }

    @Test
    public void testRouteWithSpecifyRuleVersion() {
        RuleCalculateResult result = mvrRule.routes("nserch", "message_id = 1", "V1");
        List<DataNodeDataScatterInfo> dbs = result.getDataNodeDataScatterInfoList();
        Assert.assertEquals(1, dbs.size());
        Assert.assertEquals("NSEARCH_GROUP_2", dbs.get(0).getDataNodeId());
        Assert.assertEquals(1, dbs.get(0).getTableNameSet().size());
        Assert.assertEquals("nserch_1", dbs.get(0).getTableNameSet().iterator().next());
    }

    @Test
    public void testRouteMultiVersionAndCompareTSqlTypeStringString() {
        Choicer choicer = new Choicer();
        choicer.addComparative("MESSAGE_ID", new Comparative(Comparative.equal, 1)); // 一定要大写
        RuleCalculateResult result = null;
        try {
            result = mvrRule.routes(false, "nserch", choicer, Lists.newArrayList());
        } catch (RuleCalculateDiffrentException e) {
            e.printStackTrace();
        }

        List<DataNodeDataScatterInfo> dbs = result.getDataNodeDataScatterInfoList();
        Assert.assertEquals(1, dbs.size());
        Assert.assertEquals("NSEARCH_GROUP_2", dbs.get(0).getDataNodeId());
        Assert.assertEquals(1, dbs.get(0).getTableNameSet().size());
        Assert.assertEquals("nserch_1", dbs.get(0).getTableNameSet().iterator().next());
    }
}
