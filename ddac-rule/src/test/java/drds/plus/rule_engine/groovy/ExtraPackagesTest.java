package drds.plus.rule_engine.groovy;

import drds.plus.rule_engine.Route;
import drds.plus.rule_engine.rule_calculate.DataNodeDataScatterInfo;
import drds.plus.rule_engine.rule_calculate.RuleCalculateResult;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

/**
 * @author <a href="junyu@taobao.com">junyu</a>
 * @version 1.0
 * @description
 * @date 2011-8-11 11:19:02
 * @since 1.6
 */
public class ExtraPackagesTest {

    static Route route;

    @BeforeClass
    public static void setUp() {

    }

    @Test
    public void testTddlRule() {
        String conditionStr = "message_id in (996,997,998,999,1000,1001,1002,1003,1004):int";
        RuleCalculateResult result = route.routes("nserch", conditionStr);
        List<DataNodeDataScatterInfo> dbs = result.getDataNodeDataScatterInfoList();
        Assert.assertEquals(3, dbs.size());
        StringBuilder sb = new StringBuilder("目标库:");
        sb.append(dbs.get(0).getDataNodeId());
        sb.append(" 所要执行的表:");
        for (String table : dbs.get(0).getTableNameSet()) {
            sb.append(table);
            sb.append(" ");
        }

        Assert.assertEquals("目标库:NSEARCH_GROUP_1 所要执行的表:nserch_6 nserch_0 nserch_12 ", sb.toString());
    }
}
