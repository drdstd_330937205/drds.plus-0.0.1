package drds.plus.rule_engine;

import drds.plus.common.model.comparative.ColumnNameToComparativeMapChoicer;
import drds.plus.common.model.comparative.Comparative;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

class Choicer implements ColumnNameToComparativeMapChoicer {

    private Map<String, Comparative> comparatives = new HashMap<String, Comparative>();

    public Choicer() {

    }

    public Choicer(Map<String, Comparative> comparatives) {
        this.comparatives = comparatives;
    }

    public void addComparative(String name, Comparative comparative) {
        this.comparatives.put(name, comparative);
    }

    public Comparative getColumnComparative(String colName, List<Object> arguments) {
        return comparatives.get(colName);
    }

    public Map<String, Comparative> getColumnNameToComparativeMap(Set<String> partnationSet, List<Object> argumentList) {
        return null;
    }
}
