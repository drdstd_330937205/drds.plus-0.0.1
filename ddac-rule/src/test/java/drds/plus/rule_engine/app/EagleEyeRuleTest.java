package drds.plus.rule_engine.app;

import drds.plus.rule_engine.Route;
import drds.plus.rule_engine.rule_calculate.DataNodeDataScatterInfo;
import drds.plus.rule_engine.rule_calculate.RuleCalculateResult;
import org.junit.BeforeClass;
import org.junit.Test;


public class EagleEyeRuleTest {

    static Route route;

    @BeforeClass
    public static void setUp() {

    }

    @Test
    public void testEagleEye() {
        RuleCalculateResult target = route.routes("hsflog", "days=1:int");
        for (DataNodeDataScatterInfo db : target.getDataNodeDataScatterInfoList()) {
            System.out.println("--------------------------");
            System.out.println(db.getDataNodeId() + " ---------> " + db.getTableNameSet());
        }

        target = route.routes("hsflog", "traceid=1:int");
        for (DataNodeDataScatterInfo db : target.getDataNodeDataScatterInfoList()) {
            System.out.println("--------------------------");
            System.out.println(db.getDataNodeId() + " ---------> " + db.getTableNameSet());
        }
    }

    @Test
    public void testNoitfy() {
        RuleCalculateResult target = route.routes("notify_msg", "message_id in (8FABA2621386CA2924E0451F44D598A8):string");
        for (DataNodeDataScatterInfo db : target.getDataNodeDataScatterInfoList()) {
            System.out.println("--------------------------");
            System.out.println(db.getDataNodeId() + " ---------> " + db.getTableNameSet());
        }

        target = route.routes("notify_msg", "gmt_create_days in (15970):int");
        for (DataNodeDataScatterInfo db : target.getDataNodeDataScatterInfoList()) {
            System.out.println("--------------------------");
            System.out.println(db.getDataNodeId() + " ---------> " + db.getTableNameSet());
        }
    }
}
