package drds.plus.rule_engine.utils;

class Frag {

    public final String value;
    public final boolean isPlaceHolderName;

    public Frag(String piece, boolean isPlaceHolderName) {
        this.value = piece;
        this.isPlaceHolderName = isPlaceHolderName;
    }
}
