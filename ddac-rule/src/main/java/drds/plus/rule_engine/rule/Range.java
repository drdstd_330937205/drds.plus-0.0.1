package drds.plus.rule_engine.rule;

public class Range {

    public final Integer start; // 起始值
    public final Integer end; // 结束值

    public Range(Integer start, Integer end) {
        this.start = start;
        this.end = end;
    }
}
