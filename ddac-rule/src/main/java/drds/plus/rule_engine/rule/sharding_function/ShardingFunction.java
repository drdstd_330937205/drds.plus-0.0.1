package drds.plus.rule_engine.rule.sharding_function;

import java.util.Map;

/**
 * groovy动态生成
 */
public interface ShardingFunction {

    Object eval(Map map, Object outerCtx);
}
