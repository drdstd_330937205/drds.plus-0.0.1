package drds.plus.rule_engine.rule;

import drds.plus.common.model.comparative.Comparative;

import java.util.Map;
import java.util.Set;


public interface Rule<T> {

    /**
     * @return 规则计算需要的列
     */
    Map<String, RuleColumn> getColumnNameToRuleColumnMap();

    /**
     * @return 规则计算需要的列
     */
    Set<RuleColumn> getRuleColumnSet();

    /**
     * @return 返回规则表达式字符串
     */
    String getExpression();

    /**
     * 列值对进行rule表达式求值
     *
     * @param columnValues 列值对。个数与getRuleColumns相同。
     * @param outerContext 动态的额外参数。比如从ThreadLocal中传入的表名前缀
     * @return 根据一组列值对计算结果
     */
    T eval(Map<String/* 列名 */, Object/* 列值 */> columnValues, Object outerContext);

    /**
     * 比较树匹配
     *
     * @param columnNameToComparativeMap 从SQL提取出来的比较树
     * @param ctx                        规则执行的上下文。用于关联规则执行时，规则间必要信息的传递。对于EnumerativeRule来说。在库表规则有公共列时，
     *                                   会在每一个库规则的值下面，执行表规则；执行时库规则产生该值的描点信息将以该参数传入。
     * @param outerContext               动态的额外参数。比如从ThreadLocal中传入的表名前缀
     * @return 规则计算结果，和得到这个结果的所有数据。
     */
    Map<T, ? extends Object> calculate(Map<String/* 列名 */, Comparative> columnNameToComparativeMap, Object outerContext);


}
