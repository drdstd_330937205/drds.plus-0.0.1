package drds.plus.rule_engine.rule;

public class RuleColumn {

    /**
     * 是否为可选列，若optional==true，则选择rule时，sql可以不包含该列。到时对该列值域做遍历
     */
    public final boolean optional; //
    /**
     * sql中的列名，必须是大写，这里在setter显示的设置成大写了
     */
    public final String columnName;

    public RuleColumn(String name, boolean optional) {
        this.columnName = name.toLowerCase();
        this.optional = optional;
    }
}
