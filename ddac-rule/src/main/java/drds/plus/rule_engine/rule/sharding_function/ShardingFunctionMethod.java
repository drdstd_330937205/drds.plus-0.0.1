package drds.plus.rule_engine.rule.sharding_function;

import drds.plus.common.thread_local.ThreadLocalMap;
import drds.plus.common.utils.convertor.Convertor;
import drds.plus.common.utils.convertor.ConvertorHelper;

import java.util.Calendar;
import java.util.Date;


public class ShardingFunctionMethod {

    public static final String GROOVY_STATIC_METHOD_CALENDAR = "GROOVY_STATIC_METHOD_CALENDAR";
    private final static long[] pow10 = {1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000, 1000000000, 10000000000L, 100000000000L, 1000000000000L, 10000000000000L, 100000000000000L, 1000000000000000L, 10000000000000000L, 100000000000000000L, 1000000000000000000L};

    private final static Convertor commonConvertor = ConvertorHelper.commonToCommon;

    /**
     * @return 返回4位年份
     */
    public static int yyyy(Date date) {
        Calendar cal = getCalendar(date);
        return cal.get(Calendar.YEAR);
    }

    public static int yyyy(Calendar cal) {
        return cal.get(Calendar.YEAR);
    }

    /**
     * @return 返回2位年份（年份的后两位）
     */
    public static int yy(Date date) {
        Calendar cal = getCalendar(date);
        return cal.get(Calendar.YEAR) % 100;
    }

    public static int yy(Calendar cal) {
        return cal.get(Calendar.YEAR) % 100;
    }

    /**
     * @return 返回月份数字，注意：从1开始：1-12（返回 Calendar.MONTH对应的值加1）
     */
    public static int month(Date date) {
        Calendar cal = getCalendar(date);
        return cal.get(Calendar.MONTH) + 1;
    }

    public static int month(Calendar cal) {
        return cal.get(Calendar.MONTH) + 1;
    }

    /**
     * @return 返回2位的月份字串，从01开始：01-12（Calendar.MONTH对应的值加1）
     */
    public static String mm(Date date) {
        Calendar cal = getCalendar(date);
        int m = cal.get(Calendar.MONTH) + 1;
        return m < 10 ? "0" + m : String.valueOf(m);
    }

    public static String mm(Calendar cal) {
        int m = cal.get(Calendar.MONTH) + 1;
        return m < 10 ? "0" + m : String.valueOf(m);
    }

    /**
     * @return 返回 Calendar.DAY_OF_WEEK 对应的值
     */
    public static int week(Date date) {
        Calendar cal = getCalendar(date);
        return cal.get(Calendar.DAY_OF_WEEK);
    }

    public static int week(Calendar cal) {
        return cal.get(Calendar.DAY_OF_WEEK);
    }

    /**
     * 旧规则默认的dayofweek : 如果offset = 0;那么为默认 SUNDAY=1; MONDAY=2; TUESDAY=3;
     * WEDNESDAY=4; THURSDAY=5; FRIDAY=6; SATURDAY=7;
     */
    public static int dayofweek(Date date, int offset) {
        Calendar cal = getCalendar(date);
        return cal.get(Calendar.DAY_OF_WEEK) + offset;
    }

    public static int dayofweek(Calendar cal, int offset) {
        return cal.get(Calendar.DAY_OF_WEEK) + offset;
    }

    /**
     * 表规则结果是库内表数组下标，必须从0开始， 因此必须让day of week从0
     * 开始。通过直接offset = -1 解决：星期日=0,星期一=1,...星期六=6
     */
    public static int dayofweek(Date date) {
        return dayofweek(date, -1);
    }

    public static int dayofweek(Calendar cal) {
        return dayofweek(cal, -1);
    }

    /**
     * @return 返回4位年份和2位月份的字串，月份从01开始：01-12
     */
    public static String yyyymm(Date date) {
        Calendar cal = getCalendar(date);
        return yyyy(cal) + mm(cal);
    }

    public static String yyyymm(Calendar cal) {
        return yyyy(cal) + mm(cal);
    }

    public static String yyyymmdd(Date date) {
        Calendar cal = getCalendar(date);
        return yyyy(cal) + mm(cal) + date(date);
    }

    public static String yyyymmdd(Calendar cal) {
        return yyyy(cal) + mm(cal) + date(cal);
    }

    /**
     * @return 返回 4位年份_2位月份 的字串，月份从01开始：01-12
     */
    public static String yyyy_mm(Date date) {
        Calendar cal = getCalendar(date);
        return yyyy(cal) + "_" + mm(cal);
    }

    public static String yyyy_mm(Calendar cal) {
        return yyyy(cal) + "_" + mm(cal);
    }

    /**
     * @return 返回2位年份和2位月份的字串，月份从01开始：01-12
     */
    public static String yymm(Date date) {
        Calendar cal = getCalendar(date);
        return yy(cal) + mm(cal);
    }

    public static String yymm(Calendar cal) {
        return yy(cal) + mm(cal);
    }

    /**
     * @return 返回 2位年份_2位月份 的字串，月份从01开始：01-12
     */
    public static String yy_mm(Date date) {
        Calendar cal = getCalendar(date);
        return yy(cal) + "_" + mm(cal);
    }

    public static String yy_mm(Calendar cal) {
        return yy(cal) + "_" + mm(cal);
    }

    /**
     * @return 返回 Calendar.DATE 对应的值。每月的1号值为1, 2号值为2...
     */
    public static int date(Date date) {
        Calendar cal = getCalendar(date);
        return cal.get(Calendar.DATE);
    }

    public static int date(Calendar cal) {
        return cal.get(Calendar.DATE);
    }

    /**
     * @return 返回2位的天数，从01开始：01-31（Calendar.DATE对应的值）
     */
    public static String dd(Date date) {
        Calendar calendar = getCalendar(date);
        int d = calendar.get(Calendar.DATE);
        return d < 10 ? "0" + d : String.valueOf(d);
    }

    public static String dd(Calendar cal) {
        int d = cal.get(Calendar.DATE);
        return d < 10 ? "0" + d : String.valueOf(d);
    }

    public static String mmdd(Date date) {
        Calendar cal = getCalendar(date);
        return mm(cal) + dd(cal);
    }

    public static String mmdd(Calendar cal) {
        return mm(cal) + dd(cal);
    }

    @SuppressWarnings("unused")
    private static Calendar getCalendar(Calendar calendar) {
        return calendar;
    }

    private static Calendar getCalendar(Date date) {
        Calendar calendar = (Calendar) ThreadLocalMap.get(GROOVY_STATIC_METHOD_CALENDAR);
        if (calendar == null) {
            calendar = Calendar.getInstance();
            ThreadLocalMap.put(GROOVY_STATIC_METHOD_CALENDAR, calendar);
        }
        calendar.setTime(date);
        return calendar;
    }
/////////////////////////////////////////////////////////////////////////////

    /**
     * 从左开始，取指定多的位数。默认是一个long形长度的数据，也就是bitNumber= 19
     *
     * @param targetID 目标id，也就是等待被decode的数据
     * @param st       从哪儿开始取，如果想取最左边的一位那么可以输入st = 0;ed =1;
     * @param ed       取到哪儿，如果想取最左边的两位，那么可以输入st = 0;ed = 2;
     * @return
     */
    public static int quarter(Date date) {
        Calendar cal = getCalendar(date);
        int month = cal.get(Calendar.MONTH);
        return quarter(month);
    }

    public static int quarter(int month) {
        if (month > 11 || month < 0) {
            throw new IllegalArgumentException("month range is 1~12");
        }
        return month / 3 + 1;
    }


    public static int halfayear(long month) {
        return halfayear((int) month);
    }


    public static int halfayear(Date date) {
        Calendar cal = getCalendar(date);
        int month = cal.get(Calendar.MONTH);
        return halfayear(month);
    }

    public static int halfayear(int month) {
        if (month > 11 || month < 0) {
            throw new IllegalArgumentException("month range is 1~12,current value is " + month);
        }
        return month / 6 + 1;
    }

    public static Long longValue(Object o) {
        if (o == null) {
            return null;
        }

        try {
            return (Long) commonConvertor.convert(o, Long.class);
        } catch (RuntimeException e) {
            Convertor convertor = ConvertorHelper.getInstance().getConvertor(o.getClass(), Long.class);
            if (convertor != null) {
                return (Long) convertor.convert(o, Long.class);
            } else {
                throw new RuntimeException("Unsupported convert: [" + o.getClass().getName() + "," + Long.class.getName() + "]");
            }
        }
    }
}
