package drds.plus.rule_engine.rule_calculate;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * 数据节点 数据分布信息
 */
public class DataNodeDataScatterInfo {


    private String dataNodeId;

    /**
     * 这个规则下的符合查询条件的表名列表
     */
    private Map<String, ColumnNameToValueSetMap> tableNameToColumnNameToValueSetMapMap;

    /**
     * 返回表名的结果集
     *
     * @return 空Set if 没有表 表名结果集
     */
    public Set<String> getTableNameSet() {
        if (tableNameToColumnNameToValueSetMapMap == null) {
            return null;
        }
        return tableNameToColumnNameToValueSetMapMap.keySet();
    }

    public Map<String, ColumnNameToValueSetMap> getTableNameToColumnNameToValueSetMapMap() {
        return tableNameToColumnNameToValueSetMapMap;
    }

    public void setTableNameToColumnNameToValueSetMapMap(Map<String, ColumnNameToValueSetMap> tableNameToColumnNameToValueSetMapMap) {
        this.tableNameToColumnNameToValueSetMapMap = tableNameToColumnNameToValueSetMapMap;
    }

    public void addTable(String tableName) {
        if (tableNameToColumnNameToValueSetMapMap == null) {
            tableNameToColumnNameToValueSetMapMap = new HashMap<String, ColumnNameToValueSetMap>();
        }
        tableNameToColumnNameToValueSetMapMap.put(tableName, ColumnNameToValueSetMap.EMPTY_COLUMN_NAME_TO_VALUE_SET_MAP);
    }

    public String getDataNodeId() {
        return dataNodeId;
    }

    public void setDataNodeId(String dataNodeId) {
        this.dataNodeId = dataNodeId;
    }

}
