package drds.plus.rule_engine.rule_calculate;

import drds.plus.common.model.comparative.Comparative;

import java.util.List;
import java.util.Map;

/**
 * 匹配结果对象
 */
public class RuleCalculateResult {

    private final List<DataNodeDataScatterInfo> dataNodeDataScatterInfoList; // 匹配的db结果

    private final Map<String, Comparative> tableComparativeMap; // 计算出该结果Rule中匹配的表参数

    public RuleCalculateResult(List<DataNodeDataScatterInfo> dataNodeDataScatterInfoList, Map<String, Comparative> tableComparativeMap) {
        this.dataNodeDataScatterInfoList = dataNodeDataScatterInfoList;

        this.tableComparativeMap = tableComparativeMap;
    }

    /**
     * 规则计算后的结果对象
     *
     * @return
     */
    public List<DataNodeDataScatterInfo> getDataNodeDataScatterInfoList() {
        return dataNodeDataScatterInfoList;
    }


    /**
     * 产生这个匹配结果时，对应的表参数是什么,不会出现Null值
     *
     * @return
     */
    public Map<String, Comparative> getTableComparativeMap() {
        return tableComparativeMap;
    }

}
