package drds.plus.rule_engine;

import drds.plus.common.lifecycle.Lifecycle;
import drds.plus.common.model.comparative.ColumnNameToComparativeMapChoicer;
import drds.plus.rule_engine.rule_calculate.RuleCalculateDiffrentException;
import drds.plus.rule_engine.rule_calculate.RuleCalculateResult;
import drds.plus.rule_engine.table_rule.TableRuleSet;

import java.util.List;


public interface IRoute extends Lifecycle {

    /**
     * 根据当前规则，计算table rule结果. <br/>
     * ps. 当前规则可能为local本地规则或者是当前使用中的远程规则
     *
     * @param tableName                         逻辑表名
     * @param columnNameToComparativeMapChoicer 参数提取，比如statement sql中自带的参数
     * @param args                              通过prepareStatement设置的参数
     * @return
     */
    RuleCalculateResult routes(String tableName, ColumnNameToComparativeMapChoicer columnNameToComparativeMapChoicer, List<Object> args);

    /**
     * 指定version规则版本，计算table rule结果
     *
     * @param tableName                         逻辑表名
     * @param columnNameToComparativeMapChoicer 参数提取，比如statement sql中自带的参数
     * @param args                              通过prepareStatement设置的参数
     * @param version                           指定规则
     * @return
     */
    RuleCalculateResult routes(String tableName, ColumnNameToComparativeMapChoicer columnNameToComparativeMapChoicer, List<Object> args, String version);

    /**
     * 指定规则，计算table rule结果
     *
     * @param tableName                         逻辑表名
     * @param columnNameToComparativeMapChoicer 参数提取，比如statement sql中自带的参数
     * @param args                              通过prepareStatement设置的参数
     * @param specifyVtr                        指定规则
     * @return
     */
    RuleCalculateResult routes(String tableName, ColumnNameToComparativeMapChoicer columnNameToComparativeMapChoicer, List<Object> args, TableRuleSet specifyVtr);

    RuleCalculateResult routes(boolean isSelect, String tableName, ColumnNameToComparativeMapChoicer columnNameToComparativeMapChoicer, List<Object> args) throws RuleCalculateDiffrentException;

    /**
     * 根据当前规则，同时支持新旧规则模式的计算 <br/>
     * ps. 运行时切库会同时指定新旧两个版本号，针对读请求使用老规则，针对写请求如果新老规则相同则正常返回，如果不同则抛diff异常.<br/>
     *
     * <pre>
     * 一般切换步骤：
     * 1. 根据新规则，将老库数据做一次数据复制
     * 2. 线上配置新老规则，同时生效
     * 3. 应用前端停写，等待后端增量数据迁移完全追平
     * 4. 线上配置为新规则
     * 5. 删除老库上的数据
     * </pre>
     *
     * @param isSelect                          原始sql类型
     * @param tableName                         逻辑表名
     * @param columnNameToComparativeMapChoicer 参数提取，比如statement sql中自带的参数
     * @param args                              通过prepareStatement设置的参数
     * @param forceAllowFullTableScan           允许用户强制指定是否开启全表扫描,true代表强制开启/false代表使用当前规则
     * @return
     * @throws RuleCalculateDiffrentException
     */
    RuleCalculateResult routes(boolean isSelect, String tableName, ColumnNameToComparativeMapChoicer columnNameToComparativeMapChoicer, List<Object> args, boolean forceAllowFullTableScan) throws RuleCalculateDiffrentException;

    // ==================== 以下方法可支持非jdbc协议使用rule =================

    /**
     * 根据当前规则，计算table rule结果. <br/>
     *
     * @param tableName 逻辑表
     * @param condition 类似statement sql表达式
     * @return
     */
    RuleCalculateResult routes(String tableName, String condition);

    /**
     * 指定规则，计算table rule结果. <br/>
     *
     * @param tableName 逻辑表
     * @param condition 类似statement sql表达式
     * @return
     */
    RuleCalculateResult routes(String tableName, String condition, TableRuleSet tableRuleSet);

    /**
     * 指定version规则版本，计算table rule结果. <br/>
     *
     * @param tableName 逻辑表
     * @param condition 类似statement sql表达式
     * @return
     */
    RuleCalculateResult routes(String tableName, String condition, String version);

}
