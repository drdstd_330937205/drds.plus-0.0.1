package drds.plus.rule_engine.table_rule;

import drds.plus.common.lifecycle.AbstractLifecycle;
import drds.plus.common.lifecycle.Lifecycle;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;
import java.util.Set;


@Slf4j
public abstract class AbstractITableRule extends AbstractLifecycle implements Lifecycle, ITableRule {


    @Setter
    @Getter

    protected String virtualTbName; // 逻辑表名
    @Setter
    @Getter
    protected Map<String, Set<String>> actualTopology;


    public String getVirtualTbName() {
        return virtualTbName;
    }

    public void setVirtualTbName(String virtualTbName) {
        this.virtualTbName = virtualTbName;
    }

    public Map<String, Set<String>> getActualTopology() {
        return actualTopology;
    }

    /**
     * 判断是否为物理拓扑中的表
     */
    public boolean isActualTable(String actualTable) {
        if (actualTable == null) {
            return false;
        }

        for (Set<String> tables : this.actualTopology.values()) {
            for (String table : tables) {
                if (table.equalsIgnoreCase(actualTable)) {
                    return true;
                }
            }
        }

        return false;
    }
}
