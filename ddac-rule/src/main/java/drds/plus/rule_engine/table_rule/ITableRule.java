package drds.plus.rule_engine.table_rule;


import drds.plus.rule_engine.rule.Rule;

import java.util.List;
import java.util.Map;
import java.util.Set;


public interface ITableRule<D, T> {


    Rule getRule();

    /**
     * 返回本规则实际对应的全部库表拓扑结构
     *
     * @return columnName:dbIndex; value:实际物理表名的集合
     */
    Map<String, Set<String>> getActualTopology();

    Object getOuterContext();


    boolean isAllowFullTableScan();

    String getTableNamePattern();


    boolean isBroadcast();

    String getJoinDataNodeId();


    /**
     * 获取当前默认版本，逻辑表的分区字段，如果存在多个规则时，返回多个规则分区字段的总和
     */
    List<String> getShardColumnNameList();
}
