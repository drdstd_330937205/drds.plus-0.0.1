package drds.plus.rule_engine.table_rule;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import drds.plus.common.lifecycle.AbstractLifecycle;
import drds.plus.common.lifecycle.Lifecycle;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Slf4j
public class VersionToTableRuleSetMap extends AbstractLifecycle implements Lifecycle {
    public Logger log() {
        return log;
    }

    private static final String NO_VERSION = "NO_VERSION";
    @Setter
    @Getter
    protected String applicationId;

    // 默认物理的dbIndex，针对allowEmptyRule=true时有效
    @Setter
    @Getter
    protected String defaultDataNodeId;


    /**
     * columnName = 0(old),1(new),2,3,4... value= version
     */
    @Setter
    @Getter
    private volatile Map<String, TableRuleSet> versionToTableRuleSetMap = Maps.newLinkedHashMap();
    @Setter
    @Getter
    private volatile Map<Integer, String> versionIndexToVersionMap = Maps.newHashMap();//index++


    public void doInit() {
        // 本地文件单版本规则
        TableRuleSet tableRuleSet = new TableRuleSet();//new
        versionToTableRuleSetMap.put(NO_VERSION, tableRuleSet);
        // 构建versionIndex
        int index = 0;
        Map<Integer, String> versionIndexToVersionMap = new HashMap<Integer, String>();
        for (String version : versionToTableRuleSetMap.keySet()) {
            versionIndexToVersionMap.put(index, version);
            index++;
        }
        this.versionIndexToVersionMap = versionIndexToVersionMap;
    }

    /**
     * <pre>
     * 返回当前使用的rule规则
     * 1. 如果是本地文件，则直接返回本地文件的版本 (本地文件存在多版本时，直接返回第一个版本)
     * 2. 如果是动态规则，则直接返回第一个版本
     *
     * ps. 正常情况，只有一个版本会处于使用中，也就是在数据库动态切换出现多版本使用中.
     * </pre>
     */
    public TableRuleSet getCurrentTableRuleSet() {
        if (versionIndexToVersionMap.size() == 0) {
            throw new RuntimeException("规则对象为空!请检查是否存在规则!");
        }

        return versionToTableRuleSetMap.get(versionIndexToVersionMap.get(0));
    }

    public TableRuleSet getTableRuleSet(String version) {
        TableRuleSet tableRuleSet = versionToTableRuleSetMap.get(version);
        if (tableRuleSet == null) {
            throw new RuntimeException("规则对象为空!请检查是否存在规则!");
        }

        return tableRuleSet;
    }

    /**
     * 获取当前在用的版本，理论上正常只有一个版本(切换时出现两个版本)，顺序返回版本，第一个版本为当前正在使用中的旧版本
     */
    public List<String> getVersionList() {
        int size = versionIndexToVersionMap.size();
        List<String> list = Lists.newArrayList();
        for (int i = 0; i < size; i++) {
            list.add(versionIndexToVersionMap.get(i));
        }
        return list;
    }


    public void doDestroy() {
    }


}
