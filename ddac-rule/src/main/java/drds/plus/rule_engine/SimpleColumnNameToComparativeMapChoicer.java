package drds.plus.rule_engine;

import drds.plus.common.model.comparative.ColumnNameToComparativeMapChoicer;
import drds.plus.common.model.comparative.Comparative;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

class SimpleColumnNameToComparativeMapChoicer implements ColumnNameToComparativeMapChoicer {

    private Map<String, Comparative> columnNameToComparativeMap = new HashMap<String, Comparative>();

    public SimpleColumnNameToComparativeMapChoicer(Map<String, Comparative> columnNameToComparativeMap) {
        this.columnNameToComparativeMap = columnNameToComparativeMap;
    }

    public Map<String, Comparative> getColumnNameToComparativeMap(Set<String> partnationSet, List<Object> argumentList) {
        return this.columnNameToComparativeMap;
    }

    public Comparative getColumnComparative(String columnName, List<Object> argumentList) {
        return this.columnNameToComparativeMap.get(columnName);
    }
}
