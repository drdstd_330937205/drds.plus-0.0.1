package drds.plus.rule_engine.column_value_enumerator.handler;

import drds.plus.common.model.comparative.Comparative;

import java.util.Set;

public interface CloseIntervalColumnValueEnumerateHandler {

    /**
     * 穷举出从from，根据自增value和自增次数Times，将结果写入retValue参数中
     */
    void exhaust(Comparative from, //
                 Integer cumulativeTime, //
                 Comparable<?> atomIncreaseValue, //
                 Set<Object> returnSet);//

    /**
     * 穷举出从from到to中的所有值，根据自增value和自增次数Times，将结果写入retValue参数中
     */
    void exhaust(Comparative from, //
                 Comparative to, //
                 Integer cumulativeTime, //
                 Comparable<?> atomIncreaseValue, //
                 Set<Object> returnSet);//

}
