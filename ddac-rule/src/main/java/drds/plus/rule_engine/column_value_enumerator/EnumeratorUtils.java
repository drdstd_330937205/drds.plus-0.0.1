package drds.plus.rule_engine.column_value_enumerator;

import java.math.BigDecimal;

public class EnumeratorUtils {

    /**
     * 将BigDecimal转换为long或者double
     */
    public static Comparable<?> toPrimaryValue(Comparable<?> comparable) {
        if (comparable instanceof BigDecimal) {
            BigDecimal bigDecimal = (BigDecimal) comparable;
            int scale = bigDecimal.scale();
            if (scale == 0) {
                // long int
                try {
                    return bigDecimal.longValueExact();
                } catch (ArithmeticException e) {
                    return bigDecimal;
                }
            } else {
                // double float
                return bigDecimal.doubleValue();
            }
        } else {
            return comparable;
        }
    }
}
