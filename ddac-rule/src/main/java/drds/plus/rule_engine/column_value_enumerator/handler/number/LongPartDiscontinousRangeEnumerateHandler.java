package drds.plus.rule_engine.column_value_enumerator.handler.number;

import drds.plus.rule_engine.column_value_enumerator.EnumeratorUtils;

public class LongPartDiscontinousRangeEnumerateHandler extends NumberPartDiscontinousRangeEnumerateHandler {

    protected Number cast2Number(Comparable begin) {
        return (Long) begin;
    }

    protected Number getNumber(Comparable begin) {
        return (Long) EnumeratorUtils.toPrimaryValue(begin);
    }

    protected Number plus(Number begin, int plus) {
        return (Long) begin + plus;
    }
}
