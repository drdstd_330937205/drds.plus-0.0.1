package drds.plus.rule_engine.column_value_enumerator.handler;

import drds.plus.common.model.comparative.Comparative;

import java.util.Set;

/**
 * 如果不能进行枚举，那么就是用默认的枚举器 默认枚举器只支持comparativeOr条件，以及等于的关系。不支持大于小于等一系列关系。
 */
public class DefaultCloseIntervalColumnValueEnumerateHandler implements CloseIntervalColumnValueEnumerateHandler {

    public void exhaust(Comparative from, Comparative to, Integer cumulativeTime, Comparable<?> atomIncreaseValue, Set<Object> returnSet) {
        throw new IllegalArgumentException("default column_value_enumerator not support traversal");

    }

    public void exhaust(Comparative from, Integer cumulativeTime, Comparable<?> atomIncreaseValue, Set<Object> returnSet) {
        throw new IllegalStateException("default column_value_enumerator not support traversal, not support > < >= <=");
    }
}
