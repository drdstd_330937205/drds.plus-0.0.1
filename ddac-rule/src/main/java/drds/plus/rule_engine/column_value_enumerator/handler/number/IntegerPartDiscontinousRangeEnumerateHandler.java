package drds.plus.rule_engine.column_value_enumerator.handler.number;

public class IntegerPartDiscontinousRangeEnumerateHandler extends NumberPartDiscontinousRangeEnumerateHandler {

    protected Number cast2Number(Comparable begin) {
        return (Integer) begin;
    }

    protected Number getNumber(Comparable begin) {
        return (Integer) begin;
    }

    protected Number plus(Number begin, int plus) {
        return (Integer) begin + plus;
    }

}
