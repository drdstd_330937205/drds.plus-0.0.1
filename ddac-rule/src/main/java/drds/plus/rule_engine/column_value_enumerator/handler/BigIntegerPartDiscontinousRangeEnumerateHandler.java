package drds.plus.rule_engine.column_value_enumerator.handler;

import drds.plus.rule_engine.column_value_enumerator.handler.number.NumberPartDiscontinousRangeEnumerateHandler;

import java.math.BigInteger;

public class BigIntegerPartDiscontinousRangeEnumerateHandler extends NumberPartDiscontinousRangeEnumerateHandler {

    protected BigInteger cast2Number(Comparable begin) {
        return (BigInteger) begin;
    }

    protected BigInteger getNumber(Comparable begin) {
        return (BigInteger) begin;
    }

    protected BigInteger plus(Number begin, int plus) {
        return ((BigInteger) begin).add(BigInteger.valueOf(plus));
    }

    protected boolean inputCloseRangeGreaterThanMaxFieldOfDifination(Comparable from, Comparable to, Integer cumulativeTimes, Comparable<?> atomIncrValue) {
        if (cumulativeTimes == null) {
            return false;
        }
        if (atomIncrValue == null) {
            atomIncrValue = DEFAULT_LONG_ATOMIC_VALUE;
        }
        BigInteger fromBig = cast2Number(from);
        BigInteger toBig = cast2Number(to);
        int atomIncValLong = ((Number) atomIncrValue).intValue();
        int size = cumulativeTimes;
        return (toBig.subtract(fromBig).compareTo(BigInteger.valueOf(atomIncValLong * size)) > 0);
    }
}
