package drds.plus.rule_engine.column_value_enumerator;

import drds.plus.common.model.comparative.Comparative;

public class EnumerationInterruptException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    private transient Comparative comparative;

    public EnumerationInterruptException(Comparative comparative) {
        super(comparative.toString());
        this.comparative = comparative;
    }

    public Comparative getComparative() {
        return comparative;
    }

    public void setComparative(Comparative comparative) {
        this.comparative = comparative;
    }

    public synchronized Throwable fillInStackTrace() {
        return this;
    }

}
