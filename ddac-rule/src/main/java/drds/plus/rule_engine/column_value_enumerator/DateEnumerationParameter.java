package drds.plus.rule_engine.column_value_enumerator;

import java.util.Calendar;

/**
 * 用于传递自增数字和自增数字对应在Calendar里的类型 继承Comparable是因为开始预留的接口是Comparable...
 */
public class DateEnumerationParameter implements Comparable {

    public final int atomicIncreaseNumber;
    public final int calendarFieldType;

    /**
     * 默认使用Date作为日期类型的基本自增单位
     */
    public DateEnumerationParameter(int atomicIncreaseNumber) {
        this.atomicIncreaseNumber = atomicIncreaseNumber;
        this.calendarFieldType = Calendar.DATE;
    }

    public DateEnumerationParameter(int atomicIncreaseNumber, int calendarFieldType) {
        this.atomicIncreaseNumber = atomicIncreaseNumber;
        this.calendarFieldType = calendarFieldType;
    }

    public int compareTo(Object o) {
        throw new IllegalArgumentException("should not be here !");
    }

}
