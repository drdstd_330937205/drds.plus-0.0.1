package drds.plus.datanode.configuration;

public class DataSourceIndexAndNeedRetryIfDailed {

    public final String dataSourceIndex;
    public final boolean needRetryIfDailed;

    public DataSourceIndexAndNeedRetryIfDailed(String dataSourceIndex, boolean needRetryIfDailed) {
        this.dataSourceIndex = dataSourceIndex;
        this.needRetryIfDailed = needRetryIfDailed;
    }
}
