package drds.plus.datanode.configuration;

import drds.plus.datasource.api.DataSource;
import lombok.Getter;
import lombok.Setter;


public class DataSourceWrapper {
    @Setter
    @Getter
    private final String dataSourceId; // 这个DataSource对应的dbKey
    @Setter
    @Getter
    private final String weightString; // 权重信息字符串
    @Setter
    @Getter
    private final Weight weight; // 权重信息
    @Setter
    @Getter
    private final DataSource dataSource; // 被封装的目标DataSource


    public DataSourceWrapper(String dataSourceId, String weightString, DataSource dataSource) {
        this.dataSourceId = dataSourceId;
        this.weight = new Weight(weightString);
        this.weightString = weightString;
        this.dataSource = dataSource;

    }

    /**
     * 验证此DataSource的路由index信息中，是否包含指定的index
     */
    public boolean isMatchDataSourceIndex(String dataSourceId) {
        return this.dataSourceId == dataSourceId;
    }

    /**
     * 是否有读权重。r0则放回false
     */
    public boolean hasReadWeight() {
        return weight.readWeight != 0;
    }

    /**
     * 是否有写权重。w0则放回false
     */
    public boolean hasWriteWeight() {
        return weight.writeWeight != 0;
    }


}
