package drds.plus.datanode.configuration;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class DataNodeExtraConfiguration {

    /**
     * when set this parameter is true,table not in tableDsIndexMap or sql_process not in
     * sqlDsIndexMap, this sql_process will be forced go to nonspi db, priority is low(compare
     * to local seted dataSourceIndex, tableDsIndexMap,sqlDsIndexMap),higher than
     * weight query. add by junyu,2011-11-01
     */
    private boolean defaultMain = false;

    /**
     * this map define the actual_table and dataSourceIndex relation add by
     * junyu,2011-11-01
     */
    private Map<String/* table */, String/* dataSourceIndex */> tableDsIndexMap = new HashMap<String, String>();

    /**
     * this map define the sql_process and dataSourceIndex relation add by junyu,2011-11-01
     */
    private Map<String/* sql_process */, String/* dataSourceIndex */> sqlDsIndexMap = new HashMap<String, String>();

    /**
     * this comparativeList contain the sqls whitch are forbidden add by
     * jiechen,2011-12-29
     */
    private Set<String/* sql_process */> sqlForbidSet = new HashSet<String>();

    public boolean isDefaultMain() {
        return defaultMain;
    }

    public void setDefaultMain(boolean defaultMain) {
        this.defaultMain = defaultMain;
    }

    public Map<String, String> getTableDsIndexMap() {
        return tableDsIndexMap;
    }

    public void setTableDsIndexMap(Map<String, String> tableDsIndexMap) {
        this.tableDsIndexMap = tableDsIndexMap;
    }

    public Map<String, String> getSqlDsIndexMap() {
        return sqlDsIndexMap;
    }

    public void setSqlDsIndexMap(Map<String, String> sqlDsIndexMap) {
        this.sqlDsIndexMap = sqlDsIndexMap;
    }

    public Set<String> getSqlForbidSet() {
        return sqlForbidSet;
    }

    public void setSqlForbidSet(Set<String> sqlForbidSet) {
        this.sqlForbidSet = sqlForbidSet;
    }

}
