package drds.plus.datanode.fetcher;

import drds.plus.datasource.api.DataSource;

import java.sql.SQLException;

public interface DataSourceFetcher {

    DataSource getDataSource(String dataSourceId) throws SQLException;

}
