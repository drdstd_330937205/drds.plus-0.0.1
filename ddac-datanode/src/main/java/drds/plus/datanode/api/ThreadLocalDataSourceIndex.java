package drds.plus.datanode.api;

import drds.plus.common.model.ThreadLocalString;
import drds.plus.common.thread_local.ThreadLocalMap;
import drds.plus.datanode.configuration.DataSourceIndexAndNeedRetryIfDailed;
import drds.plus.datanode.select.Selector;

public class ThreadLocalDataSourceIndex {

    public static boolean existsIndex() {
        return getIndexAsObject() != null;
    }

    public static Integer getIndexAsObject() {
        Integer indexObject = null;
        try {
            indexObject = (Integer) ThreadLocalMap.get(ThreadLocalString.DATASOURCE_INDEX);
            if (indexObject == null) {
                return null;
            }
            return indexObject;
        } catch (Exception e) {
            throw new IllegalArgumentException(msg(indexObject));
        }
    }

    public static DataSourceIndexAndNeedRetryIfDailed getIndex() {
        String indexObject = null;
        try {
            indexObject = (String) ThreadLocalMap.get(ThreadLocalString.DATASOURCE_INDEX);
            // 不存在索引时返回-1，这样调用者只要知道返回值是-1就会认为业务层没有设置过索引
            if (indexObject == null) {
                return new DataSourceIndexAndNeedRetryIfDailed(Selector.NOT_EXIST_USER_SPECIFIED_INDEX, false);
            }

            String index = indexObject;
            boolean failRetryFlag = ThreadLocalDataSourceIndex.getFailRetryFlag();
            return new DataSourceIndexAndNeedRetryIfDailed(index, failRetryFlag);
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    private static boolean getFailRetryFlag() {
        Boolean failOver = (Boolean) ThreadLocalMap.get(ThreadLocalString.RETRY_IF_SET_DS_INDEX);
        if (failOver == null) {
            return false;
        } else {
            return failOver;
        }
    }

    public static void clearIndex() {
        ThreadLocalMap.remove(ThreadLocalString.DATASOURCE_INDEX);
        ThreadLocalMap.remove(ThreadLocalString.RETRY_IF_SET_DS_INDEX);
    }

    private static String msg(Integer indexObject) {
        return indexObject + " 不是一个有效的数据源索引，索引只能是大于0的数字";
    }
}
