package drds.plus.datanode.executor;

import drds.plus.datasource.exception_sorter.ExceptionSorter;

import java.sql.SQLException;
import java.util.List;

/**
 * Executor.onSqlException 直接抛出异常
 */
public abstract class AbstractExecutor<T> implements Executor<T> {

    public T onSqlException(List<SQLException> sqlExceptionList, ExceptionSorter exceptionSorter, Object... args) throws SQLException {
        throw new SQLException();
    }
}
