package drds.plus.datanode.executor;

import drds.plus.datanode.configuration.DataSourceWrapper;
import drds.plus.datasource.exception_sorter.ExceptionSorter;

import java.sql.SQLException;
import java.util.List;

/**
 * 在DBSelector管理的数据源上重试执行操作的回调接口
 */
public interface Executor<T> {

    /**
     * tryExecute中重试调用tryOnDataSource遇到非数据库不可用异常，或用完重试次数时，会调用该方法
     *
     * @param sqlExceptionList 历次重试失败抛出的异常。 最后一个异常可能是数据库不可用的异常，也可能是普通的SQL异常
     *                         最后一个之前的异常是数据库不可用的异常
     * @param exceptionSorter  当前用到的判断Exception类型的分类器
     * @param args             与tryOnDataSource时的args相同，都是用户调用tryExecute时传入的arg
     * @return 用户（实现者）觉得是否返回什么值
     * @throws SQLException
     */
    T onSqlException(List<SQLException> sqlExceptionList, ExceptionSorter exceptionSorter, Object... args) throws SQLException;

    T createConnectionWrapperAndExecute(DataSourceWrapper dataSourceWrapper, Object... args) throws SQLException;
}
