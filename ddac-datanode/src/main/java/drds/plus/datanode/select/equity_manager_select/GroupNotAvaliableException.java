package drds.plus.datanode.select.equity_manager_select;

import java.sql.SQLException;

/**
 * 当一组的数据库都试过，都不可用了，并且没有更多的数据源了，抛出该错误
 */
public class GroupNotAvaliableException extends SQLException {

    public GroupNotAvaliableException(String reason, String SQLState, int vendorCode) {
        super(reason, SQLState, vendorCode);
    }

    public GroupNotAvaliableException(String reason, String SQLState) {
        super(reason, SQLState);
    }

    public GroupNotAvaliableException(String reason) {
        super(reason);
    }

    public GroupNotAvaliableException() {
    }

    public GroupNotAvaliableException(Throwable cause) {
        super(cause);
    }

    public GroupNotAvaliableException(String reason, Throwable cause) {
        super(reason, cause);
    }

    public GroupNotAvaliableException(String reason, String sqlState, Throwable cause) {
        super(reason, sqlState, cause);
    }

    public GroupNotAvaliableException(String reason, String sqlState, int vendorCode, Throwable cause) {
        super(reason, sqlState, vendorCode, cause);
    }
}
