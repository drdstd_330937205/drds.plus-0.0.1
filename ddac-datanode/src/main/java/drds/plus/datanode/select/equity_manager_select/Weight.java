package drds.plus.datanode.select.equity_manager_select;

/**
 * 保持不变对象，只能重建，不能修改
 */
public class Weight {
    public final String[] weightKeys; // 调用者保证不能修改其元素
    public final int[] weightValues; // 调用者保证不能修改其元素
    public final int[] weightValueEnds; // 调用者保证不能修改其元素

    public Weight(String[] weightKeys, int[] weights, int[] weightValueEnds) {
        this.weightKeys = weightKeys;
        this.weightValues = weights;
        this.weightValueEnds = weightValueEnds;
    }
}
