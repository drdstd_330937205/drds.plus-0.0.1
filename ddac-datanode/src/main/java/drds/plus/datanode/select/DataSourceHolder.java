package drds.plus.datanode.select;

import drds.plus.datanode.configuration.DataSourceWrapper;

import java.util.concurrent.locks.ReentrantLock;

public class DataSourceHolder {

    public final DataSourceWrapper dataSourceWrapper;
    public final ReentrantLock lock = new ReentrantLock();
    public volatile boolean isNotAvailable = false;
    public volatile long lastRetryTime = 0;

    public DataSourceHolder(DataSourceWrapper dataSourceWrapper) {
        this.dataSourceWrapper = dataSourceWrapper;
    }
}
