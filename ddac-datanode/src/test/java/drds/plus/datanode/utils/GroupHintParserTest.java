package drds.plus.datanode.utils;

import drds.plus.datanode.api.DataNodeHintParser;
import drds.plus.datanode.configuration.DataSourceIndexAndNeedRetryIfDailed;
import org.junit.Assert;
import org.junit.Test;

public class GroupHintParserTest {

    @Test
    public void testSingleIndex() {
        String sql = "/*+TDDL_GROUP({groupIndex:12})*/query * from tab";
        DataSourceIndexAndNeedRetryIfDailed index = DataNodeHintParser.convertHint2Index(sql);
        Assert.assertEquals(index.dataSourceIndex, 12);
        Assert.assertEquals(index.needRetryIfDailed, false);
    }

    @Test
    public void testIndexWithRetry() {
        String sql = "/*+TDDL_GROUP({groupIndex:12,needRetryIfDailed:true})*/query * from tab";
        DataSourceIndexAndNeedRetryIfDailed index = DataNodeHintParser.convertHint2Index(sql);
        Assert.assertEquals(index.dataSourceIndex, 12);
        Assert.assertEquals(index.needRetryIfDailed, true);
    }

    @Test
    public void testNoIndexHint() {
        String sql = "query * from tab";
        DataSourceIndexAndNeedRetryIfDailed index = DataNodeHintParser.convertHint2Index(sql);
        Assert.assertNull(index);
    }

    @Test
    public void testRemoveIndex() {
        String sql = "/*+TDDL_GROUP({groupIndex:12,needRetryIfDailed:true})*/query * from tab";
        String remove = DataNodeHintParser.removeDataNodeIdHint(sql);
        Assert.assertEquals(remove, " query * from tab");
    }

    @Test
    public void testExceptionOnlyRetry() {
        String sql = "/*+TDDL_GROUP({needRetryIfDailed:true})*/query * from tab";
        try {
            DataNodeHintParser.convertHint2Index(sql);
            Assert.fail();
        } catch (Exception e) {
            Assert.assertEquals("the standard datanode hint is:'groupIndex:12[,needRetryIfDailed:true]',current dataSourceIndex hint is:needRetryIfDailed:true", e.getMessage());
        }
    }

    @Test
    public void testExceptionErrorHint() {
        String sql = "/*+TDDL_GROUP({groupIndexx:12})*/query * from tab";
        try {
            DataNodeHintParser.convertHint2Index(sql);
            Assert.fail();
        } catch (Exception e) {
            Assert.assertEquals("the standard datanode hint is:'groupIndex:12[,needRetryIfDailed:true]',current dataSourceIndex hint is:groupIndexx:12", e.getMessage());
        }
    }
}
