package drds.plus.datanode;

import com.alibaba.druid.pool.DruidDataSource;
import drds.plus.datanode.api.DatasourceManager;
import drds.plus.datanode.configuration.DataSourceWrapper;
import drds.plus.datanode.utils.PropLoadTestUtil;
import drds.plus.datasource.api.DataSource;
import drds.plus.datasource.configuration.DatasourceConfiguration;
import drds.plus.datasource.configuration.DatasourceConfigurationParser;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;

import java.sql.Connection;
import java.sql.Statement;


@Ignore("测试基类")
public class BaseGroupTest {

    protected static final String APPNAME = "tddl_sample";
    protected static final String GROUP0 = "tddl_sample_group_0";
    protected static final String GROUP1 = "tddl_sample_group_1";
    protected static final String GROUP2 = "tddl_sample_group_2";
    protected static final String DSKEY0 = "tddl_sample_0";
    protected static final String DSKEY1 = "tddl_sample_1";
    protected static final String DSKEY2 = "tddl_sample_2";

    @BeforeClass
    public static void beforeClass() {
        // MockServer.setUpMockServer();
        // 初始化一些配置
        mockConfig("group0", APPNAME, GROUP0, DSKEY0);
        mockConfig("group1", APPNAME, GROUP1, DSKEY1);
        mockConfig("group2", APPNAME, GROUP2, DSKEY2);
    }

    @AfterClass
    public static void after() {
        // MockServer.tearDownMockServer();
    }

    private static void mockConfig(String dir, String appName, String groupName, String dbKey) {
        String globaStr = PropLoadTestUtil.loadPropFile2String("conf/" + dir + "/globa.properties");
        // MockServer.setConfigInfo(ConnectionPropertiesConfiguration.getGlobalDataId(dbKey), globaStr);

        String appStr = PropLoadTestUtil.loadPropFile2String("conf/" + dir + "/applicationConfigurationFile.properties");
        // MockServer.setConfigInfo(ConnectionPropertiesConfiguration.getAppDataId(applicationId, dbKey), appStr);

        String passwdStr = PropLoadTestUtil.loadPropFile2String("conf/" + dir + "/password.properties");
        // 解析配置
        DatasourceConfiguration datasourceConfiguration = DatasourceConfigurationParser.parseDatasourceConfiguration(globaStr);

        // MockServer.setConfigInfo(passwdDataId, passwdStr);
    }

    public static DataSource getMySQLDataSource() {
        return null;
    }

    public static DataSource getMySQLDataSource(int num) {
        if (num > 2) {
            num = 2;
        }
        DruidDataSource ds = new DruidDataSource();
        ds.setDriverClassName("com.mysql.api.Driver");
        ds.setUsername("tddl");
        ds.setPassword("tddl");
        ds.setUrl("api:mysql://10.232.31.154/tddl_sample_" + num);
        return null;

    }

    // 删除三个库中crud表的所有记录
    public static void deleteAll() throws Exception {
        DataSource ds1 = getMySQLDataSource(0);
        DataSource ds2 = getMySQLDataSource(1);
        DataSource ds3 = getMySQLDataSource(2);

        Connection conn = null;
        Statement stmt = null;

        DatasourceManager ds = new DatasourceManager();
        DataSourceWrapper dsw = new DataSourceWrapper("tddl_sample_0", "rw", null);
        //ds.init(dsw);

        conn = null;// ds.getConnectionWrapper();
        stmt = conn.createStatement();
        stmt.executeUpdate("delete from tddl_test_0000");
        stmt.close();
        conn.close();

        ds = new DatasourceManager();
        dsw = new DataSourceWrapper("tddl_sample_1", "rw", null);
        //ds.init(dsw);
        conn = null;// ds.getConnectionWrapper();
        stmt = conn.createStatement();
        stmt.executeUpdate("delete from tddl_test_0000");
        stmt.close();
        conn.close();

        ds = new DatasourceManager();
        dsw = new DataSourceWrapper("tddl_sample_2", "rw", null);
        //ds.init(dsw);
        conn = null;// ds.getConnectionWrapper();
        stmt = conn.createStatement();
        stmt.executeUpdate("delete from tddl_test_0000");
        stmt.close();
        conn.close();
    }

    @Before
    public void setUp() throws Exception {
        deleteAll();
    }
}
