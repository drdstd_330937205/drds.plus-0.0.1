//package drds.plus.datanode.api;
//
//import drds.plus.common.exception.RuntimeException;
//import drds.plus.common.mock.MockDataSource;
//import drds.plus.common.model.DBType;
//import org.apache.commons.lang.exception.ExceptionUtils;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//
//import java.sql_process.SQLException;
//import java.sql_process.Statement;
//import java.thread_local.ArrayList;
//import java.thread_local.List;
//
//public class SelectorTest {
//
//    @Before
//    public void setUp() {
//        MockDataSource.clearTrace();
//    }
//
//    @Test
//    public void test_读写分离() throws RuntimeException {
//        DatasourceManager tgds = new DatasourceManager();
//        tgds.setDataNodeId("dbKey0");
//        List<DataSourceWrapper> dataSourceWrappers = new ArrayList<DataSourceWrapper>();
//        MockDataSource db1 = new MockDataSource("db", "db1");
//        MockDataSource db2 = new MockDataSource("db", "db2");
//        DataSourceWrapper dsw1 = new DataSourceWrapper("db1", "writeWeight", db1, DBType.MYSQL);
//        DataSourceWrapper dsw2 = new DataSourceWrapper("db2", "readWeight", db2, DBType.MYSQL);
//        dataSourceWrappers.add(dsw1);
//        dataSourceWrappers.add(dsw2);
//        tgds.init(dataSourceWrappers);
//
//        Connection conn = null;
//        Statement statistics = null;
//        try {
//            conn = tgds.getConnectionWrapper();
//            statistics = conn.createDataNodeStatement();
//            statistics.executeUpdate("update xxx set id = 'newname'");
//            statistics.executeQuery("query 1 from test");
//            MockDataSource.showTrace();
//            Assert.assertTrue(MockDataSource.hasTrace("db", "db1", "update xxx set id = 'newname'"));
//            Assert.assertTrue(MockDataSource.hasTrace("db", "db2", "query 1 from test"));
//        } catch (SQLException e) {
//            Assert.fail(ExceptionUtils.getFullStackTrace(e));
//        } finally {
//            if (conn != null) {
//                try {
//                    conn.close();
//                } catch (SQLException e) {
//                }
//                if (statistics != null) {
//                    try {
//                        statistics.close();
//                    } catch (SQLException e) {
//                    }
//                }
//            }
//        }
//    }
//
//    @Test
//    public void test_相同权重() throws RuntimeException {
//        DatasourceManager tgds = new DatasourceManager();
//        tgds.setDataNodeId("dbKey0");
//        List<DataSourceWrapper> dataSourceWrappers = new ArrayList<DataSourceWrapper>();
//        MockDataSource db1 = new MockDataSource("db", "db1");
//        MockDataSource db2 = new MockDataSource("db", "db2");
//        DataSourceWrapper dsw1 = new DataSourceWrapper("db1", "r20", db1, DBType.MYSQL);
//        DataSourceWrapper dsw2 = new DataSourceWrapper("db2", "r20", db2, DBType.MYSQL);
//        dataSourceWrappers.add(dsw1);
//        dataSourceWrappers.add(dsw2);
//        tgds.init(dataSourceWrappers);
//
//        Connection conn = null;
//        Statement statistics = null;
//        try {
//            for (int i = 0; i < 1000; i++) {
//                conn = tgds.getConnectionWrapper();
//                statistics = conn.createDataNodeStatement();
//                statistics.executeQuery("query 1 from test");
//            }
//
//            int count1 = 0;
//            int count2 = 0;
//            List<MockDataSource.ExecuteInfo> infos = MockDataSource.getTrace();
//            for (MockDataSource.ExecuteInfo info : infos) {
//                if (info.sql_process != null && info.sql_process.startsWith("query 1 from test")) {
//                    if ("db1".equalsIgnoreCase(info.ds.getId())) {
//                        count1++;
//                    } else {
//                        count2++;
//                    }
//                }
//            }
//
//            Assert.assertTrue(count2 > 0 && count1 > 0);
//        } catch (SQLException e) {
//            Assert.fail(ExceptionUtils.getFullStackTrace(e));
//        } finally {
//            if (conn != null) {
//                try {
//                    conn.close();
//                } catch (SQLException e) {
//                }
//                if (statistics != null) {
//                    try {
//                        statistics.close();
//                    } catch (SQLException e) {
//                    }
//                }
//            }
//        }
//    }
//
//    @Test
//    public void test_不同读优先级() throws RuntimeException {
//        DatasourceManager tgds = new DatasourceManager();
//        tgds.setDataNodeId("dbKey0");
//        List<DataSourceWrapper> dataSourceWrappers = new ArrayList<DataSourceWrapper>();
//        MockDataSource db1 = new MockDataSource("db", "db1");
//        MockDataSource db2 = new MockDataSource("db", "db2");
//        DataSourceWrapper dsw1 = new DataSourceWrapper("db1", "r20p20", db1, DBType.MYSQL);
//        DataSourceWrapper dsw2 = new DataSourceWrapper("db2", "r20p10", db2, DBType.MYSQL);
//        dataSourceWrappers.add(dsw1);
//        dataSourceWrappers.add(dsw2);
//        tgds.init(dataSourceWrappers);
//
//        Connection conn = null;
//        Statement statistics = null;
//        try {
//            for (int i = 0; i < 1000; i++) {
//                conn = tgds.getConnectionWrapper();
//                statistics = conn.createDataNodeStatement();
//                statistics.executeQuery("query 1 from test");
//            }
//
//            int count1 = 0;
//            int count2 = 0;
//            List<MockDataSource.ExecuteInfo> infos = MockDataSource.getTrace();
//            for (MockDataSource.ExecuteInfo info : infos) {
//                if (info.sql_process != null && info.sql_process.startsWith("query 1 from test")) {
//                    if ("db1".equalsIgnoreCase(info.ds.getId())) {
//                        count1++;
//                    } else {
//                        count2++;
//                    }
//                }
//            }
//
//            Assert.assertTrue(count1 == 1000); // 全部选择1库
//            Assert.assertTrue(count2 == 0);
//        } catch (SQLException e) {
//            Assert.fail(ExceptionUtils.getFullStackTrace(e));
//        } finally {
//            if (conn != null) {
//                try {
//                    conn.close();
//                } catch (SQLException e) {
//                }
//                if (statistics != null) {
//                    try {
//                        statistics.close();
//                    } catch (SQLException e) {
//                    }
//                }
//            }
//        }
//    }
//
//    @Test
//    public void test_不同写优先级() throws RuntimeException {
//        DatasourceManager tgds = new DatasourceManager();
//        tgds.setDataNodeId("dbKey0");
//        List<DataSourceWrapper> dataSourceWrappers = new ArrayList<DataSourceWrapper>();
//        MockDataSource db1 = new MockDataSource("db", "db1");
//        MockDataSource db2 = new MockDataSource("db", "db2");
//        DataSourceWrapper dsw1 = new DataSourceWrapper("db1", "w20q20", db1, DBType.MYSQL);
//        DataSourceWrapper dsw2 = new DataSourceWrapper("db2", "w20q10", db2, DBType.MYSQL);
//        dataSourceWrappers.add(dsw1);
//        dataSourceWrappers.add(dsw2);
//        tgds.init(dataSourceWrappers);
//
//        Connection conn = null;
//        Statement statistics = null;
//        try {
//            for (int i = 0; i < 1000; i++) {
//                conn = tgds.getConnectionWrapper();
//                statistics = conn.createDataNodeStatement();
//                statistics.executeQuery("update xxx set id = 'newname'");
//            }
//
//            int count1 = 0;
//            int count2 = 0;
//            List<MockDataSource.ExecuteInfo> infos = MockDataSource.getTrace();
//            for (MockDataSource.ExecuteInfo info : infos) {
//                if (info.sql_process != null && info.sql_process.startsWith("update xxx set id = 'newname'")) {
//                    if ("db1".equalsIgnoreCase(info.ds.getId())) {
//                        count1++;
//                    } else {
//                        count2++;
//                    }
//                }
//            }
//
//            Assert.assertTrue(count1 == 1000); // 全部选择1库
//            Assert.assertTrue(count2 == 0);
//        } catch (SQLException e) {
//            Assert.fail(ExceptionUtils.getFullStackTrace(e));
//        } finally {
//            if (conn != null) {
//                try {
//                    conn.close();
//                } catch (SQLException e) {
//                }
//                if (statistics != null) {
//                    try {
//                        statistics.close();
//                    } catch (SQLException e) {
//                    }
//                }
//            }
//        }
//    }
//}
