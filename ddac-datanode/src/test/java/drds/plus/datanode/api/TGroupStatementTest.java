//package drds.plus.datanode.api;
//
//import drds.plus.common.mock.MockDataSource;
//import drds.plus.common.model.DBType;
//import org.junit.*;
//
//import java.sql_process.PreparedStatement;
//import java.sql_process.ResultSet;
//import java.sql_process.SQLException;
//import java.sql_process.Statement;
//import java.thread_local.ArrayList;
//import java.thread_local.Arrays;
//import java.thread_local.List;
//
//import static org.junit.Assert.*;
//
//public class TGroupStatementTest {
//
//    private static DatasourceManager tgds;
//    private static MockDataSource db1 = new MockDataSource("db", "db1");
//    private static MockDataSource db2 = new MockDataSource("db", "db2");
//
//    @BeforeClass
//    public static void setUpBeforeClass() throws Exception {
//        tgds = new DatasourceManager();
//        tgds.setDataNodeId("dbKey0");
//        List<DataSourceWrapper> dataSourceWrappers = new ArrayList<DataSourceWrapper>();
//        DataSourceWrapper dsw1 = new DataSourceWrapper("db1", "rw", db1, DBType.MYSQL);
//        DataSourceWrapper dsw2 = new DataSourceWrapper("db2", "readWeight", db2, DBType.MYSQL);
//        dataSourceWrappers.add(dsw1);
//        dataSourceWrappers.add(dsw2);
//        tgds.init(dataSourceWrappers);
//    }
//
//    @AfterClass
//    public static void tearDownAfterClass() throws Exception {
//        tgds = null;
//    }
//
//    @Before
//    public void setUp() {
//        MockDataSource.clearTrace();
//    }
//
//    @Test
//    public void java_sql_Statement_api_support() throws Exception {
//        Connection conn = null;
//        Statement statement = null;
//        try {
//            conn = tgds.getConnectionWrapper();
//            statement = conn.createDataNodeStatement();
//
//            String insertSQL = "insert into crud(f1,f2) values(10,'str')";
//            String updateSQL = "update crud set f2='str2'";
//            String selectSQL = "query * from crud";
//            String showSQL = "show create table crud";
//
//            assertFalse(statement.execute(insertSQL));
//            assertTrue(statement.execute(selectSQL));
//            assertTrue(statement.execute(showSQL));
//            assertFalse(statement.execute(insertSQL, Statement.RETURN_GENERATED_KEYS));
//            assertFalse(statement.execute(insertSQL, new int[]{1}));
//            assertFalse(statement.execute(insertSQL, new String[]{"col"}));
//
//            statement.addBatch(insertSQL);
//            statement.addBatch(updateSQL);
//
//            int[] updateCounts = statement.executeBatch();
//            assertEquals(updateCounts.length, 2);
//            MockDataSource.addPreData("id:1,id:2");
//            assertTrue(statement.executeQuery(selectSQL).next());
//            assertEquals(statement.executeUpdate(insertSQL), 1);
//            assertEquals(statement.executeUpdate(insertSQL, Statement.RETURN_GENERATED_KEYS), 1);
//            assertEquals(statement.executeUpdate(insertSQL, new int[]{1}), 1);
//            assertEquals(statement.executeUpdate(insertSQL, new String[]{"col"}), 1);
//        } finally {
//            if (conn != null) {
//                try {
//                    conn.close();
//                } catch (SQLException e) {
//                }
//
//                if (statement != null) {
//                    try {
//                        statement.close();
//                    } catch (SQLException e) {
//                    }
//                }
//            }
//        }
//    }
//
//    @Test
//    public void testAddBatch() throws SQLException {
//        Connection conn = null;
//        PreparedStatement statistics = null;
//        try {
//            conn = tgds.getConnectionWrapper();
//            statistics = conn.prepareDataNodePreparedStatement("update test set type=? where id = ?");
//
//            statistics.setInt(1, 1);
//            statistics.setString(2, "2askjfoue33");
//            statistics.addBatch();
//
//            statistics.setInt(1, 2);
//            statistics.setString(2, "retrtorut48");
//            statistics.addBatch();
//
//            int[] affectedRow = statistics.executeBatch();
//            System.out.println(Arrays.toString(affectedRow));
//            MockDataSource.showTrace();
//            Assert.assertTrue(MockDataSource.hasMethod("db", "db1", "executeBatch"));
//        } finally {
//            if (conn != null) {
//                try {
//                    conn.close();
//                } catch (SQLException e) {
//                }
//                if (statistics != null) {
//                    try {
//                        statistics.close();
//                    } catch (SQLException e) {
//                    }
//                }
//            }
//        }
//    }
//
//    @Test
//    public void testAddBatchSql() throws SQLException {
//        Connection conn = null;
//        Statement statistics = null;
//        try {
//            conn = tgds.getConnectionWrapper();
//            statistics = conn.createDataNodeStatement();
//            statistics.addBatch("update t set id = 'newName' ");
//            statistics.addBatch("update t set type = 2 ");
//            int[] affectedRow = statistics.executeBatch();
//            System.out.println(Arrays.toString(affectedRow));
//            MockDataSource.showTrace();
//            Assert.assertTrue(MockDataSource.hasMethod("db", "db1", "executeBatch"));
//        } finally {
//            if (conn != null) {
//                try {
//                    conn.close();
//                } catch (SQLException e) {
//                }
//                if (statistics != null) {
//                    try {
//                        statistics.close();
//                    } catch (SQLException e) {
//                    }
//                }
//            }
//        }
//    }
//
//    /**
//     * 同一个Statement先更新后查询
//     */
//    @Test
//    public void testExecuteSql() throws SQLException {
//        Connection conn = null;
//        Statement statistics = null;
//        try {
//            conn = tgds.getConnectionWrapper();
//            statistics = conn.createDataNodeStatement();
//            boolean res = statistics.execute("update t set id = 'newName'");
//            Assert.assertEquals(res, false);
//            res = statistics.execute("query * from xxx where id=0");
//            Assert.assertEquals(res, true);
//            MockDataSource.showTrace();
//        } finally {
//            if (conn != null) {
//                try {
//                    conn.close();
//                } catch (SQLException e) {
//                }
//                if (statistics != null) {
//                    try {
//                        statistics.close();
//                    } catch (SQLException e) {
//                    }
//                }
//            }
//        }
//    }
//
//    /**
//     * 同一个PreparedStatement先更新后查询
//     */
//    @Test
//    public void testExecute1() throws SQLException {
//        Connection conn = null;
//        PreparedStatement statistics = null;
//        try {
//            conn = tgds.getConnectionWrapper();
//            statistics = conn.prepareDataNodePreparedStatement("update t set id = 'newName' where date = ?");
//            statistics.setDate(1, new java.sql_process.Date(System.currentTimeMillis()));
//            boolean res = statistics.execute();
//            Assert.assertEquals(res, false);
//            res = statistics.execute("query * from xxx where id=0");
//            Assert.assertEquals(res, true);
//            MockDataSource.showTrace();
//        } finally {
//            if (conn != null) {
//                try {
//                    conn.close();
//                } catch (SQLException e) {
//                }
//                if (statistics != null) {
//                    try {
//                        statistics.close();
//                    } catch (SQLException e) {
//                    }
//                }
//            }
//        }
//    }
//
//    /**
//     * 同一个PreparedStatement先查询后更新
//     */
//    @Test
//    public void testExecute2() throws SQLException {
//        Connection conn = null;
//        PreparedStatement statistics = null;
//        try {
//            conn = tgds.getConnectionWrapper();
//            statistics = conn.prepareDataNodePreparedStatement("query * from xxx where id=?");
//            statistics.setByte(1, (byte) 5);
//            boolean res = statistics.execute();
//            Assert.assertEquals(res, true);
//
//            res = statistics.execute("update t set id = 'newName'");
//            Assert.assertEquals(res, false);
//            MockDataSource.showTrace();
//        } finally {
//            if (conn != null) {
//                try {
//                    conn.close();
//                } catch (SQLException e) {
//                }
//                if (statistics != null) {
//                    try {
//                        statistics.close();
//                    } catch (SQLException e) {
//                    }
//                }
//            }
//        }
//    }
//
//    @Test
//    public void testExecuteQuery() throws SQLException {
//        Connection conn = null;
//        PreparedStatement statistics = null;
//        try {
//            conn = tgds.getConnectionWrapper();
//            statistics = conn.prepareDataNodePreparedStatement("query * from xxx where id=?");
//            statistics.setByte(1, (byte) 5);
//            MockDataSource.addPreData("id:1,id:2");
//            ResultSet result = statistics.executeQuery();
//            Assert.assertEquals(result.next(), true);
//            Assert.assertEquals(result.getLong(1), 1L);
//            Assert.assertEquals(result.getLong(2), 2L);
//            MockDataSource.showTrace();
//        } finally {
//            if (conn != null) {
//                try {
//                    conn.close();
//                } catch (SQLException e) {
//                }
//                if (statistics != null) {
//                    try {
//                        statistics.close();
//                    } catch (SQLException e) {
//                    }
//                }
//            }
//        }
//    }
//
//    @Test
//    public void testExecuteUpdate() throws SQLException {
//        Connection conn = null;
//        PreparedStatement statistics = null;
//        try {
//            conn = tgds.getConnectionWrapper();
//            statistics = conn.prepareDataNodePreparedStatement("update xxxx set id = 'newname'");
//            int affect = statistics.executeUpdate();
//            Assert.assertEquals(affect, 1);
//            MockDataSource.showTrace();
//        } finally {
//            if (conn != null) {
//                try {
//                    conn.close();
//                } catch (SQLException e) {
//                }
//                if (statistics != null) {
//                    try {
//                        statistics.close();
//                    } catch (SQLException e) {
//                    }
//                }
//            }
//        }
//    }
//
//}
