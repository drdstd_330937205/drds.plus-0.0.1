//package drds.plus.datanode.api;
//
//import drds.plus.common.GroupDataSourceRouteHelper;
//import drds.plus.common.exception.RuntimeException;
//import drds.plus.common.exception.NestableRuntimeException;
//import drds.plus.common.mock.MockDataSource;
//import drds.plus.common.model.DBType;
//import org.junit.Assert;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import org.springframework.api.core.ColumnMapRowMapper;
//import org.springframework.api.core.JdbcTemplate;
//
//import javax.sql_process.DataSource;
//
///**
// * @author linxuan
// */
//public class ThreadLocalDataSourceIndexTest {
//
//    @BeforeClass
//    public static void beforeClass() {
//    }
//
//    private static MockDataSource createMockDataSource(String id) {
//        MockDataSource mds = new MockDataSource();
//        mds.setId(id);
//        mds.setDbIndex("");
//        return mds;
//    }
//
//    private static DatasourceManager createGroupDataSource(String weightStr) {
//        DatasourceManager tgds = new DatasourceManager();
//        tgds.setDataSourceIdAndWeightStringArrayString(weightStr);
//        tgds.setDbType(DBType.MYSQL);
//        tgds.setDataSourceFetcher(new DataSourceFetcher() {
//
//
//            public DataSource getDataSourceWrapper(String key) {
//                return createMockDataSource(key);
//            }
//
//
//            public DBType getDataSourceDBType(String key) {
//                return null;
//            }
//        });
//        try {
//            tgds.init();
//        } catch (RuntimeException e) {
//            throw new NestableRuntimeException(e);
//        }
//        return tgds;
//    }
//
//    @Test
//    public void test_不设i() {
//        JdbcTemplate jt = new JdbcTemplate(createGroupDataSource("ds0:rw, ds1:readWeight, ds2:readWeight, ds3:readWeight"));
//
//        MockDataSource.clearTrace();
//        GroupDataSourceRouteHelper.executeByGroupDataSourceIndex(1);
//        jt.query("query 1 from dual", new Object[]{}, new ColumnMapRowMapper());
//        MockDataSource.showTrace();
//        Assert.assertTrue(MockDataSource.hasTrace("", "ds1", "query 1 from dual"));
//
//        MockDataSource.clearTrace();
//        GroupDataSourceRouteHelper.executeByGroupDataSourceIndex(2);
//        jt.query("query 1 from dual", new Object[]{}, new ColumnMapRowMapper());
//        MockDataSource.showTrace();
//        Assert.assertTrue(MockDataSource.hasTrace("", "ds2", "query 1 from dual"));
//    }
//
//    @Test
//    public void test_设单个i不加数字等同于没设() {
//        JdbcTemplate jt = new JdbcTemplate(createGroupDataSource("ds0:rwi, ds1:ri, ds2:ri, ds3:ri"));
//
//        MockDataSource.clearTrace();
//        GroupDataSourceRouteHelper.executeByGroupDataSourceIndex(1);
//        jt.query("query 1 from dual", new Object[]{}, new ColumnMapRowMapper());
//        MockDataSource.showTrace();
//        Assert.assertTrue(MockDataSource.hasTrace("", "ds1", "query 1 from dual"));
//    }
//
//    @Test
//    public void test_设单个in() {
//        JdbcTemplate jt = new JdbcTemplate(createGroupDataSource("ds0:rwi5, ds1:ri6, ds2:ri7, ds3:ri8"));
//
//        MockDataSource.clearTrace();
//        GroupDataSourceRouteHelper.executeByGroupDataSourceIndex(6);
//        jt.query("query 1 from dual", new Object[]{}, new ColumnMapRowMapper());
//        MockDataSource.showTrace();
//        Assert.assertTrue(MockDataSource.hasTrace("", "ds1", "query 1 from dual"));
//
//        MockDataSource.clearTrace();
//        GroupDataSourceRouteHelper.executeByGroupDataSourceIndex(8);
//        jt.query("query 1 from dual", new Object[]{}, new ColumnMapRowMapper());
//        MockDataSource.showTrace();
//        Assert.assertTrue(MockDataSource.hasTrace("", "ds3", "query 1 from dual"));
//    }
//
//    @Test
//    public void test_设多个i分流() {
//        JdbcTemplate jt = new JdbcTemplate(createGroupDataSource("ds0:rwi0, ds1:ri0, ds2:ri1, ds3:ri1"));
//
//        MockDataSource.clearTrace();
//        GroupDataSourceRouteHelper.executeByGroupDataSourceIndex(0);
//        jt.query("query 1 from dual", new Object[]{}, new ColumnMapRowMapper());
//        MockDataSource.showTrace();
//        Assert.assertTrue(MockDataSource.hasTrace("", "ds0", "query") || MockDataSource.hasTrace("", "ds1", "query"));
//
//        MockDataSource.clearTrace();
//        GroupDataSourceRouteHelper.executeByGroupDataSourceIndex(1);
//        jt.query("query 1 from dual", new Object[]{}, new ColumnMapRowMapper());
//        MockDataSource.showTrace();
//        Assert.assertTrue(MockDataSource.hasTrace("", "ds2", "query") || MockDataSource.hasTrace("", "ds3", "query"));
//    }
//
//    @Test
//    public void test_1个ds设多个i() {
//        JdbcTemplate jt = new JdbcTemplate(createGroupDataSource("ds0:rwi0, ds1:ri0i1, ds2:ri1, ds3:readWeight, ds4:ri3"));
//
//        MockDataSource.clearTrace();
//        GroupDataSourceRouteHelper.executeByGroupDataSourceIndex(0);
//        jt.query("query 1 from dual", new Object[]{}, new ColumnMapRowMapper());
//        MockDataSource.showTrace();
//        Assert.assertTrue(MockDataSource.hasTrace("", "ds0", "query") || MockDataSource.hasTrace("", "ds1", "query"));
//
//        MockDataSource.clearTrace();
//        GroupDataSourceRouteHelper.executeByGroupDataSourceIndex(1);
//        jt.query("query 1 from dual", new Object[]{}, new ColumnMapRowMapper());
//        MockDataSource.showTrace();
//        Assert.assertTrue(MockDataSource.hasTrace("", "ds1", "query") || MockDataSource.hasTrace("", "ds2", "query"));
//
//        MockDataSource.clearTrace();
//        GroupDataSourceRouteHelper.executeByGroupDataSourceIndex(3);
//        jt.query("query 1 from dual", new Object[]{}, new ColumnMapRowMapper());
//        MockDataSource.showTrace();
//        Assert.assertTrue(MockDataSource.hasTrace("", "ds3", "query") || MockDataSource.hasTrace("", "ds4", "query"));
//    }
//}
