//package drds.plus.datanode.api;
//
//import drds.plus.common.exception.RuntimeException;
//import drds.plus.common.mock.MockDataSource;
//import drds.plus.common.model.DBType;
//import org.apache.commons.lang.exception.ExceptionUtils;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//
//import java.sql_process.Session;
//import java.sql_process.ResultSet;
//import java.sql_process.SQLException;
//import java.sql_process.Statement;
//import java.thread_local.ArrayList;
//import java.thread_local.List;
//
//import static org.junit.Assert.*;
//
///**
// * @author yangzhu
// */
//public class TGroupConnectionTest {
//
//    @Before
//    public void setUp() {
//        MockDataSource.clearTrace();
//    }
//
//    @Test
//    public void java_sql_Connection_api_support() throws Exception {
//        DatasourceManager ds = new DatasourceManager();
//
//        Session conn = ds.getConnectionWrapper();
//        assertFalse(conn.isClosed());
//        assertTrue(conn.getAutoCommit());
//        assertNull(conn.getWarnings());
//        assertTrue((conn.getResultSetMetaData() instanceof TGroupDatabaseMetaData));
//
//        assertTrue((conn.createDataNodeStatement() instanceof Statement));
//        assertTrue((conn.createDataNodeStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE) instanceof Statement));
//        assertTrue((conn.createDataNodeStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
//                ResultSet.CONCUR_UPDATABLE,
//                ResultSet.HOLD_CURSORS_OVER_COMMIT) instanceof Statement));
//
//        assertTrue((conn.prepareDataNodePreparedStatement("sql_process") instanceof PreparedStatement));
//        assertTrue((conn.prepareDataNodePreparedStatement("sql_process", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE) instanceof PreparedStatement));
//        assertTrue((conn.prepareDataNodePreparedStatement("sql_process",
//                ResultSet.TYPE_SCROLL_INSENSITIVE,
//                ResultSet.CONCUR_UPDATABLE,
//                ResultSet.HOLD_CURSORS_OVER_COMMIT) instanceof PreparedStatement));
//        assertTrue((conn.prepareDataNodePreparedStatement("sql_process", Statement.RETURN_GENERATED_KEYS) instanceof PreparedStatement));
//        assertTrue((conn.prepareDataNodePreparedStatement("sql_process", new int[0]) instanceof PreparedStatement));
//        assertTrue((conn.prepareDataNodePreparedStatement("sql_process", new String[0]) instanceof PreparedStatement));
//
//        // assertTrue((conn.prepareCall("sql_process") instanceof
//        // TGroupCallableStatement));
//        // assertTrue((conn.prepareCall("sql_process",
//        // ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)
//        // instanceof TGroupCallableStatement));
//        // assertTrue((conn.prepareCall("sql_process",
//        // ResultSet.TYPE_SCROLL_INSENSITIVE,
//        // ResultSet.CONCUR_UPDATABLE,
//        // ResultSet.HOLD_CURSORS_OVER_COMMIT) instanceof
//        // TGroupCallableStatement));
//    }
//
//    @Test
//    public void test_一个连接上创建两个Statement() throws RuntimeException {
//        DatasourceManager tgds = new DatasourceManager();
//        tgds.setDataNodeId("dbKey0");
//        List<DataSourceWrapper> dataSourceWrappers = new ArrayList<DataSourceWrapper>();
//        MockDataSource db1 = new MockDataSource("db", "db1");
//        MockDataSource db2 = new MockDataSource("db", "db2");
//        DataSourceWrapper dsw1 = new DataSourceWrapper("db1", "rw", db1, DBType.MYSQL);
//        DataSourceWrapper dsw2 = new DataSourceWrapper("db2", "readWeight", db2, DBType.MYSQL);
//        dataSourceWrappers.add(dsw1);
//        dataSourceWrappers.add(dsw2);
//        tgds.init(dataSourceWrappers);
//
//        Connection conn = null;
//        Statement statistics = null;
//        try {
//            db1.setClosed(true);
//            db2.setClosed(false);
//            // 链接一旦选定就会保持所选择的库，直到close
//            conn = tgds.getConnectionWrapper();
//            statistics = conn.createDataNodeStatement();
//            statistics.executeQuery("query 1 from test");
//            MockDataSource.showTrace();
//            Assert.assertTrue(MockDataSource.hasTrace("db", "db2", "query 1 from test"));
//
//            db1.setClosed(false);
//            db2.setClosed(true);
//            statistics = conn.createDataNodeStatement();
//            statistics.executeQuery("query 2 from test");
//            // Assert.assertTrue(MockDataSource.hasTrace("db", "db1",
//            // "query 1 from test"));
//            Assert.fail("没有重用第一个连接");
//        } catch (SQLException e) {
//            // Assert.fail(ExceptionUtils.getFullStackTrace(e));
//            System.out.println(e.getMessage());
//        } finally {
//            if (conn != null) {
//                try {
//                    conn.close();
//                } catch (SQLException e) {
//                }
//                if (statistics != null) {
//                    try {
//                        statistics.close();
//                    } catch (SQLException e) {
//                    }
//                }
//            }
//        }
//    }
//
//    @Test
//    public void test_创建Statement失败重试_读请求() throws RuntimeException {
//        DatasourceManager tgds = new DatasourceManager();
//        tgds.setDataNodeId("dbKey0");
//        List<DataSourceWrapper> dataSourceWrappers = new ArrayList<DataSourceWrapper>();
//        MockDataSource db1 = new MockDataSource("db", "db1");
//        MockDataSource db2 = new MockDataSource("db", "db2");
//        DataSourceWrapper dsw1 = new DataSourceWrapper("db1", "rw", db1, DBType.MYSQL);
//        DataSourceWrapper dsw2 = new DataSourceWrapper("db2", "readWeight", db2, DBType.MYSQL);
//        dataSourceWrappers.add(dsw1);
//        dataSourceWrappers.add(dsw2);
//        tgds.init(dataSourceWrappers);
//
//        Connection conn = null;
//        Statement statistics = null;
//        try {
//            conn = tgds.getConnectionWrapper();
//            statistics = conn.createDataNodeStatement();
//            MockDataSource.addPreException(MockDataSource.m_createStatement, db1.genFatalSQLException());
//            statistics.executeQuery("query 1 from test");
//            MockDataSource.showTrace();
//            Assert.assertTrue(MockDataSource.hasMethod("db", "db1", "getConnectionWrapper"));
//            // 会在db2做重试
//            Assert.assertTrue(MockDataSource.hasMethod("db", "db2", "getConnectionWrapper"));
//        } catch (SQLException e) {
//            Assert.fail(ExceptionUtils.getFullStackTrace(e));
//            // System.out.println(e.getMessage());
//        } finally {
//            if (conn != null) {
//                try {
//                    conn.close();
//                } catch (SQLException e) {
//                }
//                if (statistics != null) {
//                    try {
//                        statistics.close();
//                    } catch (SQLException e) {
//                    }
//                }
//            }
//        }
//    }
//
//    @Test
//    public void test_创建Statement失败不重试_写请求() throws RuntimeException {
//        DatasourceManager tgds = new DatasourceManager();
//        tgds.setDataNodeId("dbKey0");
//        List<DataSourceWrapper> dataSourceWrappers = new ArrayList<DataSourceWrapper>();
//        MockDataSource db1 = new MockDataSource("db", "db1");
//        MockDataSource db2 = new MockDataSource("db", "db2");
//        DataSourceWrapper dsw1 = new DataSourceWrapper("db1", "rw", db1, DBType.MYSQL);
//        DataSourceWrapper dsw2 = new DataSourceWrapper("db2", "readWeight", db2, DBType.MYSQL);
//        dataSourceWrappers.add(dsw1);
//        dataSourceWrappers.add(dsw2);
//        tgds.init(dataSourceWrappers);
//
//        Connection conn = null;
//        Statement statistics = null;
//        try {
//            conn = tgds.getConnectionWrapper();
//            statistics = conn.createDataNodeStatement();
//            MockDataSource.addPreException(MockDataSource.m_createStatement, db1.genFatalSQLException());
//            statistics.executeQuery("update test set id = 'newname'");
//        } catch (SQLException e) {
//            // Assert.fail(ExceptionUtils.getFullStackTrace(e));
//            System.out.println(e.getMessage());
//            Assert.assertTrue(MockDataSource.hasMethod("db", "db1", "getConnectionWrapper"));
//            // 写操作不会在db2做重试
//            Assert.assertFalse(MockDataSource.hasMethod("db", "db2", "getConnectionWrapper"));
//        } finally {
//            if (conn != null) {
//                try {
//                    conn.close();
//                } catch (SQLException e) {
//                }
//                if (statistics != null) {
//                    try {
//                        statistics.close();
//                    } catch (SQLException e) {
//                    }
//                }
//            }
//        }
//    }
//
//    @Test
//    public void test_autocommit() throws RuntimeException {
//        DatasourceManager tgds = new DatasourceManager();
//        tgds.setDataNodeId("dbKey0");
//        List<DataSourceWrapper> dataSourceWrappers = new ArrayList<DataSourceWrapper>();
//        MockDataSource db1 = new MockDataSource("db", "db1");
//        MockDataSource db2 = new MockDataSource("db", "db2");
//        DataSourceWrapper dsw1 = new DataSourceWrapper("db1", "writeWeight", db1, DBType.MYSQL);
//        DataSourceWrapper dsw2 = new DataSourceWrapper("db2", "readWeight", db2, DBType.MYSQL);
//        dataSourceWrappers.add(dsw1);
//        dataSourceWrappers.add(dsw2);
//        tgds.init(dataSourceWrappers);
//
//        Connection conn = null;
//        Statement statistics = null;
//        try {
//            conn = tgds.getConnectionWrapper();
//            statistics = conn.createDataNodeStatement();
//            statistics.executeQuery("query 1 from test");
//            conn.setAutoCommit(false);
//            statistics.executeUpdate("update t set id='newName'");
//            conn.commit();
//        } catch (SQLException e) {
//            Assert.fail(ExceptionUtils.getFullStackTrace(e));
//        } finally {
//            if (conn != null) {
//                try {
//                    conn.close();
//                } catch (SQLException e) {
//                }
//                if (statistics != null) {
//                    try {
//                        statistics.close();
//                    } catch (SQLException e) {
//                    }
//                }
//            }
//        }
//        MockDataSource.showTrace();
//        Assert.assertTrue(MockDataSource.hasMethod("db", "db1", "getConnectionWrapper"));
//        Assert.assertTrue(MockDataSource.hasMethod("db", "db2", "getConnectionWrapper"));
//        Assert.assertTrue(MockDataSource.hasMethod("db", "db1", "setAutoCommit"));
//        Assert.assertTrue(MockDataSource.hasMethod("db", "db1", "commit"));
//        Assert.assertFalse(MockDataSource.hasMethod("db", "db1", "rollback"));
//        Assert.assertFalse(MockDataSource.hasMethod("db", "db2", "setAutoCommit"));
//        Assert.assertFalse(MockDataSource.hasMethod("db", "db2", "commit"));
//        Assert.assertFalse(MockDataSource.hasMethod("db", "db2", "rollback"));
//    }
//
//    @Test
//    public void test_no_trans() throws RuntimeException {
//        DatasourceManager tgds = new DatasourceManager();
//        tgds.setDataNodeId("dbKey0");
//        List<DataSourceWrapper> dataSourceWrappers = new ArrayList<DataSourceWrapper>();
//        MockDataSource db1 = new MockDataSource("db", "db1");
//        MockDataSource db2 = new MockDataSource("db", "db2");
//        DataSourceWrapper dsw1 = new DataSourceWrapper("db1", "writeWeight", db1, DBType.MYSQL);
//        DataSourceWrapper dsw2 = new DataSourceWrapper("db2", "readWeight", db2, DBType.MYSQL);
//        dataSourceWrappers.add(dsw1);
//        dataSourceWrappers.add(dsw2);
//        tgds.init(dataSourceWrappers);
//
//        Connection conn = null;
//        Statement statistics = null;
//        try {
//            conn = tgds.getConnectionWrapper();
//            statistics = conn.createDataNodeStatement();
//            statistics.executeQuery("query 1 from test");
//        } catch (SQLException e) {
//            Assert.fail(ExceptionUtils.getFullStackTrace(e));
//        } finally {
//            if (conn != null) {
//                try {
//                    conn.close();
//                } catch (SQLException e) {
//                }
//                if (statistics != null) {
//                    try {
//                        statistics.close();
//                    } catch (SQLException e) {
//                    }
//                }
//            }
//        }
//        MockDataSource.showTrace();
//        Assert.assertFalse(MockDataSource.hasMethod("db", "db1", "getConnectionWrapper"));
//        Assert.assertTrue(MockDataSource.hasMethod("db", "db2", "getConnectionWrapper"));
//        Assert.assertFalse(MockDataSource.hasMethod("db", "db1", "setAutoCommit"));
//        Assert.assertFalse(MockDataSource.hasMethod("db", "db1", "commit"));
//        Assert.assertFalse(MockDataSource.hasMethod("db", "db1", "rollback"));
//        Assert.assertFalse(MockDataSource.hasMethod("db", "db2", "setAutoCommit"));
//        Assert.assertFalse(MockDataSource.hasMethod("db", "db2", "commit"));
//        Assert.assertFalse(MockDataSource.hasMethod("db", "db2", "rollback"));
//    }
//
//    @Test
//    public void test_write_trans() throws RuntimeException {
//        DatasourceManager tgds = new DatasourceManager();
//        tgds.setDataNodeId("dbKey0");
//        List<DataSourceWrapper> dataSourceWrappers = new ArrayList<DataSourceWrapper>();
//        MockDataSource db1 = new MockDataSource("db", "db1");
//        MockDataSource db2 = new MockDataSource("db", "db2");
//        DataSourceWrapper dsw1 = new DataSourceWrapper("db1", "writeWeight", db1, DBType.MYSQL);
//        DataSourceWrapper dsw2 = new DataSourceWrapper("db2", "readWeight", db2, DBType.MYSQL);
//        dataSourceWrappers.add(dsw1);
//        dataSourceWrappers.add(dsw2);
//        tgds.init(dataSourceWrappers);
//
//        Connection conn = null;
//        Statement statistics = null;
//        try {
//            conn = tgds.getConnectionWrapper();
//            statistics = conn.createDataNodeStatement();
//            conn.setAutoCommit(false);
//            statistics.executeUpdate("update t set id='newName'");
//            conn.commit();
//        } catch (SQLException e) {
//            Assert.fail(ExceptionUtils.getFullStackTrace(e));
//        } finally {
//            if (conn != null) {
//                try {
//                    conn.close();
//                } catch (SQLException e) {
//                }
//                if (statistics != null) {
//                    try {
//                        statistics.close();
//                    } catch (SQLException e) {
//                    }
//                }
//            }
//        }
//        MockDataSource.showTrace();
//        Assert.assertTrue(MockDataSource.hasMethod("db", "db1", "getConnectionWrapper"));
//        Assert.assertFalse(MockDataSource.hasMethod("db", "db2", "getConnectionWrapper"));
//        Assert.assertTrue(MockDataSource.hasMethod("db", "db1", "setAutoCommit"));
//        Assert.assertTrue(MockDataSource.hasMethod("db", "db1", "commit"));
//        Assert.assertFalse(MockDataSource.hasMethod("db", "db1", "rollback"));
//        Assert.assertFalse(MockDataSource.hasMethod("db", "db2", "setAutoCommit"));
//        Assert.assertFalse(MockDataSource.hasMethod("db", "db2", "commit"));
//        Assert.assertFalse(MockDataSource.hasMethod("db", "db2", "rollback"));
//    }
//
//    @Test
//    public void test_read_trans() throws RuntimeException {
//        DatasourceManager tgds = new DatasourceManager();
//        tgds.setDataNodeId("dbKey0");
//        List<DataSourceWrapper> dataSourceWrappers = new ArrayList<DataSourceWrapper>();
//        MockDataSource db1 = new MockDataSource("db", "db1");
//        MockDataSource db2 = new MockDataSource("db", "db2");
//        DataSourceWrapper dsw1 = new DataSourceWrapper("db1", "writeWeight", db1, DBType.MYSQL);
//        DataSourceWrapper dsw2 = new DataSourceWrapper("db2", "readWeight", db2, DBType.MYSQL);
//        dataSourceWrappers.add(dsw1);
//        dataSourceWrappers.add(dsw2);
//        tgds.init(dataSourceWrappers);
//
//        Connection conn = null;
//        Statement statistics = null;
//        try {
//            conn = tgds.getConnectionWrapper();
//            statistics = conn.createDataNodeStatement();
//            conn.setAutoCommit(false);
//            statistics.executeQuery("query 1 from test");
//            conn.commit();
//        } catch (SQLException e) {
//            Assert.fail(ExceptionUtils.getFullStackTrace(e));
//        } finally {
//            if (conn != null) {
//                try {
//                    conn.close();
//                } catch (SQLException e) {
//                }
//                if (statistics != null) {
//                    try {
//                        statistics.close();
//                    } catch (SQLException e) {
//                    }
//                }
//            }
//        }
//        MockDataSource.showTrace();
//        // 如果是事务读，强制选择了写库
//        Assert.assertTrue(MockDataSource.hasMethod("db", "db1", "getConnectionWrapper"));
//        Assert.assertFalse(MockDataSource.hasMethod("db", "db2", "getConnectionWrapper"));
//        Assert.assertTrue(MockDataSource.hasMethod("db", "db1", "setAutoCommit"));
//        Assert.assertTrue(MockDataSource.hasMethod("db", "db1", "commit"));
//        Assert.assertFalse(MockDataSource.hasMethod("db", "db1", "rollback"));
//        Assert.assertFalse(MockDataSource.hasMethod("db", "db2", "setAutoCommit"));
//        Assert.assertFalse(MockDataSource.hasMethod("db", "db2", "commit"));
//        Assert.assertFalse(MockDataSource.hasMethod("db", "db2", "rollback"));
//    }
//
//    @Test
//    public void test_write_and_read_trans() throws RuntimeException {
//        DatasourceManager tgds = new DatasourceManager();
//        tgds.setDataNodeId("dbKey0");
//        List<DataSourceWrapper> dataSourceWrappers = new ArrayList<DataSourceWrapper>();
//        MockDataSource db1 = new MockDataSource("db", "db1");
//        MockDataSource db2 = new MockDataSource("db", "db2");
//        DataSourceWrapper dsw1 = new DataSourceWrapper("db1", "writeWeight", db1, DBType.MYSQL);
//        DataSourceWrapper dsw2 = new DataSourceWrapper("db2", "readWeight", db2, DBType.MYSQL);
//        dataSourceWrappers.add(dsw1);
//        dataSourceWrappers.add(dsw2);
//        tgds.init(dataSourceWrappers);
//
//        Connection conn = null;
//        Statement statistics = null;
//        try {
//            conn = tgds.getConnectionWrapper();
//            statistics = conn.createDataNodeStatement();
//            conn.setAutoCommit(false);
//            statistics.executeQuery("update t set id='newName'");
//            statistics.executeQuery("query 1 from test");
//            conn.commit();
//        } catch (SQLException e) {
//            Assert.fail(ExceptionUtils.getFullStackTrace(e));
//        } finally {
//            if (conn != null) {
//                try {
//                    conn.close();
//                } catch (SQLException e) {
//                }
//                if (statistics != null) {
//                    try {
//                        statistics.close();
//                    } catch (SQLException e) {
//                    }
//                }
//            }
//        }
//        MockDataSource.showTrace();
//        // 事务中的读请求，选择写库
//        Assert.assertTrue(MockDataSource.hasMethod("db", "db1", "getConnectionWrapper"));
//        Assert.assertFalse(MockDataSource.hasMethod("db", "db2", "getConnectionWrapper"));
//        Assert.assertTrue(MockDataSource.hasMethod("db", "db1", "setAutoCommit"));
//        Assert.assertTrue(MockDataSource.hasMethod("db", "db1", "commit"));
//        Assert.assertFalse(MockDataSource.hasMethod("db", "db1", "rollback"));
//        Assert.assertFalse(MockDataSource.hasMethod("db", "db2", "setAutoCommit"));
//        Assert.assertFalse(MockDataSource.hasMethod("db", "db2", "commit"));
//        Assert.assertFalse(MockDataSource.hasMethod("db", "db2", "rollback"));
//    }
//
//    @Test
//    public void test_read_and_write_trans() throws RuntimeException {
//        DatasourceManager tgds = new DatasourceManager();
//        tgds.setDataNodeId("dbKey0");
//        List<DataSourceWrapper> dataSourceWrappers = new ArrayList<DataSourceWrapper>();
//        MockDataSource db1 = new MockDataSource("db", "db1");
//        MockDataSource db2 = new MockDataSource("db", "db2");
//        DataSourceWrapper dsw1 = new DataSourceWrapper("db1", "writeWeight", db1, DBType.MYSQL);
//        DataSourceWrapper dsw2 = new DataSourceWrapper("db2", "readWeight", db2, DBType.MYSQL);
//        dataSourceWrappers.add(dsw1);
//        dataSourceWrappers.add(dsw2);
//        tgds.init(dataSourceWrappers);
//
//        Connection conn = null;
//        Statement statistics = null;
//        try {
//            conn = tgds.getConnectionWrapper();
//            statistics = conn.createDataNodeStatement();
//            conn.setAutoCommit(false);
//            statistics.executeQuery("query 1 from test");
//            statistics.executeQuery("update t set id='newName'");
//            conn.commit();
//        } catch (SQLException e) {
//            Assert.fail(ExceptionUtils.getFullStackTrace(e));
//        } finally {
//            if (conn != null) {
//                try {
//                    conn.close();
//                } catch (SQLException e) {
//                }
//                if (statistics != null) {
//                    try {
//                        statistics.close();
//                    } catch (SQLException e) {
//                    }
//                }
//            }
//        }
//        MockDataSource.showTrace();
//        Assert.assertTrue(MockDataSource.hasMethod("db", "db1", "getConnectionWrapper"));
//        Assert.assertFalse(MockDataSource.hasMethod("db", "db2", "getConnectionWrapper"));
//        Assert.assertTrue(MockDataSource.hasMethod("db", "db1", "setAutoCommit"));
//        Assert.assertTrue(MockDataSource.hasMethod("db", "db1", "commit"));
//        Assert.assertFalse(MockDataSource.hasMethod("db", "db1", "rollback"));
//        Assert.assertFalse(MockDataSource.hasMethod("db", "db2", "setAutoCommit"));
//        Assert.assertFalse(MockDataSource.hasMethod("db", "db2", "commit"));
//        Assert.assertFalse(MockDataSource.hasMethod("db", "db2", "rollback"));
//    }
//
//    @Test
//    public void test_read_untrans_write_trans() throws RuntimeException {
//        DatasourceManager tgds = new DatasourceManager();
//        tgds.setDataNodeId("dbKey0");
//        List<DataSourceWrapper> dataSourceWrappers = new ArrayList<DataSourceWrapper>();
//        MockDataSource db1 = new MockDataSource("db", "db1");
//        MockDataSource db2 = new MockDataSource("db", "db2");
//        DataSourceWrapper dsw1 = new DataSourceWrapper("db1", "writeWeight", db1, DBType.MYSQL);
//        DataSourceWrapper dsw2 = new DataSourceWrapper("db2", "readWeight", db2, DBType.MYSQL);
//        dataSourceWrappers.add(dsw1);
//        dataSourceWrappers.add(dsw2);
//        tgds.init(dataSourceWrappers);
//
//        Connection conn = null;
//        Statement statistics = null;
//        try {
//            conn = tgds.getConnectionWrapper();
//            statistics = conn.createDataNodeStatement();
//            statistics.executeQuery("query 1 from test");
//            conn.setAutoCommit(false);
//            statistics.executeQuery("update t set id='newName'");
//            conn.commit();
//        } catch (SQLException e) {
//            Assert.fail(ExceptionUtils.getFullStackTrace(e));
//        } finally {
//            if (conn != null) {
//                try {
//                    conn.close();
//                } catch (SQLException e) {
//                }
//                if (statistics != null) {
//                    try {
//                        statistics.close();
//                    } catch (SQLException e) {
//                    }
//                }
//            }
//        }
//        MockDataSource.showTrace();
//        // 刚开始读不处于事务中，选择读请求
//        Assert.assertTrue(MockDataSource.hasMethod("db", "db1", "getConnectionWrapper"));
//        Assert.assertTrue(MockDataSource.hasMethod("db", "db2", "getConnectionWrapper"));
//        Assert.assertTrue(MockDataSource.hasMethod("db", "db1", "setAutoCommit"));
//        Assert.assertTrue(MockDataSource.hasMethod("db", "db1", "commit"));
//        Assert.assertFalse(MockDataSource.hasMethod("db", "db1", "rollback"));
//        Assert.assertFalse(MockDataSource.hasMethod("db", "db2", "setAutoCommit"));
//        Assert.assertFalse(MockDataSource.hasMethod("db", "db2", "commit"));
//        Assert.assertFalse(MockDataSource.hasMethod("db", "db2", "rollback"));
//    }
//}
