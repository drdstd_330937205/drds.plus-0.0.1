package drds.plus.datanode.configuration;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author yangzhu
 */
public class WeightUnitTest {

    @Test
    public void all() {
        Weight w = new Weight(null);
        Assert.assertEquals(w.readWeight, 10);
        Assert.assertEquals(w.writeWeight, 10);
        Assert.assertEquals(w.readPriority, 0);
        Assert.assertEquals(w.writePriority, 0);

        w = new Weight("");
        Assert.assertEquals(w.readWeight, 0);
        Assert.assertEquals(w.writeWeight, 0);
        Assert.assertEquals(w.readPriority, 0);
        Assert.assertEquals(w.writePriority, 0);

        w = new Weight("   ");
        Assert.assertEquals(w.readWeight, 0);
        Assert.assertEquals(w.writeWeight, 0);
        Assert.assertEquals(w.readPriority, 0);
        Assert.assertEquals(w.writePriority, 0);

        w = new Weight("rwpq");
        Assert.assertEquals(w.readWeight, 10);
        Assert.assertEquals(w.writeWeight, 10);
        Assert.assertEquals(w.readPriority, 0);
        Assert.assertEquals(w.writePriority, 0);

        w = new Weight("");
        Assert.assertEquals(w.readWeight, 0);
        Assert.assertEquals(w.writeWeight, 0);
        Assert.assertEquals(w.readPriority, 0);
        Assert.assertEquals(w.writePriority, 0);

        w = new Weight("r10w20p1q2");
        Assert.assertEquals(w.readWeight, 10);
        Assert.assertEquals(w.writeWeight, 20);
        Assert.assertEquals(w.readPriority, 1);
        Assert.assertEquals(w.writePriority, 2);

        w = new Weight("R10W20P1Q2");
        Assert.assertEquals(w.readWeight, 10);
        Assert.assertEquals(w.writeWeight, 20);
        Assert.assertEquals(w.readPriority, 1);
        Assert.assertEquals(w.writePriority, 2);
    }
}
