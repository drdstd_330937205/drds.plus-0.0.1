package drds.plus.sql_process.sequence;


import drds.plus.common.lifecycle.AbstractLifecycle;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;

@Slf4j
public class SequenceManager extends AbstractLifecycle implements ISequenceManager {
    private static ISequenceManager sequenceManager;

    public static ISequenceManager getSequenceManager() {
        if (sequenceManager == null) {
            synchronized (SequenceManager.class) {
                if (sequenceManager == null) {
                    try {
                        /**
                         * 查找真实的实现.关于查找实现。会专门一个类，在启动的时候校验该类是否存在,要求其class static块里面为空才能保证加载成功。
                         */
                        sequenceManager = (ISequenceManager) Class.forName("drds.plus.executor.sequence.SequenceManager").newInstance();
                    } catch (Exception e) {
                        e.printStackTrace();

                        throw new RuntimeException(e);
                    }
                    sequenceManager.init();
                }
            }
        }
        return sequenceManager;
    }

    protected void doInit() {
    }

    protected void doDestroy() {
    }

    public Long nextValue(String sequenceName) {
        return sequenceManager.nextValue(sequenceName);
    }

    public Long nextValue(String sequenceName, int batchSize) {
        return sequenceManager.nextValue(sequenceName, batchSize);
    }

    public Logger log() {
        return log;
    }
}
