package drds.plus.sql_process.sequence;

import drds.plus.common.lifecycle.Lifecycle;

public interface ISequenceManager extends Lifecycle {

    Long nextValue(String seqName);

    Long nextValue(String seqName, int batchSize);

}
