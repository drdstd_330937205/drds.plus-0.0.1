package drds.plus.sql_process.parser.visitor;


import drds.plus.parser.abstract_syntax_tree.expression.comparison.range.In;
import drds.plus.parser.abstract_syntax_tree.expression.primary.literal.LiteralBoolean;
import drds.plus.parser.abstract_syntax_tree.expression.primary.literal.LiteralNull;
import drds.plus.parser.abstract_syntax_tree.expression.primary.literal.LiteralNumber;
import drds.plus.parser.abstract_syntax_tree.expression.primary.literal.LiteralString;
import drds.plus.parser.abstract_syntax_tree.expression.primary.misc.Identifier;
import drds.plus.parser.abstract_syntax_tree.expression.primary.misc.InList;
import drds.plus.parser.abstract_syntax_tree.expression.primary.misc.ParameterMarker;
import drds.plus.parser.abstract_syntax_tree.statement.DeleteStatement;
import drds.plus.parser.abstract_syntax_tree.statement.InsertStatement;
import drds.plus.parser.abstract_syntax_tree.statement.ReplaceStatement;
import drds.plus.parser.abstract_syntax_tree.statement.select.table.Table;
import drds.plus.parser.visitor.VisitorImpl;

import java.util.HashSet;
import java.util.Set;

/**
 * 格式化对应的sql
 */
public class ParameterizedVistor extends VisitorImpl {

    private int replaceCount = 0;


    private Set<String> tableNameSet = new HashSet<String>();

    public ParameterizedVistor(StringBuilder sb) {
        super(sb, null);

    }

    public void visit(In in) {
        InList inList = in.getInExpressionList();
        if (inList != null && !(inList.getList().size() == 1 && inList.getList().get(0) instanceof ParameterMarker)) {
            sb.append('(');
            if (in.isNot()) {
                sb.append(" not in (?)");
            } else {
                sb.append(" in (?)");
            }
            sb.append(')');
            incrementReplaceCunt();
        } else {
            super.visit(in);
        }
    }

    public void visit(LiteralBoolean literalBoolean) {
        {
            sb.append('?');
            incrementReplaceCunt();
        }
    }

    public void visit(LiteralNull literalNull) {
        {
            sb.append('?');
            incrementReplaceCunt();
        }
    }

    public void visit(LiteralNumber literalNumber) {
        {
            sb.append('?');
            incrementReplaceCunt();
        }
    }

    public void visit(LiteralString literalString) {
        {
            sb.append('?');
            incrementReplaceCunt();
        }
    }

    // ======================= where build ===================

    public void visit(Table table) {
        tableNameSet.add(table.getTableName().getText());
        super.visit(table);
    }

    public void visit(DeleteStatement deleteStatement) {
        tableNameSet.add(deleteStatement.getTableName());
        super.visit(deleteStatement);
    }

    public void visit(InsertStatement insertStatement) {
        Identifier tableName = insertStatement.getTableName();
        tableNameSet.add(tableName.getText());
        super.visit(insertStatement);
    }

    public void visit(ReplaceStatement replaceStatement) {
        Identifier tableName = replaceStatement.getTableName();

        tableNameSet.add(tableName.getText());
        super.visit(replaceStatement);
    }


    public int getReplaceCount() {
        return replaceCount;
    }

    public void incrementReplaceCunt() {
        replaceCount++;
    }

    public Set<String> getTableNameSet() {
        return tableNameSet;
    }

}
