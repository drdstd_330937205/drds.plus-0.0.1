package drds.plus.sql_process.parser;

import drds.plus.common.model.SqlType;
import drds.plus.sql_process.abstract_syntax_tree.node.Node;
import drds.plus.sql_process.abstract_syntax_tree.node.dml.Delete;
import drds.plus.sql_process.abstract_syntax_tree.node.dml.Insert;
import drds.plus.sql_process.abstract_syntax_tree.node.dml.Replace;
import drds.plus.sql_process.abstract_syntax_tree.node.dml.Update;
import drds.plus.sql_process.abstract_syntax_tree.node.query.Query;

import java.util.Set;

/**
 * 语法树构建结果
 */
public interface ParseInfo {

    String getSql();

    String getParameterizedSql();


    SqlType getSqlType();

    Set<String> getTableNameToSchemaMap();

    boolean isAbstractSyntaxTreeNode();

    Node getNode();


    Update getUpdateNode();

    Insert getInsertNode();

    Replace getReplaceNode();

    Delete getDeleteNode();

    Query getQueryNode();
}
