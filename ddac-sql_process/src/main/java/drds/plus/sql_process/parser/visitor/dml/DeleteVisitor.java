package drds.plus.sql_process.parser.visitor.dml;

import drds.plus.parser.abstract_syntax_tree.expression.Expression;
import drds.plus.parser.abstract_syntax_tree.statement.DeleteStatement;
import drds.plus.parser.visitor.EmptyVisitor;
import drds.plus.sql_process.abstract_syntax_tree.node.dml.Delete;
import drds.plus.sql_process.abstract_syntax_tree.node.query.TableQuery;
import drds.plus.sql_process.parser.visitor.ExpressionVisitor;

public class DeleteVisitor extends EmptyVisitor {

    private Delete deleteNode;

    public void visit(DeleteStatement deleteStatement) {
        TableQuery tableQueryNode = new TableQuery(deleteStatement.getTableName());
        Expression where = deleteStatement.getWhere();
        if (where != null) {
            ExpressionVisitor expressionVisitor = new ExpressionVisitor();
            where.accept(expressionVisitor);
            tableQueryNode.setWhereAndSetNeedBuild(expressionVisitor.getFilter());
        }
        this.deleteNode = tableQueryNode.delete();
    }

    public Delete getDeleteNode() {
        return deleteNode;
    }
}
