package drds.plus.sql_process.parser;

import drds.plus.common.lifecycle.Lifecycle;

/**
 * 基于sql构建语法树
 */
public interface SqlParseManager extends Lifecycle {

    ParseInfo parse(final String sql, boolean cached);
}
