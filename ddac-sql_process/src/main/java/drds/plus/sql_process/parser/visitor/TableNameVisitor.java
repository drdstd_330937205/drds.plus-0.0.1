package drds.plus.sql_process.parser.visitor;


import drds.plus.parser.abstract_syntax_tree.expression.primary.misc.Identifier;
import drds.plus.parser.abstract_syntax_tree.statement.DeleteStatement;
import drds.plus.parser.abstract_syntax_tree.statement.InsertStatement;
import drds.plus.parser.abstract_syntax_tree.statement.ReplaceStatement;
import drds.plus.parser.abstract_syntax_tree.statement.UpdateStatement;
import drds.plus.parser.abstract_syntax_tree.statement.select.table.Table;
import drds.plus.parser.visitor.EmptyVisitor;

import java.util.HashSet;
import java.util.Set;

/**
 * 提取sql中的表名
 */
public class TableNameVisitor extends EmptyVisitor {

    // schema名对应表名
    private Set<String> tableNameSet = new HashSet<String>();

    public void visit(InsertStatement insertStatement) {
        Identifier tableName = insertStatement.getTableName();

        tableNameSet.add(tableName.getText());
        super.visit(insertStatement);
    }

    public void visit(ReplaceStatement replaceStatement) {
        Identifier tb = replaceStatement.getTableName();

        tableNameSet.add(tb.getText());
        super.visit(replaceStatement);
    }

    public void visit(UpdateStatement updateStatement) {
        String tableName = ((Table) updateStatement.getTable()).getTableName().getText();

        tableNameSet.add(tableName);
        super.visit(updateStatement);
    }

    public void visit(DeleteStatement deleteStatement) {
        String tableName = deleteStatement.getTableName();
        tableNameSet.add(tableName);
        super.visit(deleteStatement);
    }

    public void visit(Table table) {

        tableNameSet.add(table.getTableName().getText());
        super.visit(table);
    }

    public Set<String> getTableNameSet() {
        return tableNameSet;
    }
}
