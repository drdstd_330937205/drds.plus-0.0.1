package drds.plus.sql_process.parser.visitor.dml;

import drds.plus.parser.abstract_syntax_tree.expression.primary.misc.Identifier;
import drds.plus.parser.abstract_syntax_tree.expression.primary.misc.RowValues;
import drds.plus.parser.abstract_syntax_tree.statement.ReplaceStatement;
import drds.plus.parser.visitor.EmptyVisitor;
import drds.plus.sql_process.abstract_syntax_tree.node.dml.Replace;
import drds.plus.sql_process.abstract_syntax_tree.node.query.TableQuery;
import drds.plus.sql_process.parser.visitor.ExpressionVisitor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * replace处理
 */
public class ReplaceVisitor extends EmptyVisitor {

    private Replace replaceNode;

    public void visit(ReplaceStatement replaceStatement) {
        TableQuery tableQueryNode = getTableQueryNode(replaceStatement);
        String columnNamesString = this.getColumnNamesString(replaceStatement);
        List<RowValues> rowValuesList = replaceStatement.getRowValuesList();
        if (rowValuesList != null && rowValuesList.size() == 1) {
            RowValues rowValues = rowValuesList.get(0);
            Object[] $rowValues = getRowValue(rowValues);
            this.replaceNode = tableQueryNode.put(columnNamesString, $rowValues);
        } else if (rowValuesList != null && rowValuesList.size() > 1) {
            List<List<Object>> rowValueList = new ArrayList<List<Object>>();
            for (RowValues rowValues : rowValuesList) {
                Object[] $rowValues = getRowValue(rowValues);
                rowValueList.add(Arrays.asList($rowValues));
            }
            this.replaceNode = tableQueryNode.put(columnNamesString, rowValueList);
        } else {
            throw new IllegalStateException("replace语句必须包含values值部分");
        }

    }

    private TableQuery getTableQueryNode(ReplaceStatement replaceStatement) {
        return new TableQuery(replaceStatement.getTableName().getText());
    }

    private String getColumnNamesString(ReplaceStatement replaceStatement) {
        List<Identifier> columnNameList = replaceStatement.getColumnNameList();
        StringBuilder sb = new StringBuilder();
        if (columnNameList != null && columnNameList.size() != 0) {
            for (int i = 0; i < columnNameList.size(); i++) {
                if (i > 0) {
                    sb.append(" ");
                }
                sb.append(columnNameList.get(i).getText());
            }
        }

        return sb.toString();
    }

    private Object[] getRowValue(RowValues rowValues) {
        Object[] $rowValues = new Object[rowValues.getRowValueList().size()];
        for (int i = 0; i < rowValues.getRowValueList().size(); i++) {
            ExpressionVisitor expressionVisitor = new ExpressionVisitor();
            rowValues.getRowValueList().get(i).accept(expressionVisitor);
            $rowValues[i] = expressionVisitor.getObject();
        }
        return $rowValues;
    }

    public Replace getReplaceNode() {
        return replaceNode;
    }
}
