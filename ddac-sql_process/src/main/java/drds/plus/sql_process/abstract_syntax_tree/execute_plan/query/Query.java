package drds.plus.sql_process.abstract_syntax_tree.execute_plan.query;

import drds.plus.sql_process.abstract_syntax_tree.execute_plan.ExecutePlan;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.Item;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.Filter;
import drds.plus.sql_process.abstract_syntax_tree.expression.order_by.OrderBy;

import java.util.List;

public interface Query<T extends Query> extends ExecutePlan<T> {


    Filter getValueFilter();

    /**
     * valueFilter 简单来说就是对所有数据，一行一行进行检索的filter. 可以保证所有bool条件都能够查询，但性能较慢，一般来说尽可能先使用key filter之后再使用value filter
     */
    void setValueFilter(Filter valueFilter);


    Filter getSubQueryFilter();

    /**
     * subQueryFunctionFilter 简单来说就是对所有数据执行类似exists subquery语法
     */
    void setSubQueryFilter(Filter subQueryFilter);


    List<Item> getItemList();

    /**
     * 设置当前查询节点的column filters ,只有出现在columnFilters里面的列，才可以出现在这个结果集的展现列中。
     */
    void setSelectItemList(List<Item> itemList);


    List<OrderBy> getOrderByList();

    /**
     * 设置order by条件 如果有order by ，那么数据必须按照order by 条件进行数据重排，除非数据本身key就是按照该数据结构进行排列的。
     */
    void setOrderByList(List<OrderBy> orderByList);


    Comparable getLimitFrom();

    /**
     * 设置这次数据从第几个开始取
     */
    void setLimitFrom(Comparable limitFrom);


    Comparable getLimitTo();

    /**
     * 设置这次数据取到第几个。
     */
    void setLimitTo(Comparable limitTo);


    List<OrderBy> getGroupByList();

    /**
     * 结果集按照什么进行排序。 如果无序，那么order by里面的asc或desc是空
     */
    void setGroupByList(List<OrderBy> groupBys);


    String getAlias();

    /**
     * 设置当前query的别名
     */
    void setAlias(String alias);


    void setCanMerge(Boolean canMerge);

    /**
     * <pre>
     * 在处理join的时候，会出现未决节点 比如 数据IDX_PRI: pk - > col1,col2,col3.
     * 数据按照pk进行切分索引IDX_COL1: col1->pk 数据按照col1进行切分
     * 1. 那么索引查找的时候，会生成一个join 先查询IDX_COL1.
     * 2. 然后根据IDX_COL1的"结果"，来决定应该去哪些pk索引的数据节点上进行查询（因为IDX_PRI也是分了多个机器的）。
     * 所以这时候IDX_PRI是不能预先知道自己在这次查询中应该去查哪些节点的。 这时候。 针对IDX_PRI的查询节点，就是未决节点,这时候canMerge为true.
     * </pre>
     */
    Boolean canMerge();

    /**
     * 是否显式指定使用临时表
     */
    void setExplicitUseTemporaryTable(Boolean isUseTempTable);

    Boolean isExplicitUseTemporaryTable();


    Boolean isSubQuery();

    /**
     * 是否是个子查询
     */
    void setIsSubQuery(Boolean isSubQuery);


    Filter getHaving();

    /**
     * having 子句支持。用在groupby 后面
     */
    void having(Filter having);

    /**
     * 是否是最外层的查询
     */
    boolean isTopQuery();

    void setTopQuery(boolean topQuery);

    QueryConcurrencyWay getQueryConcurrencyWay();

    void setQueryConcurrency(QueryConcurrencyWay queryConcurrencyWay);


    boolean isExistAggregate();

    /**
     * 是否为存在聚合信息，比如出现limit/group by/count/max等，此节点就会被标记为true，不允许进行join sort
     * join的展开优化
     */
    void setExistAggregate(boolean isExistAggregate);


    Filter getOtherJoinOnFilter();

    /**
     * 非column=column的join on中的条件
     */
    void setOtherJoinOnFilter(Filter otherJoinOnFilter);

    Long getSubQueryFilterId();

    void setSubQueryFilterId(Long subQueryOnFilterId);

    boolean isCorrelatedSubQuery();

    void setCorrelatedSubQuery(boolean isCorrelatedSubquery);

    /**
     * 锁模式。
     */
    LockMode getLockMode();

    void setLockMode(LockMode lockModel);

}
