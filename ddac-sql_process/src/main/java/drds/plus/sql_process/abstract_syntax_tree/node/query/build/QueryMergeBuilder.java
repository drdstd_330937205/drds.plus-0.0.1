package drds.plus.sql_process.abstract_syntax_tree.node.query.build;

import drds.plus.sql_process.abstract_syntax_tree.ObjectCreateFactory;
import drds.plus.sql_process.abstract_syntax_tree.expression.bind_value.BindValue;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.Item;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.column.Column;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.Filter;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.Function;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.FunctionType;
import drds.plus.sql_process.abstract_syntax_tree.expression.order_by.OrderBy;
import drds.plus.sql_process.abstract_syntax_tree.node.Node;
import drds.plus.sql_process.abstract_syntax_tree.node.query.MergeQuery;
import drds.plus.sql_process.abstract_syntax_tree.node.query.Query;
import drds.plus.sql_process.utils.OptimizerUtils;
import drds.tools.$;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class QueryMergeBuilder extends QueryBuilder {

    public QueryMergeBuilder(MergeQuery mergeQueryNode) {
        this.setQuery(mergeQueryNode);
    }

    public MergeQuery getQuery() {
        return (MergeQuery) super.getQuery();
    }

    public void build() {
        for (Node node : this.getQuery().getNodeList()) {
            node.build();
        }

        this.buildExistSequenceValue();
        if (!(this.getQuery().getFirstSubNodeQueryNode() instanceof Query)) {
            return;
        }
        this.buildAlias();
        this.buildSelectItemList();
        this.buildWhere();
        this.buildGroupBy();
        this.buildOrderBy();
        this.buildHaving();
        this.buildFunction();
        this.buildExistAggregate();
        // 最后一个处理
        this.buildLimit();
    }

    /**
     * <pre>
     * 1. max(id)+min(id)，要把聚合函数单独推到子节点去(比如max(id),min(id))，然后在父节点留一个scalar函数进行计算
     * 2. substring(),to_date()等简单scalar函数(不包含条件1的函数)，推到子节点去，然后在父节点留一个字段，而不是函数
     * </pre>
     */

    public void buildFunction() {
        super.buildFunction();
        List<Item> simpleScalarFunctionItemList = new ArrayList<Item>();
        List<Function> aggregateFunctionListInScalarFunction = new ArrayList<Function>();
        for (Item item : this.getQuery().getSelectItemList()) {
            if (item instanceof Function) {
                if (FunctionType.aggregate_function.equals(((Function) item).getFunctionType())) {
                    continue;
                } else {
                    List<Function> $aggregateFunctionListInScalarFunction = this.findAggregateFunctionListInScalarFunction((Function) item);
                    if (!$aggregateFunctionListInScalarFunction.isEmpty()) {
                        aggregateFunctionListInScalarFunction.addAll($aggregateFunctionListInScalarFunction);
                    } else {
                        simpleScalarFunctionItemList.add(item);
                    }
                }
            }
        }
        List<Item> toBeRemovedItemList = new ArrayList<Item>();
        for (Item item : ((Query) this.getQuery().getFirstSubNodeQueryNode()).getSelectItemList()) {
            if (item instanceof Function && FunctionType.scalar.equals(((Function) item).getFunctionType())) {
                if ($.isNotNullAndHasElement(this.findAggregateFunctionListInScalarFunction((Function) item))) { // scalar里存在聚合函数
                    toBeRemovedItemList.add(item);
                }
            }
        }

        for (Node node : this.getQuery().getNodeList()) {
            ((Query) node).getSelectItemList().removeAll(toBeRemovedItemList);// 干掉查询的min(id)+max(id)函数
            for (Item item : aggregateFunctionListInScalarFunction) {
                // 只添加min(id) ,max(id)的独立函数
                ((Query) node).addSelectItem(item.copy());
            }
            node.build();
        }
    }

    public void buildFunction(Function function, boolean findInSelectItemList) {
        if (FunctionType.aggregate_function == function.getFunctionType()) {
            setExistAggregateExpression();
        }

        for (Object arg : function.getArgList()) {
            if (arg instanceof Function) {
                this.buildSelectItem((Item) arg, findInSelectItemList);
            } else if (!this.getQuery().isDistinctShardColumns() && arg instanceof Item) {
                // 针对非distinct的函数，没必要下推字段
                if (((Item) arg).isDistinct()) {
                    this.buildSelectItem((Item) arg, findInSelectItemList);
                }
            }
        }
    }

    private List<Function> findAggregateFunctionListInScalarFunction(Function function) {
        List<Function> functionList = new ArrayList<Function>();
        this.findAggregateFunctionListInScalarFunction(function, functionList);
        return functionList;
    }

    private void findAggregateFunctionListInScalarFunction(Function function, List<Function> functionList) {
        if (FunctionType.aggregate_function.equals(function.getFunctionType())) {
            functionList.add(function);
        }
        for (Object arg : function.getArgList()) {
            if (arg instanceof Function) {
                this.findAggregateFunctionListInScalarFunction((Function) arg, functionList);
            }
        }
    }

    public void buildHaving() {
        if (this.getQuery().isGroupByShardColumns()) {
            // 如果是groupBy分库键，having条件直接底下算
            this.getQuery().having("");
        } else if (this.getQuery().getHaving() == null) {
            if (this.getQuery().getFirstSubNodeQueryNode() instanceof Query) {
                Filter havingFilter = ((Query) this.getQuery().getFirstSubNodeQueryNode()).getHaving();
                if (havingFilter != null) {
                    this.getQuery().having(OptimizerUtils.copyFilter(havingFilter));
                    replaceAliasInFilter(this.getQuery().getHaving(), ((Query) this.getQuery().getFirstSubNodeQueryNode()).getAlias());
                    // 干掉子节点查询的having条件，转移到父节点中 【暂时不清理，交由OrderByPusher来判断】
                    // for (DataNodeDataScatterInfo child : this.getQuery().getDataNodeDataScatterInfoList()) {
                    // if (child instanceof Query) {
                    // ((Query) child).having("");
                    // }
                    // }
                }
            }
        }
    }

    private void replaceAliasInFilter(Object filter, String alias) {
        if (filter instanceof Function) {
            for (Object sub : ((Function) filter).getArgList()) {
                this.replaceAliasInFilter(sub, alias);
            }
        }

        if (filter instanceof Item) {
            if (alias != null) {
                ((Item) filter).setTableName(alias);
            }

            if (((Item) filter).getAlias() != null) {
                ((Item) filter).setColumnName(((Item) filter).getAlias());
                ((Item) filter).setAlias(null);
            }
        }

    }

    public void buildLimit() {
        if (this.getQuery().getLimitFrom() == null && this.getQuery().getLimitTo() == null) {

            // 将子节点的limit条件转移到父节点
            // 不能出现多级的merge节点都带着limit
            Comparable from = ((Query) this.getQuery().getFirstSubNodeQueryNode()).getLimitFrom();
            Comparable to = ((Query) this.getQuery().getFirstSubNodeQueryNode()).getLimitTo();
            if ((from instanceof BindValue && ((BindValue) from).getValue() != null) || (to instanceof BindValue && ((BindValue) to).getValue() != null)) {
                // 说明是batch中针对limit使用绑定变量，暂时不支持
                throw new UnsupportedOperationException("batch中不支持对分库分表limit的绑定变量");
            }
            this.getQuery().setLimitFrom(from);
            this.getQuery().setLimitTo(to);
            if (from instanceof Long && to instanceof Long) {
                if ((from != null && (Long) from != -1) || (to != null && (Long) to != -1)) {
                    for (Node node : this.getQuery().getNodeList()) {
                        if (query.isExistAggregateExpression()) {
                            // 如果节点存在聚合函数，limit不能下推，干掉子节点的limit，由父节点进行计算
                            ((Query) node).setLimitFrom(null);
                            ((Query) node).setLimitTo(null);
                        } else {
                            // 底下采取limit 0,from+to逻辑，上层来过滤
                            ((Query) node).setLimitFrom(0L);
                            ((Query) node).setLimitTo((Long) from + (Long) to);
                        }
                    }
                }
            }
        }

    }


    public void buildOrderBy() {
        // 如果merge本身没指定order by，则继承子节点的order by
        if (!$.isNotNullAndHasElement(this.getQuery().getOrderByList())) {
            Query query = (Query) this.getQuery().getFirstSubNodeQueryNode();
            if (query.getOrderByList() != null) {
                for (OrderBy orderBy : query.getOrderByList()) {
                    Item copy = orderBy.getItem().copy();
                    if (query.getAlias() != null) {
                        copy.setTableName(query.getAlias());
                    }
                    if (orderBy.getItem().getAlias() != null) {
                        copy.setColumnName(orderBy.getItem().getAlias());
                    }
                    this.getQuery().addOrderByItemAndSetNeedBuild(copy, orderBy.getAsc());
                }
            }
        }

        super.buildOrderBy();
        // 检查group列是否在select列中
        for (OrderBy orderBy : query.getOrderByList()) {
            checkOrderByColumnExistInSelectList((Query) query.getFirstSubNodeQueryNode(), orderBy);
        }
    }

    public void buildGroupBy() {
        // 如果merge本身没指定group by，则继承子节点的group by
        if (!$.isNotNullAndHasElement(this.getQuery().getGroupByList())) {
            Query query = (Query) this.getQuery().getFirstSubNodeQueryNode();
            if (query.getGroupByList() != null) {
                for (OrderBy orderBy : query.getGroupByList()) {
                    OrderBy copy = orderBy.copy();
                    if (query.getAlias() != null) {
                        copy.setTableName(query.getAlias());
                    }
                    if (orderBy.getAlias() != null) {
                        copy.setColumnName(orderBy.getAlias());
                    }
                    this.getQuery().addGroupByItemAndSetNeedBuild(copy);
                }
            }
        }

        super.buildGroupBy();

        // 检查group列是否在select列中
        for (OrderBy orderBy : query.getGroupByList()) {
            checkOrderByColumnExistInSelectList((Query) query.getFirstSubNodeQueryNode(), orderBy);
        }
    }

    private void buildAlias() {
        if (this.getQuery().getAlias() == null) {
            this.getQuery().setAliasAndSetNeedBuild(((Query) this.getQuery().getFirstSubNodeQueryNode()).getAlias());
        }
    }

    /**
     * 构建列信息
     */
    public void buildSelectItemList() {
        if (!$.isNotNullAndHasElement(this.getQuery().getSelectItemList())) {
            Column column = ObjectCreateFactory.createColumn();
            column.setColumnName(Column.star);
            this.getQuery().getSelectItemList().add(column);
        }
        // 如果有 * ，最后需要把*删掉
        List<Item> deleteItemList = new LinkedList();
        for (Item selectItem : getQuery().getSelectItemList()) {
            if (selectItem.getColumnName().equals(Column.star)) {
                deleteItemList.add(selectItem);
            }
        }
        if (!deleteItemList.isEmpty()) {
            this.getQuery().getSelectItemList().removeAll(deleteItemList);
        }
        for (Item deleteItem : deleteItemList) {
            // 遇到*就把所有列再添加一遍
            // selectStatement *,id这样的语法最后会有两个id列，mysql是这样的
            Query query = (Query) this.getQuery().getFirstSubNodeQueryNode();
            for (Item selectItem : query.getSelectItemList()) {
                if (deleteItem.getTableName() != null) {
                    if (!deleteItem.getTableName().equals(selectItem.getTableName())) {
                        break;
                    }
                }
                Item copy = selectItem.copy();
                if (query.getAlias() != null) {
                    copy.setTableName(query.getAlias());
                } else {
                    copy.setTableName(selectItem.getTableName());
                }
                if (selectItem.getAlias() != null) {
                    copy.setColumnName(selectItem.getAlias());
                } else {
                    copy.setColumnName(selectItem.getColumnName());
                }
                getQuery().getSelectItemList().add(copy);
            }
        }

        for (int i = 0; i < getQuery().getSelectItemList().size(); i++) {
            getQuery().getSelectItemList().set(i, this.buildSelectItem(getQuery().getSelectItemList().get(i)));
        }
    }

    /**
     * 从第一个子QueryNode中获取
     */
    public Item getItemFromTableMetaDataOrQueryNodeToBeDependentOn(Item item) {
        Query query1 = (Query) this.getQuery().getFirstSubNodeQueryNode();
        if (Column.star.equals(item.getColumnName())) {
            return item;
        }

        if (item instanceof Function) {
            return item;
        }

        if (query1.hasItem(item)) {
            Item itemFromQueryNode = this.getItemFromSelectItemListOfOtherQueryNodeAndUpdateTableName(query1, item);
            if (itemFromQueryNode == null) {
                // 下推select
                for (int i = 0; i < this.getQuery().getNodeList().size(); i++) {
                    Query query = (Query) this.getQuery().getNodeList().get(i);
                    query.addSelectItem(item.copy());
                }
                itemFromQueryNode = this.getItemFromSelectItemListOfOtherQueryNodeAndUpdateTableName(query1, item);
            }
            return itemFromQueryNode;
        } else {
            return null;
        }
    }

}
