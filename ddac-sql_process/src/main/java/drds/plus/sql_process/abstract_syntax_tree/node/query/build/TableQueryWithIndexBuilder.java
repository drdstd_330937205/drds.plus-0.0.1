package drds.plus.sql_process.abstract_syntax_tree.node.query.build;

import drds.plus.sql_process.abstract_syntax_tree.ObjectCreateFactory;
import drds.plus.sql_process.abstract_syntax_tree.configuration.ColumnMetaData;
import drds.plus.sql_process.abstract_syntax_tree.configuration.IndexMapping;
import drds.plus.sql_process.abstract_syntax_tree.configuration.TableMetaData;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.Item;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.column.Column;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.Function;
import drds.plus.sql_process.abstract_syntax_tree.node.query.TableQueryWithIndex;
import drds.plus.sql_process.optimizer.OptimizerContext;
import drds.plus.sql_process.optimizer.OptimizerException;
import drds.plus.sql_process.utils.OptimizerUtils;
import drds.tools.$;

import java.util.LinkedList;
import java.util.List;

public class TableQueryWithIndexBuilder extends QueryBuilder {

    public TableQueryWithIndexBuilder(TableQueryWithIndex tableQueryWithIndex) {
        this.setQuery(tableQueryWithIndex);
    }

    public TableQueryWithIndex getQuery() {
        return (TableQueryWithIndex) super.getQuery();
    }

    public void build() {
        this.buildIndexMetaData();
        this.buildSelectItemList();
        this.buildGroupBy();
        this.buildOrderBy();
        this.buildWhere();
        this.buildExistAggregate();
        this.buildExistSequenceValue();
    }


    /**
     * 构建索引信息
     */
    public void buildIndexMetaData() {
        String indexName = getQuery().getIndexName();
        if (indexName != null) {
            IndexMapping indexMapping = OptimizerContext.getOptimizerContext().getIndexManager().getIndexMetaData(indexName);
            if (indexMapping == null) {
                throw new OptimizerException("index :" + indexName + " is not found");
            }
            TableMetaData tableMetaData = OptimizerContext.getOptimizerContext().getSchemaManager().getTableMetaData(indexMapping.getTableName());
            if (indexMapping == null) {
                throw new OptimizerException("tableMetaData is null when index :" + indexName);
            }
            getQuery().setTableMetaData(tableMetaData);
            getQuery().setIndexMapping(indexMapping);
        } else if (getQuery().getIndexMapping() == null) {
            throw new OptimizerException("index is null");
        }

    }

    /**
     * 構建列信息
     */
    public void buildSelectItemList() {
        if (this.getQuery().getSelectItemList().isEmpty()) {
            Column column = ObjectCreateFactory.createColumn();
            column.setColumnName(Column.star);
            this.getQuery().getSelectItemList().add(column);
        }
        // 如果有 * ，最后需要把*删掉
        List<Item> deleteItemList = new LinkedList<Item>();
        for (Item item : getQuery().getSelectItemList()) {
            if (Column.star.equals(item.getColumnName())) {
                deleteItemList.add(item);//all * need to delete
            }
        }
        if ($.isNotNullAndHasElement(deleteItemList)) {
            this.getQuery().getSelectItemList().removeAll(deleteItemList);
            // 遇到*就把所有列再添加一遍,query *,id这样的语法最后会有两个id列
            for (ColumnMetaData columnMetaData : this.getQuery().getIndexMapping().getKeyColumnMetaDataList()) {
                this.getQuery().getSelectItemList().add(OptimizerUtils.columnMetaDataToColumn(columnMetaData, this.getQuery().getIndexName()));
            }
            for (ColumnMetaData columnMetaData : this.getQuery().getIndexMapping().getValueColumnMetaDataList()) {
                this.getQuery().getSelectItemList().add(OptimizerUtils.columnMetaDataToColumn(columnMetaData, this.getQuery().getIndexName()));
            }
        }
        for (int i = 0; i < getQuery().getSelectItemList().size(); i++) {
            this.getQuery().getSelectItemList().set(i, this.buildSelectItem(getQuery().getSelectItemList().get(i)));
        }
    }

    /**
     * TableMetaData中获取item
     */
    public Item getItemFromTableMetaDataOrQueryNodeToBeDependentOn(Item item) {
        if (item.getTableName() != null) {
            if ((!item.getTableName().equals(this.getQuery().getIndexName())) //
                    && //
                    (!item.getTableName().equals(this.getQuery().getAlias()))) {//
                return null;
            }
        }
        if (Column.star.equals(item.getColumnName())) {
            return item;
        }

        if (item instanceof Function) {
            item.setTableName(this.getQuery().getIndexName());
            return item;
        }
        ColumnMetaData columnMetaData = this.getQuery().getIndexMapping().getColumnMetaData(item.getColumnName());
        if (columnMetaData == null) {
            return null;
        }
        return OptimizerUtils.columnMetaDataToColumn(columnMetaData, getQuery().getIndexName());
    }
}
