package drds.plus.sql_process.abstract_syntax_tree.execute_plan.dml;

public enum PutType {
    replace, insert, update, delete
}
