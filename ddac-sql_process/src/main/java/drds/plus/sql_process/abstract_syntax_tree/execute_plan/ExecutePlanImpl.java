package drds.plus.sql_process.abstract_syntax_tree.execute_plan;

public abstract class ExecutePlanImpl<T extends ExecutePlan> implements ExecutePlan<T> {

    protected String requestHostName;
    protected Long requestId;
    protected Long subRequestId;
    protected String dataNodeId;
    protected boolean consistentRead = true;
    protected Integer thread;


    protected String sql;
    protected boolean streaming = false;
    protected boolean lazyLoad = false;
    protected boolean existSequenceVal = false; // 是否存在sequence
    protected Long lastSequenceVal = 0L; // 上一次生成的sequenceVal
    protected ExplainMode explainMode = null;

    public void setDataNodeId(String dataNodeId) {
        this.dataNodeId = dataNodeId;

    }

    public boolean isConsistent() {
        return consistentRead;
    }


    public String getDataNodeId() {
        return dataNodeId;
    }

    public String getRequestHostName() {
        return requestHostName;
    }

    public Long getRequestId() {
        return requestId;
    }

    public String getSql() {
        return this.sql;
    }

    public Long getSubRequestId() {
        return requestId;
    }

    public Integer getThreadNo() {
        return this.thread;
    }

    public boolean isStreaming() {
        return this.streaming;
    }


    public void setConsistent(boolean consistent) {
        this.consistentRead = consistent;

    }


    public void setRequestHostName(String requestHostName) {
        this.requestHostName = requestHostName;

    }

    public void setRequestId(Long requestId) {
        this.requestId = requestId;

    }

    public void setSql(String sql) {
        this.sql = sql;

    }

    public void setStreaming(boolean streaming) {
        this.streaming = streaming;

    }

    public void setSubRequestId(Long subRequestId) {
        this.subRequestId = subRequestId;

    }

    /**
     * 表明一个建议的用于执行该节点的线程id
     */

    public void setThreadNo(Integer threadNo) {
        this.thread = threadNo;

    }


    public boolean isLazyLoad() {
        return this.lazyLoad;
    }

    public void setLazyLoad(boolean lazyLoad) {
        this.lazyLoad = lazyLoad;
    }

    public boolean isExistSequenceValue() {
        return this.existSequenceVal;
    }

    public void setExistSequenceValue(boolean existSequenceValue) {
        this.existSequenceVal = existSequenceValue;
    }

    public Long getLastSequenceValue() {
        return lastSequenceVal;
    }

    public void setLastSequenceValue(Long sequenceValue) {
        this.lastSequenceVal = sequenceValue;
    }

    public ExplainMode getExplainMode() {
        return explainMode;
    }

    public void setExplainMode(ExplainMode explainMode) {
        this.explainMode = explainMode;
    }

    public boolean isExplain() {
        return explainMode != null;
    }


}
