package drds.plus.sql_process.abstract_syntax_tree.execute_plan.query;

/**
 * 不支持hash_join
 */
public enum JoinStrategy {
    nest_loop_join, index_nest_loop_join, sort_merge_join
}
