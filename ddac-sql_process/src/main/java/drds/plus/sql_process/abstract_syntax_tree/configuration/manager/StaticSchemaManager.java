package drds.plus.sql_process.abstract_syntax_tree.configuration.manager;


import drds.plus.common.lifecycle.AbstractLifecycle;
import drds.plus.sql_process.abstract_syntax_tree.configuration.TableMetaData;
import drds.plus.sql_process.abstract_syntax_tree.configuration.parse.TableMetaDataParser;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


@Slf4j
public class StaticSchemaManager extends AbstractLifecycle implements SchemaManager {

    @Override
    public Logger log() {
        return log;
    }

    protected Map<String, TableMetaData> tableNameToTableMetaDataMap;

    private String applicationId = null;


    public StaticSchemaManager(String applicationId) {
        super();

        this.applicationId = applicationId;

    }

    public StaticSchemaManager() {
    }


    public static StaticSchemaManager parseSchema(InputStream inputStream) {
        if (inputStream == null) {
            throw new IllegalArgumentException("in is null");
        }

        try {
            StaticSchemaManager staticSchemaManager = new StaticSchemaManager();
            staticSchemaManager.init();
            TableMetaDataParser tableMetaDataParser = new TableMetaDataParser();
            List<TableMetaData> tableMetaDataList = tableMetaDataParser.parse(inputStream);
            for (TableMetaData tableMetaData : tableMetaDataList) {
                staticSchemaManager.putTable(tableMetaData.getTableName(), tableMetaData);
            }
            return staticSchemaManager;
        } finally {
            //IOUtils.closeQuietly(in);
        }

    }


    void loadDualTable() {

        String data = null;// 获取数据
        TableMetaDataParser tableMetaDataParser = new TableMetaDataParser();
        TableMetaData tableMetaData = tableMetaDataParser.parse(data).get(0);
        this.putTable(tableMetaData.getTableName(), tableMetaData);
    }

    protected void doInit() {

        super.doInit();
        tableNameToTableMetaDataMap = new ConcurrentHashMap<String, TableMetaData>();
        loadDualTable();
        String data = null;
        InputStream inputStream = null;
        try {
            inputStream = new ByteArrayInputStream(data.getBytes());
            TableMetaDataParser tableMetaDataParser = new TableMetaDataParser();
            List<TableMetaData> tableMetaDataList = tableMetaDataParser.parse(inputStream);
            this.tableNameToTableMetaDataMap.clear();
            for (TableMetaData tableMetaData : tableMetaDataList) {
                this.putTable(tableMetaData.getTableName(), tableMetaData);
            }
            log.info("where fetched:");
            log.info(this.tableNameToTableMetaDataMap.keySet().toString());
            loadDualTable();
        } catch (Exception e) {
            log.error("where tableMetaDataParser error, schema file is:\n" + data, e);
            throw new RuntimeException(e);
        } finally {

        }

    }

    protected void doDestroy() {
        super.doDestroy();
        tableNameToTableMetaDataMap.clear();
    }

    public TableMetaData getTableMetaData(String tableName) {
        TableMetaData tableMetaData = tableNameToTableMetaDataMap.get((tableName));

        if (tableMetaData != null) {
            return tableMetaData;
        }
        return null;
    }

    public void putTable(String tableName, TableMetaData tableMetaData) {
        tableNameToTableMetaDataMap.put(tableName.toUpperCase(), tableMetaData);
    }

    public Collection<TableMetaData> getAllTables() {
        return tableNameToTableMetaDataMap.values();
    }

    public void reload() {
        this.doDestroy();
        this.isInited = false;
        this.init();
    }


}
