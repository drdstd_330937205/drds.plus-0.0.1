package drds.plus.sql_process.abstract_syntax_tree.expression.item.function;

import java.util.List;

/**
 * group filter, ie: (id = 1 or id = 3 or id is null)
 * <br/>
 * 将多个相同列的or filter条件做为一个整体，进行下推处理
 */
public interface OrsFilter extends Filter<OrsFilter> {

    Object getColumn();

    void setColumn(Object column);

    List<Filter> getFilterList();

    void setFilterList(List<Filter> filterList);

    void addFilter(Filter filter);
}
