package drds.plus.sql_process.abstract_syntax_tree.expression.item;

import drds.plus.common.jdbc.Parameters;
import drds.plus.sql_process.abstract_syntax_tree.Visitable;
import drds.plus.sql_process.type.Type;

/**
 * 描述一个列信息，可能会是字段列，函数列，常量列
 */
public interface Item<GenericParadigm extends Item> extends Visitable, Comparable {

    /**
     * 参数赋值，计算过程获取parameter参数获取?对应的数据
     *
     * <pre>
     * a. 比如原始sql中使用了?占位符，通过PrepareStatement.setXXX设置参数 b. sql解析后构造了，将?解析为IBindVal对象
     */
    GenericParadigm assignment(Parameters parameters);

    Type getType();

    void setType(Type type);

    // --------------- name相关信息 ----------------------

    /**
     * setAliasAndSetNeedBuild ，只在一个表（索引）对象结束的时候去处理 一般情况下，取值不需要使用这个东西。 直接使用columnName即可。
     */
    String getAlias();

    void setAlias(String alias);

    String getTableName();

    void setTableName(String tableName);

    String getColumnName();

    void setColumnName(String columnName);

    /**
     * 是否为相同的名字，如果目标的alias name存在则对比alias columnName，否则对比column columnName
     */
    boolean isSameColumnName(Item select);

    boolean isAutoIncrement();

    void setAutoIncrement(boolean autoIncrement);

    /**
     * @return tableName + columnName
     */
    String getFullName();

    // -----------------特殊标记------------------

    boolean isDistinct();

    void setDistinct(boolean distinct);

    boolean isNot();

    void setIsNot(boolean isNot);

    Long getCorrelateSubQueryItemId();

    void setCorrelateSubQueryItemId(Long correlateSubQueryItemId);

    // -----------------对象复制 -----------------

    GenericParadigm copy();

}
