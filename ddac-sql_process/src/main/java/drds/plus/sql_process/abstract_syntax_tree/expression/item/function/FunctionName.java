package drds.plus.sql_process.abstract_syntax_tree.expression.item.function;

public interface FunctionName {
    String max = "MAX";
    String min = "min";
    String avg = "avg";
    String sum = "sum";
    String count = "count";
    //
    String add = "+";
    String sub = "-";
    String multiply = "*";
    String division = "/";//
    String mod = "%";


    String minus = "minus";
    String row = "row";


    String interval_primary = "interval_primary";
    String get_format = "get_format";
    String extract = "extract";
    String timestampadd = "timestampadd";
    String timestampdiff = "timestampdiff";
    String convert = "convertByValueType";
    String case_when = "case_when";
    String cast = "cast";
    String $char = "char";
    // subquery
    String subquery_scalar = "subquery_scalar";
    // 特殊处理in比较，subquery可返回多条记录，比如id in (subquery)
    String subquery_list = "subquery_list";
}
