package drds.plus.sql_process.abstract_syntax_tree.expression.item.function;

import java.util.List;

/**
 * boolean filter.
 *
 * <pre>
 * 例子：
 * a. column > 100
 * b. count(id) > 100
 * </pre>
 */
public interface BooleanFilter extends Filter<BooleanFilter> {

    void setOperation(Operation operation);

    Object getColumn();

    void setColumn(Object column);

    Object getValue();

    void setValue(Object value);

    /**
     * 多个value，出现id in ()
     */
    List<Object> getValueList();

    void setValueList(List<Object> valueList);

}
