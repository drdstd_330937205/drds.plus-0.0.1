package drds.plus.sql_process.abstract_syntax_tree.expression.item.function;

/**
 * 条件表达式
 */
public interface Filter<T extends Filter> extends Function<T> {

    Operation getOperation();

    void setOperation(Operation operation);

}
