package drds.plus.sql_process.abstract_syntax_tree.expression.order_by;

import drds.plus.common.jdbc.Parameters;
import drds.plus.sql_process.abstract_syntax_tree.Visitable;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.Item;
import drds.plus.sql_process.type.Type;

public interface OrderBy extends Visitable {

    Item getItem();

    void setColumn(Item columnName);

    Boolean getAsc();

    void setAsc(Boolean direction);

    OrderBy assignment(Parameters parameters);

    String getAlias();

    String getTableName();

    void setTableName(String alias);

    String getColumnName();

    void setColumnName(String alias);

    Type getDataType();

    String toStringWithInden(int inden);

    OrderBy copy();

}
