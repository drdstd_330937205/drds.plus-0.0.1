package drds.plus.sql_process.abstract_syntax_tree.execute_plan.dml;

import drds.plus.sql_process.abstract_syntax_tree.ObjectCreateFactory;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.Item;
import drds.plus.sql_process.utils.OptimizerUtils;

import java.util.List;

public class InsertImpl extends Put<Insert> implements Insert {

    private List<Item> duplicateUpdateColumns;
    private List<Object> duplicateUpdateValues;

    public InsertImpl() {
        putType = PutType.insert;
    }

    public List<Object> getDuplicateUpdateValues() {
        return duplicateUpdateValues;
    }

    public void setDuplicateUpdateValues(List<Object> valueList) {
        this.duplicateUpdateValues = valueList;
    }

    public List<Item> getDuplicateUpdateColumns() {
        return duplicateUpdateColumns;
    }

    public void setDuplicateUpdateColumns(List<Item> cs) {
        this.duplicateUpdateColumns = cs;
    }

    public Insert copy() {
        Insert insert = ObjectCreateFactory.createInsert();
        copySelfTo(insert);
        insert.setDuplicateUpdateColumns(OptimizerUtils.copySelectItemList(duplicateUpdateColumns));
        insert.setDuplicateUpdateValues(OptimizerUtils.copyValues(duplicateUpdateValues));
        return insert;
    }


}
