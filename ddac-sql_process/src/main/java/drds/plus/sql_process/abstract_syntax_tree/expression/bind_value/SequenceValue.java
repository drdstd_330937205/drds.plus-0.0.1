package drds.plus.sql_process.abstract_syntax_tree.expression.bind_value;

/**
 * sequence.nextval
 */
public interface SequenceValue extends BindValue, Comparable {

    String SEQ_NEXTVAL = "NEXTVAL";

    void setOriginIndex(int index);

}
