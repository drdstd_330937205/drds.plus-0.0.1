package drds.plus.sql_process.abstract_syntax_tree.node.query.build;


import drds.plus.sql_process.abstract_syntax_tree.ObjectCreateFactory;
import drds.plus.sql_process.abstract_syntax_tree.configuration.ColumnMetaData;
import drds.plus.sql_process.abstract_syntax_tree.configuration.TableMetaData;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.Item;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.column.Column;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.Function;
import drds.plus.sql_process.abstract_syntax_tree.node.query.TableQuery;
import drds.plus.sql_process.optimizer.OptimizerContext;
import drds.plus.sql_process.utils.OptimizerUtils;
import drds.tools.$;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class TableQueryBuilder extends QueryBuilder {

    public TableQueryBuilder(TableQuery tableQueryNode) {
        this.setQuery(tableQueryNode);
    }

    public void build() {
        this.buildTableMetaData();
        this.buildSelectItemList();
        this.buildWhere();
        this.buildGroupBy();
        this.buildOrderBy();
        this.buildHaving();
        this.buildExistAggregate();
        this.buildExistSequenceValue();
    }

    public TableQuery getQuery() {
        return (TableQuery) super.getQuery();
    }

    /**
     * 构建TableMeta
     */
    public void buildTableMetaData() {
        String tableName = getQuery().getTableName();
        if (tableName == null) {
            throw new IllegalArgumentException("tableName is null");
        }
        TableMetaData tableMetaData = OptimizerContext.getOptimizerContext().getSchemaManager().getTableMetaData(tableName);
        if (tableMetaData == null) {
            throw new IllegalArgumentException("tableMetaData is null");
        }
        getQuery().setTableMetaData(tableMetaData);
    }

    /**
     * 构建列信息
     */
    public void buildSelectItemList() {
        if (!$.isNotNullAndHasElement(this.getQuery().getSelectItemList())) {
            Column column = ObjectCreateFactory.createColumn();
            column.setColumnName(Column.star);
            this.getQuery().getSelectItemList().add(column);
        }
        // 如果有 * ，最后需要把*删掉
        List<Integer> deleteIndexList = new LinkedList<Integer>();
        int index = 0;
        for (Item item : getQuery().getSelectItemList()) {
            if (Column.star.equals(item.getColumnName())) {
                deleteIndexList.add(index);
                break;
            }
        }
        // 把星号替换成列，允许存在多个*，或则多个相同字段的列
        if (!deleteIndexList.isEmpty()) {
            List<Item> itemList = new ArrayList<Item>();
            for (int i = 0; i < this.getQuery().getSelectItemList().size(); i++) {
                if (!Column.star.equals(this.getQuery().getSelectItemList().get(i).getColumnName())) {
                    itemList.add(this.getQuery().getSelectItemList().get(i));
                } else {
                    for (ColumnMetaData columnMetaData : this.getQuery().getTableMetaData().getOrderByColumnMetaDataList()) {
                        Column column = ObjectCreateFactory.createColumn();
                        column.setColumnName(columnMetaData.getColumnName());
                        column.setType(columnMetaData.getType());
                        itemList.add(column);//
                    }
                }
            }
            this.getQuery().setSelectItemListAndSetNeedBuild(itemList);
        }
        for (int i = 0; i < getQuery().getSelectItemList().size(); i++) {
            getQuery().getSelectItemList().set(i, this.buildSelectItem(getQuery().getSelectItemList().get(i)));
        }

    }

    /**
     * TableMetaData中获取item
     */
    public Item getItemFromTableMetaDataOrQueryNodeToBeDependentOn(Item item) {
        // 如果存在表名，则进行强校验，比如字段为A.ID，否则直接进行ID名字匹配
        if (item.getTableName() != null //
                &&//
                !item.getTableName().equals(this.getQuery().getTableName()) //
                &&//
                !item.getTableName().equals(this.getQuery().getAlias())) {//
            return null;
        }
        if (Column.star.equals(item.getColumnName())) {
            return item;
        }
        if (item instanceof Function) {
            item.setTableName(this.getQuery().getTableName());
            return item;
        }

        ColumnMetaData columnMetaData = getQuery().getTableMetaData().getColumnMetaData(item.getColumnName());
        if (columnMetaData == null) {
            return null;
        }
        return OptimizerUtils.columnMetaDataToColumn(columnMetaData, getQuery().getTableName());
    }

}
