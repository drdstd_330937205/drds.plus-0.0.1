package drds.plus.sql_process.abstract_syntax_tree.expression.item.function;

public enum FunctionType {
    /**
     * 函数的操作面向一系列的值，并返回一个单一的值，可以理解为聚合函数
     */
    aggregate_function,
    /**
     * 函数的操作面向某个单一的值，并返回基于输入值的一个单一的值，可以理解为转换函数
     */
    scalar
}
