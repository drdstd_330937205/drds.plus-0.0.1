package drds.plus.sql_process.abstract_syntax_tree.execute_plan.query;

import drds.plus.sql_process.abstract_syntax_tree.execute_plan.ExecutePlan;

import java.util.List;


public interface MergeQuery extends Query<Query> {

    List<ExecutePlan> getExecutePlanList();

    void setExecutePlanList(List<ExecutePlan> executePlanList);

    /**
     * 获取一个subNode
     */
    ExecutePlan getExecutePlan();

    void addExecutePlan(ExecutePlan executePlan);

    /**
     * Merge可以根据中间结果得知具体在哪个节点上进行查询 所以Merge的分库操作可以放到执行器中进行
     *
     * @return true 表示已经经过sharding 。 false表示未经处理
     */
    Boolean isSharded();

    void setSharded(boolean sharded);

    Boolean isUnion();

    void setUnion(boolean isUnion);

    /**
     * 是否为group by分库键
     *
     * @return
     */
    boolean isGroupByShardColumns();

    void setGroupByShardColumns(boolean groupByShardColumns);

    /**
     * 是否为group by分库键
     *
     * @return
     */
    boolean isDistinctGroupByShardColumns();

    void setDistinctGroupByShardColumns(boolean distinctByShardColumns);

    /**
     * 是否为广播表多写
     */
    boolean isBroadcastPut();

    void setBroadcastPut(boolean broadcastPut);
}
