package drds.plus.sql_process.abstract_syntax_tree.execute_plan.query;

import drds.plus.sql_process.abstract_syntax_tree.IExecutePlanVisitor;
import drds.plus.sql_process.abstract_syntax_tree.ObjectCreateFactory;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.Filter;
import drds.plus.sql_process.utils.OptimizerUtils;

public class QueryWithIndexImpl extends QueryImpl implements QueryWithIndex {

    protected Filter keyFilter;

    protected String indexName;
    protected String tableName;
    protected Query subQuery;

    public Filter getKeyFilter() {
        return keyFilter;
    }

    public void setKeyFilter(Filter keyFilter) {
        this.keyFilter = keyFilter;

    }

    public void setTableName(String tableName) {
        this.tableName = tableName;

    }

    public String getTableName() {
        return this.tableName;
    }

    public String getIndexName() {
        return this.indexName;
    }

    public void setIndexName(String indexName) {
        this.indexName = indexName;

    }

    public void setSubQuery(Query subQuery) {
        this.subQuery = subQuery;

    }

    public Query getSubQuery() {
        return subQuery;
    }

    public QueryWithIndex copy() {
        QueryWithIndex queryWithIndex = ObjectCreateFactory.createQueryWithIndex();
        copySelfTo((QueryImpl) queryWithIndex);

        if (this.getSubQuery() != null) {
            queryWithIndex.setSubQuery((Query) this.getSubQuery().copy());
        }
        queryWithIndex.setTableName(this.getTableName());
        queryWithIndex.setIndexName(this.getIndexName());
        queryWithIndex.setKeyFilter(OptimizerUtils.copyFilter(this.getKeyFilter()));
        return queryWithIndex;
    }

    public void accept(IExecutePlanVisitor executePlanVisitor) {
        executePlanVisitor.visit(this);
    }

}
