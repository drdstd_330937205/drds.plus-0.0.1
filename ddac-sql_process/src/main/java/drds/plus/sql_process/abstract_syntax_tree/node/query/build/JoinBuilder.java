package drds.plus.sql_process.abstract_syntax_tree.node.query.build;

import drds.plus.sql_process.abstract_syntax_tree.ObjectCreateFactory;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.Item;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.column.Column;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.BooleanFilter;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.Filter;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.Function;
import drds.plus.sql_process.abstract_syntax_tree.expression.order_by.OrderBy;
import drds.plus.sql_process.abstract_syntax_tree.node.Node;
import drds.plus.sql_process.abstract_syntax_tree.node.query.Join;
import drds.plus.sql_process.abstract_syntax_tree.node.query.MergeQuery;
import drds.plus.sql_process.abstract_syntax_tree.node.query.Query;
import drds.plus.sql_process.utils.DnfFilters;
import drds.tools.$;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class JoinBuilder extends QueryBuilder {

    public JoinBuilder(Join joinNode) {
        this.setQuery(joinNode);
    }

    public Join getQuery() {
        return (Join) super.getQuery();
    }

    public void build() {
        if (this.getQuery().isPrimaryKeyJoin()) {
            return;
        }
        this.buildSelectItemList();
        this.buildJoinKeys();
        this.buildWhere();
        this.buildGroupBy();
        this.buildOrderBy();
        this.buildHaving();
        this.buildReferedItemList();
        this.buildExistAggregate();
        this.buildExistSequenceValue();
    }

    private void buildReferedItemList() {
        List<Item> referedItemList = new ArrayList();
        referedItemList.addAll(this.getQuery().getSelectItemList());
        for (BooleanFilter booleanFilter : this.getQuery().getJoinFilterList()) {
            Item left = (Item) booleanFilter.getColumn();
            Item right = (Item) booleanFilter.getValue();
            if (!referedItemList.contains(left)) {
                referedItemList.add(left);
            }
            if (!referedItemList.contains(right)) {
                referedItemList.add(right);
            }
        }

    }

    /**
     * 构建列信息
     */
    public void buildSelectItemList() {
        // 如果为空，代表是用*
        if ($.isNotNullAndHasElement(this.getQuery().getSelectItemList())) {
            Column column = ObjectCreateFactory.createColumn();
            column.setColumnName(Column.star);
            this.getQuery().getSelectItemList().add(column);
        }
        // 如果有 * ，最后需要把*删掉
        List<Integer> deleteIndexList = new LinkedList<Integer>();
        int index = 0;
        for (Item selected : getQuery().getSelectItemList()) {
            if (Column.star.equals(selected.getColumnName())) {
                deleteIndexList.add(index);
                break;
            }
        }
        if (!deleteIndexList.isEmpty()) {
            List<Item> itemList = new ArrayList<Item>();
            for (int i = 0; i < this.getQuery().getSelectItemList().size(); i++) {
                Item selectItem = this.getQuery().getSelectItemList().get(i);
                if (this.getQuery().getSelectItemList().get(i).getColumnName().equals(Column.star)) {
                    // 遇到*就把所有列再添加一遍
                    // query *,id这样的语法最后会有两个id列
                    // 各个节点进行分开处理
                    for (Node node : this.getQuery().getNodeList()) {
                        for (Item item : ((Query) node).copySelectItemList()) {
                            // 考虑
                            // a. SELECT B.* FROM TABLE1 A INNER JOIN TABLE2 B
                            // b. SELECT * FROM TABLE1 A INNER JOIN TABLE2 B
                            if (selectItem.getTableName() != null && !selectItem.getTableName().equals(item.getTableName())) {
                                continue;
                            }
                            Column column = ObjectCreateFactory.createColumn();
                            // 尝试复制子节点的表别名
                            if (((Query) node).getAlias() != null) {
                                column.setTableName(((Query) node).getAlias());
                            } else {
                                column.setTableName(item.getTableName());
                            }
                            if (item.getAlias() != null) {
                                column.setColumnName(item.getAlias());
                            } else {
                                column.setColumnName(item.getColumnName());
                            }
                            itemList.add(column);
                        }
                    }
                } else {
                    itemList.add(this.getQuery().getSelectItemList().get(i));
                }
            }
            this.getQuery().setSelectItemListAndSetNeedBuild(itemList);
        }
        for (int i = 0; i < getQuery().getSelectItemList().size(); i++) {
            getQuery().getSelectItemList().set(i, this.buildSelectItem(getQuery().getSelectItemList().get(i)));
        }

    }

    private void buildJoinKeys() {
        if (this.getQuery().isPrimaryKeyJoin()) {
            return;
        }
        List<BooleanFilter> otherJoinOnFilterList = new ArrayList(this.getQuery().getJoinFilterList().size());
        for (BooleanFilter booleanFilter : this.getQuery().getJoinFilterList()) {
            Item leftKey = null;
            if (booleanFilter.getColumn() != null && booleanFilter.getColumn() instanceof Item) {
                leftKey = this.getItemFromSelectItemListOfOtherQueryNodeAndUpdateTableName(this.getQuery().getLeftNode(), (Item) booleanFilter.getColumn());
            }
            Item rightKey = null;
            if (booleanFilter.getValue() != null && booleanFilter.getValue() instanceof Item) {
                rightKey = this.getItemFromSelectItemListOfOtherQueryNodeAndUpdateTableName(this.getQuery().getRightNode(), (Item) booleanFilter.getValue());
            }
            // 可能顺序调换了，重新找一次
            if (leftKey == null || rightKey == null) {
                if (booleanFilter.getValue() != null && booleanFilter.getValue() instanceof Item) {
                    leftKey = this.getItemFromSelectItemListOfOtherQueryNodeAndUpdateTableName(this.getQuery().getLeftNode(), (Item) booleanFilter.getValue());
                }
                if (booleanFilter.getColumn() != null && booleanFilter.getColumn() instanceof Item) {
                    rightKey = this.getItemFromSelectItemListOfOtherQueryNodeAndUpdateTableName(this.getQuery().getRightNode(), (Item) booleanFilter.getColumn());
                }
            }
            if (leftKey == null || rightKey == null) {
                // 可能有以下情况
                // id=1,s.id=s.key_id
                Filter otherJoinOnFilter = this.getQuery().getOtherJoinOnFilter();
                otherJoinOnFilter = DnfFilters.and(otherJoinOnFilter, booleanFilter);
                this.getQuery().setOtherJoinOnFilter(otherJoinOnFilter);
                otherJoinOnFilterList.add(booleanFilter);
                continue;
            }
            /**
             * 如果是回表操作，不能把索引的joinKey添加到temp中，否则如果有merge，这个列会被加到sql的select中， 而导致找不到列
             */
            if (!this.getQuery().isPrimaryKeyJoin()) {
                booleanFilter.setColumn(buildSelectItem(leftKey));
                booleanFilter.setValue(buildSelectItem(rightKey));
            }
        }
        this.getQuery().getJoinFilterList().removeAll(otherJoinOnFilterList);
        this.buildFilter(this.getQuery().getOtherJoinOnFilter(), false);
    }

    /**
     * 从左右节点获取
     */
    public Item getItemFromTableMetaDataOrQueryNodeToBeDependentOn(Item item) {
        if (item instanceof Function) {
            return item;
        }

        if (Column.star.equals(item.getColumnName())) {
            return item;
        }
        Query leftNode = this.getQuery().getLeftNode();
        Query rightNode = this.getQuery().getRightNode();
        //
        boolean leftNodeHasColumnItem = leftNode.hasItem(item);
        boolean rightNodeHasColumnItem = rightNode.hasItem(item);
        if (leftNodeHasColumnItem && rightNodeHasColumnItem) {
            throw new IllegalArgumentException("ColumnImpl '" + item.getColumnName() + "' is ambiguous in Join");
        }
        //
        Item itemFromLeftNode = null;
        Item itemFromRightNode = null;
        if (leftNodeHasColumnItem) {// 可能在select/from中
            itemFromLeftNode = this.getItemFromSelectItemListOfOtherQueryNodeAndUpdateTableName(leftNode, item);
            if (itemFromLeftNode == null) {// 如果不在select中，添加到select进行join传递
                leftNode.addSelectItem(item.copy());
                itemFromLeftNode = this.getItemFromSelectItemListOfOtherQueryNodeAndUpdateTableName(leftNode, item);
            }
        }
        if (rightNodeHasColumnItem) {// 可能在select/from中
            itemFromRightNode = this.getItemFromSelectItemListOfOtherQueryNodeAndUpdateTableName(rightNode, item);
            if (itemFromRightNode == null) {// 如果不在select中，添加到select进行join传递
                rightNode.addSelectItem(item.copy());
                itemFromRightNode = this.getItemFromSelectItemListOfOtherQueryNodeAndUpdateTableName(rightNode, item);
            }
        }
        return itemFromLeftNode == null ? itemFromRightNode : itemFromLeftNode;
    }


    protected void buildGroupBy() {
        super.buildGroupBy();

        // 如果子节点出现merge,说明不可下推,需要自己做计算
        if (this.getQuery().getLeftNode() instanceof MergeQuery || this.getQuery().getRightNode() instanceof MergeQuery) {
            // 检查group列是否在select列中
            for (OrderBy orderBy : query.getGroupByList()) {
                checkOrderByColumnExistInSelectList(query, orderBy);
            }
        }
    }

    protected void buildOrderBy() {
        super.buildOrderBy();

        // 如果子节点出现merge,说明不可下推,需要自己做计算
        if (this.getQuery().getLeftNode() instanceof MergeQuery || this.getQuery().getRightNode() instanceof MergeQuery) {
            // 检查group列是否在select列中
            for (OrderBy orderBy : query.getOrderByList()) {
                checkOrderByColumnExistInSelectList(query, orderBy);
            }
        }
    }


}
