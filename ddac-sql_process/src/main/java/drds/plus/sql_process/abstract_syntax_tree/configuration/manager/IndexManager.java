package drds.plus.sql_process.abstract_syntax_tree.configuration.manager;

import drds.plus.common.lifecycle.Lifecycle;
import drds.plus.sql_process.abstract_syntax_tree.configuration.IndexMapping;

/**
 * 获取索引信息
 */
public interface IndexManager extends Lifecycle {

    IndexMapping getIndexMetaData(String name);
}
