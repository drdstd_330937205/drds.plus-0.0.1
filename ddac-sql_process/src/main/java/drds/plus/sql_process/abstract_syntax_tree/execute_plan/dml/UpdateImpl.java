package drds.plus.sql_process.abstract_syntax_tree.execute_plan.dml;

import drds.plus.sql_process.abstract_syntax_tree.ObjectCreateFactory;

public class UpdateImpl extends Put<Update> implements Update {

    public UpdateImpl() {
        putType = PutType.update;
    }

    public Update copy() {
        Update update = ObjectCreateFactory.createUpdate();
        copySelfTo(update);
        return update;
    }
}
