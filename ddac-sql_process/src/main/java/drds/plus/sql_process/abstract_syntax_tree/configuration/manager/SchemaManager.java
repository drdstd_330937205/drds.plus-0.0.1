package drds.plus.sql_process.abstract_syntax_tree.configuration.manager;


import drds.plus.common.lifecycle.Lifecycle;
import drds.plus.sql_process.abstract_syntax_tree.configuration.TableMetaData;

import java.util.Collection;

/**
 * 用来描述一个逻辑表由哪些key-val组成的 <br/>
 */
public interface SchemaManager extends Lifecycle {


    TableMetaData getTableMetaData(String tableName);

    void putTable(String tableName, TableMetaData tableMetaData);

    Collection<TableMetaData> getAllTables();

    void reload();

}
