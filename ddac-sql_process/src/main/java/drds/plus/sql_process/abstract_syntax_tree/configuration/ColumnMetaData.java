package drds.plus.sql_process.abstract_syntax_tree.configuration;

import drds.plus.sql_process.type.Type;


/**
 * ColumnImpl 的元信息描述
 */
public class ColumnMetaData {


    /**
     * 列名
     */
    protected final String columnName;
    /**
     * 当前列的类型
     */
    protected final Type type;
    /**
     * 当前列的别名
     */
    protected final String alias;
    /**
     * 是否准许为空
     */
    protected final boolean nullable;
    /**
     * 是否为自增
     */
    protected final boolean autoIncrement;
    /**
     * 表名
     */
    private final String tableName;
    private String fullName;

    public ColumnMetaData(String tableName, String columnName, String alias, Type type, boolean nullable) {
        this.tableName = tableName;
        this.columnName = columnName;
        this.alias = alias;
        this.type = type;
        this.nullable = nullable;
        this.autoIncrement = false;
    }

    public ColumnMetaData(String tableName, String columnName, Type type, String alias, boolean nullable, boolean autoIncrement) {
        this.tableName = tableName;
        this.columnName = columnName;
        this.alias = alias;
        this.type = type;
        this.nullable = nullable;
        this.autoIncrement = autoIncrement;
    }

    public String getTableName() {
        return tableName;
    }

    public String getColumnName() {
        return columnName;
    }

    public Type getType() {
        return type;
    }

    public String getAlias() {
        return alias;
    }

    public boolean isNullable() {
        return nullable;
    }

    public boolean isAutoIncrement() {
        return autoIncrement;
    }

    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((alias == null) ? 0 : alias.hashCode());
        result = prime * result + (autoIncrement ? 1231 : 1237);
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        result = prime * result + ((columnName == null) ? 0 : columnName.hashCode());
        result = prime * result + (nullable ? 1231 : 1237);
        result = prime * result + ((tableName == null) ? 0 : tableName.hashCode());
        return result;
    }

    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ColumnMetaData other = (ColumnMetaData) obj;
        if (alias == null) {
            if (other.alias != null)
                return false;
        } else if (!alias.equals(other.alias))
            return false;
        if (autoIncrement != other.autoIncrement)
            return false;
        if (type == null) {
            if (other.type != null)
                return false;
        } else if (!type.equals(other.type))
            return false;
        if (columnName == null) {
            if (other.columnName != null)
                return false;
        } else if (!columnName.equals(other.columnName))
            return false;
        if (nullable != other.nullable)
            return false;
        if (tableName == null) {
            return other.tableName == null;
        } else
            return tableName.equals(other.tableName);
    }

    public String toStringWithInden(int inden) {
        StringBuilder sb = new StringBuilder();
        String tabTittle = "";// GeneralUtil.getTab(inden);
        sb.append(tabTittle).append(tableName).append(".");
        sb.append(columnName);
        if (alias != null) {
            sb.append(" setAliasAndSetNeedBuild ").append(alias);
        }
        return sb.toString();
    }

    public String toString() {
        return toStringWithInden(0);
    }

    public String getFullName() {
        if (this.fullName == null) {
            String cn = this.getAlias() != null ? this.getAlias() : this.getColumnName();
            String tableName = this.getTableName() == null ? "" : this.getTableName();
            StringBuilder sb = new StringBuilder(tableName.length() + 1 + cn.length());
            sb.append(this.getTableName());
            sb.append('.');
            sb.append(cn);

            this.fullName = sb.toString();
        }
        return this.fullName;
    }

}
