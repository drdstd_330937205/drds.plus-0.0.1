package drds.plus.sql_process.abstract_syntax_tree;

public interface Visitable {

    void accept(IExecutePlanVisitor executePlanVisitor);
}
