package drds.plus.sql_process.abstract_syntax_tree;

import drds.plus.sql_process.abstract_syntax_tree.execute_plan.dml.*;
import drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.*;
import drds.plus.sql_process.abstract_syntax_tree.expression.NullValue;
import drds.plus.sql_process.abstract_syntax_tree.expression.bind_value.BindValue;
import drds.plus.sql_process.abstract_syntax_tree.expression.bind_value.BindValueImpl;
import drds.plus.sql_process.abstract_syntax_tree.expression.bind_value.SequenceValue;
import drds.plus.sql_process.abstract_syntax_tree.expression.bind_value.SequenceValueImpl;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.column.Column;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.column.ColumnImpl;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.*;
import drds.plus.sql_process.abstract_syntax_tree.expression.order_by.OrderBy;
import drds.plus.sql_process.abstract_syntax_tree.expression.order_by.OrderByImpl;


public class ObjectCreateFactory {


    public static QueryWithIndex createQueryWithIndex() {
        return new QueryWithIndexImpl();
    }

    public static Replace createReplace() {
        return new ReplaceImpl();
    }

    public static Insert createInsert() {
        return new InsertImpl();
    }

    public static Delete createDelete() {
        return new DeleteImpl();
    }

    public static Update createUpdate() {
        return new UpdateImpl();
    }

    public static Column createColumn() {
        return new ColumnImpl();
    }

    public static OrderBy createOrderBy() {
        return new OrderByImpl();
    }

    public static Join createJoin() {
        return new JoinImpl();
    }

    public static MergeQuery createMergeQuery() {
        return new MergeQueryImpl();
    }

    public static BindValue createBindValue(int bind) {
        return new BindValueImpl(bind);
    }

    public static SequenceValue createSequenceValue(Object name) {
        return new SequenceValueImpl(name);
    }

    public static BooleanFilter createBooleanFilter() {
        return new BooleanFilterImpl();
    }

    public static OrsFilter createOrsFilter() {
        return new OrsFilterImpl();
    }

    public static LogicalOperationFilter createLogicalOperationFilter() {
        return new LogicalOperationFilterImpl();
    }

    public static Function createFunction() {
        return new FunctionImpl();
    }

    public static Comparable createNullValue() {
        return NullValue.getNullValue();
    }
}
