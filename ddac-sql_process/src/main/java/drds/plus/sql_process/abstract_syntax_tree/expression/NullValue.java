package drds.plus.sql_process.abstract_syntax_tree.expression;

import drds.plus.sql_process.abstract_syntax_tree.IExecutePlanVisitor;
import drds.plus.sql_process.abstract_syntax_tree.Visitable;

public class NullValue implements Comparable, Visitable {

    private static NullValue instance = new NullValue();
    private String string = "null";

    private NullValue() {
    }

    public static NullValue getNullValue() {
        return instance;
    }

    public int compareTo(Object o) {
        if (o == this) {
            return 0;
        }
        if (o instanceof NullValue) {
            return 0;
        }
        return -1;

    }

    public String toString() {
        return string;
    }

    public void accept(IExecutePlanVisitor executePlanVisitor) {
        executePlanVisitor.visit(this);
    }

}
