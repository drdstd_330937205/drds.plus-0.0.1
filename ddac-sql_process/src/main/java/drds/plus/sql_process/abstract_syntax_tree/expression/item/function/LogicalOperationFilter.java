package drds.plus.sql_process.abstract_syntax_tree.expression.item.function;

import java.util.List;

/**
 * 逻辑运算
 * logical expression . usually they are "and" and "or" filter
 */
public interface LogicalOperationFilter extends Filter<LogicalOperationFilter> {

    List<Filter> getFilterList();

    void setFilterList(List<Filter> filterList);

    Filter getLeft();

    Filter getRight();

    void setLeft(Filter left);

    void setRight(Filter Right);

    void addFilter(Filter filter);
}
