package drds.plus.sql_process.abstract_syntax_tree.expression.item.function;

public enum Operation {

    and(0),//
    or(1),//
    //
    greater_than(2),//
    greater_than_or_equal(4),//
    equal(6), //
    not_equal(10),//
    like(7), //
    less_than(3),//
    less_than_or_equal(5),//
    //
    is_null(8),//
    is_not_null(9),//
    is_true(18), //
    is_not_true(19), //
    is_false(16),//
    is_not_false(17),//
    //
    in(11), //
    is(12), //
    constant(13),//
    exists(20), //
    group_or(21),//

    // ALL
    greater_than_all(30),//
    greater_than_or_equal_all(32),//
    equal_all(34),//
    not_equal_all(35),//
    less_than_all(31), //
    less_than_or_equal_all(33),//
    // ANY
    greater_than_any(40), //
    greater_than_or_equal_any(42),//
    equal_any(44),//
    not_equal_any(45),//
    less_than_any(41), //
    less_than_or_equal_any(43); //

    private final int index;

    Operation(int index) {
        this.index = index;
    }

    public static Operation valueOf(int i) {
        for (Operation operation : values()) {
            if (operation.index == i) {
                return operation;
            }
        }

        throw new IndexOutOfBoundsException("Invalid ordinal");
    }

    public int getValue() {
        return index;
    }

    public boolean isAll() {
        return index >= Operation.greater_than_all.getValue() && index <= Operation.not_equal_all.getValue();
    }

    public boolean isAny() {
        return index >= Operation.greater_than_any.getValue() && index <= Operation.not_equal_any.getValue();
    }

    public String toString() {
        return String.valueOf(index);
    }

    public String getOperationString() {
        switch (this) {
            case and:
                return "and";
            case or:
                return "or";
            case greater_than:
                return ">";
            case less_than:
                return "<";
            case in:
                return "in";
            case greater_than_or_equal:
                return ">=";
            case less_than_or_equal:
                return "<=";
            case equal:
                return "=";
            case like:
                return "like";
            case is_null:
                return "is null";
            case is:
                return "is";
            case is_not_null:
                return "is not null";
            case not_equal:
                return "!=";
            case constant:
                return "constant";
//            case null_safe_equal:
//                return "<=>";

            case is_true:
                return "is true";
            case is_not_true:
                return "is not true";
            case is_false:
                return "is false";
            case is_not_false:
                return "is not false";
            case exists:
                return "exists";
            case greater_than_all:
                return "> all";
            case less_than_all:
                return "< all";
            case greater_than_or_equal_all:
                return ">= all";
            case less_than_or_equal_all:
                return "<= all";
            case equal_all:
                return "= all";
            case not_equal_all:
                return "!= all";
            case greater_than_any:
                return "> any";
            case less_than_any:
                return "< any";
            case greater_than_or_equal_any:
                return ">= any";
            case less_than_or_equal_any:
                return "<= any";
            case equal_any:
                return "= any";
            case not_equal_any:
                return "!= any";
            case group_or:
                return "or";
            default:
                return null;
        }
    }
}
