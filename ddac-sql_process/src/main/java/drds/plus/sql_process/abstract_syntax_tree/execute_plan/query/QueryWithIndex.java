package drds.plus.sql_process.abstract_syntax_tree.execute_plan.query;

import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.Filter;

public interface QueryWithIndex extends Query<Query> {

    /**
     * columnName 过滤器。 最重要的过滤器哦。 用来使用kv中的key进行查找。
     * 比较高的查询效率，这里的filter相当于使用map.get()的方式来取数据，所以效率很高。
     */
    Filter getKeyFilter();

    /**
     * columnName 过滤器。 最重要的过滤器哦。 用来使用kv中的key进行查找。
     * 比较高的查询效率，这里的filter相当于使用map.get()的方式来取数据，所以效率很高。
     */
    void setKeyFilter(Filter keyFilter);

    /**
     * 实际表名，student_0000
     */
    String getTableName();

    void setTableName(String actualTableName);

    /**
     * 索引名，逻辑上的，student._id
     */
    String getIndexName();

    void setIndexName(String indexName);

    /**
     * 获取子查询
     */
    Query getSubQuery();

    /**
     * 设置子查询
     */
    void setSubQuery(Query queryCommon);

}
