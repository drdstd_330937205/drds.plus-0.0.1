package drds.plus.sql_process.abstract_syntax_tree.node.dml;

import drds.plus.sql_process.abstract_syntax_tree.ObjectCreateFactory;
import drds.plus.sql_process.abstract_syntax_tree.execute_plan.dml.IPut;
import drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.Query;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.Item;
import drds.plus.sql_process.abstract_syntax_tree.node.query.TableQuery;
import drds.plus.sql_process.abstract_syntax_tree.node.query.TableQueryWithIndex;
import drds.plus.sql_process.optimizer.OptimizerContext;

import java.util.List;

public class Update extends Dml<Update> {

    public Update(TableQuery tableQueryNode) {
        super(tableQueryNode);
    }

    public List<Item> getUpdateColumnNameList() {
        return this.columnNameList;
    }

    public void setUpdateColumnNameList(List<Item> columnNameList) {
        this.columnNameList = columnNameList;

    }

    public List<Object> getUpdateColumnValueList() {
        return this.columnValueList;
    }

    public void setUpdateColumnValueList(List<Object> columnValueList) {
        this.columnValueList = columnValueList;

    }

    //
    public IPut toExecutePlan(int shareIndex) {
        drds.plus.sql_process.abstract_syntax_tree.execute_plan.dml.Update update = ObjectCreateFactory.createUpdate();
        for (Item item : this.getColumnNameList()) {
            List<String> shardColumnNameList = OptimizerContext.getOptimizerContext().getRouteOptimizer().getShardColumnNameList(this.getWhere().getTableMetaData().getTableName());
            if (shardColumnNameList.contains(item.getColumnName())) {
                throw new IllegalArgumentException("column :" + item.getColumnName() + " 是分库键，不允许修改");
            }
            if (this.getWhere().getTableMetaData().getPrimaryKeyKeyColumnNameToColumnMetaDataMap().containsKey(item.getColumnName())) {
                throw new IllegalArgumentException("column :" + item.getColumnName() + " 是主键，不允许修改");
            }
        }
        update.setConsistent(true);
        if (this.getWhere().getActualTableName() != null) {
            update.setTableName(this.getWhere().getActualTableName());
        } else if (this.getWhere() instanceof TableQueryWithIndex) {
            update.setTableName(((TableQueryWithIndex) this.getWhere()).getIndexName());
        } else {
            update.setTableName(this.getWhere().getTableName());
        }
        update.setIndexName(this.getWhere().getIndexMappingUsed().getIndexName());
        //
        update.setExistSequenceValue(this.isExistSequenceValue());
        //
        update.setUpdateItemList(this.getUpdateColumnNameList());
        update.setUpdateValues(this.getColumnValueList());
        update.setIsValueListList(this.isValueListList());
        update.setValueListList(this.getValueListList());
        update.setQueryTree((Query) this.getWhere().toExecutePlan());
        //
        update.setDataNodeId(this.getWhere().getDataNodeId());
        update.setBatchIndexList(this.getBatchIndexList());
        return update;
    }

    public Update deepCopy() {
        Update updateNode = new Update(null);
        super.deepCopySelfTo(updateNode);
        return updateNode;
    }

    public Update copy() {
        Update updateNode = new Update(null);
        super.copySelfTo(updateNode);
        return updateNode;
    }

    public boolean processAutoIncrement() {
        // update不开启
        return false;
    }

}
