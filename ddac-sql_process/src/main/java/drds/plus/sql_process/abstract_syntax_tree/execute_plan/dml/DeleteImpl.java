package drds.plus.sql_process.abstract_syntax_tree.execute_plan.dml;

import com.google.common.collect.Lists;
import drds.plus.sql_process.abstract_syntax_tree.ObjectCreateFactory;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.Item;

import java.util.List;

public class DeleteImpl extends Put<Delete> implements Delete {

    public DeleteImpl() {
        putType = PutType.delete;
    }

    public void setUpdateItemList(List<Item> itemList) {
        throw new UnsupportedOperationException();
    }

    public List<Item> getUpdateItemList() {
        return Lists.newArrayList();
    }

    public void setUpdateValues(List<Object> values) {
        throw new UnsupportedOperationException();
    }

    public List<Object> getUpdateValues() {
        return Lists.newArrayList();
    }

    public Delete copy() {
        Delete delete = ObjectCreateFactory.createDelete();
        copySelfTo(delete);
        return delete;
    }
}
