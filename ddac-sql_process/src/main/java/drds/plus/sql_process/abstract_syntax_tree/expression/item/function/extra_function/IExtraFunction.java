package drds.plus.sql_process.abstract_syntax_tree.expression.item.function.extra_function;

import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.Function;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.FunctionType;
import drds.plus.sql_process.type.Type;

/**
 * 扩展函数实例，比如用于实现Merge的count/min等聚合函数
 */
public interface IExtraFunction {

    /**
     * 设置function配置定义
     */
    void setFunction(Function function);

    /**
     * aggregate_function/scalar函数
     */
    FunctionType getFunctionType();

    /**
     * 获取最后返回结果类型
     */
    Type getReturnType();

    String[] getFunctionNames();

    void clear();
}
