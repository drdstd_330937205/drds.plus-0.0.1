package drds.plus.sql_process.abstract_syntax_tree.node.query.build;

import drds.plus.sql_process.abstract_syntax_tree.ObjectCreateFactory;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.Item;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.column.Column;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.BooleanFilter;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.Function;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.Operation;
import drds.plus.sql_process.abstract_syntax_tree.node.Node;
import drds.plus.sql_process.abstract_syntax_tree.node.query.$Query$;
import drds.plus.sql_process.abstract_syntax_tree.node.query.Query;

import java.util.LinkedList;
import java.util.List;

public class $Query$Builder extends QueryBuilder {

    public $Query$Builder($Query$ $Query$) {
        this.setQuery($Query$);
    }

    public $Query$ getQuery() {
        return ($Query$) super.getQuery();
    }

    public void build() {
        for (Node node : this.getQuery().getNodeList()) {
            node.build();
        }

        if (!(this.getQuery().getFirstSubNodeQueryNode() instanceof Query)) {// 嵌套子类
            return;
        }

        this.buildAlias();
        this.buildSelectItemList();

        this.buildWhere();
        this.buildGroupBy();
        this.buildOrderBy();

        if (this.getQuery().getDataNodeId() == null) {
            this.getQuery().setDataNodeId(this.getQuery().getFirstSubNodeQueryNode().getDataNodeId());
        }

        this.buildExistAggregate();
        this.buildExistSequenceValue();
    }

    private void buildAlias() {
        if (this.getQuery().getAlias() == null) {
            this.getQuery().setAliasAndSetNeedBuild(this.getQuery().getFirstSubNodeQueryNode().getAlias());
        }
    }

    public void buildSelectItemList() {
        if (this.getQuery().getSelectItemList().isEmpty()) {
            Column column = ObjectCreateFactory.createColumn();
            column.setColumnName(Column.star);
            this.getQuery().getSelectItemList().add(column);
        }

        // 如果有 * ，最后需要把*删掉
        List<Item> deleteItemList = new LinkedList();
        for (Item item : getQuery().getSelectItemList()) {
            if (item.getColumnName().equals(Column.star)) {
                deleteItemList.add(item);
            }
        }

        if (!deleteItemList.isEmpty()) {
            this.getQuery().getSelectItemList().removeAll(deleteItemList);
        }
        for (Item deleteItem : deleteItemList) {
            // 遇到*就把所有列再添加一遍
            // query *,id这样的语法最后会有两个id列，mysql是这样的
            Query query = this.getQuery().getFirstSubNodeQueryNode();
            for (Item item : query.getSelectItemList()) {
                if (deleteItem.getTableName() != null && !deleteItem.getTableName().equals(item.getTableName())) {
                    break;
                }
                //
                Column column = ObjectCreateFactory.createColumn();
                if (query.getAlias() != null) {// sub setAliasAndSetNeedBuild
                    column.setTableName(query.getAlias());
                } else {
                    column.setTableName(item.getTableName());
                }
                if (item.getAlias() != null) {
                    column.setColumnName(item.getAlias());
                } else {
                    column.setColumnName(item.getColumnName());
                }
                getQuery().getSelectItemList().add(column);// 允许多列
            }
        }

        for (int i = 0; i < getQuery().getSelectItemList().size(); i++) {
            getQuery().getSelectItemList().set(i, this.buildSelectItem(getQuery().getSelectItemList().get(i)));
        }

    }

    /**
     * (Query)中获取item
     */
    public Item getItemFromTableMetaDataOrQueryNodeToBeDependentOn(Item item) {
        if (Column.star.equals(item.getColumnName())) {
            return item;
        }
        if (item instanceof Function) {
            return item;
        }
        if (item instanceof BooleanFilter && ((BooleanFilter) item).getOperation().equals(Operation.constant)) {
            return item;
        }
        return this.getItemFromSelectItemListOfOtherQueryNodeAndUpdateTableName(this.getQuery().getFirstSubNodeQueryNode(), item);
    }


}
