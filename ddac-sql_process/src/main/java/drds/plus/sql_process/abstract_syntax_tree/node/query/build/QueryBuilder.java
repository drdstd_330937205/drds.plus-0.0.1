package drds.plus.sql_process.abstract_syntax_tree.node.query.build;


import drds.plus.sql_process.abstract_syntax_tree.ObjectCreateFactory;
import drds.plus.sql_process.abstract_syntax_tree.expression.bind_value.SequenceValue;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.Item;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.column.Column;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.*;
import drds.plus.sql_process.abstract_syntax_tree.expression.order_by.OrderBy;
import drds.plus.sql_process.abstract_syntax_tree.node.Node;
import drds.plus.sql_process.abstract_syntax_tree.node.dml.Dml;
import drds.plus.sql_process.abstract_syntax_tree.node.query.Query;
import drds.plus.sql_process.abstract_syntax_tree.node.query.TableQuery;
import drds.plus.sql_process.abstract_syntax_tree.node.query.TableQueryWithIndex;
import drds.plus.sql_process.utils.UniqueIdGenerator;
import drds.tools.$;

import java.util.List;

public abstract class QueryBuilder {

    protected Query query;

    public QueryBuilder() {
    }

    public Query getQuery() {
        return query;
    }

    public void setQuery(Query query) {
        this.query = query;
    }

    public abstract void build();

    protected void buildFunction() {
        for (Item item : getQuery().getSelectItemList()) {
            if (item instanceof Function) {
                this.buildFunction((Function) item, false);
            }
        }
    }

    protected void buildFunction(Function function, boolean findInSelectList) {
        if (FunctionType.aggregate_function == function.getFunctionType()) {
            setExistAggregateExpression();
        }
        if (function.getArgList().size() == 0) {
            return;
        }
        List<Object> argList = function.getArgList();
        for (int i = 0; i < argList.size(); i++) {
            if (argList.get(i) instanceof Item) {
                argList.set(i, this.buildSelectItem((Item) argList.get(i), findInSelectList));
            } else if (argList.get(i) instanceof SequenceValue) {
                setExistSequenceValue();
            }
        }
    }

    public Item buildSelectItem(Item item) {
        return this.buildSelectItem(item, false);
    }

    /**
     * 用于标记当前节点是否需要根据meta信息填充信息
     *
     * <pre>
     * SQL.
     *  a. select id + 2 as id , id from test where id = 2 having id = 4;
     *  b. select id + 2 as id , id from test where id = 2 order by count(id)
     *
     * 解释：
     * 1.  column/where/join中列，是取自from的表字段
     * 2.  having/order by/group by中的列，是取自select中返回的字段，获取对应别名数据
     * </pre>
     *
     * @param item
     * @param findInSelectItemList 如果在from的meta中找不到，是否继续在select中寻找
     */
    public Item buildSelectItem(Item item, boolean findInSelectItemList) {
        if (item == null) {
            return null;
        }
        if (item.getCorrelateSubQueryItemId() != null && item.getCorrelateSubQueryItemId() > 0L) {
            this.query.addCorrelatedSubQueryItem(item);
            return item;
        }
        // 比如select a.id from table1 a，将a.id改名为table1.id
        // 改为真正的表名
        if (item.getTableName() != null) {
            // 对于TableNode如果别名存在别名
            if (query instanceof TableQuery && (!(query instanceof TableQueryWithIndex))) {
                boolean equals =//
                        item.getTableName().equals(query.getAlias()) //
                                ||//
                                item.getTableName().equals(((TableQuery) query).getTableName());//
                if (query.isSubQuery() && query.getSubAlias() != null) {
                    equals |= item.getTableName().equals(query.getSubAlias());
                }
                if (!equals) {
                    if (query.getParent() == null) {
                        throw new IllegalArgumentException("column: " + item.getFullName() + " is not existed in either " + this.getQuery().getName() + " or selectStatement clause");
                    }
                } else {
                    item.setTableName(((TableQuery) query).getTableName());
                }
            }
        }

        Item column = null;
        if (findInSelectItemList) { // 优先查找select
            Item selectItemFromSelectItemList = getItemFromSelectItemList(item);
            if (selectItemFromSelectItemList != null) {
                column = selectItemFromSelectItemList;
                // 在select中找到了一次后，下次不能再从select中，遇到MAX(ID) AS ID会陷入死循环 但遇到count(JID)查找时，JID为select中的列，需要再次从select中查找
                findInSelectItemList = false;
            }
        }
        Item columnFromMetaData = null;
        boolean isCorrelate = false;
        if (column == null) {
            columnFromMetaData = this.getItemFromTableMetaDataOrQueryNodeToBeDependentOn(item);
            if (columnFromMetaData == null) {
                columnFromMetaData = getItemFromTableMetaDataOrQueryNodeToBeDependentOnOfParentQueryNode(item);
                if (columnFromMetaData != null) {
                    isCorrelate = true;
                }
            }
            if (columnFromMetaData != null) {
                column = columnFromMetaData;
                // 直接从子类的table定义中获取表字段，然后根据当前column状态，设置alias和distinct
                column.setAlias(item.getAlias());
                column.setDistinct(item.isDistinct());
            }
        }

        if (isCorrelate) {
            this.query.setCorrelatedSubQuery(true);
            // 替换为correlated value
            if (column.getCorrelateSubQueryItemId().equals(0L)) {
                column.setCorrelateSubQueryItemId(UniqueIdGenerator.genCorrelateSubQueryItemId());
            }
            this.query.addCorrelatedSubQueryItem(column);
        }
        if (column == null) {
            throw new IllegalArgumentException("column: " + item.getFullName() + " is not existed in either " + this.getQuery().getName() + " or query clause");
        }
        //
        if ((column instanceof Column) && !Column.star.equals(column.getColumnName())) {
            query.addReferedItem(column); // refered不需要重复字段,select添加允许重复
            if (column.isDistinct()) {
                setExistAggregateExpression();
            }
        }

        if (column instanceof Function) {
            if (isSubQuery((Function) column)) {
                Object arg = ((Function) column).getArgList().get(0);
                if (arg instanceof Query) {
                    buildSubQueryId((Function) column);
                }
            }
            buildFunction((Function) column, findInSelectItemList);
            // 尝试下推function
            // pushFunction((function) column);
        }

        return column;
    }

    /**
     * 从select列表中查找
     */
    private Item getItemFromSelectItemList(Item item) {
        Item result = null;
        for (Item selectItem : this.getQuery().getSelectItemList()) {
            if (item.getTableName() != null && (!(query instanceof TableQueryWithIndex))) {
                if (!item.getTableName().equals(selectItem.getTableName())) {
                    continue;
                }
            }
            boolean isThis = false;
            // 在当前select中查找，先比较column name，再比较alias
            if (selectItem.getColumnName().equals(item.getColumnName())) {
                isThis = true;
            } else if (selectItem.getAlias() != null && selectItem.getAlias().equals(item.getColumnName())) {
                isThis = true;
            }
            if (isThis) {
                result = selectItem;
                return result;
            }
        }
        return result;
    }


    protected abstract Item getItemFromTableMetaDataOrQueryNodeToBeDependentOn(Item item);

    /**
     * 解决correlated subquery问题, 当前对象找不到，尝试找一下parent节点
     */
    protected Item getItemFromTableMetaDataOrQueryNodeToBeDependentOnOfParentQueryNode(Item item) {
        if (this.getQuery().getParent() != null) {
            Item $item = this.getQuery().getParent().getQueryNodeBuilder().getItemFromTableMetaDataOrQueryNodeToBeDependentOn(item);
            if ($item != null) {
                String alias = this.getQuery().getParent().getAlias();
                if (alias != null) {
                    $item.setTableName(alias);
                }
            }
            return $item;
        } else {
            return null;
        }
    }


    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * 查找select item
     */
    public Item getItem(Item item) {
        /**
         *先在父节点里查找,在再自己节点查找
         */
        Item column = this.getItemFromSelectItemListOfOtherQueryNode(this.getQuery(), item);
        if (column == null) {
            column = this.getItemFromTableMetaDataOrQueryNodeToBeDependentOn(item);
        }
        return column;
    }

    /**
     * 从select列表中查找字段，并根据查找的字段信息进行更新，比如更新tableName
     */
    protected Item getItemFromSelectItemListOfOtherQueryNodeAndUpdateTableName(Query query, Item item) {
        Item selectItem = getItemFromSelectItemListOfOtherQueryNode(query, item);
        if (selectItem == null) {
            return null;
        }
        if (item instanceof Column) {
            item.setType(selectItem.getType());
            item.setAutoIncrement(selectItem.isAutoIncrement());
            // 如果是子表的结构，比如Join/Merge的子节点，字段的名字直接使用别名
            if (query.getAlias() != null) {
                item.setTableName(query.getAlias());
            } else {
                item.setTableName(selectItem.getTableName());
            }
        }
        return item;
    }

    /**
     * 从其他queryTreeNode的select列表中查找字段
     */
    protected Item getItemFromSelectItemListOfOtherQueryNode(Query query, Item item) {
        if (item == null) {
            return item;
        }

        if (item instanceof BooleanFilter && ((BooleanFilter) item).getOperation().equals(Operation.constant)) {
            return item;
        }

        Item result = null;
        for (Item selectItem : query.getSelectItemList()) {
            if (item.getTableName() != null) {
                boolean isSameName = item.getTableName().equals(query.getAlias()) ||//
                        item.getTableName().equals(selectItem.getTableName());//
                if (query.isSubQuery() && query.getSubAlias() != null) {
                    isSameName |= item.getTableName().equals(query.getSubAlias());//
                }
                if (!isSameName) {
                    continue;
                }
            }
            if (Column.star.equals(item.getColumnName())) {
                return item;
            }
            // 若列别名存在，只比较别名
            boolean isSameColumnName = item.isSameColumnName(selectItem);
            if (isSameColumnName) {
                if (result != null) {
                    // 说明出现两个ID，需要明确指定TABLE
                    throw new IllegalArgumentException("ColumnImpl: '" + item.getFullName() + "' is ambiguous by exist [" + selectItem.getFullName() + "," + result.getFullName() + "]");
                }
                result = selectItem;
            }
        }

        return result;
    }
    ////////////////////////


    protected void buildWhere() {
        // sql语法中，where条件中的列不允许使用别名，所以无需从select中找列
        this.buildFilter(query.getIndexQueryKeyFilter(), false);
        this.buildFilter(query.getWhere(), false);
        this.buildFilter(query.getResultFilter(), false);
        this.buildFilter(query.getOtherJoinOnFilter(), false);
        this.buildFilter(query.getSubQueryFunctionFilter(), false);
    }

    protected void buildFilter(Filter filter, boolean findInSelectItemList) {
        if (filter == null) {
            return;
        }

        if (filter instanceof LogicalOperationFilter) {
            for (Filter filter1 : ((LogicalOperationFilter) filter).getFilterList()) {
                this.buildFilter(filter1, findInSelectItemList);
            }
        } else if (filter instanceof OrsFilter) {
            for (Filter filter1 : ((OrsFilter) filter).getFilterList()) {
                this.buildFilter(filter1, findInSelectItemList);
            }
        } else {
            buildBooleanFilter((BooleanFilter) filter, findInSelectItemList);
        }
    }

    protected void buildBooleanFilter(BooleanFilter booleanFilter, boolean findInSelectItemList) {
        if (booleanFilter == null) {
            return;
        }
        Object column = booleanFilter.getColumn();
        Object value = booleanFilter.getValue();
        // ================= 处理子查询=================
        // subQuery，比如WHERE ID = (SELECT ID FROM A)
        if (column instanceof Function && isSubQuery((Function) column)) {
            Object arg = ((Function) column).getArgList().get(0);
            if (arg instanceof Query) {
                buildSubQueryId((Function) column);
                Query query = (Query) arg;
                query.build();
            }
        } else if (column instanceof Item) {
            booleanFilter.setColumn(this.buildSelectItem((Item) column, findInSelectItemList));
        } else if (column instanceof SequenceValue) {
            setExistSequenceValue();
        } else {
            //...
        }

        // subQuery，比如WHERE ID = (SELECT ID FROM A)
        if (value instanceof Function && isSubQuery((Function) value)) {
            Object arg = ((Function) value).getArgList().get(0);
            if (arg instanceof Query) {
                buildSubQueryId((Function) value);
                Query query = (Query) arg;
                query.build();
            }
        } else if (value instanceof Item) {
            booleanFilter.setValue(this.buildSelectItem((Item) value, findInSelectItemList));
        } else if (value instanceof SequenceValue) {
            setExistSequenceValue();
        } else {
            //...
        }

        if (value != null && value instanceof Function && ((Function) value).getArgList().size() > 0) {
            // 特殊优化 all/any
            // val > all (query...) -> val > max (query...)
            // val < all (query...) -> val < min (query...)
            // val > any (query...) -> val > min (query...)
            // val < any (query...) -> val < max (query...)
            // val >= all (query...) -> val >= max (query...)
            // val <= all (query...) -> val <= min (query...)
            // val >= any (query...) -> val >= min (query...)
            // val <= any (query...) -> val <= max (query...)
            // http://dev.mysql.com/doc/internals/en/transformations-all-any.html
            Object arg = ((Function) value).getArgList().get(0);
            if (arg instanceof Query) {
                Query query = (Query) ((Function) value).getArgList().get(0);
                // 如果出现ALL，右边一定是subquery
                if (query.getSelectItemList().size() != 1) { // 只能包含一列
                    throw new RuntimeException("Operand should contain 1 column(s)");
                }
                if (booleanFilter.getOperation().isAll()) {
                    // 针对出现group by时，不能简单替换为max/min值
                    if (!query.isExistAggregateExpression()) {
                        Item item = query.getSelectItemList().get(0);
                        switch (booleanFilter.getOperation()) {
                            case greater_than_all:
                                // id > max(id)
                                query.setSelectItemList(buildMaxOrMinFunction(item, true));
                                booleanFilter.setOperation(Operation.greater_than);
                                break;
                            case greater_than_or_equal_all:
                                // id > max(id)
                                query.setSelectItemList(buildMaxOrMinFunction(item, true));
                                booleanFilter.setOperation(Operation.greater_than_or_equal);
                                break;
                            case less_than_all:
                                // id < min(id)
                                query.setSelectItemList(buildMaxOrMinFunction(item, false));
                                booleanFilter.setOperation(Operation.less_than);
                                break;
                            case less_than_or_equal_all:
                                // id < min(id)
                                query.setSelectItemList(buildMaxOrMinFunction(item, false));
                                booleanFilter.setOperation(Operation.less_than_or_equal);
                                break;
                            default:
                                break;
                        }
                    }

                    // 针对没有成功优化为max/min的，需要转化为list计算,让执行计划拿到list数据
                    if (booleanFilter.getOperation().isAll()) {
                        // 转变为list计算,让执行计划拿到list数据
                        ((Function) value).setFunctionName(FunctionName.subquery_list);
                    }
                } else if (booleanFilter.getOperation().isAny()) {
                    // 针对出现group by时，不能简单替换为max/min值
                    if (!query.isExistAggregateExpression()) {
                        Item item = query.getSelectItemList().get(0);
                        switch (booleanFilter.getOperation()) {
                            case greater_than_any:
                                // id > min(id)
                                query.setSelectItemList(buildMaxOrMinFunction(item, false));
                                booleanFilter.setOperation(Operation.greater_than);
                                break;
                            case greater_than_or_equal_any:
                                // id > min(id)
                                query.setSelectItemList(buildMaxOrMinFunction(item, false));
                                booleanFilter.setOperation(Operation.greater_than_or_equal);
                                break;
                            case less_than_any:
                                // id < min(id)
                                query.setSelectItemList(buildMaxOrMinFunction(item, true));
                                booleanFilter.setOperation(Operation.less_than);
                                break;
                            case less_than_or_equal_any:
                                // id < min(id)
                                query.setSelectItemList(buildMaxOrMinFunction(item, true));
                                booleanFilter.setOperation(Operation.less_than_or_equal);
                                break;
                            default:
                                break;
                        }
                    }

                    // 针对没有成功优化为max/min的，需要转化为list计算,让执行计划拿到list数据
                    if (booleanFilter.getOperation().isAny()) {
                        // 转变为list计算,让执行计划拿到list数据
                        ((Function) value).setFunctionName(FunctionName.subquery_list);
                    }
                }
            }
        }

        if (booleanFilter.getOperation() == Operation.in) {
            List<Object> valueList = booleanFilter.getValueList();
            if (valueList != null && !valueList.isEmpty()) {
                // in的子查询
                if (valueList.get(0) instanceof Function && isSubQuery((Function) valueList.get(0))) {
                    Object arg = ((Function) valueList.get(0)).getArgList().get(0);
                    if (arg instanceof Query) {
                        buildSubQueryId((Function) valueList.get(0));
                        Query query = (Query) arg;
                        query.build();
                    }
                } else {
                    for (Object object : valueList) {
                        if (object instanceof SequenceValue) {
                            setExistSequenceValue();
                        }
                    }
                }
            }
        }
    }

    private boolean isSubQuery(Function function) {
        return function.getFunctionName().equals(FunctionName.subquery_list) || function.getFunctionName().equals(FunctionName.subquery_scalar);
    }

    protected void buildSubQueryId(Function function) {
        Query query = (Query) function.getArgList().get(0);
        Long subQueryId = query.getSubQueryId();
        if (subQueryId.equals(0L)) {
            query.setSubQueryId(UniqueIdGenerator.genCorrelateSubQueryItemId());
        }
        return;
    }

    private Function buildMaxOrMinFunction(Item item, boolean isMax) {
        Function function = ObjectCreateFactory.createFunction();
        function.setFunctionName(isMax ? "max" : "min");
        function.getArgList().add(item);
        //
        function.setColumnName(function.getFunctionName() + "(" + item.getColumnName() + ")");

        return function;
    }


    protected void buildGroupBy() {
        for (OrderBy orderBy : query.getGroupByList()) {
            if (orderBy.getItem() instanceof Item) {
                orderBy.setColumn(this.buildSelectItem(orderBy.getItem(), true));
            }
        }
        if ($.isNotNullAndHasElement(query.getGroupByList())) {
            setExistAggregateExpression();
        }
    }

    protected void buildHaving() {
        // having是允许使用select中的列的，如 havaing count(id)>1
        this.buildFilter(this.getQuery().getHaving(), true);
    }

    protected void buildOrderBy() {
        for (OrderBy orderBy : query.getOrderByList()) {
            if (orderBy.getItem() instanceof Item) {
                orderBy.setColumn(this.buildSelectItem(orderBy.getItem(), true));
            }
        }
    }


    protected void buildExistAggregate() {
        // 存在distinct
        for (Item item : this.query.getReferedItemList()) {
            if (item.isDistinct() || (item instanceof Function && ((Function) item).isNeedDistinctArg())) {
                setExistAggregateExpression();
                return;
            }
        }
        // 如果子节点中有一个是聚合查询，则传递到父节点
        for (Node node : this.getQuery().getNodeList()) {
            if (node instanceof Query) {
                if (((Query) node).isExistAggregateExpression()) {
                    setExistAggregateExpression();
                    return;
                }
            }
        }
    }

    /**
     * 目前order by/group by中的列必须在select列中存在
     */
    protected void checkOrderByColumnExistInSelectList(Query query, OrderBy orderBy) {
        Item item = orderBy.getItem();
        if (item instanceof Function) {
            Item column = getItemFromSelectItemListOfOtherQueryNodeAndUpdateTableName(query, item);
            if (column == null) {
                throw new RuntimeException("ColumnImpl: " + orderBy.getItem() + " is not existed in query clause");
            }
        }
    }

    /**
     * 含有group by条件 或者 字段含有distinct标记,或者 是聚合函数,或者函数有isNeed distinct标记
     */
    protected void setExistAggregateExpression() {
        this.query.setExistAggregateExpression(true);
    }

    protected void buildExistSequenceValue() {
        // 如果子节点中有一个是聚合查询，则传递到父节点
        for (Node node : this.getQuery().getNodeList()) {
            if (node instanceof Query) {//merge暂时不进行判断
                if (node.isExistSequenceValue()) {
                    setExistSequenceValue();
                    return;
                }
            } else if (node instanceof Dml) {
                if (node.isExistSequenceValue()) {
                    setExistSequenceValue();
                    return;
                }
            }
        }
    }


    protected void setExistSequenceValue() {
        this.query.setExistSequenceValue(true);
    }


}
