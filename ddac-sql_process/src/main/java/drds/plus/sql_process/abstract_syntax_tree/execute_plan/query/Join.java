package drds.plus.sql_process.abstract_syntax_tree.execute_plan.query;

import drds.plus.sql_process.abstract_syntax_tree.expression.item.Item;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.Filter;

import java.util.List;


public interface Join extends Query<Query> {

    /**
     * 设置join节点
     */
    void setJoinNodes(Query left, Query right);

    /**
     * 获取 join 元素
     */
    Query getLeftNode();

    /**
     * 设置左join 元素
     */
    void setLeftNode(Query left);

    /**
     * 获取左join元素
     */
    Query getRightNode();

    /**
     * 设置右join 元素
     */
    void setRightNode(Query right);

    /**
     * 设置join的列
     */
    void setJoinItemList(List<Item> leftColumns, List<Item> rightColumns);

    /**
     * 添加左left join的列
     */
    void addLeftJoinColumn(Item left);

    /**
     * 获取左join的列
     */
    List<Item> getLeftJoinColumnList();

    /**
     * 添加 右left join的列
     */
    void addRightJoinOnColumn(Item right);

    /**
     * 获取右join的列名
     */
    List<Item> getRightJoinColumnList();

    /**
     * 获取join类型
     */
    JoinStrategy getJoinStrategy();

    /**
     * 设置join类型
     */
    void setJoinStrategy(JoinStrategy joinStrategy);

    /**
     * 是否左outer join
     */
    Boolean isLeftOuterJoin();

    /**
     * 获取左outer join
     */
    void setLeftOuterJoin(boolean on_off);

    /**
     * 是否右outer join
     */
    Boolean isRightOuterJoin();

    /**
     * 设置右 outer join
     */
    void setRightOuterJoin(boolean on_off);

    /**
     * 包含where后的所有条件，仅用于拼sql
     */
    Filter getWhereFilter();

    void setWhereFilter(Filter whereFilter);

}
