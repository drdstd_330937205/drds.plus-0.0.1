package drds.plus.sql_process.abstract_syntax_tree.execute_plan.query;

import drds.plus.sql_process.abstract_syntax_tree.IExecutePlanVisitor;
import drds.plus.sql_process.abstract_syntax_tree.ObjectCreateFactory;
import drds.plus.sql_process.abstract_syntax_tree.execute_plan.ExecutePlan;

import java.util.LinkedList;
import java.util.List;

public class MergeQueryImpl extends QueryImpl implements MergeQuery {

    protected List<ExecutePlan> executePlanList = new LinkedList<ExecutePlan>();
    protected Boolean isSharded = true;
    protected Boolean isUnion = false;
    protected Boolean isGroupByShardColumns = false;
    protected Boolean isDistinctShardColumns = false;
    protected Boolean isDmlByBroadcast = false;

    public Query copy() {
        MergeQuery mergeQuery = ObjectCreateFactory.createMergeQuery();
        this.copySelfTo((QueryImpl) mergeQuery);
        for (ExecutePlan executePlan : this.getExecutePlanList()) {
            mergeQuery.addExecutePlan(executePlan.copy());
        }
        mergeQuery.setSharded(this.isSharded);
        mergeQuery.setUnion(this.isUnion);
        mergeQuery.setGroupByShardColumns(this.isGroupByShardColumns);
        mergeQuery.setDistinctGroupByShardColumns(this.isDistinctShardColumns);
        return mergeQuery;
    }

    public void accept(IExecutePlanVisitor executePlanVisitor) {
        executePlanVisitor.visit(this);
    }

    public List<ExecutePlan> getExecutePlanList() {
        return executePlanList;
    }

    public ExecutePlan getExecutePlan() {
        if (this.executePlanList.isEmpty()) {
            return null;
        }

        return executePlanList.get(0);
    }

    public void setExecutePlanList(List<ExecutePlan> executePlanList) {
        this.executePlanList = executePlanList;

    }

    public void addExecutePlan(ExecutePlan executePlan) {
        executePlanList.add(executePlan);

    }

    public Boolean isSharded() {
        return isSharded;
    }

    public void setSharded(boolean isSharded) {
        this.isSharded = isSharded;

    }

    public Boolean isUnion() {
        return isUnion;
    }

    public void setUnion(boolean isUnion) {
        this.isUnion = isUnion;

    }

    public boolean isGroupByShardColumns() {
        return isGroupByShardColumns;
    }

    public void setGroupByShardColumns(boolean isGroupByShardColumns) {
        this.isGroupByShardColumns = isGroupByShardColumns;

    }

    public boolean isDistinctGroupByShardColumns() {
        return isDistinctShardColumns;
    }

    public void setDistinctGroupByShardColumns(boolean distinctByShardColumns) {
        this.isDistinctShardColumns = distinctByShardColumns;

    }

    public boolean isBroadcastPut() {
        return isDmlByBroadcast;
    }

    public void setBroadcastPut(boolean broadcastPut) {
        this.isDmlByBroadcast = broadcastPut;

    }

}
