package drds.plus.sql_process.abstract_syntax_tree.execute_plan.dml;

import drds.plus.sql_process.abstract_syntax_tree.expression.item.Item;

import java.util.List;


public interface Insert extends IPut<Insert> {

    List<Object> getDuplicateUpdateValues();

    void setDuplicateUpdateValues(List<Object> duplicateUpdateValues);

    List<Item> getDuplicateUpdateColumns();

    void setDuplicateUpdateColumns(List<Item> duplicateUpdateColumns);

}
