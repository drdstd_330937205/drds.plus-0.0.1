package drds.plus.sql_process.abstract_syntax_tree.execute_plan.dml;

import drds.plus.sql_process.abstract_syntax_tree.ObjectCreateFactory;

public class ReplaceImpl extends Put<Replace> implements Replace {

    public ReplaceImpl() {
        putType = PutType.replace;
    }

    public Replace copy() {
        Replace replace = ObjectCreateFactory.createReplace();
        copySelfTo(replace);
        return replace;
    }
}
