package drds.plus.sql_process.abstract_syntax_tree.expression.bind_value;

import drds.plus.common.jdbc.Parameters;
import drds.plus.sql_process.abstract_syntax_tree.IExecutePlanVisitor;
import drds.plus.sql_process.abstract_syntax_tree.Visitable;

/**
 * 绑定变量
 */
public interface BindValue extends Comparable, Visitable {

    Object assignment(Parameters parameters);

    void accept(IExecutePlanVisitor executePlanVisitor);

    int getIndex();

    /**
     * 返回绑定变量的具体值
     */
    Object getValue();

    BindValue copy();
}
