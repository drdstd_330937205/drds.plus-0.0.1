package drds.plus.sql_process.abstract_syntax_tree;

import drds.plus.sql_process.abstract_syntax_tree.execute_plan.dml.Delete;
import drds.plus.sql_process.abstract_syntax_tree.execute_plan.dml.Insert;
import drds.plus.sql_process.abstract_syntax_tree.execute_plan.dml.Replace;
import drds.plus.sql_process.abstract_syntax_tree.execute_plan.dml.Update;
import drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.Join;
import drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.QueryWithIndex;
import drds.plus.sql_process.abstract_syntax_tree.expression.NullValue;
import drds.plus.sql_process.abstract_syntax_tree.expression.bind_value.BindValue;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.column.Column;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.Filter;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.Function;
import drds.plus.sql_process.abstract_syntax_tree.expression.order_by.OrderBy;

import java.util.List;

public interface IExecutePlanVisitor {

    void visit(Column column);

    void visit(Filter filter);

    void visit(Function function);

    void visit(Join join);

    void visit(OrderBy orderBy);

    void visit(QueryWithIndex queryWithIndex);

    void visit(NullValue nullValue);

    void visit(List list);

    void visit(Object o);

    void visit(Replace replace);

    void visit(Insert insert);

    void visit(Update update);

    void visit(Delete delete);

    void visit(BindValue bindValue);
}
