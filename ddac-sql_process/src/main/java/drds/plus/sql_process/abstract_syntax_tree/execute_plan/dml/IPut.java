package drds.plus.sql_process.abstract_syntax_tree.execute_plan.dml;

import drds.plus.sql_process.abstract_syntax_tree.execute_plan.ExecutePlan;
import drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.Query;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.Item;

import java.util.List;

public interface IPut<T extends IPut> extends ExecutePlan<T> {

    /**
     * depend where filter
     */
    Query getQuery();

    void setQueryTree(Query query);

    List<Item> getUpdateItemList();

    /**
     * set a = 1 ,b = 2 , c = 3 那么这个应该是 [‘a‘,‘b‘,‘c‘]
     *
     * @param columns
     */
    void setUpdateItemList(List<Item> columns);

    String getTableName();

    void setTableName(String indexName);

    String getIndexName();

    void setIndexName(String indexName);

    List<Object> getUpdateValues();

    /**
     * set a = 1 ,b = 2 , c = 3 那么这个应该是 [1，2，3]
     */
    void setUpdateValues(List<Object> values);

    PutType getPutType();


    /**
     * 用于多值insert
     */
    List<List<Object>> getValueListList();

    boolean isValueListList();

    void setValueListList(List<List<Object>> multiValues);

    void setIsValueListList(boolean isMutiValues);

    int getValueListListSize();

    List<Object> getValueList(int index);

    /**
     * 这个节点上执行哪些batch
     */
    List<Integer> getBatchIndexList();

    void setBatchIndexList(List<Integer> batchIndexList);


}
