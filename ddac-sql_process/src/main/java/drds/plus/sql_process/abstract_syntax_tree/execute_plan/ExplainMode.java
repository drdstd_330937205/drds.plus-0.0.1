package drds.plus.sql_process.abstract_syntax_tree.execute_plan;

public enum ExplainMode {
    /**
     * 逻辑模式,不输出物理表
     */
    LOGIC,
    /**
     * 简略模式,输出物理表,会对LOGIC相同的做合并
     */
    SIMPLE,
    /**
     * 详细模式，在simple模式下，输出列信息
     */
    DETAIL;

    public boolean isLogic() {
        return this == LOGIC || isSimple();
    }

    public boolean isSimple() {
        return this == SIMPLE || isDetail();
    }

    public boolean isDetail() {
        return this == DETAIL;
    }

}
