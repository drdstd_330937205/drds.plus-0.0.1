package drds.plus.sql_process.abstract_syntax_tree.node.dml;

import drds.plus.sql_process.abstract_syntax_tree.ObjectCreateFactory;
import drds.plus.sql_process.abstract_syntax_tree.execute_plan.dml.IPut;
import drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.Query;
import drds.plus.sql_process.abstract_syntax_tree.node.query.TableQuery;
import drds.plus.sql_process.abstract_syntax_tree.node.query.TableQueryWithIndex;

public class Delete extends Dml<Delete> {

    public Delete(TableQuery tableQueryNode) {
        super(tableQueryNode);
    }

    public IPut toExecutePlan(int shareIndex) {
        drds.plus.sql_process.abstract_syntax_tree.execute_plan.dml.Delete delete = ObjectCreateFactory.createDelete();
        delete.setConsistent(true);
        //
        if (this.getWhere().getActualTableName() != null) {
            delete.setTableName(this.getWhere().getActualTableName());
        } else if (this.getWhere() instanceof TableQueryWithIndex) {
            delete.setTableName(((TableQueryWithIndex) this.getWhere()).getIndexName());
        } else {
            delete.setTableName(this.getWhere().getTableName());
        }
        delete.setIndexName(this.getWhere().getIndexMappingUsed().getIndexName());
        //
        delete.setExistSequenceValue(this.isExistSequenceValue());
        //
        delete.setIsValueListList(this.isValueListList());
        delete.setValueListList(this.getValueListList());
        delete.setQueryTree((Query) this.getWhere().toExecutePlan());
        //
        delete.setDataNodeId(this.getWhere().getDataNodeId());
        delete.setBatchIndexList(this.getBatchIndexList());

        return delete;
    }

    public Delete deepCopy() {
        Delete deleteNode = new Delete(null);
        super.deepCopySelfTo(deleteNode);
        return deleteNode;
    }

    public Delete copy() {
        Delete deleteNode = new Delete(null);
        super.copySelfTo(deleteNode);
        return deleteNode;
    }

    public boolean processAutoIncrement() {
        // delete不开启
        return false;
    }

}
