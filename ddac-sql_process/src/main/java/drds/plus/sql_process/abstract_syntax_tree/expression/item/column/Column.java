package drds.plus.sql_process.abstract_syntax_tree.expression.item.column;

import drds.plus.sql_process.abstract_syntax_tree.expression.item.Item;

/**
 * 代表一个列信息，非函数列
 */
public interface Column extends Item<Column> {

    String star = "*";
}
