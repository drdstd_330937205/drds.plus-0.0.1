package drds.plus.sql_process.abstract_syntax_tree.execute_plan.query;

/**
 * 是否并行
 */
public enum QueryConcurrencyWay {
    // sequential<data_node_concurrent<concurrent
    sequential, // 完全串行
    data_node_concurrent, // 库间并行 库内串行
    concurrent// 完全并行
}
