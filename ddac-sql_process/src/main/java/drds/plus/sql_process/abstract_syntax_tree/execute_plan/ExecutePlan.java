package drds.plus.sql_process.abstract_syntax_tree.execute_plan;

import drds.plus.sql_process.abstract_syntax_tree.Visitable;

/**
 * 描述执行计划
 */
public interface ExecutePlan<T extends ExecutePlan> extends Visitable {

    String USE_LAST_DATA_NODE = "_USE_LAST_DATA_NODE_";

    String LAST_SEQUENCE_VALUE = "LAST_SEQUENCE_VALUE";

    String DUAL_GROUP = "DUAL_GROUP";

    /**
     * 设定当前的查询在哪里执行
     *
     * @param dataNodeId 执行的逻辑节点名字
     */
    void setDataNodeId(String dataNodeId);

    /**
     * 获取当前执行节点在哪里执行
     */
    String getDataNodeId();

    /**
     * 当前查询是否要求是一个实时一致性读写请求。
     */
    boolean isConsistent();

    /**
     * 设定当前节点是要求一个实时一致性读写读取。
     */
    void setConsistent(boolean consistent);

    /**
     * 获取请求的源hostName,主要是结合requestId两个属性来唯一的标志这个请求。
     * 用于让所有的机器都能追踪到这个请求，并且能够估算请求运行的百分比。
     */
    String getRequestHostName();

    /**
     * 设置发起请求的源hostName.可选，主要是结合requestID 两个属性来唯一的标志这个请求用的。
     */
    void setRequestHostName(String requestHostName);

    /**
     * 获取这个请求的requestID
     */
    Long getRequestId();

    /**
     * 设置这个请求的全局requestID
     */
    void setRequestId(Long requestId);

    /**
     * 获取子请求ID 用于标记一个查询是否能够被trace.如果能够被trace
     * 那么就应该设置hostName,全局requestID并且设置subRequestID
     */
    Long getSubRequestId();

    /**
     * 设置子请求ID. 用于标记一个查询是否能够被trace.如果能够被trace
     * 那么就应该设置hostName,全局requestID并且设置subRequestID
     */
    void setSubRequestId(Long subRequestId);


    Integer getThreadNo();

    /**
     * 表明一个建议的用于执行该节点的线程id
     */
    void setThreadNo(Integer threadNo);


    boolean isStreaming();

    void setStreaming(boolean streaming);

    String getSql();

    void setSql(String sql);

    boolean isLazyLoad();

    void setLazyLoad(boolean lazyLoad);

    boolean isExistSequenceValue();

    void setExistSequenceValue(boolean existSequenceValue);

    Long getLastSequenceValue();

    void setLastSequenceValue(Long sequenceValue);

    ExplainMode getExplainMode();

    void setExplainMode(ExplainMode mode);

    boolean isExplain();

    T copy();

    // ------------------复制----------------

}
