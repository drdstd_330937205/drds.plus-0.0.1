package drds.plus.sql_process.abstract_syntax_tree.expression.item.function;

import drds.plus.sql_process.abstract_syntax_tree.expression.item.Item;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.extra_function.IExtraFunction;

import java.util.List;

/**
 * 代表一个函数列，比如max(id)
 */
public interface Function<T extends Function> extends Item<T> {

    String getFunctionName();

    void setFunctionName(String funcName);

    FunctionType getFunctionType();

    List getArgList();

    void setArgList(List list);

    boolean isNeedDistinctArg();

    void setNeedDistinctArg(boolean b);

    /**
     * 获取执行函数实现
     */
    IExtraFunction getExtraFunction();

    /**
     * 设置执行函数
     */
    void setExtraFunction(IExtraFunction function);

}
