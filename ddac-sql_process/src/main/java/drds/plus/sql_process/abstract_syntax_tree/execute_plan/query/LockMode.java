package drds.plus.sql_process.abstract_syntax_tree.execute_plan.query;

public enum LockMode {
    undefined, shared_lock, exclusive_lock
}
