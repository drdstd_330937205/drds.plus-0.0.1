package drds.plus.sql_process.utils.range;

import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.Filter;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * 比如条件 a>10 || a=5 ||a < 1 min=1 max=10 equalList=[5]
 */
public class OrRangeProcessor extends AbstractRangeProcessor {

    private final List<List<Filter>> filterListList = new ArrayList();
    /**
     * 其他不符合rang的条件
     */
    private final List<Range> rangeList = new LinkedList<Range>();
    private final Object column;
    private boolean fullSet = false;

    public OrRangeProcessor(Object c) {
        this.column = c;
    }

    public boolean isFullSet() {
        return fullSet;
    }

    public boolean process(Filter filter) {
        if (isFullSet()) {
            return true;
        }

        Range range = getRange(filter);
        if (range == null) {
            List<Filter> filterList = new ArrayList();
            filterList.add(filter);
            this.filterListList.add(new ArrayList(filterList));
            return true;
        }

        for (int i = 0; i < rangeList.size(); i++) {
            Range rangeExisted = rangeList.get(i);
            /**
             * 可能模式 A {(a )a和(b )b-> (a(b a)b)有交集 /(a(b)b)a}有交集 ,B{%a]a [b%b}-> %a]a
             * [b%b}无交集/%a[b ]a %b有交集（全集）
             */
            if (rangeExisted.intersects(range)) {
                // 两个区间有交际，但又互不是包含关系，则为全集，比如 a < 2 or a > 1
                if (!rangeExisted.contains(range) && !range.contains(rangeExisted)) {
                    this.fullSet = true;
                    return true;
                } else {
                    // 若存在包含关系，则取并集
                    rangeList.set(i, rangeExisted.union(range));
                    return true;
                }
            } else {
                // 如果不相交，则判断是否是=，并判断能否合并，比如 A > 5 , A = 5
                if (range.isSingleValue()) {
                    if (range.getMaxValue().equals(rangeExisted.getMinValue()) || range.getMaxValue().equals(rangeExisted.getMaxValue())) {
                        rangeList.set(i, rangeExisted.union(range));
                        return true;
                    }
                }
                if (rangeExisted.isSingleValue()) {
                    if (rangeExisted.getMaxValue().equals(range.getMinValue()) || rangeExisted.getMaxValue().equals(range.getMaxValue())) {
                        rangeList.set(i, rangeExisted.union(range));
                        return true;
                    }
                }
            }

        }
        // 若跟已存在的哪个区间都没有交集，则为独立的区间，加到range列表里
        if (range != null) {
            rangeList.add(range);
        }

        return true;
    }

    public List<List<Filter>> toFilterListList() {

        if (this.isFullSet()) {
            return new LinkedList<List<Filter>>();
        }
        List<List<Filter>> filterListListCopy = new LinkedList();
        filterListListCopy.addAll(this.filterListList);
        for (Range range : rangeList) {
            filterListListCopy.add(buildFilterList(range, column));
        }
        return filterListListCopy;
    }

}
