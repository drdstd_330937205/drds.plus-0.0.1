package drds.plus.sql_process.rule;

import drds.plus.common.lifecycle.AbstractLifecycle;
import drds.plus.sql_process.abstract_syntax_tree.configuration.IndexMapping;
import drds.plus.sql_process.abstract_syntax_tree.configuration.manager.IndexManager;
import drds.plus.sql_process.abstract_syntax_tree.configuration.manager.SchemaManager;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;

/**
 * 基于{@linkplain RuleSchemaManager}完成index的获取
 */
@Slf4j
public class IndexManagerImpl extends AbstractLifecycle implements IndexManager {
    @Override
    public Logger log() {
        return log;
    }

    private SchemaManager schemaManager;

    public IndexManagerImpl(SchemaManager schemaManager) {
        this.schemaManager = schemaManager;
    }

    public IndexMapping getIndexMetaData(String name) {
        int index = name.indexOf(".");//pk
        if (index < 0) {
            index = name.length();
        }
        String tableName = name.substring(0, index);
        IndexMapping indexMapping = schemaManager.getTableMetaData(tableName).getIndexMetaData(name);
        if (indexMapping == null) {
            throw new IllegalArgumentException("where : " + tableName + " index : " + index + " is not found");
        }
        return indexMapping;
    }
}
