package drds.plus.sql_process.type.impl;

import com.google.common.primitives.Bytes;
import drds.plus.common.model.RowValues;
import drds.plus.sql_process.type.*;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * {@link Bytes} 类型
 */
public class BytesType extends AbstractType<byte[]> {

    public Class getValueClass() {
        return byte[].class;
    }

    public int encode(Object value, byte[] bytes, int offset) {
        return Encoder.encode(this.convert(value), bytes, offset);
    }

    public int getLength(Object value) {
        if (value == null) {
            return 1;
        } else {
            return Encoder.calculateEncodedLength(convert(value));
        }
    }

    public DecodeResult decode(byte[] bytes, int offset) {
        byte[][] data = new byte[0][];
        int length = Decoder.decode(bytes, offset, data);
        return new DecodeResult(data[0], length);
    }

    public byte[] incr(Object value) {
        throw new UnsupportedOperationException("bytes类型不支持incr操作");
    }

    public byte[] decr(Object value) {
        throw new UnsupportedOperationException("bytes类型不支持decr操作");
    }

    public byte[] getMaxValue() {
        return new byte[]{Byte.MAX_VALUE};
    }

    public byte[] getMinValue() {
        return new byte[]{Byte.MIN_VALUE};
    }

    public Calculator getCalculator() {
        throw new UnsupportedOperationException("bytes类型不支持计算操作");
    }

    public ResultGetter getResultGetter() {
        return new ResultGetter() {

            public Object get(ResultSet resultSet, int index) throws SQLException {
                return resultSet.getBytes(index);
            }

            public Object get(RowValues rowValues, int index) {
                Object val = rowValues.getObject(index);
                return convert(val);
            }
        };
    }

    public int compare(Object o1, Object o2) {
        if (o1 == o2) {
            return 0;
        }
        if (o1 == null) {
            return -1;
        }

        if (o2 == null) {
            return 1;
        }

        byte[] value = convert(o1);
        byte[] targetValue = convert(o2);
        int notZeroOffset = 0;
        int targetNotZeroOffset = 0;
        for (notZeroOffset = 0; notZeroOffset < value.length; notZeroOffset++) {
            if (value[notZeroOffset] != 0) {
                break;
            }
        }

        for (targetNotZeroOffset = 0; targetNotZeroOffset < targetValue.length; targetNotZeroOffset++) {
            if (targetValue[targetNotZeroOffset] != 0) {
                break;
            }
        }

        int actualLength = value.length - notZeroOffset;
        int actualTargetLength = targetValue.length - targetNotZeroOffset;

        if (actualLength > actualTargetLength) {
            return 1;
        } else if (actualLength < actualTargetLength) {
            return -1;
        } else {
            int index = notZeroOffset;
            int targetIndex = targetNotZeroOffset;
            while (true) {
                if (index >= value.length || targetIndex >= targetValue.length) {
                    break;
                }
                short shortValue = (short) (value[index] & 0xff);
                short shortTargetValue = (short) (targetValue[targetIndex] & 0xff);
                boolean re = (shortValue == shortTargetValue);
                if (re) {
                    index++;
                    targetIndex++;
                    continue;
                } else {
                    return shortValue > shortTargetValue ? 1 : -1;
                }

            }
            return 0;
        }
    }

    public int getSqlType() {
        return java.sql.Types.BINARY;
    }

}
