package drds.plus.sql_process.type;

import drds.tools.ShouldNeverHappenException;

import java.math.BigDecimal;
import java.math.BigInteger;


public class Decoder {


    public static Byte decodeByteObject(byte[] bytes, int offset) throws CorruptEncodingException {
        try {
            int b = bytes[offset];
            if (b == Constants.NULL_BYTE_HIGH || b == Constants.NULL_BYTE_LOW) {
                return null;
            }
            return decodeByte(bytes, offset + 1);
        } catch (IndexOutOfBoundsException e) {
            throw new CorruptEncodingException(null, e);
        }
    }

    public static byte decodeByte(byte[] bytes, int offset) throws CorruptEncodingException {
        try {
            return (byte) (bytes[offset] ^ 0x80);
        } catch (IndexOutOfBoundsException e) {
            throw new CorruptEncodingException(null, e);
        }
    }

    public static Short decodeShortObject(byte[] bytes, int offset) throws CorruptEncodingException {
        try {
            int b = bytes[offset];
            if (b == Constants.NULL_BYTE_HIGH || b == Constants.NULL_BYTE_LOW) {
                return null;
            }
            return decodeShort(bytes, offset + 1);
        } catch (IndexOutOfBoundsException e) {
            throw new CorruptEncodingException(null, e);
        }
    }

    public static short decodeShort(byte[] bytes, int offset) throws CorruptEncodingException {
        try {
            return (short) (((bytes[offset] << 8) | (bytes[offset + 1] & 0xff)) ^ 0x8000);
        } catch (IndexOutOfBoundsException e) {
            throw new CorruptEncodingException(null, e);
        }
    }

    public static Integer decodeIntegerObject(byte[] bytes, int offset) throws CorruptEncodingException {
        try {
            int b = bytes[offset];
            if (b == Constants.NULL_BYTE_HIGH || b == Constants.NULL_BYTE_LOW) {
                return null;
            }
            return decodeInt(bytes, offset + 1);
        } catch (IndexOutOfBoundsException e) {
            throw new CorruptEncodingException(null, e);
        }
    }

    public static int decodeInt(byte[] bytes, int offset) throws CorruptEncodingException {
        try {
            int value = (bytes[offset] << 24) | ((bytes[offset + 1] & 0xff) << 16) | ((bytes[offset + 2] & 0xff) << 8) | (bytes[offset + 3] & 0xff);
            return value ^ 0x80000000;
        } catch (IndexOutOfBoundsException e) {
            throw new CorruptEncodingException(null, e);
        }
    }

    public static Long decodeLongObject(byte[] bytes, int offset) throws CorruptEncodingException {
        try {
            int b = bytes[offset];
            if (b == Constants.NULL_BYTE_HIGH || b == Constants.NULL_BYTE_LOW) {
                return null;
            }
            return decodeLong(bytes, offset + 1);
        } catch (IndexOutOfBoundsException e) {
            throw new CorruptEncodingException(null, e);
        }
    }

    public static long decodeLong(byte[] bytes, int offset) throws CorruptEncodingException {
        try {
            return (((long) (((bytes[offset]) << 24) | ((bytes[offset + 1] & 0xff) << 16) | ((bytes[offset + 2] & 0xff) << 8) | ((bytes[offset + 3] & 0xff))) ^ 0x80000000) << 32) | (((((bytes[offset + 4]) << 24) | ((bytes[offset + 5] & 0xff) << 16) | ((bytes[offset + 6] & 0xff) << 8) | ((bytes[offset + 7] & 0xff))) & 0xffffffffL));
        } catch (IndexOutOfBoundsException e) {
            throw new CorruptEncodingException(null, e);
        }
    }

    public static Float decodeFloatObject(byte[] bytes, int offset) throws CorruptEncodingException {
        int b = bytes[offset];
        if (b == Constants.NULL_BYTE_HIGH || b == Constants.NULL_BYTE_LOW) {
            return null;
        }
        int bits = decodeFloatBits(bytes, offset++);
        return Float.intBitsToFloat(bits);
    }

    protected static int decodeFloatBits(byte[] bytes, int offset) throws CorruptEncodingException {
        try {
            return (bytes[offset] << 24) | ((bytes[offset + 1] & 0xff) << 16) | ((bytes[offset + 2] & 0xff) << 8) | (bytes[offset + 3] & 0xff);
        } catch (IndexOutOfBoundsException e) {
            throw new CorruptEncodingException(null, e);
        }
    }

    public static Double decodeDoubleObject(byte[] bytes, int offset) throws CorruptEncodingException {
        int b = bytes[offset];
        if (b == Constants.NULL_BYTE_HIGH || b == Constants.NULL_BYTE_LOW) {
            return null;
        }
        long bits = decodeDoubleBits(bytes, offset++);

        return Double.longBitsToDouble(bits);
    }

    protected static long decodeDoubleBits(byte[] bytes, int offset) throws CorruptEncodingException {
        try {
            return (((long) (((bytes[offset]) << 24) | ((bytes[offset + 1] & 0xff) << 16) | ((bytes[offset + 2] & 0xff) << 8) | ((bytes[offset + 3] & 0xff)))) << 32) | (((((bytes[offset + 4]) << 24) | ((bytes[offset + 5] & 0xff) << 16) | ((bytes[offset + 6] & 0xff) << 8) | ((bytes[offset + 7] & 0xff))) & 0xffffffffL));
        } catch (IndexOutOfBoundsException e) {
            throw new CorruptEncodingException(null, e);
        }
    }

    public static int decodeBigDecimalObject(byte[] bytes, int offset, BigDecimal[] values) throws CorruptEncodingException {
        try {
            int b = bytes[offset++];
            int length = getLength(bytes, offset, b);
            offset += length;
            byte[] newBytes = new byte[length];
            System.arraycopy(bytes, offset, newBytes, 0, length);
            values[0] = new BigDecimal(new String(newBytes));
            return offset + length;

        } catch (IndexOutOfBoundsException e) {
            throw new CorruptEncodingException(null, e);
        }
    }

    private static int getLength(byte[] bytes, int offset, int b) {
        if (b == 1) {
            return bytes[offset++];
        } else if (b == 2) {

        } else if (b == 3) {

        } else if (b == 4) {

        } else if (b == 4) {

        } else if (b == 5) {

        } else {
            throw new ShouldNeverHappenException();
        }
        return 0;
    }

    public static int decodeBigIntegerObject(byte[] bytes, int offset, BigInteger[] values) throws CorruptEncodingException {
        try {
            int b = bytes[offset++];
            System.out.println(b + "---");
            int length = 0;
            if (b == 1) {
                length = bytes[offset++];
            }
            byte[] newBytes = new byte[length];
            System.arraycopy(bytes, offset, newBytes, 0, length);
            values[0] = new BigInteger(new String(newBytes));
            return offset + length;

        } catch (IndexOutOfBoundsException e) {
            throw new CorruptEncodingException(null, e);
        }
    }

    public static int decodeStringObject(byte[] bytes, int offset, String[] values) throws CorruptEncodingException {
        try {
            int b = bytes[offset++];
            System.out.println(b + "---");
            int length = 0;
            if (b == 1) {
                length = bytes[offset++];
            }
            byte[] newBytes = new byte[length];
            System.arraycopy(bytes, offset, newBytes, 0, length);
            values[0] = new String(newBytes);
            return offset + length;
        } catch (IndexOutOfBoundsException e) {
            throw new CorruptEncodingException(null, e);
        }
    }

    ////////////////////


    /**
     * Decodes a Boolean object from exactly 1 byte.
     *
     * @param bytes  source of encoded bytes
     * @param offset offset into source array
     * @return Boolean object or null
     */
    public static Boolean decodeBooleanObj(byte[] bytes, int offset) throws CorruptEncodingException {
        try {
            switch (bytes[offset]) {
                case Constants.NULL_BYTE_LOW:
                case Constants.NULL_BYTE_HIGH:
                    return null;
                case (byte) 128:
                    return Boolean.TRUE;
                default:
                    return Boolean.FALSE;
            }
        } catch (IndexOutOfBoundsException e) {
            throw new CorruptEncodingException(null, e);
        }
    }


    /**
     * Decodes a BigInteger.
     *
     * @param bytes  source of encoded data
     * @param offset offset into encoded data
     * @param values decoded BigInteger is stored in element 0, which may be null
     * @return amount of bytes read from source
     * @throws CorruptEncodingException if source data is corrupt
     * @since 1.2
     */
    public static int decode(byte[] bytes, int offset, BigInteger[] values) throws CorruptEncodingException {
        byte[][] bytesRef = new byte[1][];
        int amt = decode(bytes, offset, bytesRef);
        values[0] = (bytesRef[0] == null) ? null : new BigInteger(bytesRef[0]);
        return amt;
    }

    /**
     * Decodes a BigDecimal.
     *
     * @param bytes  source of encoded data
     * @param offset offset into encoded data
     * @param values decoded BigDecimal is stored in element 0, which may be null
     * @return amount of bytes read from source
     * @throws CorruptEncodingException if source data is corrupt
     * @since 1.2
     */
    public static int decode(byte[] bytes, int offset, BigDecimal[] values) throws CorruptEncodingException {
        try {
            final int originalOffset = offset;

            int b = bytes[offset++] & 0xff;
            if (b >= 0xf8) {
                values[0] = null;
                return 1;
            }

            int scale;
            if (b <= 0x7f) {
                scale = b;
            } else if (b <= 0xbf) {
                scale = ((b & 0x3f) << 8) | (bytes[offset++] & 0xff);
            } else if (b <= 0xdf) {
                scale = ((b & 0x1f) << 16) | ((bytes[offset++] & 0xff) << 8) | (bytes[offset++] & 0xff);
            } else if (b <= 0xef) {
                scale = ((b & 0x0f) << 24) | ((bytes[offset++] & 0xff) << 16) | ((bytes[offset++] & 0xff) << 8) | (bytes[offset++] & 0xff);
            } else {
                scale = ((bytes[offset++] & 0xff) << 24) | ((bytes[offset++] & 0xff) << 16) | ((bytes[offset++] & 0xff) << 8) | (bytes[offset++] & 0xff);
            }

            if ((scale & 1) != 0) {
                scale = (~(scale >> 1)) | (1 << 31);
            } else {
                scale >>>= 1;
            }

            BigInteger[] unscaledRef = new BigInteger[1];
            int amt = decode(bytes, offset, unscaledRef);

            values[0] = new BigDecimal(unscaledRef[0], scale);

            return (offset + amt) - originalOffset;
        } catch (IndexOutOfBoundsException e) {
            throw new CorruptEncodingException(null, e);
        }
    }

    /**
     * Decodes the given byte array.
     *
     * @param bytes  source of encoded data
     * @param offset offset into encoded data
     * @param values decoded byte array is stored in element 0, which may be null
     * @return amount of bytes read from source
     * @throws CorruptEncodingException if source data is corrupt
     */
    public static int decode(byte[] bytes, int offset, byte[][] values) throws CorruptEncodingException {
        try {
            final int originalOffset = offset;

            int b = bytes[offset++] & 0xff;
            if (b >= 0xf8) {
                values[0] = null;
                return 1;
            }

            int valueLength;
            if (b <= 0x7f) {
                valueLength = b;
            } else if (b <= 0xbf) {
                valueLength = ((b & 0x3f) << 8) | (bytes[offset++] & 0xff);
            } else if (b <= 0xdf) {
                valueLength = ((b & 0x1f) << 16) | ((bytes[offset++] & 0xff) << 8) | (bytes[offset++] & 0xff);
            } else if (b <= 0xef) {
                valueLength = ((b & 0x0f) << 24) | ((bytes[offset++] & 0xff) << 16) | ((bytes[offset++] & 0xff) << 8) | (bytes[offset++] & 0xff);
            } else {
                valueLength = ((bytes[offset++] & 0xff) << 24) | ((bytes[offset++] & 0xff) << 16) | ((bytes[offset++] & 0xff) << 8) | (bytes[offset++] & 0xff);
            }

            if (valueLength == 0) {
                values[0] = Constants.EMPTY_BYTE_ARRAY;
            } else {
                byte[] value = new byte[valueLength];
                System.arraycopy(bytes, offset, value, 0, valueLength);
                values[0] = value;
            }

            return offset - originalOffset + valueLength;
        } catch (IndexOutOfBoundsException e) {
            throw new CorruptEncodingException(null, e);
        }
    }


}
