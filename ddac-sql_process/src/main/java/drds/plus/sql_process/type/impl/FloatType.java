package drds.plus.sql_process.type.impl;

import drds.plus.sql_process.type.*;

public class FloatType extends CommonType<Float> {

    private final Calculator calculator = new AbstractCalculator() {

        public Object doAdd(Object v1, Object v2) {
            Float i1 = convert(v1);
            Float i2 = convert(v2);
            return i1 + i2;
        }

        public Object doSub(Object v1, Object v2) {
            Float i1 = convert(v1);
            Float i2 = convert(v2);
            return i1 - i2;
        }

        public Object doMultiply(Object v1, Object v2) {
            Float i1 = convert(v1);
            Float i2 = convert(v2);
            return i1 * i2;
        }

        public Object doDivide(Object v1, Object v2) {
            Float i1 = convert(v1);
            Float i2 = convert(v2);

            if (i2.equals(0.0f)) {
                return null;
            }

            return i1 / i2;
        }

        public Object doMod(Object v1, Object v2) {
            Float i1 = convert(v1);
            Float i2 = convert(v2);

            if (i2.equals(0.0f)) {
                return null;
            }
            return i1 % i2;
        }

    };

    public int encode(Object value, byte[] bytes, int offset) {
        Encoder.encode(this.convert(value), bytes, offset);
        return getLength(null);
    }

    public int getLength(Object value) {
        if (value == null) {
            return 1;
        } else {
            return 4;
        }
    }

    public DecodeResult decode(byte[] bytes, int offset) {
        Float v = Decoder.decodeFloatObject(bytes, offset);
        return new DecodeResult(v, getLength(v));
    }

    public Float incr(Object value) {
        return this.convert(value) + 0.000001f;
    }

    public Float decr(Object value) {
        return this.convert(value) - 0.000001f;
    }

    public Float getMaxValue() {
        return Float.MAX_VALUE;
    }

    public Float getMinValue() {
        return Float.MIN_VALUE;
    }

    public Calculator getCalculator() {
        return calculator;
    }

    public int getSqlType() {
        return java.sql.Types.FLOAT;
    }
}
