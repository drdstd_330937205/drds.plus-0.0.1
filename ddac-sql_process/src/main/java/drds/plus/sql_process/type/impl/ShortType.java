package drds.plus.sql_process.type.impl;

import drds.plus.sql_process.type.*;

public class ShortType extends CommonType<Short> {

    private final Calculator calculator = new AbstractCalculator() {

        public Object doAdd(Object v1, Object v2) {
            Short i1 = convert(v1);
            Short i2 = convert(v2);
            return i1 + i2;
        }

        public Object doSub(Object v1, Object v2) {
            Short i1 = convert(v1);
            Short i2 = convert(v2);
            return i1 - i2;
        }

        public Object doMultiply(Object v1, Object v2) {
            Short i1 = convert(v1);
            Short i2 = convert(v2);
            return i1 * i2;
        }

        public Object doDivide(Object v1, Object v2) {
            Short i1 = convert(v1);
            Short i2 = convert(v2);

            if (i2 == 0) {
                return null;
            }

            return i1 / i2;
        }

        public Object doMod(Object v1, Object v2) {
            Short i1 = convert(v1);
            Short i2 = convert(v2);

            if (i2 == 0) {
                return null;
            }
            return i1 % i2;
        }


    };

    public int encode(Object value, byte[] bytes, int offset) {
        return Encoder.encodeShort(this.convert(value), bytes, offset);
    }

    public int getLength(Object value) {
        if (value == null) {
            return 1;
        } else {
            return 3;
        }
    }

    public DecodeResult decode(byte[] bytes, int offset) {
        Short v = Decoder.decodeShortObject(bytes, offset);
        return new DecodeResult(v, getLength(v));
    }

    public Short incr(Object value) {
        return (short) (convert(value) + 1);
    }

    public Short decr(Object value) {
        return (short) (convert(value) - 1);
    }

    public Short getMaxValue() {
        return Short.MAX_VALUE;
    }

    public Short getMinValue() {
        return Short.MIN_VALUE;
    }

    public Calculator getCalculator() {
        return calculator;
    }

    public int getSqlType() {
        return java.sql.Types.SMALLINT;
    }
}
