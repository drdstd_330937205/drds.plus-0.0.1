package drds.plus.sql_process.type.impl;

import java.sql.Blob;

/**
 * {@linkplain Blob}类型，使用BytesType来替代
 */
public class BlobType extends BytesType {

    public int getSqlType() {
        return java.sql.Types.BLOB;
    }
}
