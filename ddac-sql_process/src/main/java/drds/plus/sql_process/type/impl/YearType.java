package drds.plus.sql_process.type.impl;

/**
 * 针对mysql中特殊的year type
 */
public class YearType extends LongType {

    public int getSqlType() {
        return YEAR_SQL_TYPE;
    }

}
