package drds.plus.sql_process.type.impl;

import java.sql.Clob;

/**
 * {@linkplain Clob}类型，使用StringType来代替
 */
public class ClobType extends StringType {

    public int getSqlType() {
        return java.sql.Types.CLOB;
    }
}
