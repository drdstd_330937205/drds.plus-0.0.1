package drds.plus.sql_process.type;

import drds.plus.common.model.RowValues;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface ResultGetter {

    Object get(ResultSet resultSet, int index) throws SQLException;

    Object get(RowValues rowValues, int index);
}
