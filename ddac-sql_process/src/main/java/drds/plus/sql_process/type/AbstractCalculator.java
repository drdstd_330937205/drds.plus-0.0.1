package drds.plus.sql_process.type;

public abstract class AbstractCalculator implements Calculator {

    public abstract Object doAdd(Object v1, Object v2);

    public abstract Object doSub(Object v1, Object v2);

    public abstract Object doMultiply(Object v1, Object v2);

    public abstract Object doDivide(Object v1, Object v2);

    public abstract Object doMod(Object v1, Object v2);


    public Object add(Object v1, Object v2) {
        if (v1 == null || v2 == null)
            return null;

        return this.doAdd(v1, v2);
    }

    public Object sub(Object v1, Object v2) {
        if (v1 == null || v2 == null)
            return null;

        return this.doSub(v1, v2);
    }

    public Object multiply(Object v1, Object v2) {
        if (v1 == null || v2 == null)
            return null;

        return this.doMultiply(v1, v2);
    }

    public Object divide(Object v1, Object v2) {
        if (v1 == null || v2 == null)
            return null;

        return this.doDivide(v1, v2);
    }

    public Object mod(Object v1, Object v2) {
        if (v1 == null || v2 == null)
            return null;

        return this.doMod(v1, v2);
    }


}
