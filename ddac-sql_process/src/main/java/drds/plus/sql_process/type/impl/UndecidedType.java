package drds.plus.sql_process.type.impl;

/**
 * 未决类型
 */
public class UndecidedType extends StringType {

    public int getSqlType() {
        return UNDECIDED_SQL_TYPE;
    }
}
