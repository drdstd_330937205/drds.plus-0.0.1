package drds.plus.sql_process.type.impl;

/**
 * 标准的Bit类型实现
 */
public class BitType extends IntegerType {

    public int getSqlType() {
        return java.sql.Types.BIT;
    }
}
