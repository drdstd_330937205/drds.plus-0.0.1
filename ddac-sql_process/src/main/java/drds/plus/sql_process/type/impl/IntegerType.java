package drds.plus.sql_process.type.impl;

import drds.plus.sql_process.type.*;

/**
 * int/Integer类型
 */
public class IntegerType extends CommonType<Integer> {

    private final Calculator calculator = new AbstractCalculator() {

        public Object doAdd(Object v1, Object v2) {
            Integer i1 = convert(v1);
            Integer i2 = convert(v2);
            return i1 + i2;
        }

        public Object doSub(Object v1, Object v2) {
            Integer i1 = convert(v1);
            Integer i2 = convert(v2);
            return i1 - i2;
        }

        public Object doMultiply(Object v1, Object v2) {
            Integer i1 = convert(v1);
            Integer i2 = convert(v2);
            return i1 * i2;
        }

        public Object doDivide(Object v1, Object v2) {
            Integer i1 = convert(v1);
            Integer i2 = convert(v2);

            if (i2 == 0) {
                return null;
            }
            return i1 / i2;
        }

        public Object doMod(Object v1, Object v2) {
            Integer i1 = convert(v1);
            Integer i2 = convert(v2);

            if (i2 == 0) {
                return null;
            }

            return i1 % i2;
        }


    };

    public int encode(Object value, byte[] bytes, int offset) {
        return Encoder.encode(this.convert(value), bytes, offset);
    }

    public int getLength(Object value) {
        if (value == null) {
            return 1;
        } else {
            return 5;
        }
    }

    public DecodeResult decode(byte[] bytes, int offset) {
        Integer v = Decoder.decodeIntegerObject(bytes, offset);
        return new DecodeResult(v, getLength(v));
    }

    public Integer incr(Object value) {
        return convert(value) + 1;
    }

    public Integer decr(Object value) {
        return convert(value) - 1;
    }

    public Integer getMaxValue() {
        return Integer.MAX_VALUE;
    }

    public Integer getMinValue() {
        return Integer.MIN_VALUE;
    }

    public Calculator getCalculator() {
        return calculator;
    }

    public int getSqlType() {
        return java.sql.Types.INTEGER;
    }
}
