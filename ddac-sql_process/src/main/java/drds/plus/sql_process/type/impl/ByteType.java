package drds.plus.sql_process.type.impl;

import drds.plus.sql_process.type.*;

/**
 * {@link Byte} 类型
 */
public class ByteType extends CommonType<Byte> {

    private final Calculator calculator = new AbstractCalculator() {

        public Object doAdd(Object v1, Object v2) {
            Byte i1 = convert(v1);
            Byte i2 = convert(v2);
            return i1 + i2;
        }

        public Object doSub(Object v1, Object v2) {
            Byte i1 = convert(v1);
            Byte i2 = convert(v2);
            return i1 - i2;
        }

        public Object doMultiply(Object v1, Object v2) {
            Byte i1 = convert(v1);
            Byte i2 = convert(v2);
            return i1 * i2;
        }

        public Object doDivide(Object v1, Object v2) {
            Byte i1 = convert(v1);
            Byte i2 = convert(v2);

            if (i2 == 0) {
                return null;
            }
            return i1 / i2;
        }

        public Object doMod(Object v1, Object v2) {
            Byte i1 = convert(v1);
            Byte i2 = convert(v2);

            if (i2 == 0) {
                return null;
            }

            return i1 % i2;
        }


    };

    public int encode(Object value, byte[] bytes, int offset) {
        byte b = this.convert(value);
        return Encoder.encodeByte(b, bytes, offset);
    }

    public int getLength(Object value) {
        if (value == null) {
            return 1;
        } else {
            return 2;
        }
    }

    public DecodeResult decode(byte[] bytes, int offset) {
        Byte v = Decoder.decodeByteObject(bytes, offset);
        return new DecodeResult(v, getLength(v));
    }

    public Byte incr(Object value) {
        return Byte.valueOf(((Integer) (this.convert(value).intValue() + 1)).byteValue());
    }

    public Byte decr(Object value) {
        return Byte.valueOf(((Integer) (this.convert(value).intValue() - 1)).byteValue());
    }

    public Byte getMaxValue() {
        return Byte.MAX_VALUE;
    }

    public Byte getMinValue() {
        return Byte.MIN_VALUE;
    }

    public Calculator getCalculator() {
        return calculator;
    }

    public int getSqlType() {
        return java.sql.Types.TINYINT;
    }
}
