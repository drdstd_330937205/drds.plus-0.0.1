package drds.plus.sql_process.type;

public class DecodeResult {

    public Object value;
    public int length;

    public DecodeResult(Object value, int length) {
        super();
        this.value = value;
        this.length = length;
    }

}
