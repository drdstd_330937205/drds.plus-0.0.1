package drds.plus.sql_process.type;

public interface Calculator {

    Object add(Object v1, Object v2);

    Object sub(Object v1, Object v2);

    Object multiply(Object v1, Object v2);

    Object divide(Object v1, Object v2);

    Object mod(Object v1, Object v2);

}
