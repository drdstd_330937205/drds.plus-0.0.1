package drds.plus.sql_process.type.impl;

import drds.plus.sql_process.type.*;

/**
 * {@linkplain Long}类型
 */
public class LongType extends CommonType<Long> {

    private final Calculator calculator = new AbstractCalculator() {

        public Object doAdd(Object v1, Object v2) {
            Long i1 = convert(v1);
            Long i2 = convert(v2);
            return i1 + i2;
        }

        public Object doSub(Object v1, Object v2) {
            Long i1 = convert(v1);
            Long i2 = convert(v2);
            return i1 - i2;
        }

        public Object doMultiply(Object v1, Object v2) {
            Long i1 = convert(v1);
            Long i2 = convert(v2);
            return i1 * i2;
        }

        public Object doDivide(Object v1, Object v2) {
            Long i1 = convert(v1);
            Long i2 = convert(v2);
            if (i2 == 0L) {
                return null;
            }
            return i1 / i2;
        }

        public Object doMod(Object v1, Object v2) {
            Long i1 = convert(v1);
            Long i2 = convert(v2);
            if (i2 == 0L) {
                return null;
            }
            return i1 % i2;
        }


    };

    public int encode(Object value, byte[] bytes, int offset) {
        return Encoder.encode(this.convert(value), bytes, offset);
    }

    public int getLength(Object value) {
        if (value == null) {
            return 1;
        } else {
            return 9;
        }
    }

    public DecodeResult decode(byte[] bytes, int offset) {
        Long v = Decoder.decodeLongObject(bytes, offset);
        return new DecodeResult(v, getLength(v));
    }

    public Long incr(Object value) {
        return convert(value) + 1;
    }

    public Long decr(Object value) {
        return convert(value) - 1;
    }

    public Long getMaxValue() {
        return Long.MAX_VALUE;
    }

    public Long getMinValue() {
        return Long.MIN_VALUE;
    }

    public Calculator getCalculator() {
        return calculator;
    }

    public int getSqlType() {
        return java.sql.Types.BIGINT;
    }
}
