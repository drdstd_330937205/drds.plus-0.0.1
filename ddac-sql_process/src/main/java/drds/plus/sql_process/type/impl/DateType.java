package drds.plus.sql_process.type.impl;

import drds.plus.common.model.RowValues;
import drds.plus.common.utils.convertor.Convertor;
import drds.plus.sql_process.type.*;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;

/**
 * {@link Date}类型
 */
public class DateType extends AbstractType<Date> {

    private static final Date maxDate = Date.valueOf("9999-12-31");
    private static final Date minDate = Date.valueOf("1900-01-01");
    private final Calculator calculator = new AbstractCalculator() {

        public Object doAdd(Object v1, Object v2) {
            Calendar cal = Calendar.getInstance();
            if (v1 instanceof drds.plus.sql_process.type.impl.IntervalType) {
                Date i2 = convert(v2);
                cal.setTime(i2);
                ((IntervalType) v1).process(cal, 1);
            } else if (v2 instanceof IntervalType) {
                Date i1 = convert(v1);
                cal.setTime(i1);
                ((IntervalType) v2).process(cal, 1);
            } else {
                throw new UnsupportedOperationException("时间类型不支持算术符操作");
            }

            return convert(cal.getTime());
        }

        public Object doSub(Object v1, Object v2) {
            Calendar cal = Calendar.getInstance();
            if (v1 instanceof IntervalType) {
                Date i2 = convert(v2);
                cal.setTime(i2);
                ((IntervalType) v1).process(cal, -1);
            } else if (v2 instanceof IntervalType) {
                Date i1 = convert(v1);
                cal.setTime(i1);
                ((IntervalType) v2).process(cal, -1);
            } else {
                throw new UnsupportedOperationException("时间类型不支持算术符操作");
            }

            return convert(cal.getTime());
        }

        public Object doMultiply(Object v1, Object v2) {
            throw new UnsupportedOperationException("时间类型不支持算术符操作");
        }

        public Object doDivide(Object v1, Object v2) {
            throw new UnsupportedOperationException("时间类型不支持算术符操作");
        }

        public Object doMod(Object v1, Object v2) {
            throw new UnsupportedOperationException("时间类型不支持算术符操作");
        }

    };
    private Convertor longToDate = null;

    public DateType() {
        longToDate = this.getConvertor(Long.class);
    }

    public ResultGetter getResultGetter() {
        return new ResultGetter() {

            public Object get(ResultSet resultSet, int index) throws SQLException {
                return resultSet.getDate(index);
            }

            public Object get(RowValues rowValues, int index) {
                Object val = rowValues.getObject(index);
                return convert(val);
            }
        };
    }

    public int encode(Object value, byte[] bytes, int offset) {
        return Encoder.encode(LongType.convert(value), bytes, offset);
    }

    public int getLength(Object value) {
        if (value == null) {
            return 1;
        } else {
            return 9;
        }
    }

    public DecodeResult decode(byte[] bytes, int offset) {
        Long v = Decoder.decodeLongObject(bytes, offset);
        if (v == null) {
            return new DecodeResult(null, getLength(v));
        } else {
            Date date = (Date) longToDate.convert(v, getValueClass());
            return new DecodeResult(date, getLength(v));
        }
    }

    public Date incr(Object value) {
        return new Date(((Date) value).getTime() + 1l);
    }

    public Date decr(Object value) {
        return new Date(((Date) value).getTime() - 1l);
    }

    public Date getMaxValue() {
        return maxDate;
    }

    public Date getMinValue() {
        return minDate;
    }

    public int compare(Object o1, Object o2) {
        if (o1 == o2) {
            return 0;
        }
        if (o1 == null) {
            return -1;
        }

        if (o2 == null) {
            return 1;
        }

        Date d1 = convert(o1);
        Date d2 = convert(o2);

        return d1.compareTo(d2);
    }

    public Calculator getCalculator() {
        return calculator;
    }

    public int getSqlType() {
        return java.sql.Types.DATE;
    }
}
