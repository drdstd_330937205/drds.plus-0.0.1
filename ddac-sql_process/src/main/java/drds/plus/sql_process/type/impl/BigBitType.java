package drds.plus.sql_process.type.impl;

import drds.plus.common.utils.convertor.ConvertorHelper;

/**
 * 标准的Bit类型实现
 */
public class BigBitType extends BigDecimalType {

    public BigBitType() {
        super();
        convertor = ConvertorHelper.bitBytesToBigDecimal;
    }

    public int getSqlType() {
        return java.sql.Types.DECIMAL;
    }

}
