package drds.plus.sql_process.optimizer.cost_esitimater.statistics;

import lombok.Getter;
import lombok.Setter;

import java.util.LinkedList;
import java.util.List;

/**
 * 一个表的所有索引的统计数据
 */
public class TableIndexStatistics {
    @Setter
    @Getter
    // 表名
    private String tableName;
    @Setter
    @Getter
    // 所有索引采集的元数据
    private List<TableIndexStatisticsItem> tableIndexStatisticsItemList = new LinkedList<TableIndexStatisticsItem>();

    public void add(TableIndexStatisticsItem tableIndexStatisticsItem) {
        if (tableIndexStatisticsItem == null) {
            return;
        }
        tableIndexStatisticsItemList.add(tableIndexStatisticsItem);
    }

    public TableIndexStatisticsItem getTableIndexStatisticsItem(String indexName) {
        for (TableIndexStatisticsItem tableIndexStatisticsItem : tableIndexStatisticsItemList) {
            if (indexName.equals(tableIndexStatisticsItem.getIndexName())) {
                return tableIndexStatisticsItem;
            }
        }

        return null;
    }

}
