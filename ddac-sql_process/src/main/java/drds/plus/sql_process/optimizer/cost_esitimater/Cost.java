package drds.plus.sql_process.optimizer.cost_esitimater;

/**
 * 估算数据获取成本
 */
public class Cost implements Comparable {
    /**
     * keyFilter过滤后i的行数
     */
    long keyFilteredCount;
    /**
     * keyFilter与valueFilter共同过滤后的行数
     */
    private long keyFilteredAndValueFilteredCount = 1;
    //
    private long diskIoCost = 0;
    private boolean dependentOnOtherQuery = false;
    private long networkTransferCount; // 网络成本

    public long getKeyFilteredAndValueFilteredCount() {
        return this.keyFilteredAndValueFilteredCount;
    }

    public void setKeyFilteredAndValueFilteredCount(long count) {
        this.keyFilteredAndValueFilteredCount = count;

    }

    public long getDiskIoCost() {
        return this.diskIoCost;
    }

    public void setDiskIoCost(long diskIoCost) {
        this.diskIoCost = diskIoCost;

    }

    public boolean isDependentOnOtherQuery() {
        return dependentOnOtherQuery;
    }

    public void setDependentOnOtherQuery(boolean dependentOnOtherQuery) {
        this.dependentOnOtherQuery = dependentOnOtherQuery;

    }

    public long getNetworkTransferCount() {
        return networkTransferCount;
    }

    public void setNetworkTransferCount(long nc) {
        this.networkTransferCount = nc;

    }

    public long getKeyFilteredCount() {
        return this.keyFilteredCount;
    }

    public void setKeyFilteredCount(long count) {
        this.keyFilteredCount = count;

    }

    public int compareTo(Object arg) {
        throw new UnsupportedOperationException();
    }

}
