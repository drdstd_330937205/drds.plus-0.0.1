package drds.plus.sql_process.optimizer.cost_esitimater.statistics;

import drds.plus.common.lifecycle.Lifecycle;

/**
 * 表上的统计信息
 */
public interface StatisticsManager extends Lifecycle {
    TableStatistics getTableStatistics(String tableName);

    TableIndexStatisticsItem getIndexStatistics(String indexName);
}
