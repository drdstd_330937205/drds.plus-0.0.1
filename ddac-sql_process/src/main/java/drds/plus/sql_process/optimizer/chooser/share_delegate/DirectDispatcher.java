package drds.plus.sql_process.optimizer.chooser.share_delegate;

import net.sf.cglib.proxy.Dispatcher;

class DirectDispatcher implements Dispatcher {

    private ShareDelegate shareDelegate;

    public DirectDispatcher(ShareDelegate shareDelegate) {
        this.shareDelegate = shareDelegate;
    }

    public Object loadObject() throws Exception {
        return shareDelegate.delegateObject;
    }

}
