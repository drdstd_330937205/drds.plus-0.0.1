package drds.plus.sql_process.optimizer.chooser;

public enum FilterType {
    /**
     * 如果有索引，就是索引上的keyFilter，如果没索引，就是主表上的KeyFilter
     */
    IndexQueryKeyFilter,
    /**
     * 在索引表上的ValueFilter
     */
    IndexQueryValueFilter,
    /**
     * 在主表上的ValueFilter
     */
    ResultFilter

}
