package drds.plus.sql_process.optimizer.cost_esitimater;

import drds.plus.sql_process.abstract_syntax_tree.node.query.Query;

public interface CostEstimater {

    /**
     * 根据查询树，预估一下执行代价，如果统计状态服务不可用，抛出异常
     */
    Cost estimate(Query query);
}
