package drds.plus.sql_process.optimizer.cost_esitimater.statistics.parse;

import com.google.common.collect.Lists;
import drds.plus.common.utils.XmlHelper;
import drds.plus.sql_process.optimizer.cost_esitimater.statistics.TableIndexStatistics;
import drds.plus.sql_process.optimizer.cost_esitimater.statistics.TableIndexStatisticsItem;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;

/**
 * 解析matrix配置
 */
public class TableIndexStatisticsParser {

    private static final String XSD_SCHEMA = "META-INF/stat.xsd";

    public static List<TableIndexStatistics> parse(String data) {
        InputStream is = new ByteArrayInputStream(data.getBytes());
        return parse(is);
    }

    public static List<TableIndexStatistics> parse(InputStream in) {
        Document doc = XmlHelper.createDocument(in, null);
        Element root = doc.getDocumentElement();
        List<TableIndexStatistics> stats = Lists.newArrayList();
        NodeList list = root.getElementsByTagName("tableIndexStat");
        for (int i = 0; i < list.getLength(); i++) {
            Node item = list.item(i);
            stats.add(parseTableIndexStat(item));
        }

        return stats;
    }

    private static TableIndexStatistics parseTableIndexStat(Node node) {
        NodeList list = node.getChildNodes();
        TableIndexStatistics stat = new TableIndexStatistics();
        for (int i = 0; i < list.getLength(); i++) {
            Node item = list.item(i);
            if ("tableName".equals(item.getNodeName())) {
                stat.setTableName(item.getFirstChild().getNodeValue().toLowerCase());
            } else if ("indexStats".equals(item.getNodeName())) {
                NodeList indexChilds = item.getChildNodes();
                for (int j = 0; j < indexChilds.getLength(); j++) {
                    Node indexItem = indexChilds.item(j);
                    if ("indexStat".equals(indexItem.getNodeName())) {
                        stat.add(parseKVIndexStat(indexItem));
                    }
                }
            }
        }

        return stat;
    }

    private static TableIndexStatisticsItem parseKVIndexStat(Node node) {
        NodeList list = node.getChildNodes();
        TableIndexStatisticsItem stat = new TableIndexStatisticsItem();
        for (int i = 0; i < list.getLength(); i++) {
            Node item = list.item(i);
            if ("indexName".equals(item.getNodeName())) {
                stat.setIndexName(item.getFirstChild().getNodeValue().toLowerCase());
            } else if ("indexType".equals(item.getNodeName())) {
                stat.setIndexType(Integer.valueOf(item.getFirstChild().getNodeValue()));
            } else if ("distinctKeys".equals(item.getNodeName())) {
                stat.setDistinctKeyCount(Integer.valueOf(item.getFirstChild().getNodeValue()));
            } else if ("numRows".equals(item.getNodeName())) {
                stat.setNumRows(Integer.valueOf(item.getFirstChild().getNodeValue()));
            } else if ("factor".equals(item.getNodeName())) {
                stat.setFactor(Double.valueOf(item.getFirstChild().getNodeValue()));
            }
        }

        return stat;
    }
}
