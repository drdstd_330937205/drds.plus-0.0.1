package drds.plus.sql_process.optimizer.cost_esitimater.statistics;

public class TableStatistics {

    // 表名
    private String tableName;
    // 表的全部行数
    private long rowCount;

    public long getRowCount() {
        return rowCount;
    }

    public void setRowCount(long rowCount) {
        this.rowCount = rowCount;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

}
