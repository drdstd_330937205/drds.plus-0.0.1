package drds.plus.sql_process.optimizer.chooser;

import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.BooleanFilter;

import java.util.List;

public class JoinInfo {
    boolean theSameJoinDataNodeIdOrHasBroadcastLable = false; // 成功还是失败
    String joinDataNodeId;
    boolean hasBroadcastLable = false;
    List<BooleanFilter> joinFilterList;

}
