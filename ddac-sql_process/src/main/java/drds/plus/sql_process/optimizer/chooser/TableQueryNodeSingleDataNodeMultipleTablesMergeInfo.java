package drds.plus.sql_process.optimizer.chooser;

import drds.plus.sql_process.abstract_syntax_tree.node.query.Query;

import java.util.ArrayList;
import java.util.List;

class TableQueryNodeSingleDataNodeMultipleTablesMergeInfo {

    List<Query> queryList = new ArrayList<Query>();
    long totalKeyFilteredAndValueFilteredCount = 0;
}
