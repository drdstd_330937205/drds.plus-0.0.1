package drds.plus.sql_process.optimizer;

import drds.plus.common.model.hint.DirectlyRouteCondition;
import drds.plus.sql_process.abstract_syntax_tree.node.Node;

class OptimizeResult {

    public Node abstractSyntaxTreeNode = null;
    public DirectlyRouteCondition directlyRouteCondition = null; // 执行计划
    public RuntimeException exception = null;
}
