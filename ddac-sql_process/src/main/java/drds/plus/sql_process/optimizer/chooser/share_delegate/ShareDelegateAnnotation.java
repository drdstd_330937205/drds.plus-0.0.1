package drds.plus.sql_process.optimizer.chooser.share_delegate;

import java.lang.annotation.*;

/**
 * 定义需要被share模式代理的方法
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface ShareDelegateAnnotation {

}
