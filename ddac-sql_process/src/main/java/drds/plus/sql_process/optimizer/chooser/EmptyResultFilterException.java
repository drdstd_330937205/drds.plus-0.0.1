package drds.plus.sql_process.optimizer.chooser;

import drds.plus.sql_process.abstract_syntax_tree.node.Node;


/**
 * 空结果的过滤条件异常，比如 0 = 1的条件
 */
public class EmptyResultFilterException extends RuntimeException {

    private static final long serialVersionUID = -7525463650321091760L;
    private Node node;

    public Node getNode() {
        return node;
    }

    public void setNode(Node node) {
        this.node = node;
    }

    public synchronized Throwable fillInStackTrace() {
        return this;
    }

}
