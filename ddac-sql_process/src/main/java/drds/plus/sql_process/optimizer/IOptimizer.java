package drds.plus.sql_process.optimizer;

import drds.plus.common.jdbc.Parameters;
import drds.plus.common.lifecycle.Lifecycle;
import drds.plus.sql_process.abstract_syntax_tree.execute_plan.ExecutePlan;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.Function;
import drds.plus.sql_process.abstract_syntax_tree.node.Node;

import java.util.Map;

/**
 * 优化器执行接口
 */
public interface IOptimizer extends Lifecycle {
    /**
     * 基于sql进行语法树构建+优化 , cache变量可控制优化的语法树是否会被缓存
     */
    Object optimizeHintOrNode(String sql, Parameters parameters, boolean cached, Map<String, Object> extraCmd) throws OptimizerException;

    /**
     * 基于语法树进行优化
     */
    Node optimizeNode(Node node, Parameters parameters, Map<String, Object> extraCmd) throws OptimizerException;

    /**
     * 设置对应子查询的执行结果,并返回下一个subquery function
     */
    Function subQueryAssignValueAndReturnNextSubQueryFunction(Node node, Map<Long, Object> subQueryFilterIdToResultMap, Map<String, Object> extraCmd) throws OptimizerException;

    /**
     * 将语法树生成对应执行计划, 注意：需要先调用optimizeAst进行语法树优化
     */
    ExecutePlan optimizeNodeAndToExecutePlanAndOptimizeExecutePlan(Node node, Parameters parameters, Map<String, Object> extraCmd) throws OptimizerException;

}
