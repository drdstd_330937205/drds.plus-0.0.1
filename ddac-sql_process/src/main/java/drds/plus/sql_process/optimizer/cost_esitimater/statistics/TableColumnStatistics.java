package drds.plus.sql_process.optimizer.cost_esitimater.statistics;

import java.util.List;

/**
 * 一个表的列的统计数据
 */
public class TableColumnStatistics {

    // 表名
    private String tableName;
    // 列的统计数据
    private List<KVColumnStatistics> columnStats;

    public List<KVColumnStatistics> getColumnStats() {
        return columnStats;
    }

    public void setColumnStats(List<KVColumnStatistics> columnStats) {
        this.columnStats = columnStats;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public void addColumnStat(KVColumnStatistics columnStat) {
        if (columnStat == null) {
            return;
        }
        columnStats.add(columnStat);
    }

}
