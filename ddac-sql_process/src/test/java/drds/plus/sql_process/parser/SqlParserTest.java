package drds.plus.sql_process.parser;

import drds.plus.common.jdbc.Parameters;
import drds.plus.common.jdbc.SetParameterMethod;
import drds.plus.common.jdbc.SetParameterMethodAndArgs;
import drds.plus.common.model.SqlType;
import drds.plus.sql_process.BaseOptimizerTest;
import drds.plus.sql_process.abstract_syntax_tree.ObjectCreateFactory;
import drds.plus.sql_process.abstract_syntax_tree.expression.bind_value.BindValueImpl;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.column.Column;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.BooleanFilter;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.Filter;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.Function;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.Operation;
import drds.plus.sql_process.abstract_syntax_tree.node.dml.Delete;
import drds.plus.sql_process.abstract_syntax_tree.node.dml.Insert;
import drds.plus.sql_process.abstract_syntax_tree.node.dml.Replace;
import drds.plus.sql_process.abstract_syntax_tree.node.dml.Update;
import drds.plus.sql_process.abstract_syntax_tree.node.query.*;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.util.*;

/**
 * @author jianghang 2013-11-15 下午3:55:47
 * @since 5.0.0
 */
public class SqlParserTest extends BaseOptimizerTest {

    @Test
    public void testQuery_简单主键查询() throws SqlParserException {
        String sql = "query * from table1 where id=1 or id = 2";
        Query qn = query(sql);
        qn.build();

        Query qnExpected = new TableQuery("TABLE1");
        qnExpected.setWhere("ID=1 or ID=2");
        qnExpected.build();
        assertEquals(qn, qnExpected);
    }

    @Test
    public void testQuery_字段别名() throws SqlParserException {
        String sql = "SELECT ID AS TID,NAME,SCHOOL FROM TABLE1  WHERE ID=1";
        Query qn = query(sql);
        qn.build();

        Query qnExpected = new TableQuery("TABLE1");
        qnExpected.setSelectItemListAndSetNeedBuild("ID AS TID,NAME,SCHOOL");
        qnExpected.setWhere("ID=1");
        qnExpected.build();
        assertEquals(qn, qnExpected);
    }

    @Test
    public void testQuery_字段别名_表别名() throws SqlParserException {
        String sql = "SELECT T.ID AS TID,T.NAME,T.SCHOOL FROM TABLE1 T  WHERE ID=1";
        Query qn = query(sql);
        qn.build();

        Query qnExpected = new TableQuery("TABLE1");
        qnExpected.setAliasAndSetNeedBuild("T");
        qnExpected.setSelectItemListAndSetNeedBuild("ID AS TID,NAME,SCHOOL");
        qnExpected.setWhere("ID=1");
        qnExpected.build();
        assertEquals(qn, qnExpected);
    }

    @Test
    public void testQuery_函数别名() throws SqlParserException {
        String sql = "SELECT T.ID , LENGTH(NAME) AS LEN FROM TABLE1 T  WHERE ID=1";
        Query qn = query(sql);
        qn.build();

        Function function = ObjectCreateFactory.createFunction();
        function.setColumnName("LENGTH(NAME)");
        function.setAlias("LEN");
        function.setFunctionName("LENGTH");

        Query qnExpected = new TableQuery("TABLE1");
        qnExpected.setAliasAndSetNeedBuild("T");
        qnExpected.setSelectItemListAndSetNeedBuild("T.ID");
        qnExpected.setWhere("ID=1");
        qnExpected.addSelectItem(function);
        qnExpected.build();
        assertEquals(qn, qnExpected);
    }

    @Test
    public void testQuery_普通join() throws SqlParserException {
        String sql = "SELECT * FROM TABLE1 A JOIN TABLE2 B ON A.ID=B.ID WHERE A.NAME=1";
        Query qn = query(sql);
        qn.build();

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        TableQuery tableQueryNode2 = new TableQuery("TABLE2");
        Query qnExpected = tableQueryNode1;
        qnExpected.setAliasAndSetNeedBuild("A");
        tableQueryNode2.setAliasAndSetNeedBuild("B");
        Join joinNode = qnExpected.join(tableQueryNode2);
        joinNode.addJoinKeys("A.ID", "B.ID");
        joinNode.setWhere("A.NAME=1");
        qnExpected.build();
        assertEquals(qn, qnExpected);
    }

    @Test
    public void testQuery_内连接() throws SqlParserException {
        String sql = "SELECT * FROM TABLE1 A INNER JOIN TABLE2 B ON A.ID=B.ID WHERE A.NAME=1";
        Query qn = query(sql);
        qn.build();

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        TableQuery tableQueryNode2 = new TableQuery("TABLE2");
        tableQueryNode1.setAliasAndSetNeedBuild("A");
        tableQueryNode2.setAliasAndSetNeedBuild("B");
        Join joinNode = tableQueryNode1.join(tableQueryNode2);
        joinNode.setInnerJoin();
        Query qnExpected = joinNode;
        joinNode.addJoinKeys("A.ID", "B.ID");
        joinNode.setWhere("A.NAME=1");
        qnExpected.build();
        assertEquals(qn, qnExpected);
    }

    @Test
    public void testQuery_左连接() throws SqlParserException {
        String sql = "SELECT * FROM TABLE1 A LEFT JOIN TABLE2 B ON A.ID=B.ID WHERE A.NAME=1";
        Query qn = query(sql);
        qn.build();

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        TableQuery tableQueryNode2 = new TableQuery("TABLE2");
        tableQueryNode1.setAliasAndSetNeedBuild("A");
        tableQueryNode2.setAliasAndSetNeedBuild("B");
        Join joinNode = tableQueryNode1.join(tableQueryNode2);
        joinNode.setLeftOuterJoin();
        joinNode.addJoinKeys("A.ID", "B.ID");
        joinNode.setWhere("A.NAME=1");
        Query qnExpected = joinNode;
        qnExpected.build();
        assertEquals(qn, qnExpected);
    }

    @Test
    public void testQuery_右连接() throws SqlParserException {
        String sql = "SELECT * FROM TABLE1 A RIGHT JOIN TABLE2 B ON A.ID=B.ID WHERE A.NAME=1";
        Query qn = query(sql);
        qn.build();

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        TableQuery tableQueryNode2 = new TableQuery("TABLE2");
        tableQueryNode1.setAliasAndSetNeedBuild("A");
        tableQueryNode2.setAliasAndSetNeedBuild("B");
        Join joinNode = tableQueryNode1.join(tableQueryNode2);
        joinNode.setRightOuterJoin();
        joinNode.addJoinKeys("A.ID", "B.ID");
        joinNode.setWhere("A.NAME=1");
        Query qnExpected = joinNode;
        qnExpected.build();
        assertEquals(qn, qnExpected);
    }

    @Ignore("mysql parser语法上暂时不支持，需要修改")
    @Test
    public void testQuery_outter连接() throws SqlParserException {
        String sql = "SELECT * FROM TABLE1 A OUTER JOIN TABLE2 B ON A.ID=B.ID WHERE A.NAME=1";
        Query qn = query(sql);
        qn.build();

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        TableQuery tableQueryNode2 = new TableQuery("TABLE2");
        tableQueryNode1.setAliasAndSetNeedBuild("A");
        tableQueryNode2.setAliasAndSetNeedBuild("B");
        Join joinNode = tableQueryNode1.join(tableQueryNode2);
        joinNode.setFullOuterJoin();
        joinNode.addJoinKeys("A.ID", "B.ID");
        joinNode.setWhere("A.NAME=1");
        Query qnExpected = joinNode;
        qnExpected.build();
        assertEquals(qn, qnExpected);
    }

    @Test
    public void testQuery_普通链接_多个连接条件() throws SqlParserException {
        String sql = "SELECT * FROM TABLE1 A INNER JOIN TABLE2 B ON A.ID=B.ID and A.NAME = B.NAME WHERE A.NAME=1";
        Query qn = query(sql);
        qn.build();

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        TableQuery tableQueryNode2 = new TableQuery("TABLE2");
        tableQueryNode1.setAliasAndSetNeedBuild("A");
        tableQueryNode2.setAliasAndSetNeedBuild("B");
        Join joinNode = tableQueryNode1.join(tableQueryNode2);
        joinNode.addJoinKeys("A.ID", "B.ID");
        joinNode.addJoinKeys("A.NAME", "B.NAME");
        joinNode.setWhere("A.NAME=1");
        Query qnExpected = joinNode;
        qnExpected.build();
        assertEquals(qn, qnExpected);
    }

    @Test
    public void testQuery_普通链接_字段别名() throws SqlParserException {
        String sql = "SELECT A.ID AS AID,A.SCHOOL AS ASCHOOL,B.* FROM TABLE1 A INNER JOIN TABLE2 B ON A.ID=B.ID and A.NAME = B.NAME WHERE A.NAME=1";
        Query qn = query(sql);
        qn.build();

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        TableQuery tableQueryNode2 = new TableQuery("TABLE2");
        tableQueryNode1.setAliasAndSetNeedBuild("A");
        tableQueryNode2.setAliasAndSetNeedBuild("B");
        Join joinNode = tableQueryNode1.join(tableQueryNode2);
        joinNode.addJoinKeys("A.ID", "B.ID");
        joinNode.addJoinKeys("A.NAME", "B.NAME");
        joinNode.setWhere("A.NAME=1");
        joinNode.setSelectItemListAndSetNeedBuild("A.ID AS AID,A.SCHOOL AS ASCHOOL,B.*");
        Query qnExpected = joinNode;
        qnExpected.build();
        assertEquals(qn, qnExpected);
    }

    @Test
    public void testQuery_普通链接_order_group_having() throws SqlParserException {
        String sql = "SELECT A.ID AS AID,A.SCHOOL AS ASCHOOL,B.* FROM TABLE1 A INNER JOIN TABLE2 B ON A.ID=B.ID and A.NAME = B.NAME WHERE A.NAME=1";
        sql += " GROUP BY AID HAVING AID > 0 ORDER BY A.ID asc ";
        Query qn = query(sql);
        qn.build();

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        TableQuery tableQueryNode2 = new TableQuery("TABLE2");
        tableQueryNode1.setAliasAndSetNeedBuild("A");
        tableQueryNode2.setAliasAndSetNeedBuild("B");
        Join joinNode = tableQueryNode1.join(tableQueryNode1);
        joinNode.addJoinKeys("A.ID", "B.ID");
        joinNode.addJoinKeys("A.NAME", "B.NAME");
        joinNode.setWhere("A.NAME=1");
        joinNode.setSelectItemListAndSetNeedBuild("A.ID AS AID,A.SCHOOL AS ASCHOOL,B.*");
        joinNode.addGroupByItemAndSetNeedBuild("AID");
        joinNode.having("AID > 0");
        joinNode.addOrderByItemAndSetNeedBuild("A.ID", true);
        Query qnExpected = joinNode;
        qnExpected.build();
        assertEquals(qn, qnExpected);
    }

    @Test
    public void testQuery_普通链接_函数() throws SqlParserException {
        String sql = "SELECT A.ID setAliasAndSetNeedBuild AID,A.ID,count(A.ID),count(*) FROM TABLE1 A INNER JOIN TABLE2 B ON A.ID=B.ID and A.NAME = B.NAME WHERE A.NAME=1";
        sql += " GROUP BY AID HAVING AID > 0 ORDER BY A.ID asc ";
        Query qn = query(sql);
        qn.build();

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        TableQuery tableQueryNode2 = new TableQuery("TABLE2");
        tableQueryNode1.setAliasAndSetNeedBuild("A");
        tableQueryNode2.setAliasAndSetNeedBuild("B");
        Join joinNode = tableQueryNode1.join(tableQueryNode2);
        joinNode.addJoinKeys("A.ID", "B.ID");
        joinNode.addJoinKeys("A.NAME", "B.NAME");

        joinNode.setWhere("A.NAME=1");
        joinNode.setSelectItemListAndSetNeedBuild("A.ID AS AID,A.ID");
        joinNode.addGroupByItemAndSetNeedBuild("AID");
        joinNode.having("AID > 0");
        joinNode.addOrderByItemAndSetNeedBuild("A.ID", true);
        Query qnExpected = joinNode;
        Function function1 = ObjectCreateFactory.createFunction();
        function1.setColumnName("count(A.ID)");
        function1.setFunctionName("count");

        Function function2 = ObjectCreateFactory.createFunction();
        function2.setColumnName("count(*)");
        function2.setFunctionName("count");
        qnExpected.addSelectItem(function1);
        qnExpected.addSelectItem(function2);

        qnExpected.build();
        assertEquals(qn, qnExpected);
    }

    // @Test
    // 后续调整
    public void testQuery_内连接_OR条件_不支持() throws Exception {
        String sql = "SELECT * FROM TABLE1 A INNER JOIN TABLE2 B ON A.ID=B.ID or A.NAME = B.NAME WHERE A.NAME=1";
        try {
            Query qn = query(sql);
            qn.build();
            Assert.fail();
        } catch (Exception e) {
            Assert.assertEquals("java.lang.IllegalArgumentException: not support 'or' in join on statment ", e.getCause().getMessage());
        }
    }

    @Test
    public void testQuery_多表join() throws Exception {
        String sql = "SELECT * FROM TABLE1 JOIN TABLE2 LEFT JOIN TABLE3 ON (TABLE3.ID=TABLE2.ID) LEFT JOIN TABLE4 ON (TABLE4.ID=TABLE1.ID) WHERE TABLE2.ID= TABLE4.ID";
        Query qn = query(sql);
        qn.build();

        Assert.assertTrue(qn instanceof Join);
    }

    @Test
    public void testQuery_正常orderby() throws Exception {
        String sql = "SELECT ID,NAME FROM TABLE1 WHERE ID=1 ORDER BY ID";
        Query qn = query(sql);
        qn.build();

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        tableQueryNode1.setSelectItemListAndSetNeedBuild("ID,NAME");
        tableQueryNode1.setWhere("ID=1");
        tableQueryNode1.addOrderByItemAndSetNeedBuild("ID");

        Query qnExpected = tableQueryNode1;
        qnExpected.build();
        assertEquals(qn, qnExpected);
    }

    @Test
    public void testQuery_OrderByAsc() throws Exception {
        String sql = "SELECT ID,NAME FROM TABLE1 WHERE ID=1 ORDER BY ID asc";
        Query qn = query(sql);
        qn.build();

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        tableQueryNode1.setSelectItemListAndSetNeedBuild("ID,NAME");
        tableQueryNode1.setWhere("ID=1");
        tableQueryNode1.addOrderByItemAndSetNeedBuild("ID", true);
        Query qnExpected = tableQueryNode1;
        qnExpected.build();
        assertEquals(qn, qnExpected);
    }

    @Test
    public void testQuery_OrderByDesc() throws Exception {
        String sql = "SELECT ID,NAME FROM TABLE1 WHERE ID=1 ORDER BY ID desc";
        Query qn = query(sql);
        qn.build();

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        tableQueryNode1.setSelectItemListAndSetNeedBuild("ID,NAME");
        tableQueryNode1.setWhere("ID=1");
        tableQueryNode1.addOrderByItemAndSetNeedBuild("ID", false);
        Query qnExpected = tableQueryNode1;
        qnExpected.build();
        assertEquals(qn, qnExpected);
    }

    public void testQuery_OrderByIdAndNameASC() throws Exception {
        String sql = "SELECT ID,NAME FROM TABLE1 WHERE ID=1 ORDER BY ID,NAME";
        Query qn = query(sql);
        qn.build();

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        tableQueryNode1.setSelectItemListAndSetNeedBuild("ID,NAME");
        tableQueryNode1.setWhere("ID=1");
        tableQueryNode1.addOrderByItemAndSetNeedBuild("ID");
        tableQueryNode1.addOrderByItemAndSetNeedBuild("NAME");
        Query qnExpected = tableQueryNode1;
        qnExpected.build();
        assertEquals(qn, qnExpected);

    }

    @Test
    public void testQuery_OrderByIdAndNameDesc() throws Exception {
        String sql = "SELECT ID,NAME FROM TABLE1 WHERE ID=1 ORDER BY ID,NAME desc";
        Query qn = query(sql);
        qn.build();

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        tableQueryNode1.setSelectItemListAndSetNeedBuild("ID,NAME");
        tableQueryNode1.setWhere("ID=1");
        tableQueryNode1.addOrderByItemAndSetNeedBuild("ID");
        tableQueryNode1.addOrderByItemAndSetNeedBuild("NAME", false);
        Query qnExpected = tableQueryNode1;
        qnExpected.build();
        assertEquals(qn, qnExpected);
    }

    @Test
    public void testQuery_OrderByIdDescAndNameDesc() throws Exception {
        String sql = "SELECT ID,NAME FROM TABLE1 WHERE ID=1 ORDER BY ID desc,NAME desc";
        Query qn = query(sql);
        qn.build();

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        tableQueryNode1.setSelectItemListAndSetNeedBuild("ID,NAME");
        tableQueryNode1.setWhere("ID=1");
        tableQueryNode1.addOrderByItemAndSetNeedBuild("ID", false);
        tableQueryNode1.addOrderByItemAndSetNeedBuild("NAME", false);
        Query qnExpected = tableQueryNode1;
        qnExpected.build();
        assertEquals(qn, qnExpected);

    }

    @Test
    public void testQuery_OrExpression() throws SqlParserException {
        String sql = "SELECT * FROM TABLE1 WHERE NAME = 2323 or ID=1";
        Query qn = query(sql);
        qn.build();

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        tableQueryNode1.setWhere("NAME=2323 || ID=1");
        Query qnExpected = tableQueryNode1;
        qnExpected.build();
        assertEquals(qn, qnExpected);
    }

    @Test
    public void testQuery_复杂条件() throws SqlParserException {
        String sql = "SELECT * FROM TABLE1 WHERE (SCHOOL=1 or NAME=2) and (ID=1)";
        Query qn = query(sql);
        qn.build();

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        tableQueryNode1.setWhere("(SCHOOL=1 || NAME=2) && (ID=1)");
        Query qnExpected = tableQueryNode1;
        qnExpected.build();
        assertEquals(qn, qnExpected);
    }

    @Test
    public void testJoin_多表主键关联() throws SqlParserException {
        String sql = "SELECT * FROM TABLE1,TABLE2 WHERE TABLE1.NAME=1 and TABLE1.ID=TABLE2.ID";
        Query qn = query(sql);
        qn.build();

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        Join joinNode = tableQueryNode1.join("TABLE2");
        joinNode.setWhere("TABLE1.NAME=1 and TABLE1.ID=TABLE2.ID");
        Query qnExpected = joinNode;
        qnExpected.build();
        assertEquals(qn, qnExpected);
    }

    @Test
    public void testJoin_多表主键关联_表别名() throws SqlParserException {
        String sql = "SELECT * FROM TABLE1 T1,TABLE2 WHERE T1.NAME=1";
        Query qn = query(sql);
        qn.build();

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        TableQuery tableQueryNode2 = new TableQuery("TABLE2");
        tableQueryNode1.setAliasAndSetNeedBuild("T1");
        Join joinNode = tableQueryNode1.join(tableQueryNode2);
        joinNode.setWhere("T1.NAME=1");
        Query qnExpected = joinNode;
        qnExpected.build();
        assertEquals(qn, qnExpected);
    }

    @Test
    public void testQuery_and表达式() throws SqlParserException {
        String sql = "SELECT * FROM TABLE1 T1 WHERE ID<=10 and ID>=5";
        Query qn = query(sql);
        qn.build();

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        tableQueryNode1.setAliasAndSetNeedBuild("T1");
        tableQueryNode1.setWhere("ID<=10 and ID>=5");
        Query qnExpected = tableQueryNode1;
        qnExpected.build();
        assertEquals(qn, qnExpected);
    }

    @Test
    public void testQuery_and表达式_别名() throws SqlParserException {
        String sql = "SELECT * FROM TABLE1 T1 WHERE T1.ID=4 and T1.ID>=2";
        Query qn = query(sql);
        qn.build();

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        tableQueryNode1.setSelectItemListAndSetNeedBuild("*");
        tableQueryNode1.setAliasAndSetNeedBuild("T1");
        tableQueryNode1.setWhere("T1.ID=4 && T1.ID>=2");

        Query qnExpected = tableQueryNode1;
        qnExpected.build();

        assertEquals(qn, qnExpected);
    }

    @Test
    public void testQuery_and表达式_字符串() throws SqlParserException {
        String sql = "SELECT * FROM TABLE1 T1 WHERE NAME='4' and ID<=2";
        Query qn = query(sql);
        qn.build();

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        tableQueryNode1.setAliasAndSetNeedBuild("T1");
        tableQueryNode1.setWhere("NAME=4 && ID<=2");
        Query qnExpected = tableQueryNode1;
        qnExpected.build();
        assertEquals(qn, qnExpected);
    }

    @Test
    public void testQuery_or表达式() throws SqlParserException {
        String sql = "SELECT * FROM TABLE1 T1 WHERE ID<5 or ID<=6 or ID=3";
        Query qn = query(sql);
        qn.build();

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        tableQueryNode1.setAliasAndSetNeedBuild("T1");
        tableQueryNode1.setWhere("ID<5 || ID<=6 || ID=3");
        Query qnExpected = tableQueryNode1;
        qnExpected.build();
        assertEquals(qn, qnExpected);
    }

    @Test
    public void testQueryWith_or表达式_别名() throws SqlParserException {
        String sql = "SELECT * FROM TABLE1 T1 WHERE ID<5 or ID<=6 or ID=7";
        Query qn = query(sql);
        qn.build();

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        tableQueryNode1.setAliasAndSetNeedBuild("T1");
        tableQueryNode1.setWhere(" ID<5 || ID<=6 || ID=7");
        Query qnExpected = tableQueryNode1;
        tableQueryNode1.build();
        assertEquals(qn, qnExpected);
    }

    @Test
    public void testFunction() throws SqlParserException {
        String sql = "SELECT count(*) FROM TABLE1 T1 WHERE ID = 1";
        Query qn = query(sql);
        qn.build();

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        Function f = ObjectCreateFactory.createFunction();
        f.setFunctionName("count");
        Column c = ObjectCreateFactory.createColumn();
        c.setColumnName("*");

        List args = new ArrayList();
        args.add(c);

        f.setArgList(args);
        f.setTableName("TABLE1");
        f.setColumnName("count(*)");
        tableQueryNode1.setAliasAndSetNeedBuild("T1");
        tableQueryNode1.setWhere("ID=1");
        tableQueryNode1.setSelectItemList(f);
        Query qnExpected = tableQueryNode1;
        qnExpected.build();
        assertEquals(qn, qnExpected);
    }

    @Test
    public void testFunction1() throws SqlParserException {
        String sql = "SELECT count(ID) FROM TABLE1 T1 WHERE ID = 1";
        Query qn = query(sql);

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");

        Function f = ObjectCreateFactory.createFunction();
        f.setFunctionName("count");
        f.setColumnName("count(ID)");
        Column c = ObjectCreateFactory.createColumn();
        c.setColumnName("ID");

        List args = new ArrayList();
        args.add(c);
        f.setArgList(args);
        tableQueryNode1.setAliasAndSetNeedBuild("T1");
        tableQueryNode1.setWhere("ID=1");
        tableQueryNode1.setSelectItemList(f);
        Query qnExpected = tableQueryNode1;
        assertEquals(qn, qnExpected);
    }

    @Test
    public void testFunction_double_function() throws SqlParserException {
        String sql = "SELECT count(ID),avg(ID) FROM TABLE1 T1 WHERE ID = 1";
        Query qn = query(sql);
        qn.build();
        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        tableQueryNode1.setAliasAndSetNeedBuild("T1");
        tableQueryNode1.setWhere("ID=1");
        tableQueryNode1.setSelectItemListAndSetNeedBuild("count(ID),avg(ID)");
        Query qnExpected = tableQueryNode1;
        qnExpected.build();
        assertEquals(qn, qnExpected);
    }

    @Test
    public void testFunction_to_char() throws Exception {
        String sql = "SELECT * FROM TABLE1 T1 WHERE DATE_ADD(ID, interval_primary 1 SECOND)= '2012-11-11'";
        Query qn = query(sql);
        qn.build();

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");

        Function f = ObjectCreateFactory.createFunction();
        f.setFunctionName("DATE_ADD");

        Column c = ObjectCreateFactory.createColumn();
        c.setColumnName("ID");
        List args = new ArrayList();
        args.add(c);
        args.add("interval_primary 1 SECOND");
        f.setArgList(args);

        f.setColumnName("DATE_ADD(ID, interval_primary 1 SECOND)");
        Filter filter = ObjectCreateFactory.createBooleanFilter();
        filter.setOperation(Operation.equal);
        ((BooleanFilter) filter).setColumn(f);
        ((BooleanFilter) filter).setValue("2012-11-11");
        tableQueryNode1.setAliasAndSetNeedBuild("T1");
        tableQueryNode1.setWhereAndSetNeedBuild(filter);
        Query qnExpected = tableQueryNode1;
        qnExpected.build();
        assertEquals(qn, qnExpected);
    }

    @Test
    public void testFunction_twoArgs() throws Exception {
        String sql = "SELECT * FROM TABLE1 T1 WHERE IFNULL(STR_TO_DATE(ID, '%d,%m,%y'),1) = '1'";
        Query qn = query(sql);
        qn.build();

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");

        Function f = ObjectCreateFactory.createFunction();
        f.setFunctionName("STR_TO_DATE");

        Function f2 = ObjectCreateFactory.createFunction();
        f2.setFunctionName("IFNULL");
        f2.setColumnName("IFNULL(STR_TO_DATE(ID, '%d,%m,%y'), 1)");
        Column c = ObjectCreateFactory.createColumn();
        c.setColumnName("ID");

        List args = new ArrayList();
        args.add(c);
        args.add("%d,%m,%y");
        f.setArgList(args);

        f.setColumnName("STR_TO_DATE(id, '%d,%m,%Y')");
        args = new ArrayList();
        args.add(f);
        args.add(1);

        f2.setArgList(args);
        Filter filter = ObjectCreateFactory.createBooleanFilter();
        filter.setOperation(Operation.equal);
        ((BooleanFilter) filter).setColumn(f2);
        ((BooleanFilter) filter).setValue('1');
        tableQueryNode1.setAliasAndSetNeedBuild("T1");
        tableQueryNode1.setWhereAndSetNeedBuild(filter);
        Query qnExpected = tableQueryNode1;
        qnExpected.build();
        assertEquals(qn, qnExpected);
    }

    @Test
    public void testFunction_noArgs() throws Exception {
        String sql = "SELECT * FROM TABLE1 T1 WHERE ID = NOW()";
        Query qn = query(sql);
        qn.build();
        TableQuery tableQueryNode1 = new TableQuery("TABLE1");

        Function f = ObjectCreateFactory.createFunction();
        f.setFunctionName("NOW");
        f.setColumnName("NOW()");

        Column c = ObjectCreateFactory.createColumn();
        c.setColumnName("ID");

        Filter filter = ObjectCreateFactory.createBooleanFilter();
        filter.setOperation(Operation.equal);
        ((BooleanFilter) filter).setColumn(c);
        ((BooleanFilter) filter).setValue(f);
        tableQueryNode1.setAliasAndSetNeedBuild("T1");
        tableQueryNode1.setWhereAndSetNeedBuild(filter);
        Query qnExpected = tableQueryNode1;
        qnExpected.build();
        assertEquals(qn, qnExpected);
    }

    @Test
    public void testGroupBY() throws Exception {
        String sql = "SELECT * FROM TABLE1 T1 WHERE ID = 1 GROUP BY NAME ";
        Query qn = query(sql);
        qn.build();

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        tableQueryNode1.setAliasAndSetNeedBuild("T1");
        tableQueryNode1.setWhere(" ID=1");
        tableQueryNode1.addGroupByItemAndSetNeedBuild("NAME");
        Query qnExpected = tableQueryNode1;
        qnExpected.build();
        assertEquals(qn, qnExpected);
    }

    @Test
    public void testFunction_提前计算() throws SqlParserException {
        String sql = "SELECT 1+1 FROM TABLE1";
        Query qn = query(sql);
        qn.build();

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        tableQueryNode1.setSelectItemListAndSetNeedBuild("2");
        Query qnExpected = tableQueryNode1;
        qnExpected.build();
        assertEquals(qn, qnExpected);
    }

    @Test
    public void testFunction_提前计算_bindVal() throws SqlParserException {
        String sql = "SELECT 1+? FROM TABLE1";
        Query qn = query(sql, Arrays.asList(Integer.valueOf(1)));

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        tableQueryNode1.setSelectItemListAndSetNeedBuild("1+?");// 不做计算，否则解析结果不能缓存
        Query qnExpected = tableQueryNode1;
        assertEquals(qn, qnExpected);
    }

    @Test
    public void testJoin_条件提前计算() throws SqlParserException {
        String sql = "SELECT * FROM TABLE1 A JOIN TABLE2 B ON A.ID=B.ID WHERE A.ID>1+4 and B.ID<12-1";
        Query qn = query(sql);
        qn.build();

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        TableQuery tableQueryNode2 = new TableQuery("TABLE2");
        tableQueryNode1.setAliasAndSetNeedBuild("A");
        tableQueryNode2.setAliasAndSetNeedBuild("B");
        Join joinNode = tableQueryNode1.join(tableQueryNode2);
        joinNode.addJoinKeys("ID", "ID");
        joinNode.setWhere("A.ID>5 && B.ID<11");
        Query qnExpected = joinNode;
        qnExpected.build();
        assertEquals(qn, qnExpected);
    }

    @Test
    public void testUpdate_正常() throws SqlParserException {
        String sql = "update TABLE1 SET NAME=2 WHERE ID>=5 and ID<=5";
        Update un = update(sql);
        un.build();

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        Update unExpected = tableQueryNode1.update("NAME", new Comparable[]{2});
        tableQueryNode1.setWhere("ID>=5 and ID<=5");
        unExpected.build();
        assertEquals(un, unExpected);
    }

    @Test
    public void testDelete_正常() throws SqlParserException {
        String sql = "delete FROM TABLE1 WHERE ID>=5 and ID<=5";
        Delete dn = delete(sql);
        dn.build();

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        Delete dnExpected = tableQueryNode1.delete();
        tableQueryNode1.setWhere("ID>=5 and ID<=5");
        dnExpected.build();

        assertEquals(dn, dnExpected);
    }

    @Test
    public void testInsert_无字段() throws SqlParserException {
        String sql = "insert INTO TABLE1(ID) VALUES (2)";
        Insert in = insert(sql);
        in.build();

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        Insert inExpected = tableQueryNode1.insert("ID", new Comparable[]{2});
        inExpected.build();

        assertEquals(in, inExpected);
    }

    @Test
    public void testInsert_多字段() throws SqlParserException {
        String sql = "insert INTO TABLE1(ID, NAME, SCHOOL) VALUES (2, 'sun', 'sysu')";

        Insert in = insert(sql);
        in.build();

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        Insert inExpected = tableQueryNode1.insert("ID NAME SCHOOL", new Comparable[]{2, "sun", "sysu"});
        inExpected.build();

        assertEquals(in, inExpected);
    }

    @Test
    public void testInsert_多字段_多记录() throws SqlParserException {
        String sql = "insert INTO TABLE1(ID, NAME, SCHOOL) VALUES (1, 'sun', 'sysu'),(2, 'sun', 'sysu'),(3, 'sun', 'sysu')";

        Insert in = insert(sql);
        in.build();

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        Insert inExpected = tableQueryNode1.insert("ID NAME SCHOOL", new Object[]{Arrays.asList(1, "sun", "sysu"), Arrays.asList(2, "sun", "sysu"), Arrays.asList(3, "sun", "sysu")});
        inExpected.build();

        assertEquals(in, inExpected);
    }

    @Test
    public void testReplace_多字段_多记录() throws SqlParserException {
        String sql = "replace INTO TABLE1(ID, NAME, SCHOOL) VALUES (1, 'sun', 'sysu'),(2, 'sun', 'sysu'),(3, 'sun', 'sysu')";

        Replace in = put(sql);
        in.build();

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        Replace inExpected = tableQueryNode1.put("ID NAME SCHOOL", new Object[]{Arrays.asList(1, "sun", "sysu"), Arrays.asList(2, "sun", "sysu"), Arrays.asList(3, "sun", "sysu")});
        inExpected.build();

        assertEquals(in, inExpected);
    }

    @Test
    public void testLimit() throws SqlParserException {
        String sql = "SELECT * FROM TABLE1 LIMIT 1,10";
        Query qn = query(sql);
        qn.build();

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        tableQueryNode1.limit(1, 10);
        Query qnExpected = tableQueryNode1;
        qnExpected.build();

        assertEquals(qn, qnExpected);
    }

    @Test
    public void testLimit1() throws SqlParserException {
        String sql = "SELECT * FROM TABLE1 WHERE ID = 10 LIMIT 10";
        Query qn = query(sql);
        qn.build();

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        tableQueryNode1.setWhere("id=10");
        tableQueryNode1.setLimitFrom(0);
        tableQueryNode1.setLimitTo(10);
        Query qnExpected = tableQueryNode1;
        qnExpected.build();
        assertEquals(qn, qnExpected);
    }

    @Test
    public void testLimit_bindval1() throws SqlParserException {
        String sql = "SELECT * FROM TABLE1 WHERE TABLE1.ID = 10 LIMIT ?,10";
        Query qn = query(sql);
        qn.build();

        Assert.assertTrue(qn.getLimitFrom() instanceof BindValueImpl);
        BindValueImpl bv = (BindValueImpl) qn.getLimitFrom();
        Assert.assertEquals(1, bv.getIndex());
        Assert.assertEquals(10L, qn.getLimitTo());
    }

    @Test
    public void testLimit_bindval2() throws SqlParserException {
        String sql = "query * from table1 where table1.id = 10 limit 1,?";
        Query qn = query(sql);
        qn.build();
        Assert.assertTrue(qn.getLimitTo() instanceof BindValueImpl);
        BindValueImpl bv = (BindValueImpl) qn.getLimitTo();
        Assert.assertEquals(1, bv.getIndex());
        Assert.assertEquals(1L, qn.getLimitFrom());
    }

    @Test
    public void testLimit_bindval3() throws SqlParserException {
        String sql = "query * from table1 where table1.id = 10 limit ?,?";
        Query qn = query(sql);
        qn.build();
        Assert.assertTrue(qn.getLimitFrom() instanceof BindValueImpl);
        BindValueImpl bv = (BindValueImpl) qn.getLimitFrom();
        Assert.assertEquals(1, bv.getIndex());
        Assert.assertTrue(qn.getLimitTo() instanceof BindValueImpl);
        bv = (BindValueImpl) qn.getLimitTo();
        Assert.assertEquals(2, bv.getIndex());

    }

    @Test
    public void testPreparedInsertSql() throws SqlParserException {
        String sql = "insert INTO TABLE1(ID,NAME,SCHOOL) VALUES (?, ?, ?)";
        Insert in = insert(sql);
        Map<Integer, SetParameterMethodAndArgs> currentParameter = new HashMap<Integer, SetParameterMethodAndArgs>();
        SetParameterMethodAndArgs p1 = new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{0, 2});
        SetParameterMethodAndArgs p2 = new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{1, "sun"});
        SetParameterMethodAndArgs p3 = new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{2, "sysu"});
        currentParameter.put(1, p1);
        currentParameter.put(2, p2);
        currentParameter.put(3, p3);
        Parameters parameters = new Parameters(currentParameter, false);

        in.build();
        in.assignment(parameters);

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        Insert inExpected = tableQueryNode1.insert("ID NAME SCHOOL", new Comparable[]{2, "sun", "sysu"});
        inExpected.build();
        assertEquals(in, inExpected);
    }

    @Test
    public void testPreparedUpdateSql() throws SqlParserException {
        String sql = "update TABLE1 SET ID=? WHERE ID>=? and ID<=?";
        Update un = update(sql);

        Map<Integer, SetParameterMethodAndArgs> currentParameter = new HashMap<Integer, SetParameterMethodAndArgs>();
        SetParameterMethodAndArgs p1 = new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{0, 2});
        SetParameterMethodAndArgs p2 = new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{1, 3});
        SetParameterMethodAndArgs p3 = new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{2, 5});
        currentParameter.put(1, p1);
        currentParameter.put(2, p2);
        currentParameter.put(3, p3);

        Parameters parameters = new Parameters(currentParameter, false);
        un.assignment(parameters);
        un.build();

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        Update unExpected = tableQueryNode1.update("ID", new Comparable[]{2});
        tableQueryNode1.setWhere("ID>=3 and ID<=5");
        unExpected.build();

        assertEquals(un, unExpected);
    }

    @Test
    public void testPreparedDeleteSql() throws SqlParserException {
        String sql = "delete FROM TABLE1 WHERE ID>=? and ID<=?";
        Delete dn = delete(sql);

        Map<Integer, SetParameterMethodAndArgs> currentParameter = new HashMap<Integer, SetParameterMethodAndArgs>();
        SetParameterMethodAndArgs p1 = new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{0, 3});
        SetParameterMethodAndArgs p2 = new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{1, 5});
        currentParameter.put(1, p1);
        currentParameter.put(2, p2);

        Parameters parameters = new Parameters(currentParameter, false);
        dn.assignment(parameters);
        dn.build();

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        Delete dnExpected = tableQueryNode1.delete();
        tableQueryNode1.setWhere("ID>=3 and ID<=5");
        dnExpected.build();
        assertEquals(dn, dnExpected);
    }

    @Test
    public void testDistinct() throws SqlParserException {
        String sql = "SELECT count(DISTINCT ID) FROM TABLE1";
        Query qn = query(sql);

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        Column c = ObjectCreateFactory.createColumn();
        c.setColumnName("ID");
        c.setDistinct(true);

        Function f = ObjectCreateFactory.createFunction();
        f.setFunctionName("count");
        f.setColumnName("count(DISTINCT ID)");

        List args = new ArrayList();
        args.add(c);
        f.setArgList(args);
        qn.build();
        tableQueryNode1.setSelectItemList(f);
        Query qnExpected = tableQueryNode1;
        qnExpected.build();
        assertEquals(qn, qnExpected);
    }

    @Test
    public void testQuery_orderBy加函数() throws Exception {
        String sql = "SELECT * FROM TABLE1 WHERE ID=1 ORDER BY count(ID)";
        Query qn = query(sql);
        qn.build();

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        Column c = ObjectCreateFactory.createColumn();
        c.setColumnName("ID");

        Function f = ObjectCreateFactory.createFunction();
        f.setFunctionName("count");
        List args = new ArrayList();
        args.add(c);

        f.setArgList(args);

        f.setColumnName("count(ID)");
        tableQueryNode1.setWhere("ID=1");
        tableQueryNode1.addOrderByItemAndSetNeedBuild(f, true);
        Query qnExpected = tableQueryNode1;
        qnExpected.build();
        assertEquals(qn, qnExpected);
    }

    @Test
    public void testQuery_Like() throws SqlParserException {
        String sql = "SELECT NAME FROM TABLE1 WHERE NAME like '%XASX%'";
        Query qn = query(sql);
        qn.build();

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        tableQueryNode1.setSelectItemListAndSetNeedBuild("NAME");
        tableQueryNode1.setWhere("NAME like '%XASX%'");
        Query qnExpected = tableQueryNode1;
        qnExpected.build();
        assertEquals(qn, qnExpected);
    }

    @Test
    public void testMultiAnd() throws SqlParserException {
        String sql = "SELECT NAME FROM TABLE1 WHERE NAME=? and (ID>? and ID<?)";
        Query qn = query(sql);
        qn.build();

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        tableQueryNode1.setSelectItemListAndSetNeedBuild("NAME");
        tableQueryNode1.setWhere("NAME=? and ID>? and ID<?");
        Query qnExpected = tableQueryNode1;
        qnExpected.build();
        assertEquals(qn, qnExpected);
    }

    @Test
    public void testMultiOr() throws SqlParserException {
        String sql = "SELECT NAME FROM TABLE1 WHERE NAME=? or(ID>? or ID<?)";
        Query qn = query(sql);
        qn.build();

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        tableQueryNode1.setSelectItemListAndSetNeedBuild("NAME");
        tableQueryNode1.setWhere("NAME=? or(ID>? or ID<?)");
        Query qnExpected = tableQueryNode1;
        qnExpected.build();
        assertEquals(qn, qnExpected);
    }

    @Test
    public void testMultiAndOr() throws SqlParserException {
        String sql = "SELECT NAME FROM TABLE1 WHERE NAME=? and NAME>? and (ID=? or ID<?)";
        Query qn = query(sql);
        qn.build();

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        tableQueryNode1.setSelectItemListAndSetNeedBuild("NAME");
        tableQueryNode1.setWhere("NAME=? and NAME>? and (ID=? or ID<?)");
        Query qnExpected = tableQueryNode1;
        qnExpected.build();
        assertEquals(qn, qnExpected);
    }

    @Test
    public void testWhere_字段子查询() throws SqlParserException {
        String sql = "SELECT NAME FROM TABLE1 WHERE NAME=(SELECT NAME FROM TABLE2 B WHERE B.ID=1)";
        Query qn = query(sql);
        qn.build();

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        tableQueryNode1.setSelectItemListAndSetNeedBuild("NAME");
        Query qnExpected = tableQueryNode1;

        TableQuery tableQueryNode2 = new TableQuery("TABLE2");
        tableQueryNode2.setAliasAndSetNeedBuild("B");
        tableQueryNode2.setSelectItemListAndSetNeedBuild("NAME");
        tableQueryNode2.setWhere("B.ID=1");
        tableQueryNode1.setWhere("NAME=(SELECT NAME FROM TABLE2 B WHERE B.ID=1)");
        qnExpected.build();
        assertEquals(qn, qnExpected);
    }

    @Test
    public void testWhere_字段多级子查询() throws SqlParserException {
        String subSql = "SELECT B.* FROM TABLE2 B WHERE B.ID=1 GROUP BY SCHOOL HAVING count(*) > 1 ORDER BY ID desc LIMIT 1";
        String sql = "SELECT NAME FROM TABLE1 WHERE NAME=(SELECT C.NAME FROM (" + subSql + ") C )";
        Query qn = query(sql);
        qn.build();

        System.out.println(qn);
        //
        // TableQuery table1 = new TableQuery("TABLE1");
        // Query qnExpected = table1.selectStatement("NAME");
        //
        // TableQuery table2 = new TableQuery("TABLE2");
        // table2.setAliasAndSetNeedBuild("C").setTableSubAliasAndSetNeedBuild("B").selectStatement("*").setWhereAndSetNeedBuild("B.ID=1");
        // table2.addGroupByItem("SCHOOL");
        // table2.having("count(*) > 1");
        // table2.addOrderByItemAndSetNeedBuild("ID", false);
        // table2.limit(0, 1);
        // table2.setSubQueryAndSetNeedBuild(true);
        //
        // $Query$ subQuery = new $Query$(table2);
        // subQuery.selectStatement("C.NAME");
        //
        // Column column =
        // ObjectCreateFactorygetSequenceManager().createColumn().setColumnName("NAME");
        // BooleanFilter filter = ObjectCreateFactorygetSequenceManager()
        // .createBooleanFilter()
        // .setColumn(column)
        // .setValue(subQuery)
        // .setOperation(Operation.equal);
        // table1.setWhereAndSetNeedBuild(filter);
        // qnExpected.build();
        // assertEquals(qn, qnExpected);
    }

    @Test
    public void testWhere_表子查询() throws SqlParserException {
        String sql = "SELECT NAME FROM (SELECT * FROM TABLE1 A WHERE A.ID=1) B";
        Query qn = query(sql);
        qn.build();

        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        tableQueryNode1.setSubAliasAndSetNeedBuild("A");
        tableQueryNode1.setAliasAndSetNeedBuild("B");
        tableQueryNode1.setWhere("A.ID=1");
        Query qnExpected = new $Query$(tableQueryNode1);
        qnExpected.setSelectItemListAndSetNeedBuild("NAME");
        qnExpected.build();
        assertEquals(qn, qnExpected);
    }

    @Test
    public void testWhere_字段_表_复杂子查询() throws SqlParserException {
        String subSql = "SELECT B.* FROM TABLE2 B WHERE B.ID=1 GROUP BY SCHOOL HAVING count(*) > 1 ORDER BY ID desc LIMIT 1";
        String sql = "SELECT NAME FROM (SELECT * FROM TABLE1 A WHERE A.ID=1) A WHERE NAME=(SELECT C.NAME FROM (" + subSql + ") C )";
        Query qn = query(sql);
        qn.build();

        System.out.println(qn);
    }

    @Test
    public void testQuery_join子查询_join表() throws SqlParserException {
        String sql = "SELECT * FROM (SELECT A.ID,A.NAME FROM TABLE1 A JOIN TABLE2 B ON A.ID=B.ID WHERE A.NAME=1) C JOIN TABLE3 D ON C.ID = D.ID";
        Query qn = query(sql);
        qn.build();

        // System.out.println(qn);
        Assert.assertTrue(((Join) qn).getLeftNode() instanceof Join);
    }

    @Test
    public void testQuery_join子查询() throws SqlParserException {
        String sql = "SELECT * FROM (SELECT A.ID,A.NAME FROM TABLE1 A JOIN TABLE2 B ON A.ID=B.ID WHERE A.NAME=1) C WHERE C.ID = 6";
        Query qn = query(sql);
        qn.build();

        // System.out.println(qn);
        // 第一级是QueryNode
        // 第二级是JoinNode
        Assert.assertTrue(qn instanceof $Query$);
        Assert.assertTrue((($Query$) qn).getFirstSubNodeQueryNode() instanceof Join);
    }

    @Test
    public void testQuery_join_子查询_多级组合() throws SqlParserException {
        String joinSql = "SELECT TABLE1.ID,TABLE1.NAME FROM TABLE1 JOIN TABLE2 ON TABLE1.ID=TABLE1.ID WHERE TABLE1.NAME=1";
        String subsql = "SELECT * FROM (" + joinSql + " ) S WHERE S.NAME = 1";
        String sql = "SELECT * FROM (" + subsql + ") B , (" + subsql + ") C WHERE B.NAME = 6 and B.ID = C.ID";
        Query qn = query(sql);
        qn.build();

        // System.out.println(qn);
        // 第一级是JoinNode
        // 第二级是QuerNode / $Query$
        // 第三级是JoinNode / Join
        Assert.assertTrue(qn instanceof Join);
        Assert.assertTrue(((Join) qn).getLeftNode() instanceof $Query$);
        Assert.assertTrue(((Join) qn).getRightNode() instanceof $Query$);
        Assert.assertTrue((($Query$) ((Join) qn).getLeftNode()).getFirstSubNodeQueryNode() instanceof Join);
        Assert.assertTrue((($Query$) ((Join) qn).getRightNode()).getFirstSubNodeQueryNode() instanceof Join);
    }

    @Test
    public void testQuery_多字段in() throws SqlParserException {
        String sql = "SELECT * FROM TABLE1 WHERE (ID,NAME) in ((1,2),(2,3))";
        Query qn = query(sql);
        qn.build();
        System.out.println(qn);
    }

    @Test
    public void testQuery_子查询_in模式() throws SqlParserException {
        String sql = "SELECT * FROM TABLE1 WHERE ID in (SELECT ID FROM TABLE2 WHERE TABLE2.NAME = TABLE1.NAME)";
        Query qn = query(sql);
        qn.build();
        System.out.println(qn);
    }

    @Test
    public void testQuery_子查询_多字段in模式() throws SqlParserException {
        String sql = "SELECT * FROM TABLE1 WHERE (ID,NAME) in (SELECT ID,NAME FROM TABLE2 WHERE TABLE2.NAME = TABLE1.NAME)";
        Query qn = query(sql);
        qn.build();
        System.out.println(qn);
    }

    @Test
    public void testQuery_子查询_not_in模式() throws SqlParserException {
        String sql = "SELECT * FROM TABLE1 WHERE ID NOT in (SELECT ID FROM TABLE2 WHERE TABLE2.NAME = TABLE1.NAME)";
        Query qn = query(sql);
        qn.build();
        System.out.println(qn);
    }

    @Test
    public void testQuery_子查询correlated_in模式() throws SqlParserException {
        String sql = "SELECT * FROM TABLE1 WHERE ID in (SELECT ID FROM TABLE2 WHERE TABLE2.NAME = TABLE1.NAME)";
        Query qn = query(sql);
        qn.build();
        System.out.println(qn);
    }

    @Test
    public void testQuery_子查询_exist模式() throws SqlParserException {
        String sql = "SELECT * FROM TABLE1 WHERE  exists (SELECT ID FROM TABLE2)";
        Query qn = query(sql);
        qn.build();
        System.out.println(qn);
    }

    @Test
    public void testQuery_子查询_not_exist模式() throws SqlParserException {
        String sql = "SELECT * FROM TABLE1 WHERE NOT exists (SELECT ID FROM TABLE2 WHERE TABLE2.NAME = TABLE1.NAME)";
        Query qn = query(sql);
        qn.build();
        System.out.println(qn);
    }

    @Test
    public void testQuery_子查询_all模式() throws SqlParserException {
        // SubQueryAll
        String sql = "SELECT * FROM TABLE1 WHERE ID > ALL (SELECT ID FROM TABLE2 WHERE TABLE2.NAME = TABLE1.NAME)";
        Query qn = query(sql);
        qn.build();
        System.out.println(qn);
    }

    @Test
    public void testQuery_子查询_any模式() throws SqlParserException {
        // SubQueryAny
        String sql = "SELECT * FROM TABLE1 WHERE ID <= ANY (SELECT ID FROM TABLE2 WHERE TABLE2.NAME = TABLE1.NAME)";
        Query qn = query(sql);
        qn.build();
        System.out.println(qn);
    }

    @Test
    public void testQuery_不带表() throws SqlParserException {
        String sql = "SELECT last_insert_id()";
        Query qn = query(sql);
        System.out.println(qn);
    }

    @Test
    public void testQuery_subquery_不带表() throws SqlParserException {
        String sql = "SELECT (SELECT max(ID) FROM TABLE2)";
        Query qn = query(sql);
        Assert.assertTrue(qn.getSql() != null);
    }

    @Test
    public void testQuery_addDate() throws SqlParserException {
        String sql = "SELECT DATE_ADD('2008-01-02', interval_primary 1 MONTH ) FROM  TABLE1";
        Query qn = query(sql);
        System.out.println(qn);
    }

    @Test
    public void test_sequence() throws SqlParserException {
        String qsql = "SELECT SEQ_TABLE1.NEXTVAL FROM DUAL";
        Query qn = query(qsql);
        System.out.println(qn);

        String sql = "insert INTO TABLE1(ID) VALUES (SEQ_TABLE1.NEXTVAL)";
        Insert in = insert(sql);
        in.build();
        System.out.println(in);

        sql = "CREATE SEQUENCE SEQ_TABLE2 START WITH 1000";
        ParseInfo sm = sqlParseManager.parse(sql, false);
        System.out.println(sm.getSql());
    }

    @Test
    public void test_insertSelect() throws SqlParserException {
        String sql = "insert INTO AUTOINC SELECT NULL,NAME,SCHOOL FROM TABLE1 WHERE ID > 100";
        try {
            Insert in = insert(sql);
            in.build();
            System.out.println(in);
            Assert.fail();
        } catch (Exception e) {
        }
    }

    @Test
    public void test_groupFilter() throws SqlParserException {
        String sql = "SELECT * FROM TABLE1 WHERE (ID = 1 or ID = 2 or ID is NULL) and NAME = 'ljh'";
        Query qn = query(sql);
        System.out.println(qn);
    }

    @Test
    public void test_error() throws SqlParserException {
        // String chars = "'";
        // Insert in = insert(chars);
        // in.build();
        // System.out.println(in);
    }

    // ==================================================

    private Query query(String sql) throws SqlParserException {
        ParseInfo parseInfo = sqlParseManager.parse(sql, false);
        Query query = null;
        if (parseInfo.isAbstractSyntaxTreeNode()) {
            query = parseInfo.getQueryNode();
        } else {
            query = new TableQueryWithIndex(null);
            query.setSql(sql);
        }
        return query;
    }

    private Query query(String sql, List args) throws SqlParserException {
        ParseInfo sm = sqlParseManager.parse(sql, false);
        Query qn = null;
        if (sm.isAbstractSyntaxTreeNode()) {
            qn = sm.getQueryNode();
        } else {
            qn = new TableQueryWithIndex(null);
            qn.setSql(sql);
        }
        return qn;
    }

    private Update update(String sql) throws SqlParserException {
        ParseInfo sm = sqlParseManager.parse(sql, false);
        Update qn = null;
        if (sm.getSqlType() == SqlType.UPDATE) {
            qn = sm.getUpdateNode();
        }

        return qn;
    }

    private Delete delete(String sql) throws SqlParserException {
        ParseInfo sm = sqlParseManager.parse(sql, false);
        Delete qn = null;
        if (sm.getSqlType() == SqlType.DELETE) {
            qn = sm.getDeleteNode();
        }

        return qn;
    }

    private Insert insert(String sql) throws SqlParserException {
        ParseInfo sm = sqlParseManager.parse(sql, false);
        Insert qn = null;
        if (sm.getSqlType() == SqlType.INSERT) {
            qn = sm.getInsertNode();
        }

        return qn;
    }

    private Replace put(String sql) throws SqlParserException {
        ParseInfo sm = sqlParseManager.parse(sql, false);
        Replace qn = null;
        if (sm.getSqlType() == SqlType.REPLACE) {
            qn = sm.getReplaceNode();
        }

        return qn;
    }

    private void assertEquals(Query qn, Query qnExpected) {
        // log.debug(qn.toString());
        Assert.assertEquals(qnExpected.toString(), qn.toString());
    }

    private void assertEquals(Update un, Update unExpected) {
        // log.debug(un.toString());
        Assert.assertEquals(unExpected.toString(), un.toString());
    }

    private void assertEquals(Delete dn, Delete dnExpected) {
        // log.debug(dn.toString());
        Assert.assertEquals(dnExpected.toString(), dn.toString());
    }

    private void assertEquals(Insert in, Insert inExpected) {
        // log.debug(in.toString());
        Assert.assertEquals(inExpected.toString(), in.toString());
    }

    private void assertEquals(Replace in, Replace inExpected) {
        // log.debug(in.toString());
        Assert.assertEquals(inExpected.toString(), in.toString());
    }
}
