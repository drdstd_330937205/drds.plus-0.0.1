package drds.plus.sql_process.parser;

import drds.plus.parser.SqlParser;
import drds.plus.parser.abstract_syntax_tree.statement.Statement;
import drds.plus.sql_process.BaseOptimizerTest;
import drds.plus.sql_process.parser.visitor.TableNameVisitor;
import org.junit.Assert;
import org.junit.Test;

import java.sql.SQLSyntaxErrorException;

/**
 * 测试下表名提取
 *
 * @author jianghang 2014-3-12 上午10:39:35
 * @since 5.0.0
 */
public class TableParserTest extends BaseOptimizerTest {

    @Test
    public void testInert() throws SQLSyntaxErrorException {
        String sql = "insert into sc.table1 columnValueList(?,?,?,?)";
        Assert.assertEquals("TABLE1 ", buildTableNames(sql));

        sql = "insert into table1 query * from table2";
        Assert.assertEquals("TABLE1 TABLE2 ", buildTableNames(sql));

        sql = "insert into table1 columnValueList(?,?,?,?) on duplicate columnName update col1 = ?";
        Assert.assertEquals("TABLE1 ", buildTableNames(sql));

        sql = "replace into sc.table1 columnValueList(?,?,?,?)";
        Assert.assertEquals("TABLE1 ", buildTableNames(sql));
    }

    @Test
    public void testUpdate() throws SQLSyntaxErrorException {
        String sql = "update table2 set col1=?,col2=? where col3=?";
        Assert.assertEquals("TABLE2 ", buildTableNames(sql));

        sql = "update table2 set col1=?,col2=? where id=(query id from table3)";
        Assert.assertEquals("TABLE3 TABLE2 ", buildTableNames(sql));

        sql = "update table2 set col1=?,col2=? where id=(query id from (query id from table4 limit 1) as a join table3 on table3.id = a.id)";
        Assert.assertEquals("TABLE3 TABLE2 TABLE4 ", buildTableNames(sql));
    }

    @Test
    public void testDelete() throws SQLSyntaxErrorException {
        String sql = "delete from sc.table3.* where id=(query id from table4)";
        Assert.assertEquals("TABLE3 TABLE4 ", buildTableNames(sql));
    }

    @Test
    public void testSelect() throws SQLSyntaxErrorException {
        String sql = "query * from table2,table3";
        Assert.assertEquals("TABLE3 TABLE2 ", buildTableNames(sql));

        sql = "query * from table2,(query * from table3) as t";
        Assert.assertEquals("TABLE3 TABLE2 ", buildTableNames(sql));

        sql = "query * from table2 t,table3 where t.id=(query id from table3)";
        Assert.assertEquals("TABLE3 TABLE2 ", buildTableNames(sql));
    }

    @Test
    public void testShow() throws SQLSyntaxErrorException {
        String sql = "show create where table2";
        Assert.assertEquals("TABLE2 ", buildTableNames(sql));

        sql = "show columnNameList from table2";
        Assert.assertEquals("TABLE2 ", buildTableNames(sql));
    }

    private String buildTableNames(String sql) throws SQLSyntaxErrorException {
        Statement statement = SqlParser.parse(sql);
        TableNameVisitor visitor = new TableNameVisitor();
        statement.accept(visitor);

        StringBuilder result = new StringBuilder();
        for (String table : visitor.getTableNameSet()) {
            result.append(table + " ");
        }
        return result.toString();
    }

}
