package drds.plus.sql_process.parser;

import com.alibaba.fastjson.JSON;
import drds.plus.common.model.hint.DirectlyRouteCondition;
import drds.plus.common.model.hint.ExtraCmdRouteCondition;
import drds.plus.common.model.hint.RouteCondition;
import drds.plus.common.model.hint.RuleRouteCondition;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SimpleHintParserTest {

    @Test
    public void testParser() {
        Map<String, Object> map1 = new HashMap<String, Object>();
        map1.put("relation", "and");
        map1.put("paramtype", "int");
        List<String> exprs = new ArrayList<String>();
        exprs.add("pk>4");
        exprs.add("pk<10");
        map1.put("expression_list", JSON.toJSON(exprs));
        List<Object> paramsO = new ArrayList<Object>();
        paramsO.add(JSON.toJSON(map1));
        Map<String, Object> hintmap = new HashMap<String, Object>();
        hintmap.put("columnToRuleColumnMap", JSON.toJSON(paramsO));
        hintmap.put("type", "condition");
        hintmap.put("vtab", "vtabxxx");
        String text = JSON.toJSONString(hintmap);
        System.out.println(text);

        RouteCondition route = HintParser.parse("/*+strict(" + text + ")*/");
        System.out.println(route);
    }

    @Test
    public void testDirect() {
        String sql = "/*+strict({'type':'direct','vtab':'real_tab','data_node_id':'xxx_group','real_table_names':['real_tab_0','real_tab_1']})*/query * from real_tab";
        DirectlyRouteCondition route = (DirectlyRouteCondition) HintParser.parse(sql);
        System.out.println(route);
        Assert.assertEquals("xxx_group", route.getDataNodeId());
        Assert.assertEquals(2, route.getRealTableNamesStringSet().size());
        Assert.assertEquals("real_tab", route.getVirtualTableNamesString());
    }

    @Test
    public void testCondition() {
        String sql = "/*+strict({'type':'condition','vtab':'vtabxxx','columnToRuleColumnMap':[{'relation':'and','expression_list':['pk>4','pk<10'],'paramtype':'int'}]})*/";
        RuleRouteCondition route = (RuleRouteCondition) HintParser.parse(sql);
        System.out.println(route);
        Assert.assertEquals("vtabxxx", route.getVirtualTableNamesString());
        Assert.assertEquals("(>4) and (<10)", route.getColumnNameToComparativeMap().get("PK").toString());
    }

    @Test
    public void testExtraCmd() {
        String sql = "/*+strict({'extra':{'ChooseIndex':'true','ALLOW_TEMPORARY_TABLE':'true'}})*/query * from real_tab";
        ExtraCmdRouteCondition route = (ExtraCmdRouteCondition) HintParser.parse(sql);
        System.out.println(route);
        Assert.assertEquals("{CHOOSEINDEX=true, ALLOW_TEMPORARY_TABLE=true}", route.getExtraCmds().toString());
    }
}
