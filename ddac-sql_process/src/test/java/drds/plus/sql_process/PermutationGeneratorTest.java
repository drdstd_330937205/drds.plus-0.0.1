package drds.plus.sql_process;

import drds.plus.sql_process.optimizer.chooser.join.PermutationGenerator;

import java.util.ArrayList;
import java.util.List;

public class PermutationGeneratorTest {
    public static void main(String[] args) {
        List<String> list = new ArrayList<String>();
        for (int i = 1; i <= 6; i++) {
            list.add(i + "");
        }
        PermutationGenerator permutationGenerator = new PermutationGenerator(list);
        while (permutationGenerator.hasNext()) {
            System.out.println(permutationGenerator.next());
        }
    }
}
