package drds.plus.sql_process;


import drds.plus.common.jdbc.Parameters;
import drds.plus.common.jdbc.SetParameterMethod;
import drds.plus.common.jdbc.SetParameterMethodAndArgs;
import drds.plus.common.model.Application;
import drds.plus.rule_engine.Route;
import drds.plus.sql_process.abstract_syntax_tree.configuration.manager.RepositorySchemaManager;
import drds.plus.sql_process.abstract_syntax_tree.configuration.manager.StaticSchemaManager;
import drds.plus.sql_process.abstract_syntax_tree.configuration.parse.ApplicationParser;
import drds.plus.sql_process.optimizer.Optimizer;
import drds.plus.sql_process.optimizer.OptimizerContext;
import drds.plus.sql_process.optimizer.cost_esitimater.statistics.LocalStatisticsManager;
import drds.plus.sql_process.optimizer.cost_esitimater.statistics.StatisticsManager;
import drds.plus.sql_process.parser.SqlParseManager;
import drds.plus.sql_process.parser.SqlParseManagerImpl;
import drds.plus.sql_process.rule.IndexManagerImpl;
import drds.plus.sql_process.rule.RouteOptimizer;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Ignore
public class BaseOptimizerTest {

    protected static final String APPNAME = "tddl";
    protected static final String table_file = "config/test_table.xml";
    protected static final String matrix_file = "config/test_matrix.xml";
    protected static final String rule_file = "config/test_rule.xml";
    protected static final String table_stat_file = "config/table_stat.xml";
    protected static final String table_index_stat_file = "config/kvIndex_stat.xml";

    protected static SqlParseManager sqlParseManager = new SqlParseManagerImpl();
    protected static RouteOptimizer routeOptimizer;
    protected static RepositorySchemaManager schemaManager;
    protected static Optimizer optimizer;
    protected static StatisticsManager statisticsManager;

    @BeforeClass
    public static void initial() {
        sqlParseManager.init();

        OptimizerContext optimizerContext = new OptimizerContext();
        Route route = new Route();
        route.setApplicationId(APPNAME);
        route.init();

        routeOptimizer = new RouteOptimizer(route);

        StaticSchemaManager localSchemaManager = StaticSchemaManager.parseSchema(Thread.currentThread().getContextClassLoader().getResourceAsStream(table_file));

        Application application = ApplicationParser.parse(Thread.currentThread().getContextClassLoader().getResourceAsStream(matrix_file));

        schemaManager = new RepositorySchemaManager();
        schemaManager.setLocal(localSchemaManager);
        schemaManager.setDataNode(application.getDataNode("andor_group_0"));
        schemaManager.init();

        statisticsManager = LocalStatisticsManager.parseConfig("", null);

        // statisticsManager = new RepositoryStatisticsManager();
        // statisticsManager.setLocalStatisticsManager(local);
        // statisticsManager.setUseCache(true);
        // statisticsManager.setDataNode(plus.getGroupBy("andor_group_0"));
        // statisticsManager.init();

        optimizerContext.setApplication(application);
        optimizerContext.setRouteOptimizer(routeOptimizer);
        optimizerContext.setSchemaManager(schemaManager);
        optimizerContext.setStatisticsManager(statisticsManager);
        optimizerContext.setIndexManager(new IndexManagerImpl(schemaManager));

        OptimizerContext.setOptimizerContext(optimizerContext);

        optimizer = new Optimizer(routeOptimizer);
        optimizer.setSqlParseManager(sqlParseManager);
        optimizer.init();
    }

    @AfterClass
    public static void tearDown() {
        schemaManager.destroy();
        statisticsManager.destroy();
        sqlParseManager.destroy();
        optimizer.destroy();
    }

    protected Parameters convert(List<Object> args) {
        Map<Integer, SetParameterMethodAndArgs> map = new HashMap<Integer, SetParameterMethodAndArgs>(args.size());
        int index = 1;
        for (Object obj : args) {
            SetParameterMethodAndArgs context = new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{index, obj});
            map.put(index, context);
            index++;
        }
        return new Parameters(map, false);
    }

    protected Parameters convert(Object[] args) {
        return convert(Arrays.asList(args));
    }

}
