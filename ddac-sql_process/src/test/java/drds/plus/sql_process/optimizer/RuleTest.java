package drds.plus.sql_process.optimizer;

import drds.plus.sql_process.BaseOptimizerTest;
import drds.plus.sql_process.abstract_syntax_tree.execute_plan.ExecutePlan;
import drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.MergeQuery;
import drds.plus.sql_process.parser.SqlParserException;
import org.junit.Assert;
import org.junit.Test;

public class RuleTest extends BaseOptimizerTest {

    @Test
    public void testQueryNoCondition() throws SqlParserException {
        String sql = "SELECT S.ID AS ID1 , T.ID AS ID2 FROM STUDENT S JOIN STUDENT T ON S.ID=T.ID and S.ID= ? and T.SCHOOL= ?";
        // extraCmd.add(ExtraCmd.JoinMergeJoin, "true");
        ExecutePlan qc1 = optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, convert(new Integer[]{1, 3}), null, false);
        Assert.assertEquals(qc1.getSql(), sql);
    }

    @Test
    public void testSinleDb() {
        String sql = "SELECT * FROM STUDENT WHERE ID = ?";
        ExecutePlan qc1 = optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, convert(new Integer[]{1, 3}), null, false);
        Assert.assertEquals(qc1.getSql(), sql);
    }

    @Test
    public void test一张表为单库_另一张表为分库() {
        String sql = "SELECT * FROM STUDENT s , TABLE1 t WHERE s.ID = ? and s.ID = t.ID";
        ExecutePlan qc1 = optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, convert(new Integer[]{1, 3}), null, false);
        Assert.assertEquals(qc1.getSql(), null);
    }

    @Test
    public void test_字段为null值() {
        String sql = "SELECT * FROM TABLE1 WHERE ID = ?";
        ExecutePlan qc1 = optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, convert(new Object[]{null}), null, false);
        Assert.assertTrue(qc1 instanceof MergeQuery);
    }

}
