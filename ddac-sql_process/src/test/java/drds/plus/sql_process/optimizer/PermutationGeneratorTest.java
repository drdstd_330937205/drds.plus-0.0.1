package drds.plus.sql_process.optimizer;

import ch.qos.logback.LogContext;
import drds.plus.sql_process.optimizer.chooser.join.PermutationGenerator;
import drds.tools.Constants;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class PermutationGeneratorTest {

    @Test
    public void testNext() {
        Constants.developMode = true;
        LogContext.configure("C:\\Users\\ibm\\Desktop\\plus-2.1.07\\tddl-sql_process\\src\\nonspi\\resources\\logback.xml");
        List<Integer> list = new ArrayList<Integer>();

        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);
        PermutationGenerator instance = new PermutationGenerator(list);

        while (instance.hasNext()) {
            List result = instance.next();
            System.out.println(result);
        }

    }
}
