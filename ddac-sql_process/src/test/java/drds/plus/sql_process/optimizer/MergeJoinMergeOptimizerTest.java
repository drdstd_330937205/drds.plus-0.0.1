package drds.plus.sql_process.optimizer;

import drds.plus.sql_process.BaseOptimizerTest;
import drds.plus.sql_process.abstract_syntax_tree.ObjectCreateFactory;
import drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.Join;
import drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.MergeQuery;
import drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.Query;
import drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.QueryWithIndex;
import drds.plus.sql_process.optimizer.execute_plan_optimizer.MergeJoinExpandOptimizer;
import org.junit.Test;

public class MergeJoinMergeOptimizerTest extends BaseOptimizerTest {

    private MergeJoinExpandOptimizer o = new MergeJoinExpandOptimizer();

    @Test
    public void testExpandLeft() {
        System.out.println("==========testExpandLeft==============");
        Join j = this.getMergeJoinMerge();
        System.out.println(j);
        Query res = o.expandLeft(j, null);
        System.out.println(res);
        System.out.println("========================");
        j = this.getMergeJoinQuery();
        System.out.println(j);
        res = o.expandLeft(j, null);
        System.out.println(res);
        System.out.println("========================");
        j = this.getQueryJoinMerge();
        System.out.println(j);
        res = o.expandLeft(j, null);
        System.out.println(res);
        System.out.println("========================");
        j = this.getQueryJoinQuery();
        System.out.println(j);
        res = o.expandLeft(j, null);
        System.out.println(res);
        System.out.println("========================");
    }

    @Test
    public void testExpandRight() {
        System.out.println("==========testExpandRight==============");
        Join j = this.getMergeJoinMerge();
        System.out.println(j);
        Query res = o.expandRight(j, null);
        System.out.println(res);
        System.out.println("========================");
        j = this.getMergeJoinQuery();
        System.out.println(j);
        res = o.expandRight(j, null);
        System.out.println(res);
        System.out.println("========================");
        j = this.getQueryJoinMerge();
        System.out.println(j);
        res = o.expandRight(j, null);
        System.out.println(res);
        System.out.println("========================");
        j = this.getQueryJoinQuery();
        System.out.println(j);
        res = o.expandRight(j, null);
        System.out.println(res);
        System.out.println("========================");
    }

    @Test
    public void testCartesianProduct() {
        System.out.println("==========testCartesianProduct==============");
        Join j = this.getMergeJoinMerge();
        System.out.println(j);
        Query res = o.cartesianProduct(j, null);
        System.out.println(res);
        System.out.println("========================");
        j = this.getMergeJoinQuery();
        System.out.println(j);
        res = o.cartesianProduct(j, null);
        System.out.println(res);
        System.out.println("========================");
        j = this.getQueryJoinMerge();
        System.out.println(j);
        res = o.cartesianProduct(j, null);
        System.out.println(res);
        System.out.println("========================");
        j = this.getQueryJoinQuery();
        System.out.println(j);
        res = o.cartesianProduct(j, null);
        System.out.println(res);
        System.out.println("========================");
    }

    private Join getMergeJoinMerge() {
        Join j = this.getJoin();
        j.setLeftOuterJoin(true);
        j.setRightOuterJoin(true);
        QueryWithIndex q1 = this.getQuery(1);
        QueryWithIndex q2 = this.getQuery(2);
        QueryWithIndex q3 = this.getQuery(3);
        QueryWithIndex q4 = this.getQuery(4);
        QueryWithIndex q5 = this.getQuery(5);
        QueryWithIndex q6 = this.getQuery(6);

        MergeQuery leftMergeQuery = this.getMerge(7);
        MergeQuery rightMergeQuery = this.getMerge(8);

        leftMergeQuery.addExecutePlan(q1);
        leftMergeQuery.addExecutePlan(q2);
        leftMergeQuery.addExecutePlan(q3);
        rightMergeQuery.addExecutePlan(q4);
        rightMergeQuery.addExecutePlan(q5);
        rightMergeQuery.addExecutePlan(q6);

        j.setLeftNode(leftMergeQuery);
        j.setRightNode(rightMergeQuery);
        return j;
    }

    private Join getQueryJoinMerge() {
        Join j = this.getJoin();
        j.setLeftOuterJoin(true);
        j.setRightOuterJoin(true);
        QueryWithIndex q1 = this.getQuery(1);
        QueryWithIndex q4 = this.getQuery(4);
        QueryWithIndex q5 = this.getQuery(5);
        QueryWithIndex q6 = this.getQuery(6);

        MergeQuery rightMergeQuery = this.getMerge(8);
        rightMergeQuery.addExecutePlan(q4);
        rightMergeQuery.addExecutePlan(q5);
        rightMergeQuery.addExecutePlan(q6);

        j.setLeftNode(q1);
        j.setRightNode(rightMergeQuery);
        return j;
    }

    private Join getMergeJoinQuery() {
        Join j = this.getJoin();
        j.setLeftOuterJoin(true);
        j.setRightOuterJoin(true);
        QueryWithIndex q1 = this.getQuery(1);
        QueryWithIndex q2 = this.getQuery(2);
        QueryWithIndex q3 = this.getQuery(3);
        QueryWithIndex q4 = this.getQuery(4);

        MergeQuery leftMergeQuery = this.getMerge(7);
        leftMergeQuery.addExecutePlan(q1);
        leftMergeQuery.addExecutePlan(q2);
        leftMergeQuery.addExecutePlan(q3);

        j.setLeftNode(leftMergeQuery);
        j.setRightNode(q4);
        return j;
    }

    private Join getQueryJoinQuery() {
        Join j = this.getJoin();
        j.setLeftOuterJoin(true);
        j.setRightOuterJoin(true);
        QueryWithIndex q1 = this.getQuery(1);
        QueryWithIndex q4 = this.getQuery(4);

        j.setLeftNode(q1);
        j.setRightNode(q4);
        return j;
    }

    private QueryWithIndex getQuery(Integer id) {
        QueryWithIndex q = ObjectCreateFactory.createQueryWithIndex();
        q.setAlias(id.toString());
        return q;
    }

    private MergeQuery getMerge(Integer id) {
        MergeQuery m = ObjectCreateFactory.createMergeQuery();
        m.setAlias(id.toString());
        return m;
    }

    private Join getJoin() {
        return ObjectCreateFactory.createJoin();
    }
}
