package drds.plus.sql_process.optimizer;

import drds.plus.sql_process.BaseOptimizerTest;
import drds.plus.sql_process.abstract_syntax_tree.ObjectCreateFactory;
import drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.JoinStrategy;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.column.Column;
import drds.plus.sql_process.abstract_syntax_tree.node.query.*;
import drds.plus.sql_process.optimizer.pusher.OrderByPusher;
import org.junit.Assert;
import org.junit.Test;

public class OrderByPusherTest extends BaseOptimizerTest {

    @Test
    public void test_order条件下推_子表_case1_下推NAME() {
        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        tableQueryNode1.setAliasAndSetNeedBuild("A");
        tableQueryNode1.addOrderByItemAndSetNeedBuild("ID");

        $Query$ query = new $Query$(tableQueryNode1);
        query.addOrderByItemAndSetNeedBuild("A.ID");
        query.addOrderByItemAndSetNeedBuild("A.NAME");
        query.build();

        OrderByPusher.optimize(query);

        Assert.assertEquals(2, tableQueryNode1.getOrderByList().size());
        Assert.assertEquals("TABLE1.ID", tableQueryNode1.getOrderByList().get(0).getItem().toString());
        Assert.assertEquals("TABLE1.NAME", tableQueryNode1.getOrderByList().get(1).getItem().toString());
    }

    @Test
    public void test_order条件下推_子表_case2_强制下推() {
        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        tableQueryNode1.setAliasAndSetNeedBuild("A");
        tableQueryNode1.addOrderByItemAndSetNeedBuild("ID");
        tableQueryNode1.addOrderByItemAndSetNeedBuild("NAME");

        $Query$ query = new $Query$(tableQueryNode1);
        query.addOrderByItemAndSetNeedBuild("A.NAME");
        query.addOrderByItemAndSetNeedBuild("A.SCHOOL");
        query.build();

        OrderByPusher.optimize(query);

        Assert.assertEquals(2, tableQueryNode1.getOrderByList().size());
        Assert.assertEquals("TABLE1.NAME", tableQueryNode1.getOrderByList().get(0).getItem().toString());
        Assert.assertEquals("TABLE1.SCHOOL", tableQueryNode1.getOrderByList().get(1).getItem().toString());
    }

    @Test
    public void test_order条件下推_子表_case3_不下推() {
        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        tableQueryNode1.setAliasAndSetNeedBuild("A");
        tableQueryNode1.addOrderByItemAndSetNeedBuild("ID");
        tableQueryNode1.addOrderByItemAndSetNeedBuild("NAME");
        tableQueryNode1.limit(0, 10);

        $Query$ query = new $Query$(tableQueryNode1);
        query.addOrderByItemAndSetNeedBuild("A.NAME");
        query.addOrderByItemAndSetNeedBuild("A.SCHOOL");
        query.build();

        OrderByPusher.optimize(query);

        Assert.assertEquals(2, tableQueryNode1.getOrderByList().size());
        Assert.assertEquals("TABLE1.ID", tableQueryNode1.getOrderByList().get(0).getItem().toString());
        Assert.assertEquals("TABLE1.NAME", tableQueryNode1.getOrderByList().get(1).getItem().toString());
    }

    @Test
    public void test_order条件下推_子表_case4_下推IDNAME() {
        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        tableQueryNode1.setAliasAndSetNeedBuild("A");

        $Query$ query = new $Query$(tableQueryNode1);
        query.addOrderByItemAndSetNeedBuild("A.ID");
        query.addOrderByItemAndSetNeedBuild("A.NAME");
        query.build();

        OrderByPusher.optimize(query);

        Assert.assertEquals(2, query.getOrderByList().size());
        Assert.assertEquals("TABLE1.ID", tableQueryNode1.getOrderByList().get(0).getItem().toString());
        Assert.assertEquals("TABLE1.NAME", tableQueryNode1.getOrderByList().get(1).getItem().toString());
    }

    @Test
    public void test_order条件下推_子表_case5_不下推函数() {
        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        tableQueryNode1.setAliasAndSetNeedBuild("A");

        $Query$ query = new $Query$(tableQueryNode1);
        query.setSelectItemListAndSetNeedBuild("ID AS CID, (NAME+SCHOOL) AS NAME");
        query.addOrderByItemAndSetNeedBuild("A.CID ");
        query.addOrderByItemAndSetNeedBuild("A.NAME"); // 这里的name为select中的函数
        query.build();

        OrderByPusher.optimize(query);

        Assert.assertEquals(0, tableQueryNode1.getOrderByList().size());
    }

    @Test
    public void test_join条件下推_子表_case1_下推NAME() {
        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        TableQuery tableQueryNode2 = new TableQuery("TABLE2");
        tableQueryNode1.setAliasAndSetNeedBuild("A");
        tableQueryNode1.addOrderByItemAndSetNeedBuild("ID");
        tableQueryNode2.setAliasAndSetNeedBuild("B");

        Join join = tableQueryNode1.join(tableQueryNode2);
        join.setJoinStrategy(JoinStrategy.index_nest_loop_join);
        join.addOrderByItemAndSetNeedBuild("A.ID");
        join.addOrderByItemAndSetNeedBuild("A.NAME");
        join.build();

        OrderByPusher.optimize(join);

        Assert.assertEquals(2, tableQueryNode1.getOrderByList().size());
        Assert.assertEquals("TABLE1.ID", tableQueryNode1.getOrderByList().get(0).getItem().toString());
        Assert.assertEquals("TABLE1.NAME", tableQueryNode1.getOrderByList().get(1).getItem().toString());
    }

    @Test
    public void test_join条件下推_子表_case2_强制下推() {
        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        TableQuery tableQueryNode2 = new TableQuery("TABLE2");
        tableQueryNode1.setAliasAndSetNeedBuild("A");
        tableQueryNode1.addOrderByItemAndSetNeedBuild("ID");
        tableQueryNode1.addOrderByItemAndSetNeedBuild("NAME");
        tableQueryNode2.setAliasAndSetNeedBuild("B");

        Join join = tableQueryNode1.join(tableQueryNode2);
        join.setJoinStrategy(JoinStrategy.index_nest_loop_join);
        join.addOrderByItemAndSetNeedBuild("A.NAME");
        join.addOrderByItemAndSetNeedBuild("A.SCHOOL");
        join.build();

        OrderByPusher.optimize(join);

        Assert.assertEquals(2, tableQueryNode1.getOrderByList().size());
        Assert.assertEquals("TABLE1.NAME", tableQueryNode1.getOrderByList().get(0).getItem().toString());
        Assert.assertEquals("TABLE1.SCHOOL", tableQueryNode1.getOrderByList().get(1).getItem().toString());
    }

    @Test
    public void test_join条件下推_子表_case3_不下推() {
        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        TableQuery tableQueryNode2 = new TableQuery("TABLE2");
        tableQueryNode1.setAliasAndSetNeedBuild("A");
        $Query$ query = new $Query$(tableQueryNode1);
        query.addOrderByItemAndSetNeedBuild("ID");
        query.addOrderByItemAndSetNeedBuild("NAME");
        query.limit(0, 10);

        tableQueryNode2.setAliasAndSetNeedBuild("B");

        Join join = query.join(tableQueryNode2);
        join.setJoinStrategy(JoinStrategy.index_nest_loop_join);
        join.addOrderByItemAndSetNeedBuild("A.NAME");
        join.addOrderByItemAndSetNeedBuild("A.SCHOOL");
        join.build();

        OrderByPusher.optimize(join);

        Assert.assertEquals(2, query.getOrderByList().size());
        Assert.assertEquals("A.ID", query.getOrderByList().get(0).getItem().toString());
        Assert.assertEquals("A.NAME", query.getOrderByList().get(1).getItem().toString());
    }

    @Test
    public void test_join条件下推_子表_case4_下推IDNAME() {
        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        TableQuery tableQueryNode2 = new TableQuery("TABLE2");
        tableQueryNode1.setAliasAndSetNeedBuild("A");
        tableQueryNode2.setAliasAndSetNeedBuild("B");

        Join join = tableQueryNode1.join(tableQueryNode2);
        join.setJoinStrategy(JoinStrategy.index_nest_loop_join);
        join.addOrderByItemAndSetNeedBuild("A.ID");
        join.addOrderByItemAndSetNeedBuild("A.NAME");
        join.build();

        OrderByPusher.optimize(join);

        Assert.assertEquals(2, tableQueryNode1.getOrderByList().size());
        Assert.assertEquals("TABLE1.ID", tableQueryNode1.getOrderByList().get(0).getItem().toString());
        Assert.assertEquals("TABLE1.NAME", tableQueryNode1.getOrderByList().get(1).getItem().toString());
    }

    @Test
    public void test_join条件下推_子表_case5_函数不下推() {
        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        TableQuery tableQueryNode2 = new TableQuery("TABLE2");
        tableQueryNode1.setAliasAndSetNeedBuild("A");
        tableQueryNode2.setAliasAndSetNeedBuild("B");

        Join join = tableQueryNode1.join(tableQueryNode2);
        join.setJoinStrategy(JoinStrategy.index_nest_loop_join);
        join.setSelectItemListAndSetNeedBuild("A.ID AS CID, (A.NAME + A.SCHOOL) AS NAME");
        join.addOrderByItemAndSetNeedBuild("CID ");
        join.addOrderByItemAndSetNeedBuild("NAME"); // 这里的name为select中的函数
        join.build();

        OrderByPusher.optimize(join);

        Assert.assertEquals(0, tableQueryNode1.getOrderByList().size());
    }

    @Test
    public void test_orderby多级结构下推() {
        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        TableQuery tableQueryNode2 = new TableQuery("TABLE2");

        Join join = tableQueryNode1.join(tableQueryNode2);
        join.setJoinStrategy(JoinStrategy.index_nest_loop_join);
        join.setAliasAndSetNeedBuild("S");
        join.setSelectItemListAndSetNeedBuild("TABLE1.ID AS ID , TABLE1.NAME AS NAME , TABLE2.SCHOOL AS SCHOOL");
        join.build();

        $Query$ queryA = new $Query$(join);
        queryA.setAliasAndSetNeedBuild("B");
        queryA.setSelectItemListAndSetNeedBuild("S.ID AS ID,S.NAME AS NAME");
        queryA.build();

        $Query$ queryB = queryA.deepCopy();
        queryB.setAliasAndSetNeedBuild("C");
        queryB.setSelectItemListAndSetNeedBuild("S.SCHOOL AS SCHOOL");
        queryB.build();

        Join nextJoin = queryA.join(queryB);
        nextJoin.setJoinStrategy(JoinStrategy.index_nest_loop_join);
        nextJoin.addOrderByItemAndSetNeedBuild("B.ID asc");
        nextJoin.addOrderByItemAndSetNeedBuild("B.NAME desc");
        nextJoin.build();

        OrderByPusher.optimize(nextJoin);

        // 最左节点会有两个order by pusher, ID和NAME
        Assert.assertEquals(2, tableQueryNode1.getOrderByList().size());
    }

    @Test
    public void test_orderby_SortMerge下推() {
        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        TableQuery tableQueryNode2 = new TableQuery("TABLE2");

        Join join = tableQueryNode1.join(tableQueryNode2);
        join.addJoinKeys("ID", "ID");
        join.addJoinKeys("NAME", "NAME");
        join.setFullOuterJoin();
        join.setJoinStrategy(JoinStrategy.sort_merge_join);
        join.setSelectItemListAndSetNeedBuild("TABLE1.ID AS ID , TABLE1.NAME AS NAME , TABLE1.SCHOOL AS SCHOOL");
        join.addGroupByItemAndSetNeedBuild("NAME");
        join.addGroupByItemAndSetNeedBuild("SCHOOL");
        join.addGroupByItemAndSetNeedBuild("ID"); // group by顺序可调整
        join.addOrderByItemAndSetNeedBuild("ID", false);
        join.build();

        OrderByPusher.optimize(join);
        // 推出来的结果：
        // 1. 按照join列先推，ID , NAME的排序
        // 2. 按照order by + group by的推成功，最后排序结果为ID,NAME,SCHOOL
        Assert.assertEquals("TABLE1.ID", join.getLeftNode().getOrderByList().get(0).getItem().toString());
        Assert.assertEquals(false, join.getLeftNode().getOrderByList().get(0).getAsc()); // 逆序
        Assert.assertEquals("TABLE1.NAME", join.getLeftNode().getOrderByList().get(1).getItem().toString());
        Assert.assertEquals("TABLE1.SCHOOL", join.getLeftNode().getOrderByList().get(2).getItem().toString());

        Assert.assertEquals("TABLE2.ID", join.getRightNode().getOrderByList().get(0).getItem().toString());
        Assert.assertEquals(false, join.getRightNode().getOrderByList().get(0).getAsc()); // 逆序
        Assert.assertEquals("TABLE2.NAME", join.getRightNode().getOrderByList().get(1).getItem().toString());

        Assert.assertEquals("TABLE1.ID setAliasAndSetNeedBuild ID", join.getGroupByList().get(0).getItem().toString());
        Assert.assertEquals("TABLE1.NAME setAliasAndSetNeedBuild NAME", join.getGroupByList().get(1).getItem().toString());
        Assert.assertEquals("TABLE1.SCHOOL setAliasAndSetNeedBuild SCHOOL", join.getGroupByList().get(2).getItem().toString());

        Assert.assertEquals("TABLE1.ID setAliasAndSetNeedBuild ID", join.getOrderByList().get(0).getItem().toString());
        Assert.assertEquals(false, join.getOrderByList().get(0).getAsc()); // 逆序
    }

    @Test
    public void test_orderby_SortMerge下推_只推group() {
        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        TableQuery tableQueryNode2 = new TableQuery("TABLE2");

        Join join = tableQueryNode1.join(tableQueryNode2);
        join.addJoinKeys("ID", "ID");
        join.addJoinKeys("NAME", "NAME");
        join.setFullOuterJoin();
        join.setJoinStrategy(JoinStrategy.sort_merge_join);
        join.setSelectItemListAndSetNeedBuild("TABLE1.ID AS ID , TABLE1.NAME AS NAME , TABLE1.SCHOOL AS SCHOOL");

        join.addGroupByItemAndSetNeedBuild("NAME");
        join.addGroupByItemAndSetNeedBuild("SCHOOL");
        join.addGroupByItemAndSetNeedBuild("ID"); // group by顺序可调整
        join.addOrderByItemAndSetNeedBuild("SCHOOL", false);
        join.build();

        OrderByPusher.optimize(join);
        // 推出来的结果：
        // 1. 按照join列先推，ID , NAME的排序
        // 2. 按照order by + group by的会推不成功，因为是按照school字段顺序
        // 3. 按照group by单独推会成功
        Assert.assertEquals("TABLE1.ID", join.getLeftNode().getOrderByList().get(0).getItem().toString());
        Assert.assertEquals("TABLE1.NAME", join.getLeftNode().getOrderByList().get(1).getItem().toString());
        Assert.assertEquals("TABLE1.SCHOOL", join.getLeftNode().getOrderByList().get(2).getItem().toString());
        Assert.assertEquals(false, join.getLeftNode().getOrderByList().get(2).getAsc()); // 逆序

        Assert.assertEquals("TABLE2.ID", join.getRightNode().getOrderByList().get(0).getItem().toString());
        Assert.assertEquals("TABLE2.NAME", join.getRightNode().getOrderByList().get(1).getItem().toString());

        Assert.assertEquals("TABLE1.ID setAliasAndSetNeedBuild ID", join.getGroupByList().get(0).getItem().toString());
        Assert.assertEquals("TABLE1.NAME setAliasAndSetNeedBuild NAME", join.getGroupByList().get(1).getItem().toString());
        Assert.assertEquals("TABLE1.SCHOOL setAliasAndSetNeedBuild SCHOOL", join.getGroupByList().get(2).getItem().toString());

        Assert.assertEquals("TABLE1.SCHOOL setAliasAndSetNeedBuild SCHOOL", join.getOrderByList().get(0).getItem().toString());
        Assert.assertEquals(false, join.getOrderByList().get(0).getAsc()); // 逆序
    }

    @Test
    public void test_orderby_SortMerge下推_调整join顺序() {
        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        TableQuery tableQueryNode2 = new TableQuery("TABLE2");

        Join join = tableQueryNode1.join(tableQueryNode2);
        join.addJoinKeys("ID", "ID");
        join.addJoinKeys("NAME", "NAME");
        join.setFullOuterJoin();
        join.setJoinStrategy(JoinStrategy.sort_merge_join);
        join.setSelectItemListAndSetNeedBuild("TABLE1.ID AS ID , TABLE1.NAME AS NAME , TABLE1.SCHOOL AS SCHOOL");
        join.addGroupByItemAndSetNeedBuild("NAME");
        join.addGroupByItemAndSetNeedBuild("SCHOOL"); // group by顺序可调整
        join.addOrderByItemAndSetNeedBuild("NAME", false);
        join.build();

        OrderByPusher.optimize(join);
        // 推出来的结果：
        // 1. 按照join列先推，ID , NAME的排序
        // 2. 按照order by + group by的会推不成功，因为是按照NAME+SCHOOL字段顺序
        Assert.assertEquals("TABLE1.NAME", join.getLeftNode().getOrderByList().get(0).getItem().toString());
        Assert.assertEquals("TABLE1.ID", join.getLeftNode().getOrderByList().get(1).getItem().toString());
        Assert.assertEquals(false, join.getLeftNode().getOrderByList().get(0).getAsc()); // 逆序

        Assert.assertEquals("TABLE2.NAME", join.getRightNode().getOrderByList().get(0).getItem().toString());
        Assert.assertEquals("TABLE2.ID", join.getRightNode().getOrderByList().get(1).getItem().toString());
        Assert.assertEquals(false, join.getRightNode().getOrderByList().get(0).getAsc()); // 逆序

        Assert.assertEquals("TABLE1.NAME setAliasAndSetNeedBuild NAME", join.getGroupByList().get(0).getItem().toString());
        Assert.assertEquals(false, join.getGroupByList().get(0).getAsc()); // 逆序
        Assert.assertEquals("TABLE1.SCHOOL setAliasAndSetNeedBuild SCHOOL", join.getGroupByList().get(1).getItem().toString());

        Assert.assertEquals("TABLE1.NAME setAliasAndSetNeedBuild NAME", join.getOrderByList().get(0).getItem().toString());
        Assert.assertEquals(false, join.getOrderByList().get(0).getAsc()); // 逆序
    }

    @Test
    public void test_orderby_SortMerge下推_多级结构下推() {
        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        TableQuery tableQueryNode2 = new TableQuery("TABLE2");

        // 如果底层join的顺序不是ID,NAME的顺序，暂时没法推，要递归做最优，算法太复杂，先简单只考虑一层
        Join join = tableQueryNode1.join(tableQueryNode2);
        join.addJoinKeys("ID", "ID");
        join.setJoinStrategy(JoinStrategy.sort_merge_join);
        join.setAliasAndSetNeedBuild("S");
        join.setSelectItemListAndSetNeedBuild("TABLE1.ID AS AID , TABLE1.NAME AS ANAME , TABLE1.SCHOOL AS ASCHOOL");
        join.build();

        $Query$ queryA = new $Query$(join);
        queryA.setAliasAndSetNeedBuild("B");
        queryA.setSelectItemListAndSetNeedBuild("S.AID AS BID,S.ANAME AS BNAME,S.ASCHOOL AS BSCHOOL");
        queryA.build();

        $Query$ queryB = queryA.deepCopy();
        queryB.setAliasAndSetNeedBuild("C");
        queryB.setSelectItemListAndSetNeedBuild("S.AID AS CID,S.ANAME AS CNAME,S.ASCHOOL AS CSCHOOL");
        queryB.build();

        Join nextJoin = queryA.join(queryB);
        join.addJoinKeys("BID", "CID");
        join.addJoinKeys("BNAME", "CNAME");
        nextJoin.setJoinStrategy(JoinStrategy.sort_merge_join);
        nextJoin.setSelectItemListAndSetNeedBuild("C.CID AS ID , C.CNAME AS NAME , C.CSCHOOL AS SCHOOL");
        // group by顺序可调整
        nextJoin.addGroupByItemAndSetNeedBuild("NAME");
        nextJoin.addGroupByItemAndSetNeedBuild("SCHOOL");
        nextJoin.addGroupByItemAndSetNeedBuild("ID");
        nextJoin.addOrderByItemAndSetNeedBuild("SCHOOL", false);
        nextJoin.build();

        OrderByPusher.optimize(nextJoin);
        // 推导结果有点深，就不枚举了

        // 左子树，ID , NAME
        Assert.assertEquals(2, ((TableQuery) queryA.getFirstSubNodeQueryNode().getNodeList().get(0)).getOrderByList().size());
        // 只是id join列
        Assert.assertEquals(1, ((TableQuery) queryA.getFirstSubNodeQueryNode().getNodeList().get(1)).getOrderByList().size());
        // ID , NAME
        Assert.assertEquals(2, queryA.getOrderByList().size());

        // 右子树，ID , NAME , SCHOOL
        Assert.assertEquals(3, ((TableQuery) queryB.getFirstSubNodeQueryNode().getNodeList().get(0)).getOrderByList().size());
        // 只是id join列
        Assert.assertEquals(1, ((TableQuery) queryB.getFirstSubNodeQueryNode().getNodeList().get(1)).getOrderByList().size());
        // ID , NAME , SCHOOL
        Assert.assertEquals(3, queryB.getOrderByList().size());

        // ID
        Assert.assertEquals(1, nextJoin.getOrderByList().size());
        // ID , NAME , SCHOOL
        Assert.assertEquals(3, nextJoin.getGroupByList().size());
    }

    @Test
    public void test_orderby_merge下推_group为order的子集() {
        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        tableQueryNode1.addOrderByItemAndSetNeedBuild("ID", false);
        tableQueryNode1.addOrderByItemAndSetNeedBuild("NAME", true);
        tableQueryNode1.addGroupByItemAndSetNeedBuild("ID");

        MergeQuery merge = new MergeQuery();
        merge.addNode(tableQueryNode1);
        merge.build();
        Query qtn = OrderByPusher.optimize(merge);

        // 子节点先按照NAME做group
        // by,因为子节点的orderby和父节点的groupBy满足一个前序匹配，这里直接使用父节点id,Name做order by
        // merge节点先按id做group
        // by(底下子节点为id,name排序，不需要临时表排序)，groupby处理后的结果也满足id,name的排序，直接返回，不需要临时表

        Assert.assertEquals(2, ((TableQuery) qtn.getFirstSubNodeQueryNode()).getOrderByList().size());
        Assert.assertEquals("ID", ((TableQuery) qtn.getFirstSubNodeQueryNode()).getOrderByList().get(0).getColumnName());
        Assert.assertEquals("NAME", ((TableQuery) qtn.getFirstSubNodeQueryNode()).getOrderByList().get(1).getColumnName());

        Assert.assertEquals(1, ((TableQuery) qtn.getFirstSubNodeQueryNode()).getGroupByList().size());
        Assert.assertEquals("ID", ((TableQuery) qtn.getFirstSubNodeQueryNode()).getGroupByList().get(0).getColumnName());
    }

    @Test
    public void test_orderby_merge下推_group不为order的子集() {
        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        tableQueryNode1.addOrderByItemAndSetNeedBuild("ID", false);
        tableQueryNode1.addOrderByItemAndSetNeedBuild("NAME", true);
        tableQueryNode1.addGroupByItemAndSetNeedBuild("NAME");

        MergeQuery merge = new MergeQuery();
        merge.addNode(tableQueryNode1);
        merge.build();
        Query qtn = OrderByPusher.optimize(merge);

        // 子节点先按照NAME做group
        // by,因为子节点的orderby和父节点的groupBy不是一个前序匹配，这里直接使用父节点Name做order by
        // merge节点先按NAME做group by，然后临时表排序做id,columnName

        Assert.assertEquals(1, ((TableQuery) qtn.getFirstSubNodeQueryNode()).getOrderByList().size());
        Assert.assertEquals("NAME", ((TableQuery) qtn.getFirstSubNodeQueryNode()).getOrderByList().get(0).getColumnName());

        Assert.assertEquals(1, ((TableQuery) qtn.getFirstSubNodeQueryNode()).getGroupByList().size());
        Assert.assertEquals("NAME", ((TableQuery) qtn.getFirstSubNodeQueryNode()).getGroupByList().get(0).getColumnName());
    }

    @Test
    public void test_merge的distinct下推() {
        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        TableQuery tableQueryNode2 = new TableQuery("TABLE2");

        Column id = ObjectCreateFactory.createColumn();
        id.setColumnName("ID");
        id.setDistinct(true);

        Column name = ObjectCreateFactory.createColumn();
        name.setColumnName("NAME");
        name.setDistinct(true);

        Column school = ObjectCreateFactory.createColumn();
        school.setColumnName("SCHOOL");
        school.setDistinct(true);

        tableQueryNode1.addGroupByItemAndSetNeedBuild("NAME");
        tableQueryNode1.addOrderByItemAndSetNeedBuild("ID");

        MergeQuery merge = tableQueryNode1.merge(tableQueryNode2);
        merge.setSelectItemList(id, name, school);
        merge.build();

        OrderByPusher.optimize(merge);
        Assert.assertEquals(3, tableQueryNode1.getOrderByList().size());
        Assert.assertEquals("TABLE1.NAME", tableQueryNode1.getOrderByList().get(0).getItem().toString());
    }

    @Test
    public void test_join的distinct下推() {
        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        TableQuery tableQueryNode2 = new TableQuery("TABLE2");

        Column id = ObjectCreateFactory.createColumn();
        id.setColumnName("ID");
        id.setTableName("TABLE1");
        id.setDistinct(true);

        Column name = ObjectCreateFactory.createColumn();
        name.setColumnName("NAME");
        name.setTableName("TABLE1");
        name.setDistinct(true);

        Column school = ObjectCreateFactory.createColumn();
        school.setColumnName("SCHOOL");
        school.setTableName("TABLE1");
        school.setDistinct(true);

        Join join = tableQueryNode1.join(tableQueryNode2);
        join.setSelectItemList(id, name, school);
        join.addOrderByItemAndSetNeedBuild("NAME");
        join.build();

        OrderByPusher.optimize(join);
        Assert.assertEquals(3, tableQueryNode1.getOrderByList().size());
        Assert.assertEquals("TABLE1.NAME", tableQueryNode1.getOrderByList().get(0).getItem().toString());
    }
}
