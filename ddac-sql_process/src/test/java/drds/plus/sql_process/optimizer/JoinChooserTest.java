package drds.plus.sql_process.optimizer;

import drds.plus.common.properties.ConnectionProperties;
import drds.plus.sql_process.BaseOptimizerTest;
import drds.plus.sql_process.abstract_syntax_tree.configuration.IndexMapping;
import drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.JoinStrategy;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.Item;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.Filter;
import drds.plus.sql_process.abstract_syntax_tree.node.query.*;
import drds.plus.sql_process.optimizer.chooser.FilterSpliter;
import drds.plus.sql_process.optimizer.chooser.FilterType;
import drds.plus.sql_process.optimizer.chooser.IndexChooser;
import drds.plus.sql_process.optimizer.chooser.JoinChooser;
import drds.plus.sql_process.optimizer.pusher.FilterPusher;
import drds.plus.sql_process.utils.DnfFilters;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class JoinChooserTest extends BaseOptimizerTest {

    @Test
    public void test_TableNode转化为index_主键索引() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        tableQueryNode.setWhere("ID = 1");
        build(tableQueryNode);

        Query qn = tableQueryNode.convertToJoinIfNeed();

        Assert.assertTrue(qn instanceof TableQuery);
        Assert.assertEquals("TABLE1.ID = 1", ((TableQuery) qn).getIndexQueryKeyFilter().toString());
    }

    @Test
    public void test_TableNode转化为index_查询字段都在索引上() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        tableQueryNode.setSelectItemListAndSetNeedBuild("ID,NAME");
        tableQueryNode.setWhere("NAME = 1");
        build(tableQueryNode);

        Query qn = tableQueryNode.convertToJoinIfNeed();

        Assert.assertTrue(qn instanceof TableQuery);
        Assert.assertEquals("TABLE1._NAME.NAME = 1", ((TableQuery) qn).getIndexQueryKeyFilter().toString());
    }

    @Test
    public void test_TableNode转化为index到kv的join() {
        // table1存在id的主键索引和name的二级索引，期望构造为index columnName join id
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        tableQueryNode.setWhere("NAME = 1");
        build(tableQueryNode);

        Query qn = tableQueryNode.convertToJoinIfNeed();

        Assert.assertTrue(qn instanceof Join);
        Assert.assertEquals("TABLE1._NAME.NAME = 1", ((Join) qn).getLeftNode().getIndexQueryKeyFilter().toString());
        Assert.assertEquals("TABLE1._NAME.ID = TABLE1.ID", ((Join) qn).getJoinFilterList().get(0).toString());
    }

    @Test
    public void test_TableNode转化为index到kv_复杂条件查询() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        tableQueryNode.setSelectItemListAndSetNeedBuild("(ID + NAME) AS NEWNAME"); // 设置为函数
        tableQueryNode.setWhere("NAME = 1 and ID > 3 and SCHOOL = 1");
        tableQueryNode.addOrderByItemAndSetNeedBuild("SCHOOL", false);// 增加一个隐藏列
        tableQueryNode.addGroupByItemAndSetNeedBuild("NEWNAME");
        build(tableQueryNode);

        Query qn = tableQueryNode.convertToJoinIfNeed();

        Assert.assertTrue(qn instanceof Join);
        Assert.assertEquals("TABLE1._NAME.NAME = 1", ((Join) qn).getLeftNode().getIndexQueryKeyFilter().toString());
        Assert.assertEquals("TABLE1._NAME.ID > 3", ((Join) qn).getLeftNode().getResultFilter().toString());
        Assert.assertEquals("TABLE1.SCHOOL = 1", ((Join) qn).getRightNode().getResultFilter().toString());
        Assert.assertEquals("TABLE1._NAME.ID = TABLE1.ID", ((Join) qn).getJoinFilterList().get(0).toString());
    }

    @Test
    public void test_子查询套TableNode转化为index到kv() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        tableQueryNode.setWhere("NAME = 1");
        build(tableQueryNode);

        $Query$ query = new $Query$(tableQueryNode);
        query.build();

        Query qn = query.convertToJoinIfNeed();
        Assert.assertTrue(qn instanceof $Query$);
        Assert.assertTrue(qn.getFirstSubNodeQueryNode() instanceof Join);
        Assert.assertEquals("TABLE1._NAME.NAME = 1", ((Join) qn.getFirstSubNodeQueryNode()).getLeftNode().getIndexQueryKeyFilter().toString());
        Assert.assertEquals("TABLE1._NAME.ID = TABLE1.ID", ((Join) qn.getFirstSubNodeQueryNode()).getJoinFilterList().get(0).toString());
    }

    @Test
    public void test_Join左是子查询_右是TableNode_转化为最左树() {
        TableQuery tableQueryNode1 = new TableQuery("TABLE1");


        TableQuery tableQueryNode2 = new TableQuery("TABLE2");

        Join join = tableQueryNode1.join(tableQueryNode2, "NAME", "NAME");
        join.setJoinStrategy(JoinStrategy.index_nest_loop_join);
        join.setWhere("TABLE1.NAME = 1 and TABLE1.ID > 3 and TABLE1.SCHOOL = 1");// 原本条件应该是加在join下的，这里省区推导的过程
        join.setSelectItemListAndSetNeedBuild("(TABLE2.ID + TABLE2.NAME) AS NEWNAME"); // 设置为函数
        join.addOrderByItemAndSetNeedBuild("TABLE1.SCHOOL", false);// 增加一个隐藏列
        join.addGroupByItemAndSetNeedBuild("NEWNAME");
        join.build();

        Query qn = FilterPusher.optimize(join);// 先把条件推导子节点上，构建子节点join
        build(tableQueryNode1);
        build(tableQueryNode2);
        qn = qn.convertToJoinIfNeed();

        Assert.assertTrue(qn instanceof Join);
        Assert.assertEquals("TABLE2$_NAME.ID = TABLE2.ID", ((Join) qn).getJoinFilterList().get(0).toString());

        Assert.assertTrue(((Join) qn).getLeftNode() instanceof Join);
        Assert.assertEquals("TABLE1.NAME = TABLE2$_NAME.NAME", ((Join) ((Join) qn).getLeftNode()).getJoinFilterList().get(0).toString());

        Assert.assertTrue(((Join) ((Join) qn).getLeftNode()).getLeftNode() instanceof Join);
        Join jn = (Join) ((Join) ((Join) qn).getLeftNode()).getLeftNode();
        Assert.assertEquals("TABLE1._NAME.NAME = 1", jn.getLeftNode().getIndexQueryKeyFilter().toString());
        Assert.assertEquals("TABLE1._NAME.ID > 3", jn.getLeftNode().getResultFilter().toString());
        Assert.assertEquals("TABLE1.SCHOOL = 1", jn.getRightNode().getResultFilter().toString());
        Assert.assertEquals("TABLE1._NAME.ID = TABLE1.ID", jn.getJoinFilterList().get(0).toString());
    }

    /**
     * c1有索引，c2无索引
     */
    @Test
    @Ignore("cost模型计算暂时关闭,不支持调整")
    public void test_JoinStrategy选择_调整join顺序() {
        // table11.c1为二级索引,table10.c2不存在索引
        TableQuery tableQueryNode1 = new TableQuery("TABLE11");
        TableQuery tableQueryNode2 = new TableQuery("TABLE10");
        Join joinNode = tableQueryNode1.join(tableQueryNode2);
        joinNode.addJoinKeys("C1", "C2");
        Query qn = joinNode;
        qn.build();
        qn = FilterPusher.optimize(qn);// 先把条件推导子节点上，构建子节点join
        build(tableQueryNode1);
        build(tableQueryNode2);

        qn = optimize(qn, true, true, true);
        Assert.assertEquals(JoinStrategy.index_nest_loop_join, ((Join) qn).getJoinStrategy());
        Assert.assertTrue(((Join) qn).getLeftNode() instanceof Join);
        Assert.assertEquals("TABLE10", ((Join) ((Join) qn).getLeftNode()).getLeftNode().getName());
        Assert.assertEquals("TABLE11", ((Join) qn).getRightNode().getName());
        Assert.assertEquals("TABLE11", ((TableQuery) ((Join) qn).getRightNode()).getIndexMappingUsed().getIndexName());
    }

    @Test
    public void test_JoinStrategy选择_存在条件() {
        // table11虽然是C2非主键列为join，但存在主键条件，使用NESTLOOP
        TableQuery tableQueryNode1 = new TableQuery("TABLE10");
        TableQuery tableQueryNode2 = new TableQuery("TABLE11");
        Join joinNode = tableQueryNode1.join(tableQueryNode2);
        joinNode.addJoinKeys("C1", "C2");
        Query qn = joinNode;
        qn.setWhere("TABLE11.ID = 1");
        qn.build();
        qn = FilterPusher.optimize(qn);// 先把条件推导子节点上，构建子节点join
        build(tableQueryNode1);
        build(tableQueryNode2);

        qn = optimize(qn, true, true, true);
        Assert.assertEquals(JoinStrategy.nest_loop_join, ((Join) qn).getJoinStrategy());
        Assert.assertEquals("TABLE10", ((Join) qn).getLeftNode().getName());
        Assert.assertEquals("TABLE11", ((Join) qn).getRightNode().getName());
        Assert.assertEquals("TABLE11", ((TableQuery) ((Join) qn).getRightNode()).getIndexMappingUsed().getIndexName());
    }

    @Test
    public void test_JoinStrategy选择_存在子查询_存在条件() {
        // table11虽然是C2非主键列为join，但存在主键条件，使用NESTLOOP
        TableQuery tableQueryNode1 = new TableQuery("TABLE10");
        tableQueryNode1.setWhere("TABLE10.ID = 1");
        $Query$ query = new $Query$(tableQueryNode1);

        TableQuery tableQueryNode2 = new TableQuery("TABLE11");
        Join joinNode = query.join(tableQueryNode2);
        joinNode.addJoinKeys("C1", "C2");
        Query qn = joinNode;
        qn.build();
        qn = FilterPusher.optimize(qn);// 先把条件推导子节点上，构建子节点join
        build(tableQueryNode1);
        build(tableQueryNode2);

        qn = optimize(qn, true, true, true);
        Assert.assertEquals(JoinStrategy.nest_loop_join, ((Join) qn).getJoinStrategy());
        Assert.assertEquals("TABLE10", ((Join) qn).getLeftNode().getName());
        Assert.assertEquals("TABLE11", ((Join) qn).getRightNode().getName());
        Assert.assertEquals("TABLE11", ((TableQuery) ((Join) qn).getRightNode()).getIndexMappingUsed().getIndexName());
    }

    @Test
    public void test_存在子查询() {
        // table11虽然是C2非主键列为join，但存在主键条件，使用NESTLOOP
        TableQuery tableQueryNode1 = new TableQuery("TABLE10");
        tableQueryNode1.setWhere("TABLE10.ID = 1");
        $Query$ qn = new $Query$(tableQueryNode1);
        qn.build();

        Query qtn = optimize(qn, true, true, true);
        Assert.assertEquals(((TableQuery) qtn.getFirstSubNodeQueryNode()).getIndexQueryKeyFilter().toString(), "TABLE10.ID = 1");
    }

    @Test
    public void test_存在嵌套子查询() {
        // table11虽然是C2非主键列为join，但存在主键条件，使用NESTLOOP
        TableQuery tableQueryNode1 = new TableQuery("TABLE10");
        tableQueryNode1.setWhere("TABLE10.ID = 1");
        $Query$ nestQn = new $Query$(tableQueryNode1);
        $Query$ qn = new $Query$(nestQn);
        qn.build();

        optimize(qn, true, true, true);
        Assert.assertEquals(((TableQuery) nestQn.getFirstSubNodeQueryNode()).getIndexQueryKeyFilter().toString(), "TABLE10.ID = 1");
    }

    @Test
    public void test_存在OR条件_所有字段均有索引_构建IndexMerge() {
        TableQuery tableQueryNode1 = new TableQuery("TABLE10");
        tableQueryNode1.setWhere("ID = 1 or C1 = 2");
        tableQueryNode1.build();
        Query qn = optimize(tableQueryNode1, true, true, true);
        Assert.assertTrue(qn instanceof MergeQuery);
        Assert.assertTrue(qn.getNodeList().get(0) instanceof TableQuery);
        Assert.assertTrue(qn.getNodeList().get(1) instanceof Join);
    }

    @Test
    public void test_存在OR条件_有字段无索引_构建全表扫描() {
        TableQuery tableQueryNode1 = new TableQuery("TABLE10");
        tableQueryNode1.setWhere("ID = 1 or C1 = 2 or C2 = 1");
        tableQueryNode1.build();
        Query qn = optimize(tableQueryNode1, true, true, false);
        Assert.assertTrue(qn instanceof TableQuery);
        // Assert.assertTrue(((TableQuery) qn).isFullTableScan());
    }

    @Test
    public void test_JoinStrategy选择sortmerge_outterJoin() {
        TableQuery tableQueryNode1 = new TableQuery("TABLE10");
        TableQuery tableQueryNode2 = new TableQuery("TABLE11");
        Join join = tableQueryNode1.join(tableQueryNode2);
        join.setFullOuterJoin();
        join.build();

        Query qn = optimize(join, true, true, true);
        Assert.assertEquals(JoinStrategy.sort_merge_join, ((Join) qn).getJoinStrategy());
    }

    @Test
    public void test_JoinStrategy选择sortmerge_右表存在group条件() {
        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        TableQuery tableQueryNode2 = new TableQuery("TABLE2");
        Join join = tableQueryNode1.join(tableQueryNode2);
        join.setLeftOuterJoin();
        join.addJoinKeys("ID", "ID");
        join.addJoinKeys("NAME", "NAME");
        join.addOrderByItemAndSetNeedBuild("TABLE2.ID");
        join.addGroupByItemAndSetNeedBuild("TABLE2.NAME");
        join.addGroupByItemAndSetNeedBuild("TABLE2.ID");
        join.addGroupByItemAndSetNeedBuild("TABLE2.SCHOOL");
        join.build();

        Query qn = optimize(join, true, true, true);
        Assert.assertEquals(JoinStrategy.sort_merge_join, ((Join) qn).getJoinStrategy());
    }

    @Test
    public void test_JoinStrategy选择sortmerge_右表存在group条件和order条件不能合并() {
        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        TableQuery tableQueryNode2 = new TableQuery("TABLE2");
        Join join = tableQueryNode1.join(tableQueryNode2);
        join.setLeftOuterJoin();
        join.addJoinKeys("ID", "ID");
        join.addJoinKeys("NAME", "NAME");
        join.addOrderByItemAndSetNeedBuild("TABLE2.SCHOOL");
        join.addGroupByItemAndSetNeedBuild("TABLE2.NAME");
        join.addGroupByItemAndSetNeedBuild("TABLE2.ID");
        join.build();

        Query qn = optimize(join, true, true, true);
        Assert.assertEquals(JoinStrategy.sort_merge_join, ((Join) qn).getJoinStrategy());
    }

    @Test
    public void test_JoinStrategy选择sortmerge_右表存在order条件() {
        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        TableQuery tableQueryNode2 = new TableQuery("TABLE2");
        Join join = tableQueryNode1.join(tableQueryNode2);
        join.setLeftOuterJoin();
        join.addJoinKeys("ID", "ID");
        join.addJoinKeys("NAME", "NAME");
        join.addOrderByItemAndSetNeedBuild("TABLE2.ID");
        join.build();

        Query qn = optimize(join, true, true, true);
        Assert.assertEquals(JoinStrategy.sort_merge_join, ((Join) qn).getJoinStrategy());
    }

    @Test
    public void test_JoinStrategy不选择sortmerge_右表存在order条件() {
        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        TableQuery tableQueryNode2 = new TableQuery("TABLE2");
        Join join = tableQueryNode1.join(tableQueryNode2);
        join.setLeftOuterJoin();
        join.addJoinKeys("ID", "ID");
        join.addJoinKeys("NAME", "NAME");
        join.addOrderByItemAndSetNeedBuild("TABLE2.SCHOOL"); // join列的顺序没法推导
        join.build();

        Query qn = optimize(join, true, true, true);
        Assert.assertEquals(JoinStrategy.index_nest_loop_join, ((Join) qn).getJoinStrategy());
    }

    private Query optimize(Query qtn, boolean chooseIndex, boolean chooseJoin, boolean chooseIndexMerge) {
        Map<String, Object> extraCmd = new HashMap<String, Object>();
        extraCmd.put(ConnectionProperties.CHOOSE_INDEX, chooseIndex);
        extraCmd.put(ConnectionProperties.CHOOSE_JOIN, chooseJoin);
        extraCmd.put(ConnectionProperties.CHOOSE_INDEX_MERGE, chooseIndexMerge);
        return (Query) JoinChooser.optimize(qtn, extraCmd);
    }

    private void build(TableQuery tableQueryNode) {
        tableQueryNode.build();

        Map<String, Object> extraCmd = new HashMap<String, Object>();
        extraCmd.put(ConnectionProperties.CHOOSE_INDEX, true);
        IndexMapping index = IndexChooser.findBestIndexMetaData(tableQueryNode.getTableMetaData(), new ArrayList<Item>(), DnfFilters.toDnfFilterList(tableQueryNode.getWhere()), tableQueryNode.getTableName(), extraCmd);

        tableQueryNode.setIndexMappingUsed(index);

        Map<FilterType, Filter> result = FilterSpliter.splitByIndex(DnfFilters.toDnfFilterList(tableQueryNode.getWhere()), tableQueryNode);

        tableQueryNode.setIndexQueryKeyFilterAndSetNeedBuild(result.get(FilterType.IndexQueryKeyFilter));
        tableQueryNode.setIndexQueryValueFilter(result.get(FilterType.IndexQueryValueFilter));
        tableQueryNode.setResultFilterAndSetNeedBuild(result.get(FilterType.ResultFilter));
    }

}
