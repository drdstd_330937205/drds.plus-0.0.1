package drds.plus.sql_process.optimizer;

import drds.plus.common.jdbc.Parameters;
import drds.plus.common.jdbc.SetParameterMethod;
import drds.plus.common.jdbc.SetParameterMethodAndArgs;
import drds.plus.common.properties.ConnectionProperties;
import drds.plus.sql_process.BaseOptimizerTest;
import drds.plus.sql_process.abstract_syntax_tree.execute_plan.ExecutePlan;
import drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.*;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.BooleanFilter;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.Function;
import drds.plus.sql_process.abstract_syntax_tree.node.query.$Query$;
import drds.plus.sql_process.abstract_syntax_tree.node.query.Join;
import drds.plus.sql_process.abstract_syntax_tree.node.query.TableQuery;
import drds.plus.sql_process.parser.SqlParserException;
import drds.plus.sql_process.utils.OptimizerUtils;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 整个优化器的集成测试，主要是一些query
 */
public class OptimizerTest extends BaseOptimizerTest {

    private static Map<String, Object> extraCmd = new HashMap<String, Object>();

    @BeforeClass
    public static void setUp() {
        extraCmd.put(ConnectionProperties.CHOOSE_INDEX, true);
        extraCmd.put(ConnectionProperties.CHOOSE_JOIN, false);
        extraCmd.put(ConnectionProperties.CHOOSE_INDEX_MERGE, false);
        // extraCmd.add(ConnectionProperties.MERGE_EXPAND, false);
        extraCmd.put(ConnectionProperties.JOIN_MERGE_JOIN_JUDGE_BY_RULE, true);
    }

    @Test
    public void test_单表查询_无条件() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        //Query qn = (Query)tableQueryNode;
        Query qc = (Query) optimizer.optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(tableQueryNode, null, extraCmd);

        Assert.assertTrue(qc instanceof MergeQuery);
        Assert.assertEquals(QueryConcurrencyWay.sequential, ((MergeQuery) qc).getQueryConcurrencyWay());// 串行
        ExecutePlan dne = ((MergeQuery) qc).getExecutePlanList().get(0);
        Assert.assertTrue(dne instanceof QueryWithIndex);
        QueryWithIndex queryWithIndex = (QueryWithIndex) dne;
        Assert.assertEquals(null, queryWithIndex.getKeyFilter());
        Assert.assertEquals(null, queryWithIndex.getValueFilter());
    }

    // 单表主键查询
    // ID为主键，同时在ID上存在索引
    // 直接查询KV ID->data
    // keyFilter为ID=1
    @Test
    public void test_单表查询_主键条件() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        tableQueryNode.setWhere("ID=1 and ID<40");

        Query qc = (Query) optimizer.optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(tableQueryNode, null, extraCmd);
        Assert.assertTrue(qc instanceof QueryWithIndex);
        Assert.assertEquals("TABLE1.ID = 1", ((QueryWithIndex) qc).getKeyFilter().toString());
        Assert.assertEquals(null, ((QueryWithIndex) qc).getValueFilter());
    }

    // 单表主键查询
    // ID为主键，同时在ID上存在索引
    // 因为!=不能使用主键索引
    // valueFilter为ID!=1
    @Test
    public void test_单表查询_主键条件_不等于只能是valueFilter() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        tableQueryNode.setWhere("ID != 1");
        Query qc = (Query) optimizer.optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(tableQueryNode, null, extraCmd);

        Assert.assertTrue(qc instanceof MergeQuery);
        Assert.assertEquals(QueryConcurrencyWay.data_node_concurrent, ((MergeQuery) qc).getQueryConcurrencyWay());// 并行
        ExecutePlan dne = ((MergeQuery) qc).getExecutePlanList().get(0);
        Assert.assertTrue(dne instanceof QueryWithIndex);
        QueryWithIndex queryWithIndex = (QueryWithIndex) dne;
        Assert.assertEquals(null, queryWithIndex.getKeyFilter());
        Assert.assertEquals("TABLE1.ID != 1", queryWithIndex.getValueFilter().toString());
    }

    // 单表非主键索引查询
    // NAME上存在索引
    // 会生成一个Join节点
    // 左边通过NAME索引找到满足条件的PK，keyFilter应该为NAME=1
    // 与pk->data JoinImpl
    // Join类型为IndexNestLoop
    @Test
    public void test_单表查询_value条件() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        tableQueryNode.setWhere("NAME = 1");

        Query qc = (Query) optimizer.optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(tableQueryNode, null, extraCmd);
        Assert.assertTrue(qc instanceof MergeQuery);
        Assert.assertEquals(QueryConcurrencyWay.data_node_concurrent, ((MergeQuery) qc).getQueryConcurrencyWay());// 并行
        ExecutePlan dne = ((MergeQuery) qc).getExecutePlanList().get(0);
        Assert.assertTrue(dne instanceof drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.Join);
        drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.Join join = (drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.Join) dne;
        QueryWithIndex left = (QueryWithIndex) join.getLeftNode();
        Assert.assertEquals("TABLE1._NAME.NAME = 1", left.getKeyFilter().toString());
    }

    // 单表非主键无索引查询
    // SCHOOL上不存在索引
    // 所以会执行全表扫描
    // 只会生成一个IQuery
    // SCHOOL=1作为valueFilter
    @Test
    public void test_单表查询_非任何索引条件() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        tableQueryNode.setWhere("SCHOOL = 1");

        Query qc = (Query) optimizer.optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(tableQueryNode, null, extraCmd);
        Assert.assertTrue(qc instanceof MergeQuery);
        Assert.assertEquals(QueryConcurrencyWay.data_node_concurrent, ((MergeQuery) qc).getQueryConcurrencyWay());// 并行
        ExecutePlan dne = ((MergeQuery) qc).getExecutePlanList().get(0);
        Assert.assertTrue(dne instanceof QueryWithIndex);
        QueryWithIndex queryWithIndex = (QueryWithIndex) dne;
        Assert.assertEquals("TABLE1.SCHOOL = 1", queryWithIndex.getValueFilter().toString());
    }

    // 单表or查询
    // 查询条件由or连接，
    // 由于NAME和ID上存在索引，所以会生成两个子查询
    // or的两边分别作为子查询的keyFilter
    // 由于NAME=2323的子查询为非主键索引查询
    // 所以此处会生成一个join节点
    // 最后一个merge节点用于合并子查询的结果
    @Test
    public void test_单表查询_OR条件_1() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        tableQueryNode.setWhere("NAME = 2323 or ID=1");
        extraCmd.put(ConnectionProperties.CHOOSE_INDEX_MERGE, true);
        Query qc = (Query) optimizer.optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(tableQueryNode, null, extraCmd);
        extraCmd.put(ConnectionProperties.CHOOSE_INDEX_MERGE, false);
        Assert.assertTrue(qc instanceof MergeQuery);
        Assert.assertTrue(((MergeQuery) qc).isUnion());// 是union查询
        Assert.assertTrue(((MergeQuery) qc).getExecutePlanList().get(0) instanceof QueryWithIndex);
        QueryWithIndex queryWithIndex = (QueryWithIndex) ((MergeQuery) qc).getExecutePlanList().get(0);
        Assert.assertEquals("TABLE1.ID = 1", queryWithIndex.getKeyFilter().toString());
        Assert.assertTrue(((MergeQuery) qc).getExecutePlanList().get(1) instanceof MergeQuery);
        Assert.assertTrue(((MergeQuery) ((MergeQuery) qc).getExecutePlanList().get(1)).getExecutePlanList().get(0) instanceof drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.Join);
        drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.Join join = (drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.Join) ((MergeQuery) ((MergeQuery) qc).getExecutePlanList().get(1)).getExecutePlanList().get(0);
        Assert.assertEquals("TABLE1._NAME.NAME = 2323", ((QueryWithIndex) join.getLeftNode()).getKeyFilter().toString());
    }

    // 单表复杂查询条件
    // SCHOOL=1 and (ID=4 or ID=3)
    // 应该展开为
    // (SCHOOL=1 and ID=4) or (SCHOOL=1 and ID=3)
    @Test
    public void test_单表查询_复杂条件展开() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        tableQueryNode.setWhere("SCHOOL=1 and (ID=4 or ID=3)");
        extraCmd.put(ConnectionProperties.CHOOSE_INDEX_MERGE, true);
        extraCmd.put(ConnectionProperties.expand_or_with_other, true);
        Query qc = (Query) optimizer.optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(tableQueryNode, null, extraCmd);
        extraCmd.put(ConnectionProperties.CHOOSE_INDEX_MERGE, false);
        extraCmd.put(ConnectionProperties.expand_or_with_other, false);
        Assert.assertTrue(qc instanceof MergeQuery);
        Assert.assertTrue(((MergeQuery) qc).isUnion());// 是union查询
        Assert.assertTrue(((MergeQuery) qc).getExecutePlanList().get(0) instanceof QueryWithIndex);
        Assert.assertTrue(((MergeQuery) qc).getExecutePlanList().get(1) instanceof QueryWithIndex);
        QueryWithIndex queryWithIndex1 = (QueryWithIndex) ((MergeQuery) qc).getExecutePlanList().get(0);
        Assert.assertEquals("TABLE1.ID = 4", queryWithIndex1.getKeyFilter().toString());
        Assert.assertEquals("TABLE1.SCHOOL = 1", queryWithIndex1.getValueFilter().toString());
        QueryWithIndex queryWithIndex2 = (QueryWithIndex) ((MergeQuery) qc).getExecutePlanList().get(1);
        Assert.assertEquals("TABLE1.ID = 3", queryWithIndex2.getKeyFilter().toString());
        Assert.assertEquals("TABLE1.SCHOOL = 1", queryWithIndex2.getValueFilter().toString());
    }

    // 单表复杂查询条件
    // SCHOOL=1 and (ID=4 or ID=3)
    // 应该展开为
    // (SCHOOL=1 and ID=4) or (SCHOOL=1 and ID=3)
    @Test
    public void test_单表查询_复杂条件IN展开() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        tableQueryNode.setWhere("SCHOOL=1 and (ID in (4,3))");
        extraCmd.put(ConnectionProperties.CHOOSE_INDEX_MERGE, true);
        extraCmd.put(ConnectionProperties.expand_in_to_or, true);
        extraCmd.put(ConnectionProperties.expand_or_with_other, true);
        Query qc = (Query) optimizer.optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(tableQueryNode, null, extraCmd);
        extraCmd.put(ConnectionProperties.CHOOSE_INDEX_MERGE, false);
        extraCmd.put(ConnectionProperties.expand_in_to_or, false);
        extraCmd.put(ConnectionProperties.expand_or_with_other, false);
        Assert.assertTrue(qc instanceof MergeQuery);
        Assert.assertTrue(((MergeQuery) qc).isUnion());// 是union查询
        Assert.assertTrue(((MergeQuery) qc).getExecutePlanList().get(0) instanceof QueryWithIndex);
        Assert.assertTrue(((MergeQuery) qc).getExecutePlanList().get(1) instanceof QueryWithIndex);
        QueryWithIndex queryWithIndex1 = (QueryWithIndex) ((MergeQuery) qc).getExecutePlanList().get(0);
        Assert.assertEquals("TABLE1.ID = 4", queryWithIndex1.getKeyFilter().toString());
        Assert.assertEquals("TABLE1.SCHOOL = 1", queryWithIndex1.getValueFilter().toString());
        QueryWithIndex queryWithIndex2 = (QueryWithIndex) ((MergeQuery) qc).getExecutePlanList().get(1);
        Assert.assertEquals("TABLE1.ID = 3", queryWithIndex2.getKeyFilter().toString());
        Assert.assertEquals("TABLE1.SCHOOL = 1", queryWithIndex2.getValueFilter().toString());
    }

    // 两表Join查询，右表连接键为主键，右表为主键查询
    // 开启了join sort join
    // 右表为主键查询的情况下，Join策略应该选择IndexNestLoop
    @Test
    public void test_两表Join_主键() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        Join join = tableQueryNode.join("TABLE2", "ID", "ID");
        Query qc = (Query) optimizer.optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(join, null, extraCmd);
        // 应该是join sort join
        Assert.assertTrue(qc instanceof MergeQuery);
        Assert.assertTrue(((MergeQuery) qc).getExecutePlanList().get(0) instanceof drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.Join);
        drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.Join subJoin = (drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.Join) ((MergeQuery) qc).getExecutePlanList().get(0);
        Assert.assertEquals(JoinStrategy.index_nest_loop_join, subJoin.getJoinStrategy());
    }

    // 两表Join查询，右表连接键为主键，右表为主键查询
    // 开启了join sort join
    // 右表虽然为二级索引的查询，但Join列不是索引列，应该选择NestLoop
    // 会是一个table1 join ( table2 index join table2 columnName )的多级join
    @Test
    public void test_两表Join_主键_存在二级索引条件() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        Join join = tableQueryNode.join("TABLE2", "ID", "ID");
        join.setWhere("TABLE2.NAME = 1");
        Query qc = (Query) optimizer.optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(join, null, extraCmd);
        Assert.assertTrue(qc instanceof MergeQuery);
        Assert.assertTrue(((MergeQuery) qc).getExecutePlanList().get(0) instanceof drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.Join);
        drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.Join subJoin = (drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.Join) ((MergeQuery) qc).getExecutePlanList().get(0);
        Assert.assertTrue(subJoin.getRightNode() instanceof drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.Join);
        Assert.assertEquals(JoinStrategy.nest_loop_join, subJoin.getJoinStrategy());
    }

    // 两表Join查询，右表连接键为主键，右表为主键查询
    // 开启了join sort join
    // 右表主键索引的查询，Join列也索引列，应该选择IndexNestLoop
    // 会是一个(table1 join table2 index ) join table2 columnName 的多级join
    @Test
    public void test_两表Join_主键索引_存在主键索引条件() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        Join join = tableQueryNode.join("TABLE2", "ID", "ID");
        join.setWhere("TABLE2.ID in (1,2)");
        Query qc = (Query) optimizer.optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(join, null, extraCmd);
        Assert.assertTrue(qc instanceof MergeQuery);
        Assert.assertTrue(((MergeQuery) qc).getExecutePlanList().get(0) instanceof drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.Join);
        drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.Join subJoin = (drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.Join) ((MergeQuery) qc).getExecutePlanList().get(0);
        Assert.assertEquals(JoinStrategy.index_nest_loop_join, subJoin.getJoinStrategy());
    }

    // 两表Join查询，右表连接键为主键，右表为二级索引查询
    // 开启了join sort join
    // 右表二级索引的查询，Join列也是二级索引索引，应该选择NestLoop
    // 会是一个(table1 index join table1 index ) join (table2 index join table2
    // columnName)的多级join
    @Test
    public void test_两表Join_二级索引_存在二级索引条件() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        Join join = tableQueryNode.join("TABLE2", "NAME", "NAME");
        join.setWhere("TABLE2.NAME = 1");
        Query qc = (Query) optimizer.optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(join, null, extraCmd);
        Assert.assertTrue(qc instanceof drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.Join);
        Assert.assertTrue(((drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.Join) qc).getLeftNode() instanceof drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.Join);
        Assert.assertTrue(((drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.Join) qc).getRightNode() instanceof MergeQuery);
        Assert.assertEquals(JoinStrategy.index_nest_loop_join, ((drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.Join) qc).getJoinStrategy());
        drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.Join subJoin = (drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.Join) ((drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.Join) qc).getLeftNode();
        Assert.assertTrue(((drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.Join) subJoin).getLeftNode() instanceof MergeQuery);
        Assert.assertTrue(((drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.Join) subJoin).getRightNode() instanceof MergeQuery);
        Assert.assertEquals(JoinStrategy.index_nest_loop_join, subJoin.getJoinStrategy());
    }

    @Test
    public void test_三表Join_主键索引_存在主键索引条件() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        Join join = tableQueryNode.join("TABLE2", "TABLE1.ID", "TABLE2.ID");
        join = join.join("TABLE3", "TABLE1.ID", "TABLE3.ID");
        join.setWhere("TABLE3.ID in (1,2)");
        Query qc = (Query) optimizer.optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(join, null, extraCmd);
        Assert.assertTrue(qc instanceof MergeQuery);
        Assert.assertTrue(((MergeQuery) qc).getExecutePlanList().get(0) instanceof drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.Join);
        drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.Join subJoin = (drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.Join) ((MergeQuery) qc).getExecutePlanList().get(0);
        Assert.assertTrue(subJoin.getLeftNode() instanceof drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.Join);
        Assert.assertEquals(JoinStrategy.index_nest_loop_join, subJoin.getJoinStrategy());
    }

    @Test
    public void test_两表join_orderby_groupby_limit条件() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        Join join = tableQueryNode.join("TABLE2", "ID", "ID");
        join.setSelectItemList(OptimizerUtils.createColumnFromString("TABLE1.ID AS JID"), OptimizerUtils.createColumnFromString("CONCAT(TABLE1.NAME,TABLE1.SCHOOL) AS JNAME"));
        join.addOrderByItemAndSetNeedBuild("JID");
        join.addGroupByItemAndSetNeedBuild("JNAME");
        join.having("count(JID) > 0");
        Query qc = (Query) optimizer.optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(join, null, extraCmd);
        Assert.assertTrue(qc instanceof MergeQuery);
        Assert.assertEquals(QueryConcurrencyWay.concurrent, ((MergeQuery) qc).getQueryConcurrencyWay());// 串行
    }

    @Test
    public void test_两表join_单独limit条件_不做并行() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        Join join = tableQueryNode.join("TABLE2", "ID", "ID");
        join.setSelectItemList(OptimizerUtils.createColumnFromString("TABLE1.ID AS JID"), OptimizerUtils.createColumnFromString("CONCAT(TABLE1.NAME,TABLE1.SCHOOL) AS JNAME"));
        join.limit(10, 20);
        Query qc = (Query) optimizer.optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(join, null, extraCmd);
        Assert.assertTrue(qc instanceof MergeQuery);
        Assert.assertEquals(QueryConcurrencyWay.sequential, ((MergeQuery) qc).getQueryConcurrencyWay());// 串行
        drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.Join jn = (drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.Join) ((MergeQuery) qc).getExecutePlanList().get(0);
        Assert.assertEquals("0", jn.getLimitFrom().toString());
        Assert.assertEquals("30", jn.getLimitTo().toString());
    }

    @Test
    public void test_单表查询_存在聚合函数_limit不下推() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        tableQueryNode.limit(10, 20);
        tableQueryNode.setSelectItemListAndSetNeedBuild("count(distinct id)");
        //Query qn = (Query)tableQueryNode;
        Query qc = (Query) optimizer.optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(tableQueryNode, null, extraCmd);

        Assert.assertTrue(qc instanceof MergeQuery);
        Assert.assertEquals(QueryConcurrencyWay.data_node_concurrent, ((MergeQuery) qc).getQueryConcurrencyWay());// 并行
        Assert.assertEquals(10L, qc.getLimitFrom());
        Assert.assertEquals(20L, qc.getLimitTo());
        ExecutePlan dne = ((MergeQuery) qc).getExecutePlanList().get(0);
        Assert.assertTrue(dne instanceof QueryWithIndex);
        QueryWithIndex queryWithIndex = (QueryWithIndex) dne;
        Assert.assertEquals(null, queryWithIndex.getLimitFrom());
        Assert.assertEquals(null, queryWithIndex.getLimitTo());
    }

    @Test
    public void test_两表join_单库多表_单表_生成JoinMergeJoin() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        Join join = tableQueryNode.join("TABLE8", "ID", "ID");
        join.setWhere("TABLE1.ID in (0,1)");
        Query qc = (Query) optimizer.optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(join, null, extraCmd);
        Assert.assertTrue(qc instanceof MergeQuery);
        Assert.assertEquals(QueryConcurrencyWay.data_node_concurrent, ((MergeQuery) qc).getQueryConcurrencyWay());// 串行
    }

    @Test
    public void test_两表join_单表_单库多表_生成JoinMergeJoin() {
        TableQuery tableQueryNode = new TableQuery("TABLE8");
        Join join = tableQueryNode.join("TABLE1", "ID", "ID");
        join.setWhere("TABLE1.ID in (0,1)");
        Query qc = (Query) optimizer.optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(join, null, extraCmd);
        Assert.assertTrue(qc instanceof MergeQuery);
        Assert.assertEquals(QueryConcurrencyWay.data_node_concurrent, ((MergeQuery) qc).getQueryConcurrencyWay());// 串行
    }

    @Test
    public void test_两表join_单表子查询_单库多表_生成JoinMergeJoin() {
        TableQuery tableQueryNode = new TableQuery("TABLE8");
        $Query$ query = new $Query$(tableQueryNode);
        Join join = query.join("TABLE1", "ID", "ID");
        join.setWhere("TABLE1.ID in (0,1)");
        Query qc = (Query) optimizer.optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(join, null, extraCmd);
        Assert.assertTrue(qc instanceof MergeQuery);
        Assert.assertEquals(QueryConcurrencyWay.data_node_concurrent, ((MergeQuery) qc).getQueryConcurrencyWay());// 串行
    }

    @Test
    public void test_两表join_单表子查询_单库多表_单表存在limit_生成JoinMergeJoin() {
        TableQuery tableQueryNode = new TableQuery("TABLE8");
        $Query$ query = new $Query$(tableQueryNode);
        query.limit(1, 10);
        Join join = query.join("TABLE1", "ID", "ID");
        join.setWhere("TABLE1.ID in (0,1)");
        Query qc = (Query) optimizer.optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(join, null, extraCmd);
        Assert.assertTrue(qc instanceof MergeQuery);
        Assert.assertEquals(QueryConcurrencyWay.data_node_concurrent, ((MergeQuery) qc).getQueryConcurrencyWay());// 串行
    }

    @Test
    public void test_两表join_多库单表_单库多表_直接是join() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        Join join = tableQueryNode.join("TABLE8", "ID", "ID");
        join.setWhere("TABLE1.ID in (1,2)");
        Query qc = (Query) optimizer.optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(join, null, extraCmd);
        Assert.assertTrue(qc instanceof drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.Join);
        Assert.assertEquals(QueryConcurrencyWay.sequential, ((drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.Join) qc).getQueryConcurrencyWay());// 串行
    }

    @Test
    public void test_两表join_单库单表_单表会优化为下推() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        Join join = tableQueryNode.join("TABLE8", "NAME", "NAME");
        join.setWhere("TABLE1.ID = 0");
        Query qc = (Query) optimizer.optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(join, null, extraCmd);
        Assert.assertTrue(qc instanceof drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.Join);
        Assert.assertEquals(QueryConcurrencyWay.sequential, ((drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.Join) qc).getQueryConcurrencyWay());// 串行
    }

    // @Test
    public void test_单表查询_函数下推() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        tableQueryNode.setSelectItemListAndSetNeedBuild("ID");
        tableQueryNode.addOrderByItemAndSetNeedBuild("count(ID)");
        tableQueryNode.addGroupByItemAndSetNeedBuild("NAME");
        tableQueryNode.having("count(ID) > 1");
        tableQueryNode.setWhere("ID = 1");
        Query qc = (Query) optimizer.optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(tableQueryNode, null, extraCmd);
        Assert.assertTrue(qc instanceof QueryWithIndex);
        Assert.assertEquals(QueryConcurrencyWay.sequential, ((QueryWithIndex) qc).getQueryConcurrencyWay());// 并行
    }

    // @Test
    public void test_单表merge_函数下推() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        tableQueryNode.setSelectItemListAndSetNeedBuild("max(ID) AS ID");
        tableQueryNode.addOrderByItemAndSetNeedBuild("count(ID)");
        tableQueryNode.addGroupByItemAndSetNeedBuild("SUBSTRING(NAME,0,10)");
        tableQueryNode.having("count(ID) > 1");
        Query qc = (Query) optimizer.optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(tableQueryNode, null, extraCmd);
        Assert.assertTrue(qc instanceof MergeQuery);
        Assert.assertEquals(QueryConcurrencyWay.concurrent, ((MergeQuery) qc).getQueryConcurrencyWay());// 并行

        ExecutePlan dne = ((MergeQuery) qc).getExecutePlanList().get(0);
        Assert.assertTrue(dne instanceof QueryWithIndex);
        QueryWithIndex queryWithIndex = (QueryWithIndex) dne;
        Assert.assertEquals("SUBSTRING(NAME, 0, 10)", queryWithIndex.getItemList().get(1).toString());// 下推成功
        Assert.assertEquals("count(ID)", queryWithIndex.getItemList().get(2).toString());// 下推成功
    }

    @Test
    public void test_单表sql() {
        String sql = "query last_insert_id()";
        Query qc = (Query) optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, extraCmd, true);

        System.out.println(qc);
        Assert.assertTrue(qc instanceof QueryWithIndex);
        Assert.assertTrue(qc.getSql() != null);
    }

    @Test
    public void test_group分库键_没有order() {
        String sql = "query * from TABLE1 GROUP BY ID";
        Query qc = (Query) optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, extraCmd, true);
        Assert.assertTrue(qc instanceof MergeQuery);
        Assert.assertEquals(QueryConcurrencyWay.data_node_concurrent, ((MergeQuery) qc).getQueryConcurrencyWay());// group并行
        Assert.assertTrue(((MergeQuery) qc).getOrderByList().size() == 0);
    }

    @Test
    public void test_group分库键_带order() {
        String sql = "query * from TABLE1 GROUP BY ID ORDER BY ID";
        Query qc = (Query) optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, extraCmd, true);
        Assert.assertTrue(qc instanceof MergeQuery);
        Assert.assertEquals(QueryConcurrencyWay.concurrent, ((MergeQuery) qc).getQueryConcurrencyWay());// group并行
        Assert.assertTrue(((MergeQuery) qc).getOrderByList().size() > 0);
    }

    @Test
    public void test_group普通字段() {
        String sql = "query * from TABLE1 GROUP BY NAME";
        Query qc = (Query) optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, extraCmd, true);
        Assert.assertTrue(qc instanceof MergeQuery);
        Assert.assertEquals(QueryConcurrencyWay.concurrent, ((MergeQuery) qc).getQueryConcurrencyWay());// group并行
        Assert.assertTrue(((QueryImpl) ((MergeQuery) qc).getExecutePlan()).getOrderByList().size() > 0);
    }

    @Test
    public void test_distinct分库键() {
        String sql = "query distinct(id) from TABLE1";
        Query qc = (Query) optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, extraCmd, true);
        Assert.assertTrue(qc instanceof MergeQuery);
        Assert.assertEquals(QueryConcurrencyWay.data_node_concurrent, ((MergeQuery) qc).getQueryConcurrencyWay());// 并行
        Assert.assertTrue(((MergeQuery) qc).getOrderByList().size() == 0);
    }

    @Test
    public void test_count_distinct分库键() {
        String sql = "query count(distinct id) from TABLE1";
        Query qc = (Query) optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, extraCmd, true);
        Assert.assertTrue(qc instanceof MergeQuery);
        Assert.assertEquals(QueryConcurrencyWay.data_node_concurrent, ((MergeQuery) qc).getQueryConcurrencyWay());// 并行
        Assert.assertTrue(((MergeQuery) qc).getOrderByList().size() == 0);
    }

    @Test
    public void test_count_distinct普通字段() {
        String sql = "query count(distinct columnName) from TABLE1";
        Query qc = (Query) optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, extraCmd, true);
        Assert.assertTrue(qc instanceof MergeQuery);
        Assert.assertEquals(QueryConcurrencyWay.concurrent, ((MergeQuery) qc).getQueryConcurrencyWay());// 并行
        Assert.assertTrue(((MergeQuery) qc).getOrderByList().size() > 0);
    }

    @Test
    public void test_distinct普通字段() {
        String sql = "query distinct(columnName) from TABLE1";
        Query qc = (Query) optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, extraCmd, true);
        Assert.assertTrue(qc instanceof MergeQuery);
        Assert.assertEquals(QueryConcurrencyWay.concurrent, ((MergeQuery) qc).getQueryConcurrencyWay());// 并行
        Assert.assertTrue(((MergeQuery) qc).getOrderByList().size() > 0);
    }

    @Test
    public void test_distinct分库键_group分库键() {
        // 这种sql group没意义，distinct的优先级高于group
        String sql = "query distinct(id) from TABLE1 group by id";
        Query qc = (Query) optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, extraCmd, true);
        Assert.assertTrue(qc instanceof MergeQuery);
        Assert.assertEquals(QueryConcurrencyWay.concurrent, ((MergeQuery) qc).getQueryConcurrencyWay());// 并行
        Assert.assertTrue(((MergeQuery) qc).getOrderByList().size() > 0);
    }

    @Test
    public void test_distinct分库键_存在orderby() {
        String sql = "query distinct(id) from TABLE1 order by id";
        Query qc = (Query) optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, extraCmd, true);
        Assert.assertTrue(qc instanceof MergeQuery);
        Assert.assertEquals(QueryConcurrencyWay.concurrent, ((MergeQuery) qc).getQueryConcurrencyWay());// 并行
        Assert.assertTrue(((MergeQuery) qc).getOrderByList().size() > 0);
    }

    @Test
    public void test_存在条件_存在orderby() {
        String sql = "query * from TABLE1 where id > 1 order by id";
        Query qc = (Query) optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, extraCmd, true);
        Assert.assertTrue(qc instanceof MergeQuery);
        Assert.assertEquals(QueryConcurrencyWay.concurrent, ((MergeQuery) qc).getQueryConcurrencyWay());// 并行
    }

    @Test
    public void test_单表_forUpdate() {
        String sql = "query * from TABLE1 where id = 1 for update";
        Query qc = (Query) optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, extraCmd, true);
        Assert.assertTrue(qc instanceof QueryWithIndex);
        Assert.assertEquals(LockMode.exclusive_lock, ((QueryWithIndex) qc).getLockMode());
    }

    @Test
    public void test_单表_shareLock() {
        String sql = "query * from TABLE1 where id = 1 LOCK in SHARE MODE";
        Query qc = (Query) optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, extraCmd, true);
        Assert.assertTrue(qc instanceof QueryWithIndex);
        Assert.assertEquals(LockMode.shared_lock, ((QueryWithIndex) qc).getLockMode());
    }

    @Test
    public void test_merge_forUpdate() {
        String sql = "query * from TABLE1 where id in (1,2,3) for update";
        Query qc = (Query) optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, extraCmd, true);
        Assert.assertTrue(qc instanceof MergeQuery);
        Assert.assertEquals(LockMode.exclusive_lock, ((QueryWithIndex) ((MergeQuery) qc).getExecutePlan()).getLockMode());
    }

    @Test
    public void test_子查询_内部forUpdate() {
        String sql = "query * from (query * from TABLE1 where id = 1 for update) a";
        Query qc = (Query) optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, extraCmd, true);
        Assert.assertTrue(qc instanceof QueryWithIndex);
        Assert.assertEquals(LockMode.exclusive_lock, ((QueryWithIndex) ((QueryWithIndex) qc).getSubQuery()).getLockMode());
    }

    @Test
    public void test_子查询_外部forUpdate() {
        String sql = "query * from (query * from TABLE1 where id = 1) a for update";
        Query qc = (Query) optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, extraCmd, true);
        Assert.assertTrue(qc instanceof QueryWithIndex);
        Assert.assertEquals(LockMode.exclusive_lock, ((QueryWithIndex) qc).getLockMode());
    }

    @Test
    public void testQuery_子查询_in模式() throws SqlParserException {
        String sql = "SELECT ID,NAME FROM TABLE1 WHERE NAME in (SELECT NAME FROM TABLE2 WHERE TABLE2.NAME = TABLE1.NAME) and ID in (1,2)";
        Query qc = (Query) optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, extraCmd, true);
        Assert.assertTrue(qc instanceof MergeQuery);

        QueryWithIndex queryWithIndex1 = (QueryWithIndex) ((MergeQuery) qc).getExecutePlanList().get(0);
        QueryWithIndex queryWithIndex2 = (QueryWithIndex) ((MergeQuery) qc).getExecutePlanList().get(1);

        Function subquery1 = (Function) ((List) queryWithIndex1.getSubQueryFilter().getArgList().get(1)).get(0);
        Function subquery2 = (Function) ((List) queryWithIndex2.getSubQueryFilter().getArgList().get(1)).get(0);
        Assert.assertTrue(subquery1.getArgList().get(0) instanceof QueryWithIndex);

        QueryWithIndex merge1 = (QueryWithIndex) subquery1.getArgList().get(0);
        QueryWithIndex merge2 = (QueryWithIndex) subquery2.getArgList().get(0);
        Assert.assertEquals(merge1.getSubQueryFilterId(), merge2.getSubQueryFilterId());
    }

    @Test
    public void testQuery_子查询_多字段in模式() throws SqlParserException {
        String sql = "SELECT * FROM TABLE1 WHERE (ID,SCHOOL) in (SELECT ID,SCHOOL FROM TABLE2 WHERE TABLE2.NAME = TABLE1.NAME)";
        Query qc = (Query) optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, extraCmd, true);
        Assert.assertTrue(qc instanceof MergeQuery);

        QueryWithIndex queryWithIndex1 = (QueryWithIndex) ((MergeQuery) qc).getExecutePlanList().get(0);
        QueryWithIndex queryWithIndex2 = (QueryWithIndex) ((MergeQuery) qc).getExecutePlanList().get(1);

        Function subquery1 = (Function) ((List) queryWithIndex1.getSubQueryFilter().getArgList().get(1)).get(0);
        Function subquery2 = (Function) ((List) queryWithIndex2.getSubQueryFilter().getArgList().get(1)).get(0);
        Assert.assertTrue(subquery1 == subquery2);
        Assert.assertTrue((drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.Join) subquery1.getArgList().get(0) instanceof drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.Join);
    }

    @Test
    public void testQuery_子查询_not_in模式() throws SqlParserException {
        String sql = "SELECT * FROM TABLE1 WHERE ID NOT in (SELECT ID FROM TABLE2 WHERE TABLE2.NAME = TABLE1.NAME)";
        Query qc = (Query) optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, extraCmd, true);
        Assert.assertTrue(qc instanceof MergeQuery);

        QueryWithIndex queryWithIndex1 = (QueryWithIndex) ((MergeQuery) qc).getExecutePlanList().get(0);
        QueryWithIndex queryWithIndex2 = (QueryWithIndex) ((MergeQuery) qc).getExecutePlanList().get(1);

        Function subquery1 = (Function) ((List) queryWithIndex1.getSubQueryFilter().getArgList().get(1)).get(0);
        Function subquery2 = (Function) ((List) queryWithIndex2.getSubQueryFilter().getArgList().get(1)).get(0);
        Assert.assertTrue(subquery1 == subquery2);
        Assert.assertTrue((QueryWithIndex) subquery1.getArgList().get(0) instanceof QueryWithIndex);
    }

    @Test
    public void testQuery_子查询correlated_in模式() throws SqlParserException {
        String sql = "SELECT * FROM TABLE1 WHERE ID in (SELECT ID FROM TABLE2 WHERE TABLE2.NAME = TABLE1.NAME)";
        Query qc = (Query) optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, extraCmd, true);
        Assert.assertTrue(qc instanceof MergeQuery);

        QueryWithIndex queryWithIndex1 = (QueryWithIndex) ((MergeQuery) qc).getExecutePlanList().get(0);
        QueryWithIndex queryWithIndex2 = (QueryWithIndex) ((MergeQuery) qc).getExecutePlanList().get(1);

        Function subquery1 = (Function) ((List) queryWithIndex1.getSubQueryFilter().getArgList().get(1)).get(0);
        Function subquery2 = (Function) ((List) queryWithIndex2.getSubQueryFilter().getArgList().get(1)).get(0);
        Assert.assertTrue(subquery1 == subquery2);
        Assert.assertTrue(subquery1.getArgList().get(0) instanceof QueryWithIndex);
    }

    @Test
    public void testQuery_子查询_exist模式() throws SqlParserException {
        String sql = "SELECT * FROM TABLE1 WHERE  exists (SELECT ID FROM TABLE2 WHERE TABLE2.NAME = TABLE1.NAME)";
        Query qc = (Query) optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, extraCmd, true);
        Assert.assertTrue(qc instanceof MergeQuery);

        QueryWithIndex queryWithIndex1 = (QueryWithIndex) ((MergeQuery) qc).getExecutePlanList().get(0);
        QueryWithIndex queryWithIndex2 = (QueryWithIndex) ((MergeQuery) qc).getExecutePlanList().get(1);

        Function subquery1 = (Function) queryWithIndex1.getSubQueryFilter().getArgList().get(0);
        Function subquery2 = (Function) queryWithIndex2.getSubQueryFilter().getArgList().get(0);
        Assert.assertTrue(subquery1 == subquery2);
        Assert.assertTrue((QueryWithIndex) subquery1.getArgList().get(0) instanceof QueryWithIndex);
    }

    @Test
    public void testQuery_子查询_not_exist模式() throws SqlParserException {
        String sql = "SELECT * FROM TABLE1 WHERE NOT exists (SELECT ID FROM TABLE2 WHERE TABLE2.NAME = TABLE1.NAME)";
        Query qc = (Query) optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, extraCmd, true);
        Assert.assertTrue(qc instanceof MergeQuery);

        QueryWithIndex queryWithIndex1 = (QueryWithIndex) ((MergeQuery) qc).getExecutePlanList().get(0);
        QueryWithIndex queryWithIndex2 = (QueryWithIndex) ((MergeQuery) qc).getExecutePlanList().get(1);

        Function subquery1 = (Function) queryWithIndex1.getSubQueryFilter().getArgList().get(0);
        Function subquery2 = (Function) queryWithIndex2.getSubQueryFilter().getArgList().get(0);
        Assert.assertTrue(subquery1 == subquery2);
        Assert.assertEquals("NOT", subquery1.getFunctionName());
        // 结构为： NOT(func) -> FILTER -> subquery_scalar(func) -> subquery
        Assert.assertTrue((QueryWithIndex) ((Function) ((BooleanFilter) subquery1.getArgList().get(0)).getArgList().get(0)).getArgList().get(0) instanceof QueryWithIndex);
    }

    @Test
    public void testQuery_子查询_all模式() throws SqlParserException {
        String sql = "SELECT * FROM TABLE1 WHERE ID > ALL (SELECT ID FROM TABLE2 WHERE TABLE2.NAME = TABLE1.NAME)";
        Query qc = (Query) optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, extraCmd, true);
        Assert.assertTrue(qc instanceof MergeQuery);

        QueryWithIndex queryWithIndex1 = (QueryWithIndex) ((MergeQuery) qc).getExecutePlanList().get(0);
        QueryWithIndex queryWithIndex2 = (QueryWithIndex) ((MergeQuery) qc).getExecutePlanList().get(1);

        Function subquery1 = (Function) queryWithIndex1.getSubQueryFilter().getArgList().get(1);
        Function subquery2 = (Function) queryWithIndex2.getSubQueryFilter().getArgList().get(1);
        Assert.assertTrue(subquery1 == subquery2);
        Assert.assertTrue(subquery1.getArgList().get(0) instanceof QueryWithIndex);
        QueryWithIndex merge = (QueryWithIndex) subquery1.getArgList().get(0);
        Function func = (Function) merge.getItemList().get(0);
        Assert.assertEquals("MAX", func.getFunctionName());
    }

    @Test
    public void testQuery_子查询_any模式() throws SqlParserException {
        // SubQueryAny
        String sql = "SELECT * FROM TABLE1 WHERE ID <= ANY (SELECT ID FROM TABLE2 WHERE TABLE2.NAME = TABLE1.NAME)";
        Query qc = (Query) optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, extraCmd, true);
        Assert.assertTrue(qc instanceof MergeQuery);

        QueryWithIndex queryWithIndex1 = (QueryWithIndex) ((MergeQuery) qc).getExecutePlanList().get(0);
        QueryWithIndex queryWithIndex2 = (QueryWithIndex) ((MergeQuery) qc).getExecutePlanList().get(1);

        Function subquery1 = (Function) queryWithIndex1.getSubQueryFilter().getArgList().get(1);
        Function subquery2 = (Function) queryWithIndex2.getSubQueryFilter().getArgList().get(1);
        Assert.assertTrue(subquery1 == subquery2);
        Assert.assertTrue(subquery1.getArgList().get(0) instanceof QueryWithIndex);
        QueryWithIndex merge = (QueryWithIndex) subquery1.getArgList().get(0);
        Function func = (Function) merge.getItemList().get(0);
        Assert.assertEquals("MAX", func.getFunctionName());
    }

    @Test
    public void test_lastInsertId() throws SqlParserException {
        String sql = "SELECT LAST_INSERT_ID()";
        Query qc = (Query) optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, extraCmd, true);
        Assert.assertTrue(qc.getSql() != null);
        Assert.assertEquals(ExecutePlan.USE_LAST_DATA_NODE, qc.getDataNodeId());
    }

    @Test
    public void test_nextval() throws SqlParserException {
        String sql = "SELECT TABLE1.NEXTVAL,TABLE1.NEXTVAL,TABLE1.NEXTVAL FROM DUAL";
        Query qc = (Query) optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, extraCmd, true);
        Assert.assertTrue(qc.getSql() == null);
        Assert.assertEquals("[1, 2, 3]", qc.getItemList().toString());
        Assert.assertEquals(Long.valueOf(3), qc.getLastSequenceValue());

        sql = "SELECT * FROM TABLE1 WHERE (ID > 10 or ID < 5) or ID in (TABLE1.NEXTVAL)";
        qc = (Query) optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, extraCmd, true);
        Assert.assertTrue(qc.getSql() == null);
        Assert.assertEquals(Long.valueOf(4), qc.getLastSequenceValue());
    }

    @Test
    public void test_batchNextVal() throws SqlParserException {
        String sql = "insert INTO TABLE1(ID,NAME,SCHOOL) VALUES(TABLE1.NEXTVAL,?,?)";

        Parameters parameters = new Parameters();
        for (int i = 0; i < 10; i++) {
            SetParameterMethodAndArgs p1 = new SetParameterMethodAndArgs(SetParameterMethod.setString, new Object[]{1, "ljh" + i});
            SetParameterMethodAndArgs p2 = new SetParameterMethodAndArgs(SetParameterMethod.setString, new Object[]{2, "school" + i});
            parameters.getIndexToSetParameterMethodAndArgsMap().put(1, p1);
            parameters.getIndexToSetParameterMethodAndArgsMap().put(2, p2);

            parameters.addBatch();
        }

        MergeQuery mergeQuery = (MergeQuery) optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, parameters, extraCmd, true);
        System.out.println(mergeQuery);
    }

    @Test
    public void test_insertSelect_都是分表() {
        String sql = "insert INTO TABLE1 SELECT * FROM TABLE2";
        Query qc = (Query) optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, extraCmd, true);
        Assert.assertTrue(qc instanceof MergeQuery);
        Assert.assertEquals(8, ((MergeQuery) qc).getExecutePlanList().size());
    }

    @Test
    public void test_insertSelect_都是分表_带where条件() {
        String sql = "insert INTO TABLE1(ID,NAME) SELECT ID,NAME FROM TABLE2 WHERE ID in (1,2)";
        Query qc = (Query) optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, extraCmd, true);
        Assert.assertTrue(qc instanceof MergeQuery);
        Assert.assertEquals(2, ((MergeQuery) qc).getExecutePlanList().size());
    }

    @Test
    public void test_insertSelect_都是广播表() {
        String sql = "insert INTO TABLE7 SELECT * FROM TABLE7";
        Query qc = (Query) optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, extraCmd, true);
        Assert.assertTrue(qc instanceof MergeQuery);
        Assert.assertEquals(4, ((MergeQuery) qc).getExecutePlanList().size());
    }
}
