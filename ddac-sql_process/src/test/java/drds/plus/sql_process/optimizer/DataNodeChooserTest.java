package drds.plus.sql_process.optimizer;

import drds.plus.common.properties.ConnectionProperties;
import drds.plus.sql_process.BaseOptimizerTest;
import drds.plus.sql_process.abstract_syntax_tree.node.Node;
import drds.plus.sql_process.abstract_syntax_tree.node.dml.Dml;
import drds.plus.sql_process.abstract_syntax_tree.node.dml.Insert;
import drds.plus.sql_process.abstract_syntax_tree.node.query.*;
import drds.plus.sql_process.optimizer.chooser.Router;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class DataNodeChooserTest extends BaseOptimizerTest {

    @Test
    public void test_单表查询_不生成merge() {
        TableQueryWithIndex table7 = new TableQueryWithIndex("TABLE7");
        table7.build();
        Query qtn = shard(table7, false, false);

        Assert.assertTrue(qtn instanceof TableQueryWithIndex);
        Assert.assertEquals("andor_group_0", qtn.getDataNodeId());
        Assert.assertEquals("table7", ((TableQueryWithIndex) qtn).getActualTableName());
    }

    @Test
    public void test_单表查询_生成merge() {
        TableQueryWithIndex table7 = new TableQueryWithIndex("TABLE1");
        table7.build();
        Query qtn = shard(table7, false, false);
        Assert.assertTrue(qtn instanceof MergeQuery);
        Assert.assertEquals(8, qtn.getNodeList().size());
    }

    @Test
    public void test_Join查询_左右都是单表() {
        TableQueryWithIndex table1 = new TableQueryWithIndex("TABLE1");
        table1.indexQueryKeyFilter("ID = 1");
        TableQueryWithIndex table2 = new TableQueryWithIndex("TABLE7");

        Join join = table1.join(table2, "ID", "ID");
        join.build();
        Query qtn = shard(join, false, false);

        Assert.assertTrue(qtn instanceof Join);
        Assert.assertEquals("andor_group_0", ((Join) qtn).getLeftNode().getDataNodeId());
        Assert.assertEquals("andor_group_0", ((Join) qtn).getRightNode().getDataNodeId());
        Assert.assertEquals("table1_01", ((TableQueryWithIndex) ((Join) qtn).getLeftNode()).getActualTableName());
        Assert.assertEquals("table7", ((TableQueryWithIndex) ((Join) qtn).getRightNode()).getActualTableName());
    }

    @Test
    public void test_子查询_不生成merge() {
        TableQueryWithIndex table7 = new TableQueryWithIndex("TABLE7");
        table7.build();

        $Query$ query = new $Query$(table7);
        Query qtn = shard(query, false, false);

        Assert.assertTrue(qtn instanceof $Query$);
        Assert.assertEquals("andor_group_0", qtn.getDataNodeId());
        Assert.assertEquals("andor_group_0", qtn.getFirstSubNodeQueryNode().getDataNodeId());
        Assert.assertEquals("table7", ((TableQueryWithIndex) qtn.getFirstSubNodeQueryNode()).getActualTableName());
    }

    @Test
    public void test_子查询_生成merge() {
        TableQueryWithIndex table7 = new TableQueryWithIndex("TABLE1");
        table7.build();

        $Query$ query = new $Query$(table7);
        Query qtn = shard(query, false, false);

        // Merge的query
        Assert.assertTrue(qtn instanceof MergeQuery);
        Assert.assertTrue(qtn.getFirstSubNodeQueryNode() instanceof $Query$);
        Assert.assertEquals(8, qtn.getNodeList().size());
    }

    @Test
    public void test_子查询_不生成merge_存在聚合查询() {
        TableQueryWithIndex table7 = new TableQueryWithIndex("TABLE1");
        table7.limit(0, 1);
        table7.build();

        $Query$ query = new $Query$(table7);
        Query qtn = shard(query, false, false);

        Assert.assertTrue(qtn instanceof $Query$);
        Assert.assertTrue(qtn.getFirstSubNodeQueryNode() instanceof MergeQuery);
        Assert.assertEquals(8, ((MergeQuery) qtn.getFirstSubNodeQueryNode()).getNodeList().size());
    }

    @Test
    public void test_join查询_不生成merge() {
        TableQueryWithIndex table1 = new TableQueryWithIndex("TABLE1");
        table1.indexQueryKeyFilter("ID = 1");
        TableQueryWithIndex table2 = new TableQueryWithIndex("TABLE2");
        table2.indexQueryKeyFilter("ID = 1");

        Join join = table1.join(table2, "ID", "ID");
        join.build();
        Query qtn = shard(join, false, false);

        Assert.assertTrue(qtn instanceof Join);
        Assert.assertEquals("andor_group_0", ((Join) qtn).getLeftNode().getDataNodeId());
        Assert.assertEquals("andor_group_0", ((Join) qtn).getRightNode().getDataNodeId());
        Assert.assertEquals("table1_01", ((TableQueryWithIndex) ((Join) qtn).getLeftNode()).getActualTableName());
        Assert.assertEquals("table2_01", ((TableQueryWithIndex) ((Join) qtn).getRightNode()).getActualTableName());
    }

    @Test
    public void test_join查询_生成MergeJoinMerge_没设置条件全表扫描() {
        TableQueryWithIndex table1 = new TableQueryWithIndex("TABLE1");
        TableQueryWithIndex table2 = new TableQueryWithIndex("TABLE2");

        Join join = table1.join(table2, "ID", "ID");
        join.build();
        Query qtn = shard(join, false, false);

        Assert.assertTrue(qtn instanceof Join);
        Assert.assertTrue(((Join) qtn).getLeftNode() instanceof MergeQuery);
        Assert.assertTrue(((Join) qtn).getRightNode() instanceof MergeQuery);
        Assert.assertEquals(8, ((Join) qtn).getLeftNode().getNodeList().size());
        Assert.assertEquals(1, ((Join) qtn).getRightNode().getNodeList().size());// 右边是一个未决节点
    }

    @Test
    public void test_join查询_生成JoinMergeJoin_设置filter局部表join_开启优化参数() {
        TableQueryWithIndex table1 = new TableQueryWithIndex("TABLE1");
        table1.indexQueryKeyFilter("TABLE1.ID in (1,2,3)");
        TableQueryWithIndex table2 = new TableQueryWithIndex("TABLE2");
        table2.indexQueryKeyFilter("TABLE2.ID in (1,2,3)");

        Join join = table1.join(table2, "ID", "ID");
        join.build();
        Query qtn = shard(join, false, true);

        Assert.assertTrue(qtn instanceof MergeQuery);
        Assert.assertEquals(3, qtn.getNodeList().size());
        Assert.assertTrue(qtn.getFirstSubNodeQueryNode() instanceof Join);// join sort join
    }

    @Test
    public void test_join查询_生成JoinMergeJoin_没设置条件全表扫描_开启优化参数() {
        TableQueryWithIndex table1 = new TableQueryWithIndex("TABLE1");
        TableQueryWithIndex table2 = new TableQueryWithIndex("TABLE2");

        Join join = table1.join(table2, "ID", "ID");
        join.build();
        Query qtn = shard(join, false, true);

        Assert.assertTrue(qtn instanceof MergeQuery);
        Assert.assertEquals(8, qtn.getNodeList().size());
        Assert.assertTrue(qtn.getFirstSubNodeQueryNode() instanceof Join);// join sort join
    }

    @Test
    public void test_join查询_不生成JoinMergeJoin_join条件不是分区字段_开启优化参数() {
        TableQueryWithIndex table1 = new TableQueryWithIndex("TABLE1");
        TableQueryWithIndex table2 = new TableQueryWithIndex("TABLE2");

        Join join = table1.join(table2, "NAME", "NAME");
        join.build();
        Query qtn = shard(join, false, true);

        Assert.assertTrue(qtn instanceof Join); // 不做join sort join优化
        Assert.assertTrue(((Join) qtn).getLeftNode() instanceof MergeQuery);
        Assert.assertTrue(((Join) qtn).getRightNode() instanceof MergeQuery);
        Assert.assertEquals(8, ((Join) qtn).getLeftNode().getNodeList().size());
        Assert.assertEquals(1, ((Join) qtn).getRightNode().getNodeList().size());// 右边是一个未决节点
    }

    @Test
    public void test_join查询_生成JoinMergeJoin_嵌套子查询和Join带条件局部join_开启优化参数() {
        TableQueryWithIndex table1 = new TableQueryWithIndex("TABLE1");
        table1.setAliasAndSetNeedBuild("A");
        $Query$ query1 = new $Query$(table1);
        query1.setSelectItemListAndSetNeedBuild("A.ID AS AID , A.NAME AS ANAME , A.SCHOOL AS ASCHOOL");

        TableQueryWithIndex table2 = new TableQueryWithIndex("TABLE2");
        table2.setAliasAndSetNeedBuild("B");
        $Query$ query2 = new $Query$(table2);
        query2.setSelectItemListAndSetNeedBuild("B.ID AS BID , B.NAME AS BNAME , B.SCHOOL AS BSCHOOL");

        Join join = query1.join(query2, "AID", "BID");// 用的是子表的别名
        join.build();
        Query qtn = shard(join, false, true);

        Assert.assertTrue(qtn instanceof MergeQuery);
        Assert.assertEquals(8, qtn.getNodeList().size());
        Assert.assertTrue(qtn.getFirstSubNodeQueryNode() instanceof Join);// join sort join
        Assert.assertTrue(((Join) qtn.getFirstSubNodeQueryNode()).getFirstSubNodeQueryNode() instanceof $Query$);
    }

    @Test
    public void test_join查询_生成JoinMergeJoin_嵌套子查询全表扫描_开启优化参数() {
        TableQueryWithIndex table1 = new TableQueryWithIndex("TABLE1");
        table1.indexQueryKeyFilter("TABLE1.ID in (1,2,3)");
        table1.setAliasAndSetNeedBuild("A");
        $Query$ query1 = new $Query$(table1);
        query1.setSelectItemListAndSetNeedBuild("A.ID AS AID , A.NAME AS ANAME , A.SCHOOL AS ASCHOOL");

        TableQueryWithIndex table2 = new TableQueryWithIndex("TABLE2");
        table2.indexQueryKeyFilter("TABLE2.ID in (1,2,3)");

        TableQueryWithIndex table3 = new TableQueryWithIndex("TABLE3");
        table3.indexQueryKeyFilter("TABLE3.ID in (1,2,3)");

        Join join = table2.join(table3, "ID", "ID");

        // 两层join，左边是query，右边是join
        Join nextJoin = query1.join(join, "AID", "TABLE2.ID");// 用的是子表的别名
        nextJoin.build();
        Query qtn = shard(nextJoin, false, true);

        Assert.assertTrue(qtn instanceof MergeQuery);
        Assert.assertEquals(3, qtn.getNodeList().size());
        Assert.assertTrue(qtn.getFirstSubNodeQueryNode() instanceof Join);// join sort join
        Assert.assertTrue(((Join) qtn.getFirstSubNodeQueryNode()).getLeftNode() instanceof $Query$);
        Assert.assertTrue(((Join) qtn.getFirstSubNodeQueryNode()).getRightNode() instanceof Join);
    }

    @Test
    public void test_join查询_不生成JoinMergeJoin_嵌套子查询全表扫描_存在聚合查询_开启优化参数() {
        TableQueryWithIndex table1 = new TableQueryWithIndex("TABLE1");
        table1.indexQueryKeyFilter("TABLE1.ID in (1,2,3)");
        table1.setAliasAndSetNeedBuild("A");
        table1.limit(0, 1);// 需要聚合操作
        $Query$ query1 = new $Query$(table1);
        query1.setSelectItemListAndSetNeedBuild("A.ID AS AID , A.NAME AS ANAME , A.SCHOOL AS ASCHOOL");

        TableQueryWithIndex table2 = new TableQueryWithIndex("TABLE2");
        table2.indexQueryKeyFilter("TABLE2.ID in (1,2,3)");

        TableQueryWithIndex table3 = new TableQueryWithIndex("TABLE3");
        table3.indexQueryKeyFilter("TABLE3.ID in (1,2,3)");

        Join join = table2.join(table3, "ID", "ID");

        // 两层join，左边是query，右边是join
        Join nextJoin = query1.join(join, "AID", "TABLE2.ID");// 用的是子表的别名
        nextJoin.build();
        Query qtn = shard(nextJoin, false, true);

        Assert.assertTrue(qtn instanceof Join);
        Assert.assertTrue(((Join) qtn).getLeftNode() instanceof $Query$);
        Assert.assertEquals(3, ((MergeQuery) ((Join) qtn).getLeftNode().getFirstSubNodeQueryNode()).getNodeList().size());
        Assert.assertTrue(((Join) qtn).getRightNode() instanceof MergeQuery);
    }

    @Test
    public void test_join查询_不生成JoinMergeJoin_左边是不是Merge_开启优化参数() {
        TableQueryWithIndex table1 = new TableQueryWithIndex("TABLE8");// 这是一个单表，不分库
        table1.indexQueryKeyFilter("TABLE8.ID in (1,2,3)");
        TableQueryWithIndex table2 = new TableQueryWithIndex("TABLE2");// 分库表
        table2.indexQueryKeyFilter("TABLE2.ID in (1,2,3)");

        Join join = table1.join(table2, "ID", "ID");
        join.build();
        Query qtn = shard(join, false, true);

        Assert.assertTrue(qtn instanceof Join);
        Assert.assertTrue(((Join) qtn).getLeftNode() instanceof TableQueryWithIndex);
        Assert.assertTrue(((Join) qtn).getRightNode() instanceof MergeQuery);
        Assert.assertEquals(1, ((Join) qtn).getRightNode().getNodeList().size());// 因为是个未决节点
    }

    @Test
    public void test_join查询_不生成JoinMergeJoin_左边是不是Merge_右边是子查询_开启优化参数() {
        TableQueryWithIndex table1 = new TableQueryWithIndex("TABLE8");// 这是一个单表，不分库
        table1.indexQueryKeyFilter("TABLE8.ID in (1,2,3)");
        TableQueryWithIndex table2 = new TableQueryWithIndex("TABLE2");// 分库表
        table2.indexQueryKeyFilter("TABLE2.ID in (1,2,3)");
        $Query$ query = new $Query$(table2);
        query.setSubQueryAndSetNeedBuild(true); // 是个子查询，会进行BLOCK_NEST_LOOP处理，就不需要未决节点进行mget处理

        Join join = table1.join(query, "ID", "ID");
        join.build();
        Query qtn = shard(join, false, true);

        Assert.assertTrue(qtn instanceof Join);
        Assert.assertTrue(((Join) qtn).getLeftNode() instanceof TableQueryWithIndex);
        Assert.assertTrue(((Join) qtn).getRightNode() instanceof MergeQuery);
        Assert.assertTrue(((Join) qtn).getRightNode().getFirstSubNodeQueryNode() instanceof $Query$);
        Assert.assertEquals(3, ((Join) qtn).getRightNode().getNodeList().size());// 是个子查询的Merge
    }

    @Test
    public void test_join查询_不生成JoinMergeJoin_左右分区结果不一致_开启优化参数() {
        TableQueryWithIndex table1 = new TableQueryWithIndex("TABLE1");// 这是一个单表，不分库
        table1.indexQueryKeyFilter("TABLE1.ID in (2,3,4)");
        TableQueryWithIndex table2 = new TableQueryWithIndex("TABLE2");// 分库表
        table2.indexQueryKeyFilter("TABLE2.ID in (1,2,3)");

        Join join = table1.join(table2, "ID", "ID");
        join.build();
        Query qtn = shard(join, false, true);

        Assert.assertTrue(qtn instanceof Join);
        Assert.assertTrue(((Join) qtn).getLeftNode() instanceof MergeQuery);
        Assert.assertTrue(((Join) qtn).getRightNode() instanceof MergeQuery);
        Assert.assertEquals(3, ((Join) qtn).getLeftNode().getNodeList().size());
        Assert.assertEquals(1, ((Join) qtn).getRightNode().getNodeList().size());// 未决节点
    }

    @Test
    public void test_join查询_生成JoinMergeJoin_左边是广播表_开启优化参数() {
        TableQueryWithIndex table1 = new TableQueryWithIndex("TABLE7");// 这是一个广播表
        $Query$ query = new $Query$(table1);

        TableQueryWithIndex table2 = new TableQueryWithIndex("TABLE2");// 分库表

        Join join = query.join(table2, "ID", "ID");
        join.build();
        Query qtn = shard(join, false, true);

        Assert.assertTrue(qtn instanceof MergeQuery);
        Assert.assertEquals(8, qtn.getNodeList().size());
        Assert.assertTrue(qtn.getFirstSubNodeQueryNode() instanceof Join);// join sort join
        Assert.assertTrue(((Join) qtn.getFirstSubNodeQueryNode()).getLeftNode() instanceof $Query$);
        Assert.assertTrue(((Join) qtn.getFirstSubNodeQueryNode()).getRightNode() instanceof TableQueryWithIndex);
    }

    @Test
    public void test_join查询_生成JoinMergeJoin_右边是广播表_开启优化参数() {
        TableQueryWithIndex table1 = new TableQueryWithIndex("TABLE1");// 分库表
        $Query$ query = new $Query$(table1);

        TableQueryWithIndex table2 = new TableQueryWithIndex("TABLE7");// 这是一个广播表

        Join join = query.join(table2, "ID", "ID");
        join.build();
        Query qtn = shard(join, false, true);

        Assert.assertTrue(qtn instanceof MergeQuery);
        Assert.assertEquals(8, qtn.getNodeList().size());
        Assert.assertTrue(qtn.getFirstSubNodeQueryNode() instanceof Join);// join sort join
        Assert.assertTrue(((Join) qtn.getFirstSubNodeQueryNode()).getLeftNode() instanceof $Query$);
        Assert.assertTrue(((Join) qtn.getFirstSubNodeQueryNode()).getRightNode() instanceof TableQueryWithIndex);
    }

    @Test
    public void test_join查询_不生成JoinMergeJoin_左右都是广播表_开启优化参数() {
        TableQueryWithIndex table1 = new TableQueryWithIndex("TABLE7");// 这是一个广播表
        table1.setAliasAndSetNeedBuild("A");
        $Query$ query = new $Query$(table1);

        TableQueryWithIndex table2 = new TableQueryWithIndex("TABLE7");// 这是一个广播表
        table2.setAliasAndSetNeedBuild("B");

        Join join = query.join(table2, "ID", "ID");
        join.build();
        Query qtn = shard(join, false, true);

        Assert.assertTrue(qtn instanceof Join);
        Assert.assertTrue(((Join) qtn).getLeftNode() instanceof $Query$);
        Assert.assertTrue(((Join) qtn).getRightNode() instanceof TableQueryWithIndex);
    }

    @Test
    public void test_join查询_不生成JoinMergeJoin_不同的joinGroup_开启优化参数() {
        TableQueryWithIndex table1 = new TableQueryWithIndex("TABLE1");// 分库表
        $Query$ query = new $Query$(table1);

        TableQueryWithIndex table2 = new TableQueryWithIndex("TABLE5");// 这是一个广播表

        Join join = query.join(table2, "ID", "ID");
        join.build();
        Query qtn = shard(join, false, true);

        Assert.assertTrue(qtn instanceof Join);
        Assert.assertTrue(((Join) qtn).getLeftNode() instanceof MergeQuery);
        Assert.assertTrue(((Join) qtn).getRightNode() instanceof MergeQuery);
        Assert.assertEquals(8, ((Join) qtn).getLeftNode().getNodeList().size());
        Assert.assertEquals(1, ((Join) qtn).getRightNode().getNodeList().size());// 未决节点
    }

    @Test
    public void test_join查询_生成JoinMergeJoin_左右joinGroup都是other_开启优化参数() {
        TableQueryWithIndex table1 = new TableQueryWithIndex("TABLE5");// 分库表
        $Query$ query = new $Query$(table1);

        TableQueryWithIndex table2 = new TableQueryWithIndex("TABLE6");// 这是一个广播表

        Join join = query.join(table2, "ID", "ID");
        join.build();
        Query qtn = shard(join, false, true);

        Assert.assertTrue(qtn instanceof MergeQuery);
        Assert.assertEquals(4, qtn.getNodeList().size());
        Assert.assertTrue(qtn.getFirstSubNodeQueryNode() instanceof Join);// join sort join
        Assert.assertTrue(((Join) qtn.getFirstSubNodeQueryNode()).getLeftNode() instanceof $Query$);
        Assert.assertTrue(((Join) qtn.getFirstSubNodeQueryNode()).getRightNode() instanceof TableQueryWithIndex);
    }

    @Test
    public void test_join查询_生成JoinMergeJoin_一张广播表_一张单库表_开启优化参数() {
        TableQueryWithIndex table1 = new TableQueryWithIndex("TABLE7");// 广播表
        $Query$ query = new $Query$(table1);

        TableQueryWithIndex table2 = new TableQueryWithIndex("TABLE8");// 一张单表
        TableQueryWithIndex table3 = new TableQueryWithIndex("TABLE1");// 分库表
        Join table4 = table2.join(table3, "ID", "ID");

        Join join = query.join(table4, "ID", "TABLE1.ID");
        join.build();
        Query qtn = shard(join, false, true);
        Assert.assertTrue(qtn instanceof Join);
    }

    @Test
    public void test_insertSelect查询_生成merge_没设置条件全表扫描() {
        TableQueryWithIndex table1 = new TableQueryWithIndex("TABLE1");
        TableQueryWithIndex table2 = new TableQueryWithIndex("TABLE2");
        Insert insertNode = new Insert(table1);
        insertNode.setQuery(table2);
        insertNode.build();
        Node qtn = shard(insertNode);

        Assert.assertTrue(qtn instanceof MergeQuery);
        Assert.assertTrue(((MergeQuery) qtn).getFirstSubNodeQueryNode() instanceof Insert);
        Assert.assertEquals(8, ((MergeQuery) qtn).getNodeList().size());
    }

    @Test
    public void test_insertSelect查询_生成merge_设置where条件() {
        TableQueryWithIndex table1 = new TableQueryWithIndex("TABLE1");
        TableQueryWithIndex table2 = new TableQueryWithIndex("TABLE2");
        table2.indexQueryKeyFilter("ID in (2,3)");
        Insert insertNode = new Insert(table1);
        insertNode.setQuery(table2);
        insertNode.build();
        Node qtn = shard(insertNode);

        Assert.assertTrue(qtn instanceof MergeQuery);
        Assert.assertTrue(((MergeQuery) qtn).getFirstSubNodeQueryNode() instanceof Insert);
        Assert.assertEquals(2, ((MergeQuery) qtn).getNodeList().size());
    }

    @Test
    public void test_insertSelect查询_select为join查询_生成merge_设置where条件() {
        TableQueryWithIndex table1 = new TableQueryWithIndex("TABLE1");

        TableQueryWithIndex table2 = new TableQueryWithIndex("TABLE2");
        table2.indexQueryKeyFilter("ID in (2,3)");
        TableQueryWithIndex table3 = new TableQueryWithIndex("TABLE3");
        table3.indexQueryKeyFilter("ID in (2,3)");
        Join join = table2.join(table3, "ID", "ID");
        join.setSelectItemListAndSetNeedBuild("TABLE2.*");

        Insert insertNode = new Insert(table1);
        insertNode.setQuery(join);
        insertNode.build();
        Node qtn = shard(insertNode);

        Assert.assertTrue(qtn instanceof MergeQuery);
        Assert.assertTrue(((MergeQuery) qtn).getFirstSubNodeQueryNode() instanceof Insert);
        Assert.assertEquals(2, ((MergeQuery) qtn).getNodeList().size());
    }

    @Test
    public void test_insertSelect查询_select为join_query查询_生成merge_设置where条件() {
        TableQueryWithIndex table1 = new TableQueryWithIndex("TABLE1");

        TableQueryWithIndex table2 = new TableQueryWithIndex("TABLE2");
        table2.indexQueryKeyFilter("ID in (2,3)");

        TableQueryWithIndex table3 = new TableQueryWithIndex("TABLE3");
        table3.indexQueryKeyFilter("ID in (2,3)");
        $Query$ query = new $Query$(table3);
        Join join = table2.join(query, "ID", "ID");
        join.setSelectItemListAndSetNeedBuild("TABLE2.*");

        Insert insertNode = new Insert(table1);
        insertNode.setQuery(join);
        insertNode.build();
        Node qtn = shard(insertNode);

        Assert.assertTrue(qtn instanceof MergeQuery);
        Assert.assertTrue(((MergeQuery) qtn).getFirstSubNodeQueryNode() instanceof Insert);
        Assert.assertEquals(2, ((MergeQuery) qtn).getNodeList().size());
    }

    @Test
    public void test_insertSelect查询_select广播表_生成merge_设置where条件() {
        TableQueryWithIndex table1 = new TableQueryWithIndex("TABLE1");
        TableQueryWithIndex table2 = new TableQueryWithIndex("TABLE7");// 广播表
        table2.indexQueryKeyFilter("ID in (2,3)");
        Insert insertNode = new Insert(table1);
        insertNode.setQuery(table2);
        insertNode.build();
        Node qtn = shard(insertNode);

        // 针对广播表和非广播表之间操作，直接展开
        Assert.assertTrue(qtn instanceof MergeQuery);
        Assert.assertTrue(((MergeQuery) qtn).getFirstSubNodeQueryNode() instanceof Insert);
        Assert.assertEquals(8, ((MergeQuery) qtn).getNodeList().size());
    }

    @Test
    public void test_insertSelect查询_都是广播表_生成merge_设置where条件() {
        TableQueryWithIndex table1 = new TableQueryWithIndex("TABLE7");
        TableQueryWithIndex table2 = new TableQueryWithIndex("TABLE7");// 广播表
        table2.indexQueryKeyFilter("ID in (2,3)");
        Insert insertNode = new Insert(table1);
        insertNode.setQuery(table2);
        insertNode.build();
        Node qtn = shard(insertNode);

        // 针对广播表和非广播表之间操作，直接展开
        Assert.assertTrue(qtn instanceof MergeQuery);
    }

    @Test
    public void test_insertSelect查询_select不同joinGroup_生成merge_设置where条件() {
        TableQueryWithIndex table1 = new TableQueryWithIndex("TABLE1");
        TableQueryWithIndex table2 = new TableQueryWithIndex("TABLE5");// 广播表
        Insert insertNode = new Insert(table1);
        insertNode.setQuery(table2);
        insertNode.build();
        try {
            shard(insertNode);
            Assert.fail();
        } catch (Exception e) {
            // joinGroup不同，不允许
        }
    }

    @Test
    public void test_insertSelect查询_other的joinGroup_生成merge_设置where条件() {
        TableQueryWithIndex table1 = new TableQueryWithIndex("TABLE5");
        TableQueryWithIndex table2 = new TableQueryWithIndex("TABLE6");
        table2.indexQueryKeyFilter("ID in (2,3)");
        Insert insertNode = new Insert(table1);
        insertNode.setQuery(table2);
        insertNode.build();
        Node qtn = shard(insertNode);

        Assert.assertTrue(qtn instanceof MergeQuery);
        Assert.assertTrue(((MergeQuery) qtn).getFirstSubNodeQueryNode() instanceof Insert);
        Assert.assertEquals(2, ((MergeQuery) qtn).getNodeList().size());
    }

    @Test
    public void test_insertSelect查询_一张广播表_一张单表_生成merge_设置where条件() {
        TableQueryWithIndex table1 = new TableQueryWithIndex("TABLE7");
        TableQueryWithIndex table2 = new TableQueryWithIndex("TABLE8");
        table2.indexQueryKeyFilter("ID in (2,3)");
        Insert insertNode = new Insert(table1);
        insertNode.setQuery(table2);
        insertNode.build();
        Node qtn = shard(insertNode);
        Assert.assertTrue(qtn instanceof Insert);
    }

    @Test
    public void test_insertSelect查询_都是单表_生成merge_设置where条件() {
        TableQueryWithIndex table1 = new TableQueryWithIndex("TABLE8");
        TableQueryWithIndex table2 = new TableQueryWithIndex("TABLE8");
        table2.indexQueryKeyFilter("ID in (2,3)");
        Insert insertNode = new Insert(table1);
        insertNode.setQuery(table2);
        insertNode.build();
        Node qtn = shard(insertNode);
        Assert.assertTrue(qtn instanceof Insert);
    }

    private Query shard(Query qtn, boolean joinMergeJoin, boolean joinMergeJoinByRule) {
        Map<String, Object> extraCmd = new HashMap<String, Object>();
        extraCmd.put(ConnectionProperties.JOIN_MERGE_JOIN, joinMergeJoin);
        extraCmd.put(ConnectionProperties.JOIN_MERGE_JOIN_JUDGE_BY_RULE, joinMergeJoinByRule);
        return (Query) Router.route(qtn, null, extraCmd);
    }

    private Node shard(Dml qtn) {
        return Router.route(qtn, null, null);
    }
}
