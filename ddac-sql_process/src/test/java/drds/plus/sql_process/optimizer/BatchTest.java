package drds.plus.sql_process.optimizer;

import drds.plus.common.jdbc.Parameters;
import drds.plus.common.jdbc.SetParameterMethod;
import drds.plus.common.jdbc.SetParameterMethodAndArgs;
import drds.plus.sql_process.BaseOptimizerTest;
import drds.plus.sql_process.abstract_syntax_tree.execute_plan.ExecutePlan;
import drds.plus.sql_process.abstract_syntax_tree.execute_plan.dml.*;
import drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.MergeQuery;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 批处理测试
 *
 * @author jianghang 2014-3-5 下午11:19:48
 * @since 5.0.0
 */
public class BatchTest extends BaseOptimizerTest {

    @Test
    public void testInsert() {
        String sql = "insert INTO TABLE1(ID) VALUES(?)";

//        Map<Integer, SetParameterMethodAndArgs> currentParameter = new HashMap<Integer, SetParameterMethodAndArgs>();
//        SetParameterMethodAndArgs p1 = new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{2});
//        currentParameter.put(1, p1);

        Parameters parameters = new Parameters();
        parameters.getIndexToSetParameterMethodAndArgsMap().put(1, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{1, 1}));
        parameters.addBatch();

        parameters.getIndexToSetParameterMethodAndArgsMap().put(1, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{1, 2}));
        parameters.addBatch();

        ExecutePlan plan = optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, parameters, null, false);
        Assert.assertTrue(plan instanceof MergeQuery);
        Assert.assertEquals(((Insert) ((MergeQuery) plan).getExecutePlanList().get(0)).getBatchIndexList(), Arrays.asList(0));
        Assert.assertEquals(((Insert) ((MergeQuery) plan).getExecutePlanList().get(1)).getBatchIndexList(), Arrays.asList(1));
    }

    @Test
    public void testInsert_广播表() {
        String sql = "insert INTO TABLE7(ID) VALUES(?)";

//        Map<Integer, SetParameterMethodAndArgs> currentParameter = new HashMap<Integer, SetParameterMethodAndArgs>();
//        SetParameterMethodAndArgs p1 = new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{2});
//        currentParameter.put(1, p1);

        Parameters parameters = new Parameters();
        parameters.getIndexToSetParameterMethodAndArgsMap().put(1, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{1, 1}));
        parameters.addBatch();

        parameters.getIndexToSetParameterMethodAndArgsMap().put(1, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{1, 2}));
        parameters.addBatch();

        ExecutePlan plan = optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, parameters, null, false);
        Assert.assertTrue(plan instanceof MergeQuery);
        Assert.assertEquals(4, ((MergeQuery) plan).getExecutePlanList().size());
    }

    @Test
    public void testInsert_自增id() {
        String sql = "insert INTO AUTOINC(NAME) VALUES(?)";

//        Map<Integer, SetParameterMethodAndArgs> currentParameter = new HashMap<Integer, SetParameterMethodAndArgs>();
//        SetParameterMethodAndArgs p1 = new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{2});
//        currentParameter.put(1, p1);

        Parameters parameters = new Parameters();
        parameters.getIndexToSetParameterMethodAndArgsMap().put(1, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{1, "hello1"}));
        parameters.addBatch();

        parameters.getIndexToSetParameterMethodAndArgsMap().put(1, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{1, "hello2"}));
        parameters.addBatch();

        ExecutePlan plan = optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, parameters, null, false);
        Assert.assertTrue(plan instanceof MergeQuery);
        Assert.assertEquals(((Insert) ((MergeQuery) plan).getExecutePlanList().get(0)).getBatchIndexList(), Arrays.asList(0));
        Assert.assertEquals(((Insert) ((MergeQuery) plan).getExecutePlanList().get(1)).getBatchIndexList(), Arrays.asList(1));
    }

    @Test
    public void testInsert_自增id_绑定变量为null() {
        String sql = "insert INTO AUTOINC(ID,NAME) VALUES(?,?)";

//        Map<Integer, SetParameterMethodAndArgs> currentParameter = new HashMap<Integer, SetParameterMethodAndArgs>();
//        SetParameterMethodAndArgs p1 = new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{2});
//        currentParameter.put(1, p1);

        Parameters parameters = new Parameters();
        parameters.getIndexToSetParameterMethodAndArgsMap().put(1, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{1, null}));
        parameters.getIndexToSetParameterMethodAndArgsMap().put(2, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{2, "hello1"}));
        parameters.addBatch();

        parameters.getIndexToSetParameterMethodAndArgsMap().put(1, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{1, null}));
        parameters.getIndexToSetParameterMethodAndArgsMap().put(2, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{2, "hello2"}));
        parameters.addBatch();

        ExecutePlan plan = optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, parameters, null, false);
        Assert.assertTrue(plan instanceof MergeQuery);
        Assert.assertEquals(2, ((MergeQuery) plan).getExecutePlanList().size());
        // Assert.assertEquals(((Insert) ((MergeQuery)
        // execute_plan).getExecutePlanList().get(0)).getBatchIndexList(),
        // Arrays.asList(0));
        // Assert.assertEquals(((Insert) ((MergeQuery)
        // execute_plan).getExecutePlanList().get(1)).getBatchIndexList(),
        // Arrays.asList(1));
    }

    @Test
    public void testInsert_多value() {
        String sql = "insert INTO TABLE1(ID) VALUES (?),(?),(?)";

        Parameters parameters = new Parameters();
        parameters.getIndexToSetParameterMethodAndArgsMap().put(1, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{1, 1}));

        parameters.getIndexToSetParameterMethodAndArgsMap().put(2, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{1, 5}));

        parameters.getIndexToSetParameterMethodAndArgsMap().put(3, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{1, 9}));

        ExecutePlan plan = optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, parameters, null, false);
        Assert.assertTrue(plan instanceof MergeQuery);
        List<Long> d1 = new ArrayList<Long>();
        d1.add(1L);
        List<Long> d2 = new ArrayList<Long>();
        d2.add(9L);
        List<Long> d3 = new ArrayList<Long>();
        d3.add(5L);
        Assert.assertEquals(((Insert) ((MergeQuery) plan).getExecutePlanList().get(0)).getValueListList().get(0), d1);
        Assert.assertEquals(((Insert) ((MergeQuery) plan).getExecutePlanList().get(0)).getValueListList().get(1), d2);
        Assert.assertEquals(((Insert) ((MergeQuery) plan).getExecutePlanList().get(1)).getValueListList().get(0), d3);
    }

    @Test
    public void testInsert_单库单表() {
        String sql = "insert INTO STUDENT(ID) VALUES(?)";

//        Map<Integer, SetParameterMethodAndArgs> currentParameter = new HashMap<Integer, SetParameterMethodAndArgs>();
//        SetParameterMethodAndArgs p1 = new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{2});
//        currentParameter.put(1, p1);

        Parameters parameters = new Parameters();
        parameters.getIndexToSetParameterMethodAndArgsMap().put(1, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{1, 1}));
        parameters.addBatch();

        parameters.getIndexToSetParameterMethodAndArgsMap().put(1, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{1, 2}));
        parameters.addBatch();

        ExecutePlan plan = optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, parameters, null, false);
        Assert.assertTrue(plan instanceof Insert);
        Assert.assertEquals(((Insert) plan).getBatchIndexList(), Arrays.asList(0, 1));
    }

    @Test
    public void testInsert_单库单表_多value() {
        String sql = "insert INTO STUDENT(ID) VALUES (?),(?),(?)";

        Parameters parameters = new Parameters();
        parameters.getIndexToSetParameterMethodAndArgsMap().put(1, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{1, 1}));

        parameters.getIndexToSetParameterMethodAndArgsMap().put(2, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{1, 5}));

        parameters.getIndexToSetParameterMethodAndArgsMap().put(3, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{1, 9}));

        ExecutePlan plan = optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, parameters, null, false);
        Assert.assertTrue(plan instanceof Insert);
        Assert.assertTrue(plan.getSql() != null);
    }

    @Test
    public void testPut() {
        String sql = "replace INTO TABLE1(ID) VALUES(?)";

//        Map<Integer, SetParameterMethodAndArgs> currentParameter = new HashMap<Integer, SetParameterMethodAndArgs>();
//        SetParameterMethodAndArgs p1 = new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{2});
//        currentParameter.put(1, p1);

        Parameters parameters = new Parameters();
        parameters.getIndexToSetParameterMethodAndArgsMap().put(1, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{1, 1}));
        parameters.addBatch();

        parameters.getIndexToSetParameterMethodAndArgsMap().put(1, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{1, 2}));
        parameters.addBatch();

        ExecutePlan plan = optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, parameters, null, false);
        Assert.assertTrue(plan instanceof MergeQuery);
        Assert.assertEquals(((Replace) ((MergeQuery) plan).getExecutePlanList().get(0)).getBatchIndexList(), Arrays.asList(0));
        Assert.assertEquals(((Replace) ((MergeQuery) plan).getExecutePlanList().get(1)).getBatchIndexList(), Arrays.asList(1));
    }

    @Test
    public void testPut_广播表() {
        String sql = "replace INTO TABLE7(ID) VALUES(?)";

//        Map<Integer, SetParameterMethodAndArgs> currentParameter = new HashMap<Integer, SetParameterMethodAndArgs>();
//        SetParameterMethodAndArgs p1 = new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{2});
//        currentParameter.put(1, p1);

        Parameters parameters = new Parameters();
        parameters.getIndexToSetParameterMethodAndArgsMap().put(1, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{1, 1}));
        parameters.addBatch();

        parameters.getIndexToSetParameterMethodAndArgsMap().put(1, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{1, 2}));
        parameters.addBatch();

        ExecutePlan plan = optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, parameters, null, false);
        Assert.assertTrue(plan instanceof MergeQuery);
        Assert.assertEquals(4, ((MergeQuery) plan).getExecutePlanList().size());
    }

    @Test
    public void testPut_多value() {
        String sql = "replace INTO TABLE1(ID) VALUES (?),(?),(?)";

        Parameters parameters = new Parameters();
        parameters.getIndexToSetParameterMethodAndArgsMap().put(1, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{1, 1}));

        parameters.getIndexToSetParameterMethodAndArgsMap().put(2, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{1, 5}));

        parameters.getIndexToSetParameterMethodAndArgsMap().put(3, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{1, 9}));

        ExecutePlan plan = optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, parameters, null, false);
        Assert.assertTrue(plan instanceof MergeQuery);
        List<Long> d1 = new ArrayList<Long>();
        d1.add(1L);
        List<Long> d2 = new ArrayList<Long>();
        d2.add(9L);
        List<Long> d3 = new ArrayList<Long>();
        d3.add(5L);
        Assert.assertEquals(((IPut) ((MergeQuery) plan).getExecutePlanList().get(0)).getValueListList().get(0), d1);
        Assert.assertEquals(((IPut) ((MergeQuery) plan).getExecutePlanList().get(0)).getValueListList().get(1), d2);
        Assert.assertEquals(((IPut) ((MergeQuery) plan).getExecutePlanList().get(1)).getValueListList().get(0), d3);
    }

    @Test
    public void testUpdate() {
        String sql = "update TABLE1 SET NAME = ? WHERE ID = ?";

//        Map<Integer, SetParameterMethodAndArgs> currentParameter = new HashMap<Integer, SetParameterMethodAndArgs>();
//        SetParameterMethodAndArgs p1 = new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{2});
//        currentParameter.put(1, p1);

        Parameters parameters = new Parameters();
        parameters.getIndexToSetParameterMethodAndArgsMap().put(1, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{1, "hello1"}));
        parameters.getIndexToSetParameterMethodAndArgsMap().put(2, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{2, 1}));
        parameters.addBatch();

        parameters.getIndexToSetParameterMethodAndArgsMap().put(1, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{1, "hello2"}));
        parameters.getIndexToSetParameterMethodAndArgsMap().put(2, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{2, 2}));
        parameters.addBatch();

        ExecutePlan plan = optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, parameters, null, false);
        Assert.assertTrue(plan instanceof MergeQuery);
        Assert.assertEquals(((Update) ((MergeQuery) plan).getExecutePlanList().get(0)).getBatchIndexList(), Arrays.asList(0));
        Assert.assertEquals(((Update) ((MergeQuery) plan).getExecutePlanList().get(1)).getBatchIndexList(), Arrays.asList(1));
    }

    @Test
    public void testPut_单库单表_多value() {
        String sql = "replace INTO STUDENT(ID) VALUES (?),(?),(?)";

        Parameters parameters = new Parameters();
        parameters.getIndexToSetParameterMethodAndArgsMap().put(1, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{1, 1}));

        parameters.getIndexToSetParameterMethodAndArgsMap().put(2, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{1, 5}));

        parameters.getIndexToSetParameterMethodAndArgsMap().put(3, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{1, 9}));

        ExecutePlan plan = optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, parameters, null, false);
        Assert.assertTrue(plan instanceof IPut);
        Assert.assertTrue(plan.getSql() != null);
    }

    @Test
    public void testUpdate_范围查询() {
        String sql = "update TABLE1 SET NAME = ? WHERE ID > ? and ID < ?";

//        Map<Integer, SetParameterMethodAndArgs> currentParameter = new HashMap<Integer, SetParameterMethodAndArgs>();
//        SetParameterMethodAndArgs p1 = new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{2});
//        currentParameter.put(1, p1);

        Parameters parameters = new Parameters();
        parameters.getIndexToSetParameterMethodAndArgsMap().put(1, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{1, "hello1"}));
        parameters.getIndexToSetParameterMethodAndArgsMap().put(2, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{2, 1}));
        parameters.getIndexToSetParameterMethodAndArgsMap().put(3, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{3, 4}));
        parameters.addBatch();

        parameters.getIndexToSetParameterMethodAndArgsMap().put(1, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{1, "hello2"}));
        parameters.getIndexToSetParameterMethodAndArgsMap().put(2, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{2, 2}));
        parameters.getIndexToSetParameterMethodAndArgsMap().put(3, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{3, 5}));
        parameters.addBatch();

        ExecutePlan plan = optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, parameters, null, false);
        Assert.assertTrue(plan instanceof MergeQuery);
        Assert.assertEquals(((Update) ((MergeQuery) plan).getExecutePlanList().get(0)).getBatchIndexList(), Arrays.asList(0));
        Assert.assertEquals(((Update) ((MergeQuery) plan).getExecutePlanList().get(1)).getBatchIndexList(), Arrays.asList(1));
        Assert.assertEquals(((Update) ((MergeQuery) plan).getExecutePlanList().get(2)).getBatchIndexList(), Arrays.asList(0, 1));
    }

    @Test
    public void testUpdate_in查询() {
        String sql = "update TABLE1 SET NAME = ? WHERE ID in (?,?)";

//        Map<Integer, SetParameterMethodAndArgs> currentParameter = new HashMap<Integer, SetParameterMethodAndArgs>();
//        SetParameterMethodAndArgs p1 = new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{2});
//        currentParameter.put(1, p1);

        Parameters parameters = new Parameters();
        parameters.getIndexToSetParameterMethodAndArgsMap().put(1, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{1, "hello1"}));
        parameters.getIndexToSetParameterMethodAndArgsMap().put(2, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{2, 2}));
        parameters.getIndexToSetParameterMethodAndArgsMap().put(3, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{3, 3}));
        parameters.addBatch();

        parameters.getIndexToSetParameterMethodAndArgsMap().put(1, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{1, "hello2"}));
        parameters.getIndexToSetParameterMethodAndArgsMap().put(2, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{2, 3}));
        parameters.getIndexToSetParameterMethodAndArgsMap().put(3, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{3, 4}));
        parameters.addBatch();

        ExecutePlan plan = optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, parameters, null, false);
        Assert.assertTrue(plan instanceof MergeQuery);
        Assert.assertEquals(((Update) ((MergeQuery) plan).getExecutePlanList().get(0)).getBatchIndexList(), Arrays.asList(0));
        Assert.assertEquals(((Update) ((MergeQuery) plan).getExecutePlanList().get(1)).getBatchIndexList(), Arrays.asList(1));
        Assert.assertEquals(((Update) ((MergeQuery) plan).getExecutePlanList().get(2)).getBatchIndexList(), Arrays.asList(0, 1));
    }

    @Test
    public void testDelete() {
        String sql = "delete FROM TABLE1 WHERE ID = ?";

//        Map<Integer, SetParameterMethodAndArgs> currentParameter = new HashMap<Integer, SetParameterMethodAndArgs>();
//        SetParameterMethodAndArgs p1 = new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{2});
//        currentParameter.put(1, p1);

        Parameters parameters = new Parameters();
        parameters.getIndexToSetParameterMethodAndArgsMap().put(1, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{1, 1}));
        parameters.addBatch();

        parameters.getIndexToSetParameterMethodAndArgsMap().put(1, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{1, 2}));
        parameters.addBatch();

        ExecutePlan plan = optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, parameters, null, false);
        Assert.assertTrue(plan instanceof MergeQuery);
        Assert.assertEquals(((Delete) ((MergeQuery) plan).getExecutePlanList().get(0)).getBatchIndexList(), Arrays.asList(0));
        Assert.assertEquals(((Delete) ((MergeQuery) plan).getExecutePlanList().get(1)).getBatchIndexList(), Arrays.asList(1));
    }

    @Test
    public void testDelete_范围查询() {
        String sql = "delete FROM TABLE1 WHERE ID > ? and ID < ?";

//        Map<Integer, SetParameterMethodAndArgs> currentParameter = new HashMap<Integer, SetParameterMethodAndArgs>();
//        SetParameterMethodAndArgs p1 = new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{2});
//        currentParameter.put(1, p1);

        Parameters parameters = new Parameters();
        parameters.getIndexToSetParameterMethodAndArgsMap().put(1, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{1, 1}));
        parameters.getIndexToSetParameterMethodAndArgsMap().put(2, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{2, 4}));
        parameters.addBatch();

        parameters.getIndexToSetParameterMethodAndArgsMap().put(1, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{1, 2}));
        parameters.getIndexToSetParameterMethodAndArgsMap().put(2, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{2, 5}));
        parameters.addBatch();

        ExecutePlan plan = optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, parameters, null, false);
        Assert.assertTrue(plan instanceof MergeQuery);
        Assert.assertEquals(((Delete) ((MergeQuery) plan).getExecutePlanList().get(0)).getBatchIndexList(), Arrays.asList(0));
        Assert.assertEquals(((Delete) ((MergeQuery) plan).getExecutePlanList().get(1)).getBatchIndexList(), Arrays.asList(1));
        Assert.assertEquals(((Delete) ((MergeQuery) plan).getExecutePlanList().get(2)).getBatchIndexList(), Arrays.asList(0, 1));
    }

    @Test
    public void testDelete_in查询() {
        String sql = "delete FROM TABLE1 WHERE ID in (?,?)";

//        Map<Integer, SetParameterMethodAndArgs> currentParameter = new HashMap<Integer, SetParameterMethodAndArgs>();
//        SetParameterMethodAndArgs p1 = new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{2});
//        currentParameter.put(1, p1);

        Parameters parameters = new Parameters();
        parameters.getIndexToSetParameterMethodAndArgsMap().put(1, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{1, 2}));
        parameters.getIndexToSetParameterMethodAndArgsMap().put(2, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{2, 3}));
        parameters.addBatch();

        parameters.getIndexToSetParameterMethodAndArgsMap().put(1, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{1, 3}));
        parameters.getIndexToSetParameterMethodAndArgsMap().put(2, new SetParameterMethodAndArgs(SetParameterMethod.setObject, new Object[]{2, 4}));
        parameters.addBatch();

        ExecutePlan plan = optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, parameters, null, false);
        Assert.assertTrue(plan instanceof MergeQuery);
        Assert.assertEquals(((Delete) ((MergeQuery) plan).getExecutePlanList().get(0)).getBatchIndexList(), Arrays.asList(0));
        Assert.assertEquals(((Delete) ((MergeQuery) plan).getExecutePlanList().get(1)).getBatchIndexList(), Arrays.asList(1));
        Assert.assertEquals(((Delete) ((MergeQuery) plan).getExecutePlanList().get(2)).getBatchIndexList(), Arrays.asList(0, 1));
    }
}
