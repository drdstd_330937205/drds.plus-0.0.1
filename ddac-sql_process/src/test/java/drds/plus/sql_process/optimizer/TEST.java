package drds.plus.sql_process.optimizer;

public class TEST {

    /**
     * 无重复项
     *
     * @param arr 为待排列的数组
     * @param n   标记数组当前位置，即n=0时，对arr[0]到arr[length-1]全排列；n=1时，对arr[1]到arr[length-1]全排列...以此类推，length为数组长度
     */

    private static void test1(int[] arr, int n) {
        int length = arr.length;
        if (n >= length - 1) {//当n定位到最后一个数时，即到递归出口
            for (int i : arr) {
                System.out.print(i);
            }
            System.out.println();
        } else {
            for (int i = n; i < length; i++) {//将length-n个数分别放在第n个位置，(length-n)对于上文分析中的m
                {
                    int temp = arr[n];
                    arr[n] = arr[i];
                    arr[i] = temp;
                }//以交换位置的方式实现
                test1(arr, n + 1);//对剩下的length-n-1个数全排列
                {
                    int temp = arr[n];
                    arr[n] = arr[i];
                    arr[i] = temp;
                }//恢复原来的顺序，进行下一次交换
            }
        }
    }

    public static void main(String args[]) {
        int[] arr = {1, 2, 3, 4};
        test1(arr, 0);
        System.out.println(arr);
    }

}
