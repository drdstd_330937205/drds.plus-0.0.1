package drds.plus.sql_process.optimizer;

import drds.plus.common.model.SqlType;
import drds.plus.sql_process.BaseOptimizerTest;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.Function;
import drds.plus.sql_process.abstract_syntax_tree.node.query.Query;
import drds.plus.sql_process.abstract_syntax_tree.node.query.TableQueryWithIndex;
import drds.plus.sql_process.optimizer.pre_processor.FilterPreProcessor;
import drds.plus.sql_process.optimizer.pre_processor.SubQueryPreProcessor;
import drds.plus.sql_process.parser.ParseInfo;
import drds.plus.sql_process.parser.SqlParserException;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SubQueryPreProcessorTest extends BaseOptimizerTest {

    @Test
    public void testQuery_子查询_in模式() throws SqlParserException {
        String sql = "SELECT * FROM TABLE1 WHERE (ID > 10 or ID < 5) or  ID in (SELECT ID FROM TABLE2 WHERE ID > 10 or ID < 5)";
        Query qn = query(sql);
        FilterPreProcessor.optimize(qn, true, null);
        qn.build();

        Function func = SubQueryPreProcessor.getNextSubQueryFunction(qn);
        Assert.assertTrue(func != null);

        Query qtn = (Query) func.getArgList().get(0);
        Map<Long, Object> subquerySettings = new HashMap<Long, Object>();
        subquerySettings.put(qtn.getSubQueryId(), Arrays.asList(1, 2));
        func = SubQueryPreProcessor.subQueryAssignValueAndReturnNextSubQueryFunction(qn, subquerySettings);
        FilterPreProcessor.optimize(qn, true, null);
        Assert.assertTrue(func == null);
    }

    @Test
    public void testQuery_子查询_correlate_in模式() throws SqlParserException {
        String sql = "SELECT * FROM TABLE1 WHERE ID in (SELECT ID FROM TABLE2 WHERE TABLE2.NAME = TABLE1.NAME)";
        Query qn = query(sql);
        qn.build();

        Function func = SubQueryPreProcessor.getNextSubQueryFunction(qn);
        Assert.assertTrue(func == null);
    }

    @Test
    public void testQuery_子查询_多级查询() throws SqlParserException {
        String sql = "SELECT * FROM TABLE1 WHERE ID in (SELECT ID FROM TABLE2 WHERE NAME = (SELECT NAME FROM TABLE3))";
        sql += " and NAME = (SELECT NAME FROM TABLE4) and SCHOOL > ALL (SELECT SCHOOL FROM TABLE5)";
        Query qn = query(sql);
        qn.build();

        List<Function> funcs = SubQueryPreProcessor.findSubQueryFunctionList(qn, true);
        Assert.assertEquals(4, funcs.size());

        Function func = SubQueryPreProcessor.getNextSubQueryFunction(qn);
        Assert.assertTrue(func != null);
        Map<Long, Object> subquerySettings = new HashMap<Long, Object>();
        subquerySettings.put(((Query) func.getArgList().get(0)).getSubQueryId(), 1);

        func = SubQueryPreProcessor.subQueryAssignValueAndReturnNextSubQueryFunction(qn, subquerySettings);
        Assert.assertTrue(func != null);
        subquerySettings = new HashMap<Long, Object>();
        subquerySettings.put(((Query) func.getArgList().get(0)).getSubQueryId(), Arrays.asList(1, 2));

        func = SubQueryPreProcessor.subQueryAssignValueAndReturnNextSubQueryFunction(qn, subquerySettings);
        Assert.assertTrue(func != null);
        subquerySettings = new HashMap<Long, Object>();
        subquerySettings.put(((Query) func.getArgList().get(0)).getSubQueryId(), "LJH");

        func = SubQueryPreProcessor.subQueryAssignValueAndReturnNextSubQueryFunction(qn, subquerySettings);
        Assert.assertTrue(func != null);
        subquerySettings = new HashMap<Long, Object>();
        subquerySettings.put(((Query) func.getArgList().get(0)).getSubQueryId(), 3);

        func = SubQueryPreProcessor.subQueryAssignValueAndReturnNextSubQueryFunction(qn, subquerySettings);
        Assert.assertTrue(func == null);
    }

    @Test
    public void testQuery_子查询_各种位置子查询() throws SqlParserException {
        String sql = "SELECT (SELECT max(ID) FROM TABLE1)+1 FROM TABLE1 WHERE NOT exists(SELECT max(ID) FROM TABLE2 LIMIT 1) and (SELECT max(ID) FROM TABLE3) + 1 > (SELECT ID FROM TABLE4 WHERE NAME = (SELECT NAME FROM TABLE5))";
        sql += " GROUP BY ID + (SELECT max(ID) FROM TABLE6) HAVING ID + (SELECT max(ID) FROM TABLE7) > 10 ORDER BY ID + (SELECT max(ID) FROM TABLE8) LIMIT 10,10";
        Query qn = query(sql);
        qn.build();

        List<Function> funcs = SubQueryPreProcessor.findSubQueryFunctionList(qn, true);
        Assert.assertEquals(8, funcs.size());

        Function func = SubQueryPreProcessor.getNextSubQueryFunction(qn);
        Assert.assertTrue(func != null);
        for (int i = 0; i < 7; i++) {
            Map<Long, Object> subquerySettings = new HashMap<Long, Object>();
            subquerySettings.put(((Query) func.getArgList().get(0)).getSubQueryId(), 1);
            func = SubQueryPreProcessor.subQueryAssignValueAndReturnNextSubQueryFunction(qn, subquerySettings);
            Assert.assertTrue(func != null);
        }

        Map<Long, Object> subquerySettings = new HashMap<Long, Object>();
        subquerySettings.put(((Query) func.getArgList().get(0)).getSubQueryId(), 1);
        func = SubQueryPreProcessor.subQueryAssignValueAndReturnNextSubQueryFunction(qn, subquerySettings);
        Assert.assertTrue(func == null);
    }

    @Test
    public void testQuery_子查询_correlate替换() throws SqlParserException {
        String sql = "SELECT * FROM TABLE1 WHERE ID in (SELECT ID FROM TABLE2 WHERE TABLE2.NAME = TABLE1.NAME)";
        Query qn = query(sql);
        qn.build();

        List<Function> funcs = SubQueryPreProcessor.findSubQueryFunctionList(qn, true);
        Function func = funcs.get(0);
        Assert.assertTrue(func != null);

        Query qtn = (Query) func.getArgList().get(0);
        Map<Long, Object> subquerySettings = new HashMap<Long, Object>();
        subquerySettings.put(qtn.getCorrelatedSubQueryItemList().get(0).getCorrelateSubQueryItemId(), 3);
        func = SubQueryPreProcessor.subQueryAssignValueAndReturnNextSubQueryFunction(qn, subquerySettings);
        Assert.assertTrue(func == null);
    }

    private Query query(String sql) throws SqlParserException {
        ParseInfo parseInfo = sqlParseManager.parse(sql, false);
        Query query = null;
        if (parseInfo.getSqlType() == SqlType.SELECT) {
            query = parseInfo.getQueryNode();
        } else {
            query = new TableQueryWithIndex(null);
            query.setSql(sql);
        }
        return query;
    }
}
