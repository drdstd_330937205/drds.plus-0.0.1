package drds.plus.sql_process.optimizer;

import ch.qos.logback.LogContext;
import drds.plus.sql_process.BaseOptimizerTest;
import drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.QueryWithIndex;
import drds.plus.sql_process.abstract_syntax_tree.node.query.Join;
import drds.plus.sql_process.abstract_syntax_tree.node.query.TableQueryWithIndex;
import org.junit.Test;

public class KVIndexNodeTest extends BaseOptimizerTest {

    @Test
    public void testNormal() {
        LogContext.configure("C:\\Users\\ibm\\Desktop\\plus-2.0.05\\tddl-sql_process\\src\\nonspi\\resources\\logback.xml");
        LogContext.configure("C:\\Users\\ibm\\Desktop\\plus-2.0.05\\tddl-sql_process\\src\\nonspi\\resources\\logback.xml");
        System.out.println("============");
        TableQueryWithIndex studentID = new TableQueryWithIndex("TABLE1");
        studentID.setSelectItemListAndSetNeedBuild("ID,NAME,SCHOOL");
        studentID.indexQueryKeyFilter("ID=1");
        studentID.resultFilter("SCHOOL=1 and NAME = 333");
        studentID.build();

        QueryWithIndex queryWithIndex = (QueryWithIndex) studentID.toExecutePlan();
        System.out.println(queryWithIndex);
    }

    @Test
    public void testJoin() {
        TableQueryWithIndex studentID = new TableQueryWithIndex("TABLE1");
        studentID.setAliasAndSetNeedBuild("TABLE1");
        studentID.setSelectItemListAndSetNeedBuild("ID,NAME,SCHOOL");

        TableQueryWithIndex studentName = new TableQueryWithIndex("TABLE1._NAME");
        studentName.setAliasAndSetNeedBuild("TABLE1._NAME");
        studentName.setSelectItemListAndSetNeedBuild("ID,NAME");
        studentName.indexQueryKeyFilter("NAME=1");

        studentID.resultFilter("SCHOOL=1");

        Join join = studentName.join(studentID);
        join.addJoinKeys("ID", "ID");
        // build之前的操作順序可以任意
        join.build();

        System.out.println(join);
    }
}
