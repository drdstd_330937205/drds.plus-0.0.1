package drds.plus.sql_process.optimizer;

import drds.plus.common.properties.ConnectionProperties;
import drds.plus.sql_process.BaseOptimizerTest;
import drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.Join;
import drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.MergeQuery;
import drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.Query;
import drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.QueryWithIndex;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class StreamingTest extends BaseOptimizerTest {

    private static Map<String, Object> extraCmd = new HashMap<String, Object>();

    @BeforeClass
    public static void setUp() {
        extraCmd.put(ConnectionProperties.CHOOSE_INDEX, true);
        extraCmd.put(ConnectionProperties.CHOOSE_JOIN, false);
        extraCmd.put(ConnectionProperties.CHOOSE_INDEX_MERGE, false);
        // extraCmd.add(ConnectionProperties.MERGE_EXPAND, false);
        extraCmd.put(ConnectionProperties.JOIN_MERGE_JOIN_JUDGE_BY_RULE, true);
    }

    @Test
    public void test_单表merge无条件_开启streaming() {
        String sql = "query * from TABLE1";
        Query qc = (Query) optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, extraCmd, true);
        Assert.assertTrue(qc instanceof MergeQuery);
        Assert.assertEquals(true, ((MergeQuery) qc).isStreaming());
        Assert.assertEquals(true, ((MergeQuery) qc).getExecutePlan().isStreaming());
    }

    // @Test
    // public void test_单表merge_存在聚合函数_开启streaming() {
    // String chars = "selectStatement count(*) from TABLE1";
    // Query qc = (Query) sql_process.optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(chars, null,
    // extraCmd, true);
    // Assert.assertTrue(qc instanceof MergeQuery);
    // Assert.assertEquals(false, ((MergeQuery) qc).isStreaming());
    // Assert.assertEquals(false, ((MergeQuery) qc).getExecutePlan().isStreaming());
    // }

    @Test
    public void test_单表merge_limit_开启streaming() {
        String sql = "query * from TABLE1 WHERE ID > 10 LIMIT 10,91";
        Query qc = (Query) optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, extraCmd, true);
        Assert.assertTrue(qc instanceof MergeQuery);
        Assert.assertEquals(false, ((MergeQuery) qc).isStreaming());
        Assert.assertEquals(true, ((MergeQuery) qc).getExecutePlan().isStreaming());
    }

    @Test
    public void test_单表_limit_不开启streaming() {
        String sql = "query * from TABLE1 WHERE ID = 1 LIMIT 10,91";
        Query qc = (Query) optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, extraCmd, true);
        Assert.assertTrue(qc instanceof QueryWithIndex);
        Assert.assertEquals(false, ((QueryWithIndex) qc).isStreaming());
    }

    @Test
    public void test_单表_无条件_开启streaming() {
        String sql = "query * from TABLE7";
        Query qc = (Query) optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, extraCmd, true);
        Assert.assertTrue(qc instanceof QueryWithIndex);
        Assert.assertEquals(true, ((QueryWithIndex) qc).isStreaming());
    }

    @Test
    public void test_joinMergejoin无条件_开启streaming() {
        String sql = "query * from TABLE1 inner join  TABLE2 ON TABLE1.ID = TABLE2.ID";
        Query qc = (Query) optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, extraCmd, true);
        Assert.assertTrue(qc instanceof MergeQuery);
        Assert.assertEquals(true, ((MergeQuery) qc).isStreaming());
        Assert.assertEquals(true, ((MergeQuery) qc).getExecutePlan().isStreaming()); // 当前节点为true即可
        Assert.assertEquals(true, ((Join) ((MergeQuery) qc).getExecutePlan()).getLeftNode().isStreaming());
        Assert.assertEquals(false, ((Join) ((MergeQuery) qc).getExecutePlan()).getRightNode().isStreaming());
    }

    @Test
    public void test_joinMergejoin_limit_开启streaming() {
        String sql = "query * from TABLE1 t1 inner join TABLE2 t2 ON t1.ID = t2.ID limit 10,91";
        Query qc = (Query) optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, extraCmd, true);
        Assert.assertEquals(false, ((MergeQuery) qc).isStreaming());
        Assert.assertEquals(true, ((MergeQuery) qc).getExecutePlan().isStreaming());
    }

    @Test
    public void test_join_limit_不开启streaming() {
        String sql = "query * from TABLE1 t1 inner join TABLE2 t2 ON t1.ID = t2.ID where t1.id = 1 limit 10,91";
        Query qc = (Query) optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, extraCmd, true);
        Assert.assertEquals(false, ((Join) qc).isStreaming()); // 直接下推，当前根节点为false即可
        Assert.assertEquals(true, ((Join) qc).getLeftNode().isStreaming());
        Assert.assertEquals(false, ((Join) qc).getRightNode().isStreaming());
    }

    @Test
    public void test_join_无条件_开启streaming() {
        String sql = "query * from TABLE1 t1 inner join TABLE2 t2 ON t1.ID = t2.ID";
        Query qc = (Query) optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, extraCmd, true);
        Assert.assertEquals(true, ((MergeQuery) qc).isStreaming()); // 直接下推，当前根节点为false即可
        Assert.assertEquals(true, ((MergeQuery) qc).getExecutePlan().isStreaming());
    }

    @Test
    public void test_subQuery_merge无条件_开启streaming() {
        String sql = "query * from (query * from table1) a";
        Query qc = (Query) optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, extraCmd, true);
        Assert.assertTrue(qc instanceof MergeQuery);
        Assert.assertEquals(true, ((MergeQuery) qc).isStreaming());
        Assert.assertEquals(true, ((MergeQuery) qc).getExecutePlan().isStreaming());
        Assert.assertEquals(true, ((QueryWithIndex) ((MergeQuery) qc).getExecutePlan()).getSubQuery().isStreaming());
    }

    @Test
    public void test_subQuery_merge_limit_开启streaming() {
        String sql = "query * from (query * from table1) a limit 10,91";
        Query qc = (Query) optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, extraCmd, true);
        Assert.assertEquals(false, ((MergeQuery) qc).isStreaming());
        Assert.assertEquals(true, ((MergeQuery) qc).getExecutePlan().isStreaming());
        Assert.assertEquals(true, ((QueryWithIndex) ((MergeQuery) qc).getExecutePlan()).getSubQuery().isStreaming());
    }

    @Test
    public void test_subQuery_limit_不开启streaming() {
        String sql = "query * from (query * from table1 where id = 1) a limit 10,91";
        Query qc = (Query) optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, extraCmd, true);
        Assert.assertTrue(qc instanceof QueryWithIndex);
        Assert.assertEquals(false, ((QueryWithIndex) qc).isStreaming());
        Assert.assertEquals(true, ((QueryWithIndex) qc).getSubQuery().isStreaming());
    }

    @Test
    public void test_subQuery_无条件_开启streaming() {
        String sql = "query * from (query * from table7) a";
        Query qc = (Query) optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, extraCmd, true);
        Assert.assertTrue(qc instanceof QueryWithIndex);
        Assert.assertEquals(true, ((QueryWithIndex) qc).isStreaming());
    }

    @Test
    public void test_subQuery_子节点merge无条件_开启streaming() {
        String sql = "query * from (query * from table1 group by id) a";
        Query qc = (Query) optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, extraCmd, true);
        Assert.assertTrue(qc instanceof QueryWithIndex);
        // 主要存在group by聚合操作
        Assert.assertEquals(false, ((QueryWithIndex) qc).isStreaming());
        Assert.assertEquals(false, ((QueryWithIndex) qc).getSubQuery().isStreaming());
    }

    @Test
    public void test_subQuery_子节点merge_limit_开启streaming() {
        String sql = "query * from (query * from table1 group by id) a limit 10";
        Query qc = (Query) optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, extraCmd, true);
        Assert.assertTrue(qc instanceof QueryWithIndex);
        Assert.assertEquals(false, ((QueryWithIndex) qc).isStreaming());
        Assert.assertEquals(true, ((QueryWithIndex) qc).getSubQuery().isStreaming());
    }

    @Test
    public void test_单表merge无条件_强制关闭streaming() {
        String sql = "query * from TABLE1";
        extraCmd.put(ConnectionProperties.force_streaming, false);
        Query qc = (Query) optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, extraCmd, true);
        extraCmd.remove(ConnectionProperties.force_streaming);
        Assert.assertTrue(qc instanceof MergeQuery);
        Assert.assertEquals(false, ((MergeQuery) qc).isStreaming());
        Assert.assertEquals(false, ((MergeQuery) qc).getExecutePlan().isStreaming());
    }

    @Test
    public void test_单表_limit_强制开启streaming() {
        String sql = "query * from TABLE1 WHERE ID = 1 LIMIT 10,91";
        extraCmd.put(ConnectionProperties.force_streaming, true);
        Query qc = (Query) optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, extraCmd, true);
        extraCmd.remove(ConnectionProperties.force_streaming);
        Assert.assertTrue(qc instanceof QueryWithIndex);
        Assert.assertEquals(true, ((QueryWithIndex) qc).isStreaming());
    }
}
