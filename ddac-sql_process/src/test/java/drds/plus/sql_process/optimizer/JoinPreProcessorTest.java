package drds.plus.sql_process.optimizer;

import drds.plus.sql_process.BaseOptimizerTest;
import drds.plus.sql_process.abstract_syntax_tree.node.query.Join;
import drds.plus.sql_process.abstract_syntax_tree.node.query.TableQuery;
import drds.plus.sql_process.optimizer.pre_processor.JoinPreProcessor;
import org.junit.Assert;
import org.junit.Test;

public class JoinPreProcessorTest extends BaseOptimizerTest {

    @Test
    public void test_交换_右链接() {
        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        TableQuery tableQueryNode2 = new TableQuery("TABLE2");
        Join join = tableQueryNode1.join(tableQueryNode2);
        join.setRightOuterJoin();
        join.build();

        JoinPreProcessor.optimize(join);
        Assert.assertEquals(true, join.isLeftOuterJoin());
        Assert.assertEquals(tableQueryNode2, join.getLeftNode());
    }
}
