package drds.plus.sql_process.optimizer;

import drds.plus.common.properties.ConnectionProperties;
import drds.plus.sql_process.BaseOptimizerTest;
import drds.plus.sql_process.abstract_syntax_tree.configuration.IndexMapping;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.Item;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.Filter;
import drds.plus.sql_process.abstract_syntax_tree.node.query.TableQuery;
import drds.plus.sql_process.optimizer.chooser.FilterSpliter;
import drds.plus.sql_process.optimizer.chooser.FilterType;
import drds.plus.sql_process.optimizer.chooser.IndexChooser;
import drds.plus.sql_process.optimizer.pre_processor.FilterPreProcessor;
import drds.plus.sql_process.utils.DnfFilters;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FilterSpliterTest extends BaseOptimizerTest {

    @Test
    public void test_主键索引条件() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        tableQueryNode.setWhere("ID = 1");
        build(tableQueryNode);

        Map<FilterType, Filter> result = FilterSpliter.splitByIndex(DnfFilters.toDnfFilterList(tableQueryNode.getWhere()), tableQueryNode);
        Assert.assertEquals("TABLE1.ID = 1", result.get(FilterType.IndexQueryKeyFilter).toString());
        Assert.assertEquals(null, result.get(FilterType.IndexQueryValueFilter));
        Assert.assertEquals(null, result.get(FilterType.ResultFilter));
    }

    @Test
    public void test_主键索引GroupOr条件() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        tableQueryNode.setWhere("ID = 1 or ID > 2");
        build(tableQueryNode);
        FilterPreProcessor.optimize(tableQueryNode, true, null);

        Map<FilterType, Filter> result = FilterSpliter.splitByIndex(DnfFilters.toDnfFilterList(tableQueryNode.getWhere()), tableQueryNode);
        Assert.assertEquals(null, result.get(FilterType.IndexQueryKeyFilter));
        Assert.assertEquals("(TABLE1.ID = 1 or TABLE1.ID > 2)", result.get(FilterType.IndexQueryValueFilter).toString());
        Assert.assertEquals(null, result.get(FilterType.ResultFilter));
    }

    @Test
    public void test_主键索引条件_特殊条件不走索引() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        tableQueryNode.setWhere("ID != 1 and ID is NULL and ID is NOT NULL and ID like '%A' and ID = 2");
        build(tableQueryNode);

        Map<FilterType, Filter> result = FilterSpliter.splitByIndex(DnfFilters.toDnfFilterList(tableQueryNode.getWhere()), tableQueryNode);
        Assert.assertEquals("TABLE1.ID = 2", result.get(FilterType.IndexQueryKeyFilter).toString());
        Assert.assertEquals("(TABLE1.ID != 1 and TABLE1.ID is NULL and TABLE1.ID is NOT NULL and TABLE1.ID like %A)", result.get(FilterType.IndexQueryValueFilter).toString());
        Assert.assertEquals(null, result.get(FilterType.ResultFilter));
    }

    @Test
    public void test_二级索引条件() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        tableQueryNode.setWhere("NAME = 1");
        build(tableQueryNode);

        Map<FilterType, Filter> result = FilterSpliter.splitByIndex(DnfFilters.toDnfFilterList(tableQueryNode.getWhere()), tableQueryNode);
        Assert.assertEquals("TABLE1.NAME = 1", result.get(FilterType.IndexQueryKeyFilter).toString());
        Assert.assertEquals(null, result.get(FilterType.IndexQueryValueFilter));
        Assert.assertEquals(null, result.get(FilterType.ResultFilter));
    }

    @Test
    public void test_混合索引条件() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        tableQueryNode.setWhere("NAME = 1 and SCHOOL = 2 and ID > 3");
        build(tableQueryNode); // 会选择NAME做索引，因为=的选择度高，优先选择

        Map<FilterType, Filter> result = FilterSpliter.splitByIndex(DnfFilters.toDnfFilterList(tableQueryNode.getWhere()), tableQueryNode);
        Assert.assertEquals("TABLE1.NAME = 1", result.get(FilterType.IndexQueryKeyFilter).toString());
        Assert.assertEquals("TABLE1.ID > 3", result.get(FilterType.IndexQueryValueFilter).toString());
        Assert.assertEquals("TABLE1.SCHOOL = 2", result.get(FilterType.ResultFilter).toString());
    }

    @Test
    public void test_无索引条件() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        tableQueryNode.setWhere("SCHOOL = 2");
        build(tableQueryNode); // 无索引，默认选择主键做遍历

        Map<FilterType, Filter> result = FilterSpliter.splitByIndex(DnfFilters.toDnfFilterList(tableQueryNode.getWhere()), tableQueryNode);
        Assert.assertEquals(null, result.get(FilterType.IndexQueryKeyFilter));
        Assert.assertEquals("TABLE1.SCHOOL = 2", result.get(FilterType.IndexQueryValueFilter).toString());
        Assert.assertEquals(null, result.get(FilterType.ResultFilter));
    }

    private void build(TableQuery tableQueryNode) {
        tableQueryNode.build();

        Map<String, Object> extraCmd = new HashMap<String, Object>();
        extraCmd.put(ConnectionProperties.CHOOSE_INDEX, true);
        IndexMapping index = IndexChooser.findBestIndexMetaData(tableQueryNode.getTableMetaData(), new ArrayList<Item>(), DnfFilters.toDnfFilterList(tableQueryNode.getWhere()), tableQueryNode.getTableName(), extraCmd);

        tableQueryNode.setIndexMappingUsed(index);
    }
}
