package drds.plus.sql_process.optimizer;

import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.Filter;
import drds.plus.sql_process.abstract_syntax_tree.node.query.$Query$;
import drds.plus.sql_process.abstract_syntax_tree.node.query.Join;
import drds.plus.sql_process.abstract_syntax_tree.node.query.TableQuery;
import drds.plus.sql_process.optimizer.pre_processor.FilterPreProcessor;
import drds.plus.sql_process.optimizer.pusher.FilterPusher;
import drds.plus.sql_process.utils.DnfFilters;
import drds.plus.sql_process.utils.Filters;
import drds.plus.sql_process.utils.OptimizerUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class FilterPusherTest {// extends BaseOptimizerTest

    @Test
    public void test_where中无匹配条件不下推() {
        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        TableQuery tableQueryNode2 = new TableQuery("TABLE2");

        Join join = tableQueryNode1.join(tableQueryNode2);
        join.setWhere("(TABLE1.ID>5 or TABLE2.ID<10) and TABLE1.NAME = TABLE2.NAME");
        join.build();
        FilterPreProcessor.optimize(join, true, null);
        FilterPusher.optimize(join);

        Assert.assertEquals(null, join.getLeftNode().getWhere());
        Assert.assertEquals(null, join.getRightNode().getWhere());
        Assert.assertTrue(join.getJoinFilterList().isEmpty());
    }

    @Test
    public void test_where中非Group_OR条件不可下推() {
        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        TableQuery tableQueryNode2 = new TableQuery("TABLE2");

        Join join = tableQueryNode1.join(tableQueryNode2);
        join.setWhere("(TABLE1.ID>5 or TABLE2.NAME<10) and TABLE1.ID = TABLE2.ID");
        join.build();
        FilterPreProcessor.optimize(join, true, null);
        FilterPusher.optimize(join);

        Assert.assertEquals(null, join.getLeftNode().getWhere());
        Assert.assertEquals(null, join.getRightNode().getWhere());
        Assert.assertTrue(join.getJoinFilterList().isEmpty());
    }

    @Test
    public void test_where中Group_OR条件下推() {
        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        TableQuery tableQueryNode2 = new TableQuery("TABLE2");

        Join join = tableQueryNode1.join(tableQueryNode2);
        join.setWhere("(TABLE1.ID>10 or TABLE1.ID<5) and TABLE1.ID = TABLE2.ID");
        join.build();
        FilterPreProcessor.optimize(join, true, null);
        FilterPusher.optimize(join);

        Assert.assertEquals("(TABLE1.ID > 10 or TABLE1.ID < 5)", join.getLeftNode().getWhere().toString());
        Assert.assertEquals("(TABLE2.ID > 10 or TABLE2.ID < 5)", join.getRightNode().getWhere().toString());
        Assert.assertEquals("TABLE1.ID = TABLE2.ID", join.getJoinFilterList().get(0).toString());
    }

    @Test
    public void test_where条件下推() {
        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        TableQuery tableQueryNode2 = new TableQuery("TABLE2");

        Join join = tableQueryNode1.join(tableQueryNode2);
        join.setWhere("TABLE1.ID>5 and TABLE2.ID<10 and TABLE1.NAME = TABLE2.NAME");
        join.build();
        FilterPreProcessor.optimize(join, true, null);
        FilterPusher.optimize(join);

        Assert.assertEquals("TABLE1.ID > 5", join.getLeftNode().getWhere().toString());
        Assert.assertEquals("TABLE2.ID < 10", join.getRightNode().getWhere().toString());
        Assert.assertEquals("TABLE1.NAME = TABLE2.NAME", join.getJoinFilterList().get(0).toString());
    }

    @Test
    public void test_where条件下推_join列存在函数不处理() {
        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        TableQuery tableQueryNode2 = new TableQuery("TABLE2");

        Join join = tableQueryNode1.join(tableQueryNode2);
        join.setWhere("TABLE1.ID>5 and TABLE2.ID<10 and TABLE1.NAME = TABLE2.NAME + 1");
        join.build();
        FilterPreProcessor.optimize(join, true, null);
        FilterPusher.optimize(join);

        Assert.assertEquals("TABLE1.ID > 5", join.getLeftNode().getWhere().toString());
        Assert.assertEquals("TABLE2.ID < 10", join.getRightNode().getWhere().toString());
        Assert.assertEquals("TABLE1.NAME = TABLE2.NAME + 1", join.getWhere().toString());// 还是留在where中
    }

    @Test
    public void test_where条件下推_条件推导下推() {
        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        TableQuery tableQueryNode2 = new TableQuery("TABLE2");

        Join join = tableQueryNode1.join(tableQueryNode2);
        join.setWhere("TABLE1.ID>5 and TABLE2.ID<10 and TABLE1.ID = TABLE2.ID");
        join.build();
        FilterPreProcessor.optimize(join, true, null);
        FilterPusher.optimize(join);

        Assert.assertEquals("(TABLE1.ID > 5 and TABLE1.ID < 10)", join.getLeftNode().getWhere().toString());
        Assert.assertEquals("(TABLE2.ID < 10 and TABLE2.ID > 5)", join.getRightNode().getWhere().toString());
        Assert.assertEquals("TABLE1.ID = TABLE2.ID", join.getJoinFilterList().get(0).toString());
    }

    @Test
    public void test_where条件下推_子查询() {
        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        TableQuery tableQueryNode2 = new TableQuery("TABLE2");

        Join join = tableQueryNode1.join(tableQueryNode2);
        join.setAliasAndSetNeedBuild("S");
        join.setSelectItemListAndSetNeedBuild("TABLE1.ID,TABLE1.NAME,TABLE1.SCHOOL");
        join.setWhere("TABLE1.ID>5 and TABLE2.ID<10 and TABLE1.NAME = TABLE2.NAME");
        join.build();

        $Query$ query = new $Query$(join);
        query.setWhere("S.SCHOOL = 6");
        query.build();
        FilterPusher.optimize(query);
        Assert.assertEquals("(TABLE1.SCHOOL = 6 and TABLE1.ID > 5)", join.getLeftNode().getWhere().toString());
        Assert.assertEquals("TABLE2.ID < 10", join.getRightNode().getWhere().toString());
        Assert.assertEquals("TABLE1.NAME = TABLE2.NAME", join.getJoinFilterList().get(0).toString());
    }

    @Test
    public void test_where条件下推_子查询_函数列不传递() {
        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        TableQuery tableQueryNode2 = new TableQuery("TABLE2");

        Join join = tableQueryNode1.join(tableQueryNode2);
        join.setAliasAndSetNeedBuild("S");
        join.setSelectItemListAndSetNeedBuild("TABLE1.ID");
        join.getSelectItemList().add(OptimizerUtils.createColumnFromString("CONCAT_WS(' ',TABLE1.NAME,TABLE1.SCHOOL) AS NAME"));
        join.setWhere("TABLE1.ID>5 and TABLE2.ID<10 and TABLE1.NAME = TABLE2.NAME");
        join.build();

        $Query$ query = new $Query$(join);
        query.setWhere("S.NAME = 1");
        query.build();
        FilterPusher.optimize(query);
        Assert.assertEquals("TABLE1.ID > 5", join.getLeftNode().getWhere().toString());
        Assert.assertEquals("TABLE2.ID < 10", join.getRightNode().getWhere().toString());
        Assert.assertEquals("TABLE1.NAME = TABLE2.NAME", join.getJoinFilterList().get(0).toString());
    }

    @Test
    public void test_where条件下推_多级子查询_函数列不传递_字段列传递() {
        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        TableQuery tableQueryNode2 = new TableQuery("TABLE2");

        Join join = tableQueryNode1.join(tableQueryNode2);
        join.setAliasAndSetNeedBuild("S");
        join.setSelectItemListAndSetNeedBuild("TABLE1.ID");
        join.getSelectItemList().add(OptimizerUtils.createColumnFromString("CONCAT_WS(' ',TABLE1.NAME,TABLE1.SCHOOL) AS NAME"));
        join.setWhere("TABLE1.ID>5 and TABLE2.ID<10 and TABLE1.NAME = TABLE2.NAME");
        join.build();

        $Query$ query = new $Query$(join);
        query.setAliasAndSetNeedBuild("B");
        query.setWhere("S.NAME = 1");
        query.build();

        $Query$ nextQuery = new $Query$(query);
        nextQuery.setWhere("B.ID = 6");
        nextQuery.build();

        FilterPusher.optimize(nextQuery);
        Assert.assertEquals("(TABLE1.ID = 6 and TABLE1.ID > 5)", join.getLeftNode().getWhere().toString());
        Assert.assertEquals("TABLE2.ID < 10", join.getRightNode().getWhere().toString());
        Assert.assertEquals("TABLE1.NAME = TABLE2.NAME", join.getJoinFilterList().get(0).toString());
    }

    @Test
    public void test_where条件下推_多级join() {
        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        TableQuery tableQueryNode2 = new TableQuery("TABLE2");
        TableQuery tableQueryNode3 = new TableQuery("TABLE3");

        Join join = tableQueryNode1.join(tableQueryNode2);
        join.setSelectItemListAndSetNeedBuild("TABLE1.ID AS ID , TABLE1.NAME AS NAME");
        join.setWhere("TABLE1.ID>5 and TABLE2.ID<10 and TABLE1.NAME = TABLE2.NAME");
        join.build();

        Join nextJoin = join.join(tableQueryNode3);
        nextJoin.setWhere("TABLE1.NAME = 6 and TABLE1.ID = TABLE3.ID");
        nextJoin.build();
        FilterPusher.optimize(nextJoin);

        Assert.assertEquals("(TABLE1.NAME = 6 and TABLE1.ID > 5)", ((Join) nextJoin.getLeftNode()).getLeftNode().getWhere().toString());
        Assert.assertEquals("(TABLE2.ID < 10 and TABLE2.NAME = 6)", ((Join) nextJoin.getLeftNode()).getRightNode().getWhere().toString());
        Assert.assertEquals("TABLE1.ID = TABLE3.ID", nextJoin.getJoinFilterList().get(0).toString());
    }

    @Test
    public void test_where条件下推_多级join_子查询() {
        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        TableQuery tableQueryNode2 = new TableQuery("TABLE2");

        Join join = tableQueryNode1.join(tableQueryNode2);
        join.setAliasAndSetNeedBuild("S");
        join.setSelectItemListAndSetNeedBuild("TABLE1.ID AS ID , TABLE1.NAME AS NAME , TABLE2.SCHOOL AS SCHOOL");
        join.setWhere("TABLE1.ID>5 and TABLE2.ID<10 and TABLE1.NAME = TABLE2.NAME");
        join.build();

        $Query$ queryA = new $Query$(join);
        queryA.setAliasAndSetNeedBuild("B");
        queryA.setWhere("S.NAME = 2");
        queryA.build();

        $Query$ queryB = queryA.deepCopy();
        queryB.setAliasAndSetNeedBuild("C");
        queryB.setWhere("S.NAME = 3");//C.SCHOOL = 4
        queryB.build();

        Join nextJoin = queryA.join(queryB);
        nextJoin.setWhere("C.SCHOOL = 4 and B.ID = C.ID");
        nextJoin.build();
        FilterPusher.optimize(nextJoin);

        Assert.assertEquals("B.ID = C.ID", nextJoin.getJoinFilterList().get(0).toString());
        //
        Assert.assertEquals("(TABLE1.NAME = 2 and TABLE1.ID > 5)", ((Join) nextJoin.getLeftNode().getFirstSubNodeQueryNode()).getLeftNode().getWhere().toString());
        Assert.assertEquals("(TABLE2.ID < 10 and TABLE2.NAME = 2)", ((Join) nextJoin.getLeftNode().getFirstSubNodeQueryNode()).getRightNode().getWhere().toString());
        Assert.assertEquals("TABLE1.NAME = TABLE2.NAME", ((Join) nextJoin.getLeftNode().getFirstSubNodeQueryNode()).getJoinFilterList().get(0).toString());

        Assert.assertEquals("(TABLE1.NAME = 3 and TABLE1.ID > 5)", ((Join) nextJoin.getRightNode().getFirstSubNodeQueryNode()).getLeftNode().getWhere().toString());
        Assert.assertEquals("(TABLE2.SCHOOL = 4 and TABLE2.ID < 10 and TABLE2.NAME = 3)", ((Join) nextJoin.getRightNode().getFirstSubNodeQueryNode()).getRightNode().getWhere().toString());
        Assert.assertEquals("TABLE1.NAME = TABLE2.NAME", ((Join) nextJoin.getRightNode().getFirstSubNodeQueryNode()).getJoinFilterList().get(0).toString());
    }

    @Test
    public void test_where条件_group_or下推_多级join_子查询() {
        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        TableQuery tableQueryNode2 = new TableQuery("TABLE2");

        Join join = tableQueryNode1.join(tableQueryNode2);
        join.setAliasAndSetNeedBuild("S");
        join.setSelectItemListAndSetNeedBuild("TABLE1.ID AS ID , TABLE1.NAME AS NAME , TABLE2.SCHOOL AS SCHOOL");
        join.setWhere("TABLE1.ID>5 and TABLE2.ID<10 and TABLE1.NAME = TABLE2.NAME");
        join.build();

        $Query$ queryA = new $Query$(join);
        queryA.setAliasAndSetNeedBuild("B");
        queryA.setWhere("S.NAME = 2");
        queryA.build();

        $Query$ queryB = queryA.deepCopy();
        queryB.setAliasAndSetNeedBuild("C");
        queryB.setWhere("S.NAME = 3");
        queryB.build();

        Join nextJoin = queryA.join(queryB);
        nextJoin.setWhere("C.SCHOOL = 4 and B.ID = C.ID");
        nextJoin.build();
        FilterPusher.optimize(nextJoin);

        Assert.assertEquals("B.ID = C.ID", nextJoin.getJoinFilterList().get(0).toString());
        Assert.assertEquals("(TABLE1.NAME = 2 and TABLE1.ID > 5)", ((Join) nextJoin.getLeftNode().getFirstSubNodeQueryNode()).getLeftNode().getWhere().toString());
        Assert.assertEquals("(TABLE2.ID < 10 and TABLE2.NAME = 2)", ((Join) nextJoin.getLeftNode().getFirstSubNodeQueryNode()).getRightNode().getWhere().toString());
        Assert.assertEquals("TABLE1.NAME = TABLE2.NAME", ((Join) nextJoin.getLeftNode().getFirstSubNodeQueryNode()).getJoinFilterList().get(0).toString());

        Assert.assertEquals("(TABLE1.NAME = 3 and TABLE1.ID > 5)", ((Join) nextJoin.getRightNode().getFirstSubNodeQueryNode()).getLeftNode().getWhere().toString());
        Assert.assertEquals("(TABLE2.SCHOOL = 4 and TABLE2.ID < 10 and TABLE2.NAME = 3)", ((Join) nextJoin.getRightNode().getFirstSubNodeQueryNode()).getRightNode().getWhere().toString());
        Assert.assertEquals("TABLE1.NAME = TABLE2.NAME", ((Join) nextJoin.getRightNode().getFirstSubNodeQueryNode()).getJoinFilterList().get(0).toString());
    }

    @Test
    public void test_join条件下推() {
        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        TableQuery tableQueryNode2 = new TableQuery("TABLE2");

        Join join = tableQueryNode1.join(tableQueryNode2);
        join.addJoinKeys("TABLE1.NAME", "TABLE2.NAME");
        addOtherJoinFilter(join, "TABLE1.ID>5 and TABLE2.ID<10");
        join.build();
        FilterPreProcessor.optimize(join, true, null);
        FilterPusher.optimize(join);

        Assert.assertEquals("TABLE1.ID > 5", join.getLeftNode().getOtherJoinOnFilter().toString());
        Assert.assertEquals("TABLE2.ID < 10", join.getRightNode().getOtherJoinOnFilter().toString());
        Assert.assertEquals("TABLE1.NAME = TABLE2.NAME", join.getJoinFilterList().get(0).toString());
    }

    @Test
    public void test_join条件_group_on下推() {
        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        TableQuery tableQueryNode2 = new TableQuery("TABLE2");

        Join join = tableQueryNode1.join(tableQueryNode2);
        join.addJoinKeys("TABLE1.ID", "TABLE2.ID");
        addOtherJoinFilter(join, "TABLE1.ID>10 or TABLE1.ID<5");
        join.build();
        FilterPreProcessor.optimize(join, true, null);
        FilterPusher.optimize(join);

        Assert.assertEquals("(TABLE1.ID > 10 or TABLE1.ID < 5)", join.getLeftNode().getOtherJoinOnFilter().toString());
        Assert.assertEquals("(TABLE2.ID > 10 or TABLE2.ID < 5)", join.getRightNode().getOtherJoinOnFilter().toString());
        Assert.assertEquals("TABLE1.ID = TABLE2.ID", join.getJoinFilterList().get(0).toString());
    }

    @Test
    public void test_join条件_不满足group_or下推() {
        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        TableQuery tableQueryNode2 = new TableQuery("TABLE2");

        Join join = tableQueryNode1.join(tableQueryNode2);
        join.addJoinKeys("TABLE1.ID", "TABLE2.ID");
        addOtherJoinFilter(join, "TABLE1.ID>10 or TABLE2.ID<5");
        join.build();
        FilterPreProcessor.optimize(join, true, null);
        FilterPusher.optimize(join);

        Assert.assertEquals("(TABLE2.ID < 5 or TABLE1.ID > 10)", join.getOtherJoinOnFilter().toString());
        Assert.assertEquals("TABLE1.ID = TABLE2.ID", join.getJoinFilterList().get(0).toString());
    }

    @Test
    public void test_join条件下推_条件推导下推() {
        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        TableQuery tableQueryNode2 = new TableQuery("TABLE2");

        Join join = tableQueryNode1.join(tableQueryNode2);
        join.addJoinKeys("TABLE1.ID", "TABLE2.ID");
        addOtherJoinFilter(join, "TABLE1.ID>5 and TABLE2.ID<10");
        join.build();
        FilterPreProcessor.optimize(join, true, null);
        FilterPusher.optimize(join);

        Assert.assertEquals("(TABLE1.ID > 5 and TABLE1.ID < 10)", join.getLeftNode().getOtherJoinOnFilter().toString());
        Assert.assertEquals("(TABLE2.ID < 10 and TABLE2.ID > 5)", join.getRightNode().getOtherJoinOnFilter().toString());
        Assert.assertEquals("TABLE1.ID = TABLE2.ID", join.getJoinFilterList().get(0).toString());
    }

    @Test
    public void test_join条件下推_groupOR混合条件推导下推() {
        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        TableQuery tableQueryNode2 = new TableQuery("TABLE2");

        Join join = tableQueryNode1.join(tableQueryNode2);
        join.addJoinKeys("TABLE1.ID", "TABLE2.ID");
        addOtherJoinFilter(join, "(TABLE1.ID>10 or TABLE1.ID = 7) and TABLE2.ID<5");
        join.build();
        FilterPreProcessor.optimize(join, true, null);
        FilterPusher.optimize(join);
        FilterPreProcessor.optimize(join, true, null);// 再跑一次

        Assert.assertEquals("((TABLE1.ID > 10 or TABLE1.ID = 7) and TABLE1.ID < 5)", join.getLeftNode().getOtherJoinOnFilter().toString());
        Assert.assertEquals("(TABLE2.ID < 5 and (TABLE2.ID > 10 or TABLE2.ID = 7))", join.getRightNode().getOtherJoinOnFilter().toString());
        Assert.assertEquals("TABLE1.ID = TABLE2.ID", join.getJoinFilterList().get(0).toString());
    }

    @Test
    public void test_join条件下推_多级join() {
        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        TableQuery tableQueryNode2 = new TableQuery("TABLE2");
        TableQuery tableQueryNode3 = new TableQuery("TABLE3");

        Join join = tableQueryNode1.join(tableQueryNode2);
        join.addJoinKeys("TABLE1.NAME", "TABLE2.NAME");
        join.setAliasAndSetNeedBuild("A");
        join.setSelectItemListAndSetNeedBuild("TABLE1.ID AS ID , TABLE1.NAME AS NAME");
        addOtherJoinFilter(join, "TABLE1.ID>5 and TABLE2.ID<10");
        join.build();

        Join nextJoin = join.join(tableQueryNode3);
        nextJoin.addJoinKeys("A.ID", "TABLE3.ID");
        addOtherJoinFilter(nextJoin, "A.NAME = 6");
        nextJoin.build();
        FilterPreProcessor.optimize(join, true, null);
        FilterPusher.optimize(nextJoin);

        Assert.assertEquals("A.ID = TABLE3.ID", nextJoin.getJoinFilterList().get(0).toString());
        Assert.assertEquals("TABLE1.NAME = TABLE2.NAME", ((Join) nextJoin.getLeftNode()).getJoinFilterList().get(0).toString());
        Assert.assertEquals("(TABLE1.NAME = 6 and TABLE1.ID > 5)", ((Join) nextJoin.getLeftNode()).getLeftNode().getOtherJoinOnFilter().toString());
        Assert.assertEquals("(TABLE2.ID < 10 and TABLE2.NAME = 6)", ((Join) nextJoin.getLeftNode()).getRightNode().getOtherJoinOnFilter().toString());

    }

    @Test
    public void test_join条件下推_GROUP_ON多级join() {
        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        TableQuery tableQueryNode2 = new TableQuery("TABLE2");
        TableQuery tableQueryNode3 = new TableQuery("TABLE3");

        Join join = tableQueryNode1.join(tableQueryNode2);
        join.addJoinKeys("TABLE1.NAME", "TABLE2.NAME");
        join.setAliasAndSetNeedBuild("A");
        join.setSelectItemListAndSetNeedBuild("TABLE1.ID AS ID , TABLE1.NAME AS NAME");
        addOtherJoinFilter(join, "TABLE1.ID>5 and TABLE2.ID<10");
        join.build();

        Join nextJoin = join.join(tableQueryNode3);
        join.addJoinKeys("A.ID", "TABLE3.ID");
        addOtherJoinFilter(nextJoin, "A.NAME = 6 or A.NAME > 10");
        nextJoin.build();
        FilterPreProcessor.optimize(join, true, null);
        FilterPusher.optimize(nextJoin);

        Assert.assertEquals("A.ID = TABLE3.ID", nextJoin.getJoinFilterList().get(0).toString());
        Assert.assertEquals("TABLE1.NAME = TABLE2.NAME", ((Join) nextJoin.getLeftNode()).getJoinFilterList().get(0).toString());
        Assert.assertEquals("((TABLE1.NAME = 6 or TABLE1.NAME > 10) and TABLE1.ID > 5)", ((Join) nextJoin.getLeftNode()).getLeftNode().getOtherJoinOnFilter().toString());
        Assert.assertEquals("(TABLE2.ID < 10 and (TABLE2.NAME = 6 or TABLE2.NAME > 10))", ((Join) nextJoin.getLeftNode()).getRightNode().getOtherJoinOnFilter().toString());

    }

    private void addOtherJoinFilter(Join jn, String filter) {
        Filter f = Filters.createFilter(filter);
        f = FilterPreProcessor.processFilter(f, true, null);
        List<List<Filter>> DNFFilters = DnfFilters.toDnfFilterListList(DnfFilters.toDnfAndFlat(f));
        jn.setOtherJoinOnFilter(DnfFilters.orDnfFilterListList(DNFFilters));
    }
}
