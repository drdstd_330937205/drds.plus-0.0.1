package drds.plus.sql_process.optimizer;

import drds.plus.sql_process.BaseOptimizerTest;
import drds.plus.sql_process.abstract_syntax_tree.execute_plan.ExecutePlan;
import drds.plus.sql_process.abstract_syntax_tree.execute_plan.dml.IPut;
import drds.plus.sql_process.abstract_syntax_tree.execute_plan.query.MergeQuery;
import drds.plus.sql_process.abstract_syntax_tree.node.Node;
import drds.plus.sql_process.abstract_syntax_tree.node.dml.Delete;
import drds.plus.sql_process.abstract_syntax_tree.node.dml.Insert;
import drds.plus.sql_process.abstract_syntax_tree.node.dml.Replace;
import drds.plus.sql_process.abstract_syntax_tree.node.dml.Update;
import drds.plus.sql_process.abstract_syntax_tree.node.query.TableQuery;
import org.junit.Assert;
import org.junit.Test;


public class DMLNodeChooserTest extends BaseOptimizerTest {

    @Test
    public void testUpdate() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        String values[] = {"NAME"};
        tableQueryNode.setWhere("ID>=5 and ID<=100");
        Update updateNode = ((TableQuery) tableQueryNode).update("NAME", values);
        ExecutePlan plan = optimizer.optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(updateNode, null, null);
        Assert.assertTrue(plan instanceof MergeQuery);
        Assert.assertEquals(8, ((MergeQuery) plan).getExecutePlanList().size());

        String sql = "updateNode TABLE1 SET NAME = NAME WHERE ID>=5 and ID<=100";
        plan = optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, null, false);
        Assert.assertTrue(plan instanceof MergeQuery);
        Assert.assertEquals(8, ((MergeQuery) plan).getExecutePlanList().size());
    }

    public void testUpdate_范围更新生成merge() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        String values[] = {"NAME"};
        tableQueryNode.setWhere("ID>=5");
        Update updateNode = ((TableQuery) tableQueryNode).update("NAME", values);
        ExecutePlan plan = optimizer.optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(updateNode, null, null);
        Assert.assertTrue(plan instanceof MergeQuery);
        Assert.assertEquals(8, ((MergeQuery) plan).getExecutePlanList().size());

        String sql = "updateNode TABLE1 SET NAME = NAME WHERE ID>=5";
        plan = optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, null, false);
        Assert.assertTrue(plan instanceof MergeQuery);
        Assert.assertEquals(8, ((MergeQuery) plan).getExecutePlanList().size());
    }

    @Test
    public void testUpdate_广播表() {
        TableQuery tableQueryNode = new TableQuery("TABLE7");
        String values[] = {"NAME"};
        tableQueryNode.setWhere("ID>=5 and ID<=100");
        Update updateNode = ((TableQuery) tableQueryNode).update("NAME", values);
        ExecutePlan plan = optimizer.optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(updateNode, null, null);
        Assert.assertTrue(plan instanceof MergeQuery);
        Assert.assertEquals(4, ((MergeQuery) plan).getExecutePlanList().size());

        String sql = "updateNode TABLE7 SET NAME = NAME WHERE ID>=5 and ID<=100";
        plan = optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, null, false);
        Assert.assertTrue(plan instanceof MergeQuery);
        Assert.assertEquals(4, ((MergeQuery) plan).getExecutePlanList().size());
    }

    @Test
    public void testPut() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        Comparable values[] = {2};
        Replace update = ((TableQuery) tableQueryNode).put("ID", values);
        ExecutePlan plan = optimizer.optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(update, null, null);
        Assert.assertTrue(plan instanceof IPut);

        String sql = "insert INTO TABLE1(ID) VALUES(2)";
        plan = optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, null, false);
        Assert.assertTrue(plan instanceof IPut);
    }

    @Test
    public void testPut_多value() {
        String sql = "replace INTO TABLE1(ID) VALUES(1),(2),(3),(4),(5),(6),(7),(8)";
        ExecutePlan plan = optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, null, false);
        Assert.assertTrue(plan instanceof MergeQuery);
        Assert.assertEquals(8, ((MergeQuery) plan).getExecutePlanList().size());
    }

    @Test
    public void testPut_广播表() {
        TableQuery tableQueryNode = new TableQuery("TABLE7");
        Comparable values[] = {2};
        Replace update = ((TableQuery) tableQueryNode).put("ID", values);
        ExecutePlan plan = optimizer.optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(update, null, null);
        Assert.assertTrue(plan instanceof MergeQuery);
        Assert.assertEquals(4, ((MergeQuery) plan).getExecutePlanList().size());

        String sql = "replace INTO TABLE7(ID) VALUES(2)";
        plan = optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, null, false);
        Assert.assertTrue(plan instanceof MergeQuery);
        Assert.assertEquals(4, ((MergeQuery) plan).getExecutePlanList().size());
    }

    @Test
    public void testPut_多value_广播表() {
        String sql = "replace INTO TABLE7(ID) VALUES(1),(2),(3),(4),(5),(6),(7),(8)";
        ExecutePlan plan = optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, null, false);
        Assert.assertTrue(plan instanceof MergeQuery);
        Assert.assertEquals(4, ((MergeQuery) plan).getExecutePlanList().size());
    }

    @Test
    public void testPut_全字段() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        Comparable values[] = {2, "sysu", "sun"};
        Replace replaceNode = tableQueryNode.put("ID SCHOOL NAME", values);
        ExecutePlan plan = optimizer.optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(replaceNode, null, null);
        Assert.assertTrue(plan instanceof IPut);

        String sql = "replace INTO TABLE1(ID,SCHOOL,NAME) VALUES(2,'sysu','sun')";
        plan = optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, null, false);
        Assert.assertTrue(plan instanceof IPut);
    }

    @Test
    public void testPut_自增id_null值() {
        TableQuery tableQueryNode = new TableQuery("AUTOINC");
        Comparable values[] = {null, "sysu", "sun"};
        Replace replaceNode = tableQueryNode.put("ID SCHOOL NAME", values);
        ExecutePlan plan = optimizer.optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(replaceNode, null, null);
        Assert.assertTrue(plan instanceof IPut);

        String sql = "replace INTO AUTOINC(ID,SCHOOL,NAME) VALUES(null,'sysu','sun')";
        plan = optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, null, false);
        Assert.assertTrue(plan instanceof IPut);
    }

    @Test
    public void testPut_自增id_不存在列() {
        TableQuery tableQueryNode = new TableQuery("AUTOINC");
        Comparable values[] = {"sysu", "sun"};
        Replace replaceNode = tableQueryNode.put("SCHOOL NAME", values);
        ExecutePlan plan = optimizer.optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(replaceNode, null, null);
        Assert.assertTrue(plan instanceof IPut);

        String sql = "replace INTO AUTOINC(SCHOOL,NAME) VALUES('sysu','sun')";
        plan = optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, null, false);
        Assert.assertTrue(plan instanceof IPut);
    }

    @Test
    public void testDelete() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        tableQueryNode.setWhere("ID>=5 and ID<=100");
        Node delete = ((TableQuery) tableQueryNode).delete();// .delete();
        ExecutePlan plan = optimizer.optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(delete, null, null);
        Assert.assertTrue(plan instanceof MergeQuery);
        Assert.assertEquals(8, ((MergeQuery) plan).getExecutePlanList().size());

        String sql = "delete FROM TABLE1 WHERE ID>=5 and ID<=100";
        plan = optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, null, false);
        Assert.assertTrue(plan instanceof MergeQuery);
        Assert.assertEquals(8, ((MergeQuery) plan).getExecutePlanList().size());
    }

    @Test
    public void testDelete_广播表() {
        TableQuery tableQueryNode = new TableQuery("TABLE7");
        tableQueryNode.setWhere("ID>=5 and ID<=100");
        Node delete = ((TableQuery) tableQueryNode).delete();// .delete();
        ExecutePlan plan = optimizer.optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(delete, null, null);
        Assert.assertTrue(plan instanceof MergeQuery);
        Assert.assertEquals(4, ((MergeQuery) plan).getExecutePlanList().size());

        String sql = "delete FROM TABLE7 WHERE ID>=5 and ID<=100";
        plan = optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, null, false);
        Assert.assertTrue(plan instanceof MergeQuery);
        Assert.assertEquals(4, ((MergeQuery) plan).getExecutePlanList().size());
    }

    @Test
    public void testDelete_范围删除会生成merge() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        tableQueryNode.setWhere("ID>=5");
        Delete deleteNode = ((TableQuery) tableQueryNode).delete();
        ExecutePlan plan = optimizer.optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(deleteNode, null, null);
        Assert.assertTrue(plan instanceof MergeQuery);
        Assert.assertEquals(8, ((MergeQuery) plan).getExecutePlanList().size());

        String sql = "deleteNode FROM TABLE1 WHERE ID>=5";
        plan = optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, null, false);
        Assert.assertTrue(plan instanceof MergeQuery);
        Assert.assertEquals(8, ((MergeQuery) plan).getExecutePlanList().size());
    }

    @Test
    public void testInsert() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        Comparable values[] = {2};
        Insert insertNode = tableQueryNode.insert("ID", values);
        ExecutePlan plan = optimizer.optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(insertNode, null, null);
        Assert.assertTrue(plan instanceof drds.plus.sql_process.abstract_syntax_tree.execute_plan.dml.Insert);

        String sql = "insertNode INTO TABLE1(ID) VALUES(2)";
        plan = optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, null, false);
        Assert.assertTrue(plan instanceof drds.plus.sql_process.abstract_syntax_tree.execute_plan.dml.Insert);
    }

    @Test
    public void testInsert_多value() {
        String sql = "insert INTO TABLE1(ID) VALUES(1),(2),(3),(4),(5),(6),(7),(8)";
        ExecutePlan plan = optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, null, false);
        Assert.assertTrue(plan instanceof MergeQuery);
        Assert.assertEquals(8, ((MergeQuery) plan).getExecutePlanList().size());
    }

    @Test
    public void testInsert_广播表() {
        TableQuery tableQueryNode = new TableQuery("TABLE7");
        Comparable values[] = {2};
        Insert insertNode = tableQueryNode.insert("ID", values);
        ExecutePlan plan = optimizer.optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(insertNode, null, null);
        Assert.assertTrue(plan instanceof MergeQuery);
        Assert.assertEquals(4, ((MergeQuery) plan).getExecutePlanList().size());

        String sql = "insertNode INTO TABLE7(ID) VALUES(2)";
        plan = optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, null, false);
        Assert.assertTrue(plan instanceof MergeQuery);
        Assert.assertEquals(4, ((MergeQuery) plan).getExecutePlanList().size());
    }

    @Test
    public void testInsert_多value_广播表() {
        String sql = "insert INTO TABLE7(ID) VALUES(1),(2),(3),(4),(5),(6),(7),(8)";
        ExecutePlan plan = optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, null, false);
        Assert.assertTrue(plan instanceof MergeQuery);
        Assert.assertEquals(4, ((MergeQuery) plan).getExecutePlanList().size());
    }

    @Test
    public void testInsert_全字段() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        Comparable values[] = {2, "sysu", "sun"};
        Insert insertNode = tableQueryNode.insert("ID SCHOOL NAME", values);
        ExecutePlan plan = optimizer.optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(insertNode, null, null);
        Assert.assertTrue(plan instanceof drds.plus.sql_process.abstract_syntax_tree.execute_plan.dml.Insert);

        String sql = "insertNode INTO TABLE1 VALUES(2,'sysu','sun')";
        plan = optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, null, false);
        Assert.assertTrue(plan instanceof drds.plus.sql_process.abstract_syntax_tree.execute_plan.dml.Insert);
    }

    @Test
    public void testInsert_自增id_null值() {
        TableQuery tableQueryNode = new TableQuery("AUTOINC");
        Comparable values[] = {null, "sysu", "sun"};
        Insert insertNode = tableQueryNode.insert("ID SCHOOL NAME", values);
        ExecutePlan plan = optimizer.optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(insertNode, null, null);
        Assert.assertTrue(plan instanceof drds.plus.sql_process.abstract_syntax_tree.execute_plan.dml.Insert);

        String sql = "insertNode INTO AUTOINC(ID,SCHOOL,NAME) VALUES(null,'sysu','sun')";
        plan = optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, null, false);
        Assert.assertTrue(plan instanceof drds.plus.sql_process.abstract_syntax_tree.execute_plan.dml.Insert);
    }

    @Test
    public void testInsert_自增id_不存在列() {
        TableQuery tableQueryNode = new TableQuery("AUTOINC");
        Comparable values[] = {"sysu", "sun"};
        Insert insertNode = tableQueryNode.insert("SCHOOL NAME", values);
        ExecutePlan plan = optimizer.optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(insertNode, null, null);
        Assert.assertTrue(plan instanceof drds.plus.sql_process.abstract_syntax_tree.execute_plan.dml.Insert);

        String sql = "insertNode INTO AUTOINC(SCHOOL,NAME) VALUES('sysu','sun')";
        plan = optimizer.optimizeHintOrNode$optimizeNodeAndToExecutePlanAndOptimizeExecutePlanForTest(sql, null, null, false);
        Assert.assertTrue(plan instanceof drds.plus.sql_process.abstract_syntax_tree.execute_plan.dml.Insert);
    }

}
