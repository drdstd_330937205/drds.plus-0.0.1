package drds.plus.sql_process.optimizer;

import drds.plus.common.properties.ConnectionProperties;
import drds.plus.sql_process.BaseOptimizerTest;
import drds.plus.sql_process.abstract_syntax_tree.configuration.IndexMapping;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.Item;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.Filter;
import drds.plus.sql_process.abstract_syntax_tree.node.query.Query;
import drds.plus.sql_process.abstract_syntax_tree.node.query.TableQuery;
import drds.plus.sql_process.optimizer.chooser.IndexChooser;
import drds.plus.sql_process.utils.DnfFilters;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Dreamond
 */
public class IndexChooserTest extends BaseOptimizerTest {

    private Map extraCmd = new HashMap();
    private List<Item> emptyColumns = new ArrayList<Item>();

    @Before
    public void setUp() {
        extraCmd.put(ConnectionProperties.CHOOSE_INDEX, true);
    }

    @Test
    public void testChooseIndex() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        tableQueryNode.setWhere("ID=1");
        Query qn = tableQueryNode;
        qn.build();

        IndexMapping index = IndexChooser.findBestIndexMetaData(tableQueryNode.getTableMetaData(), emptyColumns, toDNFFilter(tableQueryNode.getWhere()), tableQueryNode.getTableName(), extraCmd);

        Assert.assertNotNull(index);
        Assert.assertEquals(index.getIndexName(), "TABLE1");
    }

    /**
     * NAME=1的选择性显然比SCHOOL>1好，所以选择二级索引NAME @
     */
    @Test
    public void testChooseIndex列出现的顺序不影响索引选择() {
        TableQuery tableQueryNode1 = new TableQuery("TABLE1");
        tableQueryNode1.setWhere("SCHOOL>1&&NAME=1");
        Query qn1 = tableQueryNode1;
        qn1.build();

        IndexMapping index = IndexChooser.findBestIndexMetaData(tableQueryNode1.getTableMetaData(), tableQueryNode1.getReferedItemList(), toDNFFilter(tableQueryNode1.getWhere()), tableQueryNode1.getTableName(), extraCmd);

        Assert.assertNotNull(index);
        Assert.assertEquals(index.getIndexName(), "TABLE1._NAME");

        TableQuery tableQueryNode2 = new TableQuery("TABLE1");
        tableQueryNode2.setWhere("NAME=1&&SCHOOL>1");
        Query qn2 = tableQueryNode2;
        qn2.build();

        index = IndexChooser.findBestIndexMetaData(tableQueryNode2.getTableMetaData(), tableQueryNode2.getReferedItemList(), toDNFFilter(tableQueryNode2.getWhere()), tableQueryNode2.getTableName(), extraCmd);

        Assert.assertNotNull(index);
        Assert.assertEquals(index.getIndexName(), "TABLE1._NAME");
    }

    /**
     * 虽然C1，C2上存在组合索引，但是由于范围查询的选择度不如等值查询 因此还是选择了单索引NAME=1
     */
    @Test
    public void testChooseIndex单索引选择度好于组合索引() {
        TableQuery tableQueryNode = new TableQuery("TABLE9");
        tableQueryNode.setWhere("C1>10&&C2>3&&NAME=1");
        Query qn = tableQueryNode;
        qn.build();

        IndexMapping index = IndexChooser.findBestIndexMetaData(tableQueryNode.getTableMetaData(), tableQueryNode.getReferedItemList(), toDNFFilter(tableQueryNode.getWhere()), tableQueryNode.getTableName(), extraCmd);

        Assert.assertNotNull(index);
        Assert.assertEquals(index.getIndexName(), "TABLE9._NAME");
    }

    /**
     * 虽然C1，C2上都存在单索引，但是C1，C2还是组合索引，这种情况下优先选择组合索引 @
     */
    @Test
    public void testChooseIndex选择组合索引() {
        TableQuery tableQueryNode = new TableQuery("TABLE9");
        tableQueryNode.setWhere("C1>10&&C2=3");
        Query qn = tableQueryNode;
        qn.build();

        IndexMapping index = IndexChooser.findBestIndexMetaData(tableQueryNode.getTableMetaData(), tableQueryNode.getReferedItemList(), toDNFFilter(tableQueryNode.getWhere()), tableQueryNode.getTableName(), extraCmd);

        Assert.assertNotNull(index);
        Assert.assertEquals(index.getIndexName(), "TABLE9._C1_C2");
    }

    /**
     * C4,C5上只存在倒排索引，单C4的选择更高，选择C4倒排
     */
    @Test
    public void testChooseIndex选择倒排索引() {
        TableQuery tableQueryNode = new TableQuery("TABLE9");
        tableQueryNode.setWhere("C4=10&&C5>3");
        Query qn = tableQueryNode;
        qn.build();

        IndexMapping index = IndexChooser.findBestIndexMetaData(tableQueryNode.getTableMetaData(), tableQueryNode.getReferedItemList(), toDNFFilter(tableQueryNode.getWhere()), tableQueryNode.getTableName(), extraCmd);

        Assert.assertNotNull(index);
        Assert.assertEquals(index.getIndexName(), "TABLE9._C4");
    }

    /**
     * C6,C7同时存在组合索引和倒排索引 同时有倒排和组合索引，并且选择度一样，优先选择组合
     */
    @Test
    public void testChooseIndex选择度相同优先选组合() {
        TableQuery tableQueryNode = new TableQuery("TABLE9");
        tableQueryNode.setWhere("C6=10&&C7=3");
        Query qn = tableQueryNode;
        qn.build();

        IndexMapping index = IndexChooser.findBestIndexMetaData(tableQueryNode.getTableMetaData(), tableQueryNode.getReferedItemList(), toDNFFilter(tableQueryNode.getWhere()), tableQueryNode.getTableName(), extraCmd);

        Assert.assertNotNull(index);
        Assert.assertEquals(index.getIndexName(), "TABLE9._C6_C7");
    }

    @Test
    public void testChooseIndex手动指定索引() {
        TableQuery tableQueryNode = new TableQuery("TABLE9");
        tableQueryNode.build();
        tableQueryNode.setIndexMappingUsed(tableQueryNode.getTableMetaData().getIndexMetaDataList().get(0));
        tableQueryNode.build();
        System.out.println(tableQueryNode.toExecutePlan());
    }

    private List<Filter> toDNFFilter(Filter where) {
        return DnfFilters.toDnfFilterListList(where).get(0);
    }
}
