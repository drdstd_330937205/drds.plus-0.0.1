package drds.plus.sql_process.optimizer;

import drds.plus.sql_process.BaseOptimizerTest;
import drds.plus.sql_process.abstract_syntax_tree.node.query.TableQuery;
import drds.plus.sql_process.optimizer.chooser.EmptyResultFilterException;
import drds.plus.sql_process.optimizer.pre_processor.FilterPreProcessor;
import drds.plus.sql_process.utils.DnfFilters;
import org.junit.Assert;
import org.junit.Test;

public class FilterPreProcessorTest extends BaseOptimizerTest {

    @Test
    public void test_路径短化_永真() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        tableQueryNode.setWhere("1 and ID = 1");
        tableQueryNode.build();
        FilterPreProcessor.optimize(tableQueryNode, true, null);
        Assert.assertEquals(tableQueryNode.getWhere().toString(), "TABLE1.ID = 1");
    }

    @Test
    public void test_路径短化_永真_多路() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        tableQueryNode.setWhere("(ID = 1 and NAME = 'HELLO') or (1) ");
        tableQueryNode.build();
        FilterPreProcessor.optimize(tableQueryNode, true, null);
        Assert.assertEquals(tableQueryNode.getWhere().toString(), "1");
    }

    @Test
    public void test_路径短化_永假() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        tableQueryNode.setWhere("0 and ID = 1");
        tableQueryNode.build();
        try {
            FilterPreProcessor.optimize(tableQueryNode, true, null);
            Assert.fail();
        } catch (EmptyResultFilterException e) {
            // 会出现EmptyResultFilterException异常
        }
    }

    @Test
    public void test_路径短化_永假_多路() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        tableQueryNode.setWhere("(ID = 1 and NAME = 'HELLO') or (0) ");
        tableQueryNode.build();
        FilterPreProcessor.optimize(tableQueryNode, true, null);
        Assert.assertEquals(tableQueryNode.getWhere().toString(), "(TABLE1.ID = 1 and TABLE1.NAME = HELLO)");
    }

    @Test
    public void test_路径短化_组合() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        tableQueryNode.setWhere("(1 and (ID = 1 or ID = 2) and NAME = 'HELLO') or (0 and 1) or (ID = 3)");
        tableQueryNode.build();
        FilterPreProcessor.optimize(tableQueryNode, true, null);
        System.out.println(DnfFilters.toDnfAndFlat(tableQueryNode.getWhere()));
        Assert.assertEquals(tableQueryNode.getWhere().toString(), "((TABLE1.ID = 1 and TABLE1.NAME = HELLO) or (TABLE1.ID = 2 and TABLE1.NAME = HELLO) or TABLE1.ID = 3)");
    }

    @Test
    public void test_列调整_常量() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        tableQueryNode.setWhere("1 = ID and NAME = 'HELLO'");
        tableQueryNode.build();
        FilterPreProcessor.optimize(tableQueryNode, true, null);
        Assert.assertEquals(tableQueryNode.getWhere().toString(), "(TABLE1.ID = 1 and TABLE1.NAME = HELLO)");
    }

    @Test
    public void test_列调整_列值() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        tableQueryNode.setWhere("ID = NAME and 'HELLO'= NAME and NAME = ID ");
        tableQueryNode.build();
        FilterPreProcessor.optimize(tableQueryNode, true, null);
        Assert.assertEquals(tableQueryNode.getWhere().toString(), "(TABLE1.ID = TABLE1.NAME and TABLE1.NAME = HELLO and TABLE1.NAME = TABLE1.ID)");
    }

    @Test
    public void test_列调整_函数() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        tableQueryNode.setWhere("2 = ID+1");
        tableQueryNode.build();
        FilterPreProcessor.optimize(tableQueryNode, true, null);
        Assert.assertEquals(tableQueryNode.getWhere().toString(), "ID + 1 = 2");
    }

    @Test
    public void test_列调整_各种组合() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        tableQueryNode.setWhere("HEX(2) = ID+1 and NAME = HEX(NAME)");
        tableQueryNode.build();
        FilterPreProcessor.optimize(tableQueryNode, true, null);
        Assert.assertEquals(tableQueryNode.getWhere().toString(), "(HEX(2) = ID + 1 and TABLE1.NAME = HEX(NAME))");
    }

    @Test
    public void test_合并_AND条件() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        tableQueryNode.setWhere("ID > 1 and ID < 10");
        tableQueryNode.build();
        FilterPreProcessor.optimize(tableQueryNode, true, null);
        Assert.assertEquals(tableQueryNode.getWhere().toString(), "(TABLE1.ID > 1 and TABLE1.ID < 10)");

        tableQueryNode.setWhere("ID > 1 and ID < 10 and ID >= 2 and ID < 11");
        tableQueryNode.build();
        FilterPreProcessor.optimize(tableQueryNode, true, null);
        Assert.assertEquals(tableQueryNode.getWhere().toString(), "(TABLE1.ID >= 2 and TABLE1.ID < 10)");

        tableQueryNode.setWhere("ID > 1 and ID < 0");
        tableQueryNode.build();
        try {
            FilterPreProcessor.optimize(tableQueryNode, true, null);
            Assert.fail();
        } catch (EmptyResultFilterException e) {
            // 会出现EmptyResultFilterException异常
        }
    }

    @Test
    public void test_合并_OR条件() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        tableQueryNode.setWhere("ID > 1 or ID < 3");
        tableQueryNode.build();
        FilterPreProcessor.optimize(tableQueryNode, true, null);
        Assert.assertEquals(tableQueryNode.getWhere().toString(), "1");

        tableQueryNode.setWhere("ID > 1 or ID > 3");
        tableQueryNode.build();
        FilterPreProcessor.optimize(tableQueryNode, true, null);
        Assert.assertEquals(tableQueryNode.getWhere().toString(), "TABLE1.ID > 1");

        tableQueryNode.setWhere("ID > 1 or ID = 5");
        tableQueryNode.build();
        FilterPreProcessor.optimize(tableQueryNode, true, null);
        Assert.assertEquals(tableQueryNode.getWhere().toString(), "TABLE1.ID > 1");
    }

    @Test
    public void test_合并_Group_OR条件() {
        TableQuery tableQueryNode = new TableQuery("TABLE1");
        tableQueryNode.setWhere("ID = 1 or 2 = ID or ID = 3 or ID in (4 , 5)");
        tableQueryNode.build();
        FilterPreProcessor.optimize(tableQueryNode, true, null);
        Assert.assertEquals(tableQueryNode.getWhere().toString(), "TABLE1.ID in [1, 2, 3, 4, 5]");

        tableQueryNode.setWhere("ID = 1 or ID = 2 or 3 < ID");
        tableQueryNode.build();
        FilterPreProcessor.optimize(tableQueryNode, true, null);
        Assert.assertEquals(tableQueryNode.getWhere().toString(), "(TABLE1.ID in [1, 2] or TABLE1.ID > 3)");

        tableQueryNode.setWhere("(ID = 1 or ID > 3) and NAME = 'LJH'");
        tableQueryNode.build();
        FilterPreProcessor.optimize(tableQueryNode, true, null);
        Assert.assertEquals(tableQueryNode.getWhere().toString(), "((TABLE1.ID = 1 or TABLE1.ID > 3) and TABLE1.NAME = LJH)");

        tableQueryNode.setWhere("((ID = 1 or ID > 3) or NAME = 'LJH') or ID = 2");
        // 目前不会调整or条件的顺序来满足group
        tableQueryNode.build();
        FilterPreProcessor.optimize(tableQueryNode, true, null);
        FilterPreProcessor.optimize(tableQueryNode, true, null);
        Assert.assertEquals(tableQueryNode.getWhere().toString(), "((TABLE1.ID = 1 or TABLE1.ID > 3) or TABLE1.NAME = LJH or TABLE1.ID = 2)");

        tableQueryNode.setWhere("ID = 1 or ID = sqrt(2) or ID = 2");
        // 目前针对列非常量的不满足group
        tableQueryNode.build();
        FilterPreProcessor.optimize(tableQueryNode, true, null);
        Assert.assertEquals(tableQueryNode.getWhere().toString(), "(TABLE1.ID = 1 or TABLE1.ID = SQRT(2) or TABLE1.ID = 2)");
    }

}
