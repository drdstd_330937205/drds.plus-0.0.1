package drds.plus.sql_process.utils;

import drds.plus.sql_process.abstract_syntax_tree.ObjectCreateFactory;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.column.Column;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.BooleanFilter;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.Filter;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.LogicalOperationFilter;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.Operation;
import drds.plus.sql_process.optimizer.chooser.EmptyResultFilterException;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

/**
 * @author jianghang 2013-11-13 下午4:57:52
 * @since 5.0.0
 */
public class FilterUtilsTest {

    @Test
    public void testDNF() {
        // ( ( A and D ) or B) and C
        Filter filter = and(or(and(filter("A"), filter("D")), filter("B")), filter("C"));
        // 结果： ((A and D) and C) or (B and C)，没有拉平
        Assert.assertEquals(or(newAnd(and(filter("A"), filter("D")), filter("C")), and(filter("B"), filter("C"))), DnfFilters.toDnf(filter));

        // 结果： (A and D and C) or (B and C)， 拉平处理
        Assert.assertEquals(or(and(and(filter("A"), filter("D")), filter("C")), and(filter("B"), filter("C"))), DnfFilters.toDnfAndFlat(filter));

        Assert.assertEquals(Arrays.asList(Arrays.asList(filter("A"), filter("D"), filter("C")), Arrays.asList(filter("B"), filter("C"))), DnfFilters.toDnfFilterListList(DnfFilters.toDnf(filter)));
    }

    @Test
    public void testMerge() {
        try {
            // 1 < A < 10
            Filter filter = and(filter("A", 1, Operation.greater_than), filter("A", 10, Operation.less_than));
            Assert.assertEquals("(A > 1 and A < 10)", DnfFilters.merge(filter).toString());

            // 1 < A <= 10, 2 <= A < 11
            filter = and(filter("A", 1, Operation.greater_than), filter("A", 10, Operation.less_than_or_equal));
            filter = newAnd(and(filter("A", 2, Operation.greater_than_or_equal), filter("A", 11, Operation.less_than)), filter);
            Assert.assertEquals("(A >= 2 and A <= 10)", DnfFilters.merge(filter).toString());
        } catch (EmptyResultFilterException e) {
            Assert.fail();
        }

        try {
            // 1 < A , A < 0
            Filter filter = and(filter("A", 1, Operation.greater_than), filter("A", 0, Operation.less_than));
            DnfFilters.merge(filter);
            Assert.fail();// 不可能到这一步
        } catch (EmptyResultFilterException e) {
        }

        try {
            // A > 1 or A < 3
            Filter filter = or(filter("A", 1, Operation.greater_than), filter("A", 3, Operation.less_than));
            Assert.assertEquals("1", DnfFilters.merge(filter).toString());

            // A > 1 or A > 3
            filter = or(filter("A", 1, Operation.greater_than), filter("A", 3, Operation.greater_than));
            Assert.assertEquals("A > 1", DnfFilters.merge(filter).toString());

            // A > 1 or A = 5
            filter = or(filter("A", 1, Operation.greater_than), filter("A", 5, Operation.equal));
            Assert.assertEquals("A > 1", DnfFilters.merge(filter).toString());
        } catch (EmptyResultFilterException e) {
            Assert.fail();
        }

        try {
            // A >= 3 or A <= 1 or A in (2)
            Filter filter = or(or(filter("A", 3, Operation.greater_than_or_equal), filter("A", 1, Operation.less_than_or_equal)), filter("A", Arrays.asList(1, 2, 3, 4), Operation.in));
            Assert.assertEquals("(A in [1, 2, 3, 4] or A >= 3 or A <= 1)", DnfFilters.merge(filter).toString());
        } catch (EmptyResultFilterException e) {
            Assert.fail();
        }

    }

    @Test
    public void testCreateFilter() {
        String where = "id > 1 and id <= 10";
        Filter filter = Filters.createFilter(where);
        System.out.println(filter);

        where = "columnName = 'hello' and id in ('1','2')";
        filter = Filters.createFilter(where);
        System.out.println(filter);

        where = "gmt_create = '2013-11-11 11:11:11'";
        filter = Filters.createFilter(where);
        System.out.println(filter);

        where = "1+1";
        filter = Filters.createFilter(where);
        System.out.println(filter);
    }

    private Filter filter(Comparable column) {
        BooleanFilter booleanFilter = ObjectCreateFactory.createBooleanFilter();
        booleanFilter.setColumn(column);
        booleanFilter.setValue(1);
        booleanFilter.setOperation(Operation.equal);
        return booleanFilter;
    }

    private Filter newAnd(Filter left, Filter right) {
        LogicalOperationFilter and = ObjectCreateFactory.createLogicalOperationFilter();
        and.setOperation(Operation.and);
        and.addFilter(left);
        and.addFilter(right);
        return and;
    }

    private Filter filter(Comparable column, Object value, Operation op) {
        BooleanFilter booleanFilter = ObjectCreateFactory.createBooleanFilter();
        Column column1 = ObjectCreateFactory.createColumn();
        column1.setColumnName((String) column);
        booleanFilter.setColumn(column1);
        booleanFilter.setValue(value);
        booleanFilter.setOperation(op);
        return booleanFilter;
    }

    private Filter and(Filter left, Filter right) {
        return DnfFilters.and(left, right);
    }

    private Filter or(Filter left, Filter right) {
        return DnfFilters.or(left, right);
    }
}
