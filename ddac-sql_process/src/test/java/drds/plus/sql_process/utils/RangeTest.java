package drds.plus.sql_process.utils;

import drds.plus.sql_process.abstract_syntax_tree.ObjectCreateFactory;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.column.Column;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.BooleanFilter;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.Filter;
import drds.plus.sql_process.abstract_syntax_tree.expression.item.function.Operation;
import drds.plus.sql_process.utils.range.AndRangeProcessor;
import drds.plus.sql_process.utils.range.OrRangeProcessor;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

/**
 * @author jianghang 2013-11-13 下午4:57:57
 * @since 5.0.0
 */
public class RangeTest {

    private static final Column COLUMN = ObjectCreateFactory.createColumn();

    static {
        COLUMN.setColumnName("A");
    }

    @Test
    public void testAnd() {
        // 1 < A < 10
        AndRangeProcessor andProcessor = new AndRangeProcessor(COLUMN);
        andProcessor.process(filter(COLUMN, 1, Operation.greater_than));
        andProcessor.process(filter(COLUMN, 10, Operation.less_than));
        Assert.assertEquals(Arrays.asList(filter(COLUMN, 1, Operation.greater_than), filter(COLUMN, 10, Operation.less_than)), andProcessor.toFilterList());

        // 1 < A <= 10, 2 <= A < 11
        andProcessor = new AndRangeProcessor(COLUMN);
        andProcessor.process(filter(COLUMN, 1, Operation.greater_than));
        andProcessor.process(filter(COLUMN, 10, Operation.less_than_or_equal));

        andProcessor.process(filter(COLUMN, 2, Operation.greater_than_or_equal));
        andProcessor.process(filter(COLUMN, 11, Operation.less_than));
        Assert.assertEquals(Arrays.asList(filter(COLUMN, 2, Operation.greater_than_or_equal), filter(COLUMN, 10, Operation.less_than_or_equal)), andProcessor.toFilterList());

        // 1 < A , A < 0
        andProcessor = new AndRangeProcessor(COLUMN);
        andProcessor.process(filter(COLUMN, 1, Operation.greater_than));
        boolean failed = andProcessor.process(filter(COLUMN, 0, Operation.less_than));
        Assert.assertEquals(false, failed);
    }

    @Test
    public void testOr() {
        // A > 1 or A < 3
        OrRangeProcessor orProcessor = new OrRangeProcessor(COLUMN);
        orProcessor.process(filter(COLUMN, 1, Operation.greater_than));
        orProcessor.process(filter(COLUMN, 3, Operation.less_than));
        Assert.assertTrue(orProcessor.toFilterListList().isEmpty());// full set

        // A > 1 or A > 3
        orProcessor = new OrRangeProcessor(COLUMN);
        orProcessor.process(filter(COLUMN, 1, Operation.greater_than));
        orProcessor.process(filter(COLUMN, 3, Operation.greater_than_or_equal));
        Assert.assertEquals(Arrays.asList(Arrays.asList(filter(COLUMN, 1, Operation.greater_than))), orProcessor.toFilterListList());

        // A > 1 or A = 5
        orProcessor = new OrRangeProcessor(COLUMN);
        orProcessor.process(filter(COLUMN, 1, Operation.greater_than));
        orProcessor.process(filter(COLUMN, 0, Operation.equal));
        Assert.assertEquals(Arrays.asList(Arrays.asList(filter(COLUMN, 1, Operation.greater_than)), Arrays.asList(filter(COLUMN, 0, Operation.equal))), orProcessor.toFilterListList());
    }

    private Filter filter(Comparable column, Comparable value, Operation op) {
        BooleanFilter booleanFilter = ObjectCreateFactory.createBooleanFilter();
        booleanFilter.setColumn(column);
        booleanFilter.setValue(value);
        booleanFilter.setOperation(op);
        return booleanFilter;
    }

}
