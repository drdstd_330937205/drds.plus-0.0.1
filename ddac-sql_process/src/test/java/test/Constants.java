package test;

class Constants {


    /**
     * Byte to use for null, low ordering
     */
    static final byte NULL_BYTE_LOW = 0;

    /**
     * Byte to use for null, high ordering
     */
    static final byte NULL_BYTE_HIGH = (byte) ~NULL_BYTE_LOW;

    /**
     * Byte to use for not-null, low ordering
     */
    static final byte NOT_NULL_BYTE_HIGH = (byte) 128;

}
