package test;

import java.math.BigInteger;

public class Main {

    public static void main(String[] args) {
        byte[] bytes = new byte[1024];
        int offset = 0;
        BigInteger bigInteger = new BigInteger("1244");
        Encoder.encode(bigInteger, bytes, offset);
        BigInteger[] values = new BigInteger[1];
        Decoder.decodeBigIntegerObject(bytes, offset, values);
        System.out.println(values[0]);

    }
}
