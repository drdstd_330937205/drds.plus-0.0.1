package drds.plus.parser.parser;

import drds.plus.parser.abstract_syntax_tree.expression.BinaryExpression;
import drds.plus.parser.abstract_syntax_tree.expression.Expression;
import drds.plus.parser.abstract_syntax_tree.expression.arithmetic.Add;
import drds.plus.parser.abstract_syntax_tree.expression.arithmetic.Mod;
import drds.plus.parser.abstract_syntax_tree.expression.arithmetic.Subtract;
import drds.plus.parser.abstract_syntax_tree.expression.comparison.Is;
import drds.plus.parser.abstract_syntax_tree.expression.comparison.fuzzy_matching.Like;
import drds.plus.parser.abstract_syntax_tree.expression.comparison.fuzzy_matching.Regexp;
import drds.plus.parser.abstract_syntax_tree.expression.comparison.range.*;
import drds.plus.parser.abstract_syntax_tree.expression.logical.And;
import drds.plus.parser.abstract_syntax_tree.expression.logical.Or;
import drds.plus.parser.abstract_syntax_tree.expression.primary.literal.LiteralNumber;
import drds.plus.parser.abstract_syntax_tree.expression.primary.literal.LiteralString;
import drds.plus.parser.abstract_syntax_tree.expression.primary.misc.Identifier;
import drds.plus.parser.abstract_syntax_tree.expression.primary.misc.RowValues;
import drds.plus.parser.abstract_syntax_tree.expression.primary.misc.Wildcard;
import drds.plus.parser.abstract_syntax_tree.statement.Query;
import drds.plus.parser.abstract_syntax_tree.statement.SelectStatement;
import drds.plus.parser.lexer.Lexer;
import org.junit.Assert;

public class MySQLExprParserTest extends AbstractSyntaxTest {

    public void testFunction1() throws Exception {
        String sql = " EXTRACT(YEAR FROM '1999-07-02') ";
        Lexer lexer = new Lexer(sql);
        ExpressionParser parser = new ExpressionParser(lexer);
        Expression expr = parser.expression();
        String output = output(expr, sql);
        Assert.assertEquals("EXTRACT(YEAR FROM '1999-07-02')", output);

        sql = " GET_FORMAT  (     DATE,  'EUR') ";
        lexer = new Lexer(sql);
        parser = new ExpressionParser(lexer);
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("GET_FORMAT(DATE, 'EUR')", output);

        sql = "UTC_DATE";
        lexer = new Lexer(sql);
        parser = new ExpressionParser(lexer);
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("UTC_DATE()", output);

        sql = "UTC_TIME";
        lexer = new Lexer(sql);
        parser = new ExpressionParser(lexer);
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("UTC_TIME()", output);

        sql = "UTC_TIMeSTAMP";
        lexer = new Lexer(sql);
        parser = new ExpressionParser(lexer);
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("UTC_TIMESTAMP()", output);

        sql = "GET_FORMAT(DATE,'USA')";
        lexer = new Lexer(sql);
        parser = new ExpressionParser(lexer);
        expr = parser.expression();
        output = output(expr, sql);

        sql = "GET_FORMAT(TIME,'USA')";
        lexer = new Lexer(sql);
        parser = new ExpressionParser(lexer);
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("GET_FORMAT(TIME, 'USA')", output);

        sql = "GET_FORMAT(DATETIME,'USA')";
        lexer = new Lexer(sql);
        parser = new ExpressionParser(lexer);
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("GET_FORMAT(DATETIME, 'USA')", output);

        sql = "TIMESTAMPADD(MINUTE_SECOND,1,'2003-01-02')";
        lexer = new Lexer(sql);
        parser = new ExpressionParser(lexer);
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("TIMESTAMPADD(MINUTE_SECOND, 1, '2003-01-02')", output);

        sql = "TIMESTAMPDIFF(MINUTE_SECOND,'2003-01-02','2003-01-02')";
        lexer = new Lexer(sql);
        parser = new ExpressionParser(lexer);
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("TIMESTAMPDIFF(MINUTE_SECOND, '2003-01-02', '2003-01-02')", output);
    }

    public void testExpr1() throws Exception {
        String sql = "\"abc\" /* */  '\\'s' + id2/ id3, 123-456*(ii moD d)";
        Lexer lexer = new Lexer(sql);
        ExpressionParser parser = new ExpressionParser(lexer);
        Expression expr = parser.expression();
        String output = output(expr, sql);
        Assert.assertEquals("'abc\\'s' + ID2 / ID3", output);
        Assert.assertEquals(Add.class, expr.getClass());
        BinaryExpression bex = (BinaryExpression) ((Add) expr).getRight();
        //Assert.assertEquals(Divide.class, bex.getClass());
        Assert.assertEquals(Identifier.class, bex.getRight().getClass());
        lexer.nextToken();
        parser = new ExpressionParser(lexer);
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("123 - 456 * (II % D)", output);
        Assert.assertEquals(Subtract.class, expr.getClass());

        sql = "(n'\"abc\"' \"abc\" /* */  '\\'s' + 1.123e1/ id3)*(.1e3-a||b)mod x'abc'&&(selectStatement 0b1001^b'0000')";
        lexer = new Lexer(sql);
        parser = new ExpressionParser(lexer);
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("(N'\"abc\"abc\\'s' + 11.23 / ID3) * (1E+2 - A OR B) % x'abc' AND (SELECT b'1001' ^ b'0000')", output);
        Assert.assertEquals(And.class, expr.getClass());
        bex = (BinaryExpression) ((And) expr).getExpression(0);
        Assert.assertEquals(Mod.class, bex.getClass());
        bex = (BinaryExpression) bex.getLeft();
        //Assert.assertEquals(Multiply.class, bex.getClass());
        bex = (BinaryExpression) bex.getLeft();
        Assert.assertEquals(Add.class, bex.getClass());
        Assert.assertEquals(LiteralString.class, bex.getLeft().getClass());
        bex = (BinaryExpression) bex.getRight();
        //Assert.assertEquals(Divide.class, bex.getClass());
        Assert.assertEquals(SelectStatement.class, ((And) expr).getExpression(1).getClass());

        sql = "not! ~`query` in (1,current_date,`current_date`)like `all` div a between (c&&d) and (d|e)";
        lexer = new Lexer(sql);
        parser = new ExpressionParser(lexer);
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("NOT ! ~ `SELECT` IN (1, CURDATE(), `CURRENT_DATE`) LIKE `ALL` DIV A BETWEEN (C AND D) AND D | E", output);

//        TernaryOperatorExpression tex = (TernaryOperatorExpression) ((Not) expr).getExpression();
//        Assert.assertEquals(BetweenAnd.class, tex.getClass());
//        Assert.assertEquals(Like.class, tex.getFirst().getClass());
//        Assert.assertEquals(And.class, tex.getSecond().getClass());
//        //Assert.assertEquals(BitOr.class, tex.getNotGreaterThan().getClass());
//        tex = (TernaryOperatorExpression) ((BetweenAnd) tex).getComparee();
//        Assert.assertEquals(In.class, tex.getFirst().getClass());
//        Assert.assertEquals(IntegerDivide.class, tex.getSecond().getClass());
//        bex = (BinaryExpression) (In) tex.getFirst();
//        Assert.assertEquals(NegativeValue.class, bex.getLeft().getClass());
//        Assert.assertEquals(InList.class, bex.getRight().getClass());
//        UnaryExpression uex = (UnaryExpression) ((NegativeValue) bex.getLeft());
        // Assert.assertEquals(BitInvert.class, uex.getExpression().getClass());

        sql = " binary case ~a||b&&c^d xor e when 2>any(query a ) then 3 else 4 end is not null =a";
        lexer = new Lexer(sql);
        parser = new ExpressionParser(lexer);
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("BINARY CASE ~ A OR B AND C ^ D XOR E WHEN 2 > ANY (SELECT A) THEN 3 ELSE 4 END IS NOT NULL = A", output);
        Assert.assertEquals(Equal.class, expr.getClass());

        Assert.assertEquals(Is.class, bex.getLeft().getClass());
        Assert.assertEquals(Identifier.class, bex.getRight().getClass());
        Is cex = (Is) bex.getLeft();
        // Assert.assertEquals(CastBinaryExpression.class, cex.getExpression().getClass());
        // uex = (UnaryExpression) cex.getExpression();
        // Assert.assertEquals(CaseWhen.class, uex.getExpression().getClass());
        // CaseWhen cwex = (CaseWhen) uex.getExpression();
        // Assert.assertEquals(Or.class, cwex.getComparee().getClass());
        // ListExpression pex = (Or) cwex.getComparee();
        // Assert.assertEquals(BitInvert.class, pex.getExpression(0).getClass());
//        Assert.assertEquals(Xor.class, pex.getExpression(1).getClass());
//        bex = (Xor) pex.getExpression(1);
//        Assert.assertEquals(And.class, bex.getLeft().getClass());
//        Assert.assertEquals(Identifier.class, bex.getRight().getClass());
//        pex = (And) bex.getLeft();
//        Assert.assertEquals(Identifier.class, pex.getExpression(0).getClass());
//        //Assert.assertEquals(BitXor.class, pex.getExpression(1).getClass());
//
//        sql_process = " !interval(a,b)<=>a>>b collate x /?+a!=@@1 or @var sounds like -(a-b) mod -(d or e)";
//        lexer = new Lexer(sql_process);
//        parser = new ExpressionParser(lexer);
//        expr = parser.expression();
//        output = output(expr, sql_process);
//        Assert.assertEquals(
//                "! INTERVAL(A, B) <=> A >> B COLLATE x / ? + A != @@1 OR @var SOUNDS LIKE - (A - B) % - (D OR E)",
//                output);
//        Assert.assertEquals(Or.class, expr.getClass());
//        pex = (Or) expr;
//        Assert.assertEquals(NotEqual.class, pex.getExpression(0).getClass());
//        Assert.assertEquals(SoundsLike.class, pex.getExpression(1).getClass());
//        bex = (BinaryExpression) pex.getExpression(0);
//        Assert.assertEquals(NullSafeEquals.class, bex.getLeft().getClass());
//        //Assert.assertEquals(SysVarPrimary.class, bex.getRight().getClass());
//        bex = (BinaryExpression) bex.getLeft();
//        Assert.assertEquals(NegativeValue.class, bex.getLeft().getClass());
//        //Assert.assertEquals(BitShift.class, bex.getRight().getClass());
//        bex = (BinaryExpression) bex.getRight();
//        Assert.assertEquals(Identifier.class, bex.getLeft().getClass());
//        Assert.assertEquals(Add.class, bex.getRight().getClass());
//        bex = (BinaryExpression) bex.getRight();
//        Assert.assertEquals(Divide.class, bex.getLeft().getClass());
//        Assert.assertEquals(Identifier.class, bex.getRight().getClass());
//        bex = (BinaryExpression) bex.getLeft();
//        Assert.assertEquals(CollateExpression.class, bex.getLeft().getClass());
//        Assert.assertEquals(ParameterMarker.class, bex.getRight().getClass());
//        bex = (BinaryExpression) ((Or) expr).getExpression(1);
//        //Assert.assertEquals(UsrDefVarPrimary.class, bex.getLeft().getClass());
//        Assert.assertEquals(Mod.class, bex.getRight().getClass());
//        bex = (BinaryExpression) bex.getRight();
//        Assert.assertEquals(Minus.class, bex.getLeft().getClass());
//        Assert.assertEquals(Minus.class, bex.getRight().getClass());
//        uex = (UnaryExpression) bex.getLeft();
//        Assert.assertEquals(Subtract.class, uex.getExpression().getClass());
//        uex = (UnaryExpression) bex.getRight();
//        Assert.assertEquals(Or.class, uex.getExpression().getClass());
    }

    public void testLogical() throws Exception {
        String sql = "a || b Or c";
        ExpressionParser parser = new ExpressionParser(new Lexer(sql));
        Expression expr = parser.expression();
        String output = output(expr, sql);
        Assert.assertEquals("A OR B OR C", output);
        Assert.assertEquals(Or.class, expr.getClass());
        Or or = (Or) expr;
        Assert.assertEquals(3, or.size());
        Assert.assertEquals("b", ((Identifier) or.getExpression(1)).getText());


        sql = "a and     b && c";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("A AND B AND C", output);
        Assert.assertEquals(And.class, expr.getClass());
        And and = (And) expr;
        Assert.assertEquals(3, or.size());
        Assert.assertEquals("b", ((Identifier) and.getExpression(1)).getText());

        sql = "not NOT Not a";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("NOT NOT NOT A", output);

    }

    public void testComparision() throws Exception {
        String sql = "a  betwEen b and c Not between d and e";
        ExpressionParser parser = new ExpressionParser(new Lexer(sql));
        Expression expr = parser.expression();
        String output = output(expr, sql);
        Assert.assertEquals("A BETWEEN B AND C NOT BETWEEN D AND E", output);
        BetweenAnd ba = (BetweenAnd) expr;
        Assert.assertEquals("a", ((Identifier) ba.getComparee()).getText());
        Assert.assertEquals("b", ((Identifier) ba.getNotLessThan()).getText());
        Assert.assertEquals(false, ba.isNot());
        ba = (BetweenAnd) ba.getNotGreaterThan();
        Assert.assertEquals("c", ((Identifier) ba.getComparee()).getText());
        Assert.assertEquals("d", ((Identifier) ba.getNotLessThan()).getText());
        Assert.assertEquals("e", ((Identifier) ba.getNotGreaterThan()).getText());
        Assert.assertEquals(true, ba.isNot());

        sql = "a between b between c and d and e between f and g";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("A BETWEEN B BETWEEN C AND D AND E BETWEEN F AND G", output);
        ba = (BetweenAnd) expr;
        BetweenAnd ba2 = (BetweenAnd) ba.getNotLessThan();
        BetweenAnd ba3 = (BetweenAnd) ba.getNotGreaterThan();
        Assert.assertEquals("a", ((Identifier) ba.getComparee()).getText());
        Assert.assertEquals("b", ((Identifier) ba2.getComparee()).getText());
        Assert.assertEquals("c", ((Identifier) ba2.getNotLessThan()).getText());
        Assert.assertEquals("d", ((Identifier) ba2.getNotGreaterThan()).getText());
        Assert.assertEquals("e", ((Identifier) ba3.getComparee()).getText());
        Assert.assertEquals("f", ((Identifier) ba3.getNotLessThan()).getText());
        Assert.assertEquals("g", ((Identifier) ba3.getNotGreaterThan()).getText());

        sql = "((query a)) between (query b)   and (query d) ";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("(SELECT A) BETWEEN (SELECT B) AND (SELECT D)", output);

        sql = "a  rliKe b not REGEXP c";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("A REGEXP B NOT REGEXP C", output);
        Regexp re = (Regexp) expr;
        Regexp re2 = (Regexp) re.getLeft();
        Assert.assertEquals("a", ((Identifier) re2.getLeft()).getText());
        Assert.assertEquals("b", ((Identifier) re2.getRight()).getText());
        Assert.assertEquals("c", ((Identifier) re.getRight()).getText());
        Assert.assertEquals(true, re.isNot());
        Assert.assertEquals(false, re2.isNot());

        sql = "((a)) like (((b)))escape (((d)))";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("A LIKE B ESCAPE D", output);

        sql = "((query a)) like (((query b)))escape (((query d)))";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("(SELECT A) LIKE (SELECT B) ESCAPE (SELECT D)", output);

        sql = "a  like b NOT LIKE c escape d";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("A LIKE B NOT LIKE C ESCAPE D", output);
        Like le = (Like) expr;
        Like le2 = (Like) le.getComparee();
        Assert.assertEquals("a", ((Identifier) le2.getComparee()).getText());
        Assert.assertEquals("b", ((Identifier) le2.getPattern()).getText());
        Assert.assertEquals("c", ((Identifier) le.getPattern()).getText());
        Assert.assertEquals("d", ((Identifier) le.getEscape()).getText());
        Assert.assertEquals(true, le.isNot());
        Assert.assertEquals(false, le2.isNot());

        sql = "b NOT LIKE c ";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("B NOT LIKE C", output);

        sql = "a in (b) not in (query id from t1)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("A IN (B) NOT IN (SELECT ID FROM T1)", output);
        In in = (In) expr;
        In in2 = (In) in.getLeft();
        Assert.assertEquals("a", ((Identifier) in2.getLeft()).getText());
        Assert.assertTrue(Query.class.isAssignableFrom(in.getRight().getClass()));
        Assert.assertEquals(true, in.isNot());
        Assert.assertEquals(false, in2.isNot());

        sql = "(query a)is not null";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("(SELECT A) IS NOT NULL", output);

        sql = "a is not null is not false is not true is not UNKNOWn is null is false is true is unknown";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("A IS NOT NULL IS NOT FALSE IS NOT TRUE IS NOT UNKNOWN IS NULL IS FALSE IS TRUE IS UNKNOWN", output);
        Is is = (Is) expr;
        Is is2 = (Is) is.getExpression();
        Is is3 = (Is) is2.getExpression();
        Is is4 = (Is) is3.getExpression();
        Is is5 = (Is) is4.getExpression();
        Is is6 = (Is) is5.getExpression();
        Is is7 = (Is) is6.getExpression();
        Is is8 = (Is) is7.getExpression();
        //Assert.assertEquals(Is.IS_UNKNOWN, is.getMode());
        Assert.assertEquals(Is.IS_TRUE, is2.getMode());
        Assert.assertEquals(Is.IS_FALSE, is3.getMode());
        Assert.assertEquals(Is.IS_NULL, is4.getMode());
        //Assert.assertEquals(Is.IS_NOT_UNKNOWN, is5.getMode());
        Assert.assertEquals(Is.IS_NOT_TRUE, is6.getMode());
        Assert.assertEquals(Is.IS_NOT_FALSE, is7.getMode());
        Assert.assertEquals(Is.IS_NOT_NULL, is8.getMode());
        Assert.assertEquals("a", ((Identifier) is8.getExpression()).getText());

        sql = "a = b <=> c >= d > e <= f < g <> h != i";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("A = B <=> C >= D > E <= F < G <> H != I", output);
        NotEqual neq = (NotEqual) expr;
        //LessOrGreaterThan lg = (LessOrGreaterThan) neq.getLeft();
        LessThan l = null;//(LessThan) lg.getLeft();
        LessThanOrEquals leq = (LessThanOrEquals) l.getLeft();
        GreaterThan g = (GreaterThan) leq.getLeft();
        GreaterThanOrEquals geq = (GreaterThanOrEquals) g.getLeft();

        Assert.assertEquals("i", ((Identifier) neq.getRight()).getText());
        //Assert.assertEquals("h", ((Identifier) lg.getRight()).getText());
        Assert.assertEquals("g", ((Identifier) l.getRight()).getText());
        Assert.assertEquals("f", ((Identifier) leq.getRight()).getText());
        Assert.assertEquals("e", ((Identifier) g.getRight()).getText());
        Assert.assertEquals("d", ((Identifier) geq.getRight()).getText());


        sql = "a like b escape c";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("A LIKE B ESCAPE C", output);

        sql = "(query a) collate z";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("(SELECT A) COLLATE z", output);

        sql = "val1 IN (1,2,'a')";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("VAL1 IN (1, 2, 'a')", output);
    }

    public void testArithmetic() throws Exception {
        String sql = "? + @usrVar1 * c/@@version- e % -f diV g";
        ExpressionParser parser = new ExpressionParser(new Lexer(sql));
        Expression expr = parser.expression();
        String output = output(expr, sql);
        Assert.assertEquals("? + @usrVar1 * C / @@version - E % - F DIV G", output);
        Subtract sub = (Subtract) expr;
        Add add = (Add) sub.getLeft();
        //IntegerDivide idiv = (IntegerDivide) sub.getRight();
        Mod mod = null;//(Mod) idiv.getLeft();
        //Divide div = (Divide) add.getRight();
        //Multiply mt = (Multiply) div.getLeft();


        sql = "a+-b";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("A + - B", output);

        sql = "a+--b";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("A + - - B", output);

        sql = "a++b";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("A + B", output);

        sql = "a+++b";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("A + B", output);

        sql = "a++-b";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("A + - B", output);

        sql = "a + b mod (-((query id from t1 limit 1)- e) ) ";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("A + B % - ((SELECT ID FROM T1 LIMIT 0, 1) - E)", output);
        add = (Add) expr;
        mod = (Mod) add.getRight();
        Assert.assertTrue(Query.class.isAssignableFrom(sub.getLeft().getClass()));
        Assert.assertEquals("e", ((Identifier) sub.getRight()).getText());

    }

    // public void testBitHex() throws Exception

    public void testString() throws Exception {
        String sql = "_latin1'abc\\'d' 'ef\"'";
        Lexer lexer = new Lexer(sql);
        ExpressionParser parser = new ExpressionParser(lexer);
        Expression expr = parser.expression();
        String output = output(expr, sql);
        Assert.assertEquals("_latin1'abc\\'def\"'", output);

        sql = "n'abc\\'d' \"ef'\"\"\"";
        lexer = new Lexer(sql);
        parser = new ExpressionParser(lexer);
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("N'abc\\'def\\'\"'", output);

        sql = "`char`'an'";
        lexer = new Lexer(sql);
        parser = new ExpressionParser(lexer);
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("`CHAR`", output);

        sql = "_latin1 n'abc' ";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("_LATIN1", output);
    }

    public void testAnyAll() throws Exception {
        String sql = "1 >= any (query id from t1 limit 1)";
        ExpressionParser parser = new ExpressionParser(new Lexer(sql));
        Expression expr = parser.expression();
        String output = output(expr, sql);
        Assert.assertEquals("1 >= ANY (SELECT ID FROM T1 LIMIT 0, 1)", output);
        Assert.assertEquals(GreaterThanOrEquals.class, expr.getClass());

        sql = "1 >= any (query ID from t1 limit 1) > aLl(query tb1.id from tb1 t1,tb2 alias t2 where t1.id=t2.id limit 1)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("1 >= ANY (SELECT ID FROM T1 LIMIT 0, 1) > ALL (SELECT TB1.ID FROM TB1 AS T1, TB2 AS T2 WHERE T1.ID = T2.ID LIMIT 0, 1)", output);
        GreaterThan gt = (GreaterThan) expr;
        GreaterThanOrEquals ge = (GreaterThanOrEquals) gt.getLeft();
        Assert.assertEquals(LiteralNumber.class, ge.getLeft().getClass());

        sql = "1 >= any + any";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("1 >= ANY + ANY", output);
    }

    public void testPrimary() throws Exception {
        String sql = "(1,2,existS (query id.* from t1))";
        ExpressionParser parser = new ExpressionParser(new Lexer(sql));
        Expression expr = parser.expression();
        String output = output(expr, sql);
        Assert.assertEquals("ROW(1, 2, EXISTS (SELECT ID.* FROM T1))", output);
        RowValues row = (RowValues) expr;
        Assert.assertEquals(3, row.getRowValueList().size());

        sql = "*";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("*", output);
        Assert.assertTrue(Wildcard.class.isAssignableFrom(expr.getClass()));


        sql = " ${INSENSITIVE}. ";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("${INSENSITIVE}", output);

        sql = "current_date, ";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("CURDATE()", output);

        sql = "CurRent_Date  (  ) ";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("CURDATE()", output);

        sql = "CurRent_TiMe   ";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("CURTIME()", output);

        sql = "CurRent_TiMe  () ";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("CURTIME()", output);

        sql = "CurRent_TimesTamp ";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("NOW()", output);

        sql = "CurRent_TimesTamp  ()";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("NOW()", output);

        sql = "localTimE";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("NOW()", output);

        sql = "localTimE  () ";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("NOW()", output);

        sql = "localTimesTamP  ";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("NOW()", output);

        sql = "localTimesTamP  () ";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("NOW()", output);

        sql = "CurRent_user ";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("CURRENT_USER()", output);

        sql = "CurRent_user  () ";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("CURRENT_USER()", output);

        sql = "default  () ";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("DEFAULT()", output);

        sql = "vaLueS(1,col1*2)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("VALUES(1, COL1 * 2)", output);

        sql = "(1,2,mod(m,n))";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("ROW(1, 2, M % N)", output);

        sql = "chaR (77,121,'77.3')";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("CHAR(77, 121, '77.3')", output);

        sql = "CHARSET(CHAR(0x65 USING utf8))";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("CHARSET(CHAR(x'65' USING utf8))", output);

        sql = "CONVERT(_latin1'Müller' USING utf8)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("CONVERT(_latin1'Müller' USING utf8)", output);

        // QS_TODO

    }

    public void testStartedFromIdentifier() throws Exception {
        // QS_TODO
        String sql = "cast(CAST(1-2 AS UNSIGNED) AS SIGNED)";
        ExpressionParser parser = new ExpressionParser(new Lexer(sql));
        Expression expr = parser.expression();
        String output = output(expr, sql);
        Assert.assertEquals("CAST(CAST(1 - 2 AS UNSIGNED) AS SIGNED)", output);

        sql = "position('a' in \"abc\")";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("LOCATE('a', 'abc')", output);

        sql = "cast(CAST(1-2 AS UNSIGNED integer) AS SIGNED integer)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("CAST(CAST(1 - 2 AS UNSIGNED) AS SIGNED)", output);

        sql = "CAST(expr alias char)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("CAST(EXPR AS CHAR)", output);

        sql = "CAST(6/4 AS DECIMAL(3,1))";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("CAST(6 / 4 AS DECIMAL(3, 1))", output);

        sql = "CAST(6/4 AS DECIMAL(3))";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("CAST(6 / 4 AS DECIMAL(3))", output);

        sql = "CAST(6/4 AS DECIMAL)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("CAST(6 / 4 AS DECIMAL)", output);

        sql = "CAST(now() alias date)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("CAST(NOW() AS DATE)", output);

        sql = "CAST(expr alias char(5))";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("CAST(EXPR AS CHAR(5))", output);

        sql = "SUBSTRING('abc',pos,len)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("SUBSTRING('abc', POS, LEN)", output);

        sql = "SUBSTRING('abc' FROM pos FOR len)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("SUBSTRING('abc', POS, LEN)", output);

        sql = "SUBSTRING(str,pos)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("SUBSTRING(STR, POS)", output);

        sql = "SUBSTRING('abc',1,2)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("SUBSTRING('abc', 1, 2)", output);

        sql = "row(1,2,str)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("ROW(1, 2, STR)", output);

        sql = "position(\"abc\" in '/*abc*/')";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("LOCATE('abc', '/*abc*/')", output);

        sql = "locate(localtime,b)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("LOCATE(NOW(), B)", output);

        sql = "locate(locate(a,b),`match`)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("LOCATE(LOCATE(A, B), `MATCH`)", output);

        sql = "TRIM(LEADING 'x' FROM 'xxxbarxxx')";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("TRIM(LEADING 'x' FROM 'xxxbarxxx')", output);

        sql = "TRIM(BOTH 'x' FROM 'xxxbarxxx')";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("TRIM(BOTH 'x' FROM 'xxxbarxxx')", output);

        sql = "TRIM(TRAILING 'xyz' FROM 'barxxyz')";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("TRIM(TRAILING 'xyz' FROM 'barxxyz')", output);

        sql = "TRIM('  if   ')";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("TRIM('  if   ')", output);

        sql = "TRIM( 'x' FROM 'xxxbarxxx')";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("TRIM('x' FROM 'xxxbarxxx')", output);

        sql = "TRIM(both  FROM 'barxxyz')";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("TRIM(BOTH  FROM 'barxxyz')", output);

        sql = "TRIM(leading  FROM 'barxxyz')";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("TRIM(LEADING  FROM 'barxxyz')", output);

        sql = "TRIM(TRAILING  FROM 'barxxyz')";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("TRIM(TRAILING  FROM 'barxxyz')", output);

        sql = "avg(DISTINCT results)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("AVG(DISTINCT RESULTS)", output);

        sql = "avg(results)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("AVG(RESULTS)", output);

        sql = "max(DISTINCT results)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("MAX(DISTINCT RESULTS)", output);

        sql = "max(results)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("MAX(RESULTS)", output);

        sql = "min(DISTINCT results)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("MIN(DISTINCT RESULTS)", output);

        sql = "min(results)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("MIN(RESULTS)", output);

        sql = "sum(DISTINCT results)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("SUM(DISTINCT RESULTS)", output);

        sql = "sum(results)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("SUM(RESULTS)", output);

        sql = "count(DISTINCT expr1,expr2,expr3)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("COUNT(DISTINCT EXPR1, EXPR2, EXPR3)", output);

        sql = "count(*)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("COUNT(*)", output);

        sql = "GROUP_CONCAT(DISTINCT expr1,expr2,expr3 ORDER BY col_name1 desc,col_name2 SEPARATOR ' ')";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("GROUP_CONCAT(DISTINCT EXPR1, EXPR2, EXPR3 ORDER BY COL_NAME1 desc, COL_NAME2 SEPARATOR  )", output);

        sql = "GROUP_CONCAT(a||b,expr2,expr3 ORDER BY col_name1 asc,col_name2 SEPARATOR '@ ')";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("GROUP_CONCAT(A OR B, EXPR2, EXPR3 ORDER BY COL_NAME1 asc, COL_NAME2 SEPARATOR @ )", output);

        sql = "GROUP_CONCAT(expr1 ORDER BY col_name1 asc,col_name2 SEPARATOR 'str_val ')";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("GROUP_CONCAT(EXPR1 ORDER BY COL_NAME1 asc, COL_NAME2 SEPARATOR str_val )", output);

        sql = "GROUP_CONCAT(DISTINCT test_score ORDER BY test_score desc )";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("GROUP_CONCAT(DISTINCT TEST_SCORE ORDER BY TEST_SCORE desc SEPARATOR ,)", output);

        sql = "GROUP_CONCAT(DISTINCT test_score ORDER BY test_score asc )";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("GROUP_CONCAT(DISTINCT TEST_SCORE ORDER BY TEST_SCORE asc SEPARATOR ,)", output);

        sql = "GROUP_CONCAT(c1)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("GROUP_CONCAT(C1 SEPARATOR ,)", output);

        sql = "GROUP_CONCAT(c1 separator '')";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("GROUP_CONCAT(C1 SEPARATOR )", output);

        sql = "default";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("DEFAULT", output);

        sql = "default(col)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("DEFAULT(COL)", output);

        sql = "database()";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("DATABASE()", output);

        sql = "if(1>2,a+b,a:=1)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("IF(1 > 2, A + B, A := 1)", output);

        sql = "insert('abc',1,2,'')";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("INSERT('abc', 1, 2, '')", output);

        sql = "left(\"hjkafag\",4)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("LEFT('hjkafag', 4)", output);

        sql = "repeat('ag',2.1e1)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("REPEAT('ag', 21)", output);

        sql = "replace('anjd',\"df\",'af')";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("REPLACE('anjd', 'df', 'af')", output);

        sql = "right(\"hjkafag\",4)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("RIGHT('hjkafag', 4)", output);

        sql = "schema()";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("DATABASE()", output);

        sql = "utc_date()";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("UTC_DATE()", output);

        sql = "Utc_time()";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("UTC_TIME()", output);

        sql = "Utc_timestamp()";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("UTC_TIMESTAMP()", output);
    }

    public void testInterval() throws Exception {
        // QS_TODO
        String sql = "DATE_ADD('2009-01-01', INTERVAL (6/4) HOUR_MINUTE)";
        ExpressionParser parser = new ExpressionParser(new Lexer(sql));
        Expression expr = parser.expression();
        String output = output(expr, sql);
        Assert.assertEquals("DATE_ADD('2009-01-01', INTERVAL (6 / 4) HOUR_MINUTE)", output);

        sql = "'2008-12-31 23:59:59' + INTERVAL 1 SECOND";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("'2008-12-31 23:59:59' + INTERVAL 1 SECOND", output);

        sql = " INTERVAL 1 DAY + '2008-12-31'";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("INTERVAL 1 DAY + '2008-12-31'", output);

        sql = "DATE_ADD('2100-12-31 23:59:59',INTERVAL '1:1' MINUTE_SECOND)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("DATE_ADD('2100-12-31 23:59:59', INTERVAL '1:1' MINUTE_SECOND)", output);

        sql = "DATE_SUB('2005-01-01 00:00:00',INTERVAL '1 1:1:1' DAY_SECOND)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("DATE_SUB('2005-01-01 00:00:00', INTERVAL '1 1:1:1' DAY_SECOND)", output);

        sql = "DATE_ADD('1900-01-01 00:00:00',INTERVAL '-1 10' DAY_HOUR)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("DATE_ADD('1900-01-01 00:00:00', INTERVAL '-1 10' DAY_HOUR)", output);

        sql = "DATE_SUB('1998-01-02', INTERVAL 31 DAY)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("DATE_SUB('1998-01-02', INTERVAL 31 DAY)", output);

        sql = "DATE_ADD('1992-12-31 23:59:59.000002',INTERVAL '1.999999' SECOND_MICROSECOND)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("DATE_ADD('1992-12-31 23:59:59.000002', INTERVAL '1.999999' SECOND_MICROSECOND)", output);

        sql = "DATE_ADD('2013-01-01', INTERVAL 1 HOUR)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("DATE_ADD('2013-01-01', INTERVAL 1 HOUR)", output);

        sql = "DATE_ADD('2009-01-30', INTERVAL 1 MONTH)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("DATE_ADD('2009-01-30', INTERVAL 1 MONTH)", output);

        sql = "DATE_ADD('1992-12-31 23:59:59.000002',INTERVAL '1:1.999999' minute_MICROSECOND)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("DATE_ADD('1992-12-31 23:59:59.000002', INTERVAL '1:1.999999' MINUTE_MICROSECOND)", output);

        sql = "DATE_ADD('1992-12-31 23:59:59.000002',INTERVAL '1:1:1.999999' hour_MICROSECOND)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("DATE_ADD('1992-12-31 23:59:59.000002', INTERVAL '1:1:1.999999' HOUR_MICROSECOND)", output);

        sql = "DATE_ADD('2100-12-31 23:59:59',INTERVAL '1:1:1' hour_SECOND)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("DATE_ADD('2100-12-31 23:59:59', INTERVAL '1:1:1' HOUR_SECOND)", output);

        sql = "DATE_ADD('1992-12-31 23:59:59.000002',INTERVAL '1 1:1:1.999999' day_MICROSECOND)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("DATE_ADD('1992-12-31 23:59:59.000002', INTERVAL '1 1:1:1.999999' DAY_MICROSECOND)", output);

        sql = "DATE_ADD('2100-12-31 23:59:59',INTERVAL '1 1:1' day_minute)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("DATE_ADD('2100-12-31 23:59:59', INTERVAL '1 1:1' DAY_MINUTE)", output);

        sql = "DATE_ADD('2100-12-31',INTERVAL '1-1' year_month)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("DATE_ADD('2100-12-31', INTERVAL '1-1' YEAR_MONTH)", output);

        sql = "INTERVAL(n1,n2,n3)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("INTERVAL(N1, N2, N3)", output);

        sql = "INTERVAL a+b day";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("INTERVAL (A + B) DAY", output);

        sql = "INTERVAL(query id from t1) day";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("INTERVAL (SELECT ID FROM T1) DAY", output);

        sql = "INTERVAL(('jklj'+a))day";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("INTERVAL ('jklj' + A) DAY", output);
    }

    public void testMatchExpression() throws Exception {
        // QS_TODO
        String sql = "MATCH (title,body) AGAINST ('database' WITH QUERY EXPANSION)";
        ExpressionParser parser = new ExpressionParser(new Lexer(sql));
        Expression expr = parser.expression();
        String output = output(expr, sql);
        Assert.assertEquals("MATCH (TITLE, BODY) AGAINST ('database' WITH QUERY EXPANSION)", output);
        Assert.assertEquals("MATCH (TITLE, BODY) AGAINST ('database' WITH QUERY EXPANSION)", output);

        sql = "MATCH (title,body) AGAINST ( (abc in (d)) IN boolean MODE)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("MATCH (TITLE, BODY) AGAINST ((ABC IN (D)) IN BOOLEAN MODE)", output);

        sql = "MATCH (title,body) AGAINST ('database')";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("MATCH (TITLE, BODY) AGAINST ('database')", output);

        sql = "MATCH (col1,col2,col3) AGAINST ((a:=b:=c) IN boolean MODE)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("MATCH (COL1, COL2, COL3) AGAINST (A := B := C IN BOOLEAN MODE)", output);

        sql = "MATCH (title,body) AGAINST ((a and (b ||c)) IN boolean MODE)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("MATCH (TITLE, BODY) AGAINST (A AND (B OR C) IN BOOLEAN MODE)", output);

        sql = "MATCH (title,body) AGAINST ((a between b and c) IN boolean MODE)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("MATCH (TITLE, BODY) AGAINST (A BETWEEN B AND C IN BOOLEAN MODE)", output);

        sql = "MATCH (title,body) AGAINST ((A between B and (abc in (D))) IN boolean MODE)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("MATCH (TITLE, BODY) AGAINST ((A BETWEEN B AND ABC IN (D)) IN BOOLEAN MODE)", output);

        sql = "MATCH (title,body) AGAINST ((not not a) IN boolean MODE)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("MATCH (TITLE, BODY) AGAINST (NOT NOT A IN BOOLEAN MODE)", output);

        sql = "MATCH (title,body) AGAINST ((a is true) IN boolean MODE)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("MATCH (TITLE, BODY) AGAINST (A IS TRUE IN BOOLEAN MODE)", output);

        sql = "MATCH (title,body) AGAINST ((query a) IN boolean MODE)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("MATCH (TITLE, BODY) AGAINST (SELECT A IN BOOLEAN MODE)", output);

        sql = "MATCH (title,body) AGAINST ('database' IN NATURAL LANGUAGE MODE)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("MATCH (TITLE, BODY) AGAINST ('database' IN NATURAL LANGUAGE MODE)", output);

        sql = "MATCH (title,body) AGAINST ('database' IN NATURAL LANGUAGE MODE WITH QUERY EXPANSION)";
        parser = new ExpressionParser(new Lexer(sql));
        expr = parser.expression();
        output = output(expr, sql);
        Assert.assertEquals("MATCH (TITLE, BODY) AGAINST ('database' IN NATURAL LANGUAGE MODE WITH QUERY EXPANSION)", output);
    }
}
