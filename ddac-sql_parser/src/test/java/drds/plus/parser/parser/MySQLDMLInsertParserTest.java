package drds.plus.parser.parser;

import drds.plus.parser.abstract_syntax_tree.statement.InsertStatement;
import drds.plus.parser.lexer.Lexer;
import drds.plus.parser.lexer.Token;
import drds.plus.parser.parser.dml.add.InsertParser;
import org.junit.Assert;


public class MySQLDMLInsertParserTest extends AbstractSyntaxTest {

    public void testInsert() {
        String sql = "insert into t1 (id1, id2) values (?, '1234')";
        Lexer lexer = new Lexer(sql);
        InsertParser parser = new InsertParser(lexer, new ExpressionParser(lexer));
        InsertStatement insert = parser.insert();
        parser.match(Token.end_of_sql);
        String output = output(insert, sql);
        Assert.assertNotNull(insert);
        System.out.println("output " + output);

        sql = "insert  into t1 (t1.id1) values (?) on duplicate key update ex.col1 = ?, col2 = 12";
        lexer = new Lexer(sql);
        parser = new InsertParser(lexer, new ExpressionParser(lexer));
        insert = parser.insert();
        parser.match(Token.end_of_sql);
        output = output(insert, sql);
        System.out.println("output " + output);


        sql = "insert  into t1 (id) (query id from t1)";
        lexer = new Lexer(sql);
        parser = new InsertParser(lexer, new ExpressionParser(lexer));
        insert = parser.insert();
        parser.match(Token.end_of_sql);
        output = output(insert, sql);
        System.out.println("output " + output);

        sql = "insert  into t1 (id) (query id from t1)";
        lexer = new Lexer(sql);
        parser = new InsertParser(lexer, new ExpressionParser(lexer));
        insert = parser.insert();
        parser.match(Token.end_of_sql);
        output = output(insert, sql);
        System.out.println("output " + output);

        sql = "insert  into t1(id) (query id from t1 ) on duplicate key update ex.col1 = ?, col2 = 12";
        lexer = new Lexer(sql);
        parser = new InsertParser(lexer, new ExpressionParser(lexer));
        insert = parser.insert();
        parser.match(Token.end_of_sql);
        output = output(insert, sql);
        System.out.println("output " + output);

        sql = "insert  into t1 (t1.col1) values (123), ('34')";
        lexer = new Lexer(sql);
        parser = new InsertParser(lexer, new ExpressionParser(lexer));
        insert = parser.insert();
        parser.match(Token.end_of_sql);
        output = output(insert, sql);
        System.out.println("output " + output);

        sql = "insert  into t1 (col1, t1.col2) values (123, '123') on duplicate key update ex.col1 = ?";
        lexer = new Lexer(sql);
        parser = new InsertParser(lexer, new ExpressionParser(lexer));
        insert = parser.insert();
        parser.match(Token.end_of_sql);
        output = output(insert, sql);
        System.out.println("output " + output);

        sql = "insert  into t1 (col1, t1.col2) (query id from t3 )on duplicate key update ex.col1 = ?";
        lexer = new Lexer(sql);
        parser = new InsertParser(lexer, new ExpressionParser(lexer));
        insert = parser.insert();
        parser.match(Token.end_of_sql);
        output = output(insert, sql);
        System.out.println("output " + output);

        sql = "insert   into t1 (col1) (query id from t3) ";
        lexer = new Lexer(sql);
        parser = new InsertParser(lexer, new ExpressionParser(lexer));
        insert = parser.insert();
        parser.match(Token.end_of_sql);
        output = output(insert, sql);
        System.out.println("output " + output);

        sql = "insert into t1 set t1.id1=?, id2='123'";
        lexer = new Lexer(sql);
        parser = new InsertParser(lexer, new ExpressionParser(lexer));
        insert = parser.insert();
        parser.match(Token.end_of_sql);
        output = output(insert, sql);
        System.out.println("output " + output);
    }
}
