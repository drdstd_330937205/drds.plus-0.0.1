/*
 * Copyright 1999-2012 Alibaba Group.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * (created at 2011-5-19)
 */
package drds.plus.parser.parser;

import drds.plus.parser.abstract_syntax_tree.statement.UpdateStatement;
import drds.plus.parser.lexer.Lexer;
import drds.plus.parser.parser.dml.UpdateParser;
import org.junit.Assert;

import java.sql.SQLSyntaxErrorException;

public class MySQLDMLUpdateParserTest extends AbstractSyntaxTest {

    /**
     * nothing has been pre-consumed <code><pre>
     * 'UPDATE' 'LOW_PRIORITY'? 'IGNORE'? table_reference
     *   'SET' colName ('='|'=') (expr|'DEFAULT') (',' colName ('='|'=') (expr|'DEFAULT'))*
     *     ('WHERE' cond)?
     *     {singleTable}? => ('ORDER' 'BY' orderBy)?  ('LIMIT' count)?
     * </pre></code>
     */

    public void testUpdate() throws SQLSyntaxErrorException {
        String sql = "update  t1 set t1.col1=?, col2='default'";
        Lexer lexer = new Lexer(sql);
        UpdateParser parser = new UpdateParser(lexer, new ExpressionParser(lexer));
        UpdateStatement update = parser.update();
        String output = output(update, sql);
        Assert.assertNotNull(update);
        System.out.println(output);

        sql = "update  t1 set col2='default'   ";
        lexer = new Lexer(sql);
        parser = new UpdateParser(lexer, new ExpressionParser(lexer));
        update = parser.update();
        output = output(update, sql);
        System.out.println(output);


        sql = "update   t1  set col2='default' , col2='123'";
        lexer = new Lexer(sql);
        parser = new UpdateParser(lexer, new ExpressionParser(lexer));
        update = parser.update();
        output = output(update, sql);
        System.out.println(output);

        sql = "update  test.t2 set col2='default' , col2='123'  where id='a'";
        lexer = new Lexer(sql);
        parser = new UpdateParser(lexer, new ExpressionParser(lexer));
        update = parser.update();
        output = output(update, sql);
        System.out.println(output);

    }
}
