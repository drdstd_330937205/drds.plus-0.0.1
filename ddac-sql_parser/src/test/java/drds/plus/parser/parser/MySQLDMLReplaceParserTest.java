/*
 * Copyright 1999-2012 Alibaba Group.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * (created at 2011-5-19)
 */
package drds.plus.parser.parser;

import drds.plus.parser.abstract_syntax_tree.statement.ReplaceStatement;
import drds.plus.parser.lexer.Lexer;
import drds.plus.parser.lexer.Token;
import drds.plus.parser.parser.dml.add.ReplaceParser;
import org.junit.Assert;

import java.sql.SQLSyntaxErrorException;


public class MySQLDMLReplaceParserTest extends AbstractSyntaxTest {

    public void testReplace() throws SQLSyntaxErrorException {
        String sql = "replace  into t1 set t1.id1=?, id2='123'";
        Lexer lexer = new Lexer(sql);
        ReplaceParser parser = new ReplaceParser(lexer, new ExpressionParser(lexer));
        ReplaceStatement replace = parser.replace();
        parser.match(Token.end_of_sql);
        Assert.assertNotNull(replace);
        String output = output(replace, sql);
        System.out.println("output " + output);

        sql = "replace into  test.t1 set t1.id1=? ";
        lexer = new Lexer(sql);
        parser = new ReplaceParser(lexer, new ExpressionParser(lexer));
        replace = parser.replace();
        parser.match(Token.end_of_sql);
        output = output(replace, sql);
        System.out.println("output " + output);

        sql = "replace into t1 (id,id)values (123,?) ";
        lexer = new Lexer(sql);
        parser = new ReplaceParser(lexer, new ExpressionParser(lexer));
        replace = parser.replace();
        parser.match(Token.end_of_sql);
        output = output(replace, sql);
        System.out.println("output " + output);

        sql = "replace into t1(id) values (12.123), (?),('okn')";
        lexer = new Lexer(sql);
        parser = new ReplaceParser(lexer, new ExpressionParser(lexer));
        replace = parser.replace();
        parser.match(Token.end_of_sql);
        output = output(replace, sql);
        System.out.println("output " + output);

        sql = "replace into t1 (id) (query id from t1)";
        lexer = new Lexer(sql);
        parser = new ReplaceParser(lexer, new ExpressionParser(lexer));
        replace = parser.replace();
        parser.match(Token.end_of_sql);
        output = output(replace, sql);
        System.out.println("output " + output);


        sql = "replace into t1 (t1.col1) values (123),('12')";
        lexer = new Lexer(sql);
        parser = new ReplaceParser(lexer, new ExpressionParser(lexer));
        replace = parser.replace();
        parser.match(Token.end_of_sql);
        output = output(replace, sql);
        System.out.println("output " + output);

        sql = "replace into t1 (col1, t1.col2) values (123,'1234') ";
        lexer = new Lexer(sql);
        parser = new ReplaceParser(lexer, new ExpressionParser(lexer));
        replace = parser.replace();
        parser.match(Token.end_of_sql);
        output = output(replace, sql);
        System.out.println("output " + output);

        sql = "replace into t1 (col1, t1.col2) (query id,id from t3) ";
        lexer = new Lexer(sql);
        parser = new ReplaceParser(lexer, new ExpressionParser(lexer));
        replace = parser.replace();
        parser.match(Token.end_of_sql);
        output = output(replace, sql);
        System.out.println("output " + output);

        sql = "replace into   t1 (col1) ( query id from t3) ";
        lexer = new Lexer(sql);
        parser = new ReplaceParser(lexer, new ExpressionParser(lexer));
        replace = parser.replace();
        parser.match(Token.end_of_sql);
        output = output(replace, sql);
        System.out.println("output " + output);

    }
}
