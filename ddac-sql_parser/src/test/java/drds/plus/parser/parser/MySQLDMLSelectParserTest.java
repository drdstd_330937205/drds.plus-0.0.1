/*
 * Copyright 1999-2012 Alibaba Group.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * (created at 2011-5-17)
 */
package drds.plus.parser.parser;

import drds.plus.parser.abstract_syntax_tree.statement.Query;
import drds.plus.parser.abstract_syntax_tree.statement.QueryStatement;
import drds.plus.parser.abstract_syntax_tree.statement.SelectStatement;
import drds.plus.parser.abstract_syntax_tree.statement.UnionStatement;
import drds.plus.parser.lexer.Lexer;
import org.junit.Assert;

import java.sql.SQLSyntaxErrorException;


public class MySQLDMLSelectParserTest extends AbstractSyntaxTest {

    @SuppressWarnings("unused")
    public static void main(String[] ars) throws Exception {
        String sql = " query id, member_id , image_path  \t , image_size , status,   gmt_modified from    wp_image where \t\t\n id =  ? and member_id\t=\t-123.456";
        //sql_process = " query id, member_id , image_path  \t , image_size , status,   gmt_modified from    wp_image where \t\t\n id = ?";
        Lexer lexer = new Lexer(sql);
        ExpressionParser expressionParser = new ExpressionParser(lexer);
        SelectParser parser = new SelectParser(lexer, expressionParser);
        Query stmt = parser.selectStatement();
        System.out.println(stmt);
    }


    public void testSelectUnion() throws SQLSyntaxErrorException {
        String sql = "(query id from t1) union all (query id from t2) union all (query id from t3)   order by d desc,a asc offset 1 size ?";
        Lexer lexer = new Lexer(sql);
        SelectParser parser = new SelectParser(lexer, new ExpressionParser(lexer));
        QueryStatement queryStatement = parser.union();
        if (queryStatement instanceof UnionStatement) {
            UnionStatement unionStatement = (UnionStatement) queryStatement;
            Assert.assertEquals(0, unionStatement.getFirstDistinctIndex());
            Assert.assertEquals(3, unionStatement.getSelectStatementList().size());
            String output = output(unionStatement, sql);
            System.out.println(output);
        }


        sql = "(query id from t1) union  (query id from t2 where 1=1) union all (query id from t3)    order by d asc";
        lexer = new Lexer(sql);
        parser = new SelectParser(lexer, new ExpressionParser(lexer));
        queryStatement = parser.union();

        if (queryStatement instanceof UnionStatement) {
            UnionStatement unionStatement = (UnionStatement) queryStatement;
            Assert.assertEquals(1, unionStatement.getFirstDistinctIndex());
            Assert.assertEquals(3, unionStatement.getSelectStatementList().size());
            String output = output(unionStatement, sql);
            System.out.println(output);
        }

        sql = "(query id from t1) union distinct (query id from t2) union  (query id from t3)";
        lexer = new Lexer(sql);
        parser = new SelectParser(lexer, new ExpressionParser(lexer));
        queryStatement = parser.union();

        if (queryStatement instanceof UnionStatement) {
            UnionStatement unionStatement = (UnionStatement) queryStatement;
            Assert.assertEquals(2, unionStatement.getFirstDistinctIndex());
            Assert.assertEquals(3, unionStatement.getSelectStatementList().size());
            String output = output(unionStatement, sql);
            System.out.println(output);
        }

    }

    public void testSelect() throws SQLSyntaxErrorException {
        String sql = "query t1.id , t2.* from t1, t2 where t1.id=1 and t1.id=t2.id";
        Lexer lexer = new Lexer(sql);
        SelectParser parser = new SelectParser(lexer, new ExpressionParser(lexer));
        SelectStatement select = parser.selectStatement();
        Assert.assertNotNull(select);
        String output = output(select, sql);
        System.out.println(output);

        sql = "query * from  offer as a  join wp_image as  b  on a.member_id=b.member_id  join product_visit as c where a.member_id=c.member_id and c.member_id='abc' ";
        lexer = new Lexer(sql);
        parser = new SelectParser(lexer, new ExpressionParser(lexer));
        select = parser.selectStatement();
        Assert.assertNotNull(select);
        output = output(select, sql);
        System.out.println(output);

        sql = "query  tb1.id,tb2.id from tb1,tb2 where tb1.id2=tb2.id2";
        lexer = new Lexer(sql);
        parser = new SelectParser(lexer, new ExpressionParser(lexer));
        select = parser.selectStatement();
        Assert.assertNotNull(select);
        output = output(select, sql);
        System.out.println(output);


        sql = "query   id1,id2 from tb1,tb2 where tb1.id1=tb2.id1 ";
        lexer = new Lexer(sql);
        parser = new SelectParser(lexer, new ExpressionParser(lexer));
        select = parser.selectStatement();
        Assert.assertNotNull(select);
        output = output(select, sql);
        System.out.println(output);


        sql = "query   tb1.id1,id2 from tb1";
        lexer = new Lexer(sql);
        parser = new SelectParser(lexer, new ExpressionParser(lexer));
        select = parser.selectStatement();
        Assert.assertNotNull(select);
        output = output(select, sql);
        System.out.println(output);

        sql = "query   tb1.id1,id2 from tb1";
        lexer = new Lexer(sql);
        parser = new SelectParser(lexer, new ExpressionParser(lexer));
        select = parser.selectStatement();
        Assert.assertNotNull(select);
        output = output(select, sql);
        System.out.println(output);

        sql = "query   tb1.id1,id2 from tb1";
        lexer = new Lexer(sql);
        parser = new SelectParser(lexer, new ExpressionParser(lexer));
        select = parser.selectStatement();
        Assert.assertNotNull(select);
        output = output(select, sql);
        System.out.println(output);


        sql = "query t1.* from tb ";
        lexer = new Lexer(sql);
        parser = new SelectParser(lexer, new ExpressionParser(lexer));
        select = parser.selectStatement();
        Assert.assertNotNull(select);
        output = output(select, sql);
        System.out.println(output);

        sql = "query      tb1.id,tb2.id " + "from tb1,tb2 where tb1.id2=tb2.id2";
        lexer = new Lexer(sql);
        parser = new SelectParser(lexer, new ExpressionParser(lexer));
        select = parser.selectStatement();
        Assert.assertNotNull(select);
        output = output(select, sql);
        System.out.println(output);

        sql = "query  id1,id2 from tb1,tb2 where tb1.id1=tb2.id2 for update";
        lexer = new Lexer(sql);
        parser = new SelectParser(lexer, new ExpressionParser(lexer));
        select = parser.selectStatement();
        Assert.assertNotNull(select);
        output = output(select, sql);
        System.out.println(output);

        sql = "query  id1,id2 from tb1,tb2 where tb1.id1=tb2.id2 lock in share mode";
        lexer = new Lexer(sql);
        parser = new SelectParser(lexer, new ExpressionParser(lexer));
        select = parser.selectStatement();
        Assert.assertNotNull(select);
        output = output(select, sql);
        System.out.println(output);


        sql = "query   id1,id2 from tb1 group by id1,id2 having id1>10 order by id3 desc";
        lexer = new Lexer(sql);
        parser = new SelectParser(lexer, new ExpressionParser(lexer));
        select = parser.selectStatement();
        Assert.assertNotNull(select);
        output = output(select, sql);
        System.out.println(output);


    }

    public void testMax() throws SQLSyntaxErrorException {
        String sql = "query   id1,max(id2),min(id2),sum(id2),count(id2),avg(id2) from tb1 group by id1,id2 having id1>10 order by id3 desc";
        Lexer lexer = new Lexer(sql);
        SelectParser parser = new SelectParser(lexer, new ExpressionParser(lexer));
        SelectStatement select = parser.selectStatement();
        Assert.assertNotNull(select);
        String output = output(select, sql);
        System.out.println(output);
    }

    public void testSelectChinese() throws SQLSyntaxErrorException {
        String sql = "query t1.id , t2.* from t1, t2 where t1.id='中文' and t1.id=test.t2.id";
        Lexer lexer = new Lexer(sql);
        SelectParser parser = new SelectParser(lexer, new ExpressionParser(lexer));
        SelectStatement select = parser.selectStatement();
        Assert.assertNotNull(select);
        String output = output(select, sql);
        System.out.println(output);
    }

}
