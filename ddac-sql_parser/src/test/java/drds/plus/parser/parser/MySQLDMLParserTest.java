package drds.plus.parser.parser;

import drds.plus.parser.abstract_syntax_tree.expression.Expression;
import drds.plus.parser.abstract_syntax_tree.expression.comparison.range.Equal;
import drds.plus.parser.abstract_syntax_tree.expression.logical.And;
import drds.plus.parser.abstract_syntax_tree.expression.primary.misc.ParameterMarker;
import drds.plus.parser.abstract_syntax_tree.statement.SelectStatement;
import drds.plus.parser.abstract_syntax_tree.statement.select.Limit;
import drds.plus.parser.abstract_syntax_tree.statement.select.table.SubQuery;
import drds.plus.parser.abstract_syntax_tree.statement.select.table.*;
import drds.plus.parser.lexer.Lexer;
import drds.plus.parser.parser.dml.DmlParser;
import org.junit.Assert;

import java.util.List;

public class MySQLDMLParserTest extends AbstractSyntaxTest {

    protected DmlParser getDMLParser(Lexer lexer) {
        ExpressionParser expressionParser = new ExpressionParser(lexer);
        DmlParser parser = new SelectParser(lexer, expressionParser);
        return parser;
    }

    protected ExpressionParser getExpressionParser(Lexer lexer) {
        ExpressionParser expressionParser = new ExpressionParser(lexer);
        return expressionParser;
    }

    public void testLimit() {
        String sql = "offset 1 size 2";
        Lexer lexer = new Lexer(sql);
        ExpressionParser parser = getExpressionParser(lexer);
        Limit limit = parser.limit();
        String output = output(limit, sql);
        Assert.assertEquals(1, limit.getOffset());
        Assert.assertEquals(2, limit.getSize());
        Assert.assertEquals("LIMIT 1, 2", output);

        sql = "offset 1 size ?";
        lexer = new Lexer(sql);
        parser = getExpressionParser(lexer);
        limit = parser.limit();
        output = output(limit, sql);
        Assert.assertEquals(1, limit.getOffset());
        Assert.assertEquals(new ParameterMarker(1), limit.getSize());
        Assert.assertEquals("LIMIT 1, ?", output);

        sql = "offset ? size 9";
        lexer = new Lexer(sql);
        parser = getExpressionParser(lexer);
        limit = parser.limit();
        output = output(limit, sql);
        Assert.assertEquals(new ParameterMarker(1), limit.getOffset());
        Assert.assertEquals(9, limit.getSize());
        Assert.assertEquals("LIMIT ?, 9", output);

        sql = "offset ? size ?";
        lexer = new Lexer(sql);
        parser = getExpressionParser(lexer);
        limit = parser.limit();
        output = output(limit, sql);
        Assert.assertEquals(new ParameterMarker(1), limit.getOffset());
        Assert.assertEquals(new ParameterMarker(2), limit.getSize());
        Assert.assertEquals("LIMIT ?, ?", output);

    }

    public void testTR1() {
        String sql = "(selectStatement * from  user) a";
        Lexer lexer = new Lexer(sql);
        DmlParser parser = getDMLParser(lexer);
        Tables trs = parser.tables();
        String output = output(trs, sql);
        List<ITable> list = trs.getTableList();
        Assert.assertEquals(1, list.size());
        Assert.assertEquals(SubQuery.class, list.get(0).getClass());
        Assert.assertEquals("(SELECT * FROM `SELECT`) AS `SELECT`", output);

        sql = "(((selecT * from any)union query `query` from `from` order by dd) alias 'a1', (((t2)))), t3";
        lexer = new Lexer(sql);
        parser = getDMLParser(lexer);
        trs = parser.tables();
        output = output(trs, sql);
        list = trs.getTableList();
        Assert.assertEquals(2, list.size());
        Assert.assertEquals(Tables.class, list.get(0).getClass());
        Assert.assertEquals(Table.class, list.get(1).getClass());
        list = ((Tables) list.get(0)).getTableList();
        Assert.assertEquals(2, list.size());
        Assert.assertEquals(SubQuery.class, list.get(0).getClass());
        Assert.assertEquals(Tables.class, list.get(1).getClass());
        Assert.assertEquals("((SELECT * FROM ANY) UNION (SELECT `SELECT` FROM `FROM` ORDER BY DD)) AS 'a1', T2, T3", output);

        sql = "(t1)";
        lexer = new Lexer(sql);
        parser = getDMLParser(lexer);
        trs = parser.tables();
        output = output(trs, sql);
        list = trs.getTableList();
        Assert.assertEquals(1, list.size());
        ITable tr = list.get(0);
        Assert.assertEquals(Tables.class, tr.getClass());
        list = ((Tables) tr).getTableList();
        Assert.assertEquals(1, list.size());
        Assert.assertEquals(Table.class, list.get(0).getClass());
        Assert.assertEquals("T1", output);

        sql = "(t1,t2,(t3))";
        lexer = new Lexer(sql);
        parser = getDMLParser(lexer);
        trs = parser.tables();
        output = output(trs, sql);
        list = trs.getTableList();
        Assert.assertEquals(1, list.size());
        Assert.assertEquals(Tables.class, list.get(0).getClass());
        tr = list.get(0);
        Assert.assertEquals(Tables.class, tr.getClass());
        list = ((Tables) tr).getTableList();
        Assert.assertEquals(3, list.size());
        Assert.assertEquals(Table.class, list.get(0).getClass());
        Assert.assertEquals(Table.class, list.get(1).getClass());
        Assert.assertEquals(Table.class, list.get(1).getClass());
        Assert.assertEquals("T1, T2, T3", output);

        sql = "(tb1 alias t1)inner join (tb2 alias t2) on t1.id=t2.id";
        lexer = new Lexer(sql);
        parser = getDMLParser(lexer);
        trs = parser.tables();
        output = output(trs, sql);
        list = trs.getTableList();
        Assert.assertEquals(1, list.size());
        Assert.assertEquals(InnerJoin.class, list.get(0).getClass());
        tr = ((InnerJoin) list.get(0)).getLeftTable();
        Assert.assertEquals(Tables.class, tr.getClass());
        list = ((Tables) tr).getTableList();
        Assert.assertEquals(1, list.size());
        Assert.assertEquals(Table.class, list.get(0).getClass());
        Expression ex = ((InnerJoin) (trs.getTableList()).get(0)).getOn();
        Assert.assertEquals(ex.getClass(), Equal.class);
        Assert.assertEquals("(TB1 AS T1) INNER JOIN (TB2 AS T2) ON T1.NAME = T2.NAME", output);

        sql = "(tb1 alias t1)inner join tb2 alias t2 using (c1)";
        lexer = new Lexer(sql);
        parser = getDMLParser(lexer);
        trs = parser.tables();
        output = output(trs, sql);

        Assert.assertEquals("(TB1 AS T1) INNER JOIN TB2 AS T2 USING (c1)", output);

        sql = "tb1 alias t1 use index (i1,i2,i3)";
        lexer = new Lexer(sql);
        parser = getDMLParser(lexer);
        trs = parser.tables();
        output = output(trs, sql);
        list = trs.getTableList();
        Assert.assertEquals(1, list.size());
        Assert.assertEquals(Table.class, list.get(0).getClass());

        Assert.assertEquals("TB1 AS T1 USE INDEX (i1, i2, i3)", output);

        sql = "tb1 alias t1 use index (i1,i2,i3),tb2 alias t2 use index (i1,i2,i3)";
        lexer = new Lexer(sql);
        parser = getDMLParser(lexer);
        trs = parser.tables();
        output = output(trs, sql);
        list = trs.getTableList();
        Assert.assertEquals(2, list.size());
        Assert.assertEquals(Table.class, list.get(0).getClass());

        Assert.assertEquals("TB1 AS T1 USE INDEX (i1, i2, i3), TB2 AS T2 USE INDEX (i1, i2, i3)", output);

        sql = "tb1 alias t1";
        lexer = new Lexer(sql);
        parser = getDMLParser(lexer);
        trs = parser.tables();
        output = output(trs, sql);
        list = trs.getTableList();
        Assert.assertEquals(1, list.size());
        Assert.assertEquals(Table.class, list.get(0).getClass());
        Assert.assertEquals("tb1", ((Table) (trs.getTableList()).get(0)).getTableName().getText());
        Assert.assertEquals("T1", ((Table) (trs.getTableList()).get(0)).getAlias());
        Assert.assertEquals("TB1 AS T1", output);

        sql = "tb1 t1";
        lexer = new Lexer(sql);
        parser = getDMLParser(lexer);
        trs = parser.tables();
        output = output(trs, sql);
        list = trs.getTableList();
        Assert.assertEquals(1, list.size());
        Assert.assertEquals(Table.class, list.get(0).getClass());
        Assert.assertEquals("tb1", ((Table) (trs.getTableList()).get(0)).getTableName().getText());
        Assert.assertEquals("T1", ((Table) (trs.getTableList()).get(0)).getAlias());
        Assert.assertEquals("TB1 AS T1", output);

        sql = "tb1,tb2,tb3";
        lexer = new Lexer(sql);
        parser = getDMLParser(lexer);
        trs = parser.tables();
        output = output(trs, sql);
        list = trs.getTableList();
        Assert.assertEquals(3, list.size());
        Assert.assertEquals(Table.class, list.get(0).getClass());
        Assert.assertEquals("tb1", ((Table) (trs.getTableList()).get(0)).getTableName().getText());
        Assert.assertEquals(null, ((Table) (trs.getTableList()).get(0)).getAlias());
        Assert.assertEquals("tb2", ((Table) (trs.getTableList()).get(1)).getTableName().getText());
        Assert.assertEquals("tb3", ((Table) (trs.getTableList()).get(2)).getTableName().getText());
        Assert.assertEquals("TB1, TB2, TB3", output);

        sql = "tb1 use key for join (i1,i2)";
        lexer = new Lexer(sql);
        parser = getDMLParser(lexer);
        trs = parser.tables();
        output = output(trs, sql);
        list = trs.getTableList();
        Assert.assertEquals(1, list.size());
        Assert.assertEquals(Table.class, list.get(0).getClass());

        Assert.assertEquals("TB1 USE KEY FOR JOIN (i1, i2)", output);

        sql = "tb1 use index for group by(i1,i2)";
        lexer = new Lexer(sql);
        parser = getDMLParser(lexer);
        trs = parser.tables();
        output = output(trs, sql);
        list = trs.getTableList();
        Assert.assertEquals(1, list.size());
        Assert.assertEquals(Table.class, list.get(0).getClass());

        Assert.assertEquals("TB1 USE INDEX FOR GROUP BY (i1, i2)", output);

        sql = "tb1 use key for order by (i1,i2) use key for group by () " + "ignore index for group by (i1,i2)";
        lexer = new Lexer(sql);
        parser = getDMLParser(lexer);
        trs = parser.tables();
        output = output(trs, sql);
        list = trs.getTableList();
        Assert.assertEquals(1, list.size());
        Assert.assertEquals(Table.class, list.get(0).getClass());
        Assert.assertEquals("tb1", ((Table) (trs.getTableList()).get(0)).getTableName().getText());
        Assert.assertEquals(null, ((Table) (trs.getTableList()).get(0)).getAlias());

        Assert.assertEquals("TB1 USE KEY FOR ORDER BY (i1, i2) " + "USE KEY FOR GROUP BY () IGNORE INDEX FOR GROUP BY (i1, i2)", output);

        sql = "tb1 use index for order by (i1,i2) force index for group by (i1)";
        lexer = new Lexer(sql);
        parser = getDMLParser(lexer);
        trs = parser.tables();
        output = output(trs, sql);
        list = trs.getTableList();
        Assert.assertEquals(1, list.size());
        Assert.assertEquals(Table.class, list.get(0).getClass());
        Assert.assertEquals("tb1", ((Table) (trs.getTableList()).get(0)).getTableName().getText());
        Assert.assertEquals(null, ((Table) (trs.getTableList()).get(0)).getAlias());

        Assert.assertEquals("TB1 USE INDEX FOR ORDER BY (i1, i2) FORCE INDEX FOR GROUP BY (i1)", output);

        sql = "tb1 ignore key for join (i1,i2)";
        lexer = new Lexer(sql);
        parser = getDMLParser(lexer);
        trs = parser.tables();
        output = output(trs, sql);
        list = trs.getTableList();
        Assert.assertEquals(1, list.size());
        Assert.assertEquals(Table.class, list.get(0).getClass());
        Assert.assertEquals("tb1", ((Table) (trs.getTableList()).get(0)).getTableName().getText());
        Assert.assertEquals(null, ((Table) (trs.getTableList()).get(0)).getAlias());

        Assert.assertEquals("TB1 IGNORE KEY FOR JOIN (i1, i2)", output);

        sql = "tb1 ignore index for group by (i1,i2)";
        lexer = new Lexer(sql);
        parser = getDMLParser(lexer);
        trs = parser.tables();
        output = output(trs, sql);
        list = trs.getTableList();
        Assert.assertEquals(1, list.size());
        Assert.assertEquals(Table.class, list.get(0).getClass());
        Assert.assertEquals("tb1", ((Table) (trs.getTableList()).get(0)).getTableName().getText());
        Assert.assertEquals(null, ((Table) (trs.getTableList()).get(0)).getAlias());

        Assert.assertEquals("TB1 IGNORE INDEX FOR GROUP BY (i1, i2)", output);

        sql = "(offer  a  straight_join wp_image b use key for join(t1,t2) on a.member_id=b.member_id inner join product_visit c )";
        lexer = new Lexer(sql);
        parser = getDMLParser(lexer);
        trs = parser.tables();
        output = output(trs, sql);
        Assert.assertEquals("OFFER AS A STRAIGHT_JOIN WP_IMAGE AS B USE KEY FOR JOIN (t1, t2) ON A.MEMBER_ID = B.MEMBER_ID INNER JOIN PRODUCT_VISIT AS C", output);

        sql = "tb1 ignore index for order by(i1)";
        lexer = new Lexer(sql);
        parser = getDMLParser(lexer);
        trs = parser.tables();
        output = output(trs, sql);
        list = trs.getTableList();
        Assert.assertEquals(1, list.size());
        Assert.assertEquals(Table.class, list.get(0).getClass());
        Assert.assertEquals("tb1", ((Table) (trs.getTableList()).get(0)).getTableName().getText());
        Assert.assertEquals(null, ((Table) (trs.getTableList()).get(0)).getAlias());

        Assert.assertEquals("TB1 IGNORE INDEX FOR ORDER BY (i1)", output);

        sql = "tb1 force key for group by (i1,i2)";
        lexer = new Lexer(sql);
        parser = getDMLParser(lexer);
        trs = parser.tables();
        output = output(trs, sql);
        list = trs.getTableList();
        Assert.assertEquals(1, list.size());
        Assert.assertEquals(Table.class, list.get(0).getClass());
        Assert.assertEquals("tb1", ((Table) (trs.getTableList()).get(0)).getTableName().getText());
        Assert.assertEquals(null, ((Table) (trs.getTableList()).get(0)).getAlias());

        Assert.assertEquals("TB1 FORCE KEY FOR GROUP BY (i1, i2)", output);

        sql = "tb1 force index for group by (i1,i2)";
        lexer = new Lexer(sql);
        parser = getDMLParser(lexer);
        trs = parser.tables();
        output = output(trs, sql);
        list = trs.getTableList();
        Assert.assertEquals(1, list.size());
        Assert.assertEquals(Table.class, list.get(0).getClass());
        Assert.assertEquals("tb1", ((Table) (trs.getTableList()).get(0)).getTableName().getText());
        Assert.assertEquals(null, ((Table) (trs.getTableList()).get(0)).getAlias());

        Assert.assertEquals("TB1 FORCE INDEX FOR GROUP BY (i1, i2)", output);

        sql = "tb1 force index for join (i1,i2)";
        lexer = new Lexer(sql);
        parser = getDMLParser(lexer);
        trs = parser.tables();
        output = output(trs, sql);
        list = trs.getTableList();
        Assert.assertEquals(1, list.size());
        Assert.assertEquals(Table.class, list.get(0).getClass());
        Assert.assertEquals("tb1", ((Table) (trs.getTableList()).get(0)).getTableName().getText());
        Assert.assertEquals(null, ((Table) (trs.getTableList()).get(0)).getAlias());

        Assert.assertEquals("TB1 FORCE INDEX FOR JOIN (i1, i2)", output);

        sql = "(tb1 force index for join (i1,i2) )left outer join tb2 alias t2 " + "use index (i1,i2,i3) on t1.id1=t2.id1";
        lexer = new Lexer(sql);
        parser = getDMLParser(lexer);
        trs = parser.tables();
        output = output(trs, sql);
        list = trs.getTableList();
        Assert.assertEquals(1, list.size());
        Assert.assertEquals(OuterJoin.class, list.get(0).getClass());
        Assert.assertEquals(true, (((OuterJoin) list.get(0)).isLeftJoin()));
        Tables ltr = (Tables) ((OuterJoin) list.get(0)).getLeftTable();
        Assert.assertEquals(1, ltr.getTableList().size());
        Assert.assertEquals(Table.class, ltr.getTableList().get(0).getClass());
        Assert.assertEquals(null, ((Table) (ltr.getTableList().get(0))).getAlias());
        Assert.assertEquals("tb1", ((Table) (ltr.getTableList().get(0))).getTableName().getText());

        Table rtf = (Table) ((OuterJoin) list.get(0)).getRightTable();
        Assert.assertEquals("T2", rtf.getAlias());
        Assert.assertEquals("tb2", rtf.getTableName().getText());

        Assert.assertEquals(Equal.class, ((OuterJoin) list.get(0)).getOn().getClass());
        Assert.assertEquals("(TB1 FORCE INDEX FOR JOIN (i1, i2)) " + "LEFT JOIN TB2 AS T2 USE INDEX (i1, i2, i3) ON T1.ID1 = T2.ID1", output);

        sql = " (((tb1 force index for join (i1,i2),tb3),tb4),tb5) " + "left outer join (tb2 alias t2 use index (i1,i2,i3)) using(id1)";
        lexer = new Lexer(sql);
        parser = getDMLParser(lexer);
        trs = parser.tables();
        output = output(trs, sql);
        list = trs.getTableList();
        Assert.assertEquals(1, list.size());
        Assert.assertEquals(OuterJoin.class, list.get(0).getClass());
        Assert.assertEquals(true, (((OuterJoin) list.get(0)).isLeftJoin()));
        ltr = (Tables) ((OuterJoin) list.get(0)).getLeftTable();
        Assert.assertEquals(2, ltr.getTableList().size());
        Assert.assertEquals(Tables.class, ltr.getTableList().get(0).getClass());
        Tables ltr1 = (Tables) (ltr.getTableList()).get(0);
        Assert.assertEquals(2, ltr1.getTableList().size());
        Assert.assertEquals(Tables.class, ltr1.getTableList().get(0).getClass());
        Tables ltr2 = (Tables) (ltr1.getTableList()).get(0);
        Assert.assertEquals(2, ltr2.getTableList().size());
        Assert.assertEquals(Table.class, ltr2.getTableList().get(0).getClass());
        Assert.assertEquals(null, ((Table) (ltr2.getTableList().get(0))).getAlias());
        Assert.assertEquals("tb1", ((Table) (ltr2.getTableList().get(0))).getTableName().getText());

        Assert.assertEquals(Table.class, ltr2.getTableList().get(1).getClass());
        Assert.assertEquals("tb3", ((Table) (ltr2.getTableList().get(1))).getTableName().getText());
        Assert.assertEquals(Table.class, ltr1.getTableList().get(1).getClass());
        Assert.assertEquals("tb4", ((Table) (ltr1.getTableList().get(1))).getTableName().getText());
        Assert.assertEquals(Table.class, ltr.getTableList().get(1).getClass());
        Assert.assertEquals("tb5", ((Table) (ltr.getTableList().get(1))).getTableName().getText());
        Tables rtr = (Tables) ((OuterJoin) list.get(0)).getRightTable();
        Assert.assertEquals("T2", ((Table) rtr.getTableList().get(0)).getAlias());
        Assert.assertEquals("tb2", ((Table) rtr.getTableList().get(0)).getTableName().getText());


        Assert.assertEquals("(TB1 FORCE INDEX FOR JOIN (i1, i2), TB3, TB4, TB5) LEFT JOIN " + "(TB2 AS T2 USE INDEX (i1, i2, i3)) USING (id1)", output);

        sql = "(tb1 force index for join (i1,i2),tb3) " + "left outer join tb2 alias t2 use index (i1,i2,i3) using(id1,id2)";
        lexer = new Lexer(sql);
        parser = getDMLParser(lexer);
        trs = parser.tables();
        output = output(trs, sql);
        Assert.assertEquals("(TB1 FORCE INDEX FOR JOIN (i1, i2), TB3) LEFT JOIN TB2 AS T2 USE INDEX (i1, i2, i3) USING (id1, id2)", output);

        sql = "(tb1 force index for join (i1,i2),tb3) left outer join (tb2 alias t2 use index (i1,i2,i3)) using(id1,id2)";
        lexer = new Lexer(sql);
        parser = getDMLParser(lexer);
        trs = parser.tables();
        output = output(trs, sql);
        Assert.assertEquals("(TB1 FORCE INDEX FOR JOIN (i1, i2), TB3) LEFT JOIN (TB2 AS T2 USE INDEX (i1, i2, i3)) USING (id1, id2)", output);

        sql = "tb1 alias t1 cross join tb2 alias t2 use index(i1)using(id1)";
        lexer = new Lexer(sql);
        parser = getDMLParser(lexer);
        trs = parser.tables();
        output = output(trs, sql);
        Assert.assertEquals("TB1 AS T1 INNER JOIN TB2 AS T2 USE INDEX (i1) USING (id1)", output);

        sql = "(tb1 alias t1) cross join tb2 alias t2 use index(i1)using(id1)";
        lexer = new Lexer(sql);
        parser = getDMLParser(lexer);
        trs = parser.tables();
        output = output(trs, sql);
        Assert.assertEquals("(TB1 AS T1) INNER JOIN TB2 AS T2 USE INDEX (i1) USING (id1)", output);

        sql = "tb1 alias _latin't1' cross join tb2 alias t2 use index(i1)";
        lexer = new Lexer(sql);
        parser = getDMLParser(lexer);
        trs = parser.tables();
        output = output(trs, sql);
        Assert.assertEquals("TB1 AS _LATIN't1' INNER JOIN TB2 AS T2 USE INDEX (i1)", output);

        sql = "((query '  @  from' from `from`)) alias t1 cross join tb2 alias t2 use index()";
        lexer = new Lexer(sql);
        parser = getDMLParser(lexer);
        trs = parser.tables();
        output = output(trs, sql);
        list = trs.getTableList();
        Assert.assertEquals(1, list.size());
        Assert.assertEquals(InnerJoin.class, list.get(0).getClass());
        SubQuery lsf = (SubQuery) ((InnerJoin) list.get(0)).getLeftTable();
        Assert.assertEquals("T1", lsf.getAlias());
        Assert.assertEquals(SelectStatement.class, lsf.getQuery().getClass());
        rtf = (Table) ((InnerJoin) list.get(0)).getRightTable();
        Assert.assertEquals("T2", rtf.getAlias());

        Assert.assertEquals("tb2", rtf.getTableName().getText());
        Assert.assertEquals("(SELECT '  @  from' FROM `FROM`) AS T1 INNER JOIN TB2 AS T2 USE INDEX ()", output);

        sql = "(tb1 alias t1) straight_join (tb2 alias t2)";
        lexer = new Lexer(sql);
        parser = getDMLParser(lexer);
        trs = parser.tables();
        output = output(trs, sql);
        Assert.assertEquals("(TB1 AS T1) STRAIGHT_JOIN (TB2 AS T2)", output);

        sql = "tb1 straight_join tb2 alias t2 on tb1.id=tb2.id";
        lexer = new Lexer(sql);
        parser = getDMLParser(lexer);
        trs = parser.tables();
        output = output(trs, sql);
        Assert.assertEquals("TB1 STRAIGHT_JOIN TB2 AS T2 ON TB1.ID = TB2.ID", output);

        sql = "tb1 left outer join tb2 on tb1.id=tb2.id";
        lexer = new Lexer(sql);
        parser = getDMLParser(lexer);
        trs = parser.tables();
        output = output(trs, sql);
        Assert.assertEquals("TB1 LEFT JOIN TB2 ON TB1.ID = TB2.ID", output);

        sql = "tb1 left outer join tb2 using(id)";
        lexer = new Lexer(sql);
        parser = getDMLParser(lexer);
        trs = parser.tables();
        output = output(trs, sql);
        Assert.assertEquals("TB1 LEFT JOIN TB2 USING (id)", output);

        sql = "(tb1 right outer join tb2 using()) join tb3 on tb1.id=tb2.id and tb2.id=tb3.id";
        lexer = new Lexer(sql);
        parser = getDMLParser(lexer);
        trs = parser.tables();
        output = output(trs, sql);
        list = trs.getTableList();
        Assert.assertEquals(1, list.size());
        Assert.assertEquals(InnerJoin.class, list.get(0).getClass());
        ltr = (Tables) ((InnerJoin) list.get(0)).getLeftTable();
        Assert.assertEquals(1, ltr.getTableList().size());
        Table lltrf = (Table) ((OuterJoin) ltr.getTableList().get(0)).getLeftTable();
        Assert.assertEquals(null, lltrf.getAlias());
        Assert.assertEquals("tb1", lltrf.getTableName().getText());

        rtf = (Table) ((InnerJoin) list.get(0)).getRightTable();
        Assert.assertEquals(null, rtf.getAlias());

        Assert.assertEquals("tb3", rtf.getTableName().getText());
        Assert.assertEquals(And.class, ((InnerJoin) list.get(0)).getOn().getClass());
        Assert.assertEquals("(TB1 RIGHT JOIN TB2 USING ()) INNER JOIN TB3 ON TB1.ID = TB2.ID AND TB2.ID = TB3.ID", output);

        sql = "tb1 right outer join tb2 using(id1,id2) " + "join (tb3,tb4) on tb1.id=tb2.id and tb2.id=tb3.id";
        lexer = new Lexer(sql);
        parser = getDMLParser(lexer);
        trs = parser.tables();
        output = output(trs, sql);
        list = trs.getTableList();
        Assert.assertEquals(1, list.size());
        Assert.assertEquals(InnerJoin.class, list.get(0).getClass());
        OuterJoin loj = (OuterJoin) ((InnerJoin) list.get(0)).getLeftTable();
        lltrf = (Table) loj.getLeftTable();
        Assert.assertEquals(null, lltrf.getAlias());
        Assert.assertEquals("tb1", lltrf.getTableName().getText());

        rtr = (Tables) ((InnerJoin) list.get(0)).getRightTable();
        Assert.assertEquals(2, rtr.getTableList().size());
        Assert.assertEquals("tb3", ((Table) (rtr.getTableList().get(0))).getTableName().getText());
        Assert.assertEquals("tb4", ((Table) (rtr.getTableList().get(1))).getTableName().getText());
        Assert.assertEquals(And.class, ((InnerJoin) list.get(0)).getOn().getClass());
        Assert.assertEquals("TB1 RIGHT JOIN TB2 USING (id1, id2) INNER JOIN (TB3, TB4) ON TB1.ID = TB2.ID AND TB2.ID = TB3.ID", output);

        sql = "tb1 left outer join tb2 join tb3 using(id)";
        lexer = new Lexer(sql);
        parser = getDMLParser(lexer);
        trs = parser.tables();
        output = output(trs, sql);
        Assert.assertEquals("TB1 LEFT JOIN (TB2 INNER JOIN TB3) USING (id)", output);

        sql = "tb1 right join tb2 on tb1.id=tb2.id";
        lexer = new Lexer(sql);
        parser = getDMLParser(lexer);
        trs = parser.tables();
        output = output(trs, sql);
        Assert.assertEquals("TB1 RIGHT JOIN TB2 ON TB1.ID = TB2.ID", output);

        sql = "tb1 natural right join tb2 ";
        lexer = new Lexer(sql);
        parser = getDMLParser(lexer);
        trs = parser.tables();
        output = output(trs, sql);
        Assert.assertEquals("TB1 NATURAL RIGHT JOIN TB2", output);


        sql = "tb1 natural left outer join tb2 ";
        lexer = new Lexer(sql);
        parser = getDMLParser(lexer);
        trs = parser.tables();
        output = output(trs, sql);
        Assert.assertEquals("TB1 NATURAL LEFT JOIN TB2", output);

        sql = "(tb1  t1) natural  join (tb2 alias t2) ";
        lexer = new Lexer(sql);
        parser = getDMLParser(lexer);
        trs = parser.tables();
        output = output(trs, sql);
        Assert.assertEquals("(TB1 AS T1) NATURAL JOIN (TB2 AS T2)", output);

        sql = "(query (query * from tb1) from `query` " + "where `any`=any(query id2 from tb2))any  ";
        lexer = new Lexer(sql);
        parser = getDMLParser(lexer);
        trs = parser.tables();
        output = output(trs, sql);
        list = trs.getTableList();
        Assert.assertEquals(SubQuery.class, list.get(0).getClass());
        Assert.assertEquals("ANY", ((SubQuery) list.get(0)).getAlias());
        Assert.assertEquals("(SELECT SELECT * FROM TB1 FROM `SELECT` WHERE `ANY` = ANY (SELECT ID2 FROM TB2)) AS ANY", output);

        sql = "((tb1),(tb3 alias t3,`query`),tb2 use key for join (i1,i2))" + " left join tb4 join tb5 using ()";
        lexer = new Lexer(sql);
        parser = getDMLParser(lexer);
        trs = parser.tables();
        output = output(trs, sql);
        list = trs.getTableList();
        Assert.assertEquals(1, list.size());
        Assert.assertEquals(OuterJoin.class, list.get(0).getClass());
        Assert.assertEquals(Tables.class, ((OuterJoin) list.get(0)).getLeftTable().getClass());
        Assert.assertEquals(InnerJoin.class, ((OuterJoin) list.get(0)).getRightTable().getClass());
        list = ((Tables) ((OuterJoin) list.get(0)).getLeftTable()).getTableList();
        list = ((Tables) list.get(1)).getTableList();
        Assert.assertEquals(2, list.size());
        Assert.assertEquals("(TB1, TB3 AS T3, `SELECT`, TB2 USE KEY FOR JOIN (i1, i2)) LEFT JOIN (TB4 INNER JOIN TB5) USING ()", output);

        sql = "((query `*` from `from` ) tb1),(tb3 alias t3,`query`),tb2 use key for join (i1,i2) " + "left join tb4 using (i1,i2)straight_join tb5";
        lexer = new Lexer(sql);
        parser = getDMLParser(lexer);
        trs = parser.tables();
        output = output(trs, sql);
        list = trs.getTableList();
        Assert.assertEquals(3, list.size());
        Assert.assertEquals(Tables.class, list.get(0).getClass());
        Assert.assertEquals(Tables.class, list.get(1).getClass());

        list = ((Tables) list.get(0)).getTableList();
        Assert.assertEquals(SubQuery.class, list.get(0).getClass());
        list = trs.getTableList();
        list = ((Tables) list.get(1)).getTableList();
        Assert.assertEquals(Table.class, list.get(0).getClass());
        Assert.assertEquals(Table.class, list.get(1).getClass());
        list = trs.getTableList();


        Assert.assertEquals("(SELECT `SELECT` FROM `FROM`) AS TB1, TB3 AS T3, `SELECT`, TB2 USE KEY FOR JOIN (i1, i2) LEFT JOIN TB4 USING (i1, i2) STRAIGHT_JOIN TB5", output);

        sql = "(`query`,(tb1 alias t1 use index for join()ignore key for group by (i1)))" + "join tb2 on cd1=any " + "right join " + "tb3 straight_join " + "(tb4 use index() left outer join (tb6,tb7) on id3=all(query `all` from `all`)) " + " on id2=any(query * from any) using  (i1)";
        lexer = new Lexer(sql);
        parser = getDMLParser(lexer);
        trs = parser.tables();
        output = output(trs, sql);
        list = trs.getTableList();
        Assert.assertEquals(1, list.size());
        Assert.assertEquals(OuterJoin.class, list.get(0).getClass());

        Assert.assertEquals(InnerJoin.class, ((OuterJoin) (list.get(0))).getLeftTable().getClass());

        Assert.assertEquals("(`SELECT`, TB1 AS T1 USE INDEX FOR JOIN () IGNORE KEY FOR GROUP BY (i1)) INNER JOIN TB2 ON CD1 = ANY RIGHT JOIN (TB3 STRAIGHT_JOIN (TB4 USE INDEX () LEFT JOIN (TB6, TB7) ON ID3 = ALL (SELECT `ALL` FROM `ALL`)) ON ID2 = ANY (SELECT * FROM ANY)) USING (i1)", output);
    }

}
