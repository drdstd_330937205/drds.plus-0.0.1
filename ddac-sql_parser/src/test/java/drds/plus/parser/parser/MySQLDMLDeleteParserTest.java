package drds.plus.parser.parser;

import drds.plus.parser.abstract_syntax_tree.statement.DeleteStatement;
import drds.plus.parser.lexer.Lexer;
import drds.plus.parser.lexer.Token;
import drds.plus.parser.parser.dml.DeleteParser;


public class MySQLDMLDeleteParserTest extends AbstractSyntaxTest {

    public void testDelete1() {
        String sql = "delete  from t1 as a where a.col1 = ? ";
        Lexer lexer = new Lexer(sql);
        DeleteParser deleteParser = new DeleteParser(lexer, new ExpressionParser(lexer));
        DeleteStatement deleteStatement = deleteParser.delete();
        deleteParser.match(Token.end_of_sql);
        String output = output(deleteStatement, sql);
        System.out.println(output);


        sql = "delete from id1 where col1 = 'adf'";
        lexer = new Lexer(sql);
        deleteParser = new DeleteParser(lexer, new ExpressionParser(lexer));
        deleteStatement = deleteParser.delete();
        deleteParser.match(Token.end_of_sql);
        output = output(deleteStatement, sql);
        System.out.println(output);


        sql = "delete from t1 where t1.id1 = 'abc'";
        lexer = new Lexer(sql);
        deleteParser = new DeleteParser(lexer, new ExpressionParser(lexer));
        deleteStatement = deleteParser.delete();
        deleteParser.match(Token.end_of_sql);
        output = output(deleteStatement, sql);
        System.out.println(output);

        sql = "delete from t1";
        lexer = new Lexer(sql);
        deleteParser = new DeleteParser(lexer, new ExpressionParser(lexer));
        deleteStatement = deleteParser.delete();
        deleteParser.match(Token.end_of_sql);
        output = output(deleteStatement, sql);
        System.out.println(output);


    }
}
