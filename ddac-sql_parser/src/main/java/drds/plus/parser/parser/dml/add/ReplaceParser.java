package drds.plus.parser.parser.dml.add;

import drds.plus.parser.abstract_syntax_tree.expression.Expression;
import drds.plus.parser.abstract_syntax_tree.expression.Pair;
import drds.plus.parser.abstract_syntax_tree.expression.primary.misc.Identifier;
import drds.plus.parser.abstract_syntax_tree.expression.primary.misc.RowValues;
import drds.plus.parser.abstract_syntax_tree.statement.Query;
import drds.plus.parser.abstract_syntax_tree.statement.ReplaceStatement;
import drds.plus.parser.lexer.Lexer;
import drds.plus.parser.lexer.Token;
import drds.plus.parser.parser.ExpressionParser;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static drds.plus.parser.lexer.Token.KW_REPLACE;


public class ReplaceParser extends InsertReplaceParser {

    public ReplaceParser(Lexer lexer, ExpressionParser expressionParser) {
        super(lexer, expressionParser);
    }


    public ReplaceStatement replace() {
        match(KW_REPLACE);
        lexer.nextToken();
        match(Token.KW_INTO);
        lexer.nextToken();
        match(Token.IDENTIFIER);
        Identifier tableName = identifier();
        //
        List<Identifier> columnNameList;
        List<Expression> expressionList;//set
        List<RowValues> rowValuesList;
        Query query;
        //
        List<Pair<Identifier, Expression>> duplicateUpdate;
        if (lexer.token() == Token.KW_SET) {
            lexer.nextToken();
            columnNameList = new LinkedList<Identifier>();
            expressionList = new LinkedList<Expression>();
            do {
                Identifier identifier = identifier();//第一个不是,
                match(Token.OP_EQUALS);
                lexer.nextToken();
                Expression expression = expressionParser.expression();
                //
                columnNameList.add(identifier);
                expressionList.add(expression);
            } while (lexer.token() == Token.PUNC_COMMA && lexer.nextToken() != Token.end_of_sql);//判断不是,的同时获取下一个token
            rowValuesList = new ArrayList<RowValues>(1);
            rowValuesList.add(new RowValues(expressionList));
            return new ReplaceStatement(tableName, columnNameList, rowValuesList);
        } else if (lexer.token() == Token.PUNC_LEFT_PAREN) {
            lexer.nextToken();
            columnNameList = buildIdentifierList();
            match(Token.PUNC_RIGHT_PAREN);
            lexer.nextToken();
            if (lexer.token() == Token.KW_VALUES) {
                lexer.nextToken();
                match(Token.PUNC_LEFT_PAREN);
                //lexer.nextToken();
                rowValuesList = rowDataList();
                return new ReplaceStatement(tableName, columnNameList, rowValuesList);
            } else if (lexer.token() == Token.PUNC_LEFT_PAREN) {
                lexer.nextToken();
                query = selectStatement();//or trySelectStatement
                match(Token.PUNC_RIGHT_PAREN);
                lexer.nextToken();
                return new ReplaceStatement(tableName, columnNameList, query);
            } else {
                throw error("不支持其他insert模式");
            }
        } else {
            throw error("unexpected token for insert: " + lexer.token());
        }
    }

}
