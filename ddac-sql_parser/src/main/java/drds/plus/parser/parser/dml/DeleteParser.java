package drds.plus.parser.parser.dml;

import drds.plus.parser.abstract_syntax_tree.expression.Expression;
import drds.plus.parser.abstract_syntax_tree.statement.DeleteStatement;
import drds.plus.parser.abstract_syntax_tree.statement.select.table.Table;
import drds.plus.parser.lexer.Lexer;
import drds.plus.parser.lexer.Token;
import drds.plus.parser.parser.ExpressionParser;

public class DeleteParser extends DmlParser {

    public DeleteParser(Lexer lexer, ExpressionParser expressionParser) {
        super(lexer, expressionParser);
    }


    public DeleteStatement delete() {
        match(Token.KW_DELETE);
        lexer.nextToken();
        match(Token.KW_FROM);
        lexer.nextToken();
        Table table = (Table) table();
        //lexer.nextToken();
        if (lexer.token() == Token.KW_WHERE) {
            lexer.nextToken();
            Expression where = expressionParser.expression();
            return new DeleteStatement(table, where);
        } else {
            return new DeleteStatement(table, null);
        }

    }

}
