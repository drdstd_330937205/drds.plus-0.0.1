package drds.plus.parser.abstract_syntax_tree.expression.primary.function.aggregation;

import drds.plus.parser.abstract_syntax_tree.expression.Expression;
import drds.plus.parser.abstract_syntax_tree.expression.primary.function.Function;
import drds.plus.parser.visitor.Visitor;

import java.util.List;


public class Count extends Function {

    /**
     * either {@link distinct} or {@link wildcard} is false. if both are false,
     * expressionList must be size 1
     */
    private final boolean distinct;

    public Count(List<Expression> expressionList) {
        super("COUNT", expressionList);
        this.distinct = true;
    }

    public Count(Expression expression) {
        super("COUNT", wrapList(expression));
        this.distinct = false;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public Function constructFunction(List<Expression> arguments) {
        return new Count(arguments);
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
