package drds.plus.parser.abstract_syntax_tree.expression.comparison.fuzzy_matching;

import drds.plus.parser.abstract_syntax_tree.expression.BinaryExpression;
import drds.plus.parser.abstract_syntax_tree.expression.Expression;
import drds.plus.parser.visitor.Visitor;
import lombok.Getter;
import lombok.Setter;


public class Regexp extends BinaryExpression {
    @Setter
    @Getter
    private final boolean not;

    public Regexp(boolean not, Expression comparee, Expression pattern) {
        super(comparee, pattern, precedence_comparision);
        this.not = not;
    }

    public String getOperator() {
        return not ? "NOT REGEXP" : "REGEXP";
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
