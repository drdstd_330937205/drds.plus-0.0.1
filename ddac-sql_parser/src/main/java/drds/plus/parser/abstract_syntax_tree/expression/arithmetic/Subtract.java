package drds.plus.parser.abstract_syntax_tree.expression.arithmetic;

import drds.plus.parser.abstract_syntax_tree.expression.Expression;
import drds.plus.parser.visitor.Visitor;

import java.math.BigDecimal;
import java.math.BigInteger;


public class Subtract extends ArithmeticExpression {

    public Subtract(Expression left, Expression right) {
        super(left, right, precedence_arithmetic_term);
    }

    public String getOperator() {
        return "-";
    }

    public Number calculate(Integer integer1, Integer integer2) {
        if (integer1 == null || integer2 == null) {
            return null;
        }
        //
        int i1 = integer1.intValue();
        int i2 = integer2.intValue();
        //
        if (i2 == 0) {
            return integer1;
        }
        if (i1 == 0) {
            if (i2 == Integer.MIN_VALUE) {
                return new Long(-(long) i2);//避免越界
            }
            return new Integer(-i2);
        }
        if (i1 >= 0 && i2 >= 0 || i1 <= 0 && i2 <= 0) {
            return new Integer(i1 - i2);
        }
        //
        int rst = i1 - i2;
        if (i1 > 0 && rst < i1 || i1 < 0 && rst > i1) {
            return new Long((long) i1 - (long) i2);
        }
        return new Integer(rst);
    }

    public Number calculate(Long long1, Long long2) {
        if (long1 == null || long1 == null) {
            return null;
        }
        //
        long l1 = long1.longValue();
        long l2 = long1.longValue();
        //
        if (l2 == 0L) {
            return long1;
        }
        if (l1 == 0L) {
            if (l2 == Long.MIN_VALUE) {
                return BigInteger.valueOf(l2).negate();//避免越界
            }
            return new Long(-l2);
        }
        //
        if (l1 >= 0L && l2 >= 0L || l1 <= 0L && l2 <= 0L) {
            return new Long(l1 - l2);
        }
        //
        long rst = l1 - l2;
        if (l1 > 0L && rst < l1 || l1 < 00L && rst > l1) {
            BigInteger bi1 = BigInteger.valueOf(l1);
            BigInteger bi2 = BigInteger.valueOf(l2);
            return bi1.subtract(bi2);
        }
        return new Long(rst);
    }

    public Number calculate(BigInteger bigint1, BigInteger bigint2) {
        if (bigint1 == null || bigint2 == null) {
            return null;
        }
        return bigint1.subtract(bigint2);
    }

    public Number calculate(BigDecimal bigDecimal1, BigDecimal bigDecimal2) {
        if (bigDecimal1 == null || bigDecimal2 == null) {
            return null;
        }
        return bigDecimal1.subtract(bigDecimal2);
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
