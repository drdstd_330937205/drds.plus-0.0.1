package drds.plus.parser.abstract_syntax_tree.statement;

import drds.plus.parser.abstract_syntax_tree.expression.primary.misc.Identifier;
import drds.plus.parser.visitor.Visitor;


public class TruncateStatement implements Statement {

    private final Identifier tableName;

    public TruncateStatement(Identifier tableName) {
        this.tableName = tableName;
    }

    public Identifier getTableName() {
        return tableName;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

}
