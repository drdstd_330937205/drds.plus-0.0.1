package drds.plus.parser.abstract_syntax_tree.expression;

import java.util.Map;

public abstract class AbstractExpression implements Expression {

    private boolean cacheEvalValue = true;
    private boolean evaled;
    private Object evaledValue;

    public Expression setCacheEvalValue(boolean cacheEvalValue) {
        this.cacheEvalValue = cacheEvalValue;
        return this;
    }

    public final Object eval(Map<? extends Object, ? extends Object> map) {
        if (cacheEvalValue) {
            if (evaled) {
                return evaledValue;
            }
            evaledValue = evalInternal(map);
            evaled = true;
            return evaledValue;
        }
        return evalInternal(map);
    }

    protected abstract Object evalInternal(Map<? extends Object, ? extends Object> map);

}
