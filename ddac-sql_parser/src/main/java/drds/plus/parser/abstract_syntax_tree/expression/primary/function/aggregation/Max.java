package drds.plus.parser.abstract_syntax_tree.expression.primary.function.aggregation;

import drds.plus.parser.abstract_syntax_tree.expression.Expression;
import drds.plus.parser.abstract_syntax_tree.expression.primary.function.Function;
import drds.plus.parser.visitor.Visitor;

import java.util.List;


public class Max extends Function {

    private final boolean distinct;

    public Max(Expression expression, boolean distinct) {
        super("MAX", wrapList(expression));
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public Function constructFunction(List<Expression> expressionList) {
        throw new UnsupportedOperationException("function of char has special argList");
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
