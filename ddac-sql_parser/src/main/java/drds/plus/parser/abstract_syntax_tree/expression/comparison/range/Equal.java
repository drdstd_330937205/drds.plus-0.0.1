package drds.plus.parser.abstract_syntax_tree.expression.comparison.range;

import drds.plus.parser.abstract_syntax_tree.expression.BinaryExpression;
import drds.plus.parser.abstract_syntax_tree.expression.Evals;
import drds.plus.parser.abstract_syntax_tree.expression.Expression;
import drds.plus.parser.abstract_syntax_tree.expression.Pair;
import drds.plus.parser.abstract_syntax_tree.expression.primary.literal.LiteralBoolean;
import drds.plus.parser.visitor.Visitor;

import java.util.Map;

public class Equal extends BinaryExpression implements Expression {


    public Equal(Expression left, Expression right) {
        super(left, right, precedence_comparision);
    }

    public String getOperator() {
        return "=";
    }

    public Object evalInternal(Map<? extends Object, ? extends Object> map) {
        Object left = this.left.eval(map);
        Object right = this.right.eval(map);
        if (left == null || right == null) {
            return null;
        }
        if (left == UNEVALUATABLE || right == UNEVALUATABLE) {
            return UNEVALUATABLE;
        }
        if (left instanceof Number || right instanceof Number) {
            Pair<Number, Number> pair = Evals.convertNum2SameLevel(left, right);
            left = pair.getKey();
            right = pair.getValue();
        }
        return left.equals(right) ? LiteralBoolean.TRUE : LiteralBoolean.FALSE;
    }


    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
