package drds.plus.parser.abstract_syntax_tree.expression.primary.literal;

import drds.plus.parser.visitor.Visitor;

import java.util.Map;


public class LiteralBoolean extends Literal {

    public static final Integer TRUE = new Integer(1);
    public static final Integer FALSE = new Integer(0);
    private final boolean value;

    public LiteralBoolean(boolean value) {
        super();
        this.value = value;
    }

    public boolean isTrue() {
        return value;
    }

    public Object evalInternal(Map<? extends Object, ? extends Object> map) {
        return value ? TRUE : FALSE;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
