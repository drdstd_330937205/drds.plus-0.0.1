package drds.plus.parser.abstract_syntax_tree.statement.select.table;

import drds.plus.parser.abstract_syntax_tree.statement.Query;
import drds.plus.parser.visitor.Visitor;
import lombok.NonNull;


public class SubQuery extends TableAlias {

    private final Query query;

    public SubQuery(@NonNull Query query, @NonNull String alias) {
        super(alias);
        this.query = query;
    }

    public Query getQuery() {
        return query;
    }


    public boolean isSingleTable() {
        return false;
    }

    public int getPriority() {
        return precedence_factor;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
