package drds.plus.parser.abstract_syntax_tree.expression.primary.function;

import drds.plus.parser.abstract_syntax_tree.expression.Expression;
import drds.plus.parser.abstract_syntax_tree.expression.PrimaryExpression;
import drds.plus.parser.visitor.Visitor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class Function extends PrimaryExpression {

    protected final String functionName;
    protected final List<Expression> argList;

    public Function(String functionName, List<Expression> argList) {
        super();
        this.functionName = functionName;
        if (argList == null || argList.isEmpty()) {
            this.argList = Collections.emptyList();
        } else {
            if (argList instanceof ArrayList) {
                this.argList = argList;
            } else {
                this.argList = new ArrayList<Expression>(argList);
            }
        }
    }

    protected static List<Expression> wrapList(Expression expression) {
        List<Expression> list = new ArrayList<Expression>(1);
        list.add(expression);
        return list;
    }

    /**
     * <code>this</code> function object being called is a prototype
     */
    public abstract Function constructFunction(List<Expression> arguments);

    public void init() {
    }

    /**
     * @return never null
     */
    public List<Expression> getArgList() {
        return argList;
    }

    public String getFunctionName() {
        return functionName;
    }

    public Expression setCacheEvalValue(boolean cacheEvalValue) {
        return super.setCacheEvalValue(cacheEvalValue);
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
