package drds.plus.parser.abstract_syntax_tree.expression.logical;

import drds.plus.parser.abstract_syntax_tree.expression.Evals;
import drds.plus.parser.abstract_syntax_tree.expression.Expression;
import drds.plus.parser.abstract_syntax_tree.expression.primary.literal.LiteralBoolean;
import drds.plus.parser.visitor.Visitor;

import java.util.Map;

public class And extends ListExpression {

    public And() {
        super(precedence_logical_and);
    }

    public String getOperator() {
        return "AND";
    }

    public Object evalInternal(Map<? extends Object, ? extends Object> map) {
        for (Expression expression : expressionList) {
            Object value = expression.eval(map);
            if (value == null) {
                return null;
            }
            if (value == UNEVALUATABLE) {
                return UNEVALUATABLE;
            }
            if (!Evals.obj2bool(value)) {
                return LiteralBoolean.FALSE;
            }
        }
        return LiteralBoolean.TRUE;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

}
