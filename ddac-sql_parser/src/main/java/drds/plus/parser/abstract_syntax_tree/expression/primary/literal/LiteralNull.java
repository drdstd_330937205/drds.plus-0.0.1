package drds.plus.parser.abstract_syntax_tree.expression.primary.literal;

import drds.plus.parser.visitor.Visitor;

import java.util.Map;


public class LiteralNull extends Literal {

    public Object evalInternal(Map<? extends Object, ? extends Object> map) {
        return null;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
