package drds.plus.parser.abstract_syntax_tree.expression.arithmetic;

import drds.plus.parser.abstract_syntax_tree.expression.BinaryExpression;
import drds.plus.parser.abstract_syntax_tree.expression.Evals;
import drds.plus.parser.abstract_syntax_tree.expression.Expression;
import drds.plus.parser.abstract_syntax_tree.expression.Pair;

import java.util.Map;

public abstract class ArithmeticExpression extends BinaryExpression implements ArithmeticCalculator {

    protected ArithmeticExpression(Expression left, Expression right, int precedence) {
        super(left, right, precedence, true);
    }

    public Object evalInternal(Map<? extends Object, ? extends Object> map) {
        Object left = this.left.eval(map);
        Object right = this.right.eval(map);
        if (left == null || right == null)
            return null;
        if (left == UNEVALUATABLE || right == UNEVALUATABLE)
            return UNEVALUATABLE;
        Pair<Number, Number> pair = Evals.convertNum2SameLevel(left, right);
        return Evals.calculate(this, pair.getKey(), pair.getValue());
    }

}
