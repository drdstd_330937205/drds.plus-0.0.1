package drds.plus.parser.abstract_syntax_tree.expression.primary.subquery;

import drds.plus.parser.abstract_syntax_tree.expression.UnaryExpression;
import drds.plus.parser.abstract_syntax_tree.statement.Query;

/**
 * 'ALL' '(' subquery ')'
 */
public class AllQuery extends UnaryExpression {

    public AllQuery(Query query) {
        super(query, precedence_primary);
    }

    public String getOperator() {
        return "ALL";
    }

}
