package drds.plus.parser.abstract_syntax_tree.expression.primary.subquery;

import drds.plus.parser.abstract_syntax_tree.expression.UnaryExpression;
import drds.plus.parser.abstract_syntax_tree.statement.Query;

/**
 * ('ANY'|'SOME') '(' subquery ')'
 */
public class AnyQuery extends UnaryExpression {

    public AnyQuery(Query query) {
        super(query, precedence_primary);
    }

    public String getOperator() {
        return "ANY";
    }

}
