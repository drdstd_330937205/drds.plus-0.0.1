package drds.plus.parser.abstract_syntax_tree.expression.primary.function;

public enum FunctionType {
    /**
     * not a function
     */
    DEFAULT,
    /**
     * ordinary function
     */
    ORDINARY, AVG, COUNT, MAX, MIN, SUM, ROW
}
