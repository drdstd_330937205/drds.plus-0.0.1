package drds.plus.parser.abstract_syntax_tree.expression.comparison.range;

import drds.plus.parser.abstract_syntax_tree.expression.AbstractExpression;
import drds.plus.parser.abstract_syntax_tree.expression.Expression;
import drds.plus.parser.visitor.Visitor;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

public class BetweenAnd extends AbstractExpression implements Expression {
    @Setter
    @Getter
    private final Expression comparee;
    @Setter
    @Getter
    private final Expression notLessThan;
    @Setter
    @Getter
    private final Expression notGreaterThan;
    @Setter
    @Getter
    private final boolean not;


    public BetweenAnd(boolean not, Expression comparee, Expression notLessThan, Expression notGreaterThan) {
        this.not = not;
        this.comparee = comparee;
        this.notLessThan = notLessThan;
        this.notGreaterThan = notGreaterThan;

    }

    public int getPriority() {
        return precedence_between_and;
    }


    public Object evalInternal(Map<? extends Object, ? extends Object> map) {
        return UNEVALUATABLE;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
