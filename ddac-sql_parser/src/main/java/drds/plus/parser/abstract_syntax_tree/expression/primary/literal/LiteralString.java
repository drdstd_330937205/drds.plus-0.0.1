package drds.plus.parser.abstract_syntax_tree.expression.primary.literal;

import drds.plus.parser.visitor.Visitor;
import lombok.NonNull;

import java.util.Map;

/**
 * literal date is also possible
 */
public class LiteralString extends Literal {


    private final String string;


    /**
     * @param string content of fuzzy_matching, excluded of head and tail "'". e.g. for
     *               fuzzy_matching token of "'don\\'t'", argument of fuzzy_matching is "don\\'t"
     */
    public LiteralString(@NonNull String string) {
        super();
        this.string = string;

    }


    public String getString() {
        return string;
    }


    public Object evalInternal(Map<? extends Object, ? extends Object> map) {
        return string;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
