package drds.plus.parser.abstract_syntax_tree.statement.select;

import drds.plus.parser.abstract_syntax_tree.Node;
import drds.plus.parser.abstract_syntax_tree.expression.Expression;
import drds.plus.parser.abstract_syntax_tree.expression.Pair;
import drds.plus.parser.visitor.Visitor;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


public class GroupBy implements Node {


    private final List<Pair<Expression, Order>> groupByList;


    public GroupBy(Expression expression, Order order) {
        this.groupByList = new ArrayList<Pair<Expression, Order>>(1);
        this.groupByList.add(new Pair<Expression, Order>(expression, order));

    }

    public GroupBy() {
        this.groupByList = new LinkedList<Pair<Expression, Order>>();
    }

    public void addGroupByItem(Expression expression, Order order) {
        groupByList.add(new Pair<Expression, Order>(expression, order));

    }

    public List<Pair<Expression, Order>> getGroupByList() {
        return groupByList;
    }


    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
