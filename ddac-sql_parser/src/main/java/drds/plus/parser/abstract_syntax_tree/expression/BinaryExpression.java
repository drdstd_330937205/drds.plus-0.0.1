package drds.plus.parser.abstract_syntax_tree.expression;

import java.util.Map;


public abstract class BinaryExpression extends AbstractExpression {

    protected final Expression left;
    protected final Expression right;
    protected final int precedence;
    protected final boolean leftCombine;

    /**
     * {@link #leftCombine} is true
     */
    protected BinaryExpression(Expression left, Expression right, int precedence) {
        this.left = left;
        this.right = right;
        this.precedence = precedence;
        this.leftCombine = true;
    }

    protected BinaryExpression(Expression left, Expression right, int precedence, boolean leftCombine) {
        this.left = left;
        this.right = right;
        this.precedence = precedence;
        this.leftCombine = leftCombine;
    }

    public Expression getLeft() {
        return left;
    }

    public Expression getRight() {
        return right;
    }

    public int getPriority() {
        return precedence;
    }

    public boolean isLeftCombine() {
        return leftCombine;
    }

    public abstract String getOperator();

    public Object evalInternal(Map<? extends Object, ? extends Object> map) {
        return UNEVALUATABLE;
    }

}
