package drds.plus.parser.abstract_syntax_tree.expression;

import java.util.Map;

public abstract class PrimaryExpression extends AbstractExpression {

    public int getPriority() {
        return precedence_primary;
    }

    public Object evalInternal(Map<? extends Object, ? extends Object> map) {
        return UNEVALUATABLE;
    }
}
