package drds.plus.parser.abstract_syntax_tree.expression.primary.function.datetime;

import drds.plus.parser.abstract_syntax_tree.expression.Expression;
import drds.plus.parser.abstract_syntax_tree.expression.primary.function.Function;

import java.util.List;

public class CurrentDate extends Function {

    public CurrentDate() {
        super("CURDATE", null);
    }

    public Function constructFunction(List<Expression> arguments) {
        return new CurrentDate();
    }

}
