package drds.plus.parser.abstract_syntax_tree.expression;

import drds.plus.parser.abstract_syntax_tree.Node;

import java.util.Map;


public interface Expression extends Node {
    int precedence_query = 0;
    int precedence_logical_or = 1;
    int precedence_logical_and = 2;
    int precedence_between_and = 3;
    int precedence_comparision = 4;
    //
    int precedence_arithmetic_term = 5;
    int precedence_arithmetic_factor = 6;
    //
    int precedence_primary = 7;
    Object UNEVALUATABLE = new Object();

    /**
     * 优先级
     */
    int getPriority();

    /**
     * @return this
     */
    Expression setCacheEvalValue(boolean cacheEvalRst);

    Object eval(Map<? extends Object, ? extends Object> parameters);
}
