package drds.plus.parser.abstract_syntax_tree.statement;

import drds.plus.parser.abstract_syntax_tree.expression.primary.misc.Identifier;
import drds.plus.parser.abstract_syntax_tree.expression.primary.misc.RowValues;

import java.util.List;

public abstract class InsertReplaceStatement extends DmlStatement {

    protected final Identifier tableName;
    protected final List<Identifier> columnNameList;
    protected final Query query;
    protected List<RowValues> rowValuesList;

    public InsertReplaceStatement(Identifier tableName, List<Identifier> columnNameList, List<RowValues> rowValuesList) {
        this.tableName = tableName;
        this.columnNameList = columnNameList;
        this.rowValuesList = ensureListType(rowValuesList);
        this.query = null;
    }

    public InsertReplaceStatement(Identifier tableName, List<Identifier> columnNameList, Query query) {
        this.tableName = tableName;
        this.columnNameList = columnNameList;
        this.rowValuesList = null;
        this.query = query;
    }

    public Identifier getTableName() {
        return tableName;
    }

    public List<Identifier> getColumnNameList() {
        return columnNameList;
    }

    public List<RowValues> getRowValuesList() {
        return rowValuesList;
    }

    public Query getQuery() {
        return query;
    }

}
