package drds.plus.parser.abstract_syntax_tree.statement.select.table;

import drds.plus.parser.abstract_syntax_tree.expression.primary.misc.Identifier;
import drds.plus.parser.visitor.Visitor;


public class Table extends TableAlias {


    private final Identifier tableName;


    public Table(Identifier tableName, String alias) {
        super(alias);
        this.tableName = tableName;

    }

    public Table(Identifier tableName) {
        this(tableName, null);
    }

    public Identifier getTableName() {
        return tableName;
    }


    public boolean isSingleTable() {
        return true;
    }

    public int getPriority() {
        return precedence_factor;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
