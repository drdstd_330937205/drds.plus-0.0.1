package drds.plus.parser.abstract_syntax_tree.expression.logical;

import drds.plus.parser.abstract_syntax_tree.expression.AbstractExpression;
import drds.plus.parser.abstract_syntax_tree.expression.Expression;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 多元函数
 */
public abstract class ListExpression extends AbstractExpression {

    protected final int precedence;
    protected List<Expression> expressionList;

    public ListExpression(int precedence) {
        this(precedence, 4);
    }

    public ListExpression(int precedence, int initArity) {
        this.precedence = precedence;
        this.expressionList = new ArrayList<Expression>(initArity);
    }

    public void append(Expression expression) {
        if (expression == null) {

        }
        if (getClass().isAssignableFrom(expression.getClass())) {
            ListExpression listExpression = (ListExpression) expression;
            expressionList.addAll(listExpression.expressionList);
        } else {
            expressionList.add(expression);
        }

    }

    /**
     * @param index start from 0
     */
    public Expression getExpression(int index) {
        if (index >= expressionList.size()) {
            throw new IllegalArgumentException("only contains " + expressionList.size() + " expressionList," + index + " is out of bound");
        }
        return expressionList.get(index);
    }

    public int size() {
        return expressionList.size();
    }

    public int getPriority() {
        return precedence;
    }

    public abstract String getOperator();

    protected Object evalInternal(Map<? extends Object, ? extends Object> map) {
        return UNEVALUATABLE;
    }
}
