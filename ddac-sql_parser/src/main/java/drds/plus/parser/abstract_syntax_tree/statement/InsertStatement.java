package drds.plus.parser.abstract_syntax_tree.statement;

import drds.plus.parser.abstract_syntax_tree.expression.Expression;
import drds.plus.parser.abstract_syntax_tree.expression.Pair;
import drds.plus.parser.abstract_syntax_tree.expression.primary.misc.Identifier;
import drds.plus.parser.abstract_syntax_tree.expression.primary.misc.RowValues;
import drds.plus.parser.visitor.Visitor;

import java.util.List;

public class InsertStatement extends InsertReplaceStatement {

    private final List<Pair<Identifier, Expression>> duplicateUpdate;

    /**
     * (insert ... values) format
     *
     * @param columnNameList can be null
     */
    public InsertStatement(Identifier tableName, List<Identifier> columnNameList, List<RowValues> rowValuesList, List<Pair<Identifier, Expression>> duplicateUpdate) {
        super(tableName, columnNameList, rowValuesList);

        this.duplicateUpdate = duplicateUpdate;
    }

    /**
     * insert ... selectStatement format
     *
     * @param columnNameList can be null
     */
    public InsertStatement(Identifier tableName, List<Identifier> columnNameList, Query select, List<Pair<Identifier, Expression>> duplicateUpdate) {
        super(tableName, columnNameList, select);
        this.duplicateUpdate = duplicateUpdate;
    }

    public List<Pair<Identifier, Expression>> getDuplicateUpdate() {
        return duplicateUpdate;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
