package drds.plus.parser.abstract_syntax_tree.statement;

import java.util.Map;

public abstract class QueryStatement extends DmlStatement implements Query {

    public int getPriority() {
        return precedence_query;
    }

    public QueryStatement setCacheEvalValue(boolean cacheEvalRst) {
        return this;
    }

    public Object eval(Map<? extends Object, ? extends Object> parameters) {
        return UNEVALUATABLE;
    }
}
