package drds.plus.parser.abstract_syntax_tree.expression.arithmetic;

import java.math.BigDecimal;
import java.math.BigInteger;


public interface ArithmeticCalculator {

    Number calculate(Integer integer1, Integer integer2);

    Number calculate(Long long1, Long long2);

    Number calculate(BigInteger bigint1, BigInteger bigint2);

    Number calculate(BigDecimal bigDecimal1, BigDecimal bigDecimal2);
}
