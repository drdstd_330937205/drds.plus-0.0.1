package drds.plus.parser.abstract_syntax_tree.expression.comparison.range;

import drds.plus.parser.abstract_syntax_tree.expression.BinaryExpression;
import drds.plus.parser.abstract_syntax_tree.expression.Expression;
import drds.plus.parser.abstract_syntax_tree.expression.primary.misc.InList;
import drds.plus.parser.visitor.Visitor;

/**
 * (NOT)? IN ( '(' expr (',' expr)* ')' | subquery
 */
public class In extends BinaryExpression implements Expression {

    private final boolean not;


    public In(boolean not, Expression left, Expression right) {
        super(left, right, precedence_comparision);
        this.not = not;
    }

    public boolean isNot() {
        return not;
    }

    public InList getInExpressionList() {
        if (right instanceof InList) {
            return (InList) right;
        }
        return null;
    }


    public String getOperator() {
        return not ? "NOT IN" : "IN";
    }


    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
