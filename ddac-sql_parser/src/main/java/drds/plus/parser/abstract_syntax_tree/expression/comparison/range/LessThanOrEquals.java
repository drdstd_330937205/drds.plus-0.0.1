package drds.plus.parser.abstract_syntax_tree.expression.comparison.range;

import drds.plus.parser.abstract_syntax_tree.expression.BinaryExpression;
import drds.plus.parser.abstract_syntax_tree.expression.Expression;
import drds.plus.parser.visitor.Visitor;

public class LessThanOrEquals extends BinaryExpression {

    public LessThanOrEquals(Expression leftOprand, Expression rightOprand) {
        super(leftOprand, rightOprand, precedence_comparision);
    }

    public String getOperator() {
        return "<=";
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
