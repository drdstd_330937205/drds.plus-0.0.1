package drds.plus.parser.abstract_syntax_tree.expression;

import drds.plus.parser.visitor.Visitor;
import lombok.NonNull;

import java.util.Map;


public abstract class UnaryExpression extends AbstractExpression {

    protected final int precedence;
    private final Expression expression;

    public UnaryExpression(@NonNull Expression expression, int precedence) {
        this.expression = expression;
        this.precedence = precedence;
    }

    public Expression getExpression() {
        return expression;
    }

    public int getPriority() {
        return precedence;
    }

    public abstract String getOperator();

    public Object evalInternal(Map<? extends Object, ? extends Object> map) {
        return UNEVALUATABLE;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

}
