package drds.plus.parser.abstract_syntax_tree.expression.primary.misc;

import drds.plus.parser.abstract_syntax_tree.expression.AbstractExpression;
import drds.plus.parser.abstract_syntax_tree.expression.Expression;
import drds.plus.parser.visitor.Visitor;

import java.util.List;
import java.util.Map;

public class InList extends AbstractExpression {

    private List<Expression> list;

    public InList(List<Expression> list) {
        this.list = list;
    }

    /**
     * @return never null
     */
    public List<Expression> getList() {
        return list;
    }

    public int getPriority() {
        return precedence_primary;
    }

    public Object evalInternal(Map<? extends Object, ? extends Object> map) {
        return UNEVALUATABLE;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
