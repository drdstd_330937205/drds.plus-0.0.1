package drds.plus.parser.abstract_syntax_tree.statement.select.table;

public abstract class TableAlias implements ITable {

    protected final String alias;

    public TableAlias(String alias) {
        this.alias = alias;
    }

    public String getAlias() {
        return alias;
    }
}
