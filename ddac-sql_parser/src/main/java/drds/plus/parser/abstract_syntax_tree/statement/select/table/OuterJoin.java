package drds.plus.parser.abstract_syntax_tree.statement.select.table;

import drds.plus.parser.abstract_syntax_tree.expression.Expression;
import drds.plus.parser.visitor.Visitor;


public class OuterJoin implements ITable {

    private final boolean isLeftJoin;
    private final ITable leftTable;
    private final ITable rightTable;
    private final Expression on;

    public OuterJoin(boolean isLeftJoin, ITable leftTable, ITable rightTable, Expression on) {
        super();
        this.isLeftJoin = isLeftJoin;
        this.leftTable = leftTable;
        this.rightTable = rightTable;
        this.on = on;

    }

    public boolean isLeftJoin() {
        return isLeftJoin;
    }

    public ITable getLeftTable() {
        return leftTable;
    }

    public ITable getRightTable() {
        return rightTable;
    }

    public Expression getOn() {
        return on;
    }


    public boolean isSingleTable() {
        return false;
    }

    public int getPriority() {
        return precedence_join;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

}
