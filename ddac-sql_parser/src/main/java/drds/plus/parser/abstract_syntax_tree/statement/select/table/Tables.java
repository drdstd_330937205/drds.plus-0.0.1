package drds.plus.parser.abstract_syntax_tree.statement.select.table;

import drds.plus.parser.visitor.Visitor;
import lombok.NonNull;

import java.util.List;


public class Tables implements ITable {

    private final List<ITable> tableList;

    public Tables(@NonNull List<ITable> tableList) {
        if (tableList == null || tableList.isEmpty()) {
            throw new RuntimeException("at least one tableName reference");
        }
        this.tableList = tableList;
    }


    /**
     * @return never null
     */
    public List<ITable> getTableList() {
        return tableList;
    }

    public boolean isSingleTable() {
        if (tableList == null) {
            return false;
        }
        int count = 0;
        ITable first = null;
        for (ITable table : tableList) {
            if (table != null && 1 == ++count) {
                first = table;
            }
        }
        return count == 1 && first.isSingleTable();
    }

    public int getPriority() {
        return ITable.precedence_refs;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

}
