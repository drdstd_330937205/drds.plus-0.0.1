package drds.plus.parser.abstract_syntax_tree.statement.select.table;

import drds.plus.parser.abstract_syntax_tree.Node;

public interface ITable extends Node {

    int precedence_refs = 0;
    int precedence_join = 1;
    int precedence_factor = 2;

    boolean isSingleTable();

    int getPriority();
}
