package drds.plus.parser.abstract_syntax_tree.expression.primary.literal;

import drds.plus.parser.visitor.Visitor;
import lombok.NonNull;

import java.util.Map;

/**
 * literal date is also possible
 */
public class LiteralNumber extends Literal {

    private final Number number;

    public LiteralNumber(@NonNull Number number) {
        super();
        this.number = number;
    }

    public Object evalInternal(Map<? extends Object, ? extends Object> map) {
        return number;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public Number getNumber() {
        return number;
    }

}
