package drds.plus.parser.abstract_syntax_tree.expression.comparison;

import drds.plus.parser.abstract_syntax_tree.expression.AbstractExpression;
import drds.plus.parser.abstract_syntax_tree.expression.Expression;
import drds.plus.parser.visitor.Visitor;

import java.util.Map;

public class Is extends AbstractExpression implements Expression {

    public static final int IS_NULL = 1;
    public static final int IS_TRUE = 2;
    public static final int IS_FALSE = 3;

    public static final int IS_NOT_NULL = 5;
    public static final int IS_NOT_TRUE = 6;
    public static final int IS_NOT_FALSE = 7;


    private final int mode;
    private final Expression expression;


    public Is(Expression expression, int mode) {
        this.expression = expression;
        this.mode = mode;
    }

    public int getMode() {
        return mode;
    }

    public Expression getExpression() {
        return expression;
    }

    public int getPriority() {
        return precedence_comparision;
    }

    public Object evalInternal(Map<? extends Object, ? extends Object> map) {
        return UNEVALUATABLE;
    }


    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
