package drds.plus.parser.abstract_syntax_tree.statement;

import drds.plus.parser.visitor.VisitorImpl;

import java.util.ArrayList;
import java.util.List;

public abstract class DmlStatement implements Statement {

    protected static List ensureListType(List list) {
        if (list == null || list.size() <= 0)
            return null;
        if (list instanceof ArrayList)
            return list;
        return new ArrayList(list);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        accept(new VisitorImpl(sb));
        return sb.toString();
    }
}
