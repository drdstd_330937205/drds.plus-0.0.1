package drds.plus.parser.abstract_syntax_tree.statement.select;

import drds.plus.parser.abstract_syntax_tree.Node;
import drds.plus.parser.abstract_syntax_tree.expression.Expression;
import drds.plus.parser.abstract_syntax_tree.expression.Pair;
import drds.plus.parser.visitor.Visitor;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


public class OrderBy implements Node {

    private final List<Pair<Expression, Order>> orderByList;

    public OrderBy() {
        this.orderByList = new LinkedList<Pair<Expression, Order>>();
    }

    public OrderBy(Expression expression, Order order) {
        this.orderByList = new ArrayList<Pair<Expression, Order>>(1);
        this.orderByList.add(new Pair<Expression, Order>(expression, order));
    }

    public OrderBy(List<Pair<Expression, Order>> orderByList) {
        if (orderByList == null)
            throw new IllegalArgumentException("order list is null");
        this.orderByList = orderByList;
    }

    public void addOrderByItem(Expression expr, Order order) {
        orderByList.add(new Pair<Expression, Order>(expr, order));

    }

    public List<Pair<Expression, Order>> getOrderByList() {
        return orderByList;
    }


    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
