package drds.plus.parser.abstract_syntax_tree.statement;

import drds.plus.parser.abstract_syntax_tree.expression.Expression;
import drds.plus.parser.abstract_syntax_tree.expression.Pair;
import drds.plus.parser.abstract_syntax_tree.statement.select.GroupBy;
import drds.plus.parser.abstract_syntax_tree.statement.select.Limit;
import drds.plus.parser.abstract_syntax_tree.statement.select.OrderBy;
import drds.plus.parser.abstract_syntax_tree.statement.select.table.Tables;
import drds.plus.parser.visitor.Visitor;

import java.util.List;


public class SelectStatement extends QueryStatement {
    private final List<Pair<Expression, String>> selectItemPairList;
    private final Tables tables;
    private final Expression where;
    private final GroupBy groupBy;
    private final Expression having;
    private final OrderBy orderBy;
    private final Limit limit;
    private boolean distinct;//
    private boolean lockInShareMode = false;
    private boolean forUpdate = false;

    public SelectStatement(List<Pair<Expression, String>> selectItemPairList, Tables tables, Expression where, GroupBy groupBy, Expression having, OrderBy orderBy, Limit limit) {
        this.selectItemPairList = selectItemPairList;
        this.tables = tables;
        this.where = where;
        this.groupBy = groupBy;
        this.having = having;
        this.orderBy = orderBy;
        this.limit = limit;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isLockInShareMode() {
        return lockInShareMode;
    }

    public void setLockInShareMode(boolean lockInShareMode) {
        this.lockInShareMode = lockInShareMode;
    }

    public boolean isForUpdate() {
        return forUpdate;
    }

    public void setForUpdate(boolean forUpdate) {
        this.forUpdate = forUpdate;
    }

    public List<Pair<Expression, String>> getSelectItemPairList() {
        return selectItemPairList;
    }

    public Tables getTables() {
        return tables;
    }

    public Expression getWhere() {
        return where;
    }

    public GroupBy getGroupBy() {
        return groupBy;
    }

    public Expression getHaving() {
        return having;
    }

    public OrderBy getOrderBy() {
        return orderBy;
    }

    public Limit getLimit() {
        return limit;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }


}
