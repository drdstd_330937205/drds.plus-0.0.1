package drds.plus.parser.abstract_syntax_tree.statement;

import drds.plus.parser.abstract_syntax_tree.expression.primary.misc.Identifier;
import drds.plus.parser.abstract_syntax_tree.expression.primary.misc.RowValues;
import drds.plus.parser.visitor.Visitor;

import java.util.List;

public class ReplaceStatement extends InsertReplaceStatement {

    public ReplaceStatement(Identifier table, List<Identifier> columnNameList, List<RowValues> rowValuesList) {
        super(table, columnNameList, rowValuesList);

    }

    public ReplaceStatement(Identifier table, List<Identifier> columnNameList, Query select) {
        super(table, columnNameList, select);

    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

}
