package drds.plus.parser.abstract_syntax_tree.expression.comparison.range;

import drds.plus.parser.abstract_syntax_tree.expression.BinaryExpression;
import drds.plus.parser.abstract_syntax_tree.expression.Expression;
import drds.plus.parser.visitor.Visitor;

public class GreaterThan extends BinaryExpression {

    public GreaterThan(Expression left, Expression right) {
        super(left, right, precedence_comparision);
    }

    public String getOperator() {
        return ">";
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
