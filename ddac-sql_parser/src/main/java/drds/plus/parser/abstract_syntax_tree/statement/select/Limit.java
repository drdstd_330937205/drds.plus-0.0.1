package drds.plus.parser.abstract_syntax_tree.statement.select;

import drds.plus.parser.abstract_syntax_tree.Node;
import drds.plus.parser.abstract_syntax_tree.expression.primary.misc.ParameterMarker;
import drds.plus.parser.visitor.Visitor;


public class Limit implements Node {

    private final Number offset;
    private final Number size;
    private final ParameterMarker offsetParameterMarker;
    private final ParameterMarker sizeParameterMarker;

    public Limit(Number offset, Number size) {
        if (offset == null)
            throw new IllegalArgumentException();
        if (size == null)
            throw new IllegalArgumentException();
        this.offset = offset;
        this.size = size;
        this.offsetParameterMarker = null;
        this.sizeParameterMarker = null;
    }

    public Limit(Number offset, ParameterMarker sizeParameterMarker) {
        if (offset == null)
            throw new IllegalArgumentException();
        if (sizeParameterMarker == null)
            throw new IllegalArgumentException();
        this.offset = offset;
        this.size = null;
        this.offsetParameterMarker = null;
        this.sizeParameterMarker = sizeParameterMarker;
    }

    public Limit(ParameterMarker offsetParameterMarker, Number size) {
        if (offsetParameterMarker == null)
            throw new IllegalArgumentException();
        if (size == null)
            throw new IllegalArgumentException();
        this.offset = null;
        this.size = size;
        this.offsetParameterMarker = offsetParameterMarker;
        this.sizeParameterMarker = null;
    }

    public Limit(ParameterMarker offsetParameterMarker, ParameterMarker sizeParameterMarker) {
        if (offsetParameterMarker == null)
            throw new IllegalArgumentException();
        if (sizeParameterMarker == null)
            throw new IllegalArgumentException();
        this.offset = null;
        this.size = null;
        this.offsetParameterMarker = offsetParameterMarker;
        this.sizeParameterMarker = sizeParameterMarker;
    }

    public Object getOffset() {
        return offset == null ? offsetParameterMarker : offset.intValue();
    }

    public Object getSize() {
        return size == null ? sizeParameterMarker : size.intValue();
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
