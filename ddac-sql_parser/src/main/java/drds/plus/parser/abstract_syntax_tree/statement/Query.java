package drds.plus.parser.abstract_syntax_tree.statement;

import drds.plus.parser.abstract_syntax_tree.expression.Expression;

public interface Query extends Expression {
}
