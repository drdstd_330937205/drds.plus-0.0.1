package drds.plus.parser.abstract_syntax_tree.expression.comparison.fuzzy_matching;

import drds.plus.parser.abstract_syntax_tree.expression.AbstractExpression;
import drds.plus.parser.abstract_syntax_tree.expression.Expression;
import drds.plus.parser.visitor.Visitor;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;


public class Like extends AbstractExpression {
    @Setter
    @Getter
    private final boolean not;
    @Setter
    @Getter
    private final Expression comparee;
    @Setter
    @Getter
    private final Expression pattern;
    @Setter
    @Getter
    private final Expression escape;

    /**
     * @param escape null is no ESCAPE
     */
    public Like(boolean not, Expression comparee, Expression pattern, Expression escape) {
        this.not = not;
        this.comparee = comparee;
        this.pattern = pattern;
        this.escape = escape;


    }


    public int getPriority() {
        return precedence_comparision;
    }

    public Object evalInternal(Map<? extends Object, ? extends Object> map) {
        return UNEVALUATABLE;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
