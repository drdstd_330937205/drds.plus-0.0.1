package drds.plus.parser.abstract_syntax_tree.expression.primary.misc;

import drds.plus.parser.abstract_syntax_tree.expression.PrimaryExpression;
import drds.plus.parser.visitor.Visitor;

import java.util.Map;


public class ParameterMarker extends PrimaryExpression {

    private final int parameterIndex;

    /**
     * @param parameterIndex start from 1
     */
    public ParameterMarker(int parameterIndex) {
        this.parameterIndex = parameterIndex;
    }

    /**
     * @return start from 1
     */
    public int getParameterIndex() {
        return parameterIndex;
    }

    public int hashCode() {
        return parameterIndex;
    }

    public boolean equals(Object obj) {
        if (obj == this)
            return true;
        if (obj instanceof ParameterMarker) {
            ParameterMarker that = (ParameterMarker) obj;
            return this.parameterIndex == that.parameterIndex;
        }
        return false;
    }

    public Object evalInternal(Map<? extends Object, ? extends Object> map) {
        return map.get(parameterIndex);
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
