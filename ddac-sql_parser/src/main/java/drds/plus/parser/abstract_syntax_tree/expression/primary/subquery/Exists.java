package drds.plus.parser.abstract_syntax_tree.expression.primary.subquery;

import drds.plus.parser.abstract_syntax_tree.expression.PrimaryExpression;
import drds.plus.parser.abstract_syntax_tree.statement.Query;
import drds.plus.parser.visitor.Visitor;


public class Exists extends PrimaryExpression {

    private final Query query;

    public Exists(Query query) {
        if (query == null)
            throw new IllegalArgumentException("query is null for EXISTS expression");
        this.query = query;
    }

    /**
     * @return never null
     */
    public Query getQuery() {
        return query;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
