package drds.plus.parser.abstract_syntax_tree.statement;


import drds.plus.parser.abstract_syntax_tree.expression.Expression;
import drds.plus.parser.abstract_syntax_tree.expression.Pair;
import drds.plus.parser.abstract_syntax_tree.expression.primary.misc.Identifier;
import drds.plus.parser.abstract_syntax_tree.statement.select.table.ITable;
import drds.plus.parser.visitor.Visitor;

import java.util.List;

public class UpdateStatement extends DmlStatement {
    private ITable table;
    private List<Pair<Identifier, Expression>> values;
    private Expression where;

    public UpdateStatement(ITable table, List<Pair<Identifier, Expression>> values, Expression where) {
        this.table = table;
        this.values = values;
        this.where = where;
    }

    public ITable getTable() {
        return table;
    }

    public List<Pair<Identifier, Expression>> getValuePairList() {
        return values;
    }

    public Expression getWhere() {
        return where;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
