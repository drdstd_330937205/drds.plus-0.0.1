package drds.plus.parser.abstract_syntax_tree.expression.primary.misc;

import drds.plus.parser.visitor.Visitor;

/**
 * 通配符*
 */
public class Wildcard extends Identifier {

    public Wildcard(Identifier parent) {
        super(parent, "*");
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
