package drds.plus.parser.abstract_syntax_tree;

import drds.plus.parser.visitor.Visitor;

public interface Node {

    void accept(Visitor visitor);
}
