package drds.plus.parser.abstract_syntax_tree.expression.primary.misc;

import drds.plus.parser.abstract_syntax_tree.expression.Expression;
import drds.plus.parser.abstract_syntax_tree.expression.PrimaryExpression;
import drds.plus.parser.visitor.Visitor;
import lombok.NonNull;

import java.util.List;

public class RowValues extends PrimaryExpression {

    private final List<Expression> rowValueList;

    public RowValues(@NonNull List<Expression> rowValueList) {
        if (rowValueList == null) {
            throw new NullPointerException();
        }
        if (rowValueList.size() == 0) {
            throw new IllegalArgumentException();
        }
        this.rowValueList = rowValueList;
    }

    /**
     * @return never null
     */
    public List<Expression> getRowValueList() {
        return rowValueList;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
