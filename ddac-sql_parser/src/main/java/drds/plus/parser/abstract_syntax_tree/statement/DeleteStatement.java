package drds.plus.parser.abstract_syntax_tree.statement;

import drds.plus.parser.abstract_syntax_tree.expression.Expression;
import drds.plus.parser.abstract_syntax_tree.statement.select.table.ITable;
import drds.plus.parser.abstract_syntax_tree.statement.select.table.Table;
import drds.plus.parser.visitor.Visitor;

public class DeleteStatement extends DmlStatement {
    private final Table table;
    private final Expression where;

    public DeleteStatement(Table table, Expression where) {
        this.table = table;
        this.where = where;
    }


    public Expression getWhere() {
        return where;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public ITable getTable() {
        return table;
    }

    public String getTableName() {
        return table.getTableName().getText();
    }
}
