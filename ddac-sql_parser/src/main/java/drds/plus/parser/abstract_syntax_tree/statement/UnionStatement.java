package drds.plus.parser.abstract_syntax_tree.statement;

import drds.plus.parser.abstract_syntax_tree.statement.select.Limit;
import drds.plus.parser.abstract_syntax_tree.statement.select.OrderBy;
import drds.plus.parser.visitor.Visitor;

import java.util.LinkedList;
import java.util.List;

public class UnionStatement extends QueryStatement {
    /**
     * might be {@link LinkedList}
     */
    private final List<SelectStatement> selectStatementList;
    /**
     * <code>Mixed UNION types are treated such that a DISTINCT union overrides any ALL union to its left</code>
     * <br/>
     * 0 means all relations of selects are union all<br/>
     * last index of {@link #selectStatementList} means all relations of selects are
     * union distinct<br/>
     */
    private int firstDistinctIndex = 0;
    private OrderBy orderBy;
    private Limit limit;

    public UnionStatement(SelectStatement selectStatement) {
        super();
        this.selectStatementList = new LinkedList<SelectStatement>();
        this.selectStatementList.add(selectStatement);
    }

    public void addSelectStatement(SelectStatement selectStatement, boolean unionAll) {
        selectStatementList.add(selectStatement);
        if (!unionAll) {
            firstDistinctIndex = selectStatementList.size() - 1;
        }

    }

    public List<SelectStatement> getSelectStatementList() {
        return selectStatementList;
    }

    public int getFirstDistinctIndex() {
        return firstDistinctIndex;
    }

    public OrderBy getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(OrderBy orderBy) {
        this.orderBy = orderBy;

    }

    public Limit getLimit() {
        return limit;
    }

    public void setLimit(Limit limit) {
        this.limit = limit;

    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
