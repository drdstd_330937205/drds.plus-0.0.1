package drds.plus.parser.abstract_syntax_tree.statement;

public enum LockMode {
    UNDEF, FOR_UPDATE, LOCK_IN_SHARE_MODE
}
