package drds.plus.parser.abstract_syntax_tree.statement.select.table;

import drds.plus.parser.abstract_syntax_tree.expression.Expression;
import drds.plus.parser.visitor.Visitor;

public class InnerJoin implements ITable {

    private final ITable leftTable;
    private final ITable rightTable;
    private Expression on;

    public InnerJoin(ITable leftTable, ITable rightTable, Expression on) {
        super();
        this.leftTable = leftTable;
        this.rightTable = rightTable;
        this.on = on;

    }

    public InnerJoin(ITable leftTable, ITable rightTable) {
        this(leftTable, rightTable, null);
    }


    public ITable getLeftTable() {
        return leftTable;
    }

    public ITable getRightTable() {
        return rightTable;
    }

    public Expression getOn() {
        return on;
    }

    public boolean isSingleTable() {
        return false;
    }

    public int getPriority() {
        return ITable.precedence_join;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

}
