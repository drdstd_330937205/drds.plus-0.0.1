package drds.plus.parser.visitor;

import drds.plus.parser.abstract_syntax_tree.expression.BinaryExpression;
import drds.plus.parser.abstract_syntax_tree.expression.UnaryExpression;
import drds.plus.parser.abstract_syntax_tree.expression.comparison.Is;
import drds.plus.parser.abstract_syntax_tree.expression.comparison.fuzzy_matching.Like;
import drds.plus.parser.abstract_syntax_tree.expression.comparison.range.BetweenAnd;
import drds.plus.parser.abstract_syntax_tree.expression.comparison.range.Equal;
import drds.plus.parser.abstract_syntax_tree.expression.comparison.range.In;
import drds.plus.parser.abstract_syntax_tree.expression.logical.And;
import drds.plus.parser.abstract_syntax_tree.expression.logical.ListExpression;
import drds.plus.parser.abstract_syntax_tree.expression.logical.Or;
import drds.plus.parser.abstract_syntax_tree.expression.primary.function.Function;
import drds.plus.parser.abstract_syntax_tree.expression.primary.function.aggregation.*;
import drds.plus.parser.abstract_syntax_tree.expression.primary.literal.LiteralBoolean;
import drds.plus.parser.abstract_syntax_tree.expression.primary.literal.LiteralNull;
import drds.plus.parser.abstract_syntax_tree.expression.primary.literal.LiteralNumber;
import drds.plus.parser.abstract_syntax_tree.expression.primary.literal.LiteralString;
import drds.plus.parser.abstract_syntax_tree.expression.primary.misc.Identifier;
import drds.plus.parser.abstract_syntax_tree.expression.primary.misc.InList;
import drds.plus.parser.abstract_syntax_tree.expression.primary.misc.ParameterMarker;
import drds.plus.parser.abstract_syntax_tree.expression.primary.misc.RowValues;
import drds.plus.parser.abstract_syntax_tree.expression.primary.subquery.Exists;
import drds.plus.parser.abstract_syntax_tree.statement.*;
import drds.plus.parser.abstract_syntax_tree.statement.select.GroupBy;
import drds.plus.parser.abstract_syntax_tree.statement.select.Limit;
import drds.plus.parser.abstract_syntax_tree.statement.select.OrderBy;
import drds.plus.parser.abstract_syntax_tree.statement.select.table.*;

public interface Visitor {
    void visit(Identifier identifier);

    void visit(ParameterMarker parameterMarker);

    void visit(RowValues rowValues);
    //

    /**
     * statement1
     */
    void visit(InsertStatement insertStatement);

    void visit(ReplaceStatement replaceStatement);

    void visit(UpdateStatement updateStatement);

    void visit(DeleteStatement deleteStatement);

    void visit(TruncateStatement truncateStatement);

    void visit(SelectStatement selectStatement);

    void visit(UnionStatement unionStatement);

    /**
     * statement of selectStatement
     */
    void visit(OrderBy orderBy);

    void visit(Limit limit);

    void visit(GroupBy groupBy);

    /**
     * table
     */
    void visit(InnerJoin innerJoin);

    void visit(OuterJoin outerJoin);

    void visit(SubQuery subQuery);

    void visit(Exists exists);

    void visit(Table table);

    void visit(Tables tables);


    /**
     * 聚合函数
     */
    void visit(Function function);

    void visit(Max max);

    void visit(Min min);

    void visit(Sum sum);

    void visit(Count count);

    void visit(Avg avg);

    /**
     * 字面量
     */
    void visit(LiteralNull literalNull);

    void visit(LiteralBoolean literalBoolean);

    void visit(LiteralString literalString);

    void visit(LiteralNumber literalNumber);

    //
    void visit(UnaryExpression unaryExpression);

    void visit(BinaryExpression binaryExpression);

    //


    void visit(Or or);

    void visit(And and);

    void visit(Equal equal);

    void visit(Is is);

    void visit(Like like);

    void visit(BetweenAnd betweenAnd);

    //
    void visit(In in);

    void visit(ListExpression listExpression);

    void visit(InList inList);


    // -------------------------------------------------------


}
