package drds.plus.parser.lexer;


public class CharTypes {

    private final static boolean[] identifierFlags = new boolean[256];
    private final static boolean[] firstIdentifierFlags = new boolean[256];

    static {
        for (char $char = 0; $char < identifierFlags.length; ++$char) {
            if ($char >= 'A' && $char <= 'Z') {
                identifierFlags[$char] = true;
            } else if ($char >= 'a' && $char <= 'z') {
                identifierFlags[$char] = true;
            } else if ($char >= '0' && $char <= '9') {
                identifierFlags[$char] = true;
            }
        }
        identifierFlags['_'] = true;
    }

    static {
        for (char $char = 0; $char < firstIdentifierFlags.length; ++$char) {
            if ($char >= 'A' && $char <= 'Z') {
                firstIdentifierFlags[$char] = true;
            } else if ($char >= 'a' && $char <= 'z') {
                firstIdentifierFlags[$char] = true;
            }
        }
        firstIdentifierFlags['_'] = true;
    }

    public static boolean isIdentifier(char $char) {
        return $char > identifierFlags.length || identifierFlags[$char];
    }

    public static boolean firstCharIsIdentifier(char $char) {
        return $char > firstIdentifierFlags.length || firstIdentifierFlags[$char];
    }

    public static boolean isWhitespace(char $char) {
        return $char == ' ' || $char == '\n' || $char == '\n' || $char == '\t';
    }

    public static boolean isDigit(char $char) {
        return $char >= '0' && $char <= '9';
    }

}
