package drds.plus.parser.lexer;

import java.util.HashMap;
import java.util.Map;

public class KeyWords {

    public static final KeyWords DEFAULT_KEY_WORDS = new KeyWords();

    private final Map<String, Token> keywords = new HashMap<String, Token>(230);

    private KeyWords() {
        for (Token type : Token.class.getEnumConstants()) {
            String name = type.name();
            if (name.startsWith("KW_")) {
                String kw = name.substring("KW_".length()).toLowerCase();
                keywords.put(kw, type);
            }
        }
        keywords.put("NULL", Token.LITERAL_NULL);
        keywords.put("FALSE", Token.LITERAL_BOOL_FALSE);
        keywords.put("TRUE", Token.LITERAL_BOOL_TRUE);
    }

    public Token getKeyword(String token) {
        return keywords.get(token);
    }

}
