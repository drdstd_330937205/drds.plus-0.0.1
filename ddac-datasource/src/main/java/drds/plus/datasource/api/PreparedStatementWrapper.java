package drds.plus.datasource.api;

import drds.plus.common.jdbc.SqlTypeParser;

import java.math.BigDecimal;
import java.sql.*;

public class PreparedStatementWrapper extends StatementWrapper {

    protected final String sql;

    public PreparedStatementWrapper(Statement statement, ConnectionWrapper connectionWrapper, DataSourceWrapper dataSourceWrapper, String sql, String appName) {
        super(statement, connectionWrapper, dataSourceWrapper, appName);
        this.sql = sql;
    }

    public void addBatch() throws SQLException {
        ((PreparedStatement) statement).addBatch();
    }

    public void clearParameters() throws SQLException {
        ((PreparedStatement) statement).clearParameters();
    }

    public boolean execute() throws SQLException {
        if (SqlTypeParser.isQuerySql(sql)) {
            executeQuery();
            return true;
        } else {
            executeUpdate();
            return false;
        }
    }

    public ResultSetWrapper executeQuery() throws SQLException {

        ensureResultSetIsEmpty();
        recordReadTimes();
        increaseConcurrentRead();

        try {
            resultSetWrapper = new ResultSetWrapper(this, ((PreparedStatement) statement).executeQuery());
            return resultSetWrapper;
        } catch (SQLException e) {
            decreaseConcurrentRead();
            throw e;
        } finally {

        }
    }

    public int executeUpdate() throws SQLException {

        ensureResultSetIsEmpty();
        recordWriteTimes();
        increaseConcurrentWrite();
        try {
            return ((PreparedStatement) statement).executeUpdate();
        } catch (SQLException e) {
            throw e;
        } finally {

            decreaseConcurrentWrite();
        }
    }

    public ResultSetMetaData getMetaData() throws SQLException {
        // 这里直接返回元数据
        return ((PreparedStatement) statement).getMetaData();
    }

    public ParameterMetaData getParameterMetaData() throws SQLException {
        // 这里直接返回原数据
        return ((PreparedStatement) statement).getParameterMetaData();
    }

    public void setBigDecimal(int parameterIndex, BigDecimal x) throws SQLException {
        ((PreparedStatement) statement).setBigDecimal(parameterIndex, x);
    }

    public void setBlob(int i, Blob x) throws SQLException {
        ((PreparedStatement) statement).setBlob(i, x);
    }

    public void setBoolean(int parameterIndex, boolean x) throws SQLException {
        ((PreparedStatement) statement).setBoolean(parameterIndex, x);
    }

    public void setClob(int i, Clob x) throws SQLException {
        ((PreparedStatement) statement).setClob(i, x);
    }

    public void setDate(int parameterIndex, Date x) throws SQLException {
        ((PreparedStatement) statement).setDate(parameterIndex, x);
    }

    public void setDouble(int parameterIndex, double x) throws SQLException {
        ((PreparedStatement) statement).setDouble(parameterIndex, x);
    }

    public void setFloat(int parameterIndex, float x) throws SQLException {
        ((PreparedStatement) statement).setFloat(parameterIndex, x);
    }

    public void setInt(int parameterIndex, int x) throws SQLException {
        ((PreparedStatement) statement).setInt(parameterIndex, x);
    }

    public void setLong(int parameterIndex, long x) throws SQLException {
        ((PreparedStatement) statement).setLong(parameterIndex, x);
    }

    public void setNull(int parameterIndex, int sqlType) throws SQLException {
        ((PreparedStatement) statement).setNull(parameterIndex, sqlType);
    }

    public void setNull(int paramIndex, int sqlType, String typeName) throws SQLException {
        ((PreparedStatement) statement).setNull(paramIndex, sqlType, typeName);
    }

    public void setObject(int parameterIndex, Object x) throws SQLException {
        ((PreparedStatement) statement).setObject(parameterIndex, x);
    }

    public void setString(int parameterIndex, String x) throws SQLException {
        ((PreparedStatement) statement).setString(parameterIndex, x);
    }

    public void setTime(int parameterIndex, Time x) throws SQLException {
        ((PreparedStatement) statement).setTime(parameterIndex, x);
    }

    public void setTimestamp(int parameterIndex, Timestamp x) throws SQLException {
        ((PreparedStatement) statement).setTimestamp(parameterIndex, x);
    }

    public boolean isClosed() throws SQLException {
        return statement.isClosed();
    }

}
