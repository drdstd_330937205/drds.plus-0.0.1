package drds.plus.datasource.api;


/**
 * datasource_wrapper层通过ExceptionSorter检测到数据源不可用时抛出， 或者数据库不可用，同时没有trylock到重试机会时也抛出 便于group层重试
 */
public class NotAvailableException extends RuntimeException {


    public NotAvailableException() {
    }

    public NotAvailableException(String message) {
        super(message);
    }

    public NotAvailableException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotAvailableException(Throwable cause) {
        super(cause);
    }

    public NotAvailableException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
