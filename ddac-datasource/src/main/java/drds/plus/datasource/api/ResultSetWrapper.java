package drds.plus.datasource.api;

import java.math.BigDecimal;
import java.sql.*;

public class ResultSetWrapper {

    public final ResultSet resultSet;
    private final StatementWrapper statementWrapper;
    private boolean isClosed = false;

    public ResultSetWrapper(StatementWrapper statementWrapper, ResultSet resultSet) {
        super();
        this.statementWrapper = statementWrapper;
        this.resultSet = resultSet;
    }

    public void afterLast() throws SQLException {
        this.resultSet.afterLast();
    }

    public void beforeFirst() throws SQLException {
        this.resultSet.beforeFirst();
    }

    public void close() throws SQLException {
        if (isClosed) {
            return;
        }
        if (statementWrapper != null) {
            statementWrapper.decreaseConcurrentRead();
        }
        isClosed = true;
        this.resultSet.close();
    }

    public boolean first() throws SQLException {
        return this.resultSet.first();
    }

    public BigDecimal getBigDecimal(int columnIndex) throws SQLException {
        return this.resultSet.getBigDecimal(columnIndex);
    }

    public BigDecimal getBigDecimal(String columnName) throws SQLException {
        return this.resultSet.getBigDecimal(columnName);
    }

    public Clob getClob(int i) throws SQLException {
        return this.resultSet.getClob(i);
    }

    public Clob getClob(String colName) throws SQLException {
        return this.resultSet.getClob(colName);
    }

    public Date getDate(int columnIndex) throws SQLException {
        return this.resultSet.getDate(columnIndex);
    }

    public Date getDate(String columnName) throws SQLException {
        return this.resultSet.getDate(columnName);
    }

    public double getDouble(int columnIndex) throws SQLException {
        return this.resultSet.getDouble(columnIndex);
    }

    public double getDouble(String columnName) throws SQLException {
        return this.resultSet.getDouble(columnName);

    }

    public int getFetchDirection() throws SQLException {
        return this.resultSet.getFetchDirection();
    }

    public void setFetchDirection(int direction) throws SQLException {
        this.resultSet.setFetchDirection(direction);
    }

    public int getFetchSize() throws SQLException {
        return this.resultSet.getFetchSize();
    }

    public void setFetchSize(int rows) throws SQLException {
        this.resultSet.setFetchSize(rows);
    }

    public float getFloat(int columnIndex) throws SQLException {
        return this.resultSet.getFloat(columnIndex);
    }

    public float getFloat(String columnName) throws SQLException {
        return this.resultSet.getFloat(columnName);
    }

    public int getInt(int columnIndex) throws SQLException {
        return this.resultSet.getInt(columnIndex);
    }

    public int getInt(String columnName) throws SQLException {
        return this.resultSet.getInt(columnName);
    }

    public long getLong(int columnIndex) throws SQLException {
        return this.resultSet.getLong(columnIndex);
    }

    public long getLong(String columnName) throws SQLException {
        return this.resultSet.getLong(columnName);
    }

    public ResultSetMetaData getResultSetMetaData() throws SQLException {
        return this.resultSet.getMetaData();
    }

    public Object getObject(int columnIndex) throws SQLException {
        return this.resultSet.getObject(columnIndex);
    }

    public Object getObject(String columnName) throws SQLException {
        return this.resultSet.getObject(columnName);
    }

    public StatementWrapper getStatement() throws SQLException {
        return statementWrapper;
    }

    public String getString(int columnIndex) throws SQLException {
        return this.resultSet.getString(columnIndex);
    }

    public String getString(String columnName) throws SQLException {
        return this.resultSet.getString(columnName);
    }

    public Time getTime(int columnIndex) throws SQLException {
        return this.resultSet.getTime(columnIndex);
    }

    public Time getTime(String columnName) throws SQLException {
        return this.resultSet.getTime(columnName);
    }

    public Timestamp getTimestamp(int columnIndex) throws SQLException {
        return this.resultSet.getTimestamp(columnIndex);
    }

    public Timestamp getTimestamp(String columnName) throws SQLException {
        return this.resultSet.getTimestamp(columnName);
    }

    public boolean isAfterLast() throws SQLException {
        return this.resultSet.isAfterLast();
    }

    public boolean isBeforeFirst() throws SQLException {
        return this.resultSet.isBeforeFirst();
    }

    public boolean isFirst() throws SQLException {
        return this.resultSet.isFirst();
    }

    public boolean isLast() throws SQLException {
        return this.resultSet.isLast();
    }

    public boolean last() throws SQLException {
        return this.resultSet.last();
    }

    public boolean next() throws SQLException {
        return this.resultSet.next();
    }

    public boolean previous() throws SQLException {
        return this.resultSet.previous();
    }

    public boolean wasNull() throws SQLException {
        return this.resultSet.wasNull();
    }

}
