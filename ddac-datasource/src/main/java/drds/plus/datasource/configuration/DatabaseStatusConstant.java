package drds.plus.datasource.configuration;

public class DatabaseStatusConstant {
    public final static String database_status_read = "r";
    public final static String database_status_write = "w";
    public final static String database_status_write_read = "rw";
    public final static String database_status_not_available = "na";
}
