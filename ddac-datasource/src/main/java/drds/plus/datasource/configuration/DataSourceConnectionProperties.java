package drds.plus.datasource.configuration;

public class DataSourceConnectionProperties {
    /**
     * 当前数据库的名字
     */
    public volatile String datasourceId;
    public volatile String ip;
    public volatile String port;
    public volatile String databaseName;
    public volatile DatabaseStatus dataBaseStatus;
    /**
     * 线程count限制，0为不限制
     */
    public volatile int threadCountRestriction;
    /**
     * 允许并发写的最大个数，0为不限制
     */
    public volatile int maxConcurrentWriteRestrict;
    /**
     * 允许并发读的最大个数，0为不限制
     */
    public volatile int maxConcurrentReadRestrict;
}
