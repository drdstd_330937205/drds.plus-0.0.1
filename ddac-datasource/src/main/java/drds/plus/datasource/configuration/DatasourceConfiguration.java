package drds.plus.datasource.configuration;

import drds.plus.datasource.connection_restrict.ConnectionRestrictEntry;
import drds.tools.$;
import lombok.Data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class DatasourceConfiguration implements Cloneable {

    /**
     * 默认初始化的线程池连接数量
     */
    public static final int defaultInitPoolSize = 0;

    /**
     * 默认初始化的defaultMaxWait druid专用，目前是和jboss的blockingTimeout是同一个配置。运维人员请注意。
     */
    public static final int defaultMaxWait = 5000;

    private String ip;

    private String port;

    private String databaseName;

    private String username;

    private String password;


    private int initPoolSize = defaultInitPoolSize;

    private int minPoolSize;

    private int maxPoolSize;

    private int blockingTimeout = defaultMaxWait;

    private long idleTimeout;

    private DatabaseStatus dataBaseStatus;

    private String dataBaseStatusString;

    private Map<String, String> connectionProperties = new HashMap<String, String>();

    /**
     * 写 次数限制
     */
    private int writeRestrictTimes;

    /**
     * 读 次数限制
     */
    private int readRestrictTimes;

    /**
     * 统计时间片
     */
    private int timeSliceInMillis;

    /**
     * 线程技术count限制
     */
    private int threadCountRestrict;

    /**
     * 允许并发读的最大个数，0为不限制
     */
    private int maxConcurrentReadRestrict;

    /**
     * 允许并发写的最大个数，0为不限制
     */
    private int maxConcurrentWriteRestrict;

    private volatile boolean isSingleInGroup;

    /**
     * 应用连接限制: 限制某个应用键值的并发连接数。
     */
    private List<ConnectionRestrictEntry> connectionRestrictEntryList;

    private String connectionInitSql;

    public void setDataBaseStatusString(String dataBaseStatusString) {
        this.dataBaseStatusString = dataBaseStatusString;
        if ($.isNotNullAndNotEmpty(dataBaseStatusString)) {
            this.dataBaseStatus = DatabaseStatus.getDatabaseStatus(dataBaseStatusString);
        }
    }

    public DatasourceConfiguration clone() {
        DatasourceConfiguration datasourceConfiguration = null;
        try {
            datasourceConfiguration = (DatasourceConfiguration) super.clone();
        } catch (CloneNotSupportedException e) {
        }
        return datasourceConfiguration;
    }

}
