package drds.plus.datasource.configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * 数据源的常量设置类
 */
public class ConnectionPropertiesConfiguration {
    public static Map<String, String> DEFAULT_MYSQL_CONNECTION_PROPERTIES = new HashMap<String, String>(1);

    static {
        ConnectionPropertiesConfiguration.DEFAULT_MYSQL_CONNECTION_PROPERTIES.put("characterEncoding", "utf-8");
    }
}
