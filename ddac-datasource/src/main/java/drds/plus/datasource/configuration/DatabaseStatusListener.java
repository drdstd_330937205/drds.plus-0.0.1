package drds.plus.datasource.configuration;

/**
 * 数据库状态变化监听器
 */
public interface DatabaseStatusListener {

    void handle(DatabaseStatus oldDatabaseStatus, DatabaseStatus newDatabaseStatus);
}
