package drds.plus.datasource.configuration;

import drds.tools.$;

/**
 * 数据库状态变量
 */
public enum DatabaseStatus {

    /**
     * R只读
     */
    R_STATUS(DatabaseStatusConstant.database_status_read),
    /**
     * W只写
     */
    W_STATUS(DatabaseStatusConstant.database_status_write),
    /**
     * RW可读写
     */
    RW_STATUS(DatabaseStatusConstant.database_status_write_read),
    /**
     * NA不可读/写
     */
    NA_STATUS(DatabaseStatusConstant.database_status_not_available);

    private String status;

    DatabaseStatus(String status) {
        this.status = status;
    }

    public static DatabaseStatus getDatabaseStatus(String type) {
        DatabaseStatus dataBaseStatus = null;
        if ($.isNotNullAndNotEmpty(type)) {
            String typeStr = type.toUpperCase().trim();
            if (typeStr.length() > 1) {
                if (DatabaseStatus.NA_STATUS.getStatus().equals(typeStr)) {
                    dataBaseStatus = DatabaseStatus.NA_STATUS;
                } else if (!typeStr.contains(DatabaseStatus.NA_STATUS.getStatus()) && typeStr.contains(DatabaseStatus.R_STATUS.getStatus()) && typeStr.contains(DatabaseStatus.W_STATUS.getStatus())) {
                    dataBaseStatus = DatabaseStatus.RW_STATUS;
                }
            } else {
                if (DatabaseStatus.R_STATUS.getStatus().equals(typeStr)) {
                    dataBaseStatus = DatabaseStatus.R_STATUS;
                } else if (DatabaseStatus.W_STATUS.getStatus().equals(typeStr)) {
                    dataBaseStatus = DatabaseStatus.W_STATUS;
                }
            }
        }
        return dataBaseStatus;
    }

    public String getStatus() {
        return status;
    }

    public boolean isNaStatus() {
        return this == DatabaseStatus.NA_STATUS;
    }

    public boolean isRstatus() {
        return this == DatabaseStatus.R_STATUS || this == DatabaseStatus.RW_STATUS;
    }

    public boolean isWstatus() {
        return this == DatabaseStatus.W_STATUS || this == DatabaseStatus.RW_STATUS;
    }

    public boolean isRWstatus() {
        return this == DatabaseStatus.RW_STATUS;
    }
}
