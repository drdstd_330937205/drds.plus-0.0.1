package drds.plus.datasource.connection_restrict;

import drds.plus.common.thread_local.ThreadLocalMap;

/**
 * 提供给单独使用 DataSource 的用户指定应用连接限制的业务键 (Key) 以及其他执行信息。
 */
public class ConnectionRestrictHelper {

    /**
     * 指定应用连接限制的业务键 (Key)
     */
    public static final String CONN_RESTRICT_KEY = "CONN_RESTRICT_KEY";//需要这里进行配置

    public static Object getConnectRestrictKey() {
        return ThreadLocalMap.get(ConnectionRestrictHelper.CONN_RESTRICT_KEY);
    }

    public static void setConnRestrictKey(Object key) {
        ThreadLocalMap.put(ConnectionRestrictHelper.CONN_RESTRICT_KEY, key);
    }

    public static void removeConnRestrictKey() {
        ThreadLocalMap.remove(ConnectionRestrictHelper.CONN_RESTRICT_KEY);
    }
}
