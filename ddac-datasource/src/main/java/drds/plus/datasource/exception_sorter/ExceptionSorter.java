package drds.plus.datasource.exception_sorter;


import java.sql.SQLException;


public interface ExceptionSorter {


    boolean isFatalException(SQLException e);
}
