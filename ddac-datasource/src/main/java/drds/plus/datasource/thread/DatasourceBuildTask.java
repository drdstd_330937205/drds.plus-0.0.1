package drds.plus.datasource.thread;

import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 伴随节点进行初始化
 */
public class DatasourceBuildTask implements Callable {
    Map map = new ConcurrentHashMap();

    @Override
    public Object call() throws Exception {
        //针对不同的数据源进行初始化。由服务器端进行控制优先顺序进行初始化。可以组间并行和组内串行处理。
        DataBaseChange dataBaseChange = new DataBaseChange(null);
        DatasourceChange datasourceChange = new DatasourceChange(null);
        dataBaseChange.onDataRecieved(null, null);
        datasourceChange.onDataRecieved(null, null);
        doDataRecieved();
        return null;
    }

    private void doDataRecieved() {
        String applicationId = null;
        String datasourceId = null;
        String changeType = null;
        String data = null;
        //一个应用只能在各自的线程里面
        if ("DataBaseChange".equals(changeType)) {
            DataBaseChange dataBaseChange = (DataBaseChange) map.get(datasourceId);
            dataBaseChange.onDataRecieved(datasourceId, data);
        } else if ("DatasourceChange".equals(changeType)) {
            DatasourceChange datasourceChange = (DatasourceChange) map.get(datasourceId);
            datasourceChange.onDataRecieved(datasourceId, data);
        }

    }
}
