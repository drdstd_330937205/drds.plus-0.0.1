package drds.plus.datasource.common;

import drds.plus.datasource.configuration.DatasourceConfiguration;
import drds.plus.datasource.configuration.DatasourceConfigurationParser;
import drds.plus.datasource.connection_restrict.ConnectionRestrictEntry;
import org.junit.Assert;
import org.junit.Test;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class TAtomConfParserUnitTest {

    @Test
    public void parserTAtomDsConfDO_解析全局配置() throws IOException {
        String globaFile = "conf/oracle/globa.properties";
        String globaStr = PropLoadTestUtil.loadPropFile2String(globaFile);
        DatasourceConfiguration datasourceConfiguration = DatasourceConfigurationParser.parseDatasourceConfiguration(globaStr);
        Properties prop = PropLoadTestUtil.loadPropFromFile(globaFile);
        Assert.assertEquals(datasourceConfiguration.getIp(), prop.get(DatasourceConfigurationParser.GLOBA_IP_KEY));
        Assert.assertEquals(datasourceConfiguration.getPort(), prop.get(DatasourceConfigurationParser.GLOBA_PORT_KEY));
        Assert.assertEquals(datasourceConfiguration.getDatabaseName(), prop.get(DatasourceConfigurationParser.GLOBA_DB_NAME_KEY));

        Assert.assertEquals(datasourceConfiguration.getDataBaseStatusString(), prop.get(DatasourceConfigurationParser.GLOBA_DB_STATUS_KEY));
    }

    @Test
    public void parserTAtomDsConfDO_解析应用配置() throws IOException {
        String appFile = "conf/oracle/app.properties";
        String appStr = PropLoadTestUtil.loadPropFile2String(appFile);
        DatasourceConfiguration datasourceConfiguration = DatasourceConfigurationParser.parseDatasourceConfiguration(appStr);
        Properties prop = PropLoadTestUtil.loadPropFromFile(appFile);
        Assert.assertEquals(datasourceConfiguration.getUsername(), prop.get(DatasourceConfigurationParser.APP_USER_NAME_KEY));

        Assert.assertEquals(String.valueOf(datasourceConfiguration.getMinPoolSize()), prop.get(DatasourceConfigurationParser.APP_MIN_POOL_SIZE_KEY));
        Assert.assertEquals(String.valueOf(datasourceConfiguration.getMaxPoolSize()), prop.get(DatasourceConfigurationParser.APP_MAX_POOL_SIZE_KEY));
        Assert.assertEquals(String.valueOf(datasourceConfiguration.getIdleTimeout()), prop.get(DatasourceConfigurationParser.APP_IDLE_TIMEOUT_KEY));
        Assert.assertEquals(String.valueOf(datasourceConfiguration.getBlockingTimeout()), prop.get(DatasourceConfigurationParser.APP_BLOCKING_TIMEOUT_KEY));
        //Assert.assertEquals(String.valueOf(datasourceConfiguration.getPreparedStatementCacheSize()), prop.get(DatasourceConfigurationParser.APP_PREPARED_STATEMENT_CACHE_SIZE_KEY));
        Map<String, String> connectionProperties = DatasourceConfigurationParser.parserConPropStr2Map(prop.getProperty(DatasourceConfigurationParser.APP_CON_PROP_KEY));
        Assert.assertEquals(datasourceConfiguration.getConnectionProperties(), connectionProperties);
    }

    @Test
    public void parserPasswd_解析密码() throws IOException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {

    }

    @Test
    public void parseConnRestrictEntries_解析应用连接限制() {
        String connRestrictStr = "K1,K2,K3,,K4:80%; K5:60%; K6,K7,:90%; ,K8:1%; K9,:10; ,K10,K11:70%; *:16,50%; *:40%; *:,30%; ~:20;";
        List<ConnectionRestrictEntry> connRestrictEntries = DatasourceConfigurationParser.parseConnRestrictEntries(connRestrictStr, 30);
        for (ConnectionRestrictEntry connectionRestrictEntry : connRestrictEntries) {
            System.out.println(connectionRestrictEntry.toString());
        }
        Assert.assertEquals(10, connRestrictEntries.size());
        Assert.assertEquals(24, connRestrictEntries.get(0).getLimits());
        Assert.assertEquals(18, connRestrictEntries.get(1).getLimits());
        Assert.assertEquals(27, connRestrictEntries.get(2).getLimits());
        Assert.assertEquals(1, connRestrictEntries.get(3).getLimits());
        Assert.assertEquals(10, connRestrictEntries.get(4).getLimits());
        Assert.assertEquals(21, connRestrictEntries.get(5).getLimits());
        Assert.assertEquals(16, connRestrictEntries.get(6).getMaxHashSlotSize());
        Assert.assertEquals(15, connRestrictEntries.get(6).getLimits());
        Assert.assertEquals(1, connRestrictEntries.get(7).getMaxHashSlotSize());
        Assert.assertEquals(12, connRestrictEntries.get(7).getLimits());
        Assert.assertEquals(1, connRestrictEntries.get(8).getMaxHashSlotSize());
        Assert.assertEquals(9, connRestrictEntries.get(8).getLimits());
        Assert.assertEquals(20, connRestrictEntries.get(9).getLimits());
    }

}
