package drds.plus.datasource.common;

import drds.plus.datasource.configuration.UrlTools;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class TAtomConURLToolsUnitTest {

    @Test
    public void getMySqlConURL_拼装MySql_URL() {
        String mysqlUrl1 = UrlTools.getUrl("192.168.1.1", "3306", "mysqlTest", null);
        String actualMySql1 = "api:mysql://192.168.1.1:3306/mysqlTest?characterEncoding=gbk";
        Assert.assertEquals(mysqlUrl1, actualMySql1);

        Map<String, String> prams = new HashMap<String, String>();
        prams.put("key1", "value1");
        prams.put("key2", "value2");
        String mysqlUrl2 = UrlTools.getUrl("192.168.1.1", "3306", "mysqlTest", prams);
        String actualMySql2 = "api:mysql://192.168.1.1:3306/mysqlTest?key2=value2&key1=value1";
        Assert.assertEquals(mysqlUrl2, actualMySql2);
    }
}
