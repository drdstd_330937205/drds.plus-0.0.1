package drds.plus.datasource.common;


import drds.plus.datasource.BaseAtomTest;
import drds.plus.datasource.api.DataSource;
import drds.plus.datasource.configuration.DatasourceConfiguration;
import drds.plus.datasource.configuration.DatasourceConfigurationParser;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.util.Properties;

public class TAtomDataSourceTest extends BaseAtomTest {

    static String TEST_SQL = "query 1 from dual";

    @Test
    public void testInitTAtomDataSource_初始化() throws Exception {

    }


    @Test
    public void testMysqlChange_切换Ip() throws IOException, Exception {
        testChange(new ChangeTestConfig() {

            public Properties doChange(Properties prop) {
                prop.setProperty(DatasourceConfigurationParser.GLOBA_IP_KEY, "127.0.0.1");
                return prop;
            }
        }, new ChangeTestConfig() {

            public Properties doChange(Properties prop) {
                prop.setProperty(DatasourceConfigurationParser.GLOBA_IP_KEY, prop.getProperty(DatasourceConfigurationParser.GLOBA_IP_KEY));
                return prop;
            }
        }, "globa", "mysql", "testMysqlChange_切换Ip");
    }


    @Test
    public void testMysqlChange_切换Port() throws IOException, Exception {
        testChange(new ChangeTestConfig() {

            public Properties doChange(Properties prop) {
                prop.setProperty(DatasourceConfigurationParser.GLOBA_PORT_KEY, "1234");
                return prop;
            }
        }, new ChangeTestConfig() {

            public Properties doChange(Properties prop) {
                prop.setProperty(DatasourceConfigurationParser.GLOBA_PORT_KEY, prop.getProperty(DatasourceConfigurationParser.GLOBA_PORT_KEY));
                return prop;
            }
        }, "globa", "mysql", "testMysqlChange_切换Port");
    }


    @Test
    public void testMsqlChange_切换DbName() throws IOException, Exception {
        testChange(new ChangeTestConfig() {

            public Properties doChange(Properties prop) {
                prop.setProperty(DatasourceConfigurationParser.GLOBA_DB_NAME_KEY, "tddl_sample_x");
                return prop;
            }
        }, new ChangeTestConfig() {

            public Properties doChange(Properties prop) {
                String dbName = prop.getProperty(DatasourceConfigurationParser.GLOBA_DB_NAME_KEY);
                prop.setProperty(DatasourceConfigurationParser.GLOBA_DB_NAME_KEY, dbName);
                return prop;
            }
        }, "globa", "mysql", "testMsqlChange_切换DbName");
    }


    @Test
    public void testMysqlChange_切换userName() throws IOException, Exception {
        testChange(new ChangeTestConfig() {

            public Properties doChange(Properties prop) {
                prop.setProperty(DatasourceConfigurationParser.APP_USER_NAME_KEY, "test1");
                return prop;
            }
        }, new ChangeTestConfig() {

            public Properties doChange(Properties prop) {
                String dbName = prop.getProperty(DatasourceConfigurationParser.APP_USER_NAME_KEY);
                prop.setProperty(DatasourceConfigurationParser.APP_USER_NAME_KEY, dbName);
                return prop;
            }
        }, "applicationConfigurationFile", "mysql", "testMysqlChange_切换userName");
    }


    @Test
    @Ignore("druid运行时改变密码，短期内不会关闭老的链接")
    public void testMySqlChange_切换Passwd() throws IOException, Exception {
        testChangePasswd(new ChangeTestConfig() {

            public Properties doChange(Properties prop) {
                prop.setProperty(DatasourceConfigurationParser.PASSWD_ENC_PASSWD_KEY, "-1d5d861112cd38cbefd3c2fcad17eaaf");
                return prop;
            }
        }, new ChangeTestConfig() {

            public Properties doChange(Properties prop) {
                String encPasswd = prop.getProperty(DatasourceConfigurationParser.PASSWD_ENC_PASSWD_KEY);
                prop.setProperty(DatasourceConfigurationParser.PASSWD_ENC_PASSWD_KEY, encPasswd);
                return prop;
            }
        }, "mysql");
    }

    private void testChangePasswd(ChangeTestConfig change, ChangeTestConfig restore, String dbType) throws IOException, Exception {


    }

    private void testChange(ChangeTestConfig change, ChangeTestConfig restore, String type, String dbType, String methodName) throws IOException, Exception {
    }

    private DataSource createTAtomDataSource(String appName, String dbKey, String configName) throws IOException, Exception {
        // 全局配置
        String globaStr = PropLoadTestUtil.loadPropFile2String("conf/" + configName + "/globa.properties");
        // MockServer.setConfigInfo(ConnectionPropertiesConfiguration.getGlobalDataId(dbKey), globaStr);
        // 应用配置
        String appStr = PropLoadTestUtil.loadPropFile2String("conf/" + configName + "/applicationConfigurationFile.properties");
        // MockServer.setConfigInfo(ConnectionPropertiesConfiguration.getAppDataId(applicationId, dbKey), appStr);
        // 解析配置
        DatasourceConfiguration datasourceConfiguration = DatasourceConfigurationParser.parseDatasourceConfiguration(globaStr);
        // 密码配置
        String passwdStr = PropLoadTestUtil.loadPropFile2String("conf/" + configName + "/password.properties");
        // MockServer.setConfigInfo(ConnectionPropertiesConfiguration.getPasswdDataId(datasourceConfiguration.getDatabaseName(),
        // datasourceConfiguration.getDbType(), datasourceConfiguration.getUsername()), passwdStr);
        // 进行初始化
        DataSource tDataSource = new DataSource();
        tDataSource.setApplicationId(appName);
        tDataSource.setDatasourceId(dbKey);
        tDataSource.init();
        return tDataSource;
    }

    private interface ChangeTestConfig {

        Properties doChange(Properties prop);
    }
}
