package drds.plus.datasource.common;

import drds.plus.datasource.configuration.DatasourceConfigurationParser;
import drds.plus.datasource.connection_restrict.ConnectionRestrictEntry;
import drds.plus.datasource.connection_restrict.ConnectionRestrictHelper;
import drds.plus.datasource.connection_restrict.ConnectionRestrictSlot;
import drds.plus.datasource.connection_restrict.ConnectionRestrictor;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ConnRestrictorUnitTest {

    @Test
    public void findSlot_查找连接槽() {
        String connRestrictStr = "K1,K4,K7:80%; K2,K5,K8:60%; K3,K6,K9:70%; *:4,50%; ~:20;";
        List<ConnectionRestrictEntry> connRestrictEntries = DatasourceConfigurationParser.parseConnRestrictEntries(connRestrictStr, 30);
        for (ConnectionRestrictEntry connectionRestrictEntry : connRestrictEntries) {
            System.out.println(connectionRestrictEntry.toString());
        }
        ConnectionRestrictor connectionRestrictor = new ConnectionRestrictor(connRestrictEntries);
        ConnectionRestrictSlot slotK1 = connectionRestrictor.getConnectionRestrictSlot("K1");
        ConnectionRestrictSlot slotK2 = connectionRestrictor.getConnectionRestrictSlot("K2");
        ConnectionRestrictSlot slotK3 = connectionRestrictor.getConnectionRestrictSlot("K3");
        ConnectionRestrictSlot slotK4 = connectionRestrictor.getConnectionRestrictSlot("K4");
        ConnectionRestrictSlot slotK5 = connectionRestrictor.getConnectionRestrictSlot("K5");
        ConnectionRestrictSlot slotK6 = connectionRestrictor.getConnectionRestrictSlot("K6");
        ConnectionRestrictSlot slotK7 = connectionRestrictor.getConnectionRestrictSlot("K7");
        ConnectionRestrictSlot slotK8 = connectionRestrictor.getConnectionRestrictSlot("K8");
        ConnectionRestrictSlot slotK9 = connectionRestrictor.getConnectionRestrictSlot("K9");
        ConnectionRestrictSlot slotK10 = connectionRestrictor.getConnectionRestrictSlot("K10");
        ConnectionRestrictSlot slotK11 = connectionRestrictor.getConnectionRestrictSlot("K11");
        ConnectionRestrictSlot slotNull = connectionRestrictor.getConnectionRestrictSlot(null);

        // 应该都有槽
        Assert.assertNotNull(slotK1);
        Assert.assertNotNull(slotK2);
        Assert.assertNotNull(slotK3);
        Assert.assertNotNull(slotK4);
        Assert.assertNotNull(slotK5);
        Assert.assertNotNull(slotK6);
        Assert.assertNotNull(slotK7);
        Assert.assertNotNull(slotK8);
        Assert.assertNotNull(slotK9);
        Assert.assertNotNull(slotK10);
        Assert.assertNotNull(slotK11);
        Assert.assertNotNull(slotNull);

        // 同一组 Key 应该分配到相同的槽
        Assert.assertSame(slotK1, slotK4);
        Assert.assertSame(slotK4, slotK7);
        Assert.assertSame(slotK2, slotK5);
        Assert.assertSame(slotK5, slotK8);
        Assert.assertSame(slotK3, slotK6);
        Assert.assertSame(slotK6, slotK9);

        // 每个 Key 的限制应该正确
        Assert.assertEquals(24, slotK7.getLimits());
        Assert.assertEquals(18, slotK8.getLimits());
        Assert.assertEquals(21, slotK9.getLimits());
        Assert.assertEquals(15, slotK10.getLimits());
        Assert.assertEquals(15, slotK11.getLimits());
        Assert.assertEquals(20, slotNull.getLimits());
    }

    @Test
    public void findSlot_HASH槽测试() {
        String connRestrictStr = "*:80%";
        List<ConnectionRestrictEntry> connRestrictEntries = DatasourceConfigurationParser.parseConnRestrictEntries(connRestrictStr, 30);
        for (ConnectionRestrictEntry connectionRestrictEntry : connRestrictEntries) {
            System.out.println(connectionRestrictEntry.toString());
        }
        ConnectionRestrictor connectionRestrictor = new ConnectionRestrictor(connRestrictEntries);
        ConnectionRestrictSlot slotK1 = connectionRestrictor.getConnectionRestrictSlot("K1");
        ConnectionRestrictSlot slotK2 = connectionRestrictor.getConnectionRestrictSlot("K2");
        ConnectionRestrictSlot slotK3 = connectionRestrictor.getConnectionRestrictSlot("K3");
        ConnectionRestrictSlot slotNull = connectionRestrictor.getConnectionRestrictSlot(null);
        Assert.assertNotNull(slotK1);
        Assert.assertNotNull(slotK2);
        Assert.assertNotNull(slotK3);
        Assert.assertNull(slotNull);

        Assert.assertSame(slotK1, slotK2);
        Assert.assertSame(slotK2, slotK3);
        Assert.assertEquals(24, slotK1.getLimits());
    }

    @Test
    public void doRestrict_连接限制() {
        String connRestrictStr = "K1,K4,K7:80%; K2,K5,K8:60%; K3,K6,K9:70%; *:4,50%; ~:20;";
        List<ConnectionRestrictEntry> connRestrictEntries = DatasourceConfigurationParser.parseConnRestrictEntries(connRestrictStr, 30);
        ConnectionRestrictor connectionRestrictor = new ConnectionRestrictor(connRestrictEntries);
        List<ConnectionRestrictSlot> slotList = new ArrayList<ConnectionRestrictSlot>();
        int tries = 0;
        try {
            // K1 的分配检查
            ConnectionRestrictHelper.setConnRestrictKey("K1");
            for (tries = 0; tries < 30; tries++) {
                slotList.add(connectionRestrictor.tryAcquire(1));
            }
        } finally {
            // 分配数目检查
            Assert.assertEquals(24, slotList.size());
            ConnectionRestrictHelper.removeConnRestrictKey();
            for (ConnectionRestrictSlot connectionRestrictSlot : slotList) {
                connectionRestrictSlot.release();
            }
            slotList.clear();
        }

        try {
            // K5 的分配检查
            ConnectionRestrictHelper.setConnRestrictKey("K5");
            for (tries = 0; tries < 30; tries++) {
                slotList.add(connectionRestrictor.tryAcquire(1));
            }
        } finally {
            // 分配数目检查
            Assert.assertEquals(18, slotList.size());
            ConnectionRestrictHelper.removeConnRestrictKey();
            for (ConnectionRestrictSlot connectionRestrictSlot : slotList) {
                connectionRestrictSlot.release();
            }
            slotList.clear();
        }

        try {
            // K9 的分配检查
            ConnectionRestrictHelper.setConnRestrictKey("K9");
            for (tries = 0; tries < 30; tries++) {
                slotList.add(connectionRestrictor.tryAcquire(1));
            }
        } finally {
            // 分配数目检查
            Assert.assertEquals(21, slotList.size());
            ConnectionRestrictHelper.removeConnRestrictKey();
            for (ConnectionRestrictSlot connectionRestrictSlot : slotList) {
                connectionRestrictSlot.release();
            }
            slotList.clear();
        }

        try {
            // HASH 方式 K12 的分配检查
            ConnectionRestrictHelper.setConnRestrictKey("K12");
            for (tries = 0; tries < 30; tries++) {
                slotList.add(connectionRestrictor.tryAcquire(1));
            }
        } finally {
            // 分配数目检查
            Assert.assertEquals(15, slotList.size());
            ConnectionRestrictHelper.removeConnRestrictKey();
            for (ConnectionRestrictSlot connectionRestrictSlot : slotList) {
                connectionRestrictSlot.release();
            }
            slotList.clear();
        }

        try {
            // null Key 的分配检查
            ConnectionRestrictHelper.removeConnRestrictKey();
            for (tries = 0; tries < 20; tries++) {
                slotList.add(connectionRestrictor.tryAcquire(1));
            }
        } finally {
            // 分配数目检查
            Assert.assertEquals(20, slotList.size());
            for (ConnectionRestrictSlot connectionRestrictSlot : slotList) {
                connectionRestrictSlot.release();
            }
            slotList.clear();
        }
    }
}
